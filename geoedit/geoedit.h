/***************************************************************************
                          geoedit.h  -  description
                             -------------------
    begin                : Mon Apr 15 18:40:55 CEST 2002
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef GEOEDIT_H
#define GEOEDIT_H

#include "geoeditview.h"


////////////////////////////////////////

class GeoEditApp : public QObject, public GuiMainWindow {
 Q_OBJECT

 public:
  GeoEditApp();

  static void usage();


 private slots:
   void exitGeoEdit();


 private:
  STD_string filename;
  ImageSet pilot;
  Geometry geometry;
  GeoEditView *view;
};

#endif 

