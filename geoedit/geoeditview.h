/***************************************************************************
                          geoeditview.h  -  description
                             -------------------
    begin                : Mon Apr 15 18:40:55 CEST 2002
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef GEOEDITVIEW_H
#define GEOEDITVIEW_H


#include <odinqt/ldrwidget.h>

#include <odinpara/geometry.h>

#include "geoeditlabel.h"

#define MAGN_FACTOR 2
#define MIN_MATRIX_SIZE 256

class GeoEditView : public QWidget {
  Q_OBJECT

  public:
    GeoEditView(Geometry& geometry, ImageSet& pilot, unsigned int blowup, QWidget *parent);

  private slots:
    void geometryChanged();
    void emitDone();
    void setProjection(bool) {geometryChanged();}
    void setCrossSection(bool) {geometryChanged();}
    void sagSet();
    void corSet();
    void axiSet();

    void magnify0(int,int,int) {magnify(0);}
    void magnify1(int,int,int) {magnify(1);}
    void magnify2(int,int,int) {magnify(2);}

  signals:
    void donePressed();
    
    
  private:
    void magnify(int index);

    unsigned int coarse;

    Geometry& geometry_cache;
    ImageSet& pilot_cache;

    LDRwidget* blockwidget[n_geometry_modes];

    GuiGridLayout* grid;

    STD_list<GeoEditLabel*> labels;

    buttonBox* projection;
    buttonBox* crosssect;

    geometryMode old_mode;
};

#endif
