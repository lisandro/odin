/***************************************************************************
                          geoeditlabel.h  -  description
                             -------------------
    begin                : Tue Apr 16 2002
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef GEOEDITLABEL_H
#define GEOEDITLABEL_H

#include <odinqt/float3d.h>

#include <odinpara/geometry.h>

#include <odindata/image.h>

#define REL_READVEC_LENGTH 0.2
#define READVEC_ARROW_LENGTH 0.1
#define CROSSSECT_COLOR "Red"
#define PROJECTION_COLOR "Green"



////////////////////////////////////////


// for debugging geoedit component
class GeoEditComp {
 public:
  static const char* get_compName();
};


////////////////////////////////////////


class GeoEditLabel : public floatBox3D  {
   Q_OBJECT

public:
  GeoEditLabel(const Image& background, unsigned int coarseFactor, QWidget *parent);

  void drawImagingArea(const Geometry& ia, bool drawProj, bool drawCross);

private:
  int xcoord2labelxpos(double pos) { return label->xpos2labelxpos(int((double)label->get_nx()*(0.5+secureDivision(pos,backgr.get_geometry().get_FOV(readDirection)))));}
  int ycoord2labelypos(double pos) { return label->ypos2labelypos(int((double)label->get_ny()*(0.5+secureDivision(pos,backgr.get_geometry().get_FOV(phaseDirection)))));}

  void drawSliceProjection(const darray& cornersProj, GuiPainter& painter);
  void drawSliceCrossSection(const darray& connectPoints, double slicethick, GuiPainter& painter, unsigned int slice);

  void drawVoxelProjection(const darray& cornersProj, GuiPainter& painter);

  void drawReadVector(const dvector& readvecstart_proj, const dvector& readvec_proj, GuiPainter& painter);

  // overloading virtual function of floatBox3D
  void repaint() {if(ia_cache) drawImagingArea(*ia_cache,drawProj_cache,drawCross_cache);}

  Image backgr;
  unsigned int coarse;

  const Geometry* ia_cache;
  bool drawProj_cache;
  bool drawCross_cache;
};


#endif
