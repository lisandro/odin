#include "geoedit.h"

int main(int argc, char *argv[]) {

  // do this here so we do not have to query X server in order to get usage for manual
  if(hasHelpOption(argc, argv)) {GeoEditApp::usage();exit(0);}

  GuiApplication a(argc, argv); // debug handler will be initialized here
  GeoEditApp *geoedit=new GeoEditApp();
  return a.start(geoedit->get_widget());
}

