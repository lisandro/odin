#include "geoedit.h"

#include <odinpara/sample.h>


void GeoEditApp::usage() {
   STD_cout << "geoedit: Geometry editor of ODIN" << STD_endl;
   STD_cout << "Usage: geoedit [options] <geometryInfo-file>" << STD_endl;
   STD_cout << "Options:" << STD_endl;
   STD_cout << "\t -pilot <pilot-scan-file> : Load pilot scan as background" << STD_endl;
   STD_cout << "\t -sample <virtual-sample-file> :  Load sample as background" << STD_endl;
   STD_cout << "\t -blowup <factor> : Enlarge size of backround by this factor" << STD_endl;
   STD_cout << "\t" << LogBase::get_usage() << STD_endl;
   STD_cout << "\t" << helpUsage() << STD_endl;
}



GeoEditApp::GeoEditApp()
 : geometry("Geometry") {

  Log<GeoEditComp> odinlog("GeoEditApp","GeoEditApp(...)");


  // command line parsing
  char optval[ODIN_MAXCHAR];

  if(hasHelpOption(GuiApplication::argc(),GuiApplication::argv())) {usage();exit(0);}


  if(GuiApplication::argc()<=1) {
    filename = get_open_filename("Please select geometryInfo to start with ...", "", "", GuiMainWindow::get_widget());
  }else {
    getLastArgument(GuiApplication::argc(),GuiApplication::argv(),optval,ODIN_MAXCHAR);
    filename=optval;
  }
  if(filename=="") exit(0);



  if(geometry.load(filename)<=0) {
    ODINLOG(odinlog,warningLog) << "unable to load " << filename << " as geometry file, will be created" << STD_endl;
  }
  ODINLOG(odinlog,normalDebug) << "load done" << STD_endl;


  if(getCommandlineOption(GuiApplication::argc(),GuiApplication::argv(),"-pilot",optval,ODIN_MAXCHAR)) {
    pilot.load(optval);
  }
  ODINLOG(odinlog,normalDebug) << "pilot done" << STD_endl;



  if(getCommandlineOption(GuiApplication::argc(),GuiApplication::argv(),"-sample",optval,ODIN_MAXCHAR)) {
    Sample sample("sample",false);
    if(sample.load(optval)>0) {
      ODINLOG(odinlog,normalDebug) << "Creating pilot from sample" << STD_endl;
      pilot=ImageSet(sample);
    }
  }
  ODINLOG(odinlog,normalDebug) << "sample done" << STD_endl;


  set_caption((STD_string("GeoEdit " VERSION) + " - " + filename).c_str());

  unsigned int blowup=1;
  if(getCommandlineOption(GuiApplication::argc(),GuiApplication::argv(),"-blowup",optval,ODIN_MAXCHAR)) {
    blowup=atoi(optval);
  }

  view=new GeoEditView(geometry,pilot,blowup,GuiMainWindow::get_widget());
  connect(view,SIGNAL(donePressed()),this,SLOT( exitGeoEdit()));
  ODINLOG(odinlog,normalDebug) << "view done" << STD_endl;

  GuiMainWindow::show(view);
//  setCentralWidget(view);
}


void GeoEditApp::exitGeoEdit() {
  geometry.write(filename);
  GuiApplication::quit();
//  qApp->quit();
}

