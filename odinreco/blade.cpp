#include "blade.h"
#include "data.h"
#include "controller.h"
#include "bladegrid.h"

#include <odindata/correlation.h>
#include <odindata/gridding.h>
#include <odindata/fitting.h>

void RecoBladeGrid::init() {
  fft = false;
  fft.set_description("Perform partial FFT into image space before feeding data into rest of the pipeline");
  append_arg(fft,"fft");

  filtermask.set_funcpars("Triangle");
  filtermask.set_description("Filter function for k-space center. NoFilter = whole blade (not only center)");
  append_arg(filtermask,"filtermask");
  
}

bool RecoBladeGrid::process(RecoData& rd, RecoController& controller) {
  Log<Reco> odinlog(c_label(),"process");

  Range all=Range::all();

  ComplexData<4>& data=rd.data(Rank<4>());

  TinyVector<int,4> inshape=data.shape();
  TinyVector<int,3> inshape3d(inshape(1), inshape(2), inshape(3));
  ODINLOG(odinlog,normalDebug) << "inshape3d=" << inshape3d << STD_endl;

  float os_factor=rd.coord().overSamplingFactor;
  ODINLOG(odinlog,normalDebug) << "os_factor=" << os_factor << STD_endl;
  if(os_factor<1.0) {
    ODINLOG(odinlog,errorLog) << "os_factor(" << os_factor << ") < 1.0" << STD_endl;
    return false;
  }

  dvector angles=controller.dim_values(cycle);
  ODINLOG(odinlog,normalDebug) << "angles=" << angles.printbody() << STD_endl;

  TinyVector<float,3> fov; fov=1;
  // fov(0)=controller.protocol().geometry.get_FOV(sliceDirection);
  fov(1)=controller.protocol().geometry.get_FOV(phaseDirection);
  fov(2)=controller.protocol().geometry.get_FOV(readDirection);
  ODINLOG(odinlog,normalDebug) << "fov=" << fov << STD_endl;

  // Prepare Input Data
  if(rd.interpolated) measlines.update(*(rd.interpolated)); // take previously interpolated coords into account
  ivector indexvec3d=measlines.get_indices(rd.coord()); // Use only planes actually measured

  ComplexData<4> infixed(inshape(0),indexvec3d.size(),inshape(2),inshape(3));

  for(unsigned int i=0; i<indexvec3d.size(); i++) {

    int iphase3d = indexvec3d[i];
    infixed(all,i,all,all) = data(all,iphase3d,all,all);
  }

  // Grid blades
  BladeGrid bg;
  bg.init(inshape3d,fov,controller.image_size(),os_factor,filtermask);
  ComplexData<4> bladesgrid_kspace = bg(infixed,angles);

  // perform FFT if parameter is set
  if (fft) {
    TinyVector<int,4> fftdims(false,false,true,true);
    bladesgrid_kspace.partial_fft(fftdims,true);
  }

  data.resize(bladesgrid_kspace.shape());
  data = bladesgrid_kspace;

  return execute_next_step(rd,controller);
}

///////////////////////////////////////////////////////////////////////////

bool RecoBladeGrid::query(RecoQueryContext& context) {
  Log<Reco> odinlog(c_label(),"query");
  if(context.mode==RecoQueryContext::prep) {
    if(!measlines.init(context.coord, context.controller)) return false;
  }
  return RecoStep::query(context);
}

////////////////////////////////////////////////////////////////////////////////////

bool RecoCycleRot::process(RecoData& rd, RecoController& controller) {
  Log<Reco> odinlog(c_label(),"process");

  Range all=Range::all();

  dvector angles=controller.dim_values(cycle);
  if(!angles.size()) {
    ODINLOG(odinlog,errorLog) << "No angles available for cycle" << STD_endl;
    return false;
  }


  // feed each rotated dataset into rest of pipeline
  for(unsigned int i=0; i<angles.size(); i++) {

    RecoData rdcopy(rd);

    rdcopy.coord().index[cycle]=i;
    rdcopy.coord().index[cycle].set_numof(angles.size());

    ComplexData<3>& data=rdcopy.data(Rank<3>());

    RotMatrix rotmat;
    rotmat.set_inplane_rotation(-angles[i]);

    TinyVector<float,2> offset=0.0;
    TinyMatrix<float,2,2> rotation;
    for(int irow=0; irow<2; irow++) {
      for(int icol=0; icol<2; icol++) {
        rotation(1-irow,1-icol)=rotmat[irow][icol];
      }
    }

    CoordTransformation<STD_complex,2> transform(TinyVector<int,2>(data.extent(1),data.extent(2)), rotation, offset, 2.0);

    for(int iphase3d=0; iphase3d<data.extent(0); iphase3d++) {
      data(iphase3d,all,all)=transform(data(iphase3d,all,all));
    }

    if(!execute_next_step(rdcopy,controller)) return false;
  }

  return true;
}

////////////////////////////////////////////////////////////////////////////////////


void RecoBladeCorr::init() {

  phasecorr_filter.set_funcpars("NoFilter");
  phasecorr_filter.set_cmdline_option("bf").set_description("Filter function for blade phase correction");
  append_arg(phasecorr_filter,"phasecorr_filter");

  phasecorr_width = 1.0;
  phasecorr_width.set_cmdline_option("bfw").set_description("Width of the filter function used for blade phase correction.[0.0 (0%) ... 1.0 (100%) of blade width; -1.0 (100%) ... 0.0 (0%) of center width]");
  append_arg(phasecorr_width,"phasecorr_width");

}

///////////////////////////////////////////////////////////////////////////


bool RecoBladeCorr::process(RecoData& rd, RecoController& controller) {
  Log<Reco> odinlog(c_label(),"process");

  Range all=Range::all();

  ComplexData<3>& indata=rd.data(Rank<3>());

  float os_factor=rd.coord().overSamplingFactor;
  ODINLOG(odinlog,normalDebug) << "os_factor=" << os_factor << STD_endl;
  if(os_factor<1.0) {
    ODINLOG(odinlog,errorLog) << "os_factor(" << os_factor << ") < 1.0" << STD_endl;
    return false;
  }

  TinyVector<int,3> inshape3d(indata.shape());
  TinyVector<int,3> inshape3d_no(inshape3d(0), inshape3d(1), int(inshape3d(2)/os_factor+0.5)); // without oversampling
  ODINLOG(odinlog,normalDebug) << "inshape3d=" << inshape3d << STD_endl;
  ODINLOG(odinlog,normalDebug) << "inshape3d_no=" << inshape3d_no << STD_endl;

  int center_diameter=STD_min(inshape3d_no(1),inshape3d_no(2)); // Size of the fully oversampled center of k-space
  int kspace_diameter=STD_max(inshape3d_no(1),inshape3d_no(2)); // The size of the overall k-space
  ODINLOG(odinlog,normalDebug) << "center_diameter/kspace_diameter=" << center_diameter << "/" << kspace_diameter << STD_endl;


  TinyVector<int,3> diashape(inshape3d(0), kspace_diameter, int(kspace_diameter*os_factor+0.5));
  if(inshape3d(1)%2 != diashape(1)%2) diashape(1)++; // for correct placing at center
  if(inshape3d(2)%2 != diashape(2)%2) diashape(2)++; // for correct placing at center
  ODINLOG(odinlog,normalDebug) << "diashape=" << diashape << STD_endl;

  TinyVector<int,3> centshape(inshape3d(0), center_diameter, int(center_diameter*os_factor+0.5));
  if(inshape3d(1)%2 != centshape(1)%2) centshape(1)++; // for correct placing at center
  if(inshape3d(2)%2 != centshape(2)%2) centshape(2)++; // for correct placing at center
  ODINLOG(odinlog,normalDebug) << "centshape=" << centshape << STD_endl;


  TinyVector<int,3> lowsrc(0,             (inshape3d(1)-centshape(1))/2,  (inshape3d(2)-centshape(2))/2);
  TinyVector<int,3> uppsrc(inshape3d(0)-1,(inshape3d(1)+centshape(1))/2-1,(inshape3d(2)+centshape(2))/2-1);
  ODINLOG(odinlog,normalDebug) << "lowsrc=" << lowsrc << STD_endl;
  ODINLOG(odinlog,normalDebug) << "uppsrc=" << uppsrc << STD_endl;

  TinyVector<int,3> lowdst(0,             (diashape(1)-centshape(1))/2,  (diashape(2)-centshape(2))/2);
  TinyVector<int,3> uppdst(inshape3d(0)-1,(diashape(1)+centshape(1))/2-1,(diashape(2)+centshape(2))/2-1);
  ODINLOG(odinlog,normalDebug) << "lowdst=" << lowdst << STD_endl;
  ODINLOG(odinlog,normalDebug) << "uppdst=" << uppdst << STD_endl;


   // The domains when copying data from central part of blades to central part of square k-space
  RectDomain<3> srcdomain(lowsrc,uppsrc);
  RectDomain<3> dstdomain(lowdst,uppdst);

  // Consistency check
  TinyVector<int,3> srcrange=srcdomain.ubound()-srcdomain.lbound();
  TinyVector<int,3> dstrange=dstdomain.ubound()-dstdomain.lbound();
  if(srcrange!=dstrange) {
    ODINLOG(odinlog,errorLog) << "range error:" << STD_endl;
    ODINLOG(odinlog,errorLog) << "srcrange=" << srcrange << STD_endl;
    ODINLOG(odinlog,errorLog) << "dstrange=" << dstrange << STD_endl;
    return false;
  }


  TinyVector<int,3> lowpad(0,             (diashape(1)-inshape3d(1))/2,  (diashape(2)-inshape3d(2))/2);
  TinyVector<int,3> upppad(inshape3d(0)-1,(diashape(1)+inshape3d(1))/2-1,(diashape(2)+inshape3d(2))/2-1);
  RectDomain<3> paddomain(lowpad,upppad);
  ODINLOG(odinlog,normalDebug) << "lowpad=" << lowpad << STD_endl;
  ODINLOG(odinlog,normalDebug) << "upppad=" << upppad << STD_endl;

  // Remove apodization from ramp regridding
  STD_string deapo_postlabel="deapodize1d";
  bool has_deapo=false;
  ComplexData<3> deapo;
  if(controller.data_announced(deapo_postlabel)) {
    RecoData rddeapo;
    if(!controller.inquire_data(deapo_postlabel, rddeapo)) return false;
    const ComplexData<1>& deapoline=rddeapo.data(Rank<1>());
    if(int(deapoline.size())!=inshape3d(2)) {
      ODINLOG(odinlog,errorLog) << "Size mistmatch" << STD_endl;
      return false;
    }
    deapo.resize(inshape3d);
    for(int iphase3d=0; iphase3d<inshape3d(0); iphase3d++) {
      for(int iphase=0; iphase<inshape3d(1); iphase++) {
        deapo(iphase3d,iphase,all)=deapoline;
      }
    }
    deapo.congrid(diashape);
    has_deapo=true;
    // Data<float,3>(cabs(deapo)).autowrite("deapo.jdx");
  }

  bool isNoFilter =  (phasecorr_filter.get_function_name() == "NoFilter");

  ComplexData<3> blade_pad(diashape);
  blade_pad=STD_complex(0.0);

  // 2D Phase correction
  // Phase correction by windowed fourier transformation and phase removing
  if (isNoFilter) {  // simply remove all phase in image space
    blade_pad(paddomain) = indata; // Put blade at center of padded kspace    

    blade_pad.fft(true); // FFT into image space
    blade_pad = float2real(cabs(blade_pad)); // remove phase

  } else {
    // Low pass filter for k-space center
    Data<float,3> filtermask(inshape3d);
    filtermask=0.0;

    TinyVector<float,3> bounds(inshape3d);
    if (phasecorr_width < 0.0) 
      bounds(2) = centshape(2) * (-1) * phasecorr_width;
    else bounds(2) *= phasecorr_width;

    for(int j=0; j<inshape3d(1); j++) {
      for(int i=0; i<inshape3d(2); i++) {

        float dj=(j-0.5*inshape3d(1))/(0.5*bounds(1));
        float di=(i-0.5*inshape3d(2))/(0.5*bounds(2));

        float relradius=norm(dj,di);

        filtermask(all,j,i)=phasecorr_filter.calculate(relradius);
      }
    }
    // filtermask.autowrite("filtermask-" + phasecorr_filter.get_function_name() + ".nii");

    ComplexData<3> lowpass_kspace(diashape);

    // 2D Phase correction
    // Phase correction by windowed fourier transformation and phase removing
    Data<float,3> phasemap(diashape);
    lowpass_kspace=STD_complex(0.0);
    blade_pad=STD_complex(0.0);

    lowpass_kspace(paddomain) = indata * float2real(filtermask); // Smooth transition at edges

    blade_pad(paddomain) = indata; // Put blade at center of padded kspace    

    lowpass_kspace.fft(true);
    blade_pad.fft(true);

    phasemap = phase(lowpass_kspace);

    blade_pad=blade_pad*expc(float2imag(-phasemap)); // Phase correction
  }

  if(has_deapo) blade_pad=blade_pad*deapo; // deapodization in image space
  blade_pad.fft(false); // inverse FFT
  indata=blade_pad(paddomain);

  return execute_next_step(rd,controller);
}



///////////////////////////////////////////////////////////////////////////

void RecoBladeComb::init() {
  keyhole=false;
  keyhole.set_description("Keyhole combination of blades");
  append_arg(keyhole,"keyhole");

  corrweight=false;
  corrweight.set_cmdline_option("bcw").set_description("Correlation weighting of all blades against the mean of all blades (in image space)");
  append_arg(corrweight,"corrweight");

  keyhole_filter.set_funcpars("CosSq");
  keyhole_filter.set_cmdline_option("bkf").set_description("Blade-keyhole filter function (and its arguments)");
  append_arg(keyhole_filter,"keyhole_filter");

  kspaceweight_filter.set_funcpars("Triangle");
  kspaceweight_filter.set_cmdline_option("bwf").set_description("Window/Filter function for kspace weighting of each blade");
  append_arg(kspaceweight_filter,"kspaceweight_filter");

  kspaceweight_width = 1;
  kspaceweight_width.set_cmdline_option("bww").set_description("Window/Filter width for kspace weighting of each blade");
  append_arg(kspaceweight_width,"kspaceweight_width");

  transition=0.2;
  transition.set_cmdline_option("bkt").set_description("Blade-keyhole transition width (fraction of oversampled k-space center)");
  append_arg(transition,"transition");
}

///////////////////////////////////////////////////////////////////////////

bool RecoBladeComb::process(RecoData& rd, RecoController& controller) {
  Log<Reco> odinlog(c_label(),"process");

  Range all=Range::all();

  ComplexData<4>& indata=rd.data(Rank<4>());

  TinyVector<int,4> inshape=indata.shape();
  TinyVector<int,3> inshape3d(inshape(1), inshape(2), inshape(3));

  unsigned int nblades=inshape(0);
  dvector angles=controller.dim_values(cycle);
  ODINLOG(odinlog,normalDebug) << "angles=" << angles.printbody() << STD_endl;
  if(nblades!=angles.size()) {
    ODINLOG(odinlog,errorLog) << "nblades(" << nblades << ") != nangles(" << angles.size() << ")" << STD_endl;
    return false;
  }

  TinyVector<float,3> fov; fov=1;
//  fov(0)=controller.protocol().geometry.get_FOV(sliceDirection); // No regridding in line3d direction
  fov(1)=controller.protocol().geometry.get_FOV(phaseDirection);
  fov(2)=controller.protocol().geometry.get_FOV(readDirection);
  ODINLOG(odinlog,normalDebug) << "fov=" << fov << STD_endl;

  Data<float,1> corrweights(nblades);

  // correlation weighting
  if (corrweight) {

    float os_factor=rd.coord().overSamplingFactor;
    ODINLOG(odinlog,normalDebug) << "os_factor=" << os_factor << STD_endl;
    if(os_factor<1.0) {
      ODINLOG(odinlog,errorLog) << "os_factor(" << os_factor << ") < 1.0" << STD_endl;
      return false;
    }

    // Filter for k-space center
    LDRfilter kernel_center;
    kernel_center.set_funcpars("Triangle");

    // Prepare Input Data
    if(rd.interpolated) measlines.update(*(rd.interpolated)); // take previously interpolated coords into account
    ivector indexvec3d=measlines.get_indices(rd.coord()); // Use only planes actually measured

    ComplexData<4> infixed(inshape(0),indexvec3d.size(),inshape(2),inshape(3));

    for(unsigned int i=0; i<indexvec3d.size(); i++) {

      int iphase3d = indexvec3d[i];
      infixed(all,i,all,all) = indata(all,iphase3d,all,all);
    }

    // Grid blades
    BladeGrid bg;
    bg.init(inshape3d,fov,controller.image_size(),os_factor,kernel_center);
    ComplexData<4> bladesgrid_kspace = bg(infixed,angles);

    // FFT
    TinyVector<int,4> gridshape(bladesgrid_kspace.shape());
    Data<float,4> bladesgrid(gridshape(0),gridshape(1),gridshape(2),gridshape(3));
    TinyVector<bool,4> fftdims(false,false,true,true);
    bladesgrid_kspace.partial_fft(fftdims,true);
    bladesgrid = cabs(bladesgrid_kspace);
    //bladesgrid.autowrite("grid-blades.nii");

    // Calculate correlation coefficient for all blades and planes
    corrweights = 0.0;
    for (int iphase3d = 0; iphase3d < gridshape(1); iphase3d++) {

      // Merge all blades
      Data<float, 2> bladesmean(gridshape(2),gridshape(3));
      bladesmean = 0.0;
      for (unsigned int iblade = 0; iblade < nblades; iblade++) {
        bladesmean += bladesgrid(iblade,iphase3d,all,all);
      }
      bladesmean /= nblades;
      //bladesmean.autowrite("grid-blades-mean.nii");

      // Calculate correlation coefficient for all blades
      for (unsigned int iblade = 0; iblade < nblades; iblade++) {
        correlationResult cr = correlation(bladesmean,bladesgrid(iblade,iphase3d,all,all));
        corrweights(iblade) += cr.r;
      }
    }
    corrweights /= gridshape(1);
    ODINLOG(odinlog,normalDebug) << "corrweights=" << corrweights << STD_endl;
  } else corrweights = 1.0;


  int ikey=0; // the index for the blade filling the center of k-space in keyhole mode
  if(keyhole) {

    int irep=rd.coord().index[repetition];
    ikey=(irep+nblades/2)%nblades;
    ODINLOG(odinlog,normalDebug) << "ikey(" << irep << ")=" << ikey << STD_endl;
  }
  KspaceTraj* trajptr=0;

  trajmutex.lock();
  if(trajmap.find(ikey)==trajmap.end()) {

    trajptr=new KspaceTraj;
    trajmap[ikey]=trajptr;

    int npts=inshape(0)*inshape(2)*inshape(3); // cycles x 2D plane

    trajptr->resize(1,npts);

    TinyVector<float,3> kspace_center=inshape3d/2;  // correct index of center: odd N -> N/2 / even N -> (N-1)/2
    ODINLOG(odinlog,normalDebug) << "kspace_center=" << kspace_center << STD_endl;

    TinyVector<float,3> kmax=inshape3d*2.0*PII/fov;
    kmax(2)/=rd.coord().overSamplingFactor;

    float kcent_radius=0.5*STD_min(kmax(1),kmax(2));

    TinyVector<float,3> findex;
    TinyVector<float,3> kcoord;
    TinyVector<float,3> kcoord_shift;
    TinyVector<float,3> kcoord_rot;

    int itraj=0;
    for(int iblade=0; iblade<inshape(0); iblade++) {
      for(int iphase=0; iphase<inshape(2); iphase++) {
        for(int iread=0; iread<inshape(3); iread++) {

          TinyVector<int,3> inplaneindex(0, iphase, iread);
          double angle_rad=-angles[iblade]; // Reverse blade rotation

          findex=(inplaneindex-kspace_center)/inshape3d;
          kcoord=findex*kmax;
          kcoord_shift=kcoord;

          float si=sin(angle_rad);
          float co=cos(angle_rad);

          /*if (shiftPhaseCorr) {
            kcoord_shift(1) += koffset(iblade,0) / inshape3d(1) * kmax(1);
            kcoord_shift(2) += koffset(iblade,1) / inshape3d(2) * kmax(2);
          }*/

          kcoord_rot(0)=0.0;
          kcoord_rot(1)=co*kcoord_shift(1)-si*kcoord_shift(2);
          kcoord_rot(2)=si*kcoord_shift(1)+co*kcoord_shift(2);
          (*trajptr)(0,itraj).coord=kcoord_rot;

          float weight = corrweights(iblade);

          float kcoord_temp;
          if (kmax(1) > kmax(2)) kcoord_temp = kcoord(2); // select the smaller(=y) coordinate
            else kcoord_temp = kcoord(1);
          float y = secureDivision(abs(kcoord_temp),kcent_radius);
          if (y > 1) // ouside of the blade
            weight = 0.0;
          else weight *= kspaceweight_filter.calculate(kspaceweight_width * y);

          if(keyhole && iblade!=ikey) {
            float relradius=secureDivision(norm(kcoord_rot(1),kcoord_rot(2)),kcent_radius);
            float plateau=1.0-transition;
            float r=1.0-secureDivision(relradius-plateau,transition);
            if(r>=0.0 && r<=1.0) weight*=keyhole_filter.calculate(r);
            if(r>1.0) weight=0.0;
          }

          (*trajptr)(0,itraj).weight=weight;
          itraj++;
        }
      }
    }
  } else {
    trajptr=trajmap[ikey];
  }
  trajmutex.unlock();



  if(rd.interpolated) measlines.update(*(rd.interpolated)); // take previously interpolated coords into account

  ivector indexvec3d=measlines.get_indices(rd.coord()); // Use only planes actually measured

  // feed each 3d-phase encoded k-space plane separately into rest of pipeline
  for(unsigned int i=0; i<indexvec3d.size(); i++) {

    int iphase3d=indexvec3d[i];

    RecoData rdcopy(rd.coord());

    rdcopy.coord().kspaceTraj=trajptr;
    rdcopy.coord().index[cycle]=0; // reset for grid step
    rdcopy.coord().index[cycle].set_numof(1);
    rdcopy.coord().overSamplingFactor=1.0; // Oversampling will be removed while regridding

    rdcopy.coord().index[line3d].set_numof(inshape3d(0));
    rdcopy.coord().index[line3d]=iphase3d;

    ComplexData<1>& outdata=rdcopy.data(Rank<1>());
    ComplexData<3> indata2d;
    indata2d.reference(indata(all,iphase3d,all,all));
    indata2d.convert_to(outdata);

    if(!execute_next_step(rdcopy,controller)) return false;
  }

  return true;
}

///////////////////////////////////////////////////////////////////////////

bool RecoBladeComb::query(RecoQueryContext& context) {
  Log<Reco> odinlog(c_label(),"query");
  if(context.mode==RecoQueryContext::prep) {
    if(!measlines.init(context.coord, context.controller)) return false;
  }
  return RecoStep::query(context);
}

///////////////////////////////////////////////////////////////////////////

RecoBladeComb::~RecoBladeComb() {
  Log<Reco> odinlog("","~RecoBladeComb");

  for(STD_map<int,KspaceTraj*>::iterator it=trajmap.begin(); it!=trajmap.end(); ++it) {
    delete it->second;
  }
}

