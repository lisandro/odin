/***************************************************************************
                          data.h  -  description
                             -------------------
    begin                : Sat Dec 30 2006
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef RECODATA_H
#define RECODATA_H

#include <odinreco/odinreco.h>
#include <odinreco/index.h>



/**
  * @addtogroup odinreco
  * @{
  */

///////////////////////////////////////////////////////


/**
  * Helper class to resolve function overloading
  */
template <int N_rank> struct Rank {};


///////////////////////////////////////////////////////


class Profiler; // forward declaration

/**
  * Class to hold intermediate results (multidimensional complex data) of steps in the reconstruction pipeline.
  */
class RecoData {

 public:

/**
  * Mode of complex data:
  * - complex_data       : Regular complex data.
  * - real_data          : Use only real part.
  * - weighted_real_data : weight real part by imaginray part.
  */
  enum dataMode {complex_data=0, real_data, weighted_real_data};

/**
  * Creates object with initial k-space coordinate 'coord'.
  */
  RecoData(const RecoCoord& coord=RecoCoord()) : override_protocol(0), mode(complex_data), interpolated(0), kcoord(coord), prof(0) {}

/**
  * Copy constructor (deep copy)
  */
  RecoData(const RecoData& rd) : prof(0) {RecoData::operator = (rd);}

/**
  * Destructor
  */
  ~RecoData();

/**
  * Assignment operator (deep copy)
  */
  RecoData& operator = (const RecoData& rd);


/**
  * Copy only the meta data from 'rd'
  */
  RecoData& copy_meta(const RecoData& rd);


/**
  * Returns reference to current N-dimensional complex data.
  * The dimension is determined according to the instantiation of template 'Rank'.
  */
  ComplexData<1>& data(Rank<1>) const {return data1;}
  ComplexData<2>& data(Rank<2>) const {return data2;}
  ComplexData<3>& data(Rank<3>) const {return data3;}
  ComplexData<4>& data(Rank<4>) const {return data4;}
  ComplexData<5>& data(Rank<5>) const {return data5;}
  ComplexData<6>& data(Rank<6>) const {return data6;}
  ComplexData<7>& data(Rank<7>) const {return data7;}
  ComplexData<8>& data(Rank<8>) const {return data8;}

/**
  * Returns reference to the k-space coordinate.
  */
  RecoCoord& coord() {return kcoord;}

/**
  * Free (deallocate) data with dimension 'dim'.
  */
  RecoData& free(int dim);

/**
  * Returns const reference to the k-space coordinate.
  */
  const RecoCoord& coord() const {return kcoord;}

/**
  * Override protocol in output files if non-zero
  */
  const Protocol* override_protocol;

/**
  * Type of data.
  */
  dataMode mode;

/**
  * Coordinates interpolated during pipeline execution.
  */
  const CoordCountMap* interpolated;

/**
  * Initializes new profiler object which is allocated/deallocted at the entry and exit of each step
  * to measure the performance of the pipeline.
  */
  void new_profiler(const STD_string& steplabel);


 private:

  RecoCoord kcoord;

  const Profiler* prof;

  mutable ComplexData<1> data1;
  mutable ComplexData<2> data2;
  mutable ComplexData<3> data3;
  mutable ComplexData<4> data4;
  mutable ComplexData<5> data5;
  mutable ComplexData<6> data6;
  mutable ComplexData<7> data7;
  mutable ComplexData<8> data8;

};




/** @}
  */


#endif

