/***************************************************************************
                          dump.h  -  description
                             -------------------
    begin                : Mon Oct 13 2008
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef RECODUMP_H
#define RECODUMP_H

#include "step.h"

class RecoDump : public RecoStep {

  // implementing virtual functions of RecoStep
  STD_string label() const {return "dump";}
  STD_string description() const {return "Dump current data in reconstruction pipeline";}
  bool process(RecoData& rd, RecoController& controller);
  RecoCoord input_coord() const {return RecoCoord::any();}
  void modify_coord(RecoCoord& coord) const {}
  RecoStep* allocate() const {return new RecoDump;}
  void init();

  LDRstring prefix;
  LDRstring format;

};


#endif

