/***************************************************************************
                          reader_ismrmrd.h  -  description
                             -------------------
    begin                : Fri Oct 2 2015
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef RECOREADERISMRMRD_H
#define RECOREADERISMRMRD_H


#include "reader.h"


#ifdef ISMRMRDSUPPORT

namespace ISMRMRD {
  class Dataset;  // forward declaration
  class AcquisitionHeader; // forward declaration
  class Limit;  // forward declaration
}

/**
  * @addtogroup odinreco
  * @{
  */



/**
  * Raw-data reader for ISRMRM raw data, see http://ismrmrd.github.io/
  */
class RecoReaderISMRMRD : public virtual RecoReaderInterface {

 public:
  RecoReaderISMRMRD(LDRblock&) : dset(0) {}
  ~RecoReaderISMRMRD();

 private:

  // Implementing virtual functions of RecoReaderInterface
  bool init(const STD_string& input_filename);
  bool fetch(RecoCoord& coord, ComplexData<1>& adc);
  const STD_vector<RecoCoord>& get_coords() const {return coord_vec_cache;}
  dvector dim_values(recoDim dim) const;
  const TinyVector<float,3>& reloffset() const {return reloffset_cache;}
  STD_string image_proc() const {return "";}
  const TinyVector<int,3>& image_size() const {return size_cache;}
  const Protocol& protocol() const {return prot_cache;}
  STD_string seqrecipe() const {return "";} // Let controller generate recipe automatically
  STD_string postProc3D() const {return "";}
  STD_string preProc3D() const {return "";}
  STD_string cmdline_opts() const;

  // Cached values
  TinyVector<float,3> reloffset_cache;
  TinyVector<int,3> size_cache;
  Protocol prot_cache;

  ISMRMRD::Dataset* dset;

  mutable STD_vector<RecoCoord> coord_vec_cache; // Vector to hold reco coordinates (indices)
  KspaceTraj kspaceTraj_cache;
  unsigned int ismrmrd_nacq; // number of ADCs in ISMRMRD dataset
  unsigned int ismrmrd_count; // counter for ADCs in ISMRMRD dataset
  unsigned int coord_count; // counter for pipeline ADCs

  // data used during reco
  unsigned int chancount; // counter for channels
  ComplexData<2> rawdata; // cache rawdata because all channels are loaded at once
  unsigned int pre_discard_cache;
  unsigned int post_discard_cache;
  unsigned int nsamples_raw_cache;
  unsigned int nchan_cache;

  static void print_flags(ISMRMRD::AcquisitionHeader& acqhead);
  static bool ignore_acq(ISMRMRD::AcquisitionHeader& acqhead);
  static bool pe_offset(const ISMRMRD::Limit& limit, unsigned int& offset, float& partfour_prot);
};



/** @}
  */

#endif // ISMRMRDSUPPORT

#endif

