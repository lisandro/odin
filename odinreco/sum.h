/***************************************************************************
                          sum.h  -  description
                             -------------------
    begin                : Thu Mar 8 2007
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef RECOSUM_H
#define RECOSUM_H


#include "step.h"


/*
  * Template class to sum up data in one or more dimensions.
  * The rank of the unmodified collected data dimensions is is given by 'Nunmod'.
  * The particular unmodified dimension are given in 'Unmod' by a specialization of 'RecoDim'.
  * The numer of dimensions to sum up is given in 'Nsum' and specified in 'Sum' by
  * a specialization of 'RecoDim'.
  * The dimensions to sum up are expected leftmost in the input data array.
  * The optional parameter 'Magn' specifies whether only the magnitude will
  * be accumulated instead of the complex values.
  */
template<unsigned int Nunmod, class Unmod, unsigned int Nsum, class Sum, bool Magn=false>
class RecoSum : public RecoStep, public Labeled {

 public:
  RecoSum(const STD_string& sum_label) : Labeled(sum_label) {}

 private:

  // implementing virtual functions of RecoStep
  STD_string label() const {return get_label();}
  STD_string description() const {return "Sum up data in one or more dimensions";}
  bool process(RecoData& rd, RecoController& controller);
  RecoCoord input_coord() const {RecoCoord result=Unmod::preset_coord(RecoIndex::collected); Sum::modify(RecoIndex::collected,result); return result;}
  void modify_coord(RecoCoord& coord) const {Sum::modify(RecoIndex::single,coord);}
  RecoStep* allocate() const {return new RecoSum(get_label());}
  void init() {}

};



#endif

