#include "messer.h"
#include "data.h"
#include "controller.h"

#include <odindata/fitting.h>

#define NOISELEVEL 0.3
#define MAXT2 1000.0

bool RecoMesser::process(RecoData& rd, RecoController& controller) {
  Log<Reco> odinlog(c_label(),"process");

  Range all=Range::all();

  ComplexData<4>& indata=rd.data(Rank<4>());
  TinyVector<int,4> inshape=indata.shape();
  TinyVector<int,3> outshape(inshape(1), inshape(2), inshape(3));

  Protocol prot(controller.protocol()); // Local copy of protocol, used for override_protocol

  float TEse=prot.seqpars.get_EchoTime();

  dvector tes=controller.dim_values(userdef);
  ODINLOG(odinlog,normalDebug) << "tes=" << tes.printbody() << STD_endl;

  int ntes=inshape(0);

  if(ntes!=int(tes.size())) {
    ODINLOG(odinlog,errorLog) << "ntes=" << ntes << " does not match tes.size()=" << tes.size() << STD_endl;
    return false;
  }

  int nfid=0;
  int nser=0;
  for(int ite=0; ite<ntes; ite++) {
    if(tes[ite]<0.5*TEse) nfid++;
    else nser++;
  }
  ODINLOG(odinlog,normalDebug) << "nfid/nser=" << nfid << "/" << nser << STD_endl;

  Data<float,1> fidlog(nfid);
  Data<float,1> fidsigma(nfid);
  Data<float,1> fidte(nfid);
  Data<float,1> serlog(nser);
  Data<float,1> sersigma(nser);
  Data<float,1> serte(nser);

  int ite=0;
  for(int i=0; i<nfid; i++) {fidte(i)=tes[ite]; ite++;}
  for(int i=0; i<nser; i++) {serte(i)=tes[ite]; ite++;}


  LinearFunction linf;

  float noiselevel=NOISELEVEL*mean(cabs(indata(0,all,all,all)));

  Data<float,1> pixel(ntes);
  Data<float,3> T2(outshape); T2=0.0;
  Data<float,3> T2star(outshape); T2star=0.0;
  Data<float,3> S0(outshape); S0=0.0;
  Data<float,3> SE(outshape); SE=0.0;

  for(int iphase3d=0; iphase3d<inshape(1); iphase3d++) {
    for(int iphase=0; iphase<inshape(2); iphase++) {
      for(int iread=0; iread<inshape(3); iread++) {

        pixel=cabs(indata(all,iphase3d,iphase,iread));
        if(pixel(0)>noiselevel) {


          ite=0;

          float R2star=0.0;
          float R2minus=0.0;
          float s0=0.0;
          float se=0.0; // signal at Spin-Echo

          // fit FID part
          if(nfid) {
            for(int i=0; i<nfid; i++) {
              fidlog(i)=log(pixel(ite));
              fidsigma(i)=pow(pixel(ite), -1);
              ite++;
            }
            linf.fit(fidlog, fidsigma, fidte);
            R2star=-linf.m.val;
            s0=exp(linf.c.val);
          }

          // fit SER part
          if(nser) {
            for(int i=0; i<nser; i++) {
              serlog(i)=log(pixel(ite));
              sersigma(i)=pow(pixel(ite), -1);
              ite++;
            }
            linf.fit(serlog, sersigma, serte);
            R2minus=-linf.m.val;
            se=exp(linf.c.val+linf.m.val*TEse);
          }

          float t2star=secureInv( R2star);
          float t2=secureInv( 0.5*(R2star+R2minus));

          check_range<float>(t2, 0.0, MAXT2);
          check_range<float>(t2star, 0.0, MAXT2);

          T2star(iphase3d,iphase,iread)=t2star;
          T2(iphase3d,iphase,iread)=t2;
          S0(iphase3d,iphase,iread)=s0;

          SE(iphase3d,iphase,iread)=se;

        }

      }
    }
  }



  // modify coordinate
  rd.coord().index[userdef].set_numof(ntes+4);
  int iuserdef=0;

  rd.override_protocol=&prot;
  STD_string series;
  int number;
  prot.study.get_Series(series, number);



  // feed echoes separately into rest of pipeline
  for(ite=0; ite<ntes; ite++) {
    RecoData rdecho(rd);
    rdecho.data(Rank<3>()).resize(outshape);
    rdecho.data(Rank<3>())=indata(ite,all,all,all);
    rdecho.coord().index[userdef]=iuserdef;
    prot.study.set_Series(series+"_TE"+ftos(tes[ite]),number);
    if(!execute_next_step(rdecho,controller)) return false;
    iuserdef++;
  }


  // feed T2star into rest of pipeline
  RecoData rdT2star(rd);
  rdT2star.data(Rank<3>()).resize(outshape);
  rdT2star.data(Rank<3>())=float2real(T2star);
  rdT2star.coord().index[userdef]=iuserdef;
  prot.study.set_Series(series+"_T2star",number);
  if(!execute_next_step(rdT2star,controller)) return false;
  iuserdef++;

  // feed T2 into rest of pipeline
  RecoData rdT2(rd);
  rdT2.data(Rank<3>()).resize(outshape);
  rdT2.data(Rank<3>())=float2real(T2);
  rdT2.coord().index[userdef]=iuserdef;
  prot.study.set_Series(series+"_T2",number);
  if(!execute_next_step(rdT2,controller)) return false;
  iuserdef++;

  // feed S0 into rest of pipeline
  RecoData rdS0(rd);
  rdS0.data(Rank<3>()).resize(outshape);
  rdS0.data(Rank<3>())=float2real(S0);
  rdS0.coord().index[userdef]=iuserdef;
  prot.study.set_Series(series+"_S0",number);
  if(!execute_next_step(rdS0,controller)) return false;
  iuserdef++;

  // feed SE into rest of pipeline
  RecoData rdSE(rd);
  rdSE.data(Rank<3>()).resize(outshape);
  rdSE.data(Rank<3>())=float2real(SE);
  rdSE.coord().index[userdef]=iuserdef;
  prot.study.set_Series(series+"_SE",number);
  if(!execute_next_step(rdSE,controller)) return false;
  iuserdef++;



  return true;
}
