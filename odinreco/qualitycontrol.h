/***************************************************************************
                          qualitycontrol.h  -  description
                             -------------------
    begin                : Tue Feb 28 2011
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef QUALITYCONTROL_H
#define QUALITYCONTROL_H

#include "step.h"

class RecoQualityControlSpike : public RecoStep {

  // implementing virtual functions of RecoStep
  STD_string label() const {return "qcspike";}
  STD_string description() const {return "Check for spikes in data";}
  bool process(RecoData& rd, RecoController& controller);
  RecoCoord input_coord() const {return RecoCoord::coord_with_mode(RecoIndex::collected,line3d,line,readout);}
  void modify_coord(RecoCoord& coord) const {}
  bool query(RecoQueryContext& context);
  RecoStep* allocate() const {return new RecoQualityControlSpike;}
  void init() {}

  STD_map<unsigned int, UInt> spikecount; // separately for each channel
  Mutex spikemutex;

  LONGEST_INT total_acqpts;
  Mutex totalmutex;

};


#endif

