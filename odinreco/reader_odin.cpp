#include "reader_odin.h"

#define SWAB_SUFFIX "swab"

RecoReaderOdin::RecoReaderOdin(LDRblock& parblock) : rawdata_fileptr(NULL) { //, rawdata_bytecount(0) {

  fifoFile.set_cmdline_option("pf").set_description("File name of FIFO (named pipe) to obtain the raw data from");
  parblock.append(fifoFile);

  padZero.set_cmdline_option("pz").set_description("Pad missing raw data (of interrupted measurement) with zeroes");
  parblock.append(padZero);
}


RecoReaderOdin::~RecoReaderOdin() {
  if(rawdata_fileptr!=NULL) fclose(rawdata_fileptr);
}


bool RecoReaderOdin::concat_data(STD_string& rawfile, const STD_string& scandir, const STD_string& fnameprefix) {
  Log<Reco> odinlog("RecoReaderOdin","concat_data");

  // Find and sort file names
  svector fnames=browse_dir(scandir);
  STD_list<STD_string> sorted_names;
  for(unsigned int i=0; i<fnames.size(); i++) {
    if(fnames[i].find(fnameprefix)==0 && LDRfileName(fnames[i]).get_suffix()!=SWAB_SUFFIX) sorted_names.push_back(fnames[i]);
  }
  sorted_names.sort();
  fnames=list2vector(sorted_names);
  ODINLOG(odinlog,normalDebug) << "fnames=" << fnames.printbody() << STD_endl;
  if(!fnames.size()) {
    ODINLOG(odinlog,errorLog) << "No filename " << fnameprefix << " found in directory " << scandir << STD_endl;
    return false;
  }


  rawfile=scandir+SEPARATOR_STR+fnames[0];

  if(fnames.size()==1) return true;

  int fd;
  FILE* file_ptr=ODIN_FOPEN(rawfile.c_str(),modestring(appendMode));
  if(file_ptr==NULL) {
    ODINLOG(odinlog,errorLog) << "Unable to open file " << rawfile << STD_endl;
    return false;
  }

  // Concatting and removing secondary files
  for(unsigned int i=1; i<fnames.size(); i++) {
    STD_string fname=scandir+SEPARATOR_STR+fnames[i];
    LONGEST_INT fsize=filesize(fname.c_str());
    char* dataptr=(char*)filemap(fname, fsize, 0, true, fd);

    ODINLOG(odinlog,infoLog) << "Concatting raw data file " << fname << STD_endl;
    fwrite(dataptr,sizeof(char),fsize,file_ptr);

    if(dataptr && fd>0) fileunmap(fd, dataptr, fsize, 0);
    rmfile(fname.c_str());
  }

  if(file_ptr!=NULL) fclose(file_ptr);

  return true;
}



bool RecoReaderOdin::init(const STD_string& input_filename) {
  Log<Reco> odinlog("RecoReaderOdin","init");

  ODINLOG(odinlog,infoLog) << "Loading recoInfo ..." << STD_endl;
  if(recoInfo.load(input_filename)<0) return false;

  const LDRkSpaceCoords& coords=recoInfo.get_kSpaceCoords();
  if(!coords.size()) {
    ODINLOG(odinlog,errorLog) << "LDRkSpaceCoords empty" << STD_endl;
    return false;
  }

  // get weighting factors for different channels
  nChannels_cache=coords[0].channels; // assume the same for all ADCs
  channelFactors=recoInfo.get_ChannelScales();
  if(channelFactors.extent(firstDim)<int(nChannels_cache)) {
    ODINLOG(odinlog,warningLog) << "number of channels larger than number of channel scales, will use uniform scale" << STD_endl;
    channelFactors.resize(nChannels_cache);
    channelFactors=1.0;
  }

  // For backwards comaptability
  LDRtriple CoordinateScaling(0.0, 0.0, 0.0, "CoordinateScaling");
  if(CoordinateScaling.load(input_filename)>0) {
    ODINLOG(odinlog,infoLog) << "Using CoordinateScaling=" << CoordinateScaling.printbody() << STD_endl;
    if(CoordinateScaling[1]<0.0) {
      image_proc_cache="-pflip";
    }
  } else {
    image_proc_cache=recoInfo.get_ImageProc();
  }

  // Get offset, thereby reverse dimension order from (read,phase,slice) to (slice,phase,read)
  for(int idir=0; idir<3; idir++) {
    reloffset_cache(2-idir)=recoInfo.get_RelativeOffset()[idir];
  }

  // get read-out shape arrays (for ramp-sampling EPI)
  for(int j=0; j<MAX_NUMOF_READOUT_SHAPES; j++) {
    fvector shape;
    unsigned int dstsize;
    recoInfo.get_ReadoutShape(j,shape,dstsize);
    if(shape.size()) {
      readoutShape_cache[j].first.reference(Data<float,1>(shape));
      readoutShape_cache[j].second=dstsize;
      ODINLOG(odinlog,normalDebug) << "shape/dstsize(" << j << ")=" << readoutShape_cache[j].first.size() << "/" << readoutShape_cache[j].second << STD_endl;
    }
  }


  // get k-space trajectories
  for(int itraj=0; itraj<MAX_NUMOF_KSPACE_TRAJS; itraj++) {
    farray ktraj(recoInfo.get_kSpaceTraj(itraj));
    if(ktraj.dim()==3) {

      // fill trajectory values (kx,ky,kz)
      int nseg=ktraj.size(0);
      int npts=ktraj.size(1);

      kspaceTraj_cache[itraj].resize(nseg,npts);

      for(int iseg=0; iseg<nseg; iseg++) {
        for(int ipt=0; ipt<npts; ipt++) {
          kspaceTraj_cache[itraj](iseg,ipt).coord=TinyVector<float,3>(ktraj(iseg,ipt,2), ktraj(iseg,ipt,1), ktraj(iseg,ipt,0)); // reverse order of x,y,z
          kspaceTraj_cache[itraj](iseg,ipt).weight=1.0;
        }
      }
    }
  }

  // get weighting vectors
  for(int iweight=0; iweight<MAX_NUMOF_ADC_WEIGHTING_VECTORS; iweight++) {
    weightVec_cache[iweight].reference(Data<STD_complex,1>(recoInfo.get_AdcWeightVector(iweight)));
  }





  STD_string scandir=LDRfileName(input_filename).get_dirname();

  // Concat data and create filename
  if(fifoFile=="") {
    if(!concat_data(rawdata_filename, scandir, recoInfo.get_RawFile())) return false;
  } else {
    rawdata_filename=fifoFile;
  }

  // get format and endianess of raw data
  dataformat=recoInfo.get_DataFormat();
  if(dataformat=="long") dataformat="s32bit"; // for backwards compatability
  rawtypesize=TypeTraits::typesize(dataformat);
  if(!rawtypesize) {
    ODINLOG(odinlog,errorLog) << "Unrecognized dataformat >" << dataformat << "<" << STD_endl;
    return false;
  }
  bool little_endian=recoInfo.is_LittleEndian();
  ODINLOG(odinlog,infoLog) << "Raw data is in file " << rawdata_filename << ", dataformat=" << dataformat <<  ", typesize=" << rawtypesize << ", little endian=" << little_endian << STD_endl;

  // swap raw data if neccessary
  if(little_endian!=little_endian_byte_order()) {
    ODINLOG(odinlog,infoLog) << "Swabbing raw data ..." << STD_endl;
    STD_string newrawfile(rawdata_filename+"."+SWAB_SUFFIX);
    STD_string swabcmd("swab "+itos(rawtypesize)+" "+rawdata_filename+" "+newrawfile+" 20");
    system(swabcmd.c_str());
    rawdata_filename=newrawfile;
  }


  // precalculations to remove zero padding from fid
  nadc=recoInfo.numof_adcs();
  zeroesPerChunk=0;
  ODINLOG(odinlog,infoLog) << "nadc=" << nadc << STD_endl;

  if(fifoFile=="") {

    LONGEST_INT nAdcChunks=recoInfo.get_NumOfAdcChunks();
    LONGEST_INT rawheadersize=recoInfo.get_RawHeaderSize();
    LONGEST_INT fsize_bytes=filesize(rawdata_filename.c_str());

    if(fsize_bytes<=rawheadersize) {
      ODINLOG(odinlog,errorLog) << "File size of " << rawdata_filename << " too small (fsize_bytes=" << fsize_bytes << ", rawheadersize=" << rawheadersize << " )" << STD_endl;
      return false;
    }

    LONGEST_INT rawfilesize=(fsize_bytes-rawheadersize)/(2*rawtypesize);

    LONGEST_INT sizePaddedChunk=rawfilesize/nAdcChunks;

    LONGEST_INT samplesTotal=recoInfo.get_TotalNumOfSamples();

    if(samplesTotal>rawfilesize) {

      if(padZero) {
        LONGEST_INT padbytes=(samplesTotal-rawfilesize)*2*rawtypesize;
        ODINLOG(odinlog,infoLog) << "Padding raw data with " << padbytes << " bytes" << STD_endl;
        if(create_empty_file(rawdata_filename, padbytes, appendMode)) {
          ODINLOG(odinlog,errorLog) << "Cannot zero pad file : " << lasterr() << STD_endl;
          return false;
        }
        rawfilesize=samplesTotal;
      } else {
        ODINLOG(odinlog,errorLog) << "Total number of expected sampling points ( " <<samplesTotal << " ) larger than rawfilesize ( " << rawfilesize << " )" << STD_endl;
        return false;
      }
    }

    LONGEST_INT zeroesTotal=rawfilesize-samplesTotal;
    zeroesPerChunk=zeroesTotal/nAdcChunks;

    ODINLOG(odinlog,infoLog) << "nAdcChunks/rawheadersize/rawfilesize=" << nAdcChunks << "/" << rawheadersize << "/" << rawfilesize << STD_endl;
    ODINLOG(odinlog,infoLog) << "samplesTotal/zeroesTotal/sizePaddedChunk/zeroesPerChunk=" << samplesTotal << "/" << zeroesTotal << "/" << sizePaddedChunk << "/" << zeroesPerChunk << STD_endl;
  }


  // load protocol
  STD_string protfile=scandir+SEPARATOR_STR+"protocol";
  if(filesize(protfile.c_str())>0) {
    prot_cache.load(protfile); // Backwards compatability
    ODINLOG(odinlog,infoLog) << "Loading protocol from " << protfile << STD_endl;
    if(prot_cache.seqpars.get_EchoTime()<=0.0) {
      LDRfloatArr tes;
      tes.set_label("SliceOrder"); // separately instead of using constructor for GCC3.2
      tes.load(protfile);
      ODINLOG(odinlog,normalDebug) << "tes" << tes << STD_endl;
      if(tes.total()>0 && tes[0]>0.0) {
        prot_cache.seqpars.set_EchoTime(tes[0]);
        ODINLOG(odinlog,infoLog) << "Using TE=" << tes[0] << " from old-style TE array" << STD_endl;
      }
    }
  } else {
    prot_cache=recoInfo.get_protocol(); // The new way: Assume protocol is contained in recoInfo
    ODINLOG(odinlog,infoLog) << "Using protocol from recoInfo" << STD_endl;
  }

  size_cache(0)=1;
  if(prot_cache.geometry.get_Mode()==voxel_3d) size_cache(0)=prot_cache.seqpars.get_MatrixSize(sliceDirection);

  size_cache(1)=prot_cache.seqpars.get_MatrixSize(phaseDirection);
  size_cache(2)=prot_cache.seqpars.get_MatrixSize(readDirection);
  ODINLOG(odinlog,normalDebug) << "size_cache=" << size_cache << STD_endl;


  // If series number is zero, set series number according to current scandir number
  STD_string seriesdescr;
  int seriesno;
  prot_cache.study.get_Series(seriesdescr,seriesno);
  if(seriesno<=0) {
    seriesno=atoi(LDRfileName(scandir).get_basename().c_str()); // Try passed in scandir number first
  }
  if(seriesno<=0) {
    seriesno=atoi(LDRfileName(getpwd()).get_basename().c_str()); // Try PWD next
  }
  prot_cache.study.set_Series(seriesdescr,seriesno);
  ODINLOG(odinlog,infoLog) << "Using seriesno=" << seriesno << " for file conversion" << STD_endl;


  index=0;
  ri_index=0;
  chancount=0;

  return true;
}


template<typename T2>
bool RecoReaderOdin::read_block(T2 dummy, ComplexData<2>& block) {
  Log<OdinData> odinlog("RecoReaderOdin","read_block");
  bool result=true;

  // Delay opening opening raw data file until neccessary for reading of FIFO
  if(rawdata_fileptr==NULL) {
    rawdata_fileptr=ODIN_FOPEN(rawdata_filename.c_str(),modestring(readMode));
    if(rawdata_fileptr==NULL) {
      ODINLOG(odinlog,errorLog) << "fopen(" << rawdata_filename << "): " << lasterr() << STD_endl;
      return false;
    }
//    rawdata_bytecount=0;

    int offset_bytes=recoInfo.get_RawHeaderSize();
    if(offset_bytes>0) { // Only if neccessary, to allow reading from a named pipe
      if(ODIN_FSEEK(rawdata_fileptr, offset_bytes, SEEK_SET)) {
        ODINLOG(odinlog,errorLog) << "fseek: " << lasterr() << STD_endl;
        return false;
      }
//      rawdata_bytecount+=offset_bytes;
    }
  }

  int nitems=2*block.size();
  T2* ptr=new T2[nitems];

  int toberead=nitems;
  int currpos=0;
  while(toberead) { // While loop for FIFO
    int freadresult=fread(ptr+currpos, sizeof(T2), toberead, rawdata_fileptr);
    if(!freadresult) { // signals an error, even for a FIFO
      ODINLOG(odinlog,errorLog) << "fread() failed: " << lasterr() << STD_endl;
      ODINLOG(odinlog,errorLog) << "ftell=" << ODIN_FTELL(rawdata_fileptr) << STD_endl;
      result=false;
      break;
    }
    currpos+=freadresult;
    toberead=nitems-currpos;
  }

//  rawdata_bytecount+=2*rawtypesize*block.size();
//  LONGEST_INT ftellcount=FTELL(rawdata_fileptr);
//  if(rawdata_bytecount!=ftellcount) {
//    ODINLOG(odinlog,errorLog) << "file pointer out of sync: rawdata_bytecount/ftellcount=" << rawdata_bytecount << "/" << ftellcount << STD_endl;
//    return false;
//  }
  
  if(result) convert_from_ptr(block, ptr, block.shape());

  delete[] ptr;

  return result;
}


bool RecoReaderOdin::fetch(RecoCoord& coord, ComplexData<1>& adc) {
  Log<Reco> odinlog("RecoReaderOdin","fetch");

  if(index>=get_coords().size()) return false;

  coord=get_coords()[index];

  // Load next block of raw data
  if(!chancount) {

    kcoord_cache=recoInfo.get_kSpaceCoord(ri_index);

    rawdata.resize(kcoord_cache.channels,kcoord_cache.adcSize);
    bool readresult=false;
    if(dataformat==TypeTraits::type2label((s16bit)0)) readresult=read_block((s16bit)0, rawdata);
    if(dataformat==TypeTraits::type2label((s32bit)0)) readresult=read_block((s32bit)0, rawdata);
    if(dataformat==TypeTraits::type2label((float)0))  readresult=read_block((float)0,  rawdata);
    if(!readresult) {
      ODINLOG(odinlog,errorLog) << "Failed to read coord[" << ri_index << "]" << STD_endl;
      return false;
    }

     // skip padding at end of chunk
    if(kcoord_cache.flags&recoLastInChunkBit) {
      int skip_bytes=zeroesPerChunk*kcoord_cache.channels*rawtypesize*2;
      if(skip_bytes>0) { // Only if neccessary, to allow reading from a named pipe
        if(ODIN_FSEEK(rawdata_fileptr, skip_bytes, SEEK_CUR)) {
           ODINLOG(odinlog,errorLog) << "fseek: " << lasterr() << STD_endl;
          return false;
        }
//        rawdata_bytecount+=skip_bytes;
      }
    }

    // reorder data in case of conctatenated acquisitions
    if(kcoord_cache.concat>1) {
      if(kcoord_cache.adcSize%kcoord_cache.concat) {
        ODINLOG(odinlog,errorLog) << "adcSize(" << kcoord_cache.adcSize << ") not a multiple of concat(" << kcoord_cache.concat << ")" << STD_endl;
        return false;
      }
      int onesize=kcoord_cache.adcSize/kcoord_cache.concat;
      TinyVector<int,3> shape_concat(kcoord_cache.concat, kcoord_cache.channels, onesize);
      ODINLOG(odinlog,normalDebug) << "shape_concat=" << shape_concat << STD_endl;
      ComplexData<3> data_concat(Array<STD_complex,3>(rawdata.c_array(), shape_concat, neverDeleteData).copy());

      Range all=Range::all();
      for(int icat=0; icat<kcoord_cache.concat; icat++) {
        int base=icat*onesize;
        Range catrange(base,base+onesize-1);
        rawdata(all,catrange)=data_concat(icat,all,all);
      }
    }

    ri_index++;
  }


  // Crop ADC
  int insize=rawdata.extent(1);
  int pre=kcoord_cache.preDiscard;
  int post=kcoord_cache.postDiscard;
  int outsize=insize-pre-post; // Size without discarded points

  if(outsize<0) {
    ODINLOG(odinlog,errorLog) << "outsize=" << outsize << STD_endl;
    return false;
  }

  adc.resize(outsize);
  adc=rawdata(chancount,Range(pre,pre+outsize-1));
  adc*=STD_complex(channelFactors(chancount));

  index++;
  chancount++;
  if(chancount>=kcoord_cache.channels) { // get next coord if channels complete
    chancount=0;
  }

  return true;
}


void RecoReaderOdin::kcoord2coord(const kSpaceCoord& kcoord, RecoCoord& coord) const {
  Log<Reco> odinlog("RecoReaderOdin","kcoord2coord");
  for(int i=0; i<n_recoIndexDims; i++) coord.index[i]=kcoord.index[i];
  for(int i=n_recoIndexDims; i<n_recoDims; i++) coord.index[i]=0;
  coord.index[readout].set_numof(kcoord.adcSize-kcoord.preDiscard-kcoord.postDiscard); // Initial size of readout
  coord.flags=kcoord.flags;
  if(kcoord.readoutIndex>=0) coord.readoutShape=&(readoutShape_cache[kcoord.readoutIndex]);
  if(kcoord.trajIndex>=0) coord.kspaceTraj=&(kspaceTraj_cache[kcoord.trajIndex]);
  if(kcoord.weightIndex>=0) coord.weightVec=&(weightVec_cache[kcoord.weightIndex]);
  coord.dwellTime=recoInfo.get_DwellTime(kcoord.dtIndex);
  coord.overSamplingFactor=kcoord.oversampling;
  coord.relCenterPos=kcoord.relcenter;

  coord.echopos=coord.index[echo]; // Move echo index to echopos to avoid it to interfere with other indices
  coord.echotrain=coord.index[epi]; // Move EPI index to echotrain to avoid it to interfere with other indices

  if(coord.index[templtype]==phasecorr_template) { // PE indices can and should be ignored
    coord.index[line]=0;
    coord.index[line3d]=0;
  } else {
    coord.index[echo]=0; // ignore, echopos will be used instead
    coord.index[epi]=0; // ignore, echotrain will be used instead
  }

}


const STD_vector<RecoCoord>& RecoReaderOdin::get_coords() const {
  Log<Reco> odinlog("RecoReaderOdin","get_coords");
  if(!coord_vec_cache.size()) {

    coord_vec_cache.resize(nadc*nChannels_cache);

    STD_map<RecoCoord,UInt> repcounter_cache;  // to adjust repetition index

    RecoCoord coord;

    unsigned int index=0;
    for(unsigned int iadc=0; iadc<nadc; iadc++) {
      kSpaceCoord kcoord_allchan=recoInfo.get_kSpaceCoord(iadc);

      kcoord2coord(kcoord_allchan, coord);

      UInt& irep=repcounter_cache[coord];
      coord.index[repetition]=irep;
      irep++;

      for(unsigned int ichan=0; ichan<nChannels_cache; ichan++) {

        coord.index[channel]=ichan;
        coord_vec_cache[index]=coord;
        index++;
      }
    }
  }

  return coord_vec_cache;
}
