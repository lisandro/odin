/***************************************************************************
                          step.h  -  description
                             -------------------
    begin                : Sat Dec 30 2006
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef RECOSTEP_H
#define RECOSTEP_H

#include <odindata/step.h>

#include <odinreco/odinreco.h>
#include <odinreco/index.h>

/**
  * @addtogroup odinreco
  * @{
  */


class RecoData; // forward declaration
class RecoController; // forward declaration

///////////////////////////////////////////////////////////////////////////////////

/**
  * Context when querying reconstruction pipeline.
  */
struct RecoQueryContext {


/**
  * Mode for querying pipeline.
  * - prep:     Will be set when querying once before starting the pipeline
  * - finalize: Will be set when querying once after the pipeline was used
  * - print: print pipeline layout
  */
  enum queryMode {prep=0, finalize, print};
  queryMode mode;

/**
  * The reconstruction controller.
  */
  RecoController& controller;

/**
  * Prefix when printing functors.
  */
  STD_string printprefix;

/**
  * Postfix when printing functors.
  */
  STD_string printpostfix;

/**
  * State/index in each dimension during current step of the pipeline.
  */
  RecoCoord coord;

  RecoQueryContext(queryMode m, RecoController& contr, unsigned short numof[n_recoDims]) : mode(m), controller(contr) {
    for(int idim=0; idim<n_recoDims; idim++) coord.index[idim].set_numof(numof[idim]); // initializing numof in this branch
  }

};

///////////////////////////////////////////////////////////////////////////////////


/**
  * The base class of all basic reconstruction steps (functors) within a reconstruction pipeline.
  */
class RecoStep : public Step<RecoStep> {

 public:

  virtual ~RecoStep() {}

/**
  * Overload this function to perform the reconstruction step.
  * Input data should be taken from 'rd' and the result stored back into 'rd'
  * before executing the next step with 'execute_next_step()'.
  * Read-only access to the current reconstruction controller is possible
  * via 'controller'.
  * Return 'true' on sucess, and 'false' if functor fails.
  */
  virtual bool process(RecoData& rd, RecoController& controller) = 0;


/**
  * Overload this function to specify the input dimensions state/index of the functor.
  */
  virtual RecoCoord input_coord() const = 0;

/**
  * Overload this function to specify the modification of the input dimensions state/index after applying the functor.
  */
  virtual void modify_coord(RecoCoord& coord) const = 0;


/**
  * Overload this function to initialize the step for usage in the pieline.
  * Read-only access to the current reconstruction controller is possible via 'controller'.
  * The input interface of the step is passed in 'input_coord'.
  */
  virtual bool pipeline_init(const RecoController& controller, const RecoCoord& input_coord) {return true;}


/**
  * Queries the reconstruction pipeline.
  * Overload this function for particular actions depending on 'context'.
  * Returns 'true' only if sucessful.
  */
  virtual bool query(RecoQueryContext& context);


/**
  * Sets the functor to be executed after this.
  */
  void set_next_step(RecoStep* step) {next_step=step;}



  // To be used by factory via duck typing
  static void create_templates(STD_list<RecoStep*>& result);
  static STD_string manual_group() {return "odinreco_steps";}
  static void interface_description(const RecoStep* step, STD_string& in, STD_string& out);



 protected:
  friend class RecoController;

/**
  * Default constructor
  */
  RecoStep() : next_step(0) {}



/**
  * Execute next functor in reconstruction pipeline,
  * returns 'true' on sucess, and 'false' if functor fails.
  * Note: Do not lock any mutexes when calling this function.
  */
  bool execute_next_step(RecoData& rd, RecoController& controller);


 private:

  void interface_dims(int& in, int& out) const;


  RecoStep* next_step;

};


///////////////////////////////////////////////////////////////////////////////////



typedef StepFactory<RecoStep> RecoStepFactory; // The factory for reco steps



/** @}
  */
#endif

