/***************************************************************************
                          epinavcorr.h  -  description
                             -------------------
    begin                : Fr Sep 19 2008
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef RECOEPINAVCORR_H
#define RECOEPINAVCORR_H

#include "step.h"

class RecoEpiNavScan : public RecoStep {

 public:

  static void modify4blackboard(RecoCoord& coord) { // index for blackboard
//    coord.set_uniform_mode(RecoIndex::ignore);
//    coord.set_mode(RecoIndex::separate, echo, channel, epi);  // the three coordinates relevant to navigators, 'epi' will be used for the echo-train index

    coord.set_mode(RecoIndex::ignore, line, freq); // irrelevant to EPI navigator correction
    coord.set_mode(RecoIndex::separate, echo);  // override collected mode
  }



 private:
  // implementing virtual functions of RecoStep
  STD_string label() const {return "epinavscan";}
  STD_string description() const {return "Calculate and post data for Siemens EPI phase correction";}
  bool process(RecoData& rd, RecoController& controller);
  RecoCoord input_coord() const {return RecoCoord::coord_with_mode(RecoIndex::collected,echo,readout);}
  void modify_coord(RecoCoord& coord) const {}
  bool query(RecoQueryContext& context);
  RecoStep* allocate() const {return new RecoEpiNavScan;}
  void init() {}

};


//////////////////////////////////////////////////////////////


class RecoEpiNavCorr : public RecoStep {

  // implementing virtual functions of RecoStep
  STD_string label() const {return "epinavcorr";}
  STD_string description() const {return "Apply Siemens EPI phase correction";}
  bool process(RecoData& rd, RecoController& controller);
  RecoCoord input_coord() const {return RecoCoord::coord_with_mode(RecoIndex::collected,readout);}
  void modify_coord(RecoCoord& coord) const {}
  RecoStep* allocate() const {return new RecoEpiNavCorr;}
  void init() {}

};



#endif

