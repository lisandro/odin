/***************************************************************************
                          grappa.h  -  description
                             -------------------
    begin                : Fri Feb 2 2007
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef RECOGRAPPA_H
#define RECOGRAPPA_H


#include "step.h"
#include "measindex.h"





///////////////////////////////////////////////////////////////////////////////////////////////////////////


 // NOTE: Dimensions of the weights are: dstchannel * reduction index * srcchannel * phase * read

template<recoDim interpolDim, recoDim orthoDim, int interpolIndex, int orthoIndex, class Ignore=RecoDim<0> >
class RecoGrappaWeights : public RecoStep {

  // implementing virtual functions of RecoStep
  STD_string label() const {
    STD_string result="grappaweights";
    if(Ignore::dim()>0) result+="templ";
    if(interpolDim==line3d) result+="3d";
    return result;
  }
  STD_string description() const {return "Calculate GRAPPA weights in dimension '"+STD_string(recoDimLabel[interpolDim])+"' and post them on the blackboard with the label 'grappaweights_"+recoDimLabel[interpolDim]+"'.";}
  bool process(RecoData& rd, RecoController& controller);
  RecoCoord input_coord() const {return RecoCoord::coord_with_mode(RecoIndex::collected,channel,line3d,line,readout);}
  void modify_coord(RecoCoord& coord) const {}
  bool query(RecoQueryContext& context);
  RecoStep* allocate() const {return new RecoGrappaWeights;}
  void init();

  LDRint   reduction_factor;
  LDRint   neighbours_read;
  LDRint   neighbours_phase;
  LDRfloat svd_trunc;
  LDRfloat discard_level;

  // helper functions
  ivector acl_lines(const RecoCoord& coord, int numof_neighb, int ired) const;
  bool calc_weights(ComplexData<5>& weights, const RecoCoord& trainingcoord, const ComplexData<4>& trainingdata);


  // Instance of RecoMeasIndex to get k-space sampling pattern,
  // i.e. hase-encoding lines actually measured,
  // by assuming the same for channel, freq, repetition and separate
  // schemes for the orthogonal phase-encoding direction
  RecoMeasIndex<interpolDim, RecoDim<3, channel, freq, repetition>, RecoDim<1,orthoDim> > measlines;

};


///////////////////////////////////////////////////////////////////////////////////////////////////////////


template<recoDim interpolDim, recoDim orthoDim, int interpolIndex, int orthoIndex>
class RecoGrappa : public RecoStep {

  // implementing virtual functions of RecoStep
  STD_string label() const {if(interpolDim==line3d) return "grappa3d"; else return "grappa";}
  STD_string description() const {return "Perform GRAPPA interpolation in dimension '"+STD_string(recoDimLabel[interpolDim])+"'";}
  bool process(RecoData& rd, RecoController& controller);
  RecoCoord input_coord() const {return RecoCoord::coord_with_mode(RecoIndex::collected,channel,line3d,line,readout);}
  void modify_coord(RecoCoord& coord) const {}
  bool query(RecoQueryContext& context);
  RecoStep* allocate() const {return new RecoGrappa;}
  void init();

  LDRint   reduction_factor;
  LDRbool  keep_shape;

  // helper functions
  int reduction_index(const ivector& indexvec, int sizePhase, int iphase) const;
  bool correct_shape(RecoData& rdkspace, const RecoController& controller) const;


  // Instance of RecoMeasIndex to get k-space sampling pattern,
  // i.e. hase-encoding lines actually measured,
  // by assuming the same for channel, freq, repetition and separate
  // schemes for the orthogonal phase-encoding direction
  RecoMeasIndex<interpolDim, RecoDim<3, channel, freq, repetition>, RecoDim<1,orthoDim> > measlines;

};




#endif

