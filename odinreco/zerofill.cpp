#include "zerofill.h"
#include "data.h"
#include "controller.h"


void RecoZeroFill::init() {

  minread=0;
  minread.set_cmdline_option("zr").set_description("Minimum size of final image in read direction obtained zero filling");
  append_arg(minread,"minread");

  minphase=0;
  minphase.set_cmdline_option("zp").set_description("Minimum size of final image in phase direction obtained zero filling");
  append_arg(minphase,"minphase");

  minslice=0;
  minslice.set_cmdline_option("zs").set_description("Minimum size of final image in slice direction obtained zero filling");
  append_arg(minslice,"minslice");

}



bool RecoZeroFill::process(RecoData& rd, RecoController& controller) {
  Log<Reco> odinlog(c_label(),"process");

  ComplexData<3>& data=rd.data(Rank<3>());
  TinyVector<int,3> oldshape=data.shape();

  TinyVector<int,3> newshape=controller.image_size();
  ODINLOG(odinlog,normalDebug) << "controller.image_size()=" << newshape << STD_endl;

  newshape(0)=STD_max(newshape(0),int(minslice));
  newshape(1)=STD_max(newshape(1),int(minphase));
  newshape(2)=STD_max(newshape(2),int(float(minread)*rd.coord().overSamplingFactor+0.5)); // take oversampling into account


  if(oldshape!=newshape) {

    for(int idim=0; idim<3; idim++) {
      newshape(idim)=STD_max(newshape(idim),oldshape(idim)); // only size increase
    }
    ODINLOG(odinlog,normalDebug) << "newshape/oldshape=" << newshape << "/" << oldshape << STD_endl;

    TinyVector<int,3> lowerBounds, upperBounds;
    for(int idim=0; idim<3; idim++) {
      int diff=newshape(idim)-oldshape(idim);
      if(diff>0) {
        lowerBounds(idim)=diff/2; // offset to center old data
        upperBounds(idim)=lowerBounds(idim)+oldshape(idim)-1;
      } else {
        lowerBounds(idim)=0;
        upperBounds(idim)=oldshape(idim)-1;
      }
    }
    ODINLOG(odinlog,normalDebug) << "lowerBounds/upperBounds=" << lowerBounds << "/" << upperBounds << STD_endl;

    ComplexData<3> datacopy(data);
    datacopy.makeUnique();

    data.resize(newshape);
    data=STD_complex(0.0);
    data(RectDomain<3>(lowerBounds, upperBounds))=datacopy;

    // adjust numof
    rd.coord().index[line3d].set_numof(newshape(0));
    rd.coord().index[line].set_numof(newshape(1));
    rd.coord().index[readout].set_numof(newshape(2));
  }

  return execute_next_step(rd,controller);
}
