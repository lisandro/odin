/***************************************************************************
                          reader_odin.h  -  description
                             -------------------
    begin                : Thu Oct 18 2007
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef RECOREADERODIN_H
#define RECOREADERODIN_H

#include "reader.h"


/**
  * @addtogroup odinreco
  * @{
  */



/**
  * Raw-data reader for data acquired with odin sequences
  */
class RecoReaderOdin : public virtual RecoReaderInterface {

 public:
  RecoReaderOdin(LDRblock& parblock);
  ~RecoReaderOdin();

 private:

  // Implementing virtual functions of RecoReaderInterface
  bool init(const STD_string& input_filename);
  bool fetch(RecoCoord& coord, ComplexData<1>& adc);
  const STD_vector<RecoCoord>& get_coords() const;
  dvector dim_values(recoDim dim) const {return recoInfo.get_DimValues(dim);}
  const TinyVector<float,3>& reloffset() const {return reloffset_cache;}
  STD_string image_proc() const {return image_proc_cache;}
  const TinyVector<int,3>& image_size() const {return size_cache;}
  const Protocol& protocol() const {return prot_cache;}
  STD_string seqrecipe() const {return recoInfo.get_Recipe();}
  STD_string postProc3D() const {return recoInfo.get_PostProc3D();}
  STD_string preProc3D() const {return recoInfo.get_PreProc3D();}
  STD_string cmdline_opts() const {return recoInfo.get_CmdLineOpts();}


  void kcoord2coord(const kSpaceCoord& kcoord, RecoCoord& coord) const;

  static bool concat_data(STD_string& rawfile, const STD_string& scandir, const STD_string& fnameprefix);

  template<typename T2> bool read_block(T2 dummy, ComplexData<2>& block);

  LDRstring fifoFile;
  LDRbool   padZero;

  // read only in fetch()
  unsigned int nadc;
  int zeroesPerChunk;
  unsigned int rawtypesize;

  // cached values
  unsigned int nChannels_cache;
  TinyVector<float,3> reloffset_cache;
  STD_string image_proc_cache;
  TinyVector<int,3> size_cache;
  Protocol prot_cache;
  ReadoutShape readoutShape_cache[MAX_NUMOF_READOUT_SHAPES];
  KspaceTraj kspaceTraj_cache[MAX_NUMOF_KSPACE_TRAJS];
  ComplexData<1> weightVec_cache[MAX_NUMOF_ADC_WEIGHTING_VECTORS];
  Data<float,1> channelFactors;


  // data used during reco
  RecoPars recoInfo;
  STD_string dataformat;
  ComplexData<2> rawdata; // cache rawdata because all channels are loaded at once
  STD_string rawdata_filename;
  FILE* rawdata_fileptr;
//  LONGEST_INT rawdata_bytecount; // for debugging
  unsigned int index; // counter for indexing ADCs (each channel increments this counter)
  unsigned int ri_index; // index for ADCs in recoInfo
  kSpaceCoord kcoord_cache; // cache num of channels and crop size of current ADC
  unsigned int chancount; // counter for channels

  mutable STD_vector<RecoCoord> coord_vec_cache;

};



/** @}
  */


#endif

