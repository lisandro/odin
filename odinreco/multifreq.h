/***************************************************************************
                          multifreq.h  -  description
                             -------------------
    begin                : Wed Jun 6 2007
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef RECOMULTIFREQ_H
#define RECOMULTIFREQ_H

#include "fieldmap.h"


class RecoMultiFreq : public RecoStep {

  // implementing virtual functions of RecoStep
  STD_string label() const {return "multifreq";}
  STD_string description() const {return "Generate multi-frequency ADCs based on fieldmap";}
  bool process(RecoData& rd, RecoController& controller);
  RecoCoord input_coord() const {return RecoCoord::coord_with_mode(RecoIndex::collected,readout);}
  void modify_coord(RecoCoord& coord) const {}
  bool query(RecoQueryContext& context);
  RecoStep* allocate() const {return new RecoMultiFreq;}
  void init();

  LDRint max_numof_freq;


  bool get_freqs(Data<float,1>& result, const RecoCoord& fmapcoord, double adcdur, RecoController& controller);


  // cache frequency values
  typedef STD_map<RecoCoord, Data<float,1> > FreqMap;
  FreqMap freqmap;
  Mutex freqmutex;

  darray echoTimeStamps; // epis x echoes
  Mutex stampmutex;
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////


class RecoFreqComb : public RecoFieldMapUser {

  // implementing virtual functions of RecoStep
  STD_string label() const {return "freqcomb";}
  STD_string description() const {return "Combine multi-frequency images using fieldmap";}
  bool process(RecoData& rd, RecoController& controller);
  RecoCoord input_coord() const {return RecoCoord::coord_with_mode(RecoIndex::collected,freq,line3d,line,readout);}
  void modify_coord(RecoCoord& coord) const {coord.set_mode(RecoIndex::single,freq);}
  RecoStep* allocate() const {return new RecoFreqComb;}
  void init() {}


  bool get_freqs(Data<float,1>& result, const RecoCoord& fmapcoord, RecoController& controller);

};


#endif

