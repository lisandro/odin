/***************************************************************************
                          reader.h  -  description
                             -------------------
    begin                : Mon Jan 8 2007
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef RECOREADER_H
#define RECOREADER_H

#include <odinreco/odinreco.h>
#include <odinreco/index.h>



/**
  * @addtogroup odinreco
  * @{
  */


/**
  * Interface class for all raw-data readers.
  */
class RecoReaderInterface {

 public:

  virtual ~RecoReaderInterface() {}

/**
  * Initializes reader.
  * 'input_filename' contains meta and/or references raw data.
  */
  virtual bool init(const STD_string& input_filename) = 0;

/**
  * Returns the next available ADC (each channel has its own ADC) in 'adc' with k-space coordinates 'coord',
  * returns 'false' only if none left.
  */
  virtual bool fetch(RecoCoord& coord, ComplexData<1>& adc) = 0;

/**
  * Gets the k-space coordinates of all ADCs (each channel has its own ADC) in a single vector.
  */
  virtual const STD_vector<RecoCoord>& get_coords() const = 0;

/**
  * Returns vector of values associated with a certain dimension.
  */
  virtual dvector dim_values(recoDim dim) const = 0;

/**
  * Returns the relative spatial offset the image should be shifted after reconstruction.
  * The result contains the offsets in (phase3d,phase,read) direction.
  */
  virtual const TinyVector<float,3>& reloffset() const = 0;

/**
  * Extra command-line arguments for processing of final images.
  */
  virtual STD_string image_proc() const = 0;

/**
  * Returns the image size, will be used if it cannot be determined from the k-space
  * coordinates (i.e. when gridding).
  */
  virtual const TinyVector<int,3>& image_size() const = 0;

/**
  * Returns the scan protocol.
  */
  virtual const Protocol& protocol() const = 0;

/**
  * Returns the reco recipe (pipeline layout) as specified by the sequence.
  * If result is left blank, the recipe will be generated automatically.
  */
  virtual STD_string seqrecipe() const = 0;

/**
  * Returns the extra chain of reconstruction steps (functors) which will be
  * performed after 3D reconstruction (including channel combination) but before
  * data is stored on disk. Leave blank if no extra steps are necessary.
  */
  virtual STD_string postProc3D() const = 0;

/**
  * Returns the extra chain of reconstruction steps (functors) described by 'recipe'
  * wil be performed after 3D FFT but before channel combination.
  * Leave blank if no extra steps are necessary.
  */
  virtual STD_string preProc3D() const = 0;

/**
  * Returns extra command-line options for the reco.
  */
  virtual STD_string cmdline_opts() const = 0;

};



/** @}
  */


#endif

