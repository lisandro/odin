/***************************************************************************
                          grid.h  -  description
                             -------------------
    begin                : Wed Feb 21 2007
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef RECOGRID_H
#define RECOGRID_H

#include "step.h"



template<int NDim>
class RecoGrid : public RecoStep {

  // implementing virtual functions of RecoStep
  STD_string label() const {return "grid"+postfix();}
  STD_string description() const {return "Grid signal data with arbitrary k-space trajectory in "+itos(NDim)+" dimensions";}
  bool process(RecoData& rd, RecoController& controller);
  RecoCoord input_coord() const {return RecoCoord::coord_with_mode(RecoIndex::collected, readout);}
  void modify_coord(RecoCoord& coord) const {if(NDim==2) coord.set_mode(RecoIndex::collected, line);}
  bool query(RecoQueryContext& context);
  RecoStep* allocate() const {return new RecoGrid<NDim>;}
  void init();

  LDRbool   deapo_shift;
  LDRfilter kernel;
  LDRfloat  kernelwidth;
  LDRfloat  gridScale;

  static TinyVector<int,NDim> gridshape(const RecoCoord& coord, RecoController& controller);

  const Gridding<STD_complex,NDim>& get_gridder(const RecoCoord& coord, RecoController& controller);

  // cache gridders
  typedef STD_map<const KspaceTraj*, Gridding<STD_complex,NDim> > GridderMap;
  GridderMap gridmap;
  Mutex gridmutex;

 public: // to accessed by RecoDeapodize
  static STD_string postfix() {return itos(NDim)+"d";}
};


/////////////////////////////////////////////////////////////////////////////////


template<int NDim>
class RecoDeapodize : public RecoStep {

  // implementing virtual functions of RecoStep
  STD_string label() const {return "deapodize"+postfix();}
  STD_string description() const {return "De-apodize gridded data in "+itos(NDim)+" dimensions";}
  bool process(RecoData& rd, RecoController& controller);
  RecoCoord input_coord() const {return RecoCoord::coord_with_mode(RecoIndex::collected, line3d, line, readout);}
  void modify_coord(RecoCoord& coord) const {}
  RecoStep* allocate() const {return new RecoDeapodize;}
  void init() {}

  static STD_string postfix() {return RecoGrid<NDim>::postfix();}

};


/////////////////////////////////////////////////////////////////////////////////


class RecoGridCut : public RecoStep {

  // implementing virtual functions of RecoStep
  STD_string label() const {return "gridcut"; }
  STD_string description() const {return "Cut out central part of image to remove effects from gridding on a denser grid";}
  bool process(RecoData& rd, RecoController& controller);
  RecoCoord input_coord() const {return RecoCoord::coord_with_mode(RecoIndex::collected, line3d, line, readout);}
  void modify_coord(RecoCoord& coord) const {}
  RecoStep* allocate() const {return new RecoGridCut;}
  void init() {}
};


#endif

