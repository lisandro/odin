/***************************************************************************
                          splitter.h  -  description
                             -------------------
    begin                : Fri Feb 2 2007
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef RECOSPLITTER_H
#define RECOSPLITTER_H


#include "step.h"


/*
  * Template class to split data in one or more dimensions and to feed it separately
  * into the remaining part of the pipeline.
  * The rank of the unmodified collected data dimensions is is given by 'Nunmod'.
  * The particular unmodified dimension are given in 'Unmod' by a specialization of 'RecoDim'.
  * The numer of dimensions to split is given in 'Nsplit' and specified in 'Split' by
  * a specialization of 'RecoDim'.
  * The dimensions to split are expected leftmost in the input data array.
  */
template<unsigned int Nunmod, class Unmod, unsigned int Nsplit, class Split>
class RecoSplitter : public RecoStep, public Labeled {

 public:
  RecoSplitter(const STD_string& split_label) : Labeled(split_label) {}

 private:

  // implementing virtual functions of RecoStep
  STD_string label() const {return get_label();}
  STD_string description() const {return "Split data in one or more dimensions";}
  bool process(RecoData& rd, RecoController& controller);
  RecoCoord input_coord() const {RecoCoord result=Unmod::preset_coord(RecoIndex::collected); Split::modify(RecoIndex::collected,result); return result;}
  void modify_coord(RecoCoord& coord) const {Split::modify(RecoIndex::separate,coord);}
  RecoStep* allocate() const {return new RecoSplitter(get_label());}
  void init() {}

};



#endif

