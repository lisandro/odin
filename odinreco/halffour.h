/***************************************************************************
                          halffour.h  -  description
                             -------------------
    begin                : Tue Oct 20 2015
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef RECOHALFFOUR_H
#define RECOHALFFOUR_H


#include "step.h"
#include "measindex.h"


class RecoHalfFour : public RecoStep {

  // implementing virtual functions of RecoStep
  STD_string label() const {return "halffour";}
  STD_string description() const {return "Mirror k-space of half-Fourier acquisition";}
  bool process(RecoData& rd, RecoController& controller);
  RecoCoord input_coord() const {return RecoCoord::coord_with_mode(RecoIndex::collected,line3d,line,readout);}
  void modify_coord(RecoCoord& coord) const {}
  RecoStep* allocate() const {return new RecoHalfFour;}
  void init() {}

};




#endif

