#include "driftcorr.h"
#include "data.h"
#include "controller.h"


void modify4repdrift(RecoCoord& coord) {
  for(int idim=0; idim<n_recoDims; idim++) coord.index[idim].set_mode(RecoIndex::ignore);  // assume the same for all dimensions ...
  coord.set_mode(RecoIndex::separate, repetition); // ... except for repetition
}


void RecoDriftCalc::init() {

  driftcalcFile="";
  driftcalcFile.set_cmdline_option("dcw").set_description("Store field drift to file with this prefix");
  append_arg(driftcalcFile, "driftcalcFile");

  driftcalcTE=0.0;
  driftcalcTE.set_cmdline_option("dce").set_unit(ODIN_TIME_UNIT).set_description("TE used for field drift calculations, if non-zero");
  append_arg(driftcalcTE, "driftcalcTE");

}


bool RecoDriftCalc::process(RecoData& rd, RecoController& controller) {
  Log<Reco> odinlog(c_label(),"process");

  ComplexData<8>& indata=rd.data(Rank<8>());
  int irep=rd.coord().index[repetition];
  ODINLOG(odinlog,normalDebug) << "irep=" << irep << STD_endl;

//  Data<float,8>(cabs(indata)).autowrite("driftdata_cabs_"+itos(irep)+".nii");
//  Data<float,8>(phase(indata)).autowrite("driftdata_phase_"+itos(irep)+".nii");
  
  STD_complex cplxsum=sum(indata);
  float cplxphase=phase(cplxsum);
//  ODINLOG(odinlog,infoLog) << "cabs(cplxsum)=" << cabs(cplxsum) << STD_endl;


  RecoCoord driftcoord=rd.coord();
  modify4repdrift(driftcoord);

  STD_complex postval;

  if(irep>0) {

    // get data of first repetition
    RecoCoord repcoord(driftcoord);
    repcoord.index[repetition]=0;
    RecoData rdfirstrep(repcoord);
    ODINLOG(odinlog,normalDebug) << "Inquiring frequency drift of irep=0" << STD_endl;
    if(!controller.inquire_data(posted_repdrift_str, rdfirstrep)) return false;
    ODINLOG(odinlog,normalDebug) << "done" << STD_endl;
    if(rdfirstrep.data(Rank<1>()).size()!=1) {
      ODINLOG(odinlog,errorLog) << "Wrong size of 1st " << posted_repdrift_str << STD_endl;
      return false;
    }
    float firstrepphase=creal(rdfirstrep.data(Rank<1>())(0));

    // get data of previous repetition
    repcoord.index[repetition]=irep-1;
    RecoData rdprevrep(repcoord);
    ODINLOG(odinlog,normalDebug) << "Inquiring frequency drift of irep-1=" << irep-1 << STD_endl;
    if(!controller.inquire_data(posted_repdrift_str, rdprevrep)) return false;
    ODINLOG(odinlog,normalDebug) << "done" << STD_endl;
    if(rdprevrep.data(Rank<1>()).size()!=1) {
      ODINLOG(odinlog,errorLog) << "Wrong size of previous " << posted_repdrift_str << STD_endl;
      return false;
    }
    float prevrepphase=creal(rdprevrep.data(Rank<1>())(0));

    ODINLOG(odinlog,normalDebug) << "firstrepphase/prevrepphase[" << irep << "]=" << firstrepphase << "/" << prevrepphase << STD_endl;

    // unwrap phase
    float prevabs=fabs(prevrepphase);
    int prevmod=0;
    if(prevabs>PII) {
      prevmod=int((prevabs-PII)/(2.0*PII))+1;
      if(prevrepphase<0.0) prevmod=-prevmod;
    }
    cplxphase+=prevmod*2.0*PII;
    ODINLOG(odinlog,normalDebug) << "prevmod[" << irep << "]=" << prevmod << STD_endl;

    float diff=cplxphase-prevrepphase;
    if(diff>PII) cplxphase-=2.0*PII;
    if(diff<-PII) cplxphase+=2.0*PII;

    float cplxphase_zerointercept=cplxphase-firstrepphase;

    float tedrift;
    if(driftcalcTE>0.0) tedrift=driftcalcTE;
    else tedrift=controller.protocol().seqpars.get_EchoTime();
    ODINLOG(odinlog,normalDebug) << "tedrift=" << tedrift << STD_endl;
  
    float driftfreq=secureDivision(cplxphase_zerointercept,tedrift);

    postval=STD_complex(cplxphase, driftfreq); // store current phase and field drift


  } else {
    postval=STD_complex(cplxphase, 0); // store initial phase
  }

  ODINLOG(odinlog,normalDebug) << "postval[" << irep << "]=" << postval << STD_endl;

  // post phase to blackboard for later use
  RecoData rdposted(driftcoord);
  rdposted.data(Rank<1>()).resize(1);
  rdposted.data(Rank<1>())(0)=postval;
  ODINLOG(odinlog,normalDebug) << "Posting frequency drift of irep=" << irep << STD_endl;
  controller.post_data(posted_repdrift_str, rdposted);

  return execute_next_step(rd,controller);
}


bool RecoDriftCalc::query(RecoQueryContext& context) {
  Log<Reco> odinlog(c_label(),"query");

  if(context.mode==RecoQueryContext::prep) {
    context.controller.announce_data(posted_repdrift_str);
  }

  if(context.mode==RecoQueryContext::finalize) {
    int nrep=context.coord.index[repetition].get_numof();
    ODINLOG(odinlog,normalDebug) << "nrep=" << nrep << STD_endl;

    if(nrep>1 && driftcalcFile!="") {

      Data<float,1> driftarr(nrep);

      // get data of all repetitions
      RecoCoord repcoord;
      for(int irep=0; irep<nrep; irep++) {
        repcoord.index[repetition]=irep;
        RecoData rdrep(repcoord);
        if(!context.controller.inquire_data(posted_repdrift_str, rdrep)) return false;
        if(rdrep.data(Rank<1>()).size()!=1) {
          ODINLOG(odinlog,errorLog) << "Wrong size of " << posted_repdrift_str << STD_endl;
          return false;
        }
        driftarr(irep)=cimag(rdrep.data(Rank<1>())(0));
      }

      driftarr.write_asc_file(driftcalcFile);
    }
  }


  return RecoStep::query(context);
}

/////////////////////////////////////////////////////////////////////////



bool RecoDriftCorr::process(RecoData& rd, RecoController& controller) {
  Log<Reco> odinlog(c_label(),"process");

  int irep=rd.coord().index[repetition];

  if(irep>1) {

    double freq=0.0;

    RecoCoord driftcoord=rd.coord();
    modify4repdrift(driftcoord);

    // get data of current repetition
    RecoCoord repcoord(driftcoord);
    repcoord.index[repetition]=irep-1; // use freq from previous repetition to avoid deadlocks
    RecoData rddrift(repcoord);
    ODINLOG(odinlog,normalDebug) << "Inquiring frequency drift of irep-1=" << irep-1 << STD_endl;
    if(!controller.inquire_data(posted_repdrift_str, rddrift)) return false;
    ODINLOG(odinlog,normalDebug) << "done" << STD_endl;
    if(rddrift.data(Rank<1>()).size()!=1) {
      ODINLOG(odinlog,errorLog) << "Wrong size of current " << posted_repdrift_str << STD_endl;
      return false;
    }
    freq=cimag(rddrift.data(Rank<1>())(0));


    if(freq) {

      ComplexData<1>& data=rd.data(Rank<1>());
      unsigned int adcSize=data.extent(0);
      double dt=rd.coord().dwellTime;
      ODINLOG(odinlog,normalDebug) << "freq/adcSize/dt=" << freq << "/" << adcSize << "/" << dt << STD_endl;

      double reftime=0.0;
      dvector echostart=controller.dim_values(echo);
      unsigned int iecho=rd.coord().echopos;
      if(echostart.size()) reftime=echostart[iecho];
      ODINLOG(odinlog,normalDebug) << "echostart/reftime=" << echostart << "/" << reftime << STD_endl;

      for(unsigned int isample=0; isample<adcSize; isample++) {
        double time=reftime+double(isample)*dt;
        double imag=-time*freq;
        STD_complex phasefactor=expc(float2imag(imag));
        data(isample)*=phasefactor;
      }

    }

  }

  return execute_next_step(rd,controller);
}
