/***************************************************************************
                          phasecorr.h  -  description
                             -------------------
    begin                : Mon Jan 24 2007
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef RECOPHASECORR_H
#define RECOPHASECORR_H

#include "step.h"

class RecoPhaseMap : public RecoStep {

 public:

  static void modify(RecoCoord& coord) { // to be used in query()
    coord.set_mode(RecoIndex::ignore, line, line3d); // dimensions irrelevant to phase correction
    coord.set_mode(RecoIndex::separate, echo);  // override collected mode
  }

  static void modify4blackboard(RecoCoord& coord) { // index for blackboard
    modify(coord);
    coord.set_mode(RecoIndex::separate, epi);  // no longer ignore EPI index
    coord.set_mode(RecoIndex::ignore, repetition, dti, line3d, templtype, freq); // re-use phasemap for all repetitions, etc.
  }


 private:
  // implementing virtual functions of RecoStep
  STD_string label() const {return "phasemap";}
  STD_string description() const {return "Calculate and post phasemap for echo-by-echo phase correction";}
  bool process(RecoData& rd, RecoController& controller);
  RecoCoord input_coord() const {return RecoCoord::coord_with_mode(RecoIndex::collected,echo,readout);}
  void modify_coord(RecoCoord& coord) const {}
  bool query(RecoQueryContext& context);
  RecoStep* allocate() const {return new RecoPhaseMap;}
  void init() {}

  bool odd_even_echoes;
};


//////////////////////////////////////////////////////////////


class RecoPhaseCorr : public RecoStep {


  // implementing virtual functions of RecoStep
  STD_string label() const {return "phasecorr";}
  STD_string description() const {return "Apply echo-by-echo phase correction";}
  bool process(RecoData& rd, RecoController& controller);
  RecoCoord input_coord() const {return RecoCoord::coord_with_mode(RecoIndex::collected,readout);}
  void modify_coord(RecoCoord& coord) const {}
  RecoStep* allocate() const {return new RecoPhaseCorr;}
  void init() {}

};



#endif

