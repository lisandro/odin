/***************************************************************************
                          channel.h  -  description
                             -------------------
    begin                : Sat Dec 30 2006
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef RECOCHANNEL_H
#define RECOCHANNEL_H

#include "step.h"


class RecoChannelSum : public RecoStep {

  // implementing virtual functions of RecoStep
  STD_string label() const {return "chansum";}
  STD_string description() const {return "Combine images from different channels";}
  bool process(RecoData& rd, RecoController& controller);
  RecoCoord input_coord() const {return RecoCoord::coord_with_mode(RecoIndex::collected,channel,line3d,line,readout);}
  void modify_coord(RecoCoord& coord) const {coord.set_mode(RecoIndex::single, channel);} // reduce all channles to one
  RecoStep* allocate() const {return new RecoChannelSum;}
  void init();

  LDRbool  phasecomb;
  LDRfloat magnexp;

};


#endif

