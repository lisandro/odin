/***************************************************************************
                          fmri.h  -  description
                             -------------------
    begin                : Thu Apr 30 2009
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef RECOFMRI_H
#define RECOFMRI_H

#include "step.h"

class RecoFMRI : public RecoStep {

  // implementing virtual functions of RecoStep
  STD_string label() const {return "fmri";}
  STD_string description() const {return "Simple fMRI analysis for fMRI localizer";}
  bool process(RecoData& rd, RecoController& controller);
  RecoCoord input_coord() const {return RecoCoord::coord_with_mode(RecoIndex::collected, repetition, line3d, line, readout);}
  void modify_coord(RecoCoord& coord) const {coord.set_mode(RecoIndex::single,repetition);}
  RecoStep* allocate() const {return new RecoFMRI;}
  void init();

  LDRint stimsize;
  LDRint restsize;

};


#endif

