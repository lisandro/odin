/***************************************************************************
                          t1fit.h  -  description
                             -------------------
    begin                : Thu Mar 15 2007
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef RECOT1FIT_H
#define RECOT1FIT_H

#include "step.h"

class RecoT1Fit : public RecoStep {

  // implementing virtual functions of RecoStep
  STD_string label() const {return "t1fit";}
  STD_string description() const {return "Calculate T1 relaxation time using data from Look-Locker sequence";}
  bool process(RecoData& rd, RecoController& controller);
  RecoCoord input_coord() const {return RecoCoord::coord_with_mode(RecoIndex::collected,userdef,line3d,line,readout);}
  void modify_coord(RecoCoord& coord) const {coord.set_mode(RecoIndex::separate,userdef);}
  RecoStep* allocate() const {return new RecoT1Fit;}
  void init();

  LDRstring fittype;
  LDRfloat masklevel;
  LDRfloat maxt1;

};


#endif

