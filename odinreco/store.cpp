#include "store.h"
#include "data.h"
#include "controller.h"


STD_string RecoStore::default_format() {
  STD_string result="jdx";
#ifdef NIFTISUPPORT
  result="nii";
#elif defined DICOMSUPPORT
  result="dcm";
#endif
  return result;
}


void RecoStore::init() {
  Log<Reco> odinlog(c_label(),"init");

  extension.set_description("Extra file name extension");
  append_arg(extension,"extension");  // make this come 1st so it can be easily used when creating recipe

  prefix=STD_string(".")+SEPARATOR_STR+"image";
  prefix.set_cmdline_option("o").set_description("Prefix of output files");
  append_arg(prefix,"prefix");

  formats=default_format();
  formats.set_cmdline_option("f").set_description("Space sepapared list of output formats (file extensions), possible formats are '"+FileIO::autoformats().printbody()+"'");
  append_arg(formats,"formats");

  imageproc="none";
  imageproc.set_cmdline_option("ip").set_description("Command line args for extra processing of final images");
  append_arg(imageproc,"imageproc");

  storephase=false;
  storephase.set_cmdline_option("sp").set_description("Store phase in addition to magnitude");
  append_arg(storephase,"storephase");

  // Add useful options of FileWriteOpts
  append_arg(wopts.dialect,"wopts.dialect");
  append_arg(wopts.wprot,"wopts.wprot");
  append_arg(wopts.datatype,"wopts.datatype");

  ODINLOG(odinlog,normalDebug) << "storephase=" << bool(storephase) << STD_endl;

}


bool RecoStore::process(RecoData& rd, RecoController& controller) {
  Log<Reco> odinlog(c_label(),"process");

  Range all=Range::all();

  ComplexData<5>& indata=rd.data(Rank<5>());
  TinyVector<int,5> inshape=indata.shape();
  int nrep=inshape(0);
  int nslice=inshape(1);
  int nphase3d=inshape(2);
  int nphase=inshape(3);
  int nread=inshape(4);

  if(nslice>1 && nphase3d>1) {
    ODINLOG(odinlog,errorLog) << "Implement me: multi-slice 3D data" << STD_endl;
    return false;
  }

  Data<float,5> fdata(inshape);
  if(rd.mode==RecoData::real_data || rd.mode==RecoData::weighted_real_data) fdata=creal(indata);
  else fdata=cabs(indata);

  ODINLOG(odinlog,normalDebug) << "storephase=" << bool(storephase) << STD_endl;

  Data<float,5> pdata;
  if(storephase) {
    pdata.resize(inshape);
    pdata=phase(indata);
  }

  TinyVector<int,4> storeshape(nrep, nslice*nphase3d, nphase, nread); // collapse slices and phase3d to one dimension
  Data<float,4> storefdata(storeshape);
  Data<float,4> storepdata;
  if(storephase) storepdata.resize(storeshape);

  if(nslice>1) {
                   storefdata(all,all,all,all)=fdata(all,all,0,all,all);
    if(storephase) storepdata(all,all,all,all)=pdata(all,all,0,all,all);
  } else {
                   storefdata(all,all,all,all)=fdata(all,0,all,all,all);
    if(storephase) storepdata(all,all,all,all)=pdata(all,0,all,all,all);
  }


  // create protocol
  Protocol prot(controller.protocol());
  if(rd.override_protocol) prot=(*(rd.override_protocol));

  // make sure protocol dimension are set correctly
  prot.seqpars.set_NumOfRepetitions(inshape(0));
  prot.seqpars.set_MatrixSize(phaseDirection, inshape(3));
  prot.seqpars.set_MatrixSize(readDirection, inshape(4));

  // Store float data whenever possible
  prot.system.set_data_type(TypeTraits::type2label(float(0)));

  Protocol protphase(prot);

  // Add fileid to series description to distinguish separate protocols
  STD_string idstr=rd.coord().print(RecoIndex::filename);
  STD_string seriesdescr;
  int seriesno;
  prot.study.get_Series(seriesdescr,seriesno);
  if(idstr!="") seriesdescr=idstr+"_"+seriesdescr; // make idstr become first for better lexicographical comparison of filenames
  prot.study.set_Series(seriesdescr,seriesno);
  protphase.study.set_Series(seriesdescr+"_phase",seriesno);

  mutex.lock();
  pdmap[prot].reference(storefdata);
  if(storephase) pdmap[protphase].reference(storepdata);
  mutex.unlock();

  return execute_next_step(rd,controller);
}


bool RecoStore::store_images(const RecoController& controller) {
  Log<Reco> odinlog(c_label(),"store_images");

  svector format=tokens(formats);
  ODINLOG(odinlog,normalDebug) << "formats/format=" << formats << "/" << format.printbody() << STD_endl;

  if(!filterchain.apply(pdmap)) return false;

  // Store all data in one file/directory, if possible
  for(unsigned int i=0; i<format.size(); i++) {
    STD_string outfilename=prefix;
    if(extension!="") outfilename+="_"+extension;
    outfilename+="."+format[i];
    if(FileIO::autowrite(pdmap, outfilename, wopts)<0) {
      ODINLOG(odinlog,errorLog) << "autowrite failed" << STD_endl;
      return false;
    }
  }

  return true;
}



bool RecoStore::query(RecoQueryContext& context) {
  Log<Reco> odinlog(c_label(),"query");

  if(context.mode==RecoQueryContext::prep) {
    STD_string procstr;
    if(imageproc!="none") procstr=imageproc;
    else procstr=context.controller.image_proc();
    ODINLOG(odinlog,normalDebug) << "procstr=" << procstr << STD_endl;
    if(!filterchain.init(procstr)) return false;
  }

  ODINLOG(odinlog,normalDebug) << "storephase=" << bool(storephase) << STD_endl;
  if(context.mode==RecoQueryContext::finalize) {
    if(!store_images(context.controller)) return false;
  }
  return RecoStep::query(context);
}


////////////////////////////////////////////////////////////////////////////

bool RecoReal::process(RecoData& rd, RecoController& controller) {
  Log<Reco> odinlog(c_label(),"process");

  rd.mode=RecoData::real_data;

  return execute_next_step(rd,controller);
}

////////////////////////////////////////////////////////////////////////////

bool RecoMagn::process(RecoData& rd, RecoController& controller) {
  Log<Reco> odinlog(c_label(),"process");

  ComplexData<3>& indata=rd.data(Rank<3>());
  indata = float2real(cabs(indata));

  return execute_next_step(rd,controller);
}
