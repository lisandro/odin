#include "bladegrid.h"

#include <odindata/gridding.h>


bool BladeGrid::init(const TinyVector<int,3>& inshape, const TinyVector<float,3> fov, const TinyVector<float,3> image_size, const float os_factor, const LDRfilter& filterkernel) {
  Log<Reco> odinlog("BladeGrid","init");

  // Initialize important dimensions
  TinyVector<int,3> inshape_no(inshape(0), inshape(1), int(inshape(2)/os_factor+0.5)); // without oversampling
  ODINLOG(odinlog,normalDebug) << "inshape_no=" << inshape_no << STD_endl;

  int center_diameter=STD_min(inshape_no(1),inshape_no(2)); // Size of the fully oversampled center of k-space
  int kspace_diameter=STD_max(inshape_no(1),inshape_no(2)); // The size of the overall k-space
  ODINLOG(odinlog,normalDebug) << "center_diameter/kspace_diameter=" << center_diameter << "/" << kspace_diameter << STD_endl;

  TinyVector<int,3> centshape(inshape(0), center_diameter, int(center_diameter*os_factor+0.5));
  if(inshape(1)%2 != centshape(1)%2) centshape(1)++; // for correct placing at center
  if(inshape(2)%2 != centshape(2)%2) centshape(2)++; // for correct placing at center
  ODINLOG(odinlog,normalDebug) << "centshape=" << centshape << STD_endl;


  // prepare filtermask
  TinyVector<int,2> inshape2d(inshape(1),inshape(2));
  filtermask_center.resize(inshape2d);
  filtermask_center = 0.0;

  if (filterkernel.get_function_name() == "NoFilter") { // NoFilter means entire blade
    filtermask_center = 1.0;
  } else {
    TinyVector<float,2> bounds(inshape2d);
    bounds(0) = centshape(1);
    bounds(1) = centshape(2);

    for(int j=0; j<inshape2d(0); j++) {
      for(int i=0; i<inshape2d(1); i++) {

        float dj=(j-0.5*inshape2d(0))/(0.5*bounds(0));
        float di=(i-0.5*inshape2d(1))/(0.5*bounds(1));

        float relradius=norm(dj,di);

        filtermask_center(j,i)=filterkernel.calculate(relradius);
      }
    }
  }
  // filtermask_center.autowrite("filtermask-center.nii");

  // Prepare gridder parameters
  kmax(0) = inshape(1)*2.0*PII/fov(1);
  kmax(1) = inshape(2)*2.0*PII/fov(2) / os_factor;
  dstextent(0) = image_size(1)*2.0*PII/fov(1);
  dstextent(1) = image_size(2)*2.0*PII/fov(2);
  dstshape(0) = image_size(1);
  dstshape(1) = image_size(2); 

  return true;
}

ComplexData<4> BladeGrid::operator () (const ComplexData<4>& src, const dvector& angles) const {
  Log<Reco> odinlog("BladeGrid","()");
  Range all=Range::all();

  LDRfilter gridkernel;
  gridkernel.set_function("Gauss");

  TinyVector<int,4> inshape4d(src.shape());
  TinyVector<int,3> inshape3d(inshape4d(1),inshape4d(2),inshape4d(3));
  TinyVector<int,2> inshape2d(inshape4d(2),inshape4d(3));
  int nBlades = inshape4d(0);
  int npts3d = product(inshape3d);

  TinyVector<float,2> kspace_center = inshape2d/2;  // correct index of center: odd N -> N/2 / even N -> (N-1)/2
  float dk = max(dstextent/dstshape);
  float kwidth = 3*dk;

  ComplexData<4> images(nBlades,inshape3d(0),dstshape(0),dstshape(1));

  ODINLOG(odinlog,normalDebug) << "dstshape/dstextent/dk/kwidth=" << dstshape << "/" << dstextent << "/" << dk << "/" << kwidth << STD_endl;

  for (int iblade = 0; iblade < nBlades; iblade++) {

    // Prepare Gridder
    TinyVector<float,2> findex;
    TinyVector<float,2> kcoord;
    TinyVector<float,2> kcoord_rot;

    STD_vector<GriddingPoint<2> > src_coords(npts3d);

    int n = 0;
    for (int iphase = 0; iphase < inshape3d(1); iphase++) {
      for (int iread = 0; iread < inshape3d(2); iread++) {

        TinyVector<int,2> inplaneindex(iphase, iread);
        findex = (inplaneindex - kspace_center) / inshape2d;

        kcoord = findex*kmax;

        float si = sin(-angles[iblade]);
        float co = cos(-angles[iblade]);

        kcoord_rot(0)=co*kcoord(0)-si*kcoord(1);
        kcoord_rot(1)=si*kcoord(0)+co*kcoord(1);

        src_coords[n].coord(0) = kcoord_rot(0);
        src_coords[n].coord(1) = kcoord_rot(1);
        src_coords[n].weight = 1.0;

        n++;
      }
    }

    Gridding<STD_complex,2> gridder;
    gridder.init(dstshape, dstextent, src_coords, gridkernel, kwidth);

    for (int islice = 0; islice < inshape3d(0); islice++) { 
      ComplexData<2> image(dstshape);
      ComplexData<2> blade(inshape2d);
      blade = src(iblade,islice,all,all) * filtermask_center;
      image = gridder(blade);

      images(iblade,islice,all,all) = image;
    }
  }

  return images;
}
