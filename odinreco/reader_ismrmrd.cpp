#include "reader_ismrmrd.h"

#ifdef ISMRMRDSUPPORT


#include <ismrmrd/dataset.h>
#include <ismrmrd/xml.h>


void RecoReaderISMRMRD::print_flags(ISMRMRD::AcquisitionHeader& acqhead) {
  Log<Reco> odinlog("RecoReaderISMRMRD","print_flags");

  if(acqhead.isFlagSet(ISMRMRD::ISMRMRD_ACQ_FIRST_IN_ENCODE_STEP1))               {ODINLOG(odinlog,normalDebug) << "ISMRMRD_ACQ_FIRST_IN_ENCODE_STEP1" << STD_endl;}
  if(acqhead.isFlagSet(ISMRMRD::ISMRMRD_ACQ_LAST_IN_ENCODE_STEP1))                {ODINLOG(odinlog,normalDebug) << "ISMRMRD_ACQ_LAST_IN_ENCODE_STEP1" << STD_endl;}
  if(acqhead.isFlagSet(ISMRMRD::ISMRMRD_ACQ_FIRST_IN_ENCODE_STEP2))               {ODINLOG(odinlog,normalDebug) << "ISMRMRD_ACQ_FIRST_IN_ENCODE_STEP2" << STD_endl;}
  if(acqhead.isFlagSet(ISMRMRD::ISMRMRD_ACQ_LAST_IN_ENCODE_STEP2))                {ODINLOG(odinlog,normalDebug) << "ISMRMRD_ACQ_LAST_IN_ENCODE_STEP2" << STD_endl;}
  if(acqhead.isFlagSet(ISMRMRD::ISMRMRD_ACQ_FIRST_IN_AVERAGE))                    {ODINLOG(odinlog,normalDebug) << "ISMRMRD_ACQ_FIRST_IN_AVERAGE" << STD_endl;}
  if(acqhead.isFlagSet(ISMRMRD::ISMRMRD_ACQ_LAST_IN_AVERAGE))                     {ODINLOG(odinlog,normalDebug) << "ISMRMRD_ACQ_LAST_IN_AVERAGE" << STD_endl;}
  if(acqhead.isFlagSet(ISMRMRD::ISMRMRD_ACQ_FIRST_IN_SLICE))                      {ODINLOG(odinlog,normalDebug) << "ISMRMRD_ACQ_FIRST_IN_SLICE" << STD_endl;}
  if(acqhead.isFlagSet(ISMRMRD::ISMRMRD_ACQ_LAST_IN_SLICE))                       {ODINLOG(odinlog,normalDebug) << "ISMRMRD_ACQ_LAST_IN_SLICE" << STD_endl;}
  if(acqhead.isFlagSet(ISMRMRD::ISMRMRD_ACQ_FIRST_IN_CONTRAST))                   {ODINLOG(odinlog,normalDebug) << "ISMRMRD_ACQ_FIRST_IN_CONTRAST" << STD_endl;}
  if(acqhead.isFlagSet(ISMRMRD::ISMRMRD_ACQ_LAST_IN_CONTRAST))                    {ODINLOG(odinlog,normalDebug) << "ISMRMRD_ACQ_LAST_IN_CONTRAST" << STD_endl;}
  if(acqhead.isFlagSet(ISMRMRD::ISMRMRD_ACQ_FIRST_IN_PHASE))                      {ODINLOG(odinlog,normalDebug) << "ISMRMRD_ACQ_FIRST_IN_PHASE" << STD_endl;}
  if(acqhead.isFlagSet(ISMRMRD::ISMRMRD_ACQ_LAST_IN_PHASE))                       {ODINLOG(odinlog,normalDebug) << "ISMRMRD_ACQ_LAST_IN_PHASE" << STD_endl;}
  if(acqhead.isFlagSet(ISMRMRD::ISMRMRD_ACQ_FIRST_IN_REPETITION))                 {ODINLOG(odinlog,normalDebug) << "ISMRMRD_ACQ_FIRST_IN_REPETITION" << STD_endl;}
  if(acqhead.isFlagSet(ISMRMRD::ISMRMRD_ACQ_LAST_IN_REPETITION))                  {ODINLOG(odinlog,normalDebug) << "ISMRMRD_ACQ_LAST_IN_REPETITION" << STD_endl;}
  if(acqhead.isFlagSet(ISMRMRD::ISMRMRD_ACQ_FIRST_IN_SET))                        {ODINLOG(odinlog,normalDebug) << "ISMRMRD_ACQ_FIRST_IN_SET" << STD_endl;}
  if(acqhead.isFlagSet(ISMRMRD::ISMRMRD_ACQ_LAST_IN_SET))                         {ODINLOG(odinlog,normalDebug) << "ISMRMRD_ACQ_LAST_IN_SET" << STD_endl;}
  if(acqhead.isFlagSet(ISMRMRD::ISMRMRD_ACQ_FIRST_IN_SEGMENT))                    {ODINLOG(odinlog,normalDebug) << "ISMRMRD_ACQ_FIRST_IN_SEGMENT" << STD_endl;}
  if(acqhead.isFlagSet(ISMRMRD::ISMRMRD_ACQ_LAST_IN_SEGMENT))                     {ODINLOG(odinlog,normalDebug) << "ISMRMRD_ACQ_LAST_IN_SEGMENT" << STD_endl;}
  if(acqhead.isFlagSet(ISMRMRD::ISMRMRD_ACQ_IS_NOISE_MEASUREMENT))                {ODINLOG(odinlog,normalDebug) << "ISMRMRD_ACQ_IS_NOISE_MEASUREMENT" << STD_endl;}
  if(acqhead.isFlagSet(ISMRMRD::ISMRMRD_ACQ_IS_PARALLEL_CALIBRATION))             {ODINLOG(odinlog,normalDebug) << "ISMRMRD_ACQ_IS_PARALLEL_CALIBRATION" << STD_endl;}
  if(acqhead.isFlagSet(ISMRMRD::ISMRMRD_ACQ_IS_PARALLEL_CALIBRATION_AND_IMAGING)) {ODINLOG(odinlog,normalDebug) << "ISMRMRD_ACQ_IS_PARALLEL_CALIBRATION_AND_IMAGING" << STD_endl;}
  if(acqhead.isFlagSet(ISMRMRD::ISMRMRD_ACQ_IS_REVERSE))                          {ODINLOG(odinlog,normalDebug) << "ISMRMRD_ACQ_IS_REVERSE" << STD_endl;}
  if(acqhead.isFlagSet(ISMRMRD::ISMRMRD_ACQ_IS_NAVIGATION_DATA))                  {ODINLOG(odinlog,normalDebug) << "ISMRMRD_ACQ_IS_NAVIGATION_DATA" << STD_endl;}
  if(acqhead.isFlagSet(ISMRMRD::ISMRMRD_ACQ_IS_PHASECORR_DATA))                   {ODINLOG(odinlog,normalDebug) << "ISMRMRD_ACQ_IS_PHASECORR_DATA" << STD_endl;}
  if(acqhead.isFlagSet(ISMRMRD::ISMRMRD_ACQ_LAST_IN_MEASUREMENT))                 {ODINLOG(odinlog,normalDebug) << "ISMRMRD_ACQ_LAST_IN_MEASUREMENT" << STD_endl;}
  if(acqhead.isFlagSet(ISMRMRD::ISMRMRD_ACQ_IS_HPFEEDBACK_DATA))                  {ODINLOG(odinlog,normalDebug) << "ISMRMRD_ACQ_IS_HPFEEDBACK_DATA" << STD_endl;}
  if(acqhead.isFlagSet(ISMRMRD::ISMRMRD_ACQ_IS_DUMMYSCAN_DATA))                   {ODINLOG(odinlog,normalDebug) << "ISMRMRD_ACQ_IS_DUMMYSCAN_DATA" << STD_endl;}
  if(acqhead.isFlagSet(ISMRMRD::ISMRMRD_ACQ_IS_RTFEEDBACK_DATA))                  {ODINLOG(odinlog,normalDebug) << "ISMRMRD_ACQ_IS_RTFEEDBACK_DATA" << STD_endl;}
  if(acqhead.isFlagSet(ISMRMRD::ISMRMRD_ACQ_IS_SURFACECOILCORRECTIONSCAN_DATA))   {ODINLOG(odinlog,normalDebug) << "ISMRMRD_ACQ_IS_SURFACECOILCORRECTIONSCAN_DATA" << STD_endl;}

  if(acqhead.isFlagSet(ISMRMRD::ISMRMRD_ACQ_USER1)) {ODINLOG(odinlog,normalDebug) << "ISMRMRD_ACQ_USER1" << STD_endl;}
  if(acqhead.isFlagSet(ISMRMRD::ISMRMRD_ACQ_USER2)) {ODINLOG(odinlog,normalDebug) << "ISMRMRD_ACQ_USER2" << STD_endl;}
  if(acqhead.isFlagSet(ISMRMRD::ISMRMRD_ACQ_USER3)) {ODINLOG(odinlog,normalDebug) << "ISMRMRD_ACQ_USER3" << STD_endl;}
  if(acqhead.isFlagSet(ISMRMRD::ISMRMRD_ACQ_USER4)) {ODINLOG(odinlog,normalDebug) << "ISMRMRD_ACQ_USER4" << STD_endl;}
  if(acqhead.isFlagSet(ISMRMRD::ISMRMRD_ACQ_USER5)) {ODINLOG(odinlog,normalDebug) << "ISMRMRD_ACQ_USER5" << STD_endl;}
  if(acqhead.isFlagSet(ISMRMRD::ISMRMRD_ACQ_USER6)) {ODINLOG(odinlog,normalDebug) << "ISMRMRD_ACQ_USER6" << STD_endl;}
  if(acqhead.isFlagSet(ISMRMRD::ISMRMRD_ACQ_USER7)) {ODINLOG(odinlog,normalDebug) << "ISMRMRD_ACQ_USER7" << STD_endl;}
  if(acqhead.isFlagSet(ISMRMRD::ISMRMRD_ACQ_USER8)) {ODINLOG(odinlog,normalDebug) << "ISMRMRD_ACQ_USER8" << STD_endl;}

}

////////////////////////////////////////////////////////////////////////////


bool RecoReaderISMRMRD::ignore_acq(ISMRMRD::AcquisitionHeader& acqhead) {
  Log<Reco> odinlog("RecoReaderISMRMRD","ignore_acq");

  bool result=false;

  if(acqhead.isFlagSet(ISMRMRD::ISMRMRD_ACQ_IS_NOISE_MEASUREMENT)) result=true;
  if(acqhead.isFlagSet(ISMRMRD::ISMRMRD_ACQ_IS_DUMMYSCAN_DATA)) result=true;

  return result;
}

////////////////////////////////////////////////////////////////////////////

bool RecoReaderISMRMRD::pe_offset(const ISMRMRD::Limit& limit, unsigned int& offset, float& partfour_prot) {
  Log<Reco> odinlog("RecoReaderISMRMRD","pe_offset");
  ODINLOG(odinlog,infoLog) << "enclimits_1=" << limit.minimum << "-" << limit.center << "-" << limit.maximum << STD_endl;
  offset=0;
  float center_float=STD_max(float(0.0),float(limit.center-0.5)); // use float to place center between two sampling points
  float pre_center=center_float-float(limit.minimum);
  float post_center=float(limit.maximum)-center_float;
  if(pre_center<0 || post_center<0) {
    ODINLOG(odinlog,errorLog) << "pre_center/post_center=" << pre_center << "/" << post_center << STD_endl;
    return false;
  }
  if(post_center<pre_center) {
    ODINLOG(odinlog,errorLog) << "Implememnt me: homodyne at end of k-space, pre_center/post_center=" << pre_center << "/" << post_center << STD_endl;
    return false;
  }
  offset=post_center-pre_center;
  partfour_prot=0.0;
  if(offset) partfour_prot=1.0-secureDivision(pre_center,post_center);
  ODINLOG(odinlog,infoLog) << "offset/partfour_prot=" << offset << "/" << partfour_prot << STD_endl;
  return true;
}

////////////////////////////////////////////////////////////////////////////

RecoReaderISMRMRD::~RecoReaderISMRMRD() {
  if(dset) delete dset;
}


bool RecoReaderISMRMRD::init(const STD_string& input_filename) {
  Log<Reco> odinlog("RecoReaderISMRMRD","init");


  dset=new ISMRMRD::Dataset(input_filename.c_str(), "dataset", false);

  STD_string xmlstr;
  dset->readHeader(xmlstr);
  ISMRMRD::IsmrmrdHeader xmlhdr;
  ISMRMRD::deserialize(xmlstr.c_str(),xmlhdr);

  ODINLOG(odinlog,normalDebug) << "xmlstr: " << xmlstr << STD_endl;

  if(xmlhdr.encoding.size()!=1) {
    ODINLOG(odinlog,errorLog) << "Implememnt me: xmlhdr.encoding.size()=" << xmlhdr.encoding.size() << STD_endl;
    return false;
  }



  /////////////////////////////////////////////////////////////////////////////////////

  // Adjust protocol. We can keep the default settings, or fill in our own values.
  // Most of these settings are irrelevant for the reconstruction, but will
  // stored together with the raw data, e.g. in DICOM files

  ISMRMRD::SubjectInformation& xmlsubj=xmlhdr.subjectInformation.get();

  ISMRMRD::Encoding& enc=xmlhdr.encoding[0];
  ISMRMRD::EncodingSpace& recospace=enc.reconSpace;
  ISMRMRD::EncodingSpace& encspace=enc.encodedSpace;


  STD_string patid="Unknown";
  if(xmlsubj.patientID) patid=xmlsubj.patientID.get();
  STD_string patname="Unknown";
  if(xmlsubj.patientName) patname=xmlsubj.patientName.get();
  STD_string patbirth="00000000";
  if(xmlsubj.patientBirthdate) patbirth=xmlsubj.patientBirthdate.get();
  char patgender='O';
  if(xmlsubj.patientGender && xmlsubj.patientGender.get().size()>0) patbirth=xmlsubj.patientGender.get()[0];
  float patweight=0.0;
  if(xmlsubj.patientWeight_kg) patweight=xmlsubj.patientWeight_kg.get();


  // Study information, which is stored together with the raw data
  prot_cache.study.set_Patient(patid, patname, patbirth, patgender, patweight, 1234.0);
//  prot_cache.study.set_Context("RecoReaderISMRMRD","DrNo");
//  prot_cache.study.set_Series("Test",7);

  ODINLOG(odinlog,normalDebug) << "study: " << prot_cache.study << STD_endl;


  if(xmlhdr.sequenceParameters) {
    ISMRMRD::SequenceParameters& sequencepars=xmlhdr.sequenceParameters.get();
    if(sequencepars.TR && sequencepars.TR.get().size()>0) prot_cache.seqpars.set_RepetitionTime(sequencepars.TR.get()[0]);
    if(sequencepars.TE && sequencepars.TE.get().size()>0) prot_cache.seqpars.set_EchoTime(sequencepars.TE.get()[0]);
    if(sequencepars.flipAngle_deg && sequencepars.flipAngle_deg.get().size()>0) prot_cache.seqpars.set_FlipAngle(sequencepars.flipAngle_deg.get()[0]);
    if(sequencepars.sequence_type) prot_cache.seqpars.set_Sequence(sequencepars.sequence_type.get());
    // TODO: sequencepars.echo_spacing
  }

  // Geometry settings
//  prot_cache.geometry.set_orientation(-66.7, 78.2, -124.7);
//  prot_cache.geometry.set_offset(readDirection,22.7);
//  prot_cache.geometry.set_offset(phaseDirection,-5.9);
//  prot_cache.geometry.set_offset(sliceDirection,1.9);

  prot_cache.geometry.set_FOV(readDirection,recospace.fieldOfView_mm.x);
  prot_cache.geometry.set_FOV(phaseDirection,recospace.fieldOfView_mm.y);

  double slicedist=secureDivision(recospace.fieldOfView_mm.z, recospace.matrixSize.z);
  prot_cache.geometry.set_sliceDistance(slicedist);
  prot_cache.geometry.set_sliceThickness(slicedist); // no gap



  /////////////////////////////////////////////////////////////////////////////////////


  // adjust size that will be returned by reader, this is required for gridding (e.g. ramp-sampling correction)
  size_cache(0)=recospace.matrixSize.z; // 2nd phase encoding direction
  size_cache(1)=recospace.matrixSize.y;
  size_cache(2)=recospace.matrixSize.x;


  // This will shift the image in 3D-space according to the offsets
  // given in relative units of the FOV
  reloffset_cache=TinyVector<float,3>(0.0,0.0,0.0); // shift image in phase and read direction by 0.05*FOV and 0.1*FOV, respectively


  ISMRMRD::EncodingLimits& enclimits=enc.encodingLimits;

  float relcenter=-1.0;
  if(enclimits.kspace_encoding_step_0) {
    ODINLOG(odinlog,infoLog) << "enclimits_0=" << enclimits.kspace_encoding_step_0.get().minimum << "-" << enclimits.kspace_encoding_step_0.get().center << "-" << enclimits.kspace_encoding_step_0.get().maximum << STD_endl;
    relcenter=secureDivision(enclimits.kspace_encoding_step_0.get().center,enclimits.kspace_encoding_step_0.get().maximum);
  }

  unsigned int lineoffset=0;
  float partfour_prot;
  if(enclimits.kspace_encoding_step_1) {
    if(!pe_offset(enclimits.kspace_encoding_step_1.get(), lineoffset, partfour_prot)) return false;
    prot_cache.seqpars.set_PartialFourier(partfour_prot);
  }

  unsigned int line3doffset=0;
  if(enclimits.kspace_encoding_step_2) {
    if(!pe_offset(enclimits.kspace_encoding_step_2.get(), line3doffset, partfour_prot)) return false;
  }



  STD_list<RecoCoord> coordlist;


  unsigned int ismrmrd_nacq=dset->getNumberOfAcquisitions();
  ODINLOG(odinlog,infoLog) << "ismrmrd_nacq=" << ismrmrd_nacq << STD_endl;

  float oversampling_factor=secureDivision(encspace.matrixSize.x,recospace.matrixSize.x);
  ODINLOG(odinlog,infoLog) << "oversampling_factor=" << oversampling_factor << STD_endl;

  typedef STD_map<unsigned int, Data<float,2> > TrajMap;
  TrajMap trajmap;
  int npts_traj=0;
  int offset_traj=0;
  typedef STD_map<unsigned int, float > TrajChecksumMap;
  TrajChecksumMap checksumcache; // for integrity check


  for(unsigned int iadc=0; iadc<ismrmrd_nacq; iadc++) {
    ISMRMRD::Acquisition acq;
    dset->readAcquisition(iadc, acq);
    ISMRMRD::AcquisitionHeader acqhead=acq.getHead(); // creating copy because AcquisitionHeader::isFlagSet() is non-const
    const ISMRMRD::EncodingCounters& acqindex=acqhead.idx;

    unsigned int trajdims=acq.trajectory_dimensions();
    if(trajdims) {
      ODINLOG(odinlog,normalDebug) << "trajectory_dimensions=" << acq.trajectory_dimensions() << STD_endl;

      int trajelements=acq.getNumberOfTrajElements()/trajdims;
      ODINLOG(odinlog,normalDebug) << "trajelements[" << iadc << "]=" << trajelements << "/" << trajelements << STD_endl;

      int nsamples=acqhead.number_of_samples-acqhead.discard_pre-acqhead.discard_post;
      if(trajelements < nsamples) {
        ODINLOG(odinlog,errorLog) << "size mismatch: trajelements(" << trajelements<< ") < nsamples(" << nsamples<< ")" << STD_endl;
        return false;
      }
      
      // cache for later use
      npts_traj=nsamples;
      offset_traj=acqhead.discard_pre;

      float* trajptr=acq.getTrajPtr();
      if(trajptr) {
        Data<float,2> trajarr(Array<float,2>(trajptr, TinyVector<int, 2>(trajelements,trajdims), duplicateData)); // create copy because trajectory array changes in place
        float trajchecksum=sum(trajarr)+trajelements+trajdims; // (check)sum for integrity check
        ODINLOG(odinlog,normalDebug) << "trajptr/trajchecksum/kspace_encode_step_1=" << trajptr << "/" << trajchecksum << "/" << acqindex.kspace_encode_step_1 << STD_endl;
        trajmap[acqindex.kspace_encode_step_1].reference(trajarr);
        TrajChecksumMap::const_iterator it=checksumcache.find(acqindex.kspace_encode_step_1);
        if(it!=checksumcache.end()) {
          if(it->second!=trajchecksum) { // integrity check whether consecutive trajectories with same cycle index are the same
            ODINLOG(odinlog,errorLog) << "sum mismatch: trajchecksum(" << trajchecksum << ") != checksumcache(" << it->second << ")" << STD_endl;
            return false;
          }
        } else {
          checksumcache[acqindex.kspace_encode_step_1]=trajchecksum;
        }
      } else {
        ODINLOG(odinlog,errorLog) << "TrajPtr missing" << STD_endl;
        return false;
      }

    }


    if(!ignore_acq(acqhead)) {

      RecoCoord coord;

      unsigned int nchan=acqhead.active_channels;

      for(unsigned int ichan=0; ichan<nchan; ichan++) {
        coord.index[channel]=ichan;

        coord.index[userdef]=acqindex.user[0]; // TODO  
        coord.index[te]=acqindex.contrast;
        coord.index[average]=acqindex.average;
        coord.index[cycle]=acqindex.segment;
        coord.index[slice]=acqindex.slice;
        coord.index[line3d]=line3doffset+acqindex.kspace_encode_step_2;
        if(trajdims) {
          coord.index[cycle]=acqindex.kspace_encode_step_1;
          coord.kspaceTraj=&kspaceTraj_cache;
        } else {
          coord.index[line]=lineoffset+acqindex.kspace_encode_step_1;
        }
        coord.index[repetition]=acqindex.repetition;

        coord.overSamplingFactor=oversampling_factor;

        if(relcenter>=0.0 && relcenter<=1.0) coord.relCenterPos=relcenter;

        if(acqhead.isFlagSet(ISMRMRD::ISMRMRD_ACQ_IS_REVERSE)) coord.flags=coord.flags|recoReflectBit;

        coordlist.push_back(coord);

        ODINLOG(odinlog,normalDebug) << "coord[" << iadc << "]=" << coord.print(RecoIndex::all) << STD_endl;
      }
    }
  }



  // Convert list to vector
  coord_vec_cache.resize(coordlist.size());
  unsigned int ivec=0;
  for(STD_list<RecoCoord>::iterator it=coordlist.begin(); it!=coordlist.end(); ++it) {
    RecoCoord& coord=(*it);
//    if(has_patref_integrated && coord.index[templtype]==grappa_template) coord.index[templtype] = no_template; // Do not assume separate GRAPPA calibration scan
    coord_vec_cache[ivec]=coord;
    ivec++;
  }

  ODINLOG(odinlog,infoLog) << "#ADCs=" << coord_vec_cache.size() << STD_endl;


  // convert trajectories
  int ntrajs=trajmap.size();
  if(ntrajs) {

    TinyVector<float,3> kscale;
    kscale(2)=secureDivision(2.0*PII*encspace.matrixSize.x,encspace.fieldOfView_mm.x);
    kscale(1)=secureDivision(2.0*PII*encspace.matrixSize.y,encspace.fieldOfView_mm.y);
    kscale(0)=secureDivision(2.0*PII*encspace.matrixSize.z,encspace.fieldOfView_mm.z);
    ODINLOG(odinlog,infoLog) << "kscale=" << kscale << STD_endl;

    kspaceTraj_cache.resize(ntrajs,npts_traj);
    for(int itraj=0; itraj<ntrajs; itraj++) {
      TrajMap::const_iterator it=trajmap.find(itraj);
      if(it!=trajmap.end()) {
        const Data<float,2>& trajarr=it->second;
        int ntrajdim=trajarr.extent(1);
        for(int ipt=0; ipt<npts_traj; ipt++) {
          TinyVector<float,3> kvec; kvec=0.0;
          for(int idim=0; idim<ntrajdim; idim++) kvec(2-idim)=trajarr(offset_traj+ipt,idim); // reverse order of x,y,z
          kspaceTraj_cache(itraj,ipt).coord=kvec*kscale;
          kspaceTraj_cache(itraj,ipt).weight=1.0;
          ODINLOG(odinlog,normalDebug) << "kspaceTraj(" << itraj << "," << ipt << ")=" << kspaceTraj_cache(itraj,ipt).coord << STD_endl;
        }
      }
    }
    prot_cache.seqpars.set_PartialFourier(0.0); // oeverride settings from enclimits
  }



  /////////////////////////////////////////////////////////////////////////////////////

  // reset global counters because it is used in fetch
  ismrmrd_count=0;
  coord_count=0;
  chancount=0;

  return true;
}




bool RecoReaderISMRMRD::fetch(RecoCoord& coord, ComplexData<1>& adc) {
  Log<Reco> odinlog("RecoReaderISMRMRD","fetch");

  if(coord_count>=coord_vec_cache.size()) return false; // This check is necessary for proper multi-threading


  // Return the next raw data ADC
  coord=coord_vec_cache[coord_count];


  // Load next block of raw data
  if(!chancount) {

    ISMRMRD::AcquisitionHeader acqhead;

    do {

      ISMRMRD::Acquisition acq;
      dset->readAcquisition(ismrmrd_count, acq);

      acqhead=acq.getHead(); // creating copy because AcquisitionHeader::isFlagSet() is non-const

      nsamples_raw_cache=acqhead.number_of_samples;
      nchan_cache=acqhead.active_channels;
      ODINLOG(odinlog,normalDebug) << "nsamples_raw/nchan=" << nsamples_raw_cache << "/" << nchan_cache << STD_endl;

      pre_discard_cache=acqhead.discard_pre;
      post_discard_cache=acqhead.discard_post;
      ODINLOG(odinlog,normalDebug) << "pre_discard_cache/post_discard_cache=" << pre_discard_cache << "/" << post_discard_cache << STD_endl;

      STD_complex* cplxptr=acq.getDataPtr();
      rawdata.reference(Array<STD_complex,2>(cplxptr, TinyVector<int,2>(nchan_cache,nsamples_raw_cache), neverDeleteData).copy());

      ismrmrd_count++;

    } while(ignore_acq(acqhead)); // Continue if current ADC should be ignored

  }


  // crop raw data
  unsigned int nsamples_cropped=nsamples_raw_cache-pre_discard_cache-post_discard_cache;
  ODINLOG(odinlog,normalDebug) << "nsamples_cropped=" << nsamples_cropped << STD_endl;
  adc.resize(nsamples_cropped);
  adc=rawdata(chancount,Range(pre_discard_cache,pre_discard_cache+nsamples_cropped-1));

  coord_count++;
  chancount++;
  if(chancount>=nchan_cache) { // get next raw data if channels complete
    chancount=0;
  }

  return true;
}


dvector RecoReaderISMRMRD::dim_values(recoDim dim) const {
  // Return values associated with dimension 'dim', i.e. the TEs of a multi-echo readout, if available
  return dvector();
}


STD_string RecoReaderISMRMRD::cmdline_opts() const {
  // Return extra command-line options of the reco
  return "";
}

#endif // HDFSUPPORT
