#include "slicetime.h"
#include "data.h"
#include "controller.h"
#include <odindata/filter.h>


bool RecoSliceTime::process(RecoData& rd, RecoController& controller) {
  Log<Reco> odinlog(c_label(),"process");

  Range all=Range::all();

  STD_string sliceorderstr=controller.dim_values(slice).printbody();
  ODINLOG(odinlog,normalDebug) << "sliceorderstr=" << sliceorderstr << STD_endl;


  FilterChain filterchain("-slicetime \""+sliceorderstr+"\""); // re-use filter step


  ComplexData<5>& indata=rd.data(Rank<5>());
  TinyVector<int,5> inshape=indata.shape();

  int nrep=inshape(0);
  int nslices=inshape(1);
  int nphase3d=inshape(2);
  int nphase=inshape(3);
  int nread=inshape(4);

  if(nrep>1 && nslices>1) {

    TinyVector<int,4> shape4filter(nrep,nslices,nphase,nread);

    Data<float,4> rdata(shape4filter);
    Data<float,4> idata(shape4filter);

    Protocol protcopy(controller.protocol());

    for(int iphase3d=0; iphase3d<nphase3d; iphase3d++) {
      rdata=creal(indata(all,all,iphase3d,all,all));
      idata=cimag(indata(all,all,iphase3d,all,all));
      filterchain.apply(protcopy,rdata);
      filterchain.apply(protcopy,idata);
      indata(all,all,iphase3d,all,all)=float2real(rdata);
      indata(all,all,iphase3d,all,all)+=float2imag(idata);
    }


  }

  return execute_next_step(rd,controller);
}
