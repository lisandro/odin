/***************************************************************************
                          bladegrid.h  -  description
                             -------------------
    begin                : Fri Jan 07 2009
    copyright            : (C) 2009 by Martin Krämer
    email                : MartinKraemer@gmx.net
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef BLADEGRID_H
#define BLADEGRID_H

#include <odinpara/ldrfilter.h>
#include <odindata/complexdata.h>

#include "odinreco.h"


class BladeGrid {

  public: 
    bool init(const TinyVector<int,3>& inshape, const TinyVector<float,3> fov, const TinyVector<float,3> image_size, const float os_factor, const LDRfilter& filterkernel);

    ComplexData<4> operator () (const ComplexData<4>& src, const dvector& angles) const;

  private:
    Data<float,2> filtermask_center;
    TinyVector<float,2> dstextent;
    TinyVector<float,2> dstshape;
    TinyVector<float,2> kmax;
};

#endif
