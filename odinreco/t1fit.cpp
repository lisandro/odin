#include "t1fit.h"
#include "data.h"
#include "controller.h"

#include <odindata/fitting.h>

///////////////////////////////////////////////////////////////////////////

// function class to fit IR timecourse
struct InversionRecoveryFunction : public ModelFunction {

  fitpar M0;
  fitpar lambda;

  float evaluate_f(float x) const {return  M0.val * (1.0 - 2.0 * exp (lambda.val * x) );}
  
  fvector evaluate_df(float x) const {
    fvector result(numof_fitpars());
    result[0]=1.0 - 2.0 * exp (lambda.val * x);
    result[1]=-x * 2.0 * M0.val * exp (lambda.val * x);
    return result;
  }

  unsigned int numof_fitpars() const {return 2;}

  fitpar& get_fitpar(unsigned int i) {
    if(i==0) return M0;
    if(i==1) return lambda;
    return dummy_fitpar;
  }
};

///////////////////////////////////////////////////////////////////////////

// function class to fit magnitude IR timecourse
struct InversionRecoveryAbsFunction : public ModelFunction {

  bool fitphi;

  fitpar M0;
  fitpar phi;
  fitpar Ri;

  float evaluate_f(float x) const {
    double winkel = atan(phi.val)/PII+0.5;
    double e = exp(-Ri.val*x);
    return fabs(M0.val*(1-2*winkel*e));
  }

  fvector evaluate_df(float x) const {
    fvector result(numof_fitpars());
    double winkel = atan(phi.val)/PII+0.5;
    double e = exp(-Ri.val*x);
    double Fun = (2*winkel*e-1);
    result[0]=fabs(M0.val)*fabs(Fun)/M0.val;
    result[1]=(-2.0*fabs(M0.val)*winkel*x*e*Fun)/(fabs(Fun));
    if(fitphi) result[2]=(2.0*fabs(M0.val)*e*Fun)/(PII*(pow(phi.val,2)+1)*fabs(Fun));
    return result;
  }

  unsigned int numof_fitpars() const {return 2+fitphi;}

  fitpar& get_fitpar(unsigned int i) {
    if(i==0) return M0;
    if(i==1) return Ri;
    if(i==2) return phi;
    return dummy_fitpar;
  }
};

///////////////////////////////////////////////////////////////////////////

void RecoT1Fit::init() {

  fittype="simple";
  fittype.set_cmdline_option("t1f").set_description("Use this type of T1 fit, can be one of 'simple', 'magnitude'");
  append_arg(fittype,"fittype");

  masklevel=0.5;
  masklevel.set_cmdline_option("t1l").set_description("Level relative to mean value of first echo for setting mask of T1 fit");
  append_arg(masklevel,"masklevel");

  maxt1=3000.0;
  maxt1.set_unit(ODIN_TIME_UNIT).set_cmdline_option("t1m").set_description("Upper limit for T1 fit");
  append_arg(maxt1,"maxt1");
}

#define START_T1 1000.0

bool RecoT1Fit::process(RecoData& rd, RecoController& controller) {
  Log<Reco> odinlog(c_label(),"process");

  Range all=Range::all();

  ComplexData<4>& indata=rd.data(Rank<4>());
  Data<float,4> magn(cabs(indata));
  TinyVector<int,4> inshape=indata.shape();
  TinyVector<int,3> outshape(inshape(1), inshape(2), inshape(3));

//  magn.autowrite("magn.jdx");

  ODINLOG(odinlog,normalDebug) << "inshape=" << inshape << STD_endl;

  dvector tis=controller.dim_values(userdef);
  ODINLOG(odinlog,normalDebug) << "tis=" << tis.printbody() << STD_endl;
  int ntis=tis.size();
  if(ntis!=inshape(0) || ntis<2) {
    ODINLOG(odinlog,errorLog) << "size mismatch or too small" << STD_endl;
    return false;
  }


  float noiselevel=masklevel*mean(magn(0,all,all,all));

  float deltaEcho=tis[1]-tis[0];

  Protocol prot(controller.protocol());

  float pulse_term=log(cos(prot.seqpars.get_FlipAngle()/180.0*PII))/deltaEcho;
  ODINLOG(odinlog,normalDebug) << "pulse_term=" << pulse_term << STD_endl;

  InversionRecoveryFunction irfunc;
  InversionRecoveryAbsFunction irabsfunc;
  irabsfunc.fitphi=false;

  FunctionFitDerivative irfit;
  if(!irfit.init(irfunc,ntis)) return false;
  FunctionFitDerivative irabsfit;
  if(!irabsfit.init(irabsfunc,ntis)) return false;

  Data<float,3> T1map(outshape); T1map=0.0;
  Data<float,3> S0map(outshape); S0map=0.0;
  Data<float,3> angmap(outshape); angmap=0.0;

  Data<float,1> pixel4fit(ntis);
  Data<float,1> ysigma(ntis); ysigma=1.0;
  Data<float,1> xvals(ntis);
  for(int iti=0; iti<ntis; iti++) xvals(iti)=tis[iti];

  for(int iphase3d=0; iphase3d<inshape(1); iphase3d++) {
    for(int iphase=0; iphase<inshape(2); iphase++) {
      for(int iread=0; iread<inshape(3); iread++) {
        if(magn(0,iphase3d,iphase,iread)>noiselevel) {

          pixel4fit=magn(all,iphase3d,iphase,iread);
          int minindex=minIndex(pixel4fit)(0);

          float M0=0.0;
	  float t1star=0.0;
          float angle=0.0;

          if(fittype=="magnitude") {

            irabsfunc.M0.val=max(pixel4fit);
            irabsfunc.phi.val=tan(PII*cos(PII*170/180-PII)-PII/2);	
            irabsfunc.Ri.val=secureInv( 1.45*tis[minindex]);

            if(irabsfit.fit(pixel4fit, ysigma, xvals)) {
              M0=irabsfunc.M0.val;
              angle=acos(-atan(irabsfunc.phi.val)/PII-0.5)/PII*180.0;
              t1star=secureInv(irabsfunc.Ri.val);
            }
          } else {

            for(int i=0; i<minindex; i++) pixel4fit(i)=-pixel4fit(i); //reverse values befor zero crossing

            irfunc.M0.val=-pixel4fit(0); // M0
            irfunc.lambda.val=secureDivision(-deltaEcho ,START_T1); // lambda
            if(irfit.fit(pixel4fit)) {
              M0=irfunc.M0.val;
              t1star=secureDivision(-deltaEcho , irfunc.lambda.val);
            }
          }


          float t1=0.0;
          if(t1star>0.0) t1=secureDivision(1.0 , 1.0/t1star + pulse_term); // correction for Look Locker sequence
          check_range<float>(t1, 0.0, maxt1);
          T1map(iphase3d,iphase,iread)=t1;

          S0map(iphase3d,iphase,iread)=M0;
          angmap(iphase3d,iphase,iread)=angle;

        }
      }
    }
  }


  // modify coordinate
  int numof_output=2;
  if(sum(angmap)>0.0) numof_output++;
  rd.coord().index[userdef].set_numof(numof_output);

  rd.override_protocol=&prot;
  STD_string series;
  int number;
  prot.study.get_Series(series, number);


  // feed T1 into rest of pipeline
  RecoData rdT1(rd);
  rdT1.data(Rank<3>()).resize(outshape);
  rdT1.data(Rank<3>())=float2real(T1map);
  rdT1.coord().index[userdef]=0;
  prot.study.set_Series(series+"_T1",number);
  if(!execute_next_step(rdT1,controller)) return false;

  // feed S0 into rest of pipeline
  RecoData rdS0(rd);
  rdS0.data(Rank<3>()).resize(outshape);
  rdS0.data(Rank<3>())=float2real(S0map);
  rdS0.coord().index[userdef]=1;
  prot.study.set_Series(series+"_S0",number);
  if(!execute_next_step(rdS0,controller)) return false;

  // feed angle into rest of pipeline
  if(sum(angmap)>0.0) {
    RecoData rdang(rd);
    rdang.data(Rank<3>()).resize(outshape);
    rdang.data(Rank<3>())=float2real(angmap);
    rdang.coord().index[userdef]=2;
    prot.study.set_Series(series+"_angle",number);
    if(!execute_next_step(rdang,controller)) return false;
  }

  return true;
}
