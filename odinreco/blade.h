/***************************************************************************
                          blade.h  -  description
                             -------------------
    begin                : Thu Mar 15 2007
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef RECOBLADE_H
#define RECOBLADE_H

#include "step.h"
#include "measindex.h"

class RecoBladeGrid : public RecoStep {

  // implementing virtual functions of RecoStep
  STD_string label() const {return "gridblade";}
  STD_string description() const {return "grid kspace center of single blades";}
  bool process(RecoData& rd, RecoController& controller);
  RecoCoord input_coord() const {return RecoCoord::coord_with_mode(RecoIndex::collected,cycle,line3d,line,readout);}
  void modify_coord(RecoCoord& coord) const {}
  bool query(RecoQueryContext& context);
  RecoStep* allocate() const {return new RecoBladeGrid;}
  void init();

  LDRbool  fft;
  LDRfilter filtermask; 

  // Phase-encoding lines actually measured
  RecoMeasIndex<line3d, RecoDim<5, line, line3d, channel, repetition, freq> > measlines;
};

//////////////////////////////////////////////////////////////

class RecoCycleRot : public RecoStep {

  // implementing virtual functions of RecoStep
  STD_string label() const {return "cyclerot";}
  STD_string description() const {return "In-plane rotation according to cycle index";}
  bool process(RecoData& rd, RecoController& controller);
  RecoCoord input_coord() const {return RecoCoord::coord_with_mode(RecoIndex::collected,line3d,line,readout);}
  void modify_coord(RecoCoord& coord) const {}
  RecoStep* allocate() const {return new RecoCycleRot;}
  void init() {}
};


//////////////////////////////////////////////////////////////



class RecoBladeCorr : public RecoStep {

  // implementing virtual functions of RecoStep
  STD_string label() const {return "bladecorr";}
  STD_string description() const {return "Pre-processing of PROPELLER blades: phase correction";}
  bool process(RecoData& rd, RecoController& controller);
  RecoCoord input_coord() const {return RecoCoord::coord_with_mode(RecoIndex::collected,line3d,line,readout);}
  void modify_coord(RecoCoord& coord) const {}
  RecoStep* allocate() const {return new RecoBladeCorr;}
  void init();

  LDRfilter phasecorr_filter;
  LDRfloat  phasecorr_width;

};


///////////////////////////////////////////////////////////////////////


class RecoBladeComb : public RecoStep {

 public:
  ~RecoBladeComb();

 private:
  // implementing virtual functions of RecoStep
  STD_string label() const {return "bladecomb";}
  STD_string description() const {return "Pre-processing of PROPELLER blades: coordinate rotation for gridding";}
  bool process(RecoData& rd, RecoController& controller);
  RecoCoord input_coord() const {return RecoCoord::coord_with_mode(RecoIndex::collected,cycle,line3d,line,readout);}
  void modify_coord(RecoCoord& coord) const {coord.set_mode(RecoIndex::separate,cycle,line3d,line);}
  bool query(RecoQueryContext& context);
  RecoStep* allocate() const {return new RecoBladeComb;}
  void init();

  LDRbool  keyhole;
  LDRbool  corrweight;
  LDRfilter keyhole_filter;
  LDRfilter kspaceweight_filter;
  LDRfloat  transition;
  LDRfloat  kspaceweight_width;

  STD_map<int,KspaceTraj*> trajmap;  // caching trajectories, one for each keyhole index
  Mutex trajmutex;

  // Phase-encoding lines actually measured
  RecoMeasIndex<line3d, RecoDim<5, line, line3d, channel, repetition, freq> > measlines;

};


#endif

