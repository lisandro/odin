/***************************************************************************
                          expfit.h  -  description
                             -------------------
    begin                : Thu Mar 15 2007
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef RECOEXPFIT_H
#define RECOEXPFIT_H

#include "step.h"

class RecoExpFit : public RecoStep {

  // implementing virtual functions of RecoStep
  STD_string label() const {return "expfit";}
  STD_string description() const {return "Fit exponential decay constant using x-values associated with 'userdef' dimension.";}
  bool process(RecoData& rd, RecoController& controller);
  RecoCoord input_coord() const {return RecoCoord::coord_with_mode(RecoIndex::collected,userdef,line3d,line,readout);}
  void modify_coord(RecoCoord& coord) const {coord.set_mode(RecoIndex::single,userdef);}
  RecoStep* allocate() const {return new RecoExpFit;}
  void init();

  LDRbool  invert;
  LDRfloat masklevel;

};


#endif

