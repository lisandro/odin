/***************************************************************************
                          conjphase.h  -  description
                             -------------------
    begin                : Wed Jun 6 2007
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef RECOCONJPHASE_H
#define RECOCONJPHASE_H

#include "fieldmap.h"


class RecoConjPhaseFT : public RecoFieldMapUser {

  // implementing virtual functions of RecoStep
  STD_string label() const {return "conjphase";}
  STD_string description() const {return "Discrete Fourier transform with conjugate-phase correction according to field map";}
  bool process(RecoData& rd, RecoController& controller);
  RecoCoord input_coord() const {return RecoCoord::coord_with_mode(RecoIndex::collected, readout);}
  void modify_coord(RecoCoord& coord) const {coord.set_mode(RecoIndex::collected, line3d, line);}
  RecoStep* allocate() const {return new RecoConjPhaseFT;}
  void init() {}

  Mutex trajmutex;
};


#endif

