#include "step.h"
#include "b1fit.h"
#include "blade.h"
#include "blackboard.h"
#include "channel.h"
#include "circmask.h"
#include "collector.h"
#include "conjphase.h"
#include "controller.h"
#include "adc.h"
#include "dump.h"
#include "dti.h"
#include "driftcorr.h"
#include "epinavcorr.h"
#include "expfit.h"
#include "fieldmap.h"
#include "filter.h"
#include "fft.h"
#include "fmri.h"
#include "grappa.h"
#include "grid.h"
#include "halffour.h"
#include "homodyne.h"
#include "messer.h"
#include "mip.h"
#include "multifreq.h"
#include "offset.h"
#include "oversampling.h"
#include "phasecorr.h"
#include "phasecourse.h"
#include "pilot.h"
#include "qualitycontrol.h"
#include "refgain.h"
#include "slicetime.h"
#include "slidingwindow.h"
#include "splitter.h"
#include "store.h"
#include "sum.h"
#include "swi.h"
#include "switch.h"
#include "t1fit.h"
#include "zerofill.h"



// Instantiate template classes
#include <odindata/step_code.h>
template class Step<RecoStep>;
template class StepFactory<RecoStep>;


bool RecoStep::query(RecoQueryContext& context) {
  Log<Reco> odinlog(c_label(),"query");

  ODINLOG(odinlog,normalDebug) << "mode=" << context.mode << STD_endl;

  if(context.mode==RecoQueryContext::print) {
    int padlength=STD_max(1,16-int(label().size()));

    int in, out;
    interface_dims(in, out);
    STD_string ifacestr;
    if(in || out) {
      if(in==out) ifacestr="(dim: "+itos(in)+")";
      else ifacestr="(dim: "+itos(in)+"->"+itos(out)+")";
    }
    ODINLOG(odinlog,infoLog) << STD_string(padlength,' ') << context.printprefix << " " << context.printpostfix << ifacestr << STD_endl;
  }

  modify_coord(context.coord); // apply changes to coordinate before querying next step

  if(next_step) return next_step->query(context);
  return true;
}


bool RecoStep::execute_next_step(RecoData& rd, RecoController& controller) {
  if(next_step) {
    rd.new_profiler(next_step->label());
    modify_coord(rd.coord()); // apply changes to coordinate

    // delete old data
    int in, out;
    interface_dims(in, out);
    if(in != out) rd.free(in);

    return next_step->process(rd,controller);
  }
  return true;
}


void RecoStep::create_templates(STD_list<RecoStep*>& result) {
  result.push_back(new RecoAdcBaseline());
  result.push_back(new RecoAdcGridPrep());
  result.push_back(new RecoAdcPad());
  result.push_back(new RecoAdcReflect());
  result.push_back(new RecoAdcWeight());
  result.push_back(new RecoB1Fit());
  result.push_back(new RecoBladeComb());
  result.push_back(new RecoBladeCorr());
  result.push_back(new RecoBladeGrid());
  result.push_back(new RecoChannelSum());
  result.push_back(new RecoCircMask());
  result.push_back(new RecoCollector<1, RecoDim<1, readout>,                      2, RecoDim<2, line3d, line> >       ("kspace"));
  result.push_back(new RecoCollector<2, RecoDim<2, line, readout>,                1, RecoDim<1, line3d> >             ("line3dcoll"));
  result.push_back(new RecoCollector<3, RecoDim<3, line3d, line, readout>,        1, RecoDim<1, slice>, true >        ("slicecoll"));
  result.push_back(new RecoCollector<3, RecoDim<3, line3d, line, readout>,        1, RecoDim<1, channel>, true >      ("chancoll"));
  result.push_back(new RecoCollector<3, RecoDim<3, line3d, line, readout>,        1, RecoDim<1, cycle>, true >        ("cyclecoll"));
  result.push_back(new RecoCollector<3, RecoDim<3, line3d, line, readout>,        1, RecoDim<1, te>, true >           ("tecoll"));
//  result.push_back(new RecoCollector<3, RecoDim<3, line3d, line, readout>,        1, RecoDim<1, epi>, true >          ("epicoll"));
  result.push_back(new RecoCollector<3, RecoDim<3, line3d, line, readout>,        1, RecoDim<1, userdef>, true >      ("usercoll"));
  result.push_back(new RecoCollector<3, RecoDim<3, line3d, line, readout>,        1, RecoDim<1, repetition>, true >   ("repcoll"));
  result.push_back(new RecoCollector<3, RecoDim<3, line3d, line, readout>,        1, RecoDim<1, freq>, true >         ("freqcoll"));
  result.push_back(new RecoCollector<4, RecoDim<4, slice, line3d, line, readout>, 1, RecoDim<1, repetition>, true >   ("image"));
  result.push_back(new RecoCollector<1, RecoDim<1, readout>,                      1, RecoDim<1, average>, true >      ("averagecoll"));
  result.push_back(new RecoCollector<1, RecoDim<1, readout>,                      1, RecoDim<1, echo>, true >         ("echocoll"));
  result.push_back(new RecoCollector<3, RecoDim<3, line3d, line, readout>,        2, RecoDim<2, channel, repetition>, true > ("chanrepcoll"));
  result.push_back(new RecoCollector<3, RecoDim<3, line3d, line, readout>,        3, RecoDim<3, channel, repetition, slice>, true > ("chanrepslicecoll"));
  result.push_back(new RecoCollector<3, RecoDim<3, line3d, line, readout>,        5, RecoDim<5, cycle, slice, echo, epi, channel>, true > ("driftcoll"));
  result.push_back(new RecoConjPhaseFT());
  result.push_back(new RecoCycleRot());
  result.push_back(new RecoDeapodize<1>());
  result.push_back(new RecoDeapodize<2>());
  result.push_back(new RecoDump());
  result.push_back(new RecoDTI());
  result.push_back(new RecoDriftCalc());
  result.push_back(new RecoDriftCorr());
  result.push_back(new RecoEpiNavScan());
  result.push_back(new RecoEpiNavCorr());
  result.push_back(new RecoExpFit());
  result.push_back(new RecoFieldMap());
  result.push_back(new RecoFilter());
  result.push_back(new RecoFFT());
  result.push_back(new RecoFMRI());
  result.push_back(new RecoFreqComb());
  result.push_back(new RecoGrappaWeights<line,line3d,1,0>());
//  result.push_back(new RecoGrappaWeights<line,line3d,1,0, RecoDim<3, repetition, freq, userdef> >()); // Assume only one GRAPPA template for all userdef indices
  result.push_back(new RecoGrappaWeights<line,line3d,1,0, RecoDim<2, repetition, freq> >());
  result.push_back(new RecoGrappaWeights<line3d,line,0,1>());
//  result.push_back(new RecoGrappaWeights<line3d,line,0,1, RecoDim<3, repetition, freq, userdef> >()); // Assume only one GRAPPA template for all userdef indices
  result.push_back(new RecoGrappaWeights<line3d,line,0,1, RecoDim<2, repetition, freq> >());
  result.push_back(new RecoGrappa<line,line3d,1,0>());
  result.push_back(new RecoGrappa<line3d,line,0,1>());
  result.push_back(new RecoGrid<1>());
  result.push_back(new RecoGrid<2>());
  result.push_back(new RecoGridCut());
  result.push_back(new RecoHalfFour());
  result.push_back(new RecoHomodyne());
  result.push_back(new RecoStore());
//  result.push_back(new RecoInterpolate());
  result.push_back(new RecoMesser());
  result.push_back(new RecoMip());
  result.push_back(new RecoMultiFreq());
  result.push_back(new RecoOffset());
  result.push_back(new RecoOversampling());
  result.push_back(new RecoPhaseMap());
  result.push_back(new RecoPhaseCorr());
  result.push_back(new RecoPhaseCourse());
  result.push_back(new RecoPilot());
  result.push_back(new RecoPost());
  result.push_back(new RecoQualityControlSpike());
  result.push_back(new RecoReal());
  result.push_back(new RecoMagn());
  result.push_back(new RecoRefGain());
  result.push_back(new RecoSliceTime());
  result.push_back(new RecoSlidingWindow());
  result.push_back(new RecoSplitter<3, RecoDim<3, line3d, line, readout>, 1, RecoDim<1, channel> > ("chansplit"));
  result.push_back(new RecoSplitter<3, RecoDim<3, line3d, line, readout>, 1, RecoDim<1, repetition> > ("repsplit"));
  result.push_back(new RecoSum<1, RecoDim<1, readout>,               1, RecoDim<1, average> >    ("averagesum"));
  result.push_back(new RecoSum<3, RecoDim<3, line3d, line, readout>, 1, RecoDim<1, cycle> >      ("cyclesum"));
  result.push_back(new RecoSum<3, RecoDim<3, line3d, line, readout>, 1, RecoDim<1, repetition> > ("repsum"));
  result.push_back(new RecoSum<3, RecoDim<3, line3d, line, readout>, 1, RecoDim<1, repetition>, true > ("repmagnsum"));
  result.push_back(new RecoSwi());
  result.push_back(new RecoSwitch());
  result.push_back(new RecoT1Fit());
  result.push_back(new RecoZeroFill());
}


STD_string interface_str(const RecoCoord& coord) {
  STD_list<STD_string> colldims;
  for(int i=0; i<n_recoDims; i++) {
    if(coord.index[i].get_mode()==RecoIndex::collected) colldims.push_back(recoDimLabel[i]);
  }
  unsigned int ncoll=colldims.size();
  if(!ncoll) return "any";

  STD_string collstr;
  for(STD_list<STD_string>::const_iterator it=colldims.begin(); it!=colldims.end(); ++it) {
    if(collstr!="") collstr+=",";
    collstr+=*it;
  }
  return itos(ncoll)+"-dimensional, these dimensions are: \\verbatim ("+collstr+") \\endverbatim";
}


void RecoStep::interface_description(const RecoStep* step, STD_string& in, STD_string& out) {
  RecoCoord coord=step->input_coord();
  in=interface_str(coord);
  step->modify_coord(coord);
  out=interface_str(coord);
}


void RecoStep::interface_dims(int& in, int& out) const {
  RecoCoord coord=input_coord();

  in=0;
  for(int i=0; i<n_recoDims; i++) {
    if(coord.index[i].get_mode()==RecoIndex::collected) in++;
  }

  modify_coord(coord);

  out=0;
  for(int i=0; i<n_recoDims; i++) {
    if(coord.index[i].get_mode()==RecoIndex::collected) out++;
  }
}
