/***************************************************************************
                          b1fit.h  -  description
                             -------------------
    begin                : Tue Oct 16 2007
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef RECOB1FIT_H
#define RECOB1FIT_H

#include "step.h"

class RecoB1Fit : public RecoStep {

  // implementing virtual functions of RecoStep
  STD_string label() const {return "b1fit";}
  STD_string description() const {return "B1-mapping method based on: Fast, Accurate, and Precise Mapping of the RF Field In Vivo Using the 180 Signal Null. Dowell et al., Magn Reson Med 58 (2007).";}
  bool process(RecoData& rd, RecoController& controller);
  RecoCoord input_coord() const {return RecoCoord::coord_with_mode(RecoIndex::collected,userdef,line3d,line,readout);}
  void modify_coord(RecoCoord& coord) const {coord.set_mode(RecoIndex::single,userdef);}
  RecoStep* allocate() const {return new RecoB1Fit;}
  void init() {}

};


#endif

