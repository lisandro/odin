#include "expfit.h"
#include "data.h"
#include "controller.h"

#include <odindata/fitting.h>

///////////////////////////////////////////////////////////////////////////

void RecoExpFit::init() {

  invert=false;
  invert.set_description("Calculate inverse after exponential decay fit (for T2)");
  append_arg(invert,"invert");


  masklevel=0.5;
  masklevel.set_cmdline_option("el").set_description("Level relative to mean value of first echo for setting mask of exponential decay fit");
  append_arg(masklevel,"masklevel");

}


bool RecoExpFit::process(RecoData& rd, RecoController& controller) {
  Log<Reco> odinlog(c_label(),"process");

  Range all=Range::all();

  ComplexData<4>& indata=rd.data(Rank<4>());
  Data<float,4> magn(cabs(indata));
  TinyVector<int,4> inshape=indata.shape();
  TinyVector<int,3> outshape(inshape(1), inshape(2), inshape(3));

  dvector xvec=controller.dim_values(userdef);
  ODINLOG(odinlog,normalDebug) << "xvec=" << xvec.printbody() << STD_endl;
  int nx=xvec.size();
  if(nx!=inshape(0)) {
    ODINLOG(odinlog,errorLog) << "size mismatch: nx(" << nx << ")!=" << inshape(0) << STD_endl;
    return false;
  }

  ComplexData<3>& outdata=rd.data(Rank<3>());
  outdata.resize(outshape);
  outdata=STD_complex(0.0);

  float noiselevel=masklevel*mean(magn(0,all,all,all));

  LinearFunction linf;
  Data<float,1> echomagn(nx);
  Data<float,1> echolog(nx);
  Data<float,1> ysigma(nx);
  Data<float,1> xvals(nx);
  for(int i=0; i<nx; i++) xvals(i)=xvec[i];

  for(int iphase3d=0; iphase3d<inshape(1); iphase3d++) {
    for(int iphase=0; iphase<inshape(2); iphase++) {
      for(int iread=0; iread<inshape(3); iread++) {
        if(magn(0,iphase3d,iphase,iread)>noiselevel) {

          echomagn=magn(all,iphase3d,iphase,iread);
          echolog=log(echomagn);
          ysigma=pow(echomagn, -1); // weight fit by magnitude
          if(linf.fit(echolog,ysigma,xvals)) {
            if(invert) {
              outdata(iphase3d,iphase,iread)=secureDivision( 1.0, -linf.m.val );
            } else {
              outdata(iphase3d,iphase,iread)=-linf.m.val;
            }
          }
        }
      }
    }
  }

  return execute_next_step(rd,controller);
}
