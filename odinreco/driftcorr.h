/***************************************************************************
                          driftcorr.h  -  description
                             -------------------
    begin                : Thu Mar 1 2012
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef RECODRIFTCORR_H
#define RECODRIFTCORR_H

#include "step.h"

static const char* posted_repdrift_str="fieldrepdrift";


static void modify4repdrift(RecoCoord& coord);


class RecoDriftCalc: public RecoStep {

  // implementing virtual functions of RecoStep
  STD_string label() const {return "driftcalc";}
  STD_string description() const {return "Calculate field drift for each repetition";}
  bool process(RecoData& rd, RecoController& controller);
  RecoCoord input_coord() const {return RecoCoord::coord_with_mode(RecoIndex::collected,cycle,slice,echo,epi,channel,line3d,line,readout);}
  void modify_coord(RecoCoord& coord) const {/*coord.set_mode(RecoIndex::single, channel,slice,line3d,line,readout);*/}
  bool query(RecoQueryContext& context);
  RecoStep* allocate() const {return new RecoDriftCalc;}
  void init();

  LDRstring driftcalcFile;
  LDRfloat driftcalcTE;

};



////////////////////////////////////////////////////////


class RecoDriftCorr: public RecoStep {

  // implementing virtual functions of RecoStep
  STD_string label() const {return "driftcorr";}
  STD_string description() const {return "Correct for linear field drift over repetitions";}
  bool process(RecoData& rd, RecoController& controller);
  RecoCoord input_coord() const {return RecoCoord::coord_with_mode(RecoIndex::collected,readout);}
  void modify_coord(RecoCoord& coord) const {}
  RecoStep* allocate() const {return new RecoDriftCorr;}
  void init() {}

  Data<float,1> driftcache;
  Mutex mutex;
};


#endif

