#include "adc.h"
#include "data.h"
#include "controller.h"

bool RecoAdcReflect::process(RecoData& rd, RecoController& controller) {
  if(rd.coord().flags&recoReflectBit) {
    // reverseSelf will modify ascending order of the Blitz array which will screw up the rest of the reco so we will apply it to a local copy
    ComplexData<1>& data=rd.data(Rank<1>());
    ComplexData<1> adc_reversed(data.copy());
    adc_reversed.reverseSelf(firstDim);
    data(Range::all())=adc_reversed(Range::all());
  }
  return execute_next_step(rd,controller);
}


///////////////////////////////////////////////////////////////////////////

bool RecoAdcGridPrep::process(RecoData& rd, RecoController& controller) {
  Log<Reco> odinlog(c_label(),"process");


  if(rd.coord().readoutShape) {

    trajmutex.lock(); // also protect global readoutShape
    if(!traj.size()) {

      const Data<float,1>& readoutShape=rd.coord().readoutShape->first;

      ComplexData<1>& indata=rd.data(Rank<1>());

      int npts=indata.size();
      ODINLOG(odinlog,normalDebug) << "npts=" << npts << STD_endl;
      if(int(readoutShape.size())!=npts) {
        trajmutex.unlock();
        ODINLOG(odinlog,errorLog) << "Size mismatch: " << readoutShape.size() << "!=" << npts << STD_endl;
        return false;
      }

      // calc kspace by integration of readout shape
      fvector kspace(npts); kspace=0.0;
      if(npts) {
        kspace[0]=0.5*readoutShape(0);
        for(int i=1; i<npts; i++) {
          kspace[i]=kspace[i-1]+0.5*(readoutShape(i-1)+readoutShape(i));
        }
      }

      kspace-=kspace.minvalue(); // lowest val is zero
      kspace.normalize(); // rel. kspace position from 0 to 1
      kspace=controller.kspace_extent()(2)*(kspace-0.5); // symmetric about origin

      traj.resize(1,npts);
      for(int i=0; i<npts; i++) {
        traj(0,i).coord=TinyVector<float,3>(0.0,0.0,kspace[i]);
      }
    }
    trajmutex.unlock();

    rd.coord().kspaceTraj=&traj;
  }

  return execute_next_step(rd,controller);
}


///////////////////////////////////////////////////////////////////////////


bool RecoAdcWeight::process(RecoData& rd, RecoController& controller) {
  Log<Reco> odinlog(c_label(),"process");

  const ComplexData<1>* weightVec=rd.coord().weightVec;
  if(weightVec) {
    MutexLock lock(weightMutex); // protect global weightVec

    int weightsize=weightVec->size();
    ODINLOG(odinlog,normalDebug) << "weightsize=" << weightsize << STD_endl;

    ComplexData<1>& data=rd.data(Rank<1>());

    if(weightsize!=data.extent(0)) {
      ODINLOG(odinlog,errorLog) << "Size mismatch weightsize(" << weightsize << ")!=adc(" << data.extent(0) << ")" << STD_endl;
      return false;
    }

    data=data*(*weightVec);
  }

  return execute_next_step(rd,controller);
}

///////////////////////////////////////////////////////////////////////////


bool RecoAdcBaseline::process(RecoData& rd, RecoController& controller) {
  Log<Reco> odinlog(c_label(),"process");

  ComplexData<1>& adc=rd.data(Rank<1>());

  float maxsig=max(cabs(adc));

  // Calculate mean complex offset of samples with small magnitude
  int nsamples_bline=0;
  STD_complex sigsum(0.0);
  for(int isample=0; isample<int(adc.size()); isample++) {
    STD_complex sigval=adc(isample);
    if(cabs(sigval)<0.05*maxsig) {
      sigsum+=sigval;
      nsamples_bline++;
    }
  }
  if(nsamples_bline) sigsum/=STD_complex(nsamples_bline);
  adc-=sigsum;
  ODINLOG(odinlog,normalDebug) << "nsamples_bline/sigsum=" << nsamples_bline << "/" << sigsum << STD_endl;

  return execute_next_step(rd,controller);
}

///////////////////////////////////////////////////////////////////////////


bool RecoAdcPad::process(RecoData& rd, RecoController& controller) {
  Log<Reco> odinlog(c_label(),"process");

  ComplexData<1>& adc=rd.data(Rank<1>());

  float relcenter=rd.coord().relCenterPos;
  
  if(relcenter<0.0 || relcenter>1.0) {
    ODINLOG(odinlog,errorLog) << "relcenter=" << relcenter << STD_endl;
    return false;
  }

  bool end_omitted=false;
  if(relcenter>0.5) {
    relcenter=1.0-relcenter;
  }

  int oldsize=adc.size();
  int newsize=int(2.0*(1.0-relcenter)*oldsize+0.5);
  ODINLOG(odinlog,normalDebug) << "oldsize/newsize=" << oldsize << "/" << newsize << STD_endl;

  ComplexData<1> newadc(newsize);
  newadc=STD_complex(0.0);
  
  int offset=newsize-oldsize;
  if(end_omitted) offset=0;
  
  newadc(Range(offset,offset+oldsize-1))=adc;
  
  adc.reference(newadc);

  // adjust numof and center position
  rd.coord().index[readout].set_numof(newsize);
  rd.coord().relCenterPos=0.5;

  // Adjust adcstart/end
  if(!end_omitted) {
    rd.coord().adcstart+=offset;
    rd.coord().adcend+=offset;
  }
  ODINLOG(odinlog,normalDebug) << "adcstart/adcend=" << rd.coord().adcstart << "/" << rd.coord().adcend << STD_endl;


  return execute_next_step(rd,controller);
}


