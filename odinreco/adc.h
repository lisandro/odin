/***************************************************************************
                          adc.h  -  description
                             -------------------
    begin                : Mon Jan 22 2007
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef RECOADC_H
#define RECOADC_H

#include "step.h"


class RecoAdcReflect : public RecoStep {

  // implementing virtual functions of RecoStep
  STD_string label() const {return "adc_reflect";}
  STD_string description() const {return "Reflect ADC if recoReflectBit is set";}
  bool process(RecoData& rd, RecoController& controller);
  RecoCoord input_coord() const {return RecoCoord::coord_with_mode(RecoIndex::collected,readout);}
  void modify_coord(RecoCoord& coord) const {}
  RecoStep* allocate() const {return new RecoAdcReflect;}
  void init() {}

};


////////////////////////////////////////////////////////


class RecoAdcGridPrep : public RecoStep {

  // implementing virtual functions of RecoStep
  STD_string label() const {return "adc_gridprep";}
  STD_string description() const {return "Prepare ADC for gridding (ramp sampling)";}
  bool process(RecoData& rd, RecoController& controller);
  RecoCoord input_coord() const {return RecoCoord::coord_with_mode(RecoIndex::collected,readout);}
  void modify_coord(RecoCoord& coord) const {}
  RecoStep* allocate() const {return new RecoAdcGridPrep;}
  void init() {}

  KspaceTraj traj;  // caching trajectory
  Mutex trajmutex;

};

////////////////////////////////////////////////////////


class RecoAdcWeight : public RecoStep {

  // implementing virtual functions of RecoStep
  STD_string label() const {return "adc_weight";}
  STD_string description() const {return "Weight ADC using weightVec";}
  bool process(RecoData& rd, RecoController& controller);
  RecoCoord input_coord() const {return RecoCoord::coord_with_mode(RecoIndex::collected,readout);}
  void modify_coord(RecoCoord& coord) const {}
  RecoStep* allocate() const {return new RecoAdcWeight;}
  void init() {}

  Mutex weightMutex; // to protect global Blitz array which contains weightVec

};

////////////////////////////////////////////////////////


class RecoAdcBaseline : public RecoStep {

  // implementing virtual functions of RecoStep
  STD_string label() const {return "adc_baseline";}
  STD_string description() const {return "Baseline correction of ADCs";}
  bool process(RecoData& rd, RecoController& controller);
  RecoCoord input_coord() const {return RecoCoord::coord_with_mode(RecoIndex::collected,readout);}
  void modify_coord(RecoCoord& coord) const {}
  RecoStep* allocate() const {return new RecoAdcBaseline;}
  void init() {}

};


/////////////////////////////////////////////////////////////////


class RecoAdcPad : public RecoStep {

  // implementing virtual functions of RecoStep
  STD_string label() const {return "adc_pad";}
  STD_string description() const {return "Pad missing points by zeroes";}
  bool process(RecoData& rd, RecoController& controller);
  RecoCoord input_coord() const {return RecoCoord::coord_with_mode(RecoIndex::collected,readout);}
  void modify_coord(RecoCoord& coord) const {}
  RecoStep* allocate() const {return new RecoAdcPad;}
  void init() {}

};



#endif

