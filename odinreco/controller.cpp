#include "controller.h"
#include "data.h"
#include "step.h"
#include "reader.h"
#include "measindex.h"
#include "fieldmap.h" // for posted_fmap_str




RecoController::RecoController(LDRblock& parblock) : input(0), pipeline(0), factory(0), pmeter(display) {

  jobs=numof_cores();
  jobs.set_cmdline_option("j").set_description("Number of concurrent jobs(threads) in reco");
  parblock.append(jobs);

  recipe.set_cmdline_option("rp").set_description("Reco recipe (pipeline layout)");
  parblock.append(recipe);

  recipeFile.set_cmdline_option("rf").set_description("Read reco recipe (pipeline layout) from this file");
  parblock.append(recipeFile);

  select.set_cmdline_option("s").set_description("Select certain dimensions ranges (space separated list of 'dim=selection', e.g. slice=3)");
  parblock.append(select);

  timeout=0;
  timeout.set_cmdline_option("to").set_description("Timeout for reco in seconds, 0=inifinite");
  parblock.append(timeout);

  maxrecursion=100;
  maxrecursion.set_cmdline_option("mr").set_description("Maximum recursion of reconstruction when waiting for blackboard data, zero means no recursion");
  parblock.append(maxrecursion);

  conjPhaseFT=false;
  conjPhaseFT.set_cmdline_option("cp").set_description("Use direct Fourier transform with conjugate-phase correction instead of gridding");
  parblock.append(conjPhaseFT);

  slidingWindow=false;
  slidingWindow.set_cmdline_option("sw").set_description("Sliding window reconstruction, e.g. for Spiral/PROPELLER");
  parblock.append(slidingWindow);

  keyhole=false;
  keyhole.set_cmdline_option("kh").set_description("Keyhole reconstruction, e.g. for PROPELLER");
  parblock.append(keyhole);

  sepChannels=false;
  sepChannels.set_cmdline_option("sc").set_description("Store images from channels separately instead of combining them");
  parblock.append(sepChannels);

  dumpKspace=false;
  dumpKspace.set_cmdline_option("ks").set_description("Store k-space data");
  parblock.append(dumpKspace);

  dumpGrappaTemplate=false;
  dumpGrappaTemplate.set_cmdline_option("gt").set_description("Store GRAPPA template images");
  parblock.append(dumpGrappaTemplate);

  disablePhaseCorr=false;
  disablePhaseCorr.set_cmdline_option("dt").set_description("Disable template-based phase correction");
  parblock.append(disablePhaseCorr);

  disableHomodyne=false;
  disableHomodyne.set_cmdline_option("dh").set_description("Disable homodyne reconstruction");
  parblock.append(disableHomodyne);

  disableMultiFreq=false;
  disableMultiFreq.set_cmdline_option("dm").set_description("Disable fieldmap-based multi-frequency reconstruction");
  parblock.append(disableMultiFreq);

  disableDriftCorr=false;
  disableDriftCorr.set_cmdline_option("dd").set_description("Disable field-drift correction");
  parblock.append(disableDriftCorr);

  disableSliceTime=false;
  disableSliceTime.set_cmdline_option("ds").set_description("Disable slice-time correction");
  parblock.append(disableSliceTime);

  disableGrappa=false;
  disableGrappa.set_cmdline_option("dg").set_description("Disable GRAPPA interpolation");
  parblock.append(disableGrappa);

  phaseCourse=false;
  phaseCourse.set_cmdline_option("pt").set_description("Combine and store phase timecourse from all channels");
  parblock.append(phaseCourse);

  qcspike=false;
  qcspike.set_cmdline_option("qs").set_description("Quality control: Check for spikes in data");
  parblock.append(qcspike);

  preproc3d="null";
  preproc3d.set_cmdline_option("prp").set_description("Recipe for post processing of 3D volume before channel combining");
  parblock.append(preproc3d);

  postproc3d="null";
  postproc3d.set_cmdline_option("pop").set_description("Recipe for post processing of 3D volume after channel combining");
  parblock.append(postproc3d);

  dumpRawPrefix="";
  dumpRawPrefix.set_cmdline_option("rd").set_description("Dump signal data and its protocol to files with this prefix, do not reconstruct images");
  parblock.append(dumpRawPrefix);

  fieldmapFile="";
  fieldmapFile.set_cmdline_option("fm").set_description("Load fieldmap data from this file");
  parblock.append(fieldmapFile);

/*
  driftcorrFile="";
  driftcorrFile.set_cmdline_option("dcr").set_description("Load field drift from this file");
  parblock.append(driftcorrFile);
*/

  driftcorrSel="";
  driftcorrSel.set_cmdline_option("dcs").set_description("Index selection string in recipe used for drift correction (space separated list of 'dim=selection', e.g. te=2)");
  parblock.append(driftcorrSel);


  factory=new RecoStepFactory(&parblock);
}


RecoController::~RecoController() {
  delete_countmap_cache();
  delete factory;
}


TinyVector<float,3> RecoController::reloffset() const {
  TinyVector<float,3> result; result=0.0;
  MutexLock lock(inputmutex);
  if(input) result=input->reloffset();
  return result;
}


dvector RecoController::dim_values(recoDim dim) const {
  dvector result;
  MutexLock lock(inputmutex);
  if(input) result=input->dim_values(dim);
  return result;
}

STD_string RecoController::image_proc() const {
  STD_string result;
  MutexLock lock(inputmutex);
  if(input) result=input->image_proc();
  return result;
}

TinyVector<int,3> RecoController::image_size() const {
  TinyVector<int,3> result; result=0;
  MutexLock lock(inputmutex);
  if(input) result=input->image_size();
  return result;
}

TinyVector<float,3> RecoController::kspace_extent() const {
  TinyVector<float,3> result; result=0;
  TinyVector<int,3> imgshape=image_size();

  for(int i=0; i<n_directions; i++) {
    float fov=prot_cache.geometry.get_FOV(direction(2-i));
    result(i)=secureDivision(2.0*PII*imgshape(i), fov);
  }

  return result;
}


bool RecoController::inquire_data(const STD_string& label, RecoData& data) {
  Log<Reco> odinlog("RecoController","inquire_data");

  if(!status) return false; // Return immediately if an error occured, i.e. do not wait for any data in this case

  ODINLOG(odinlog,normalDebug) << "Requested " << label << "(" << data.coord().print() << ")" << STD_endl;

  if(blackboard.inquire(label,data)) {
    ODINLOG(odinlog,normalDebug) << "Returning " << label << "(" << data.coord().print() << ")" << STD_endl;
    return true; // 1st shot without feeding pipeline or signalling waiting state
  }
  ODINLOG(odinlog,normalDebug) <<  label << "(" << data.coord().print() << ") not ready" << STD_endl;

  int depth=thread_depth[Thread::self()];
  if(depth<maxrecursion) { // avoid massive recursion

    ODINLOG(odinlog,normalDebug) << "Recursing in depth=" << depth << STD_endl;

    while(feed_pipeline()) { // Continue feeding pipeline until no data is available
      if(blackboard.inquire(label,data)) {
        ODINLOG(odinlog,normalDebug) << "Returning " << label << "(" << data.coord().print() << ")" << STD_endl;
        return true; // Return immediately if inquired data becomes available
      }
    }
    ODINLOG(odinlog,normalDebug) << "Pipeline empty" << STD_endl;
  }

  incr_waiting_threads("Waiting for "+label+"("+data.coord().print()+")");

  while(!blackboard.inquire(label,data,true)) { // block while waiting
    int nwaiting=numof_waiting_threads();
    ODINLOG(odinlog,normalDebug) << "nwaiting=" << nwaiting << STD_endl;
    if(int(numof_waiting_threads()) >= jobs) { // signal an error if all threads are waiting for input
      ODINLOG(odinlog,errorLog) << "Out of input data while waiting for " << label << "(" << data.coord().print() << ")" << STD_endl;

      MutexLock lock(waiting_mutex);

      // store snapshot of states of waiting threads for later error report
      waiting_error="Waiting threads:\n";
      for(WaitingMap::const_iterator it=waiting.begin(); it!=waiting.end(); ++it) {
        waiting_error+="job"+itos(it->first)+": "+it->second+"\n";
      }

      return false;
    }
//    ODINLOG(odinlog,normalDebug) <<  "sleeping while waiting for " << label << STD_endl;
//    sleep_ms(10); // Avoid tight inifinite loop, give other threads CPU time to process their input
  }
  ODINLOG(odinlog,normalDebug) << "blocked inquire successful" << STD_endl;

  decr_waiting_threads();

  ODINLOG(odinlog,normalDebug) << "Returning " << label << "(" << data.coord().print() << ")" << STD_endl;
  return true;
}


void RecoController::delete_countmap_cache() {
  for(STD_map<STD_string,const CoordCountMap*>::const_iterator it=countmap_cache.begin(); it!=countmap_cache.end(); ++it) {
    delete it->second;
  }
  countmap_cache.clear();
}


const CoordCountMap* RecoController::create_count_map(const RecoCoord& mask) const {
  Log<Reco> odinlog("RecoController","create_count_map");

  if(!input) {
    ODINLOG(odinlog,errorLog) << "Input reader not initialized yet" << STD_endl;
    return 0;
  }

  STD_string maskstring=mask.print(RecoIndex::all); // Using full printout as unique key for masks
  STD_map<STD_string,const CoordCountMap*>::const_iterator it=countmap_cache.find(maskstring);
  if(it!=countmap_cache.end()) {
    ODINLOG(odinlog,normalDebug) << "Using cached countmap for " << mask.print() << STD_endl;
    return it->second;
  }

  const STD_vector<RecoCoord>& coords=input->get_coords();
  ODINLOG(odinlog,normalDebug) << "sizeof(RecoCoord)/coords.size()=" << sizeof(RecoCoord) << "/" << coords.size() << STD_endl;

  CoordCountMap* countmap=new CoordCountMap;

  ODINLOG(odinlog,normalDebug) << "coords.size()=" << coords.size() << STD_endl;
  for(unsigned int iadc=0; iadc<coords.size(); iadc++) {
    RecoCoord coord=coords[iadc];
    if(coord.prep_count(mask)) (*countmap)[coord]++;
  }

  ODINLOG(odinlog,normalDebug) << "countmap(" << mask.print() << ")=" << countmap->size() << STD_endl;

  countmap_cache[maskstring]=countmap;

  return countmap;
}



bool RecoController::create_numof(const RecoCoord& mask, unsigned short numof[n_recoDims], STD_string& printed) const {
  Log<Reco> odinlog("RecoController","create_numof");

  const CoordCountMap* countmap=create_count_map(mask);
  if(!countmap) return false;

  // Reset
  for(unsigned int idim=0; idim<n_recoDims; idim++) numof[idim]=1;
  has_readoutshape=false;
  has_flag_cache=0;

  for(int itempl=0; itempl<n_templateTypes; itempl++) has_templcorr_cache[itempl]=false;
  for(int inav=0; inav<n_navigatorTypes; inav++) has_navigator_cache[inav]=false;


  has_traj=false;
  has_oversampling_cache=false;
  has_relcenter_cache=false;


  for(CoordCountMap::const_iterator it=countmap->begin(); it!=countmap->end(); ++it) {
    const RecoCoord& coord=it->first;
    ODINLOG(odinlog,normalDebug) << "coord(" << it->second << ")=" << coord.print() << STD_endl;
    for(unsigned int idim=0; idim<n_recoDims; idim++) {
      numof[idim]=STD_max(numof[idim],(unsigned short)(coord.index[idim]+1));
      if(coord.readoutShape) has_readoutshape=true;
      has_flag_cache=has_flag_cache|coord.flags;
      has_templcorr_cache[coord.index[templtype]]=true;
      has_navigator_cache[coord.index[navigator]]=true;
      if(coord.kspaceTraj) has_traj=true;
      if(coord.overSamplingFactor>1.0) has_oversampling_cache=true;
      if(coord.relCenterPos!=0.5) has_relcenter_cache=true;
    }
  }

  for(int idim=0; idim<n_recoDims; idim++) {
    if(numof[idim]>1) printed+="#"+STD_string(recoDimLabel[idim])+"="+itos(numof[idim])+" ";
  }

  ODINLOG(odinlog,normalDebug) << "numof: " << printed << STD_endl;

  return true;
}


static bool in_tracefunction=false; // to avoid recursion
void RecoController::tracefunction(const LogMessage& msg) {
  if(in_tracefunction) return;
  in_tracefunction=true;
  int selfid=Thread::self();
  if(selfid!=controlthread) STD_cerr << "job" << selfid << "-"; // Do not prefix messages of control thread
  STD_cerr << msg.str();
  in_tracefunction=false;
}


RecoStep* RecoController::create_pipeline(const STD_string& rec, const RecoCoord* initial_coord, int argc, char* argv[]) const {
  Log<Reco> odinlog("RecoController","create_pipeline");

  RecoStep* result=0;

  svector toks(tokens(rec,'|','(',')'));

  RecoCoord coord; // coordinate interfaces during pipeline
  if(initial_coord) coord=(*initial_coord);


  RecoStep* laststep=0;
  for(unsigned int itok=0; itok<toks.size(); itok++) {

    // store and remove functor args
    STD_string functorstr=shrink(toks[itok]);
    STD_string args=extract(functorstr, "(", ")", true);
    functorstr=rmblock(functorstr, "(", ")", true, true, false, true);
    ODINLOG(odinlog,normalDebug) << "toks/args[" << itok << "]=>" << toks[itok] << "/" << args << "<" << STD_endl;

    ODINLOG(odinlog,normalDebug) << "functorstr=>" << functorstr << "<" << STD_endl;
    RecoStep* step=factory->create(functorstr);
    if(!step) {
      ODINLOG(odinlog,errorLog) << "Could not create functor with label >" << functorstr << "<" << STD_endl;
      return 0; // Return immediately if functor could not be created
    }

    if(args!="") step->set_args(args);

    if(!step->pipeline_init(*this,coord)) {
      ODINLOG(odinlog,errorLog) << "Could not initialize pipeline of functor >" << functorstr << "< with arguments >" << args << "<" << STD_endl;
      return 0;
    }

    // check interface between steps
    RecoCoord coordin=step->input_coord();
    if(!coord.compatible(coordin)) {
      STD_string lastlabel;
      if(laststep) lastlabel=laststep->label();
      ODINLOG(odinlog,errorLog) << "Interface mismatch: output(" << lastlabel << ")=" << coord.print(RecoIndex::all) << " does not match input(" << step->label() << ")=" << coordin.print(RecoIndex::all) << STD_endl;
      return 0;
    }

    step->modify_coord(coord); // update interface

    if(!itok) result=step;

    if(laststep) {
      laststep->set_next_step(step);
    }
    laststep=step;
  }

  return result;
}


STD_string RecoController::stepmanual() const {
  return factory->manual();
}

//////////////////////////////////////////////////////////////////////////////////////


class TimeOutThread : public Thread {

 public:
  TimeOutThread(unsigned int seconds) : secs(seconds) {}
  bool init(unsigned int seconds) {
    return true;
  }

 private:

  void run() {
    Log<Reco> odinlog("TimeOutThread","run");
    sleep_ms(1000*secs);
    ODINLOG(odinlog,warningLog) << "Reco timed out after " << secs << " seconds, exiting" << STD_endl;
    exit(1);
  }

  unsigned int secs;
};

//////////////////////////////////////////////////////////////////////////////////////

template<recoDim Dim, recoDim OrthoDim>
bool RecoController::get_kspace_sampling_pattern(const RecoCoord& mask, unsigned int& reduction_factor, bool& partial_fourier, bool& half_fourier) const {
  Log<Reco> odinlog("RecoController","get_kspace_sampling_pattern");

  reduction_factor=1;
  partial_fourier=false;
  half_fourier=false;

  if(numof_cache[Dim]<2) return true;


  RecoMeasIndex<Dim, RecoDim<3, channel, freq, repetition> > measlines; // exclude unimportant dimensions for effiency

  measlines.init(mask, *this);

  for(unsigned int iorth=0; iorth<numof_cache[OrthoDim]; iorth++) {

    RecoCoord coord(mask);
    coord.index[OrthoDim]=iorth;
    ivector indexvec=measlines.get_indices(coord);

    // Calculate  reduction factor
    for(unsigned int i=1; i<indexvec.size(); i++) {
      int diff=indexvec[i]-indexvec[i-1];
      reduction_factor=STD_max(int(reduction_factor),diff);
    }

    unsigned int nindices=indexvec.size();
    if(nindices) {
     int startindex=indexvec[0];
     int maxindex=indexvec[nindices-1];

     if(startindex>=int(reduction_factor)) partial_fourier=true;

     float center=0.5*float(maxindex);
     ODINLOG(odinlog,normalDebug) << "startindex/maxindex/center[" << recoDimLabel[Dim] << "]=" << startindex << "/" << maxindex << "/" << center << STD_endl;

     if(fabs(float(startindex)-center)<=1.0) half_fourier=true;
    }
  }

  if(reduction_factor<1) reduction_factor=1;

  ODINLOG(odinlog,normalDebug) << "reduction_factor/partial_fourier/half_fourier[" << recoDimLabel[Dim] << "]=" << reduction_factor << "/" << partial_fourier << "/" << half_fourier << STD_endl;

  return true;
}

//////////////////////////////////////////////////////////////////////////////////////


bool RecoController::autorecipe(const RecoCoord& mask, STD_string& result) const {
  Log<Reco> odinlog("RecoController","autorecipe");

  result="";

  ODINLOG(odinlog,infoLog) << "Creating default recipe ..." << STD_endl;

  unsigned int reduction_factor=0;
  bool partial_fourier=false;
  bool half_fourier=false;
  if(!get_kspace_sampling_pattern<line,line3d>(mask,reduction_factor, partial_fourier, half_fourier)) return false;

  unsigned int reduction_factor3d=0;
  bool partial_fourier3d=false;
  bool half_fourier3d=false;
  if(!get_kspace_sampling_pattern<line3d,line>(mask,reduction_factor3d, partial_fourier3d, half_fourier3d)) return false;

  if(half_fourier3d) {
    ODINLOG(odinlog,errorLog) << "Not yet implemented: half Fourier reconstruction in 2nd phase encoding direction" << STD_endl;
    return false;
  }


  unsigned int max_reduction_factor=STD_max(reduction_factor,reduction_factor3d);
  unsigned int reduction_factor_prot=protocol().seqpars.get_ReductionFactor();
  if(max_reduction_factor!=reduction_factor_prot) {
    ODINLOG(odinlog,warningLog) << "Mismatch: max_reduction_factor(" << max_reduction_factor << ")=!reduction_factor_prot(" << reduction_factor_prot << ")" << STD_endl;
  }

  bool max_partial_fourier=partial_fourier || partial_fourier3d;
  bool partial_fourier_prot=(protocol().seqpars.get_PartialFourier()>0.0);
  if(max_partial_fourier!=partial_fourier_prot) {
    ODINLOG(odinlog,warningLog) << "Mismatch: max_partial_fourier(" << max_partial_fourier << ")=!partial_fourier_prot(" << partial_fourier_prot << ")" << STD_endl;
  }


  bool apply_driftcorr=(numof_cache[repetition]>1 && !disableDriftCorr);

  STD_string driftcorrstr;
  if(apply_driftcorr) driftcorrstr="driftcorr | ";


  STD_string averagestr;
  if(numof_cache[average]>1) averagestr="averagecoll | averagesum | ";


  bool has_blade=(numof_cache[cycle]>1 && !has_traj); // Assume PROPELLER


  STD_string adcprep;
  if(!has_traj && has_relcenter_cache) adcprep+="adc_pad | ";
  
  STD_string adcdeapodize;
  STD_string adcdeapodize_blade;
  if(has_readoutshape) {
    STD_string gridarg;
    if(has_blade) gridarg="(false)"; // no deapo shift
    adcprep+="adc_gridprep | grid1d"+gridarg+" | "; // regrid first to correct for antisymmetric gradient distortions before reflecting / adc_regrid does not work well for blades yet
    if(!dumpKspace) {
      if(has_blade) adcdeapodize_blade="deapodize1d | ";
      else          adcdeapodize      ="deapodize1d | ";
    }
  }
  if(has_flag_cache&recoReflectBit) adcprep+="adc_reflect | ";


  STD_string chanstr;
  if(numof_cache[channel]>1 && !sepChannels && !dumpKspace) chanstr="chancoll | chansum | ";

  STD_string corrstr;
  STD_string navstr;

  STD_string phasecorrstr;
  if(has_templcorr_cache[phasecorr_template]) {
    if(!disablePhaseCorr) phasecorrstr="phasecorr | ";
    corrstr+="templtype=P { "+averagestr+adcprep+"echocoll | phasemap}";
  }

  STD_string oversamplingstr;
  if(has_oversampling_cache && !dumpKspace) oversamplingstr="oversampling | ";

  STD_string mfreqbegin;
  STD_string mfreqend;
  STD_string chansumopt;
  bool ext_fieldmap=fieldmapFile!="";
  if(has_templcorr_cache[fieldmap_template] || ext_fieldmap) {
    if(disableMultiFreq) {
      corrstr+="templtype=F { }"; // throw away field map data
    } else {

      if(!conjPhaseFT && !dumpKspace) {
        mfreqbegin="multifreq | ";
        mfreqend="freqcoll | freqcomb | ";
      }

      if(!ext_fieldmap) {
        if(corrstr!="") corrstr+="; ";

        STD_string fmap_offsetstr="offset | ";
        if(has_blade && mfreqend!="") fmap_offsetstr=""; // disable offset for multi-frequency reco of blades


        //STD_string cyclerotstr;
        //if(has_blade) cyclerotstr="cyclerot | ";
        //corrstr+="templtype=F { adc_reflect | kspace | offset | filter(Hann) | fft | tecoll | fieldmap | "+chanstr+"interpolate | "+cyclerotstr+"post(fieldmap) | "+oversamplingstr+"slicecoll | image | store(fieldmap)}";
        corrstr+="templtype=F { adc_reflect | kspace |"+fmap_offsetstr+"filter(Hann) | fft | tecoll | fieldmap | "+chanstr+oversamplingstr+" post(fieldmap) | "+"slicecoll | image | store(fieldmap)}";
      }
    }
  }


  if(has_navigator_cache[epi_navigator]) {
    phasecorrstr="epinavcorr | ";
    if(navstr!="") navstr+="; ";
    navstr+="navigator=e { "+adcprep+"echocoll | epinavscan }";
  }

  STD_string interpolstr;
  if(!disableGrappa) {
    bool has_grappa_template=has_templcorr_cache[grappa_template];
    if(has_grappa_template || reduction_factor>1 || reduction_factor3d>1) {
      STD_string redfactstr=itos(reduction_factor);
      STD_string redfactstr3d=itos(reduction_factor3d);
      if(has_grappa_template) {
        if(corrstr!="") corrstr+="; ";
        corrstr+="templtype=G {"+averagestr+adcprep+phasecorrstr+"kspace | chancoll | ";
        if(reduction_factor>1)   corrstr+="grappaweightstempl("  +redfactstr+  ")";
        if(reduction_factor3d>1) corrstr+="grappaweightstempl3d("+redfactstr3d+")";
        if(dumpGrappaTemplate) {
          corrstr+=" | chansplit | fft | slicecoll | image | store(grappatemplate)";
        }
        corrstr+="}";
      }
      STD_string weightsstr;
      if(!has_grappa_template) {
        if(reduction_factor>1)   weightsstr+="grappaweights("+  redfactstr+  ") | ";
        if(reduction_factor3d>1) weightsstr+="grappaweights3d("+redfactstr3d+") | ";
      }
      interpolstr+="chancoll | "+weightsstr;

      STD_string keephshapestr;
      if(has_blade) keephshapestr=",true"; // do not expand k-space to image matrix size

      if(reduction_factor>1)   interpolstr+="grappa("  +redfactstr+keephshapestr+  ") | ";
      if(reduction_factor3d>1) interpolstr+="grappa3d("+redfactstr3d+keephshapestr+") | ";
      interpolstr+="chansplit | ";
    }
  }

  STD_string fftstr="fft | ";
  if(!has_traj && !disableHomodyne) {
    if(has_relcenter_cache || partial_fourier3d) {fftstr="homodyne | ";}
    if(!has_blade) {
      if(half_fourier)         {fftstr="halffour | fft | ";}
      else if(partial_fourier) {fftstr="homodyne | ";}
    }
  }

  STD_string offsetstr="offset | ";

  if(dumpKspace) {
    fftstr="";
    offsetstr="";
  }


  STD_string swstr;
  if(slidingWindow || (keyhole && has_blade) ) swstr="slidingwindow | ";

  STD_string spikestr="";
  if(qcspike) spikestr="qcspike | "; // insert after k-space has been collected but before interpolation

  STD_string kspacestr=adcprep + phasecorrstr + "kspace | " + spikestr + interpolstr;
  STD_string adcbaselinestr;
  STD_string deapodizestr;
  STD_string gridstr;
  if(has_traj) {
    adcbaselinestr="adc_baseline | ";
    if(conjPhaseFT) {
      kspacestr="adc_weight | conjphase | " + spikestr + swstr + "cyclecoll | cyclesum | ";
      offsetstr="";
      fftstr="";
    } else {
      kspacestr="grid2d | line3dcoll | " + spikestr + swstr + "cyclecoll | cyclesum | ";
      if(!dumpKspace) deapodizestr="deapodize2d | ";
      gridstr="gridcut | ";
    }
    oversamplingstr="";
  }

  STD_string bladestr;
  if(has_blade) {

    STD_string keyholeopt;
    if(keyhole) keyholeopt="(true)";

    // partial fourier reconstruction has to be applied before combining the blades!
    STD_string bladefftstr = "";
    if (mfreqend!="") bladefftstr = "fft | ";
    if (!disableHomodyne && partial_fourier) {
      if (mfreqend!="") bladefftstr = "homodyne | ";
      else bladefftstr = "homodyne | fft(false) | ";
    }

    STD_string bladecorrstr="bladecorr | ";

    if(mfreqend!="") bladecorrstr=mfreqend+"magn | "+adcdeapodize_blade+"fft(false) | ";
    mfreqend=""; // do not use in rest of pipeline

    bladestr=bladefftstr+bladecorrstr+swstr+"cyclecoll | bladecomb"+keyholeopt+" | grid2d | line3dcoll | ";
    if(!dumpKspace) deapodizestr="deapodize2d | ";
    gridstr="gridcut | ";
  }

  STD_string maskstr;
  if(has_traj || has_blade) maskstr="circmask | ";


  STD_string slicetimestr;
  if(!disableSliceTime && numof_cache[repetition]>1 && numof_cache[slice]>1 && !dumpKspace) {
    slicetimestr="slicetime | ";
  }


  STD_string pp3dpre;
  STD_string pp3dpost;
  if(!dumpKspace) {
    if(preproc3d!="null") pp3dpre=preproc3d;
    else                  pp3dpre=shrink(input->preProc3D());
    if(postproc3d!="null") pp3dpost=postproc3d;
    else                   pp3dpost=shrink(input->postProc3D());
    if(pp3dpre.size() && (pp3dpre[pp3dpre.size()-1]!='|') ) pp3dpre+="|";
    if(pp3dpost.size() && (pp3dpost[pp3dpost.size()-1]!='|') ) pp3dpost+="|";
  }

  STD_string freqcomb_and_oversampling=mfreqend+oversamplingstr;
  if(ext_fieldmap) freqcomb_and_oversampling=oversamplingstr+mfreqend; // remove oversampling 1st before combining with external fieldmap

  STD_string kspaceprep="zerofill | filter | ";
  if(dumpKspace) kspaceprep="";

  if(numof_cache[dti]>1) pp3dpost+="dti | ";

  STD_string phasecoursestr;
  if(phaseCourse) {
    phasecoursestr="switch( { chanrepcoll | phasecourse | repsplit | "+ freqcomb_and_oversampling + "slicecoll | image | " + slicetimestr + "store(phasecourse) } ) | ";
  }

  STD_string driftcalcstr;
  if(apply_driftcorr) {

//    STD_string driftrecipe=averagestr + " kspace | fft | driftcoll | driftcalc"; // FFT gives better results than without
    STD_string driftrecipe=averagestr + " kspace | driftcoll | driftcalc"; // but not with GRAPPA ?

    STD_map<recoDim,unsigned short> selmap;
    if(!create_selection_map(driftcorrSel, selmap)) return false;
    if(selmap.size()) {
      // Apply selected dimensions to recipe recursively
      driftcalcstr=driftrecipe;
      for(STD_map<recoDim,unsigned short>::const_iterator it=selmap.begin(); it!=selmap.end(); ++ it) {
        driftcalcstr=STD_string("switch(")+recoDimLabel[it->first]+"="+itos(it->second)+"{"+driftcalcstr+"})";
      }
      driftcalcstr+=" | ";
    } else {
      driftcalcstr="switch( { " + averagestr + " kspace | fft | driftcoll | driftcalc } ) | "; // using switch to branch off drift calculation pipeline
    }
  }

  result = driftcalcstr + driftcorrstr + averagestr + adcbaselinestr + mfreqbegin + kspacestr + bladestr + offsetstr
                     + kspaceprep + fftstr + adcdeapodize + deapodizestr + gridstr + phasecoursestr + pp3dpre + chanstr
                     + freqcomb_and_oversampling + pp3dpost + maskstr
                     + "slicecoll | image | " + slicetimestr + "store";


  if(disableGrappa || corrstr!="") {
    result="switch(templtype=N { "+result+"} ; "+corrstr+")";
  }

  if(navstr!="") {
    result="switch(navigator=n { "+result+"} ; "+navstr+")";
  }

  ODINLOG(odinlog,normalDebug) << "result=" << result << STD_endl;
  return true;
}


bool RecoController::create_selection_map(const STD_string& selstr, STD_map<recoDim,unsigned short>& selmap, RecoCoord* initcoord) const {
  Log<Reco> odinlog("RecoController","create_selection_map");

  // Parse selected dimensions
  selmap.clear();
  svector seltoks(tokens(selstr));
  for(unsigned int isel=0; isel<seltoks.size(); isel++) {
    svector toks(tokens(seltoks[isel],'='));
    if(toks.size()==2) {
      int dim=-1;
      for(int idim=0; idim<n_recoDims; idim++) {
        if(toks[0]==recoDimLabel[idim]) dim=idim;
      }
      if(dim>=0) {
        int sel=atoi(toks[1].c_str());
        if(initcoord) {
          initcoord->index[dim]=sel;
          initcoord->index[dim].set_mode(RecoIndex::single);
        }
        selmap[recoDim(dim)]=sel;
      } else {
        ODINLOG(odinlog,errorLog) << "Unknwon selection dimension " << toks[0] << STD_endl;
        return false;
      }
    } else {
      ODINLOG(odinlog,errorLog) << "Error while parsing selection " << seltoks[isel] << STD_endl;
      return false;
    }
  }

  return true;
}


//////////////////////////////////////////////////////////////////////////////////////

bool RecoController::init(RecoReaderInterface* reader, LDRblock& opts, int argc, char* argv[]) {
  Log<Reco> odinlog("RecoController","init");

  input=reader;
  if(!input) {
    ODINLOG(odinlog,errorLog) << "no reader provided" << STD_endl;
    return false;
  }

  // Initialize default protocol by reader, do this as afirst step
  prot_cache=input->protocol();


  bool modify_args=false; // Do not modify command-line args since the same option may be used in different template instantiations

  // Parse recoInfo args
  svector riargs(tokens(input->cmdline_opts()));
  int riargc=riargs.size()+1; // 1st string is neglected
  ODINLOG(odinlog,normalDebug) << "riargc=" << riargc << STD_endl;
  char* riargv[riargc+1];
  for(int i=0; i<(riargc+1); i++) riargv[i]=0;
  for(unsigned int i=0; i<riargs.size(); i++) {
    STD_string argstr=extract(riargs[i], "\"", "\"", true); // strip escape characters, if possible/neccessary
    if(argstr=="") argstr=riargs[i]; // use original string otherwise
    unsigned int nchars=argstr.size();
    unsigned int iarg=i+1; // 1st string is neglected
    riargv[iarg]=new char[nchars+1];
    sprintf(riargv[iarg],"%s",argstr.c_str()); // Extra '%s' format string required for Debian compiler hardening
    ODINLOG(odinlog,normalDebug) << "riargv[" << iarg << "]=" << riargv[iarg] << STD_endl;
  }
  riargv[riargc]=0;
  opts.parse_cmdline_options(riargc,riargv,modify_args);
  for(int i=0; i<(riargc+1); i++) if(riargv[i]) delete[] riargv[i];

  opts.parse_cmdline_options(argc,argv,modify_args); // Override opts by commandline options

  if(dumpRawPrefix!="") {
    dump_raw();
    return true;
  }

  // start thread-specific logging
  controlthread=Thread::self();
  LogBase::set_log_output_function(RecoController::tracefunction);


#ifndef NO_THREADS // necessary, otherwise reco won't even start in single-threaded environment
  if(timeout>0) {
    (new TimeOutThread(timeout))->start();
  }
#endif

  // Parse selected dimensions
  RecoCoord initcoord;
  STD_map<recoDim,unsigned short> selmap;

  if(!create_selection_map(select, selmap, &initcoord)) return false;

/*  svector seltoks(tokens(select));
  for(unsigned int isel=0; isel<seltoks.size(); isel++) {
    svector toks(tokens(seltoks[isel],'='));
    if(toks.size()==2) {
      int dim=-1;
      for(int idim=0; idim<n_recoDims; idim++) {
        if(toks[0]==recoDimLabel[idim]) dim=idim;
      }
      if(dim>=0) {
        int sel=atoi(toks[1].c_str());
        initcoord.index[dim]=sel;
        initcoord.index[dim].set_mode(RecoIndex::single);
        selmap[recoDim(dim)]=sel;
      } else {
        ODINLOG(odinlog,errorLog) << "Unknwon selection dimension " << toks[0] << STD_endl;
        return false;
      }
    } else {
      ODINLOG(odinlog,errorLog) << "Error while parsing selection " << seltoks[isel] << STD_endl;
      return false;
    }
  }
*/


  // create intial numof (only for selected dimension for efficiency)
  STD_string printed_numof;
  if(!create_numof(initcoord, numof_cache, printed_numof)) return false;
  ODINLOG(odinlog,infoLog) << "numof: " << printed_numof << STD_endl;
  delete_countmap_cache(); // Free initial countmap which is requested only once




  // get recipe
  if(recipeFile!="") ::load(recipe,recipeFile);
  if(recipe=="") {
    STD_string recipe_seq=input->seqrecipe();
    if(recipe_seq!="") {
      recipe=recipe_seq;
    } else {
      if(!autorecipe(initcoord,recipe)) return false;
    }
  }


  // Apply selected dimensions to recipe
  for(STD_map<recoDim,unsigned short>::const_iterator it=selmap.begin(); it!=selmap.end(); ++ it) {
    recipe=STD_string("switch(")+recoDimLabel[it->first]+"="+itos(it->second)+"{"+recipe+"})";
  }


  // Create pipeline according to recipe
  ODINLOG(odinlog,infoLog) << "recipe=" << recipe << STD_endl;
  pipeline=create_pipeline(recipe,0,argc,argv);

  if(!pipeline) {
    ODINLOG(odinlog,errorLog) << "Unable to create pipeline" << STD_endl;
    return false;
  }


  // Print pipeline
  ODINLOG(odinlog,infoLog) << "Pipeline: " << STD_endl;
  RecoQueryContext context(RecoQueryContext::print, *this, numof_cache);
  ODINLOG(odinlog,normalDebug) << "printed_numof=" << printed_numof << STD_endl;
  context.printpostfix=printed_numof;
  if(!pipeline->query(context)) return false;


  // Finally, modify protocol according to recipe/pipeline
  if( !(slidingWindow || keyhole) ) {
    prot_cache.seqpars.set_RepetitionTime( prot_cache.seqpars.get_RepetitionTime() * numof_cache[cycle] );  // adjust TR according to numof cycles
  }


  return true;
}

//////////////////////////////////////////////////////////////////////////////////////


class RecoThread : public Thread, public Labeled {

 public:
  RecoThread() {}
  bool init(const STD_string& label, RecoController& controller) {
    set_label(label);
    pcontroller=&controller;
    return true;
  }

 private:

  void run() {
    while(pcontroller->feed_pipeline()) {}
    pcontroller->incr_waiting_threads("Waiting for end of reco");
  }

  RecoController* pcontroller;
};

//////////////////////////////////////////////////////////////////////////////////////

bool RecoController::dump_raw() const {
  Log<Reco> odinlog("RecoController","dump_raw");
  if(!input) return false;

  if(input->protocol().write(dumpRawPrefix+".pro")<0) return false;

  STD_string rawfile=dumpRawPrefix+".float";
  if(rmfile(rawfile.c_str())) {
    ODINLOG(odinlog,errorLog) << "Cannot remove old file " << rawfile << STD_endl;
    return false;
  }

  ODINLOG(odinlog,infoLog) << "writing data ..." << STD_endl;
  ComplexData<1> adc;
  RecoCoord coord;
  while(input->fetch(coord, adc)) {
    if(adc.write(rawfile, appendMode)) return false;
  }

  return true;
}


//////////////////////////////////////////////////////////////////////////////////////

bool RecoController::feed_pipeline() {
  Log<Reco> odinlog("RecoController","feed_pipeline");

  if(!status) return false; // Return immediately if an error occured

  if(!input) return false;


  RecoCoord coord;
  ComplexData<1> adc;

  inputmutex.lock();
  bool fetchresult=input->fetch(coord,adc);
  inputmutex.unlock();

  if(!fetchresult) return false;


  thread_depth[Thread::self()]++;

  // Final preparations for pipeline
  for(int idim=0; idim<n_recoDims; idim++) coord.index[idim].set_numof(numof_cache[idim]);
  coord.adcstart=0;
  coord.adcend=adc.size()-1;

  RecoData rd(coord);
  rd.data(Rank<1>()).reference(adc);

  rd.new_profiler(pipeline->label());
  ODINLOG(odinlog,normalDebug) << "feeding ADC " << coord.print() << STD_endl;
  bool result=pipeline->process(rd,*this);
  ODINLOG(odinlog,normalDebug) << "processed ADC " << coord.print() << STD_endl;
  if(!result) status=false;
  pmeter.increase_counter();

  thread_depth[Thread::self()]--;

  return result;
}


bool RecoController::execute_pipeline() {
  Log<Reco> odinlog("RecoController","execute_pipeline");
  Profiler prof("execute_pipeline");

  pmeter.new_task(input->get_coords().size(),"RecoController: Executing pipeline");

  RecoThread* threads=new RecoThread[jobs];

  status=true;

  for(int i=0; i<jobs; i++) threads[i].init("job"+itos(i),*this);
  for(int i=0; i<jobs; i++) threads[i].start(8*1024*1024); // 8 MB stack size
  for(int i=0; i<jobs; i++) threads[i].wait();

  delete[] threads;

  if(waiting_error!="") {
    ODINLOG(odinlog,errorLog) <<  waiting_error;
  }

  return status;
}


bool RecoController::prepare_pipeline() {
  Log<Reco> odinlog("RecoController","prepare_pipeline");
  Profiler prof("prepare_pipeline");

  // Loading and posting fieldmap
  if(fieldmapFile!="") {

    announce_data(posted_fmap_str);

    Data<float,3> fmap;
    if(fmap.autoread(fieldmapFile)<0) return false;
    ComplexData<3> cfmap(float2real(fmap));

//    int nslice=numof_cache[slice];
    bool phase3dmode=(numof_cache[line3d]>1);

    RecoData rdfmap;
    RecoFieldMapUser::modify4fieldmap(rdfmap.coord());
    ComplexData<3>& fmapdata=rdfmap.data(Rank<3>());

    if(phase3dmode) {
      ODINLOG(odinlog,infoLog) << "Posting 3D fieldmap"<< STD_endl;

      fmapdata.reference(cfmap);
      post_data(posted_fmap_str, rdfmap);

    } else {  // assume multi-slice mode

      ODINLOG(odinlog,infoLog) << "Posting fieldmap in multi-slice mode with nslice=" << fmap.extent(0) << STD_endl;

      Range all=Range::all();

      fmapdata.resize(1,fmap.extent(1),fmap.extent(2));

      for(int islice=0; islice<fmap.extent(0); islice++) {
        rdfmap.coord().index[slice]=islice;
        fmapdata(0,all,all)=cfmap(islice,all,all);
        post_data(posted_fmap_str, rdfmap);
      }

    }

  }


  // Loading and posting field drift
/*
  if(driftcorrFile!="") {

    announce_data(posted_drift_str);

    Data<float,1> drift;
    if(drift.autoread(driftcorrFile)<0) return false;
    ComplexData<1> cdrift(float2real(drift));

    RecoData rdfdrift;
    rdfdrift.coord().any();
    rdfdrift.data(Rank<1>()).reference(cdrift);

    post_data(posted_drift_str, rdfdrift);
  }
*/


  ODINLOG(odinlog,infoLog) << "Preparing pipeline ..." << STD_endl;
  RecoQueryContext context(RecoQueryContext::prep, *this, numof_cache);
  if(!pipeline->query(context)) return false;

  delete_countmap_cache(); // free memory

  return true;
}


bool RecoController::finalize_pipeline() {
  Log<Reco> odinlog("RecoController","finalize_pipeline");
  Profiler prof("finalize_pipeline");

  ODINLOG(odinlog,infoLog) << "Finishing off ..." << STD_endl;
  RecoQueryContext context(RecoQueryContext::finalize, *this, numof_cache);
  if(!pipeline->query(context)) return false;
  return true;
}


bool RecoController::start() {
  Log<Reco> odinlog("RecoController","start");

  if(dumpRawPrefix!="") return true; // Donot reconstruct images

  if(!prepare_pipeline()) return false;

  if(!execute_pipeline()) return false;

  if(!finalize_pipeline()) return false;

  return true;
}


///////////////////////////////////////

int RecoController::controlthread;
