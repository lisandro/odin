/***************************************************************************
                          collector.h  -  description
                             -------------------
    begin                : Sun Jan 14 2007
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef RECOCOLLECTOR_H
#define RECOCOLLECTOR_H


#include "step.h"

/**
  * @addtogroup odinreco
  * @{
  */




/**
  * Base class for all collector templates to hold code common to all collectors.
  */
class RecoCollectorBase : public RecoStep {

 protected:

  RecoCollectorBase() {}
  ~RecoCollectorBase();


/**
  * Removes 'coord' from the todo map, returns 'true' only if the
  * current set is fully collected, i.e. the todo map is emtpy.
  * Note: Do not lock any mutexes when calling this function.
  * If 'custom_count' is non-zero, it will be used instead
  * of the cached 'count'.
  */
  bool completed(const RecoCoord& coord, unsigned int custom_count);

/**
  * Calcuate the count, i.e. number inputs to be processed before executing the next step.
  * 'inmask' and 'outmask' are used to filter on particular indices when calculating the todo map.
  */
  bool calculate_count(RecoQueryContext& context, const RecoCoord& inmask, const RecoCoord& outmask);


 private:

  CoordCountMap todo; // coordinate always refers to the output of functor, i.e. after applying 'modify_coord'
  Mutex todomutex;

  unsigned int count; // initialized in query/calculate_count, assumed to be the same for all coordinates

  STD_string label_cache; // label() might not be available in destructor
};




/////////////////////////////////////////////////////////


/*
  * Template class to collect data in one or more dimensions.
  * The rank of the input data is given by 'Nin'. The particular input dimension are given
  * in 'In' by a specialization of 'RecoDim'. The numer of dimensions to collect data
  * for is given in 'Ncoll' and specified in 'Coll' by a specialization of 'RecoDim'.
  * The resulting data array will have the collected dimensions leftmost, i.e.
  * if In=i1,i2 and Coll=c1, the resulting array will have dimensions (c1,i1,i2)
  * If 'use_numof' is set to 'true', the numof of the collected indices will be used
  * for the count instead of using a count map.
  */
template<unsigned int Nin, class In, unsigned int Ncoll, class Coll, bool use_numof=false>
class RecoCollector : public RecoCollectorBase, public Labeled {

 public:
  RecoCollector(const STD_string& coll_label) : Labeled(coll_label) {}

 private:

  // implementing virtual functions of RecoStep
  STD_string label() const {return get_label();}
  STD_string description() const {return "Collect data in one or more dimensions";}
  bool process(RecoData& rd, RecoController& controller);
  RecoCoord input_coord() const {return In::preset_coord(RecoIndex::collected);}
  void modify_coord(RecoCoord& coord) const {Coll::modify(RecoIndex::collected,coord);}
  bool query(RecoQueryContext& context);
  RecoStep* allocate() const {return new RecoCollector(get_label());}
  void init() {}


  typedef STD_map<RecoCoord, ComplexData<Nin+Ncoll> > DataMap;
  DataMap datamap; // the data collected
  Mutex mutex;

};


/** @}
  */

#endif

