/***************************************************************************
                          switch.h  -  description
                             -------------------
    begin                : Mon Jan 22 2007
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef RECOSWITCH_H
#define RECOSWITCH_H

#include "step.h"


class RecoSwitch : public RecoStep {

  // implementing virtual functions of RecoStep
  STD_string label() const {return "switch";}
  STD_string description() const {return "Branch off different flows through reconstruction pipeline";}
  bool process(RecoData& rd, RecoController& controller);
  RecoCoord input_coord() const {return RecoCoord::any();}
  void modify_coord(RecoCoord& coord) const {} // irrelevant since pipeline ends here
  RecoStep* allocate() const {return new RecoSwitch;}
  bool pipeline_init(const RecoController& controller, const RecoCoord& input_coord);
  bool query(RecoQueryContext& context);
  void init();


  LDRstring brancharg;


  struct BranchKey {
//    BranchKey(recoDim branchdim, unsigned int branchindex) : dim(branchdim), val(branchindex) {} // does not work with GCC 3.5

    recoDim dim;
    unsigned short val;

    bool operator < (const BranchKey& bs) const {
      if(dim!=bs.dim) return (dim<bs.dim); // dim has highest priority
      return (val<bs.val);
    }
  };


  struct BranchValue {
    RecoStep* step;
    unsigned short numof[n_recoDims];
  };


  typedef STD_map<BranchKey,BranchValue> BranchMap;
  BranchMap branches;

  STD_list<RecoStep*> unconditional;
};


#endif

