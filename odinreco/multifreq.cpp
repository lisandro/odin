#include "multifreq.h"
#include "data.h"
#include "controller.h"


static const char* posted_freqs_str="multifreqs";



/////////////////////////////////////////////////////////////////////////////////////////////////////////


void RecoMultiFreq::init() {

  max_numof_freq=256;
  max_numof_freq.set_cmdline_option("mf").set_description("Maximum number of frequencies for multi-frequency reconstruction");
  append_arg(max_numof_freq,"max_numof_freq");
}


bool RecoMultiFreq::get_freqs(Data<float,1>& result, const RecoCoord& fmapcoord, double adcdur, RecoController& controller) {
  Log<Reco> odinlog(c_label(),"get_freqs");

  bool calc_freqs=true;
  freqmutex.lock();
  FreqMap::const_iterator it=freqmap.find(fmapcoord);
  if(it!=freqmap.end()) {
    result.reference(it->second.copy()); // deep copy for thread-safe access when using freqs
    calc_freqs=false;
  }
  freqmutex.unlock();

  if(calc_freqs) {

    if(!controller.data_announced(posted_fmap_str)) {
      ODINLOG(odinlog,errorLog) << "fieldmap not available" << STD_endl;
      return false;
    }

    RecoData rdfieldmap(fmapcoord);
    if(!controller.inquire_data(posted_fmap_str, rdfieldmap)) return false;

    Data<float,3> fieldmap(creal(rdfieldmap.data(Rank<3>())));
    float minfreq=min(fieldmap);
    float maxfreq=max(fieldmap);

    float deltafreq=fabs(maxfreq-minfreq);

    stampmutex.lock();
    double echoTrainDur=echoTimeStamps.maxvalue()-echoTimeStamps.minvalue();
    stampmutex.unlock();
    echoTrainDur+=adcdur;

    ODINLOG(odinlog,normalDebug) << "deltafreq/adcdur/echoTrainDur=" << deltafreq << "/" << adcdur << "/" << echoTrainDur << STD_endl;

    int nFreq=int(4.0*deltafreq*echoTrainDur/PII+0.5); // According to Man et al., MRM 37:785(1997)
    nFreq*=2; // slightly more to avoid Moire-type artifacts
    if(nFreq>max_numof_freq) nFreq=max_numof_freq;
    if(nFreq<=0) nFreq=1;
    fvector fv(nFreq);
    if(nFreq>1) fv.fill_linear(minfreq,maxfreq);
    else fv=0.0; // use zero frequency if multi-frequency interpolation is disabled
    result=fv;
    ODINLOG(odinlog,normalDebug) << "nFreq/result=" << nFreq << "/" << fv.printbody() << STD_endl;

    MutexLock lock(freqmutex);
    if(freqmap.find(fmapcoord)==freqmap.end()) { // Do this only once, otherwise post_data() will throw an error

      freqmap[fmapcoord].reference(result); // Cache result

      // post freqs to blackboard for later use by freqcomb
      RecoData rdposted(fmapcoord);
      rdposted.data(Rank<1>()).resize(nFreq);
      rdposted.data(Rank<1>())=float2real(result);
      controller.post_data(posted_freqs_str, rdposted);
    }

  }

  return true;
}


bool RecoMultiFreq::process(RecoData& rd, RecoController& controller) {
  Log<Reco> odinlog(c_label(),"process");

  ComplexData<1>& indata=rd.data(Rank<1>());
  unsigned int adcSize=indata.extent(0);
  double dt=rd.coord().dwellTime;

  // coord for indexing field maps
  RecoCoord fmapcoord=rd.coord();
  RecoFieldMapUser::modify4fieldmap(fmapcoord);

  Data<float,1> freqs;
  if(!get_freqs(freqs,fmapcoord,double(adcSize)*dt,controller)) return false;

  double reftime=0.0; // The absolute time within an echo train
  unsigned int iecho=rd.coord().echopos;
  unsigned int iepi=rd.coord().echotrain;
  stampmutex.lock();
  if(echoTimeStamps.size(0)<2) iepi=0; // do not throw error if EPI start times are not provided (e.g. TWIX data), use zero instead
  if(iepi<echoTimeStamps.size(0) && iecho<echoTimeStamps.size(1)) {
    reftime=echoTimeStamps(iepi,iecho);
  } else {
    ODINLOG(odinlog,errorLog) << "iepi/iecho(" << iepi << "/" << iecho << ") out of range(" << echoTimeStamps.get_extent() << ")" << STD_endl;
    stampmutex.unlock();
    return false;
  }
  stampmutex.unlock();
  ODINLOG(odinlog,normalDebug) << "reftime(" << iepi << "/" << iecho << ")=" << reftime << STD_endl;

  int nfreq=freqs.size();
  ODINLOG(odinlog,normalDebug) << "nfreq/adcSize=" << nfreq << "/" << adcSize << STD_endl;


  // modulate offset for each frequency offset and feed it into rest of pipeline
  for(int ifreq=0; ifreq<nfreq; ifreq++) {
    RecoData rdcopy(rd.coord()); // start with empty data
    ComplexData<1>& modulated=rdcopy.data(Rank<1>());
    modulated.resize(adcSize);
    rdcopy.coord().index[freq].set_numof(nfreq);
    rdcopy.coord().index[freq]=ifreq;
    for(unsigned int isample=0; isample<adcSize; isample++) {

      double time=reftime+double(isample)*dt;
      double imag=-time*freqs(ifreq);
      STD_complex phasefactor=expc(float2imag(imag));
      modulated(isample)=phasefactor*indata(isample);
    }

    if(!execute_next_step(rdcopy,controller)) return false;
  }
  return true;
}



bool RecoMultiFreq::query(RecoQueryContext& context) {
  Log<Reco> odinlog(c_label(),"query");

  if(context.mode==RecoQueryContext::prep) {

    dvector echostart=context.controller.dim_values(echo);
    if(!echostart.size()) {
      echostart.resize(1);
      echostart=0.0;
    }
    ODINLOG(odinlog,normalDebug) << "echostart=" << echostart << STD_endl;

    dvector epistart=context.controller.dim_values(epi);
    if(!epistart.size()) {
      epistart.resize(1);
      epistart=0.0;
    }
    ODINLOG(odinlog,normalDebug) << "epistart=" << epistart << STD_endl;

    echoTimeStamps.redim(epistart.size(), echostart.size());
    for(unsigned int iepi=0; iepi<epistart.size(); iepi++) {
      for(unsigned int iecho=0; iecho<echostart.size(); iecho++) {
        echoTimeStamps(iepi,iecho)=epistart[iepi]+echostart[iecho];
      }
    }
    ODINLOG(odinlog,normalDebug) << "echoTimeStamps=" << echoTimeStamps << STD_endl;

    context.controller.announce_data(posted_freqs_str);
  }

  return RecoStep::query(context);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////


bool RecoFreqComb::get_freqs(Data<float,1>& result, const RecoCoord& fmapcoord, RecoController& controller) {
  Log<Reco> odinlog(c_label(),"get_freqs");

  if(!controller.data_announced(posted_freqs_str)) {
    ODINLOG(odinlog,errorLog) << "multifreqs not available" << STD_endl;
    return false;
  }

  RecoData rdfreqs(fmapcoord);
  if(!controller.inquire_data(posted_freqs_str, rdfreqs)) return false;

  ComplexData<1>& cfreqs=rdfreqs.data(Rank<1>());
  result.resize(cfreqs.shape());
  result=creal(cfreqs);

  return true;
}



bool RecoFreqComb::process(RecoData& rd, RecoController& controller) {
  Log<Reco> odinlog(c_label(),"process");

  Range all=Range::all();

  const ComplexData<4>& allfreq=rd.data(Rank<4>());

  int nfreq=allfreq.extent(0);

  TinyVector<int,3> outshape(allfreq.extent(1), allfreq.extent(2), allfreq.extent(3));
  ComplexData<3>& outdata=rd.data(Rank<3>());
  outdata.resize(outshape);

  RecoCoord fmapcoord=rd.coord();
  RecoFieldMapUser::modify4fieldmap(fmapcoord);

  Data<float,1> freqs;
  if(!get_freqs(freqs, fmapcoord, controller)) return false;
  if(nfreq!=int(freqs.size())) {
    ODINLOG(odinlog,errorLog) << "nfreq=" << nfreq << "!=" << freqs.size() << STD_endl;
    return false;
  }

  Data<float,3> fieldmap;
  if(rd.coord().index[cycle].get_mode()==RecoIndex::separate) { // for separate PROPELLER blades
    fmapcoord.index[cycle].set_mode(RecoIndex::separate);
    fmapcoord.index[cycle]=rd.coord().index[cycle];
  }
  if(!get_fmap(fieldmap, outshape, fmapcoord, controller)) return false;
//  fieldmap.autowrite("fieldmap_"+fmapcoord.print(RecoIndex::filename)+".nii");

  // Combine frequency values
  if(nfreq>1) {
    outdata=STD_complex(0.0);
    double deltafreq=fabs(freqs(1)-freqs(0));
    for(int iphase3d=0; iphase3d<outshape(0); iphase3d++) {
      for(int iphase=0; iphase<outshape(1); iphase++) {
        for(int iread=0; iread<outshape(2); iread++) {

          float fieldmap_freqoffset=fieldmap(iphase3d,iphase,iread);
          if(fieldmap_freqoffset!=0.0) { // Assume non-existent fieldmap value if exactly zero -> Pixel is zero in reconstructed image

            float norm=0.0;
            STD_complex pixsum(0.0);
            for(int ifreq=0; ifreq<nfreq; ifreq++) {
              float relfreqdiff=secureDivision( freqs(ifreq)-fieldmap_freqoffset , deltafreq );
              float argument=-4.0*relfreqdiff*relfreqdiff; // ~1 pixel kernel
              float factor=exp(argument);
              pixsum+=STD_complex(factor)*allfreq(ifreq,iphase3d,iphase,iread);
              norm+=factor;
            }
            if(norm) outdata(iphase3d,iphase,iread)=pixsum/STD_complex(norm);
          }
        }
      }
    }
  } else {
    outdata=allfreq(0,all,all,all);
  }

  return execute_next_step(rd,controller);
}


