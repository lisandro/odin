#include "filter.h"
#include "data.h"
#include "controller.h"


void RecoFilter::init() {

  filter.set_funcpars("NoFilter");
  filter.set_cmdline_option("ff").set_description("k-space filter function (and its arguments)");
  append_arg(filter,"filter");

  plateau=0.0;
  plateau.set_cmdline_option("fp").set_description("Filter plateau width (0=no plateau, 1=full plateau)");
  append_arg(plateau,"plateau");
}

bool RecoFilter::process(RecoData& rd, RecoController& controller) {
  Log<Reco> odinlog(c_label(),"process");

  ODINLOG(odinlog,normalDebug) << "filter/plateau=" << filter.get_function_name() << "/" << plateau << STD_endl;

  if(filter.get_function_name()!="NoFilter") {

    ComplexData<3>& data=rd.data(Rank<3>());
    TinyVector<int,3> shape=data.shape();
    ODINLOG(odinlog,normalDebug) << "shape=" << shape << STD_endl;

    float plat=plateau;
    check_range<float>(plat, 0.0, 1.0);
    float plateau_factor=secureInv( 1.0-plat);

    for(unsigned int i=0; i<data.size(); i++) {
      TinyVector<int,3> index=data.create_index(i);

      float factor=1.0;
      for(int idir=0; idir<3; idir++) {
        if(shape(idir)>1) {
          float relradius=2.0*secureDivision(abs(index(idir)-shape(idir)/2),shape(idir));
          if(relradius>plat) {
            relradius=plateau_factor*(relradius-plateau);
            if(relradius<=1.0) factor*=filter.calculate(relradius);
            else factor=0.0;
          }
        }
      }

      data(index)*=STD_complex(factor);
    }
  }

  return execute_next_step(rd, controller);
}
