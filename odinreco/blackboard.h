/***************************************************************************
                          blackboard.h  -  description
                             -------------------
    begin                : Mon Jan 24 2007
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef RECOBLACKBOARD_H
#define RECOBLACKBOARD_H

#include <odinreco/odinreco.h>
#include <odinreco/data.h>
#include <odinreco/step.h>

/**
  * @addtogroup odinreco
  * @{
  */



/**
  * Manages data transfer between functors
  */
class RecoBlackBoard {

 public:

/**
  * Constructs empty blackboard
  */
  RecoBlackBoard() {}

  ~RecoBlackBoard();

/**
  * Announces that data with identifier 'label' will become available during pipeline execution.
  */
  void announce(const STD_string& label) {ann[label]++;}

/**
  * Posts 'data' with identifier 'label', it will be indexed according to the k-space coordinate in 'data'.
  */
  void post(const STD_string& label, const RecoData& data);

/**
  * Returns true only if data with identifier 'label' will become available during pipeline execution.
  */
  bool announced(const STD_string& label) const {return ann.find(label)!=ann.end();}

/**
  * Returns 'data' with identifier 'label' for k-space coordinate set in 'data'.
  * If 'blocking' is 'true', the function will return only if data becomes available.
  * Returns 'true' if data is available, otherwise returns 'false' if data not yet available.
  */
  bool inquire(const STD_string& label, RecoData& data, bool blocking=false);


 private:

  bool get_data(const STD_string& label, RecoData& data);

  // The data on the blackboard
  typedef STD_map<RecoCoord, RecoData> CoordDataMap;
  typedef STD_map<STD_string,CoordDataMap> LabelDataMap;
  LabelDataMap posted;

  // The events to signal the availability of new data
  typedef STD_map<RecoCoord, Event*> CoordEventMap;
  typedef STD_map<STD_string,CoordEventMap> LabelEventMap;
  LabelEventMap eventmap;

  Mutex mutex; // Mutex for both maps

  STD_map<STD_string,UInt> ann;

};


/** @}
  */

///////////////////////////////////////////////////////////////////////////////////////////


class RecoPost : public RecoStep {

  // implementing virtual functions of RecoStep
  STD_string label() const {return "post";}
  STD_string description() const {return "Post data on blackboard";}
  bool process(RecoData& rd, RecoController& controller);
  RecoCoord input_coord() const {return RecoCoord::any();}
  void modify_coord(RecoCoord& coord) const {}
  bool query(RecoQueryContext& context);
  RecoStep* allocate() const {return new RecoPost;}
  void init();

  LDRstring postlabel;

};



#endif

