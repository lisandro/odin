/***************************************************************************
                          odinreco.h  -  description
                             -------------------
    begin                : Sat Dec 30 2006
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef ODINRECO_H
#define ODINRECO_H

#include <odinpara/reco.h>
#include <odindata/complexdata.h>


/**
  * \page odinreco_usage Automatic image reconstruction for ODIN sequences
  *
  * For image reconstruction of ODIN raw data,
  * execute the program 'odinreco' within the directory of the file 'recoInfo' (the scan directory),
  * or use the option '-r' to specify the location of 'recoInfo'. For example, the commands
  * \verbatim
    odinreco -r /path/to/raw/data/recoInfo -o  /path/to/raw/data/image
    \endverbatim
  * and
  * \verbatim
    cd /path/to/raw/data/recoInfo
    odinreco
    \endverbatim
  * will do the same, i.e. create an image using the raw data in '/path/to/raw/data/'.
  *
  * For other formats, e.g. the ISMRM Raw Data Format (ISMRMRD) or Siemens raw data,
  * specify the raw data file using the option '-r':
  * \verbatim
    odinreco -r simple_gre.h5
    \endverbatim
  *
  * Full description:
  * \verbinclude odinreco/odinreco.usage
  *
  */

/**
  * \page odinreco_doc ODIN image reconstruction framework (odinreco)
  *
  * This page describes the usage and design guidelines of the module \ref odinreco.
  * For a description how to execute the reconstruction, please refer to \ref odinreco_usage.
  *
  * \section process The reconstruction process
  * Here, image reconstruction refers to converting MR raw data to an image.
  * It is achieved by the following processing strategy:
  * Each single acquisition (ADC) is fed separately into the reconstruction pipeline.
  * The pipeline consists of a chain of steps(functors) which are executed
  * subsequently in order to process the input data with the reconstructed image
  * as the final output.
  * ADCs are processed simultaneously in different threads allowing acceleration
  * of reconstruction on multi-processor systems.
  * It is the responsibility of the steps to synchronize properly
  * if they possess global (static) data.
  *
  *
  * \section pipeline Setting up a reconstruction pipeline
  * A reconstruction pipeline is described by a string.
  * This string consists of single tokens representing a step
  * which can be any of those described in
  * \ref odinreco_steps.
  * These steps are chained together using the pipe operator (|).
  * Only steps with a compatible input and output interface
  * may be chained together.
  * A step may accept additional arguments enclosed in parenthesis
  * after the step label.
  * In addition, a step may have command-line options to fine-tune
  * its behaviour.
  * Please refer to the manual at (\ref odinreco_steps) for the 
  * input/output interface, arguments and options of each step.
  *
  * As an example, a simple 2D/3D reconstruction would look like this:
  * \verbatim
    kspace | filter(Gauss) | fft | offset | slicecoll | image | store(result)
    \endverbatim
  * This would collect k-space data (kspace), apply a Gaussian filter (filter),
  * perform an FFT if done (fft) and shift the image (offset).
  * Finally, after collecting slices and repetitions (slicecoll | image),
  * it would store the data in files starting with the prefix 'result'.
  *
  *
  * \section blackboard Blackboard
  * Some steps need data produced by another step in another branch
  * of the pipeline (e.g. phasemap, fieldmap). To pass this data around,
  * a thread-safe blackboard system is used to post data and retrieve it later.
  * In particular, the 'post' step (RecoPost) can used to post data and the function
  * RecoController::inquire_data() can be used to retrieve this data
  * in another step.
  *
  * \section options Reconstruction Options
  * Each step can have a number of options (i.e. parameters) to customize
  * its behavior. For instance, the step for zero filling accepts the
  * zero-padded sizes of the destination grid as its arguments.
  * For a list of available options of each step, please refer to
  * \ref odinreco_steps.
  * Options can be set in different ways. With ascending priority,
  * these are:
  * - Command line options as set in the sequence via recoInfo->set_CmdLineOpts(...);
  * - Command line options a set directly on the command line.
  * - By a comma-separated list in parenthesis following the step
  *   string in the recipe, e.g. "... | zerofill(1,256,256) | ..."
  */

/**
  * @addtogroup odinreco
  * @{
  */

////////////////////////////////////////////////////////////////

// helper class used for debugging the odinreco component
class Reco {
 public:
  static const char* get_compName();
};

////////////////////////////////////////////////////////////////

/**
  * Unsigned integer which initializes to zero
  */
struct UInt {

  UInt(unsigned int v=0) : val(v) {}

  UInt& operator = (unsigned int v) {val=v; return *this;}
  operator unsigned int () const {return val;}

  unsigned int operator += (unsigned int rhsval) {val+=rhsval; return *this;}
  unsigned int operator -= (unsigned int rhsval) {val-=rhsval; return *this;}
  unsigned int operator *= (unsigned int rhsval) {val*=rhsval; return *this;}
  unsigned int operator /= (unsigned int rhsval) {val/=rhsval; return *this;}
  unsigned int operator ++ ()    {val=val+1; return val;}            // prefix
  unsigned int operator ++ (int) {unsigned int tmp=val; val=val+1; return tmp;} // postfix
  unsigned int operator -- ()    {val=val-1; return val;}            // prefix
  unsigned int operator -- (int) {unsigned int tmp=val; val=val-1; return tmp;} // postfix

 private:
  unsigned int val;
};


/** @}
  */
#endif

