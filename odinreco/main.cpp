#include "controller.h"
#include "reader_odin.h"
#include "reader_custom.h"
#include "reader_ismrmrd.h"

////////////////////////////////////////////////////

// Quick hack to include reader for Twix data
#ifdef IDEA_PLUGIN
#include "../platforms/IDEA_n4/twix/reader_twix.cpp"
#endif


////////////////////////////////////////////////////


void usage(LDRblock& opts) {

  STD_string doctxt=
    "odinreco provides a generic "
    "reconstruction for imaging sequences (spin-warp, EPI, RARE, ...). "
    "It retrieves the information about the acquired signal from the "
    "file 'recoInfo' which is stored on disk together with the raw data. "
    "The recoInfo file contains information about the "
    "k-space position of each acquisition windows (ADC). "
    "As output, odinreco generates a bunch of files with different "
    "image formats (depending on the option -f and third-party libraries available to ODIN). "
    "The filename of each file starts with the given output prefix. ";

  STD_cout << STD_endl;
  STD_cout << "odinreco: Automatic reconstruction for ODIN sequences" <<  STD_endl;
  STD_cout << STD_endl;
  STD_cout << justificate(doctxt);
  STD_cout << "Usage: odinreco [ options ] -r <input-file-to-be-read>" <<  STD_endl;
  STD_cout << "Options:" <<  STD_endl;
  STD_cout << opts.get_cmdline_usage("\t");
  STD_cout << "\t" << LogBase::get_usage() <<  STD_endl;
  STD_cout << "\t" << helpUsage() << STD_endl;
}



int main(int argc,char* argv[]) {
  if(LogBase::set_log_levels(argc,argv)) return 0;

  Log<Reco> odinlog("","main");

  LDRblock opts;
  RecoController controller(opts);
  RecoReaderOdin reader_odin(opts);
  RecoReaderCustom reader_custom(opts);
#ifdef IDEA_PLUGIN
  RecoReaderTwix reader_twix(opts);
#endif
#ifdef ISMRMRDSUPPORT
  RecoReaderISMRMRD reader_ismrm(opts);
#endif

  if(isCommandlineOption(argc,argv,"-manual")){
    STD_cout << controller.stepmanual() << STD_endl;
    return 0;
  }

  if(hasHelpOption(argc,argv)) {usage(opts);return 0;}


  // Select reader by autodetection
  RecoReaderInterface* reader=0;
  char optval[ODIN_MAXCHAR];
  LDRfileName input_filename=STD_string("./recoInfo"); // default is odin data
  if(getCommandlineOption(argc,argv,"-r",optval,ODIN_MAXCHAR)) input_filename=optval;
  if(input_filename.get_basename()=="recoInfo") reader=&reader_odin;
  if(input_filename.get_suffix()=="smp") reader=&reader_custom;
#ifdef IDEA_PLUGIN
  if(input_filename.get_suffix()=="dat") reader=&reader_twix;
#endif
#ifdef ISMRMRDSUPPORT
  if(input_filename.get_suffix()=="h5") reader=&reader_ismrm;
#endif
  if(!reader) {
    ODINLOG(odinlog,errorLog) << "Unable to initialize reader for input file " << input_filename << STD_endl;
    return -1;
  }
  opts.parse_cmdline_options(argc,argv,false); // Parse reader options


  if(!reader->init(input_filename)) {
    ODINLOG(odinlog,errorLog) << "reader->init(" << input_filename << ") failed" << STD_endl;
    return -1;
  }

  if(!controller.init(reader,opts,argc,argv)) {
    ODINLOG(odinlog,errorLog) << "controller.init(...) failed" << STD_endl;
    return -1;
  }

  if(!controller.start()) {
    ODINLOG(odinlog,errorLog) << "controller.start() failed" << STD_endl;
    return -1;
  }

  Profiler::dump_final_result();

  return 0;
}
