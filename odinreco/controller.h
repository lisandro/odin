/***************************************************************************
                          controller.h  -  description
                             -------------------
    begin                : Sat Dec 30 2006
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef RECOCONTROLLER_H
#define RECOCONTROLLER_H

#include <tjutils/tjthread.h>
#include <tjutils/tjfeedback.h>

#include <odinreco/odinreco.h>
#include <odinreco/blackboard.h>
#include <odinreco/step.h>



/**
  * @addtogroup odinreco
  * @{
  */




class RecoReaderInterface; // forward declaration
class RecoStep; // forward declaration



////////////////////////////////////////////////////////////////


/**
  * Class to manage reconstruction process.
  */
class RecoController {

 public:

/**
  * Creates uninitialized reconstruction controller.  Appends parameters to 'parblock'.
  */
  RecoController(LDRblock& parblock);

  ~RecoController();

/**
  * Initializes reconstruction process controller to use reco input 'reader'
  * and option block 'opts'. Returns 'true' only if sucessful.
  */
  bool init(RecoReaderInterface* reader, LDRblock& opts, int argc, char* argv[]);

/**
  * Starts reconstruction process using 'firststep' as the start of the
  * reconstruction pipeline.
  * Returns 'true' only if sucessful.
  */
  bool start();

/**
  * Returns vector of values associated with a certain dimension.
  */
  dvector dim_values(recoDim dim) const;

/**
  * Returns the relative spatial offset the image should be shifted after reconstruction.
  * The result contains the offsets in (phase3d,phase,read) direction.
  */
  TinyVector<float,3> reloffset() const;

/**
  * Extra command-line arguments for processing of final images.
  */
  STD_string image_proc() const;

/**
  * Returns the image size, will be used if it cannot be determined from the k-space
  * coordinates (i.e. when gridding).
  */
  TinyVector<int,3> image_size() const;

/**
  * Returns the extent of the k-space in each direction (in rad/mm)
  */
  TinyVector<float,3> kspace_extent() const;

/**
  * Returns the scan protocol.
  */
  const Protocol& protocol() const {return prot_cache;}


/**
  * Announces to blackboard that data with identifier 'label' will become available during pipeline execution.
  */
  void announce_data(const STD_string& label) {blackboard.announce(label);}

/**
  * Posts 'data' with identifier 'label' on blackboard, it will be indexed according to the k-space coordinate in 'data'.
  */
  void post_data(const STD_string& label, const RecoData& data) {blackboard.post(label,data);}

/**
  * Returns whether data with 'label' will become available during pipeline execution.
  */
  bool data_announced(const STD_string& label) {return blackboard.announced(label);}

/**
  * Returns 'data' with identifier 'label' for k-space coordinate set in 'data' from blackboard.
  * Waits until data becomes available. Returns 'true' if data is available, otherwise returns 'false' if all data was processed
  * and the requested data was not available.
  */
  bool inquire_data(const STD_string& label, RecoData& data);


/**
  * Returns a map where the key are k-space coordinates
  * (except those not in 'mask') and the value of the map is a count
  * of ADC indices with this particular k-space coordinate.
  */
  const CoordCountMap* create_count_map(const RecoCoord& mask) const;

/**
  * Creates the maximum extent of reco dimension in 'numof'
  * using all coordinates except those not in 'mask'.
  * In addition, returns a printed version of the result in 'printed'.
  */
  bool create_numof(const RecoCoord& mask, unsigned short numof[n_recoDims], STD_string& printed) const;


/**
  * Creates a pipeline and returns its 1st functor according to the recipe (pieline layout) given in 'rec'.
  * Checking of input/output dimension match starts with 'initial_coord', if non-zero.
  * Returns zero if the pipeline could not be created because of input/output dimension mismatches.
  */
  RecoStep* create_pipeline(const STD_string& rec, const RecoCoord* initial_coord=0, int argc=0, char* argv[]=0) const;


/**
  * Returns documention 'code' of steps for doxygen.
  */
  STD_string stepmanual() const;


 private:
  friend class RecoThread;


  // Options
  LDRint jobs;
  LDRstring recipe;
  LDRstring recipeFile;
  LDRstring select;
  LDRint    timeout;
  LDRint    maxrecursion;
  LDRbool   conjPhaseFT;
  LDRbool   slidingWindow;
  LDRbool   keyhole;
  LDRbool   sepChannels;
  LDRbool   dumpKspace;
  LDRbool   dumpGrappaTemplate;
  LDRbool   disablePhaseCorr;
  LDRbool   disableHomodyne;
  LDRbool   disableMultiFreq;
  LDRbool   disableDriftCorr;
  LDRbool   disableSliceTime;
  LDRbool   disableGrappa;
  LDRbool   phaseCourse;
  LDRbool   qcspike;
  LDRstring preproc3d;
  LDRstring postproc3d;
  LDRstring dumpRawPrefix;
  LDRstring fieldmapFile;
//  LDRstring driftcorrFile;
  LDRstring driftcorrSel;

  bool execute_pipeline();
  bool prepare_pipeline();
  bool finalize_pipeline();

  bool feed_pipeline();

  bool dump_raw() const;

  static void tracefunction(const LogMessage& msg);
  static int controlthread;

  template<recoDim Dim, recoDim OrthoDim>
  bool get_kspace_sampling_pattern(const RecoCoord& mask, unsigned int& reduction_factor, bool& partial_fourier, bool& half_fourier) const;

  bool autorecipe(const RecoCoord& mask, STD_string& result) const;

  bool create_selection_map(const STD_string& selstr, STD_map<recoDim,unsigned short>& selmap, RecoCoord* initcoord=0) const;

  typedef STD_map<int, STD_string> WaitingMap;
  WaitingMap waiting;
  STD_string waiting_error;
  Mutex waiting_mutex;

  void incr_waiting_threads(const STD_string& condition) {MutexLock lock(waiting_mutex); waiting[Thread::self()]=condition;}
  void decr_waiting_threads() {MutexLock lock(waiting_mutex); waiting.erase(waiting.find(Thread::self()));}
  unsigned int numof_waiting_threads() {MutexLock lock(waiting_mutex); return waiting.size();}

  STD_map<int, UInt> thread_depth; // trace the depth (recursion level) of each thread

  RecoReaderInterface* input;
  mutable Mutex inputmutex;

  RecoStep* pipeline;
  RecoStepFactory* factory;

  RecoBlackBoard blackboard;

  ProgressDisplayConsole display; // Create display before pmeter
  ProgressMeter pmeter; // ProgressMeter is thread-safe


  unsigned short numof_cache[n_recoDims];
  mutable bool has_readoutshape;
  mutable unsigned char has_flag_cache;
  mutable bool has_templcorr_cache[n_templateTypes];
  mutable bool has_navigator_cache[n_navigatorTypes];
  mutable bool has_traj;
  mutable bool has_oversampling_cache;
  mutable bool has_relcenter_cache;

  Protocol prot_cache;

  volatile bool status; // Although it is used in different threads, we will assume that reads/writes to bool are atomic

  mutable STD_map<STD_string,const CoordCountMap*> countmap_cache; // cache countmap since the same countmap is calculated several times in create_count_map()
  void delete_countmap_cache();

};



/** @}
  */


#endif

