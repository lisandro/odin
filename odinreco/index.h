/***************************************************************************
                          index.h  -  description
                             -------------------
    begin                : Mon Jan 27 2007
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef RECOINDEX_H
#define RECOINDEX_H

#include <odinreco/odinreco.h>

#include <odindata/gridding.h>

/**
  * @addtogroup odinreco
  * @{
  */



/**
  * Manages information of data in one dimension.
  */
class RecoIndex {

 public:

/**
  * Mode the index is used for:
  * - separate  : Dimensions are processed separately.
  * - ignore    : Same as 'separate', but the index is ignored when comparing indices.
  * - single    : In a previous processing step, a single index was selected (or reduction to a single index), as an effect, this index is always zero.
                  (Since only one value is possible, it is ignored when sorting indices (operator <))
  * - collected : Data from all inidices is combined and transfered simultaneously.
  * - any       : Any of the above (for checking functor interfaces).
  */
  enum indexMode {separate=0, ignore, single, collected, any};


/**
  * Constructs default index object.
  */
  RecoIndex(unsigned short v=0) : mode(separate), val(v), numof(1) {}

/**
  * Assignment operator from integer.
  */
  RecoIndex& operator = (unsigned short v) {val=v; return *this;}

/**
  * Comparison operator for indexing
  */
  bool operator <  (const RecoIndex& ri) const {
     // comparison makes sense only in 'separate' mode.
    if( mode==separate && ri.mode==separate ) {
      return val < ri.val;
    }
    return false; // equal or ignored when comparing
  }

/**
  * Conversion operator
  */
  operator unsigned short () const {if(mode==single) return 0; return val;} // return zero index in single mode for numof=1

/**
  * Sets the mode
  */
  RecoIndex& set_mode(indexMode m) {mode=m; return *this;}

/**
  * Returns the mode
  */
  indexMode get_mode() const {return indexMode(mode);}

/**
  * Returns true if two indices are compatible when chaining functors
  */
  bool compatible(const RecoIndex& ri) const {
    if(mode==any || ri.mode==any) return true;
    if(mode==collected && ri.mode!=collected) return false;
    if(mode!=collected && ri.mode==collected) return false;
    if(mode==single && ri.mode==single && val!=ri.val) return false;
    return true;
  }


/**
  * Returns true if this is in the range of 'mask'. Prepares index for count map.
  */
  bool prep_count(const RecoIndex& mask) {
    if(mask.mode==single) {
      if(val!=mask.val) return false;
    }
    mode=mask.mode;
    return true;
  }


/**
  * Sets the number of indices
  */
  RecoIndex& set_numof(unsigned short n) {numof=n; return *this;}

/**
  * Returns the number of indices
  */
  unsigned short get_numof() const {if(mode==single) return 1; return numof;}


/**
  * Mode for printing the index:
  * - brief    : Brief version for debugging
  * - all      : Include all information, i.e. all indices will be printed with their current value, regardless of their mode or numof
  * - filename : Suitable for generating filenames
  */
  enum printMode {brief=0, all, filename};

/**
  * Converts the index to a string for reco dimension 'dim' according to 'printmode'
  */
  STD_string print(recoDim dim, printMode printmode=brief) const;


 private:

  unsigned char mode;
  unsigned short val;
  unsigned short numof;



};

//////////////////////////////////////////////////////////////////


typedef Data<GriddingPoint<3> ,2> KspaceTraj; //  dimension is (nsegments, npoints)

typedef STD_pair<Data<float,1>, int> ReadoutShape; // shape and target grid size



/**
  * Holds reconstruction coordinate of one acquisition frame, i.e. position of reconstruction data in multiple dimensions.
  */
struct RecoCoord {


/**
  * Construct default coordinate
  */
  RecoCoord() {common_init();}


/**
  * Comparison operator for indexing
  */
  bool operator < (const RecoCoord& rc) const {
    for(int i=0; i<n_recoDims; i++) {
      bool rc_larger= index[i]<rc.index[i];
      bool rc_smaller=rc.index[i]<index[i];
      if(rc_larger || rc_smaller) return rc_larger; // return immediately if different
    }
    return false;
  }


/**
  * Equality operator
  */
  bool operator == (const RecoCoord& rc) const {
    return !((*this)<rc || rc<(*this));
  }


/**
  * Returns true if two coordinates are compatible when chaining functors.
  */
  bool compatible(const RecoCoord& rc) const {
    for(int i=0; i<n_recoDims; i++) {
      if(!index[i].compatible(rc.index[i])) return false;
    }
    return true;
  }


/**
  * Returns true if this is in the range of 'mask'. Prepares this for count map.
  */
  bool prep_count(const RecoCoord& mask) {
    for(int i=0; i<n_recoDims; i++) {
      if(!index[i].prep_count(mask.index[i])) return false;
    }
    return true;
  }


/**
  * Converts the coordinate to a string according to 'printmode'
  */
  STD_string print(RecoIndex::printMode printmode=RecoIndex::brief) const;


/**
  * Convenience function: Returns coordinate with all dimensions mode set to 'any'
  */
  static RecoCoord any() {
    RecoCoord result;
    result.set_uniform_mode(RecoIndex::any);
    return result;
  }


/**
  * Convenience function: Sets all dimensions to mode 'm'
  */
  RecoCoord& set_uniform_mode(RecoIndex::indexMode m) {
    for(int i=0; i<n_recoDims; i++) index[i].set_mode(m);
    return *this;
  }


/**
  * Convenience function: Sets specified dimensions to mode 'm'
  */
  RecoCoord& set_mode(RecoIndex::indexMode m, recoDim d1=n_recoDims, recoDim d2=n_recoDims, recoDim d3=n_recoDims, recoDim d4=n_recoDims, recoDim d5=n_recoDims, recoDim d6=n_recoDims, recoDim d7=n_recoDims, recoDim d8=n_recoDims) {
    if(d1<n_recoDims) index[d1].set_mode(m);
    if(d2<n_recoDims) index[d2].set_mode(m);
    if(d3<n_recoDims) index[d3].set_mode(m);
    if(d4<n_recoDims) index[d4].set_mode(m);
    if(d5<n_recoDims) index[d5].set_mode(m);
    if(d6<n_recoDims) index[d6].set_mode(m);
    if(d7<n_recoDims) index[d7].set_mode(m);
    if(d8<n_recoDims) index[d8].set_mode(m);
    return *this;
  }


/**
  * Convenience function: Returns coordinate with specified dimensions set to mode 'm'
  */
  static RecoCoord coord_with_mode(RecoIndex::indexMode m, recoDim d1=n_recoDims, recoDim d2=n_recoDims, recoDim d3=n_recoDims, recoDim d4=n_recoDims, recoDim d5=n_recoDims, recoDim d6=n_recoDims, recoDim d7=n_recoDims, recoDim d8=n_recoDims) {
    RecoCoord result;
    result.set_mode(m,d1,d2,d3,d4,d5,d6,d7,d8);
    return result;
  }

/**
  * Convenience function: Returns the number of indices in dimension 'dim'
  */
  unsigned short numof(recoDim dim) const {return index[dim].get_numof();}


/**
  * The index in each dimension
  */
  RecoIndex index[n_recoDims];


/**
  * Combination of recoFlags
  */
  unsigned char flags;


/**
  * Readout shape for regridding, is zero, if none available.
  * Protect acess by mutex.
  */
  const ReadoutShape* readoutShape;


/**
  * Explicit k-space trajectory for gridding.
  * Protect acess by mutex.
  */
  const KspaceTraj* kspaceTraj;

/**
  * Complex weighting of ADC, is zero, if none available.
  */
  const ComplexData<1>* weightVec;


/**
  * Dwell time of the signal data
  */
  double dwellTime;

/**
  * Oversampling factor in readout direction
  */
  float overSamplingFactor;

/**
  * Relative position of k-space center in readout direction
  */
  float relCenterPos;

/**
  * First index in readout direction actually measured, will be converted from 'relCenterPos' in functor 'adc_pad'
  */
  unsigned short adcstart;

/**
  * Last index in readout direction actually measured, will be converted from 'relCenterPos' in functor 'adc_pad'
  */
  unsigned short adcend;

/**
  * Scale factor for gridding reconstruction
  */
  float gridScaleFactor;

/**
  * Echo-train index in a multi-echo readout, e.g. an CPMG or EPI echo train.
  * Use this for non-phasecorr template scans and set echo index to zero.
  */
  unsigned short echopos;

/**
  * EPI echo-train index for 3D multi-EPI readout and EPI navigator correction.
  */
  unsigned short echotrain;


 private:
  void common_init() {
    flags=0;
    readoutShape=0;
    kspaceTraj=0;
    weightVec=0;
    dwellTime=0.0;
    overSamplingFactor=1.0;
    relCenterPos=0.5;
    adcstart=0;
    adcend=0;
    gridScaleFactor=1.0;
    echopos=0;
    echotrain=0;
    index[readout].set_mode(RecoIndex::collected); // readout is already collected for ADCs
  }

};


////////////////////////////////////////////////////////////////////////////

/**
  * Map to count the number of times a coordinate was measured/interpolated
  */
typedef STD_map<RecoCoord, UInt> CoordCountMap;

////////////////////////////////////////////////////////////////////////////

/**
  * Helper template to define a set of 'Ndim' dimensions using default arguments for unused dimensions.
  */
template<unsigned int Ndim, recoDim d1=n_recoDims, recoDim d2=n_recoDims, recoDim d3=n_recoDims, recoDim d4=n_recoDims, recoDim d5=n_recoDims, recoDim d6=n_recoDims>
struct RecoDim {

/**
  * Returns the number of dimensions to set/modify.
  */
  static unsigned int dim() {return Ndim;}

/**
  * Returns coordinate with specified dimensions set to mode 'm'.
  */
  static RecoCoord preset_coord(RecoIndex::indexMode m) {return RecoCoord::coord_with_mode(m, d1, d2, d3, d4, d5, d6);}

/**
  * Sets specified dimensions in coordinate 'coord' to mode 'm'.
  */
  static void modify(RecoIndex::indexMode m, RecoCoord& coord) {coord.set_mode(m, d1, d2, d3, d4, d5, d6);}

/**
  * Creates 'Ndim' dimensional index vector in specified dimensions using coordinate 'coord'.
  */
  static void create_index(const RecoCoord& coord, TinyVector<int,Ndim>& index) {
    unsigned int idim=0;
    if(d1!=n_recoDims) {index(idim)=coord.index[d1]; idim++;}
    if(d2!=n_recoDims) {index(idim)=coord.index[d2]; idim++;}
    if(d3!=n_recoDims) {index(idim)=coord.index[d3]; idim++;}
    if(d4!=n_recoDims) {index(idim)=coord.index[d4]; idim++;}
    if(d5!=n_recoDims) {index(idim)=coord.index[d5]; idim++;}
    if(d6!=n_recoDims) {index(idim)=coord.index[d6]; idim++;}
  }

/**
  * Creates 'Ndim' dimensional shape vector in specified dimensions using coordinate 'coord'.
  */
  static void create_shape(const RecoCoord& coord, TinyVector<int,Ndim>& shape) {
    unsigned int idim=0;
    if(d1!=n_recoDims) {shape(idim)=coord.numof(d1); idim++;}
    if(d2!=n_recoDims) {shape(idim)=coord.numof(d2); idim++;}
    if(d3!=n_recoDims) {shape(idim)=coord.numof(d3); idim++;}
    if(d4!=n_recoDims) {shape(idim)=coord.numof(d4); idim++;}
    if(d5!=n_recoDims) {shape(idim)=coord.numof(d5); idim++;}
    if(d6!=n_recoDims) {shape(idim)=coord.numof(d6); idim++;}
  }


/**
  * Copies values of 'Ndim' dimensional index vector to specified dimensions in 'coord'.
  */
  static void set_index(const TinyVector<int,Ndim>& index, RecoCoord& coord) {
    unsigned int idim=0;
    if(d1!=n_recoDims) {coord.index[d1]=index(idim); idim++;}
    if(d2!=n_recoDims) {coord.index[d2]=index(idim); idim++;}
    if(d3!=n_recoDims) {coord.index[d3]=index(idim); idim++;}
    if(d4!=n_recoDims) {coord.index[d4]=index(idim); idim++;}
    if(d5!=n_recoDims) {coord.index[d5]=index(idim); idim++;}
    if(d6!=n_recoDims) {coord.index[d6]=index(idim); idim++;}
  }

};





/** @}
  */


#endif

