#include "fieldmap.h"
#include "data.h"
#include "controller.h"

#include <odindata/fitting.h>
#include <odindata/gridding.h>

bool RecoFieldMap::process(RecoData& rd, RecoController& controller) {
  Log<Reco> odinlog(c_label(),"process");

  Range all=Range::all();

  ComplexData<4>& indata=rd.data(Rank<4>());
  TinyVector<int,4> inshape=indata.shape();
  TinyVector<int,3> outshape(inshape(1), inshape(2), inshape(3));
  ODINLOG(odinlog,normalDebug) << "inshape/outshape=" << inshape << "/" << outshape << STD_endl;

  dvector tes=controller.dim_values(te);
  ODINLOG(odinlog,normalDebug) << "tes=" << tes.printbody() << STD_endl;

  if(int(tes.size())!=inshape(0)) {
    ODINLOG(odinlog,errorLog) << "TE size mismatch: " << tes.size() << "!=" << inshape(0) << STD_endl;
    return false;
  }
  if(inshape(0)<2) {
    ODINLOG(odinlog,errorLog) << "too few number of TEs: " << inshape(0) << STD_endl;
    return false;
  }

  bool oddeven=(inshape(0)>2); // Assume odd and even echoes (EPI-type readtout) for more than 2 TEs

  int npol=1;
  if(oddeven) npol=2; 
  int ntes[npol];
  if(oddeven) {
    ntes[0]=ntes[1]=inshape(0)/2;
    if(inshape(0)%2) ntes[0]++;
  } else {
    ntes[0]=inshape(0);
  }
  ODINLOG(odinlog,normalDebug) << "oddeven/inshape(0)/npol/ntes[0]/ntes[1]=" << oddeven << "/" << inshape(0) << "/" << npol << "/" << ntes[0] << "/" << ntes[1] << STD_endl;



  // regroup odd and even data
  ComplexData<4> tedata[2];
  Data<float,1> pixel[2];
  Data<float,1> magn[2];
  ComplexData<1> pixel_complex[2];
  Data<float,1> sigma[2];
  Data<float,1> tevals[2];
  for(int ipol=0; ipol<npol; ipol++) {
    tedata[ipol].resize(ntes[ipol], inshape(1), inshape(2), inshape(3));
    pixel[ipol].resize(ntes[ipol]);
    magn[ipol].resize(ntes[ipol]);
    pixel_complex[ipol].resize(ntes[ipol]);
    sigma[ipol].resize(ntes[ipol]);
    tevals[ipol].resize(ntes[ipol]);
  }

  int index[2]; index[0]=index[1]=0;
  for(int ite=0; ite<inshape(0); ite++) {
    int ipol=0;
    if(ntes[1]) ipol=ite%2;
    tedata[ipol](index[ipol],all,all,all)=indata(ite,all,all,all);
    tevals[ipol](index[ipol])=tes[ite];
    ODINLOG(odinlog,normalDebug) << "tevals[" << ipol << "](" << index[ipol] << ")=" << tevals[ipol](index[ipol]) << STD_endl;
    index[ipol]++;
  }

/*
  for(int ipol=0; ipol<npol; ipol++) {
    STD_string idstr="pol"+itos(ipol)+"_"+rd.coord().print(false, true);
    Data<float,4>(cabs(tedata[ipol])).autowrite("cabs_"+idstr+".nii");
    Data<float,4>(phase(tedata[ipol])).autowrite("phase_"+idstr+".nii");
  }
*/

  Data<float,3> reliability(outshape);
  reliability(all,all,all)=cabs(indata(0,all,all,all)); // Take 1st echo for reliability based on image magnitude
  float noiselevel=0.6*mean(reliability);

  indata.free();

  Data<float,3> fieldmap(outshape); fieldmap=0.0;
//  ComplexData<3> cplxphase(outshape); cplxphase=STD_complex(0.0);

  LinearFunction linf;

  for(int ipol=0; ipol<npol; ipol++) { // Separate fit for odd and even echoes

    if(ntes[ipol]) {

      float dte=0.0;
      if(ntes[ipol]==2) {
        dte=tevals[ipol](1)-tevals[ipol](0);
        ODINLOG(odinlog,normalDebug) << "dte[" << ipol << "]=" << dte << STD_endl;
      }

      for(unsigned int i=0; i<fieldmap.size(); i++) {
        TinyVector<int,3> index=fieldmap.create_index(i);

        if(reliability(index)>noiselevel) {

          pixel_complex[ipol]=tedata[ipol](all,index(0),index(1),index(2));

          if(ntes[ipol]==2) {

            STD_complex phase0=expc(float2imag(phase(pixel_complex[ipol](0))));
            STD_complex phase1=expc(float2imag(phase(pixel_complex[ipol](1))));

            float dphase=phase(phase1/phase0);

            fieldmap(index)+=secureDivision(dphase, dte);
          }

          if(ntes[ipol]>2) {

            // Create unwrapped phase
            pixel[ipol]=pixel_complex[ipol].phasemap();

            // Consider magnitude based reliability
            magn[ipol]=cabs(pixel_complex[ipol]);
            if(min(magn[ipol])>0.0) sigma[ipol]=float(1.0)/magn[ipol];
            else sigma[ipol]=1.0;

            // linear regression
            linf.fit(pixel[ipol],sigma[ipol],tevals[ipol]);
            fieldmap(index)+=linf.m.val;
//            cplxphase(index)+=expc(STD_complex(0.0,linf.c.val));
          }

        } else {
          reliability(index)=0.0; // will be re-used in RecoInterpolate
        }
      }
    }
  }

  int nsummand=0;
  for(int ipol=0; ipol<npol; ipol++) if(ntes[ipol]>1) nsummand++;
  if(nsummand>1) fieldmap/=float(nsummand); // Average over odd and even echoes

//  Data<float,3> phasemap(phase(cplxphase));
//  phasemap.autowrite("phasemap_channel"+itos(rd.coord().index[channel])+".jdx");

  ODINLOG(odinlog,normalDebug) << "sum(fieldmap)=" << sum(fieldmap) << STD_endl;

  ComplexData<3>& outdata=rd.data(Rank<3>());
  outdata.resize(outshape);
  outdata=float2real(fieldmap)+float2imag(reliability);
  rd.mode=RecoData::weighted_real_data;
  return execute_next_step(rd,controller);
}


////////////////////////////////////////////////////////////////////////////////////

bool RecoFieldMapUser::get_fmap(Data<float,3>& fieldmap, const TinyVector<int,3>& dstshape, const RecoCoord& fmapcoord, RecoController& controller) {
  Log<Reco> odinlog(c_label(),"get_fmap");

  bool calc_fmap=true;
  fmapmutex.lock();
  FieldMap::const_iterator it=fmap.find(fmapcoord);
  if(it!=fmap.end()) {
    TinyVector<int,3> srcshape=it->second.shape();
    if(srcshape!=dstshape) {
      ODINLOG(odinlog,errorLog) << "shape mismatch: srcshape" << srcshape << "!=dstshape" << dstshape << STD_endl;
      return false;
    }
    fieldmap.reference(it->second.copy()); // Deep copy for multi-threaded access
    calc_fmap=false;
  }
  fmapmutex.unlock();

  if(calc_fmap) {
    if(!controller.data_announced(posted_fmap_str)) {
      ODINLOG(odinlog,errorLog) << "fieldmap not available" << STD_endl;
      return false;
    }

    RecoData rdfieldmap(fmapcoord);
    rdfieldmap.coord().index[cycle] = 0; // reset to 0 to only get initial (not rotated) field map
    if(!controller.inquire_data(posted_fmap_str, rdfieldmap)) return false;

    float os_fmap = rdfieldmap.coord().overSamplingFactor;
    float os_user = fmapcoord.overSamplingFactor;
//    float osFactor = secureDivision(os_user, os_fmap);
    ODINLOG(odinlog,normalDebug) << "os_fmap/os_user=" << os_fmap << "/" << os_user << STD_endl;

    Data<float,3> fmreal(creal(rdfieldmap.data(Rank<3>())));
    TinyVector<int,3> fmshape(fmreal.shape());
    ODINLOG(odinlog,normalDebug) << "fmshape/dstshape=" << fmshape << "/" << dstshape << STD_endl;

    // spatial shifting
    TinyVector<float,3> reloffset=controller.reloffset();
    reloffset(0) = 0; // set zero to only shift in phase and readout direction, shifting in slice direction is not necessary because no rotation is done in slice direction
    reloffset(2)/=os_fmap;
    bool do_shift=false;
    if(max(abs(reloffset))>0.0) do_shift=true;

    // create mask for weighting
    Data<float,3> mask(fmshape);
    mask=where(Array<float,3>(fmreal)!=0.0, float(1.0), float(0.0));
    // mask.autowrite("mask_"+rdfieldmap.coord().print(RecoIndex::filename)+".nii");

    // prepare shapes and vars for gridder
    STD_vector<GriddingPoint<3> > src_coords(product(fmshape));
    TinyVector<float,3> image_center = 0.5 * (fmshape-1); // index of center in image space
    TinyVector<float,3> srcfov; srcfov = 0.0;
    TinyVector<float,3> dstfov; dstfov = 0.0;
    if (fmshape(0) > 1)  srcfov(0) = controller.protocol().geometry.get_FOV(sliceDirection);
    if (dstshape(0) > 1) dstfov(0) = controller.protocol().geometry.get_FOV(sliceDirection); // Only regridding in line3d direction if dimension > 1
    srcfov(1) = dstfov(1) = controller.protocol().geometry.get_FOV(phaseDirection);
    srcfov(2) = os_fmap*controller.protocol().geometry.get_FOV(readDirection);
    dstfov(2) = os_user*controller.protocol().geometry.get_FOV(readDirection);
    ODINLOG(odinlog,normalDebug) << "srcfov=" << srcfov << STD_endl;
    ODINLOG(odinlog,normalDebug) << "dstfov=" << dstfov << STD_endl;

    bool hasBlades = (fmapcoord.index[cycle].get_numof() > 1);
    int iblade = fmapcoord.index[cycle];
    dvector angles;
    if (hasBlades) {
      angles = controller.dim_values(cycle);
      ODINLOG(odinlog,normalDebug) << "angles=" << angles.printbody() << STD_endl;
      hasBlades = (angles.length() == fmapcoord.index[cycle].get_numof()); // check for spiral or propeller
    }
    ODINLOG(odinlog,normalDebug) << "hasBlades=" << hasBlades << STD_endl;

    int isrc = 0;
    TinyVector<float,3> coord;
    for (int islice = 0; islice < fmshape(0); islice++) {
      for (int iphase = 0; iphase < fmshape(1); iphase++) {
        for (int iread = 0; iread < fmshape(2); iread++) {

          TinyVector<int,3> index3d(islice, iphase, iread);
          coord = (index3d - image_center) / fmshape * srcfov;

          double angle_rad = 0.0;
          if(hasBlades) angle_rad = angles[iblade]; // not reversed rotation...

          GriddingPoint<3>& src_point=src_coords[isrc];
          src_point.coord = coord;
          src_point.weight = mask(islice,iphase,iread);

          if(angle_rad) {

            TinyVector<float,3> coord_min = -image_center / fmshape * srcfov;
            TinyVector<float,3> coord_max =  image_center / fmshape * srcfov;
            if (do_shift) { // Shift to center

              src_point.coord -= reloffset * srcfov;
              if (src_point.coord(1) < coord_min(1)) src_point.coord(1) = coord_max(1) - (coord_min(1) - src_point.coord(1));
              if (src_point.coord(2) < coord_min(2)) src_point.coord(2) = coord_max(2) - (coord_min(2) - src_point.coord(2));
              if (src_point.coord(1) >= coord_max(1)) src_point.coord(1) = coord_min(1) + (src_point.coord(1) - coord_max(1));
              if (src_point.coord(2) >= coord_max(2)) src_point.coord(2) = coord_min(2) + (src_point.coord(2) - coord_max(2));
            }

            // Rotation
            float si=sin(angle_rad);
            float co=cos(angle_rad);
            TinyVector<float,3> coord_temp = src_point.coord;
            src_point.coord(0)=coord_temp(0);
            src_point.coord(1)=co*coord_temp(1)-si*coord_temp(2);
            src_point.coord(2)=si*coord_temp(1)+co*coord_temp(2);

            if (do_shift) { // Shift back

              TinyVector<float,3> reloffset_rot;
              reloffset_rot(0)=reloffset(0);
              reloffset_rot(1)=co*reloffset(1)-si*reloffset(2);
              reloffset_rot(2)=si*reloffset(1)+co*reloffset(2);

              src_point.coord += reloffset_rot * srcfov;
              if (src_point.coord(1) < coord_min(1)) src_point.coord(1) = coord_max(1) - (coord_min(1) - src_point.coord(1));
              if (src_point.coord(2) < coord_min(2)) src_point.coord(2) = coord_max(2) - (coord_min(2) - src_point.coord(2));
              if (src_point.coord(1) >= coord_max(1)) src_point.coord(1) = coord_min(1) + (src_point.coord(1) - coord_max(1));
              if (src_point.coord(2) >= coord_max(2)) src_point.coord(2) = coord_min(2) + (src_point.coord(2) - coord_max(2));
            }
          }

          isrc++;
        }
      }
    }

    LDRfilter gridkernel;
    gridkernel.set_function("Gauss");

    float kwidth = 1.5*max(dstfov/dstshape);
    ODINLOG(odinlog,normalDebug) << "kwidth=" << kwidth << STD_endl;

    Gridding<float,3> gridder;
    gridder.init(dstshape, dstfov, src_coords, gridkernel, kwidth);

    fieldmap.reference(gridder(fmreal));

    fmapmutex.lock();
    fmap[fmapcoord].reference(fieldmap);
    fmapmutex.unlock();
  }

  return true;
}


void RecoFieldMapUser::modify4fieldmap(RecoCoord& coord) {
  for(int idim=0; idim<n_recoDims; idim++) coord.index[idim].set_mode(RecoIndex::ignore);  // assume the same for all dimensions ...
  coord.set_mode(RecoIndex::separate, slice); // ... except for slice dimension
}

