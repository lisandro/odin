#include "dti.h"
#include "controller.h"


bool RecoDTI::process(RecoData& rd, RecoController& controller) {
  Log<Reco> odinlog(c_label(),"process");

  Protocol prot(controller.protocol()); // Local copy of protocol, used for override_protocol

  rd.override_protocol=&prot;
  STD_string series;
  int number;
  prot.study.get_Series(series, number);

  int idti=rd.coord().index[dti];

  dvector bvals=controller.dim_values(dti);

  LDRtriple bvector(bvals[3*idti], bvals[3*idti+1], bvals[3*idti+2], "Diffusion_bVector");
  bvector*=1000.0; // in s/mm^2

  prot.study.set_Series("b"+replaceStr(bvector.printbody()," ","_"),number);
  prot.methpars.append_copy(bvector);

  return execute_next_step(rd,controller);
}
