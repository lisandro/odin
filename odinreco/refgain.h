/***************************************************************************
                          refgain.h  -  description
                             -------------------
    begin                : Thu Feb 22 2007
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef RECOREFGAIN_H
#define RECOREFGAIN_H

#include "step.h"

class RecoRefGain : public RecoStep {

  // implementing virtual functions of RecoStep
  STD_string label() const {return "refgain";}
  STD_string description() const {return "Calculate reference gain (Bruker)";}
  bool process(RecoData& rd, RecoController& controller);
  RecoCoord input_coord() const {return RecoCoord::coord_with_mode(RecoIndex::collected,userdef,line3d,line,readout);}
  void modify_coord(RecoCoord& coord) const {coord.set_mode(RecoIndex::single,userdef);}
  RecoStep* allocate() const {return new RecoRefGain;}
  void init();

  LDRfloat pulsedur;
  LDRfloat pulsegain;
};


#endif

