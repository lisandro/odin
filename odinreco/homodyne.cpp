#include "homodyne.h"
#include "data.h"
#include "controller.h"


bool RecoHomodyne::weight_and_range(int dimindex, RecoData& rd, Range& symrange, ComplexData<1>& weight) {
  Log<Reco> odinlog(c_label(),"weight_data");

  ComplexData<3>& data=rd.data(Rank<3>());
  TinyVector<int,3> shape=data.shape();

  int npts=shape(dimindex);

  int startindex=0;

  if(dimindex==2) {
    if(rd.coord().adcend<(npts-1)) {
      ODINLOG(odinlog,errorLog) << "Not implemented: Interpolation at end of readout" << STD_endl;
      return false;
    }
    startindex=rd.coord().adcstart;
  } else {
    ivector indexvec;
    if(dimindex==0) indexvec=measlines3d.get_indices(rd.coord());
    if(dimindex==1) indexvec=measlines.get_indices(rd.coord());
    startindex=indexvec.minvalue();
  }

  int endindex=npts-1-startindex;
  symrange=Range(startindex,endindex);

  ODINLOG(odinlog,normalDebug) << "startindex/endindex[" << dimindex << "]=" << startindex << "/" << endindex << STD_endl;

  weight.resize(npts);
  weight=STD_complex(1.0);

  if(startindex<=0) return true; // no homodyne reco required, end here 

  float center=0.5*float(npts-1);
  ODINLOG(odinlog,normalDebug) << "center[" << dimindex << "]=" << center << STD_endl;

  if(float(startindex)>=center) { // Not true homodyne reco
    ODINLOG(odinlog,errorLog) << "startindex=" << startindex << " larger than or equal center=" << center << STD_endl;
    return false;
  }

  fvector transition(endindex-startindex+1);
  transition.fill_linear(0.0,1.0);
  for(int i=0; i<startindex; i++) weight(i)=STD_complex(0.0); // make sure we blank all non-acquired lines
  for(int i=0; i<int(transition.size()); i++) weight(startindex+i)=transition[i];

  return true;
}


bool RecoHomodyne::process(RecoData& rd, RecoController& controller) {
  Log<Reco> odinlog(c_label(),"process");

//  Range all=Range::all();

  if(rd.interpolated) {
    measlines.update(*(rd.interpolated)); // take previously interpolated coords into account
    measlines3d.update(*(rd.interpolated)); // take previously interpolated coords into account
  }

  ComplexData<3>& data=rd.data(Rank<3>());
  TinyVector<int,3> shape=data.shape();

  Range symrange3d;
  ComplexData<1> weight3d;
  if(!weight_and_range(0, rd, symrange3d, weight3d)) return false;

  Range symrange;
  ComplexData<1> weight;
  if(!weight_and_range(1, rd, symrange, weight)) return false;

  Range symrangeread;
  ComplexData<1> weightread;
  if(!weight_and_range(2, rd, symrangeread, weightread)) return false;

  // symmetric data for phase correction
  ComplexData<3> symmetric(shape); symmetric=STD_complex(0.0);
  symmetric(symrange3d,symrange,symrangeread)=data(symrange3d,symrange,symrangeread);
//  Data<float,3>(cabs(symmetric)).autowrite("symmetric.nii");

  symmetric.fft();
//  Data<float,3>(cabs(symmetric)).autowrite("symmetric_fft_cabs.nii");
//  Data<float,3>(phase(symmetric)).autowrite("symmetric_fft_phase.nii");

  for(int iphase3d=0; iphase3d<shape(0); iphase3d++) {
    for(int iphase=0; iphase<shape(1); iphase++) {
      for(int iread=0; iread<shape(2); iread++) {
        data(iphase3d,iphase,iread)*=weight3d(iphase3d)*weight(iphase)*weightread(iread);
      }
    }
  }


  // do the FFT
  data.fft();


  // phase correction
  data*=expc(float2imag(-phase(symmetric)));

  // take real part
  ComplexData<3> realpart(shape);
  realpart=float2real(creal(data));
  data.reference(realpart);

  return execute_next_step(rd,controller);
}

///////////////////////////////////////////////////////


bool RecoHomodyne::query(RecoQueryContext& context) {
  Log<Reco> odinlog(c_label(),"query");
  if(context.mode==RecoQueryContext::prep) {
    if(!measlines.init(context.coord, context.controller)) return false;
    if(!measlines3d.init(context.coord, context.controller)) return false;
  }
  return RecoStep::query(context);
}

