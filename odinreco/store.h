/***************************************************************************
                          store.h  -  description
                             -------------------
    begin                : Sat Dec 30 2006
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef RECOSTORE_H
#define RECOSTORE_H

#include "step.h"

#include <odindata/fileio.h>
#include <odindata/filter.h>

class RecoStore : public RecoStep {

 public:

  static STD_string default_format();


 private:

  // implementing virtual functions of RecoStep
  STD_string label() const {return "store";}
  STD_string description() const {return "Store reconstructed images";}
  bool process(RecoData& rd, RecoController& controller);
  RecoCoord input_coord() const {return RecoCoord::coord_with_mode(RecoIndex::collected,repetition,slice,line3d,line,readout);}
  void modify_coord(RecoCoord& coord) const {}
  bool query(RecoQueryContext& context);
  RecoStep* allocate() const {return new RecoStore;}
  void init();

  LDRstring extension;
  LDRstring prefix;
  LDRstring formats;
  LDRstring imageproc;
  LDRbool   storephase;
  FileWriteOpts wopts; // for access to some autowrite opts

  bool store_images(const RecoController& controller);

  FileIO::ProtocolDataMap pdmap; // Will hold image data with protocol as key
  Mutex mutex;

  FilterChain filterchain;

};

////////////////////////////////////////////////////////////////////////////


class RecoReal : public RecoStep {

  // implementing virtual functions of RecoStep
  STD_string label() const {return "real";}
  STD_string description() const {return "Throw away imaginary part of data and keep only real part";}
  bool process(RecoData& rd, RecoController& controller);
  RecoCoord input_coord() const {return RecoCoord::any();}
  void modify_coord(RecoCoord& coord) const {}
  RecoStep* allocate() const {return new RecoReal;}
  void init() {}

};

class RecoMagn : public RecoStep {

  // implementing virtual functions of RecoStep
  STD_string label() const {return "magn";}
  STD_string description() const {return "Only take magnitude and set phase to zero";}
  bool process(RecoData& rd, RecoController& controller);
  RecoCoord input_coord() const {return RecoCoord::coord_with_mode(RecoIndex::collected,line3d,line,readout);}
  void modify_coord(RecoCoord& coord) const {}
  RecoStep* allocate() const {return new RecoMagn;}
  void init() {}

};


#endif

