/***************************************************************************
                          measindex.h  -  description
                             -------------------
    begin                : Mon Apr 2 2007
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef RECOMEASINDEX_H
#define RECOMEASINDEX_H


#include "controller.h"


/**
  * Helper template class to get info about indices actually measured in dimension 'dim'.
  * Implement 'Ignore' (and Separate) by a specialization of 'RecoDim' to specify dimensions
  * to be ignored (separate) before adding/retrieving index from map.
  */
template<recoDim dim, class Ignore, class Separate=RecoDim<0> >
class RecoMeasIndex {

 public:

/**
  * Initialize with coordinate 'coord' in this branch of the pipeline using 'controller'.
  */
  bool init(const RecoCoord& coord, const RecoController& controller) {
    Log<Reco> odinlog("RecoMeasIndex","init");
    RecoCoord coord4count(coord);
    prep_coord(coord4count);
    coord4count.set_mode(RecoIndex::separate, dim);

    const CoordCountMap* countmap=controller.create_count_map(coord4count);
    if(!countmap) return false;
    init_countmap=(*countmap);  // create deep copy because countmap may be deleted by controller after prep

    return create_measmap(init_countmap);
  }


/**
  * Update cache of measured/interpolated coords by adding 'extra' to the initial coordinates.
  */
  bool update(const CoordCountMap& extra) {
    Log<Reco> odinlog("RecoMeasIndex","update");

    if(!extra.size()) return true;

    CoordCountMap countmap_copy(init_countmap);

    ODINLOG(odinlog,normalDebug) << "countmap_copy/extra.size()=" << countmap_copy.size() << "/" << extra.size() << STD_endl;

    for(CoordCountMap::const_iterator it=extra.begin(); it!=extra.end(); ++it) countmap_copy[it->first]+=it->second;

    return create_measmap(countmap_copy);
  }


/**
  * Returns the list of indices for coordinate 'coord'.
  */
  ivector get_indices(const RecoCoord& coord) const {
    Log<Reco> odinlog("RecoMeasIndex","get_indices");
    ivector result;
    RecoCoord coord4meas(coord);
    prep_coord(coord4meas);
    coord4meas.set_mode(RecoIndex::ignore,dim);

    MeasMap::const_iterator it=measmap.find(coord4meas);
    if(it==measmap.end()) {
      ODINLOG(odinlog,normalDebug) << "<" << recoDimLabel[dim] << ">: No indices for coordinate " << coord4meas.print() << STD_endl;
    } else {
      result=it->second;
      ODINLOG(odinlog,normalDebug) << "coord4meas/it=" << coord4meas.print() << "/" << it->first.print() << STD_endl;
      ODINLOG(odinlog,normalDebug) << "result=" << result.printbody() << STD_endl;
    }
    return result;
  }


 private:

  static void prep_coord(RecoCoord& coord) {
    Ignore::modify(RecoIndex::ignore, coord);
    Separate::modify(RecoIndex::separate, coord);
  }


  bool create_measmap(const CoordCountMap& countmap) {
    Log<Reco> odinlog("RecoMeasIndex","create_measmap");

    STD_map<RecoCoord, STD_list<int> > indexlist;
    for(CoordCountMap::const_iterator it=countmap.begin(); it!=countmap.end(); ++it) {
      RecoCoord coord4indexlist(it->first);
      coord4indexlist.set_mode(RecoIndex::ignore, dim); // collapse index, indices will be stored in the index vectors of measmap
      indexlist[coord4indexlist].push_back(it->first.index[dim]);
    }

    for(STD_map<RecoCoord, STD_list<int> >::iterator lstit=indexlist.begin(); lstit!=indexlist.end(); ++lstit) {
      STD_list<int>& lstref=lstit->second;
      lstref.sort();
      lstref.unique();
      measmap[lstit->first]=list2vector(lstref);
    }

    for(MeasMap::const_iterator mapit=measmap.begin(); mapit!=measmap.end(); ++mapit) {
      ODINLOG(odinlog,normalDebug) << "measmap[" << mapit->first.print() << "]=" << mapit->second.printbody() << STD_endl;
    }

    return true;
  }


  CoordCountMap init_countmap;

  typedef STD_map<RecoCoord, ivector> MeasMap;
  MeasMap measmap; // will be initialized during init

};


#endif

