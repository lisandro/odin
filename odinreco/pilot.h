/***************************************************************************
                          pilot.h  -  description
                             -------------------
    begin                : Tue Mar 14 2007
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef RECOPILOT_H
#define RECOPILOT_H

#include "step.h"

class RecoPilot : public RecoStep {

  // implementing virtual functions of RecoStep
  STD_string label() const {return "pilot";}
  STD_string description() const {return "Modify geometry for pilot output";}
  bool process(RecoData& rd, RecoController& controller);
  RecoCoord input_coord() const {return RecoCoord::coord_with_mode(RecoIndex::collected,userdef,line3d,line,readout);}
  void modify_coord(RecoCoord& coord) const {coord.set_mode(RecoIndex::separate,userdef).set_mode(RecoIndex::collected,repetition,slice);}
  RecoStep* allocate() const {return new RecoPilot;}
  void init();


  void transpose_inplane(ComplexData<5>& data, Geometry& geo, bool reverse_read, bool reverse_phase);

  LDRfloat slicedist;
};


#endif

