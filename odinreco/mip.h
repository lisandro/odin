/***************************************************************************
                          mip.h  -  description
                             -------------------
    begin                : Tue Dec 4 2007
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef RECOMIP_H
#define RECOMIP_H

#include "step.h"


class RecoMip : public RecoStep {

  // implementing virtual functions of RecoStep
  STD_string label() const {return "mip";}
  STD_string description() const {return "Maximum intensity projection in slice direction";}
  bool process(RecoData& rd, RecoController& controller);
  RecoCoord input_coord() const {return RecoCoord::coord_with_mode(RecoIndex::collected,line3d,line,readout);}
  void modify_coord(RecoCoord& coord) const {}
  RecoStep* allocate() const {return new RecoMip;}
  void init();

  LDRint neighbors;

};


#endif

