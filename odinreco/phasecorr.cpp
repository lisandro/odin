#include "phasecorr.h"
#include "data.h"
#include "controller.h"

#include <odindata/fitting.h>


static const char* posted_phasemap_label="phasemap";



bool RecoPhaseMap::process(RecoData& rd, RecoController& controller) {
  Log<Reco> odinlog(c_label(),"process");

  Range all=Range::all();


  modify4blackboard(rd.coord());

  ComplexData<2>& data=rd.data(Rank<2>());
  TinyVector<int,2> shape(data.shape());
  int nechoes=shape(0);
  int nread=shape(1);

  ODINLOG(odinlog,normalDebug) << "PhaseMap" << "(" << rd.coord().print() << ")/" << shape << " complete" << STD_endl;

  // FFT in read direction
  data.partial_fft(TinyVector<bool,2>(false,true));


  ComplexData<2> phasemap(shape);

  if(odd_even_echoes) { // phase correction for EPI

    // Calculate  phase-difference for each odd and even pair of echoes
    Data<float,2> phasediff(nechoes/2, nread);
    Data<float,2> sigma(nechoes/2, nread); // Error for fit
    for(int iecho=0; iecho<nechoes/2; iecho++) {
      for(int iread=0; iread<nread; iread++) {
        STD_complex odd=data(2*iecho+1,iread);
        STD_complex even=data(2*iecho,iread);
        if(cabs(even)) {
          phasediff(iecho,iread)=phase(odd/even);
        } else {
          phasediff(iecho,iread)=0.0;
        }
        sigma(iecho,iread)=secureInv(cabs(odd)+cabs(even));
      }
    }

    // Unwrap phase difference in read direction
    phasediff=ComplexData<2>(expc(float2imag(phasediff))).phasemap();


    // Linear fit to phase, weighted by signal magnitude for each odd-even-echo pair
    LinearFunction linf;
    Data<float,1> slope(nechoes/2);
    Data<float,1> fiterr(nechoes/2);
    Data<float,1> offset(nechoes/2);
    Data<float,1> phasediff_oneline(nread);
    for(int iecho=0; iecho<nechoes/2; iecho++) {
      phasediff_oneline(all)=phasediff(iecho,all);
      linf.fit(phasediff_oneline, sigma(iecho,all));
      slope(iecho)=linf.m.val;
      fiterr(iecho)=linf.m.err; // Use as error in 2nd stage fit
      offset(iecho)=linf.c.val;
    }

    // Fit slope and offset as a function of echo pair index
    LinearFunction linf_echo_slope;
    LinearFunction linf_echo_offset;
    linf_echo_slope.fit(slope,fiterr);
    linf_echo_offset.fit(offset,fiterr);
    Data<float,1> slope_fit(nechoes/2);
    Data<float,1> offset_fit(nechoes/2);
    for(int iecho=0; iecho<nechoes/2; iecho++) {
      slope_fit(iecho)=linf_echo_slope.m.val*float(iecho)+linf_echo_slope.c.val;
      offset_fit(iecho)=linf_echo_offset.m.val*float(iecho)+linf_echo_offset.c.val;
    }

    // to calculate fielmap modulate every even/odd echo with half of the calculated phase slope
    for(int iread=0; iread<nread; iread++) {
      for(int iecho=0; iecho<nechoes/2; iecho++) {
        float phase=slope_fit(iecho)*float(iread)+offset_fit(iecho);
        phasemap(2*iecho,  iread)=expc(float2imag(-0.5*phase));
        phasemap(2*iecho+1,iread)=expc(float2imag(+0.5*phase));
      }
      if(nechoes%2) { // take last slope if odd echoes
        float phase=slope_fit(nechoes/2-1)*float(iread)+offset_fit(nechoes/2-1);
        phasemap(nechoes-1, iread)=expc(float2imag(-0.5*phase));
      }
    }

  } else {  // odd_even_echoes

    ComplexData<1> oneline(nread);
    for(int iecho=0; iecho<nechoes; iecho++) {
      oneline=data(iecho,all);
      phasemap(iecho,all)=expc(float2imag(oneline.phasemap()));
// alternative: Polynomial fit to point-by-point phasemap:     phasemap(iecho,all)=expc(float2imag(polyniomial_fit(oneline.phasemap(), Data<float,1>(cabs(oneline)), 1, 3.0)));
    }
  }

  // Post data for each echo separately on blackboard
  RecoData one_echo_phasemap;
  one_echo_phasemap.data(Rank<1>()).resize(nread);
  one_echo_phasemap.coord()=rd.coord();
  for(int i=0; i<nechoes; i++) {
    one_echo_phasemap.data(Rank<1>())=phasemap(i,all);
    one_echo_phasemap.coord().index[echo]=i;
    controller.post_data(posted_phasemap_label, one_echo_phasemap);
  }

  // For debugging, apply phase correction to template data
  // and apss it to next functor
  data=data/phasemap;
  data.partial_fft(TinyVector<bool,2>(false,true),false);
  return execute_next_step(rd,controller);
}

//////////////////////////////////////////////////////////////

bool RecoPhaseMap::query(RecoQueryContext& context) {
  Log<Reco> odinlog(c_label(),"query");

  if(context.mode==RecoQueryContext::prep) {
    context.controller.announce_data(posted_phasemap_label);
    ODINLOG(odinlog,normalDebug) << "announced " << posted_phasemap_label << STD_endl;

    RecoCoord coord4count=context.coord;
    modify(coord4count);

    const CoordCountMap* countmap=context.controller.create_count_map(coord4count);
    if(!countmap) return false;

    odd_even_echoes=false;
    for(CoordCountMap::const_iterator it=countmap->begin(); it!=countmap->end(); ++it) {
      if(it->first.flags&recoReflectBit) {
        odd_even_echoes=true;
        break;
      }
    }
    ODINLOG(odinlog,normalDebug) << "odd_even_echoes=" << odd_even_echoes << STD_endl;
  }

  return RecoStep::query(context);
}


//////////////////////////////////////////////////////////////


bool RecoPhaseCorr::process(RecoData& rd, RecoController& controller) {
  Log<Reco> odinlog(c_label(),"process");

  if(!controller.data_announced(posted_phasemap_label)) {
    ODINLOG(odinlog,errorLog) << posted_phasemap_label << " not available" << STD_endl;
    return false;
  }

  RecoData one_echo_phasemap;
  one_echo_phasemap.coord()=rd.coord();
  RecoPhaseMap::modify4blackboard(one_echo_phasemap.coord());

  // override (zero) echo/epi index for phasemap retrieval
  one_echo_phasemap.coord().index[echo]=rd.coord().echopos;
  one_echo_phasemap.coord().index[epi]=rd.coord().echotrain;

  if(!controller.inquire_data(posted_phasemap_label, one_echo_phasemap)) return false;

  ComplexData<1>& pmap=one_echo_phasemap.data(Rank<1>());

  ComplexData<1>& adc=rd.data(Rank<1>());

  if(pmap.size()!=adc.size()) {
    ODINLOG(odinlog,errorLog) << "Size mismatch: " << pmap.size() << " != " << adc.size() << STD_endl;
    return false;
  }

  // FFT forward
  adc.fft(true);


  // Phase correction
  Range all=Range::all();
  adc(all)=adc(all)/pmap(all); // Blitz throws an error if arrays are used directly

  // FFT back
  adc.fft(false);


  return execute_next_step(rd,controller);
}
