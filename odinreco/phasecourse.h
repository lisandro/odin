/***************************************************************************
                          phasecourse.h  -  description
                             -------------------
    begin                : Thu May 28 2009
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef RECOPHASECOURSE_H
#define RECOPHASECOURSE_H

#include "step.h"

class RecoPhaseCourse : public RecoStep {

  // implementing virtual functions of RecoStep
  STD_string label() const {return "phasecourse";}
  STD_string description() const {return "Combine phase timecourse from multi-channel data";}
  bool process(RecoData& rd, RecoController& controller);
  RecoCoord input_coord() const {return RecoCoord::coord_with_mode(RecoIndex::collected,channel,repetition,line3d,line,readout);}
  void modify_coord(RecoCoord& coord) const {coord.set_mode(RecoIndex::single,channel);}
  RecoStep* allocate() const {return new RecoPhaseCourse;}
  void init() {}

};


#endif

