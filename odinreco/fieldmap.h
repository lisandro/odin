/***************************************************************************
                          fieldmap.h  -  description
                             -------------------
    begin                : Mon Feb 25 2007
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef RECOFIELDMAP_H
#define RECOFIELDMAP_H

#include "step.h"

static const char* posted_fmap_str="fieldmap";


class RecoFieldMap : public RecoStep {

  // implementing virtual functions of RecoStep
  STD_string label() const {return "fieldmap";}
  STD_string description() const {return "Calculate fieldmap";}
  bool process(RecoData& rd, RecoController& controller);
  RecoCoord input_coord() const {return RecoCoord::coord_with_mode(RecoIndex::collected,te,line3d,line,readout);}
  void modify_coord(RecoCoord& coord) const {coord.set_mode(RecoIndex::single, te);}
  RecoStep* allocate() const {return new RecoFieldMap;}
  void init() {}
};

//////////////////////////////////////////////////////////////

class RecoFieldMapUser : public RecoStep {

 public:
  bool get_fmap(Data<float,3>& fieldmap, const TinyVector<int,3>& dstshape, const RecoCoord& fmapcoord, RecoController& controller);

  static void modify4fieldmap(RecoCoord& coord);

 private:
  // cache interpolated fieldmaps
  typedef STD_map<RecoCoord, Data<float,3> > FieldMap;
  FieldMap fmap;
  Mutex fmapmutex;

};


#endif

