#include "epinavcorr.h"
#include "data.h"
#include "controller.h"

#include <odindata/fitting.h>


static const char* posted_phasemap_label="epinav";



bool RecoEpiNavScan::process(RecoData& rd, RecoController& controller) {
  Log<Reco> odinlog(c_label(),"process");

  Range all=Range::all();

  ComplexData<2>& indata=rd.data(Rank<2>());
  TinyVector<int,2> inshape(indata.shape());
  int nechoes=inshape(0);
  int nread=inshape(1);

  if(nechoes!=3) {
    ODINLOG(odinlog,errorLog) << "nechoes=" << nechoes << STD_endl;
    return false;
  }


  TinyVector<int,2> navshape(2,nread);
  ComplexData<2> navdata(navshape);


  navdata(0,all)=STD_complex(0.5)*(indata(0,all)+indata(2,all)); // average both even echoes
  navdata(1,all)=indata(1,all);

  // FFT in read direction
  navdata.partial_fft(TinyVector<bool,2>(false,true));


  ComplexData<2> phasemap(navshape);  // The result

  // Calculate  phase-difference
  Data<float,1> phasediff(nread);
  Data<float,1> sigma(nread); // Error for fit
  for(int iread=0; iread<nread; iread++) {
    STD_complex odd= navdata(1,iread);
    STD_complex even=navdata(0,iread);
    if(cabs(even)) {
      phasediff(iread)=phase(odd/even);
    } else {
      phasediff(iread)=0.0;
    }
    sigma(iread)=secureInv(cabs(odd)+cabs(even));
  }

  if(rd.coord().flags&recoEpiNavPolarityBit) phasediff=-phasediff; // Reversing phasediff to account for navigator polarity

  // Unwrap phase difference
  phasediff=ComplexData<1>(expc(float2imag(phasediff))).phasemap();


  // Linear fit to phasediff
  LinearFunction linf;
  linf.fit(phasediff, sigma);
  float slope=linf.m.val;
  float offset=linf.c.val;
  ODINLOG(odinlog,normalDebug) << "slope/offset=" << slope << "/" << offset << STD_endl;

  for(int iread=0; iread<nread; iread++) {
    float phase=slope*float(iread)+offset;
    phasemap(0, iread)=expc(float2imag(-0.5*phase));
    phasemap(1, iread)=expc(float2imag(+0.5*phase));
  }


//  int itrain=rd.coord().echotrain;
//  ODINLOG(odinlog,normalDebug) << "itrain=" << itrain << STD_endl;

  // Post data for each echo separately on blackboard
  RecoData one_echo_phasemap;
  one_echo_phasemap.data(Rank<1>()).resize(nread);
  one_echo_phasemap.coord()=rd.coord();
  modify4blackboard(one_echo_phasemap.coord());
//  one_echo_phasemap.coord().index[epi]=itrain;

  for(int i=0; i<2; i++) {
    one_echo_phasemap.data(Rank<1>())=phasemap(i,all);
    one_echo_phasemap.coord().index[echo]=i;
    controller.post_data(posted_phasemap_label, one_echo_phasemap);
  }

  return true;
}


//////////////////////////////////////////////////////////////

bool RecoEpiNavScan::query(RecoQueryContext& context) {
  Log<Reco> odinlog(c_label(),"query");

  if(context.mode==RecoQueryContext::prep) {
    context.controller.announce_data(posted_phasemap_label);
    ODINLOG(odinlog,normalDebug) << "announced " << posted_phasemap_label << STD_endl;
  }

  return RecoStep::query(context);
}

//////////////////////////////////////////////////////////////


bool RecoEpiNavCorr::process(RecoData& rd, RecoController& controller) {
  Log<Reco> odinlog(c_label(),"process");

  if(!controller.data_announced(posted_phasemap_label)) {
    ODINLOG(odinlog,errorLog) << posted_phasemap_label << " not available" << STD_endl;
    return false;
  }

  RecoData one_echo_phasemap;
  one_echo_phasemap.coord()=rd.coord();
  RecoEpiNavScan::modify4blackboard(one_echo_phasemap.coord());

//  int itrain=rd.coord().echotrain;
//  ODINLOG(odinlog,normalDebug) << "itrain=" << itrain << STD_endl;
//  one_echo_phasemap.coord().index[epi]=itrain;


  int iecho=bool(rd.coord().flags&recoReflectBit);
  ODINLOG(odinlog,normalDebug) << "iecho=" << iecho << STD_endl;
  one_echo_phasemap.coord().index[echo]=iecho;

  if(!controller.inquire_data(posted_phasemap_label, one_echo_phasemap)) return false;

  ComplexData<1>& pmap=one_echo_phasemap.data(Rank<1>());

  ComplexData<1>& adc=rd.data(Rank<1>());

  if(pmap.size()!=adc.size()) {
    ODINLOG(odinlog,errorLog) << "Size mismatch: " << pmap.size() << " != " << adc.size() << STD_endl;
    return false;
  }

  // FFT forward
  adc.fft(true);


  // Phase correction
  Range all=Range::all();
  adc(all)=adc(all)/pmap(all); // Blitz throws an error if arrays are used directly

  // FFT back
  adc.fft(false);


  return execute_next_step(rd,controller);
}
