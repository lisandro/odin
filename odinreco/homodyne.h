/***************************************************************************
                          homodyne.h  -  description
                             -------------------
    begin                : Mon Apr 2 2007
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef RECOHOMODYNE_H
#define RECOHOMODYNE_H


#include "step.h"
#include "measindex.h"


class RecoHomodyne : public RecoStep {

  // implementing virtual functions of RecoStep
  STD_string label() const {return "homodyne";}
  STD_string description() const {return "Homodyne reconstruction of partial Fourier data";}
  bool process(RecoData& rd, RecoController& controller);
  RecoCoord input_coord() const {return RecoCoord::coord_with_mode(RecoIndex::collected,line3d,line,readout);}
  void modify_coord(RecoCoord& coord) const {}
  bool query(RecoQueryContext& context);
  RecoStep* allocate() const {return new RecoHomodyne;}
  void init() {}


  bool weight_and_range(int dimindex, RecoData& rd, Range& symrange, ComplexData<1>& weight);

  // Phase-encoding lines actually measured
  RecoMeasIndex<line, RecoDim<5, line, line3d, channel, repetition, freq> > measlines;
  RecoMeasIndex<line3d, RecoDim<5, line, line3d, channel, repetition, freq> > measlines3d;

};




#endif

