/***************************************************************************
                          slidingwindow.h  -  description
                             -------------------
    begin                : Tue Sep 8 2009
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef RECOSLIDINGWINDOW_H
#define RECOSLIDINGWINDOW_H

#include "step.h"

class RecoSlidingWindow : public RecoStep {

  // implementing virtual functions of RecoStep
  STD_string label() const {return "slidingwindow";}
  STD_string description() const {return "Generate sliding window data";}
  bool process(RecoData& rd, RecoController& controller);
  RecoCoord input_coord() const {return RecoCoord::any();}
  void modify_coord(RecoCoord& coord) const {}
  RecoStep* allocate() const {return new RecoSlidingWindow;}
  void init() {}

};


#endif

