/***************************************************************************
                          reader_custom.h  -  description
                             -------------------
    begin                : Thu Oct 18 2007
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef RECOREADERCUSTOM_H
#define RECOREADERCUSTOM_H

#include "reader.h"


/**
  * @addtogroup odinreco
  * @{
  */



/**
  * Raw-data reader template to implement custom readers.
  */
class RecoReaderCustom : public virtual RecoReaderInterface {

 public:
  RecoReaderCustom(LDRblock&) {}

 private:

  // Implementing virtual functions of RecoReaderInterface
  bool init(const STD_string& input_filename);
  bool fetch(RecoCoord& coord, ComplexData<1>& adc);
  const STD_vector<RecoCoord>& get_coords() const {return coord_vec_cache;}
  dvector dim_values(recoDim dim) const;
  const TinyVector<float,3>& reloffset() const {return reloffset_cache;}
  STD_string image_proc() const {return "";}
  const TinyVector<int,3>& image_size() const {return size_cache;}
  const Protocol& protocol() const {return prot_cache;}
  STD_string seqrecipe() const {return "";} // Let controller generate recipe automatically
  STD_string postProc3D() const {return "";}
  STD_string preProc3D() const {return "";}
  STD_string cmdline_opts() const;

  // Cached values
  TinyVector<float,3> reloffset_cache;
  TinyVector<int,3> size_cache;
  Protocol prot_cache;

  ComplexData<2> rawdata; // synthetic raw data
  mutable STD_vector<RecoCoord> coord_vec_cache; // Vector to hold reco coordinates (indices)
  unsigned int count; // counter for ADCs

};



/** @}
  */


#endif

