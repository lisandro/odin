/***************************************************************************
                          enumbox.h  -  description
                             -------------------
    begin                : Tue May 29 2001
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef ENUMBOX_H
#define ENUMBOX_H

#include <qgroupbox.h>

#include "odinqt.h"

/**
  * This class provides a combobox with a nice frame
  */
class enumBox : public QGroupBox  {
   Q_OBJECT

public:
  enumBox(const svector& items,QWidget *parent,const char *name,bool editButton=false,bool infoButton=false);
  ~enumBox();

public slots:
  void setValue( int val );

signals:
  void newVal( int val );
  void edit();
  void info();


private slots:
  void emitNewVal(int val);
  void reportEditClicked();
  void reportInfoClicked();

 private:
  int old_val;
  GuiComboBox*   cb;
  GuiButton*     pb_edit;
  GuiButton*     pb_info;
  GuiGridLayout* grid;
};


#endif
