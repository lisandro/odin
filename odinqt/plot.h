/***************************************************************************
                          plot.h  -  description
                             -------------------
    begin                : Mon Aug 1 2005
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef PLOT_H
#define PLOT_H

#include <qobject.h> // Include Qt stuff first to avoid ambiguities with 'debug' symbol

#include "odinqt.h"

 // forward declarations
class QwtPlot;
class QwtPlotCurve;
class QwtPlotMarker;
class QwtPlotGrid;
class QRect;
class QwtWheel;
class QwtScaleDraw;
class GuiPlotPicker;


// work around different declarations of QwtDoubleRect
#if QT_VERSION >= 0x040000
class QRectF;
typedef QRectF QwtDoubleRect;
#else
class QwtDoubleRect;
#endif



/////////////////////////////////////////////////

/**
  *  Abstraction of a plotting widget
  */
class GuiPlot : public QObject {
   Q_OBJECT

 public:
  GuiPlot(QWidget *parent, bool fixed_size=false, int width=_ARRAY_WIDGET_WIDTH_-20, int height=_ARRAY_WIDGET_HEIGHT_-20);
  ~GuiPlot();

  void set_x_axis_label(const char *xAxisLabel, bool omit=false);
  void set_y_axis_label(const char *yAxisLabelLeft, const char *yAxisLabelRight=0);

  long insert_curve(bool use_right_y_axis=false, bool draw_spikes=false, bool baseline=false);

  long insert_marker(const char* label, double x, bool outline=false, bool horizontal=false, bool animate=false);

  void remove_marker(long id);

  void set_marker_pos(long id, double x);

  // memory NOT managed by GuiPlot
  void set_curve_data(long curveid, const double* x, const double* y, int n, bool symbol=false);

  void replot();

  void autoscale();

  void autoscale_y(double& maxBound);

  void rescale_y(double maxBound);

  void clear();

  void remove_markers();

  double get_x(int x_pixel) const;
  double get_y(int y_pixel, bool right_axes=false) const;

  long closest_curve(int x, int y, int& dist) const;

  void highlight_curve(long id, bool flag);

  void set_x_axis_scale(double lbound, double ubound);
  void set_y_axis_scale(double lbound, double ubound, bool right_axes=false);

  void set_curve_pen(long id, const char* color, int width=1);

  void set_rect_outline_style();
  void set_line_outline_style(bool horizontal);

  void enable_axes(bool flag); // TODO: Use ArrayScale::enable to control this setting
  void enable_grid(bool flag);

  void print(QPainter* painter, const QRect& rect) const;


  QWidget* get_widget();

 signals:
  void plotMousePressed(const QMouseEvent&);
  void plotMouseReleased(const QMouseEvent&);
  void plotMouseMoved(const QMouseEvent&);

 private slots:
  void emit_plotMousePressed(const QMouseEvent& qme);
  void emit_plotMouseReleased(const QMouseEvent& qme);
  void emit_plotMouseMoved(const QMouseEvent& qme);


 private:
  friend class GuiPlotPicker;

  void set_axis_label(int axisId, const char* label, bool omit, int alignment);

  QwtPlot* qwtplotter;

  GuiPlotPicker* picker;


  // Use functions with check to access curve_map/marker_map
  QwtPlotCurve* get_curve(long id);
  QwtPlotMarker* get_marker(long id);


  // to emulate Qwt4 behaviour on Qwt5
  STD_map<long,QwtPlotCurve*>  curve_map;
  STD_map<long,QwtPlotMarker*> marker_map;

  QwtPlotGrid* plotgrid;

  int canvas_framewidth;

  long baseline_id_cache;
};

////////////////////////////////////////////////////////////


/**
  *  Abstraction of a slider wheel
  */
class GuiWheel : public QObject {
   Q_OBJECT

 public:
  GuiWheel(QWidget *parent);
  ~GuiWheel();

  void set_range(double min, double max);

  void set_value(double newval);

  QWidget* get_widget();

 signals:
  void valueChanged(double);

 private slots:
  void emit_valueChanged(double newval);

 private:
  QwtWheel* wheel;

};


#endif
