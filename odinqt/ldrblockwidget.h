/***************************************************************************
                          ldrblockwidget.h  -  description
                             -------------------
    begin                : Mon Aug 5 2005
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef LDRBLOCKWIDGET_H
#define LDRBLOCKWIDGET_H

#include <qgroupbox.h>

#include "odinqt.h"

#include <odinpara/ldrblock.h>

class LDRwidgetDialog; // forward declaration

////////////////////////////////////////////////////////////

class LDRblockGrid : public QWidget  {

 Q_OBJECT

 public:
  LDRblockGrid(LDRblock& block,unsigned int columns=1,QWidget *parent=0,const char* omittext="");

 signals:
  // public:
  void valueChanged();

  // private:
  void updateSubWidget();
  void deleteSubDialogs();

 public slots:
  void updateWidget();
  void deleteDialogs();
  void swapSliderTracking() {}
  void emitValueChanged() {emit valueChanged();}


 private:
  friend class LDRblockScrollView;

  void createDialog();

  GuiGridLayout* grid;

  LDRblock& val;
  STD_list<LDRwidgetDialog*> subdialogs;

};

////////////////////////////////////////////////////////////

class LDRblockScrollView : public QObject {
 Q_OBJECT

 public:
  LDRblockScrollView(LDRblock& block, unsigned int columns=1, QWidget *parent=0, const char* omittext="");
  ~LDRblockScrollView();

//  QSize gridsize() const {return ldrgrid->frameSize();}

  QWidget* get_widget() {return scroll->get_widget();}

 signals:
  void valueChanged();

 public slots:
  void updateWidget() {ldrgrid->updateWidget();}
  void deleteDialogs() {ldrgrid->deleteDialogs();}
  void swapSliderTracking() {ldrgrid->swapSliderTracking();}
  void emitValueChanged() {emit valueChanged();}

 private:
  friend class LDRblockWidget;

  void createDialog() {ldrgrid->createDialog();}

  GuiScroll* scroll;
  LDRblockGrid* ldrgrid;

};

////////////////////////////////////////////////////////////

class LDRblockWidget : public QGroupBox  {

 Q_OBJECT

 public:
  LDRblockWidget(LDRblock& ldrblock,unsigned int columns=1,QWidget *parent=0,bool doneButton=false,bool is_dialog=false,const char* omittext="", bool storeLoadButtons=false, bool readonly=false);
  ~LDRblockWidget();

//  QSize gridsize() const {if(ldrscroll) return ldrscroll->gridsize(); else return QSize();}

 signals:
  void valueChanged();
  void doneButtonPressed();


 public slots:
  void updateWidget() {if(ldrscroll) ldrscroll->updateWidget();}
  void deleteDialogs() {if(ldrscroll) ldrscroll->deleteDialogs();}
  void swapSliderTracking() {if(ldrscroll) ldrscroll->swapSliderTracking();}
  void createDialog();
  void emitValueChanged() {emit valueChanged();}

 private slots:
  void emitDone();
  void storeBlock();
  void loadBlock();

 private:

  GuiGridLayout *grid;
  GuiButton *pb_done;
  GuiButton *pb_edit;
  GuiButton *pb_store;
  GuiButton *pb_load;

  LDRblock& parblock;

  LDRblockScrollView* ldrscroll;

  GuiListView* noeditlist;
  STD_vector<GuiListItem*> noedititems;
};

////////////////////////////////////////////////////////////





class LDRwidgetDialog : public QObject, public GuiDialog  {
   Q_OBJECT
public:
  LDRwidgetDialog(LDRblock& ldr,unsigned int columns=1,QWidget *parent=0, bool modal=false, bool readonly=false);
  ~LDRwidgetDialog();

 public slots:
  void updateWidget();
  void emitChanged();

 private:
  GuiGridLayout *grid;
  LDRblockWidget *ldrwidget;


private slots:
  void callDone();

signals:
  void finished();
  void valueChanged();


};


#endif
