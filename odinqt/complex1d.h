/***************************************************************************
                          complex1d.h  -  description
                             -------------------
    begin                : Sun Aug 27 2000
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef COMPLEX1D_H
#define COMPLEX1D_H

#include <qgroupbox.h>

#include "odinqt.h"

#define SYMBOL_MAX_NUMOF_POINTS 20

class GuiPlot; // forward declaration

class DetachedComplexfloatBox1D; // forward declaration

/**
  * QLabel containing a plotted complex array (2 curves)
  */
class complexfloatBox1D : public QGroupBox  {
   Q_OBJECT

 public:
  complexfloatBox1D(const float *data1, const float *data2, int n,QWidget *parent, const char *name, bool fixed_size, const char *xAxisLabel=0, const char *yAxisLabelLeft=0, const char *yAxisLabelRight=0, float min_x=0.0, float max_x=0.0, bool detachable=false);
  complexfloatBox1D(const double *data1,const double *data2,int n,QWidget *parent, const char *name, bool fixed_size, const char *xAxisLabel=0, const char *yAxisLabelLeft=0, const char *yAxisLabelRight=0, float min_x=0.0, float max_x=0.0, bool detachable=false);
  ~complexfloatBox1D();


 public slots:
  void refresh(const float *data1,const float *data2,int n, float min_x, float max_x);
  void refresh(const float *data1,const float *data2,int n) {refresh(data1,data2,n,0.0,0.0);}

  void refresh(const double *data1,const double *data2,int n, float min_x, float max_x);
  void refresh(const double *data1,const double *data2,int n) {refresh(data1,data2,n,0.0,0.0);}



 private slots:
  void autoscale();
  void detach();

  void mousePressedInPlot(const QMouseEvent& qme);
  void mouseReleasedInPlot(const QMouseEvent& qme);


 private:
  void common_init(const char *name, bool fixed_size, bool data1, bool data2, const char *xAxisLabel, const char *yAxisLabelLeft, const char *yAxisLabelRight, bool detachable);

  void create_x_cache(float min_x, float max_x, int n);


  long curveid1;
  long curveid2;

  dvector data1_cache;
  dvector data2_cache;
  dvector x_cache;
  const double* data1_ptr;
  const double* data2_ptr;

  // Use strings to cache labels since original arrays may be outdated
  STD_string name_cache;
  STD_string xAxisLabel_cache;
  STD_string yAxisLabelLeft_cache;
  STD_string yAxisLabelRight_cache;
  float min_x_cache;
  float max_x_cache;
  int n_cache;
  bool detachable_cache;


  GuiGridLayout *grid;
  GuiPlot* plotter;

  int x_pressed,y_pressed;

  DetachedComplexfloatBox1D* detached;
};


///////////////////////////////////////////////////////////////////


class DetachedComplexfloatBox1D : public GuiDialog  {

 public:
  DetachedComplexfloatBox1D(const double *data1,const double *data2,int n,complexfloatBox1D *parent, const char *name, bool fixed_size, const char *xAxisLabel, const char *yAxisLabelLeft, const char *yAxisLabelRight, float min_x, float max_x);

  ~DetachedComplexfloatBox1D();

  void refresh(const double *data1,const double *data2,int n, float min_x, float max_x);

 private:
  void create_grid();

  GuiGridLayout *grid;
  complexfloatBox1D* cfb;

};

#endif
