/***************************************************************************
                          odinqt.h  -  description
                             -------------------
    begin                : Sun Aug 27 2000
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef ODINQT_H
#define ODINQT_H

#include <tjutils/tjlog.h>
#include <tjutils/tjstatic.h>
#include <tjutils/tjvector.h> // for svector


// global configuration
#define _FONT_TYPE_ "helvetica"
#define _ARRAY_WIDGET_WIDTH_  270
#define _ARRAY_WIDGET_HEIGHT_ 180
#define _ARRAY_BACKGROUND_COLOR_ "Black"
#define _ARRAY_FOREGROUND_COLOR1_ "White"
#define _ARRAY_FOREGROUND_COLOR2_ "Grey"
#define _ARRAY_GRID_COLOR_        "Green"
#define _ARRAY_GRID_DARK_FACTOR_  250
#define _ARRAY_SELECTION_COLOR_   "Yellow"
#define _ARRAY_HIGHLIGHT_COLOR_   "Orange"
#define _ARRAY_MARKER_COLOR_   "Blue"
#define _ARRAY_MARKER_BRIGHT_FACTOR_  180
#define _LAYOUT_STRETCH_FACTOR_ 1
#define SLIDER_CELL_WIDTH 75
#define SLIDER_CELL_HEIGHT 25
#define TEXTEDIT_WIDTH 75
#define TEXTEDIT_HEIGHT 25
#define PLOT_SYMBOLS_SIZE 5


// platform-specific configuration
#ifdef USING_WIN32
#define _FONT_SIZE_ 8
#else
#define _FONT_SIZE_ 10
#endif

/////////////////////////////////////////////////

//  forward declarations
class QObject;
class QWidget;
class QString;
class QGridLayout;
class QPushButton;
class QLineEdit;
class QPopupMenu;
class QMenu;
class QKeySequence;
class QPoint;
class QSlider;
class QComboBox;
class QLabel;
class QPainter;
class QPixmap;
class QMouseEvent;
class QColor;
class QString;
class QRegion;
class QApplication;
class QScrollView;
class QScrollArea;
class QDialog;
class QMainWindow;
class QProgressDialog;
class QImage;
class QListView;
class QListViewItem;
class QCheckListItem;
class QTableWidget;
class QTableWidgetItem;
class QTreeWidget;
class QTreeWidgetItem;
class QProgressBar;
class QScrollBar;
class QToolBar;
class QToolButton;
class QPrinter;
class QPaintDevice;
class QWizard;
class QTextEdit;
class QTextView;

class SlotDispatcher;

/////////////////////////////////////////////////

/**
  *  Helper classfor debugging the OdinQt component
  */
class OdinQt {
 public:
  static const char* get_compName();
};


/////////////////////////////////////////////////
// Helper functions for Qt portability

const char* c_str(const QString& qs);

int layout_border_size();

void init_label(QLabel* ql);

bool left_button(const QMouseEvent* qme, bool return_current_state);
bool right_button(const QMouseEvent* qme, bool return_current_state);
bool middle_button(const QMouseEvent* qme, bool return_current_state);

void add_tooltip(QWidget* w, const char* txt);

void set_platform_defaults(QWidget* w, bool enable=true);


STD_string get_save_filename(const char* caption=0, const char* startwith="", const char*  filter="", QWidget* parent=0);
STD_string get_open_filename(const char* caption=0, const char* startwith="", const char*  filter="", QWidget* parent=0);
STD_string get_directory(const char* caption=0, const char* startwith="", QWidget* parent=0);

bool message_question(const char* text, const char* caption, QWidget* parent, bool ask=false, bool error=false);


int paintdevice_height(const QPaintDevice* qpd);
int paintdevice_width(const QPaintDevice* qpd);

svector get_possible_image_fileformats();


/////////////////////////////////////////////////
// Helper classes for Qt portability

/**
  *  Abstraction of a grid layout
  */
class GuiGridLayout {

 public:
  GuiGridLayout(QWidget *parent, int rows, int columns, bool margin=true);
  ~GuiGridLayout();

  enum Alignment {Default, VCenter, Center};

  void add_widget(QWidget *w, int row, int column, Alignment alignment=Default, int rowSpan=1, int columnSpan=1 );

  void set_col_stretch(int col, int factor);
  void set_row_stretch(int row, int factor);

  void set_col_minsize(int col, int minsize);
  void set_row_minsize(int row, int minsize);

 private:
  QGridLayout* qgl;
};

/////////////////////////////////////////////////

/**
  *  Abstraction of a push button
  */
class GuiButton {

 public:
  GuiButton(QWidget *parent, QObject* receiver, const char* member, const char *onlabel, const char *offlabel=0, bool initstate=false);
  ~GuiButton();

  void set_toggled(bool state);

  void set_text(bool state);

  void set_default(bool state);

  bool is_on() const;

  QWidget* get_widget();

 private:
  friend class SlotDispatcher;
  QPushButton* qpb;
  SlotDispatcher* sd;
  const char* ontext;
  const char* offtext;
};

/////////////////////////////////////////////////

/**
  *  Abstraction of a text editing field
  */
class GuiLineEdit {

 public:
  GuiLineEdit(QWidget *parent, QObject* receiver, const char* member, int width=-1, int height=-1);
  ~GuiLineEdit();

  void set_text(const char* txt);

  const char* get_text() const;

  bool is_modified();

  QWidget* get_widget();

 private:
  friend class SlotDispatcher;
  QLineEdit* qle;
  SlotDispatcher* sd;
};


/////////////////////////////////////////////////

/**
  *  Abstraction of a popup menu
  */
class GuiPopupMenu {

 public:
  GuiPopupMenu(QWidget *parent);
  ~GuiPopupMenu();

  void insert_item(const char* text, const QObject* receiver, const char* member, int accel=0);
  void insert_separator();

  void popup(const QPoint& p);

 private:
  friend class GuiMainWindow;

  QPopupMenu* qpm; // for Qt pre4
  QMenu* qm; // for Qt4
  int id;
};

/////////////////////////////////////////////////

/**
  *  Abstraction of a slider
  */
class GuiSlider {

 public:
  GuiSlider(QWidget *parent, int minValue, int maxValue, int pageStep, int value, int tickInterval);
  ~GuiSlider();

  int get_value() const;
  void set_value(int val);
  bool modified_externally() const {bool result=modified; modified=false; return result;}

  QSlider* get_widget()  {return qs;}

 private:
  QSlider* qs;
  mutable bool modified;
};

/////////////////////////////////////////////////

class GuiToolBar; // forward declarion

/**
  *  Abstraction of a combo box
  */
class GuiComboBox {

 public:
  GuiComboBox(QWidget* parent, const svector& names);
  GuiComboBox(GuiToolBar* parent, const svector& names);
  ~GuiComboBox();

  void set_names(const svector& names);

  void set_current_item(int index);
  int get_current_item() const;

  QComboBox* get_widget()  {return qcb;}

 private:
  void common_init(QWidget* parent, const svector& names);
  QComboBox* qcb;
};

/////////////////////////////////////////////////

/**
  *  Abstraction of a painter
  */
class GuiPainter {

 public:
  GuiPainter(QPixmap* qpm);
  ~GuiPainter();

  // forwarding functions to QPainter
  void moveTo(int x, int y);
  void lineTo(int x, int y);
  bool begin(QPixmap* qpm);
  bool end();
  void setPen(const char* pencolor, int linewidth=1, bool dotted=false, float lightdark=0.0);
  void fillRect( int x, int y, int w, int h, const QColor& col);
  void drawRect(int x, int y, int w, int h);
  void drawText( int x, int y, const QString& txt, const QColor& col);

  QRegion* draw_region(const STD_list<QPoint>& plist);

  QPainter* get_painter() {return qp;}

  void repaint(QLabel* dst);

 private:
  QPainter* qp;
  QPixmap* qpm_cache;
  int x_cache, y_cache;
};

/////////////////////////////////////////////////

/**
  *  Abstraction of a GUI application
  */
class GuiApplication {

 public:
  GuiApplication(int argc=0, char *argv[]=0);
  ~GuiApplication();

  int start(QWidget* mainwidget);

  QObject* get_object();

  static void quit();

  static int argc();
  static char** argv();

  static void process_events();

 private:
  QApplication* qa;

  // Cache cmdline args separately for Qt and for later use
  static int argc_cache;
  static char** argv_cache;
  static int argc4qt;
  static char** argv4qt;

};

/////////////////////////////////////////////////

/**
  *  Abstraction of a GUI main window
  */
class GuiMainWindow {

 public:
  GuiMainWindow(QWidget* parent = 0);
  ~GuiMainWindow();

  void show(QWidget* central_widget, bool show_toolbutton_text=false);

  void set_caption(const char* text);

  void set_status_message(const char* text, int timeout_ms=0);

  void set_status_xpm(const char** xpm);

  QWidget* get_widget();

  void insert_menu( const char* text, GuiPopupMenu * gpm); 

  void insert_menu_separator(); 

  void close();


 private:
  friend class GuiToolBar;

  QMainWindow* qmw;
  QLabel* statusIcon;
  QLabel* statusText;
};


/////////////////////////////////////////////////

/**
  *  Abstraction of a scrollable window/area
  */
class GuiScroll {

 public:
  GuiScroll(QWidget* child, QWidget* parent);
  ~GuiScroll();

  QWidget* get_widget();

 private:
  QScrollView* qsv; // for Qt pre4
  QScrollArea* qsa; // for Qt4
};


/////////////////////////////////////////////////

class QDialogDerived; // to overload paintEvent

/**
  *  Abstraction of a dialog
  */
class GuiDialog {

 public:
  GuiDialog(QWidget *parent, const char* caption, bool modal=false);
  virtual ~GuiDialog();

  QWidget* get_widget();

  virtual void repaint() {} // overload to repaint dialog
  virtual void close() {} // overload to perform additional stuff when closing dialog

  void show();
  void cancel();
  void done();
  void hide();
  int exec();

 private:
  QDialogDerived* qd;
};

/////////////////////////////////////////////////

/**
  *  Abstraction of a progress dialog
  */
class GuiProgressDialog {

 public:
  GuiProgressDialog(QWidget *parent, bool modal, int total_steps);
  ~GuiProgressDialog();

  void set_progress(int progr);
  int get_progress() const;

  void set_total_steps(int steps);

  bool was_cancelled() const;

  void reset();

  void set_text(const char* txt);

  void show();
  void hide();

 private:
  QProgressDialog* qpd;
};

/////////////////////////////////////////////////

/**
  *  Abstraction of an image class
  */
class GuiImage {

 public:
  GuiImage(unsigned char* data, int width, int height, bool colormap);
  ~GuiImage();

  QPixmap* create_pixmap() const;

 private:
  QImage* qi;
};

/////////////////////////////////////////////////

class GuiListItem; // forward declaration


class GuiListViewCallback {

 public:
  virtual void clicked(GuiListItem* item) = 0;

 protected:
  GuiListViewCallback() {}
};



/**
  *  Abstraction of a list/tree view class
  */
class GuiListView {

 public:
  GuiListView(QWidget *parent, const svector& column_labels, int first_column_width=-1, int min_height=-1, GuiListViewCallback* callback=0, bool tree=false);
  ~GuiListView();

  QWidget* get_widget();

 private:
  friend class GuiListItem;
  QListView* qlv; // for Qt pre4
  QTableWidget* qtw; // for Qt4
  QTreeWidget* qtrw; // for Qt4
  SlotDispatcher* sd;
};

/////////////////////////////////////////////////

/**
  *  Abstraction of a list/tree view item
  */
class GuiListItem : public StaticHandler<GuiListItem> {

 public:
  GuiListItem() {common_init();} // used for constructing listmap
  GuiListItem(GuiListView *parent, const svector& column_entries, bool checkable=false, bool initstate=false);
  GuiListItem(GuiListItem *parent, GuiListItem *after, const svector& column_entries);
  ~GuiListItem();

  bool is_checked() const;

  const char* get_text() const;

  // functions to initialize/delete static members by the StaticHandler template class
  static void init_static();
  static void destroy_static();

 private:
  friend class SlotDispatcher;
  void common_init();
  QListViewItem* qlvi; // for Qt pre4
  QCheckListItem* qcli; // for Qt pre4
  QTableWidgetItem* qtwi; // for Qt4
  QTreeWidgetItem* qtrwi; // for Qt4
  QTreeWidget* parent_qtrw; // for Qt4

  // global map to retrieve GuiListItem from QListViewItem
  static STD_map<QListViewItem*,GuiListItem*>*    listmap; // for Qt pre4
  static STD_map<QTableWidgetItem*,GuiListItem*>* tablemap; // for Qt4
};

/////////////////////////////////////////////////

/**
  *  Abstraction of a progress bar
  */
class GuiProgressBar {

 public:
  GuiProgressBar(QWidget *parent, int total_steps);
  ~GuiProgressBar();

  void set_progress(int progr);

  void set_min_width(int w);

  QWidget* get_widget();

 private:
  QProgressBar* qpb;
};

/////////////////////////////////////////////////

/**
  *  Abstraction of a scroll bar
  */
class GuiScrollBar {

 public:
  GuiScrollBar(QWidget* parent);
  ~GuiScrollBar();

  void set_values(int min, int max, int linestep, int pagestep, int value);

  QScrollBar* get_widget();

 private:
  QScrollBar* qsb;
};

/////////////////////////////////////////////////

/**
  *  Abstraction of a tool bar
  */
class GuiToolBar {

 public:
  GuiToolBar(GuiMainWindow* parent, const char* label);
  ~GuiToolBar();

  QWidget* get_widget();

  void add_separator();

 private:
  friend class GuiToolButton;
  friend class GuiComboBox;
  QToolBar* qtb;
};

/////////////////////////////////////////////////

/**
  *  Abstraction of a tool button
  */
class GuiToolButton {

 public:
  GuiToolButton(GuiToolBar* parent, const char** xpm, const char* label, QObject* receiver, const char* member, bool checkable=false, bool initstate=false );
  ~GuiToolButton();

  bool is_on() const;
  void set_on(bool flag);

  void set_enabled(bool flag);

  void set_label(const char* text);
  void set_tooltip(const char* text);

 private:
  friend class SlotDispatcher;
  QToolButton* qtb;
  SlotDispatcher* sd;
};


/////////////////////////////////////////////////

/**
  *  Abstraction of a printer driver
  */
class GuiPrinter {

 public:
  GuiPrinter();
  ~GuiPrinter();

  bool setup(QWidget* parent);

  QPaintDevice* get_device();

 private:
  QPrinter* qp;
};

/////////////////////////////////////////////////

/**
  *  Abstraction of a text viewer
  */
class GuiTextView {

 public:
  GuiTextView(QWidget* parent, int minwidth, int minheight);
  ~GuiTextView();

  void set_text(const char* txt);
  void append_text(const char* txt);

  QWidget* get_widget();

 private:

  void scroll_end();

  QTextEdit* qte;
};

#endif
