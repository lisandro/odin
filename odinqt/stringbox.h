/***************************************************************************
                          intedit.h  -  description
                             -------------------
    begin                : Sun Aug 27 2000
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef STRINGBOX_H
#define STRINGBOX_H

#include <qgroupbox.h>

#include "odinqt.h"

/**
  * This class contains a QlineEdit to display a string
  * in a nice box with a label.
  */
class stringBox : public QGroupBox  {
   Q_OBJECT

 public:
  stringBox(const char* text, QWidget *parent, const char *name, const char *buttontext=0);
  ~stringBox();

 public slots:
  void setstringBoxText( const char* text );


 private slots:
  void reportTextChanged();
  void reportButtonClicked();


 signals:
  void stringBoxTextEntered( const char* text );
  void stringBoxButtonPressed();

 private:
  GuiGridLayout* grid;
  GuiLineEdit* le;
  GuiButton* pb;

};


#endif
