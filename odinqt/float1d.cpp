/***************************************************************************
                          float1d.cpp  -  description
                             -------------------
    begin                : Tue Jul 11 2000
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "float1d.h"


floatBox1D::floatBox1D(const float *data,int n,QWidget *parent, const char *name, bool fixed_size, const char *xAxisLabel, const char *yAxisLabel, float min_x, float max_x, bool detachable)
    : complexfloatBox1D(data,0,n,parent,name, fixed_size, xAxisLabel, yAxisLabel, 0, min_x, max_x, detachable ) {
}

floatBox1D::floatBox1D(const double *data,int n,QWidget *parent, const char *name, bool fixed_size, const char *xAxisLabel, const char *yAxisLabel, float min_x, float max_x, bool detachable)
    : complexfloatBox1D(data,0,n,parent,name, fixed_size, xAxisLabel, yAxisLabel, 0, min_x, max_x, detachable ) {
}

void floatBox1D::refresh(const float* data,int n, float min_x, float max_x) {
  complexfloatBox1D::refresh(data,0,n, min_x, max_x );
}

void floatBox1D::refresh(const double* data,int n, float min_x, float max_x) {
  complexfloatBox1D::refresh(data,0,n, min_x, max_x );
}
