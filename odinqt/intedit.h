/***************************************************************************
                          intedit.h  -  description
                             -------------------
    begin                : Sun Aug 27 2000
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef INTEDIT_H
#define INTEDIT_H

#include <qgroupbox.h>

#include "odinqt.h"

/**
  * This class implements a line edit with int values only.
  */
class intLineEdit : public QObject  {
   Q_OBJECT

 public:
  intLineEdit(int minValue, int maxValue, int value, QWidget *parent, const char *name, int width, int height );
  ~intLineEdit();

  QWidget* get_widget() {return gle->get_widget();}

 public slots:
  void setintLineEditValue( int value );

 private slots:
  void emitSignal( );

 signals:
  void intLineEditValueChanged( int value );

 private:

  void set_value(int value);

  GuiLineEdit* gle;

};


////////////////////////////////////////////////////////////


/**
  * This class contains a intlineedit to display an int value
  * in a nice box with a label.
  */
class intLineBox : public QGroupBox  {
   Q_OBJECT

 public:
  intLineBox(int value, QWidget *parent, const char *name);
  ~intLineBox();

 public slots:
  void setintLineBoxValue( int value );

 private slots:
  void emitSignal( int  value );

 signals:
  void intLineBoxValueChanged( int  value );
  void SignalToChild( int  value );

 private:
  GuiGridLayout* grid;
  intLineEdit *le;

};

////////////////////////////////////////////////////////////



/**
  * This class implements a slider useful for scientific applications
  * by composing a QSlider(int values) and a intLineEdit into one widget.
  */
class intScientSlider : public QGroupBox  {
   Q_OBJECT

 public:
  intScientSlider(int minValue, int maxValue, int Step, int value, QWidget *parent, const char *name);
  ~intScientSlider();

 public slots:
  void setintScientSliderValue( int value );


 private slots:
  void emitSignal( int value );

 signals:
  void intScientSliderValueChanged( int value );

 private:
  int value;
  GuiSlider* slider;
  GuiGridLayout* grid;
  intLineEdit *le;

};

////////////////////////////////////////////////////////////


#endif
