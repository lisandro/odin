/***************************************************************************
                          ldrwidget.h  -  description
                             -------------------
    begin                : Mon Apr 15 2002
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef LDRWIDGET_H
#define LDRWIDGET_H

#include <qwidget.h>

#include "odinqt.h"


#include <tjutils/tjarray.h>
#include <odinpara/ldrbase.h>

///////////////////////////////////////////////////////////////

#define _MAX_SLIDER_STEPS_ 100
#define _MAX_LABEL_LENGTH_ 15
#define _FLOAT_DIGITS_ 3

#define _INFOBOX_LINEWIDTH_ 50

#define _MAX_SUBWIDGETS_FOR_ALIGNCENT_ 5

///////////////////////////////////////////////////////////////

// forward declarations to decouple headers as far as possible
class LDRbase;
class intScientSlider;
class intLineBox;
class floatScientSlider;
class floatLineBox;
class enumBox;
class buttonBox;
class floatBox1D;
class floatBox3D;
class complexfloatBox1D;
class stringBox;
class floatLineBox3D;

class LDRwidgetDialog;
class LDRblockWidget;
class LDReditCaller;

///////////////////////////////////////////////////////////////


/**
  *
  * This class produces a widget according to a given Labeled Data Record (LDR) or a block
  * of them so that the parameter(s) can be edited interactively
  */
class LDRwidget : public QWidget  {
   Q_OBJECT

 public:

/**
  *
  *Constructs a widget for the parameter(s) 'ldr' with the following additional options:
  * - columns: If creating a widget for a block of parameters they will be grouped in the given number of columns
  * - parent:  The parent widget
  * - doneButton: If creating a widget for a block of parameters a 'Done' push button will be added if this option is set to 'true'
  *               The push button will then emit the doneButtonPressed() signal if clicked
  * - omittext: Text to blind out in the label of each parameter
  * - storeLoadButtons: Add buttons to store/load the parameters to/from file
  *
  */
  LDRwidget(LDRbase& ldr,unsigned int columns=1, QWidget *parent=0, bool doneButton=false, const char* omittext="", bool storeLoadButtons=false);
  ~LDRwidget();


  // members for 2D/3D plot
  void write_pixmap(const char* fname, const char* format, bool dump_all=false) const;
  void write_legend(const char* fname, const char* format) const;
  void write_map_legend(const char* fname, const char* format) const;


 signals:
/**
  * This signal will be emitted whenever one of the parameters is changed
  */
  void valueChanged();

/**
  * This signal will be emitted whenever the 'Done' button is clicked
  */
  void doneButtonPressed();

  // signals for 2D/3D plot
  void clicked(int x, int y, int z);
  void newProfile(const float *data, int npts, bool horizontal, int position);
  void newMask(const float* data, int slice);


  // private:
  void updateSubWidget();
  void deleteSubDialogs();

  void newintval(int);
  void newfloatval(float);
  void newenumval(int);
  void newboolval(bool);
  void newfloatArr1( const float*, int, float, float);
//  void newdoubleArr1( const double*, int, float, float);
  void newfloatArr2( const float*, float, float);
  void newfloatArrMap(const float*, float, float, float);
  void newcomplexArr( const float*, const float*, int, float, float);
  void newfuncval(int);
  void newstringval(const char*);
  void newfilenameval(const char*);
  void newformulaval(const char*);
  void newtripleval(float,float,float);


 public slots:

/**
  * Tell the widget that the parameter(s) were changed outside and that it has to update its view
  */
  void updateWidget();

/**
  * Delete all dialogs that may have popped up during editing the parameters
  */
  void deleteDialogs();


 private slots:
  void emitValueChanged();
  void emitDone();
  void emitClicked(int x, int y, int z) {emit clicked(x,y,z);}
  void emitNewProfile(const float *data, int npts, bool horizontal, int position) {emit newProfile(data, npts, horizontal, position);}
  void emitNewMask(const float *data, int slice) {emit newMask(data,slice);}

  void changeLDRint( int );
  void changeLDRfloat( float );
  void changeLDRenum( int );
  void changeLDRbool ( bool );
  void changeLDRaction();
  void changeLDRfunction( int );
  void changeLDRstring(const char*);
  void changeLDRfileName(const char*);
  void browseLDRfileName();
  void changeLDRformula(const char*);
  void changeLDRtriple(float,float,float);
  void infoLDRformula();
  void editLDRfunction();
  void infoLDRfunction();

 public:
  STD_string get_label() const;
  unsigned int get_rows() const {return rows;}
  unsigned int get_cols() const {return cols;}


 private:

  void set_widget(QWidget *widget, GuiGridLayout::Alignment alignment = GuiGridLayout::Default, bool override_enabled=false);

  unsigned int get_sizedfarray_size_and_factor(unsigned int& nx, unsigned int& ny, unsigned int& nz) const;

  void create_or_update_floatArrwidget(const farray& arr, bool initial);



  GuiGridLayout* grid;

  QWidget* widget_cache;

  LDRblockWidget* blockwidget;
  intScientSlider*   intslider;
  intLineBox*        intedit;
  floatScientSlider* floatslider;
  floatLineBox*      floatedit;
  enumBox*           enumwidget;
  buttonBox*         boolwidget;
  buttonBox*         actionwidget;
  QLabel*            floatArrempty;
  floatBox1D*        floatArrwidget1;
  floatLineBox*      floatArredit;
  floatBox3D*        floatArrwidget2;
  complexfloatBox1D* complexArrwidget;
  stringBox*         stringwidget;
  stringBox*         filenamewidget;
  enumBox*           funcwidget;
  stringBox*         formulawidget;
  floatLineBox3D*    triplewidget;

  // float 2D/3D stuff
  farray sizedfarray;
  farray mapfarray;
  ndim oldfarraysize;
  farray overlay_map;

  QWidget* vport;

  LDRbase& val;
  STD_string ldrlabel;
  STD_string ldrlabel_uncut;
  bool label_cut;

  STD_list<LDRwidgetDialog*> subdialogs;

  unsigned int rows,cols;

};




#endif
