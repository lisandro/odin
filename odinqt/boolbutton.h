/***************************************************************************
                          boolbutton.h  -  description
                             -------------------
    begin                : Sun Jul 16 2000
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef BOOLBUTTON_H
#define BOOLBUTTON_H

#include <qgroupbox.h>

#include "odinqt.h"

/**
  *This class provides a button with a nice frame
  */
class buttonBox : public QGroupBox {
   Q_OBJECT

public: 
  buttonBox(const char *text, QWidget *parent, const char *buttonlabel);
  buttonBox(const char *ontext,const char *offtext, bool initstate, QWidget *parent, const char *buttonlabel);
  ~buttonBox();

  bool is_on() const {return gb->is_on();}

public slots:
  void setToggleState(bool);

private slots:
  void reportclicked();
  void setButtonState();


signals:
  void	buttonClicked();
  void	buttonToggled(bool);

private:
  GuiGridLayout* grid;
  GuiButton* gb;
};

#endif
