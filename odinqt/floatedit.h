/***************************************************************************
                          floatedit.h  -  description
                             -------------------
    begin                : Sun Aug 27 2000
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef FLOATEDIT_H
#define FLOATEDIT_H

#include <qgroupbox.h>

#include "odinqt.h"

/**
  * This class implements a line edit with float values only; A number
  * of digits to display must be specified.
  */
class floatLineEdit : public QObject {
   Q_OBJECT

 public:
  floatLineEdit(float minValue, float maxValue, float value, int digits, QWidget *parent, const char *name, int width, int height );
  ~floatLineEdit();

  QWidget* get_widget() {return gle->get_widget();}

  // required public for range slider
  void set_value(float value);
  float get_value() const {return value_cache;}

 public slots:
  void setfloatLineEditValue( float value);

 private slots:
  void emitSignal();

 signals:
  void floatLineEditValueChanged(float value);

 private:

  int digits_cache;
  float value_cache;

  GuiLineEdit* gle;
};


////////////////////////////////////////////////////////////////

/**
  * This class implements a slider with float values (of course it's still
  * discrete !), so you can use rational numbers as its values
  */
class floatSlider : public QObject  {
   Q_OBJECT

 public:
  floatSlider(float minValue, float maxValue, float step, float value, QWidget *parent, const char *name );
  ~floatSlider();

  QWidget* get_widget();

 public slots:
  void setfloatSliderValue( float value);

 private slots:
  void emitSignal( int discrete_value);

 signals:
  void floatSliderValueChanged( float newvalue);


 private:
  GuiSlider* gs;
  float minValue_cache;
  float step_cache;
  int oldPosition;
};


////////////////////////////////////////////////////////////////


/**
  * This class contains a floatlineedit to display a float value
  * in a nice box with a label.
  */
class floatLineBox : public QGroupBox  {
   Q_OBJECT

 public:
  floatLineBox(float value, int digits, QWidget *parent, const char *name);
  ~floatLineBox();

 public slots:
  void setfloatLineBoxValue( float value );


 private slots:
  void emitSignal( float  value );


 signals:
  void floatLineBoxValueChanged( float  value );

 private:
  GuiGridLayout* grid;
  floatLineEdit *le;
};


////////////////////////////////////////////////////////////////

/**
  * This class implements a slider useful for scientific applications
  * by composing a floatSlider and a floatLineEdit into one widget.
  */
class floatScientSlider : public QGroupBox  {
   Q_OBJECT

 public:
  floatScientSlider(float minValue, float maxValue, float Step, float value, int digits, QWidget *parent, const char *name);
  ~floatScientSlider();

 public slots:
  void setfloatScientSliderValue( float value );

 private slots:
  void emitSignal( float  value );

 signals:
  void floatScientSliderValueChanged( float  value );

 private:
  float value;
  floatSlider* slider;
  GuiGridLayout* grid;
  floatLineEdit *le;


};

///////////////////////////////////////////////////////////////////////

/**
  * This class contains 3 floatlineedit to display 3 values
  * in a nice box with a label.
  */
class floatLineBox3D : public QGroupBox  {
   Q_OBJECT

 public:
  floatLineBox3D(float xval, float yval, float zval, int digits, QWidget *parent, const char *name);
  ~floatLineBox3D();

 public slots:
  void setfloatLineBox3DValue( float xval, float yval, float zval );

 private slots:
  void emitSignal_x( float newval );
  void emitSignal_y( float newval );
  void emitSignal_z( float newval );

 signals:
  void floatLineBox3DValueChanged( float xval, float yval, float zval );
  void SignalToChild_x( float );
  void SignalToChild_y( float );
  void SignalToChild_z( float );

 private:
  GuiGridLayout* grid;
  floatLineEdit *lex;
  floatLineEdit *ley;
  floatLineEdit *lez;
  float xcache;
  float ycache;
  float zcache;
};


#endif
