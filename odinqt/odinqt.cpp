#include <qwidget.h>
#include <qlayout.h>
#include <qfont.h>
#include <qfontinfo.h>
#include <qpushbutton.h>
#include <qlineedit.h>
#include <qslider.h>
#include <qcombobox.h>
#include <qlabel.h>
#include <qpainter.h>
#include <qevent.h>
#include <qregion.h>
#include <qtooltip.h>
#include <qfiledialog.h>
#include <qapplication.h>
#include <qpalette.h>
#include <qmessagebox.h>
#include <qdialog.h>
#include <qmainwindow.h>
#include <qstatusbar.h>
#include <qmenubar.h>
#include <qprogressdialog.h>
#include <qimage.h>
#include <qlistview.h>
#include <qprogressbar.h>
#include <qscrollbar.h>
#include <qtoolbar.h>
#include <qtoolbutton.h>
#include <qprinter.h>
#include <qtextedit.h>


#if QT_VERSION > 0x03FFFF
#define QT_VERSION_POST3
#else
#define QT_VERSION_3
#endif



#ifdef QT_VERSION_POST3
#include <qmenu.h>
#include <qscrollarea.h>
#include <qtablewidget.h>
#include <qtreewidget.h>
#include <qprintdialog.h>
#include <qheaderview.h>
#include <qtextcursor.h>
#include <qimagereader.h>
#else
#include <qpopupmenu.h>
#include <qscrollview.h>
#include <qpaintdevicemetrics.h>
#include <qwizard.h>
#endif



#include "odinqt.h"
#include "odinqt_callback.h"




#include <tjutils/tjlog_code.h>

const char* OdinQt::get_compName() {return "OdinQt";}
LOGGROUNDWORK(OdinQt)


///////////////////////////////////////////////////


const char* c_str(const QString& qs) {
  if(qs.isEmpty()) return "";
#ifdef QT_VERSION_POST3
  return qs.toLocal8Bit().constData();
#else
  return qs;
#endif
}


int layout_border_size() {
#ifdef QT_VERSION_POST3
  return 0;
#else
  return (3*QFontInfo(QFont(_FONT_TYPE_, _FONT_SIZE_)).pixelSize()/2);
#endif
}

void init_label(QLabel* ql) {
  ql->setFrameStyle( QFrame::Box | QFrame::Sunken );
  ql->setIndent( 0 );
#ifdef QT_VERSION_POST3
  ql->setCursor(Qt::CrossCursor);
#else
  ql->setCursor(Qt::crossCursor);
#endif
}

bool left_button(const QMouseEvent* qme, bool return_current_state) {
#ifdef QT_VERSION_POST3
  if(return_current_state) return (qme->buttons() & Qt::LeftButton);
  else                     return (qme->button()  & Qt::LeftButton);
#else
  if(return_current_state) return (qme->state()  & Qt::LeftButton);
  else                     return (qme->button() & Qt::LeftButton);
#endif
}

bool right_button(const QMouseEvent* qme, bool return_current_state) {
#ifdef QT_VERSION_POST3
  if(return_current_state) return (qme->buttons() & Qt::RightButton);
  else                     return (qme->button()  & Qt::RightButton);
#else
  if(return_current_state) return (qme->state()  & Qt::RightButton);
  else                     return (qme->button() & Qt::RightButton);
#endif
}

bool middle_button(const QMouseEvent* qme, bool return_current_state) {
#ifdef QT_VERSION_POST3
  if(return_current_state) return (qme->buttons() & Qt::MidButton);
  else                     return (qme->button()  & Qt::MidButton);
#else
  if(return_current_state) return (qme->state()  & Qt::MidButton);
  else                     return (qme->button() & Qt::MidButton);
#endif
}

void add_tooltip(QWidget* w, const char* txt) {
#ifdef QT_VERSION_POST3
  w->setToolTip(txt);
#else
  QToolTip::add(w,txt);
#endif
}


void set_platform_defaults(QWidget* w, bool enable) {
#ifdef Q_WS_MAC
#ifdef QT_VERSION_POST3
  w->setAttribute(Qt::WA_MacMetalStyle,enable);
#endif
#endif
}




STD_string get_save_filename(const char* caption, const char* startwith, const char* filter, QWidget* parent) {
#ifdef QT_VERSION_POST3
  QFileDialog::Options opts=0;
#ifdef Q_WS_MAC
  opts=QFileDialog::DontUseNativeDialog; // native dialog does not work well, e.g. with symlinks
#endif
  QString fname=QFileDialog::getSaveFileName(parent, caption, startwith, filter, 0, opts); 
#else
  QString fname=QFileDialog::getSaveFileName(startwith, filter, parent, "", caption);
#endif
  return c_str(fname);
}

STD_string get_open_filename(const char* caption, const char* startwith, const char* filter, QWidget* parent) {
#ifdef QT_VERSION_POST3
  QFileDialog::Options opts=0;
#ifdef Q_WS_MAC
  opts=QFileDialog::DontUseNativeDialog; // native dialog does not work well, e.g. with symlinks
#endif
  QString fname=QFileDialog::getOpenFileName(parent, caption, startwith, filter, 0, opts); 
#else
  QString fname=QFileDialog::getOpenFileName(startwith, filter, parent, "", caption);
#endif
  return c_str(fname);
}

STD_string get_directory(const char* caption, const char* startwith, QWidget* parent) {
#ifdef QT_VERSION_POST3
  QFileDialog::Options opts=QFileDialog::ShowDirsOnly;
#ifdef Q_WS_MAC
  opts|=QFileDialog::DontUseNativeDialog; // native dialog does not work well, e.g. with symlinks
#endif
  QString dname=QFileDialog::getExistingDirectory (parent, caption, startwith,opts);
#else
  QString dname=QFileDialog::getExistingDirectory(startwith, parent, "", caption, true);
#endif
  return c_str(dname);
}



bool message_question(const char* text, const char* caption, QWidget* parent, bool ask, bool error) {
  int result=0;

  if(ask) {
    // using QMessageBox::Ok and QMessageBox::Cancel does not work on Suse 9,10,
    // i.e. the order of buttons is mixed up due a patch to Qt, so we do it manually:
    QString okstr("Ok");
    QString cancelstr("Cancel");
#if QT_VERSION > 0x030FFF
    result=QMessageBox::question(parent, caption, text, okstr, cancelstr);
#else
    result=QMessageBox::information(parent, caption, text, okstr, cancelstr);
#endif
  } else {
    if(error) result=QMessageBox::critical(parent, caption, text);
    else      result=QMessageBox::information(parent, caption, text);
  }
  return (result==0); // return true if default button, which is 'Ok' , is clicked
}

///////////////////////////////////////////////////

int paintdevice_height(const QPaintDevice* qpd) {
#ifdef QT_VERSION_POST3
  return qpd->height();
#else
  QPaintDeviceMetrics metrics(qpd);
  return metrics.height();
#endif
}

int paintdevice_width(const QPaintDevice* qpd) {
#ifdef QT_VERSION_POST3
  return qpd->width();
#else
  QPaintDeviceMetrics metrics(qpd);
  return metrics.width();
#endif
}


///////////////////////////////////////////////////

svector get_possible_image_fileformats() {
#ifdef QT_VERSION_POST3
  QList<QByteArray> fmtlist=QImageReader::supportedImageFormats();
  int nfmts=fmtlist.size();
#else
  QStrList fmtlist=QImageIO::outputFormats();
  int nfmts=fmtlist.count();
#endif
  svector result; result.resize(nfmts);
  for(int ifmt=0; ifmt<nfmts; ifmt++) {
#ifdef QT_VERSION_POST3
    result[ifmt]=tolowerstr((const char*)fmtlist[ifmt]);
#else
    result[ifmt]=tolowerstr((const char*)fmtlist.at(ifmt));
#endif
  }
  return result;
}


///////////////////////////////////////////////////

GuiGridLayout::GuiGridLayout(QWidget *parent, int rows, int columns, bool margin) {
#ifdef QT_VERSION_POST3
  qgl=new QGridLayout( parent );
#else
  qgl=new QGridLayout( parent, rows, columns );
#endif
  if(margin) qgl->setMargin(layout_border_size());
}

GuiGridLayout::~GuiGridLayout() {
  delete qgl;
}

void GuiGridLayout::add_widget(QWidget *w, int row, int column, Alignment alignment, int rowSpan, int columnSpan ) {
#ifdef QT_VERSION_POST3
  Qt::Alignment align=0;
#else
  int align=0;
#endif
  if(alignment==VCenter) align=Qt::AlignVCenter;
  if(alignment==Center) align=Qt::AlignCenter;
#ifdef QT_VERSION_POST3
  qgl->addWidget(w, row, column, rowSpan, columnSpan, align );
#else
  if(rowSpan==1 && columnSpan==1) qgl->addWidget( w, row, column, align );
  else qgl->addMultiCellWidget( w, row, row+rowSpan-1, column, column+columnSpan-1, align );
#endif
}

void GuiGridLayout::set_col_stretch(int col, int factor) {
#ifdef QT_VERSION_POST3
  qgl->setColumnStretch(col,factor);
#else
  qgl->setColStretch(col,factor);
#endif
}

void GuiGridLayout::set_row_stretch(int row, int factor) {
  qgl->setRowStretch(row,factor);
}

void GuiGridLayout::set_col_minsize(int col, int minsize) {
#ifdef QT_VERSION_POST3
  qgl->setColumnMinimumWidth(col, minsize);
#else
  qgl->addColSpacing(col, minsize);
#endif
}

void GuiGridLayout::set_row_minsize(int row, int minsize) {
#ifdef QT_VERSION_POST3
  qgl->setRowMinimumHeight(row, minsize);
#else
  qgl->addRowSpacing(row, minsize);
#endif
}


///////////////////////////////////////////////////

GuiButton::GuiButton(QWidget *parent, QObject* receiver, const char* member, const char *onlabel,const char *offlabel, bool initstate) {
  ontext=onlabel;
  offtext=offlabel;

  qpb = new QPushButton(parent);

  if(onlabel && offlabel) {
#ifdef QT_VERSION_POST3
    qpb->setCheckable(true);
#else
    qpb->setToggleButton(true);
#endif
    set_toggled(initstate); // set initial text
  } else {
    if(ontext) qpb->setText(ontext);
  }

  qpb->setAutoDefault(false);

//  connect( qpb, SIGNAL(toggled(bool)), SLOT(setToggled(bool)) );

  qpb->setFixedHeight( qpb->sizeHint().height() );
  qpb->setFixedWidth( qpb->sizeHint().width() );

  sd=new SlotDispatcher(this,receiver,member);
}

void GuiButton::set_text(bool state) {
  if(state) qpb->setText( ontext );
  else qpb->setText( offtext );
}


void GuiButton::set_toggled(bool state) {
  if(!(ontext && offtext)) return;
  set_text(state);
#ifdef QT_VERSION_POST3
  qpb->setChecked (state);
#else
  qpb->setOn (state);
#endif
}

void GuiButton::set_default(bool state) {
  qpb->setDefault (state);
}

QWidget* GuiButton::get_widget() {
  return qpb;
}

bool GuiButton::is_on() const {
#ifdef QT_VERSION_POST3
  return qpb->isChecked();
#else
  return qpb->isOn();
#endif
}


GuiButton::~GuiButton() {
  delete sd;
  delete qpb;
}

/////////////////////////////////////////////////

GuiLineEdit::GuiLineEdit(QWidget *parent, QObject* receiver, const char* member, int width, int height) {
  qle=new QLineEdit(parent);
  if(width>0 && height>0) qle->setFixedSize(width,height);
  sd=new SlotDispatcher(this,receiver,member);
}

GuiLineEdit::~GuiLineEdit() {
  delete sd;
  delete qle;
}

void GuiLineEdit::set_text(const char* txt) {qle->setText(txt);}

const char* GuiLineEdit::get_text() const {return c_str(qle->text());}

bool GuiLineEdit::is_modified() {
#ifdef QT_VERSION_POST3
  bool result=qle->isModified();
  qle->setModified(false);
#else
  bool result=qle->edited();
  qle->setEdited(false);
#endif
  return result;
}

QWidget* GuiLineEdit::get_widget() {return qle;}


///////////////////////////////////////////////////

GuiPopupMenu::GuiPopupMenu(QWidget *parent) : id(0) {
#ifdef QT_VERSION_POST3
  qm=new QMenu(parent);
#else
  qpm=new QPopupMenu(parent);
#endif
}

GuiPopupMenu::~GuiPopupMenu() {
/* // never deleted
#ifdef QT_VERSION_POST3
  delete qm;
#else
  delete qpm;
#endif
*/
}  

void GuiPopupMenu::insert_item(const char* text, const QObject* receiver, const char* member, int accel) {
#ifdef QT_VERSION_POST3
 qm->addAction(text, receiver, member, accel);
#else
  id++;
  qpm->insertItem(text, receiver, member, accel, id);
#endif
}

void GuiPopupMenu::insert_separator() {
#ifdef QT_VERSION_POST3
  qm->addSeparator();
#else
  qpm->insertSeparator();
#endif
}


void GuiPopupMenu::popup(const QPoint& p) {
#ifdef QT_VERSION_POST3
 qm->exec(p); // synchronous popup
#else
  qpm->exec(p); // synchronous popup
#endif
}


///////////////////////////////////////////////////


GuiSlider::GuiSlider(QWidget *parent, int minValue, int maxValue, int pageStep, int value, int tickInterval) : modified(false) {
#ifdef QT_VERSION_POST3
  qs=new QSlider(Qt::Horizontal, parent);
  qs->setRange(minValue, maxValue);
  qs->setPageStep(pageStep);
  qs->setValue(value);
  qs->setTickPosition(QSlider::TicksBelow);
  qs->setFocusPolicy( Qt::TabFocus );
#else
  qs=new QSlider(minValue, maxValue, pageStep, value, QSlider::Horizontal, parent, "QSlider" );
  qs->setFocusPolicy( QWidget::TabFocus );
  qs->setTickmarks( QSlider::Below );
#endif
  qs->setMinimumSize(2*SLIDER_CELL_WIDTH,SLIDER_CELL_HEIGHT);
  qs->setTickInterval(tickInterval );
}

int GuiSlider::get_value() const {return qs->value();}

void GuiSlider::set_value(int val) {modified=true; qs->setValue(val);}

GuiSlider::~GuiSlider() {
 delete qs;
}

///////////////////////////////////////////////////

void GuiComboBox::set_names(const svector& names) {
#ifdef QT_VERSION_POST3
  for (unsigned int i=0; i<names.size(); i++) qcb->insertItem(i, names[i].c_str());
#else
  for (unsigned int i=0; i<names.size(); i++) qcb->insertItem( names[i].c_str() );
#endif
  set_current_item(0);
}

void GuiComboBox::common_init(QWidget* parent, const svector& names) {
#ifdef QT_VERSION_POST3
  qcb = new QComboBox(parent);
  qcb->setEditable(false);
  qcb->setSizeAdjustPolicy(QComboBox::AdjustToContents); // Width to fit all entries
#else
  qcb = new QComboBox( FALSE, parent, "comboBox" );
  qcb->setFixedWidth(qcb->sizeHint().width());
#endif
  qcb->setFixedHeight(qcb->sizeHint().height());
  set_names(names);
}


GuiComboBox::GuiComboBox(QWidget* parent, const svector& names) {
  common_init(parent,names);
}

GuiComboBox::GuiComboBox(GuiToolBar* parent, const svector& names) {
  common_init(parent->get_widget(),names);
#ifdef QT_VERSION_POST3
  parent->qtb->addWidget(qcb);
#endif
}


void GuiComboBox::set_current_item(int index) {
#ifdef QT_VERSION_POST3
  qcb->setCurrentIndex(index);
#else
  qcb->setCurrentItem(index);
#endif
}

int GuiComboBox::get_current_item() const {
#ifdef QT_VERSION_POST3
  return qcb->currentIndex();
#else
  return qcb->currentItem();
#endif
}


GuiComboBox::~GuiComboBox() {
  delete qcb;
}

///////////////////////////////////////////////////

GuiPainter::GuiPainter(QPixmap* qpm) {
  qpm_cache=qpm;
  qp=new QPainter(qpm);
  qp->setPen(_ARRAY_SELECTION_COLOR_);
#ifndef QT_VERSION_POST3
  qp->setFont(QFont(_FONT_TYPE_,_FONT_SIZE_));
#endif
}

GuiPainter::~GuiPainter() {
  delete qp;
}

void GuiPainter::moveTo(int x, int y) {
#ifdef QT_VERSION_POST3
  x_cache=x;
  y_cache=y;
#else
  qp->moveTo(x,y);
#endif
}

void GuiPainter::lineTo(int x, int y) {
#ifdef QT_VERSION_POST3
  qp->drawLine(x_cache,y_cache,x,y);
  x_cache=x;
  y_cache=y;
#else
  qp->lineTo(x,y);
#endif
}

void GuiPainter::repaint(QLabel* dst) {
#ifdef QT_VERSION_POST3
//  qp->drawPixmap(0,0,*qpm_cache);
  bool active=qp->isActive();
  if(active) qp->end();
  dst->setPixmap(*qpm_cache);
  if(active) {
    qp->begin(qpm_cache);
    qp->setPen(_ARRAY_SELECTION_COLOR_);
  }
#else
  bitBlt( dst, 2, 2, qpm_cache );
#endif
}

bool GuiPainter::begin(QPixmap* qpm) {
  return qp->begin(qpm);
}

bool GuiPainter::end() {
  return qp->end();
}

void GuiPainter::setPen(const char* pencolor, int linewidth, bool dotted, float lightdark) {
  Qt::PenStyle ps=Qt::SolidLine;
  if(dotted) ps=Qt::DotLine;
  QColor color(pencolor);

  int lightfactor=100+int(lightdark*90+0.5);

  QPen pen(color.light(lightfactor));
  pen.setWidth(linewidth);
  pen.setStyle(ps);
  qp->setPen(pen);
}

void GuiPainter::fillRect( int x, int y, int w, int h, const QColor& col) {

  // Transparent:
//  QBrush qb(col);
//  qb.setStyle(Qt::Dense4Pattern);
//  qp->fillRect(QRect(x,y,w,h),qb);

  qp->fillRect(x,y,w,h,col);
}

void GuiPainter::drawRect(int x, int y, int w, int h) {
  qp->drawRect(x,y,w,h);
}


void GuiPainter::drawText( int x, int y, const QString& txt, const QColor& col) {
  qp->setPen( col);
  qp->drawText( x, y, txt);
}

QRegion* GuiPainter::draw_region(const STD_list<QPoint>& plist) {
  QRegion* rgn=0;
  if(plist.size()<3) return rgn;
#ifdef QT_VERSION_POST3
  QPolygon qpg(plist.size());
  unsigned int index=0;
  for(STD_list<QPoint>::const_iterator it=plist.begin(); it!=plist.end(); ++it) {
    qpg.setPoint(index, it->x(), it->y());
    index++;
  }
  rgn=new QRegion(qpg,Qt::WindingFill);
#else
  QPointArray qpa(plist.size());
  unsigned int index=0;
  for(STD_list<QPoint>::const_iterator it=plist.begin(); it!=plist.end(); ++it) {
    qpa.setPoint(index, it->x(), it->y());
    index++;
  }
  rgn=new QRegion(qpa,TRUE);
#endif

  qp->setClipRegion(*rgn);
  qp->fillRect (0,0,qpm_cache->width(),qpm_cache->height(),QBrush(QColor(_ARRAY_SELECTION_COLOR_),Qt::DiagCrossPattern));

  return rgn;
}


///////////////////////////////////////////////////


GuiApplication::GuiApplication(int argc, char *argv[]) {

  // Create pristine copy of cmdline args for Qt before anything else
  argc4qt=argc;
  argv4qt=new char*[argc];
  for(int iarg=0; iarg<argc; iarg++) {
    STD_string argstr(argv[iarg]);
    int nchar=argstr.length();
    argv4qt[iarg]= new char[nchar+1];
    for(int ichar=0; ichar<nchar; ichar++) argv4qt[iarg][ichar]=argv[iarg][ichar];
    argv4qt[iarg][nchar]='\0';
  }


  // initialise debug handler, do not trigger error about components already registered because this might be run within paravision
  if(LogBase::set_log_levels(argc,argv,false)) exit(0);

  Log<OdinQt> odinlog("GuiApplication","GuiApplication");
  ODINLOG(odinlog,normalDebug) << "argc/argv=" << argc << "/" << argv << STD_endl;

  argc_cache=argc;
  argv_cache=argv;


  qa=new QApplication(argc4qt,argv4qt);

#ifndef QT_VERSION_POST3
  qa->setFont(QFont(_FONT_TYPE_, _FONT_SIZE_));
#endif

  // setting text color back to normal, even if noedit
  QPalette pal=qa->palette();
#ifdef QT_VERSION_POST3
  pal.setColor(QPalette::Disabled, QPalette::Foreground, pal.color (  QPalette::Active, QPalette::Foreground ) );
  pal.setColor(QPalette::Disabled, QPalette::Text,       pal.color (  QPalette::Active, QPalette::Text ) );
  qa->setPalette(pal);
#else
  pal.setColor(QColorGroup::Foreground, pal.color (  QPalette::Normal, QColorGroup::Foreground ) );
  pal.setColor(QColorGroup::Text,       pal.color (  QPalette::Normal, QColorGroup::Text ) );
  qa->setPalette(pal,true);
#endif

  ODINLOG(odinlog,normalDebug) << "GuiApplication::argc/argv()=" << GuiApplication::argc() << "/" << GuiApplication::argv() << STD_endl;
}


int GuiApplication::start(QWidget* mainwidget) {
#ifndef QT_VERSION_POST3
  qa->setMainWidget(mainwidget);
#endif
//  mainwidget->show();
  return qa->exec();
}


QObject* GuiApplication::get_object() {return qa;}

void GuiApplication::quit() {qApp->quit();}

int GuiApplication::argc() {return argc_cache;}

char** GuiApplication::argv() {return argv_cache;}

void GuiApplication::process_events() {qApp->processEvents();}

GuiApplication::~GuiApplication() {
  qa->quit();
  delete qa; // We must delete it to avoid error message when starting as edit widget in Paravision
}

int GuiApplication::argc4qt;
char** GuiApplication::argv4qt;
int GuiApplication::argc_cache;
char** GuiApplication::argv_cache;

///////////////////////////////////////////////////

GuiMainWindow::GuiMainWindow(QWidget* parent) {
  statusIcon=0;
  statusText=0;
  qmw=new QMainWindow(parent);
//  set_platform_defaults(qmw); // does not work because it alters attributes of all widgets
}

GuiMainWindow::~GuiMainWindow() {
  delete qmw;
  if(statusIcon) delete statusIcon;
  if(statusText) delete statusText;
}

void GuiMainWindow::show(QWidget* central_widget, bool show_toolbutton_text) {
  set_status_message("Ready ...", 2000); // necessary to show status bar in the first place

#ifdef QT_VERSION_POST3
  if(show_toolbutton_text) qmw->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
#else
  qmw->setUsesTextLabel(show_toolbutton_text);
#endif

  central_widget->setFocus();
  qmw->setCentralWidget(central_widget);
  qmw->show();
}

void GuiMainWindow::set_caption(const char* text) {
  STD_string captext(text);
#ifdef QT_VERSION_POST3
  qmw->setWindowTitle(captext.c_str());
#else
  qmw->setCaption(captext.c_str());
#endif
}

void GuiMainWindow::set_status_message(const char* text, int timeout_ms) {

  if(statusText) statusText->setText(text);
  else {
#ifdef QT_VERSION_POST3
    qmw->statusBar()->showMessage(text,timeout_ms);
#else
    if(timeout_ms) qmw->statusBar()->message(text);
    else           qmw->statusBar()->message(text,timeout_ms);
#endif
  }

}

void GuiMainWindow::set_status_xpm(const char** xpm) {
  QPixmap qp(xpm);
  if(!statusIcon) {
    statusIcon=new QLabel(qmw->statusBar());
    statusText=new QLabel(qmw->statusBar());
    statusText->setMinimumWidth(qmw->width()-qp.width());
#ifdef QT_VERSION_POST3
    qmw->statusBar()->addPermanentWidget(statusIcon);
    qmw->statusBar()->addPermanentWidget(statusText);
#else
    qmw->statusBar()->addWidget(statusIcon,1,true);
    qmw->statusBar()->addWidget(statusText,1,true);
#endif
  }
  statusIcon->setPixmap(qp);
}


QWidget* GuiMainWindow::get_widget() {return qmw;}


void GuiMainWindow::insert_menu( const char* text, GuiPopupMenu* gpm) {
#ifdef QT_VERSION_POST3
  gpm->qm->setTitle(text);
  qmw->menuBar()->addMenu(gpm->qm);
#else
  qmw->menuBar()->insertItem(text, gpm->qpm);
#endif
}

void GuiMainWindow::insert_menu_separator() {
#ifdef QT_VERSION_POST3
  qmw->menuBar()->addSeparator();
#else
  qmw->menuBar()->insertSeparator();
#endif
}

void GuiMainWindow::close() {
  qmw->close();
}


///////////////////////////////////////////////////

GuiScroll::GuiScroll(QWidget* child, QWidget* parent) {
#ifdef QT_VERSION_POST3
  qsa=new QScrollArea(parent);
  qsa->setWidget(child);
  qsa->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
  qsa->setMinimumWidth(child->width());
#else
  qsv=new QScrollView(parent);
  qsv->addChild(child);
  qsv->setHScrollBarMode(QScrollView::AlwaysOff);
  qsv->setMinimumWidth(child->width());
#endif
}

QWidget* GuiScroll::get_widget() {
#ifdef QT_VERSION_POST3
  return qsa;
#else
  return qsv;
#endif
}

GuiScroll::~GuiScroll() {
#ifdef QT_VERSION_POST3
  delete qsa;
#else
  delete qsv;
#endif
}

///////////////////////////////////////////////////

class QDialogDerived : public QDialog {

 public:
  QDialogDerived(GuiDialog* user, QWidget *parent, const char* caption, bool modal) :
#ifdef QT_VERSION_POST3
  QDialog(parent) {
    QDialog::setModal(modal);
    QDialog::setWindowTitle(caption);
#else
  QDialog(parent,"",modal) {
    QDialog::setCaption(caption);
#endif
    user_cache=user;
  }

  void call_done(int r) {
    QDialog::done(r);
  }

 protected:
  void paintEvent(QPaintEvent* event) { // overloaded from QWidget
    QDialog::paintEvent(event);
    user_cache->repaint();
  }

  void closeEvent(QCloseEvent* event) { // overloaded from QWidget
    QDialog::closeEvent(event);
    user_cache->close();
  }


 private:
  GuiDialog* user_cache;

};



GuiDialog::GuiDialog(QWidget *parent, const char* caption, bool modal) {
  qd=new QDialogDerived(this, parent, caption, modal);
}

void GuiDialog::show() {
  qd->show();
}

void GuiDialog::cancel() {
  qd->call_done(0);
}

void GuiDialog::done() {
  qd->call_done(1);
//  GuiDialog::hide(); // deleting causes segfault. why? I dunno ...
}


void GuiDialog::hide() {qd->hide();}
int GuiDialog::exec() {return qd->exec();}

QWidget* GuiDialog::get_widget() {return qd;}

GuiDialog::~GuiDialog() {
  delete qd;
}

/////////////////////////////////////////////////

GuiProgressDialog::GuiProgressDialog(QWidget *parent, bool modal, int total_steps) {
#ifdef QT_VERSION_POST3
  qpd=new QProgressDialog("", "Cancel", 0, total_steps, parent);
  qpd->setModal(modal);
#else
  qpd=new QProgressDialog("", "Cancel", total_steps, parent, "progress", modal);
#endif
  if(total_steps) qpd->setMinimumDuration(1000);
  else            qpd->setMinimumDuration(100); // zero value produces event-timing problems in Qt
  set_progress(0);
}

void GuiProgressDialog::set_progress(int progr) {
#ifdef QT_VERSION_POST3
  qpd->setValue(progr);
#else
  qpd->setProgress(progr);
#endif
}

int GuiProgressDialog::get_progress() const {
#ifdef QT_VERSION_POST3
  return qpd->value();
#else
  return qpd->progress();
#endif
}

void GuiProgressDialog::set_total_steps(int steps) {
#ifdef QT_VERSION_POST3
  qpd->setMaximum(steps);
#else
  qpd->setTotalSteps(steps);
#endif
}



bool GuiProgressDialog::was_cancelled() const {
#ifdef QT_VERSION_POST3
  return qpd->wasCanceled();
#else
  return qpd->wasCancelled();
#endif
}

void GuiProgressDialog::reset() {
  qpd->reset();
  set_progress(0);
}

void GuiProgressDialog::set_text(const char* txt) {
  qpd->setLabelText(txt);
}

void GuiProgressDialog::show() {
  qpd->show();
}

void GuiProgressDialog::hide() {
  qpd->hide();
}

GuiProgressDialog::~GuiProgressDialog() {
  delete qpd;
}

/////////////////////////////////////////////////

GuiImage::GuiImage(unsigned char* data, int width, int height, bool colormap) {
  Log<OdinQt> odinlog("GuiImage","GuiImage");
  ODINLOG(odinlog,normalDebug) << "width/height/colormap=" << width << "/" << height << "/" << colormap << STD_endl;
#ifdef QT_VERSION_POST3
  qi=new QImage( data, width, height, QImage::Format_Indexed8);
  qi->setColorCount (256);
#else
  qi=new QImage( data, width, height, 8, 0, 256, QImage::IgnoreEndian);
#endif
  QColor qc;
  for(int i=0; i<256; i++) {
    if(colormap) {
      int hue=int(240.0*(1.0-float(i)/255.0));
      qc.setHsv(hue,255,255);
      qi->setColor(i, qc.rgb() );
      if(i==0) qi->setColor(0, qRgb(0,0,0) ); // lowest value black
    } else qi->setColor(i, qRgb(i,i,i) ); // simple gray scale
  }
}

QPixmap* GuiImage::create_pixmap() const {
#ifdef QT_VERSION_POST3
  return new QPixmap(QPixmap::fromImage(*qi));
#else
  QPixmap* qpm=new QPixmap();
  qpm->convertFromImage(*qi);
  return qpm;
#endif
}

GuiImage::~GuiImage() {
  delete qi;
}

/////////////////////////////////////////////////


GuiListView::GuiListView(QWidget *parent, const svector& column_labels, int first_column_width, int min_height, GuiListViewCallback* callback, bool tree) {
#ifdef QT_VERSION_POST3

  QStringList qsl;
  for(unsigned int i=0; i<column_labels.size(); i++) {
    qsl.append(column_labels[i].c_str());
  }

  qtw=0;
  qtrw=0;

  int width_factor=1;
  if(column_labels.size()>1) width_factor=2;

  if(tree) {
    qtrw=new QTreeWidget(parent);
    qtrw->setColumnCount(column_labels.size());
    qtrw->setHeaderLabels(qsl);
    qtrw->setSortingEnabled(false);
    if(min_height>0) qtrw->setMinimumHeight(min_height);
    if(first_column_width>0) qtrw->setMinimumWidth(width_factor*first_column_width);
    qtrw->header()->resizeSection(0, first_column_width);
  } else {
    qtw=new QTableWidget(parent);
    qtw->setColumnCount(column_labels.size());
    qtw->setHorizontalHeaderLabels(qsl);
    if(min_height>0) qtw->setMinimumHeight(min_height);
    if(first_column_width>0) {
      qtw->setMinimumWidth(width_factor*first_column_width);
      qtw->horizontalHeader()->resizeSection(0, first_column_width);
    }
  }

#else
  qlv=new QListView(parent);
  for(unsigned int i=0; i<column_labels.size(); i++) {
    qlv->addColumn (column_labels[i].c_str());
  }

  if(first_column_width>0) {
    qlv->setColumnWidthMode(0,QListView::Manual);
    qlv->setColumnWidth(0,first_column_width);
  }

  if(min_height>0) {
    qlv->setMinimumHeight(min_height);
  }
  qlv->setSorting(-1);
#endif


  sd=0;
  if(callback) sd=new SlotDispatcher(this, callback);
}

QWidget* GuiListView::get_widget() {
#ifdef QT_VERSION_POST3
  if(qtrw) return qtrw;
  if(qtw)  return qtw;
  return 0;
#else
  return qlv;
#endif
}


GuiListView::~GuiListView() {
  delete sd;
#ifdef QT_VERSION_POST3
  if(qtrw) delete qtrw;
  if(qtw)  delete qtw;
#else
  delete qlv;
#endif
}

/////////////////////////////////////////////////

#ifndef QT_VERSION_POST3
void create_qstring_list(STD_vector<QString>& label, const svector& column_entries) {
  int ncols=column_entries.size();
  if(ncols>8) ncols=8;

  label.resize(8);
  for(int i=0; i<ncols; i++) {
    label[i]=column_entries[i].c_str();
  }
}
#endif


void GuiListItem::common_init() {
  qlvi=0;
  qcli=0;
  qtwi=0;
  qtrwi=0;
  parent_qtrw=0;
}

GuiListItem::GuiListItem(GuiListView *parent, const svector& column_entries, bool checkable, bool initstate) {
  Log<OdinQt> odinlog("GuiListItem","GuiListItem");
  common_init();
#ifdef QT_VERSION_POST3
  int ncols=column_entries.size();
  if(!ncols) return;

  if(parent->qtrw) { // tree view

    qtrwi=new QTreeWidgetItem(parent->qtrw);

    for(int icol=0; icol<ncols; icol++) {
      qtrwi->setText(icol, column_entries[icol].c_str());
    }

    parent_qtrw=parent->qtrw;
    parent->qtrw->expandItem(qtrwi);
//#warning GuiListItem: Implement me: setOpen(true), setSelected(false)


  } else {
    qtwi=new QTableWidgetItem[ncols];
    int row=parent->qtw->rowCount();
    parent->qtw->setRowCount(row+1);
    for(int icol=0; icol<ncols; icol++) {
      qtwi[icol].setText(column_entries[icol].c_str());
      parent->qtw->setItem(row, icol, &(qtwi[icol]));
    }
    if(checkable) {
      qtwi[0].setFlags(qtwi[0].flags() | Qt::ItemIsUserCheckable);
      Qt::CheckState state=Qt::Unchecked;
      if(initstate) state=Qt::Checked;
      qtwi[0].setCheckState(state);
    }
    (*tablemap)[&(qtwi[0])]=this;
  }
#else

  QListViewItem* last=parent->qlv->lastItem();

  if(checkable) {
    qcli=new QCheckListItem (parent->qlv, column_entries[0].c_str(), QCheckListItem::CheckBox);
    qcli->setOn(initstate);
  } else {
    STD_vector<QString> label;
    create_qstring_list(label,column_entries);
    ODINLOG(odinlog,normalDebug) << "parent->qlv=" << parent->qlv << STD_endl;
    qlvi=new QListViewItem(parent->qlv, label[0], label[1], label[2], label[3], label[4], label[5], label[6], label[7]);
    ODINLOG(odinlog,normalDebug) << "qlvi=" << qlvi << STD_endl;
    if(last) qlvi->moveItem(last); // append at end
    qlvi->setSelected(false);
    qlvi->setOpen(true);
    (*listmap)[qlvi]=this;
  }
#endif

}

GuiListItem::GuiListItem(GuiListItem* parent, GuiListItem* after, const svector& column_entries) {
  Log<OdinQt> odinlog("GuiListItem","GuiListItem");
  common_init();
#ifdef QT_VERSION_POST3
  QTreeWidgetItem* parent_qtrwi=0;
  if(parent) parent_qtrwi=parent->qtrwi;
  QTreeWidgetItem* after_qtrwi=0;
  if(after) after_qtrwi=after->qtrwi;
  qtrwi=new QTreeWidgetItem(parent_qtrwi, after_qtrwi);
  for(int icol=0; icol<int(column_entries.size()); icol++) {
    qtrwi->setText(icol, column_entries[icol].c_str());
  }

  parent_qtrw=parent->parent_qtrw;
  if(parent_qtrw) parent_qtrw->expandItem(qtrwi);
//#warning GuiListItem: Implement me: setOpen(true)

#else
  STD_vector<QString> label;
  create_qstring_list(label,column_entries);
  QListViewItem* parent_qlvi=0;
  if(parent) parent_qlvi=parent->qlvi;
  ODINLOG(odinlog,normalDebug) << "parent/parent_qlvi=" << parent << "/" << parent_qlvi << STD_endl;

  if(after && after->qlvi) {
    ODINLOG(odinlog,normalDebug) << "after->qlvi=" << after->qlvi << STD_endl;
    qlvi=new QListViewItem(parent_qlvi, after->qlvi, label[0], label[1], label[2], label[3], label[4], label[5], label[6], label[7]);
  } else {
    ODINLOG(odinlog,normalDebug) << "after=0" << STD_endl;
    qlvi=new QListViewItem(parent_qlvi, label[0], label[1], label[2], label[3], label[4], label[5], label[6], label[7]);
  }
  qlvi->setOpen(true);
#endif
}




bool GuiListItem::is_checked() const {
#ifdef QT_VERSION_POST3
  if(qtwi) return ((qtwi[0].checkState())==Qt::Checked);
#else
  if(qcli) return qcli->isOn();
#endif
  return false;
}

const char* GuiListItem::get_text() const {
#ifdef QT_VERSION_POST3
  if(qtwi) return c_str(qtwi[0].text());
#else
  if(qcli) return c_str(qcli->text());
#endif
  return "";
}



GuiListItem::~GuiListItem() {
#ifdef QT_VERSION_POST3
  if(qtwi) delete[] qtwi;
  if(qtrwi) delete qtrwi;
#else
  if(qlvi) delete qlvi;
  if(qcli) delete qcli;
#endif
}


void GuiListItem::init_static() {
#ifdef QT_VERSION_POST3
  tablemap=new STD_map<QTableWidgetItem*,GuiListItem*>;
#else
  listmap=new STD_map<QListViewItem*,GuiListItem*>;
#endif
}


void GuiListItem::destroy_static() {
#ifdef QT_VERSION_POST3
  delete tablemap;
#else
  delete listmap;
#endif
}

STD_map<QListViewItem*,GuiListItem*>* GuiListItem::listmap;
STD_map<QTableWidgetItem*,GuiListItem*>* GuiListItem::tablemap;

EMPTY_TEMPL_LIST bool StaticHandler<GuiListItem>::staticdone=false;

///////////////////////////////////////////////////////////


GuiProgressBar::GuiProgressBar(QWidget *parent, int total_steps) {
#ifdef QT_VERSION_POST3
  qpb=new QProgressBar(parent);
  qpb->setMinimum(0);
  qpb->setMaximum(total_steps);
#else
  qpb=new QProgressBar (total_steps, parent);
#endif
}


GuiProgressBar::~GuiProgressBar() {
  delete qpb;
}


void GuiProgressBar::set_progress(int progr) {
#ifdef QT_VERSION_POST3
  qpb->setValue(progr);
#else
  qpb->setProgress(progr);
#endif
}

void GuiProgressBar::set_min_width(int w) {
  qpb->setMinimumWidth(w);
}


QWidget* GuiProgressBar::get_widget() {
  return qpb;
}

//////////////////////////////////////////////////////////////////////


GuiScrollBar::GuiScrollBar(QWidget* parent) {
  qsb=new QScrollBar(Qt::Horizontal, parent);
}


GuiScrollBar::~GuiScrollBar() {
  delete qsb;
}

void GuiScrollBar::set_values(int min, int max, int linestep, int pagestep, int value) {
#ifdef QT_VERSION_POST3
  qsb->setMinimum(min);
  qsb->setMaximum(max);
  qsb->setSingleStep(linestep);
#else
  qsb->setMinValue(min);
  qsb->setMaxValue(max);
  qsb->setLineStep(linestep);
#endif
  qsb->setPageStep(pagestep);
  qsb->setValue(value);
}


QScrollBar* GuiScrollBar::get_widget() {
  return qsb;
}

/////////////////////////////////////////////////


GuiToolBar::GuiToolBar(GuiMainWindow* parent, const char* label) {
  qtb=new QToolBar(label, parent->qmw);
#ifdef QT_VERSION_POST3
  parent->qmw->addToolBar(qtb);
#endif
}

GuiToolBar::~GuiToolBar() {
  delete qtb;
}

QWidget* GuiToolBar::get_widget() {
  return qtb;
}


void GuiToolBar::add_separator() {
  qtb->addSeparator();
}


/////////////////////////////////////////////////


GuiToolButton::GuiToolButton(GuiToolBar* parent, const char** xpm, const char* label, QObject* receiver, const char* member, bool checkable, bool initstate ) {
#ifdef QT_VERSION_POST3
  qtb=new QToolButton(parent->qtb);
  if(xpm) qtb->setIcon(QPixmap(xpm));
  qtb->setCheckable(checkable);
  parent->qtb->addWidget(qtb);
#else
  qtb=new QToolButton(parent->qtb);
  if(xpm) qtb->setIconSet(QPixmap(xpm));
  qtb->setToggleButton(checkable);
#endif
  set_on(initstate);
  if(xpm) set_tooltip(label);
  else set_label(label);
  sd=new SlotDispatcher(this,receiver,member);
}

GuiToolButton::~GuiToolButton() {
  Log<OdinQt> odinlog("GuiToolButton","~GuiToolButton");
  ODINLOG(odinlog,normalDebug) << "Deleting sd" << STD_endl; 
  delete sd;
#ifndef QT_VERSION_POST3
  ODINLOG(odinlog,normalDebug) << "Deleting qtb" << STD_endl; 
  delete qtb; //  will be done by the parent toolbar on Qt4
#endif
}


bool GuiToolButton::is_on() const {
#ifdef QT_VERSION_POST3
  return qtb->isChecked();
#else
  return qtb->isOn();
#endif
}

void GuiToolButton::set_on(bool flag) {
#ifdef QT_VERSION_POST3
  qtb->setChecked(flag);
#else
  qtb->setOn(flag);
#endif
}

void GuiToolButton::set_enabled(bool flag) {
  qtb->setEnabled(flag);
}

void GuiToolButton::set_label(const char* text) {
#ifdef QT_VERSION_POST3
  qtb->setText(text);
#else
  qtb->setTextLabel(text);
#endif
}

void GuiToolButton::set_tooltip(const char* text) {
  add_tooltip(qtb,text);
}

/////////////////////////////////////////////////


GuiPrinter::GuiPrinter() {
  qp=new QPrinter(QPrinter::HighResolution);
  qp->setOrientation(QPrinter::Landscape);
}

bool GuiPrinter::setup(QWidget* parent) {
#ifdef QT_VERSION_POST3
  QPrintDialog* qpd=new QPrintDialog(qp,parent);
  return bool(qpd->exec());
#else
  return qp->setup(parent);
#endif
}

QPaintDevice* GuiPrinter::get_device() {
  return qp;
}


GuiPrinter::~GuiPrinter() {
  delete qp;
}

/////////////////////////////////////////////////

#define TEXTVIEW_MAXLINES 1000

GuiTextView::GuiTextView(QWidget* parent, int minwidth, int minheight) {
  qte=new QTextEdit(parent);
  qte->setReadOnly(true);
  qte->setMinimumSize(minwidth, minheight);

#ifdef QT_VERSION_POST3
  qte->document()->setMaximumBlockCount(TEXTVIEW_MAXLINES); // only available for Qt 4.2 or later versions
  qte->setLineWrapMode(QTextEdit::NoWrap);
#else
  qte->setTextFormat(Qt::LogText);
#if QT_VERSION > 0x0301FF
  qte->setMaxLogLines(TEXTVIEW_MAXLINES);
#endif

#endif
}


GuiTextView::~GuiTextView() {
  delete qte;
}


void GuiTextView::set_text(const char* txt) {
#ifdef QT_VERSION_POST3
  qte->setPlainText(txt);
#else
  qte->setText(txt);
#endif
  scroll_end();
}

void GuiTextView::append_text(const char* txt) {
  qte->append(txt);
  scroll_end();
}

void GuiTextView::scroll_end() {
#ifdef QT_VERSION_POST3
  QTextCursor qtc(qte->textCursor());
  qtc.movePosition(QTextCursor::End);
  qtc.movePosition(QTextCursor::StartOfLine);
  qte->setTextCursor(qtc);
  qte->ensureCursorVisible();
#else
  qte->setContentsPos(0,qte->contentsHeight()+TEXTVIEW_MAXLINES);
#endif
}


QWidget* GuiTextView::get_widget() {
  return qte;
}
