#include "stringbox.h"


stringBox::stringBox(const char* text,QWidget *parent, const char *name, const char *buttontext )
 : QGroupBox(name,parent) {

  unsigned int ncol=1;
  if(buttontext) ncol++;

  grid=new GuiGridLayout( this, 1, ncol);

  le = new GuiLineEdit(this, this, SLOT(reportTextChanged()));
  grid->add_widget( le->get_widget(), 0, 0 );


  // Create a push button
  pb=0;
  if(buttontext) {
    pb = new GuiButton( this, this, SLOT(reportButtonClicked()), buttontext );
    grid->add_widget( pb->get_widget(), 0, 1, GuiGridLayout::VCenter );
  }

  setstringBoxText(text);
}

void stringBox::reportButtonClicked() {
  emit stringBoxButtonPressed();
}

void stringBox::reportTextChanged() {
  if(le->is_modified()) {
    emit stringBoxTextEntered(le->get_text());
  }
}

void stringBox::setstringBoxText( const char* text ) {
  Log<OdinQt> odinlog("stringBox","setstringBoxText");
  ODINLOG(odinlog,normalDebug) << "text=" << text << STD_endl;
  le->set_text(text);
}

stringBox::~stringBox() {
  if(pb) delete pb;
  delete le;
  delete grid;
}
