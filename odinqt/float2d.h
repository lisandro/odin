/***************************************************************************
                          float2d.h  -  description
                             -------------------
    begin                : Sun Aug 27 2000
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef FLOAT2D_H
#define FLOAT2D_H

#include <qlabel.h>

#include "odinqt.h"
#include "plot.h"


class QPixmap; // forward declaration

/**
  * QLabel containing a QPixmap to draw 2D float array
  */
class floatLabel2D : public QLabel  {
  Q_OBJECT

 public:
  floatLabel2D(const float *data, float lowbound, float uppbound, unsigned int nx, unsigned int ny, bool disable_scale, unsigned int coarseFactor,
               QWidget *parent, const char *name,
               const float *overlay_map, float lowbound_map, float uppbound_map, unsigned int nx_map, unsigned int ny_map, bool map_firescale, float map_rectsize,
               bool colormap);
  ~floatLabel2D();

  int xpos2labelxpos(int pos);
  int ypos2labelypos(int pos);
  int labelxpos2xpos(int pos);
  int labelypos2ypos(int pos);
  int xypos2index(int xpos,int ypos);

  unsigned int get_nx() const {return nx_cache;}
  unsigned int get_ny() const {return ny_cache;}

  void  init_pixmap(bool clear=true);
  void  set_pixmap();

  void  write_pixmap(const char* fname, const char* format) const;

  void  write_legend(const char* fname, const char* format) const;

  QLabel* get_map_legend(QWidget *parent) const;
  void  write_map_legend(const char* fname, const char* format) const;

  QPixmap *pixmap;

protected:

  void mousePressEvent (QMouseEvent *);
  void mouseReleaseEvent (QMouseEvent *);
  void mouseMoveEvent ( QMouseEvent * );


public slots:
  void refresh(const float *data, float lowbound, float uppbound);
  void refreshMap(const float *map, float map_lowbound, float map_uppbound, float rectsize);

private slots:
  void drawprofil(int position,int direction);
  void drawcross(int xposition,int yposition);
  void drawroi();

signals:
  void clicked(int xposition,int yposition);
  void newProfile(const float *data, int npts, bool horizontal, int position);
  void newMask(const float *data);

private:

  static int scale_width(float lowbound, float uppbound);

  void draw_text(GuiPainter& gp, int xpos, int ypos, const char* txt) const;
  void draw_scale_text(GuiPainter& gp, int xpos, int ypos, float val) const;

  static int check_range(int val, int min, int max);

  int get_map_hue(float relval) const;
  int get_map_value(float relval) const;
  int get_map_saturation(float relval) const;

  int get_scale_size() const; // takes disable_scale into account

  bool disable_scale_cache;

  GuiPainter* roi_painter;
  mutable QPixmap* maplegend_pixmap;

  unsigned char* imagebuff;
  unsigned int len;

  const float* data_cache;
  unsigned int nx_cache;
  unsigned int ny_cache;

  mutable int scale_size_cache;

  float lowbound_cache;
  float uppbound_cache;

  unsigned int nx_map_cache;
  unsigned int ny_map_cache;
  float lowbound_map_cache;
  float uppbound_map_cache;
  bool fire_map_cache;

  float	*profile_x;
  float	*profile_y;
  unsigned int coarseFactor_cache;
  bool colormap_cache;
  long unsigned int i;

  STD_list<QPoint> roi_polygon;
  float* roi_mask;

  bool mouse_moved;
};




#endif
