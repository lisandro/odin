/***************************************************************************
                          float1d.h  -  description
                             -------------------
    begin                : Sun Aug 27 2000
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef FLOAT1D_H
#define FLOAT1D_H

#include "complex1d.h"

/**
  * This class displays a float array in a nice box with title
  */
class floatBox1D : public complexfloatBox1D  {
   Q_OBJECT

 public:
  floatBox1D(const float  *data,int n,QWidget *parent, const char *name, bool fixed_size, const char *xAxisLabel=0, const char *yAxisLabel=0, float min_x=0.0, float max_x=0.0, bool detachable=false);
  floatBox1D(const double *data,int n,QWidget *parent, const char *name, bool fixed_size, const char *xAxisLabel=0, const char *yAxisLabel=0, float min_x=0.0, float max_x=0.0, bool detachable=false);

  
 public slots:
  void refresh(const float* data,int n, float min_x, float max_x);
  void refresh(const float* data,int n) {refresh (data,n,0.0,0.0);}

  void refresh(const double* data,int n, float min_x, float max_x);
  void refresh(const double* data,int n) {refresh (data,n,0.0,0.0);}
};


#endif
