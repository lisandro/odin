
// Include Qt stuff first to avoid ambiguities with 'debug' symbol
#include "intedit.h"
#include "floatedit.h"
#include "float1d.h"
#include "complex1d.h"
#include "float3d.h"
#include "boolbutton.h"
#include "enumbox.h"
#include "stringbox.h"

#include "ldrwidget.h"
#include "ldrblockwidget.h"



#include <odinpara/ldrtypes.h>
#include <odinpara/ldrblock.h>
#include <odinpara/ldrfunction.h>
#include <odinpara/ldrarrays.h>
#include <odinpara/ldrfilter.h>


/**
  *
  * Helper class to analayze the value range of a (scalar) LDR parameter
  */
class RangeHelper {

public:
  double minval;
  double maxval;

  RangeHelper(const LDRbase& ldr) {
    minval=ldr.get_minval();
    maxval=ldr.get_maxval();
  }

  bool has_range() {
    if(maxval>minval) return true;
    else return false;
  }

  double get_stepsize(unsigned int maxsteps) {
    Log<OdinQt> odinlog("RangeHelper","get_stepsize");

    double result=fabs(maxval-minval)/double(maxsteps);
    double mantisse=log10(result);
    mantisse=int(mantisse-1.0);
    result=double(int(result*pow(10.0,-mantisse)))*pow(10.0,mantisse);

    ODINLOG(odinlog,normalDebug) << "minval=" << minval << STD_endl;
    ODINLOG(odinlog,normalDebug) << "maxval=" << maxval << STD_endl;
    ODINLOG(odinlog,normalDebug) << "mantisse=" << mantisse << STD_endl;
    ODINLOG(odinlog,normalDebug) << "stepsize=" << result << STD_endl;

    return result;
  }

};


/////////////////////////////////////////////////////////////////////////



LDRwidget::LDRwidget(LDRbase& ldr,unsigned int columns, QWidget *parent, bool doneButton, const char* omittext, bool storeLoadButtons )
              : QWidget(parent), val(ldr) {

  Log<OdinQt> odinlog(&val,"LDRwidget(...)");

  vport=this;

  widget_cache=0;


  blockwidget=0;
  intslider=0;
  intedit=0;
  floatslider=0;
  floatedit=0;
  enumwidget=0;
  boolwidget=0;
  floatArrempty=0;
  floatArrwidget1=0;
  floatArredit=0;
//  doubleArrwidget1=0;
  floatArrwidget2=0;
  complexArrwidget=0;
  stringwidget=0;
  filenamewidget=0;
  funcwidget=0;
  formulawidget=0;
  triplewidget=0;

  grid=0;

  rows=1;
  cols=1;

  if(columns<1) columns=1;

  bool cast_success=true;

  ldrlabel_uncut=replaceStr(ldr.get_label(),omittext,"",allOccurences);
  ldrlabel=ldrlabel_uncut;

  label_cut=false;
  if(ldrlabel.length()>_MAX_LABEL_LENGTH_) {
    label_cut=true;
    ldrlabel=ldrlabel_uncut.substr(0,_MAX_LABEL_LENGTH_)+"...";
  }
  
  STD_string unit(ldr.get_unit());
  if(unit!="") ldrlabel+=" ["+unit+"]";

  ODINLOG(odinlog,normalDebug) << "ldrlabel/omittext=" << ldrlabel  << "/" << omittext << STD_endl;

  grid=new GuiGridLayout( vport, 1, 1, false);

  ODINLOG(odinlog,normalDebug) << "grid & layout done" << STD_endl;



/*---------------- LDR types BEGIN ------------------------*/


  /*---------------- LDRblock ------------------------*/
  LDRblock* dumblock=0;
  dumblock=ldr.cast(dumblock);
  if(dumblock) {
    label_cut=false;
    if(dumblock->is_embedded()) cols=2;
    blockwidget=new LDRblockWidget(*dumblock,columns,vport,doneButton,false,omittext,storeLoadButtons);
    ODINLOG(odinlog,normalDebug) << "LDRblock: LDRblockWidget done" << STD_endl;
    GuiGridLayout::Alignment alignment = GuiGridLayout::Default;
    if(dumblock->numof_pars()<_MAX_SUBWIDGETS_FOR_ALIGNCENT_) alignment=GuiGridLayout::VCenter;
    set_widget(blockwidget ,alignment, true);
    ODINLOG(odinlog,normalDebug) << "LDRblock: set_widget done" << STD_endl;
    connect(blockwidget,SIGNAL(valueChanged()),this,SLOT(emitValueChanged()));
    connect(blockwidget,SIGNAL(doneButtonPressed()),this,SLOT(emitDone()));
    ODINLOG(odinlog,normalDebug) << "LDRblock: connect done" << STD_endl;
  }
  ODINLOG(odinlog,normalDebug) << "LDRblock done" << STD_endl;


  /*---------------- LDRint  ------------------------*/

  int* dumint=0;
  dumint=ldr.cast(dumint);
  if(dumint) {
    RangeHelper intrange(ldr);
    if(intrange.has_range() && ldr.get_parmode()==edit) {
      cols=2;
      long step=int(intrange.get_stepsize(_MAX_SLIDER_STEPS_));
      if(step<1) step=1;
      ODINLOG(odinlog,normalDebug) << "processing >" << ldrlabel << "<" <<  STD_endl;
      intslider=new intScientSlider(int(intrange.minval),int(intrange.maxval),step,*dumint,vport,ldrlabel.c_str());
      set_widget(intslider);
      connect(intslider,SIGNAL(intScientSliderValueChanged( int )),this,SLOT(changeLDRint( int )));
      connect(this,SIGNAL(newintval( int )),intslider,SLOT(setintScientSliderValue( int )));

    } else {
      intedit=new intLineBox(*dumint,vport,ldrlabel.c_str());
      set_widget(intedit);
      connect(intedit,SIGNAL(intLineBoxValueChanged( int )),this,SLOT(changeLDRint( int )));
      connect(this,SIGNAL(newintval( int )),intedit,SLOT(setintLineBoxValue( int )));
    }
  }
  ODINLOG(odinlog,normalDebug) << "LDRint done" << STD_endl;


  /*---------------- LDRfloat & LDRdouble ------------------------*/

  float* dumfloat=0;
  dumfloat=ldr.cast(dumfloat);
  double* dumdouble=0;
  dumdouble=ldr.cast(dumdouble);
  if(dumfloat || dumdouble ) {
    float val=0.0;
    if(dumfloat) val=(*dumfloat);
    if(dumdouble) val=(*dumdouble);
    RangeHelper floatrange(ldr);
    if(floatrange.has_range() && ldr.get_parmode()==edit) {
      cols=2;
      floatslider=new floatScientSlider(floatrange.minval,floatrange.maxval,floatrange.get_stepsize(_MAX_SLIDER_STEPS_),val,_FLOAT_DIGITS_,vport,ldrlabel.c_str());
      set_widget(floatslider);
      connect(floatslider,SIGNAL(floatScientSliderValueChanged( float )),this,SLOT(changeLDRfloat( float )));
      connect(this,SIGNAL(newfloatval( float )),floatslider,SLOT(setfloatScientSliderValue( float )));
    } else {
      floatedit=new floatLineBox(val,_FLOAT_DIGITS_,vport,ldrlabel.c_str());
      set_widget(floatedit);
      connect(floatedit,SIGNAL(floatLineBoxValueChanged( float )),this,SLOT(changeLDRfloat( float )));
      connect(this,SIGNAL(newfloatval( float )),floatedit,SLOT(setfloatLineBoxValue( float )));
    }
  }
  ODINLOG(odinlog,normalDebug) << "LDRfloat & LDRdouble done" << STD_endl;


  /*---------------- LDRenum ------------------------*/

  LDRenum* dumenum=0;
  dumenum=ldr.cast(dumenum);
  if(dumenum) {
    enumwidget=new enumBox(dumenum->get_alternatives(),vport,ldrlabel.c_str());
    enumwidget->setValue(dumenum->get_item_index());
    set_widget(enumwidget);
    ODINLOG(odinlog,normalDebug) << "widget done" << STD_endl;
    connect(enumwidget,SIGNAL(newVal( int )), this,SLOT(changeLDRenum( int )));
    connect(this,SIGNAL(newenumval( int )),enumwidget,SLOT(setValue( int )));
    ODINLOG(odinlog,normalDebug) << "connect done" << STD_endl;
  } else cast_success=false;
  ODINLOG(odinlog,normalDebug) << "LDRenum done" << STD_endl;



  /*---------------- LDRbool ------------------------*/

  bool* dumbool=0;
  dumbool=ldr.cast(dumbool);
  if(dumbool) {
    boolwidget=new buttonBox("Yes","No",*dumbool, vport, ldrlabel.c_str());
    set_widget(boolwidget);
    connect(boolwidget,SIGNAL(buttonToggled(bool)), this,SLOT(changeLDRbool( bool )));
    connect(this,SIGNAL(newboolval( bool )),boolwidget,SLOT(setToggleState( bool )));
  } else cast_success=false;
  ODINLOG(odinlog,normalDebug) << "LDRbool done" << STD_endl;


  /*---------------- LDRaction ------------------------*/

  LDRaction* dumaction=0;
  dumaction=ldr.cast(dumaction);
  if(dumaction && (dumaction->get_parmode()==edit) ) {
    actionwidget=new 	buttonBox("Now",vport,ldrlabel.c_str());
    set_widget(actionwidget);
    connect(actionwidget,SIGNAL(buttonClicked()), this,SLOT(changeLDRaction()));
  } else cast_success=false;
  ODINLOG(odinlog,normalDebug) << "LDRaction done" << STD_endl;


  /*---------------- LDRfloatArr ------------------------*/


  farray* dumfarray=0;
  dumfarray=ldr.cast(dumfarray);
  if(dumfarray) {
    create_or_update_floatArrwidget(*dumfarray,true);
  } else cast_success=false;
  ODINLOG(odinlog,normalDebug) << "LDRfloatArr done" << STD_endl;


  /*---------------- LDRdoubleArr ------------------------*/


  darray* dumdarray=0;
  dumdarray=ldr.cast(dumdarray);
  if(dumdarray) {
    ODINLOG(odinlog,normalDebug) << "dumdarray=" << (void*)dumdarray << "/" << STD_string(dumdarray->get_extent()) << STD_endl;
    farray farr(dumdarray->get_extent());
    for(unsigned int i=0; i<farr.total(); i++) farr[i]=(*dumdarray)[i];
    ODINLOG(odinlog,normalDebug) << "farr=" << STD_string(farr.get_extent()) << STD_endl;
    create_or_update_floatArrwidget(farr,true);
  } else cast_success=false;
  ODINLOG(odinlog,normalDebug) << "LDRdoubleArr done" << STD_endl;


  /*---------------- LDRcomplexArr ------------------------*/



  carray* dumcarray=0;
  dumcarray=ldr.cast(dumcarray);
  if(dumcarray) {
    ODINLOG(odinlog,normalDebug) << "cast succesful" << STD_endl;
    ODINLOG(odinlog,normalDebug) << "dumcarray->dim()=" << dumcarray->dim() << STD_endl;
    if(dumcarray->dim()==1) {
      fvector ampvec=amplitude(*dumcarray);
      fvector phavec=phase(*dumcarray);

      rows=3;
      cols=2;
      ODINLOG(odinlog,normalDebug) << "rows/cols=" << rows << "/" << cols << STD_endl;

      ArrayScale x_axprops=val.get_gui_props().scale[xPlotScale];
      ArrayScale y1_axprops=val.get_gui_props().scale[yPlotScaleLeft];
      ArrayScale y2_axprops=val.get_gui_props().scale[yPlotScaleRight];
      bool fixedsize=val.get_gui_props().fixedsize;


      complexArrwidget=new complexfloatBox1D(ampvec.c_array(),phavec.c_array(),dumcarray->length(),vport,ldrlabel.c_str(),fixedsize,
                                     x_axprops.get_label_with_unit().c_str(), y1_axprops.get_label_with_unit().c_str(), y2_axprops.get_label_with_unit().c_str(),
                                     x_axprops.minval,x_axprops.maxval,val.get_gui_props().fixedsize);
      set_widget(complexArrwidget, GuiGridLayout::Default, true);
      connect(this,SIGNAL(newcomplexArr( const float*, const float*, int, float, float)),
        complexArrwidget,SLOT(refresh( const float*, const float*, int, float, float)));
    }
  } else cast_success=false;
  ODINLOG(odinlog,normalDebug) << "LDRcomplexArr done" << STD_endl;


  /*---------------- LDRfunction ------------------------*/


  LDRfilter("dummy"); // created dummy to force initialization of registered_functions (in USING_WIN32/DLL)
  ODINLOG(odinlog,normalDebug) << "Testing for function" << STD_endl;
  LDRfunction* dumfunc=0;
  dumfunc=ldr.cast(dumfunc);
  if(dumfunc) {
    ODINLOG(odinlog,normalDebug) << "LDR is function" << STD_endl;
    cols=2;
    funcwidget=new enumBox(dumfunc->get_alternatives(),vport,ldrlabel.c_str(),true,true);
    ODINLOG(odinlog,normalDebug) << "funcwidget done" << STD_endl;
    funcwidget->setValue(dumfunc->get_function_index());
    ODINLOG(odinlog,normalDebug) << "setValue done" << STD_endl;
    set_widget(funcwidget);
    ODINLOG(odinlog,normalDebug) << "set_widget done" << STD_endl;
    connect(funcwidget,SIGNAL(newVal( int )), this,SLOT(changeLDRfunction( int )));
    connect(funcwidget,SIGNAL(edit()), this,SLOT(editLDRfunction()));
    connect(funcwidget,SIGNAL(info()), this,SLOT(infoLDRfunction()));
    connect(this,SIGNAL(newfuncval( int )),funcwidget,SLOT(setValue( int )));
    ODINLOG(odinlog,normalDebug) << "connect done" << STD_endl;
  } else cast_success=false;
  ODINLOG(odinlog,normalDebug) << "LDRfunction done" << STD_endl;


  /*---------------- LDRstring ------------------------*/


  STD_string* dumstring=0;
  dumstring=ldr.cast(dumstring);
  if(dumstring) {
    cols=2;
    stringwidget=new stringBox(dumstring->c_str(),vport,ldrlabel.c_str());
    set_widget(stringwidget);
    connect(stringwidget,SIGNAL(stringBoxTextEntered( const char* )), this,SLOT(changeLDRstring( const char* )));
    connect(this,SIGNAL(newstringval( const char* )),stringwidget,SLOT(setstringBoxText( const char* )));
  } else cast_success=false;
  ODINLOG(odinlog,normalDebug) << "LDRstring done" << STD_endl;


  /*---------------- LDRfileName ------------------------*/


  LDRfileName* dumfileName=0;
  dumfileName=ldr.cast(dumfileName);
  if(dumfileName) {
    cols=2;
    filenamewidget=new stringBox(dumfileName->c_str(),vport,ldrlabel.c_str(),"Browse");
    set_widget(filenamewidget);
    connect(filenamewidget,SIGNAL(stringBoxTextEntered( const char* )), this,SLOT(changeLDRfileName( const char* )));
    connect(filenamewidget,SIGNAL(stringBoxButtonPressed()), this,SLOT(browseLDRfileName()));
    connect(this,SIGNAL(newfilenameval( const char* )),filenamewidget,SLOT(setstringBoxText( const char* )));
  } else cast_success=false;
  ODINLOG(odinlog,normalDebug) << "LDRfileName done" << STD_endl;


  /*---------------- LDRformula ------------------------*/


  LDRformula* dumformula=0;
  dumformula=ldr.cast(dumformula);
  if(dumformula) {
    cols=2;
    formulawidget=new stringBox(dumformula->c_str(),vport,ldrlabel.c_str(),"Syntax");
    set_widget(formulawidget);
    connect(formulawidget,SIGNAL(stringBoxTextEntered( const char* )), this,SLOT(changeLDRformula( const char* )));
    connect(formulawidget,SIGNAL(stringBoxButtonPressed()), this,SLOT(infoLDRformula()));
    connect(this,SIGNAL(newformulaval( const char* )),formulawidget,SLOT(setstringBoxText( const char* )));
  } else cast_success=false;
  ODINLOG(odinlog,normalDebug) << "LDRformula done" << STD_endl;


  /*---------------- LDRtriple ------------------------*/


  LDRtriple* dumtriple=0;
  dumtriple=ldr.cast(dumtriple);
  if(dumtriple) {
    cols=2;
    triplewidget=new floatLineBox3D((*dumtriple)[0],(*dumtriple)[1],(*dumtriple)[2],_FLOAT_DIGITS_,vport,ldrlabel.c_str());
    set_widget(triplewidget);
    connect(triplewidget,SIGNAL(floatLineBox3DValueChanged(float,float,float)), this,SLOT(changeLDRtriple(float,float,float)));
    connect(this,SIGNAL(newtripleval(float,float,float)),triplewidget,SLOT(setfloatLineBox3DValue(float,float,float)));
  } else cast_success=false;
  ODINLOG(odinlog,normalDebug) << "LDRtriple done" << STD_endl;


/*---------------- LDR types swith END ------------------------*/

  if(!cast_success) {
    ODINLOG(odinlog,normalDebug) << "Unable to proces parameter >" << ldrlabel << "<" << STD_endl;
  }

  return;
}


void LDRwidget::write_pixmap(const char* fname, const char* format, bool dump_all) const {
  if(floatArrwidget2) floatArrwidget2->write_pixmap(fname, format, dump_all);
}

void LDRwidget::write_legend(const char* fname, const char* format) const {
  if(floatArrwidget2) floatArrwidget2->write_legend(fname, format);
}

void LDRwidget::write_map_legend(const char* fname, const char* format) const {
  if(floatArrwidget2) floatArrwidget2->write_map_legend(fname, format);
}



LDRwidget::~LDRwidget(){
  // do not acces 'val' here because it might be deleted already
  if(blockwidget) delete blockwidget;
  if(intslider) delete intslider;
  if(intedit) delete intedit;
  if(floatslider) delete floatslider;
  if(floatedit) delete floatedit;
  if(enumwidget) delete enumwidget;
  if(boolwidget) delete boolwidget;
  if(floatArrempty) delete floatArrempty;
  if(floatArrwidget1) delete floatArrwidget1;
  if(floatArredit) delete floatArredit;
  if(floatArrwidget2) delete floatArrwidget2;
//  if(doubleArrwidget1) delete doubleArrwidget1;
  if(complexArrwidget) delete complexArrwidget;
  if(funcwidget) delete funcwidget;
  if(stringwidget) delete stringwidget;
  if(filenamewidget) delete filenamewidget;
  if(formulawidget) delete formulawidget;
  if(triplewidget) delete triplewidget;
  if(grid) delete grid;
}

STD_string LDRwidget::get_label() const {return val.get_label();}

void LDRwidget::set_widget(QWidget *widget, GuiGridLayout::Alignment alignment, bool override_enabled) {
  Log<OdinQt> odinlog(&val,"set_widget");

  widget_cache=widget;

  parameterMode parmode=val.get_parmode();
  ODINLOG(odinlog,normalDebug) << "parmode(" << ldrlabel << ")=" << parmode << STD_endl;
  if(parmode==hidden) return;
  bool enabled=true;
  if(parmode>=noedit) enabled=false;
  if(override_enabled) enabled=true;
  ODINLOG(odinlog,normalDebug) << "enabled=" << enabled << STD_endl;
  widget_cache->setEnabled(enabled);
  ODINLOG(odinlog,normalDebug) << "parameterMode done" << STD_endl;

  bool show_tooltip=label_cut;
  STD_string tooltip=ldrlabel_uncut;
  STD_string descr=val.get_description();
  if(descr!="") {
    tooltip+=": "+descr;
    tooltip=justificate(tooltip);
    show_tooltip=true;
  }
  if(show_tooltip) add_tooltip(widget_cache,tooltip.c_str());

  grid->add_widget( widget, 0, 0, alignment);
  widget->show();
}


unsigned int LDRwidget::get_sizedfarray_size_and_factor(unsigned int& nx, unsigned int& ny, unsigned int& nz) const {
  Log<OdinQt> odinlog(&val,"get_sizedfarray_size_and_factor");

  PixmapProps pixprops=val.get_gui_props().pixmap;

  int dim=sizedfarray.dim();
  nx=sizedfarray.size(dim-1);
  ny=sizedfarray.size(dim-2);
  nz=1;
  if(dim==3) nz=sizedfarray.size(dim-3);
  unsigned int factor=1;
  ODINLOG(odinlog,normalDebug) << "ny/nx=" << ny << "/" << nx << STD_endl;
  ODINLOG(odinlog,normalDebug) << "minsize/maxsize=" << pixprops.minsize << "/" << pixprops.maxsize << STD_endl;
  if (nx >= ny)   factor=(unsigned int)secureDivision(pixprops.minsize,ny);
  else            factor=(unsigned int)secureDivision(pixprops.minsize,nx);
  if(!factor) factor=1;
  if( (factor*nx) > pixprops.maxsize) factor=(unsigned int)secureDivision(pixprops.maxsize,nx);
  if( (factor*ny) > pixprops.maxsize) factor=(unsigned int)secureDivision(pixprops.maxsize,ny);
  if(!factor) factor=1;
  return factor;
}






void LDRwidget::create_or_update_floatArrwidget(const farray& arr, bool initial) {
  Log<OdinQt> odinlog(&val,"create_or_update_floatArrwidget");

  rows=2;
  cols=2;

  ArrayScale x_axprops=val.get_gui_props().scale[xPlotScale];
  ArrayScale y_axprops=val.get_gui_props().scale[yPlotScaleLeft];

  sizedfarray=arr;
  sizedfarray.autosize();
  unsigned int newdim=sizedfarray.dim();
  unsigned int newsize=sizedfarray.total();
  if(newdim==3) newdim=2; // treat 2D and 3D equally
  if(newsize==0) newdim=0; // treat zero size as zero dim
  ODINLOG(odinlog,normalDebug) << "newdim/newsize=" << newdim << "/" << newsize << STD_endl;


  if(!initial) {
    unsigned int olddim=oldfarraysize.dim();
    unsigned int oldsize=oldfarraysize.total();
    if(olddim==3) olddim=2; // treat 2D and 3D equally
    bool delete_old=false;
    if(newdim!=olddim) delete_old=true;
    if(newdim==1 && (newsize!=oldsize) && (newsize==1 || oldsize==1)) delete_old=true;
    if(newdim>1 && sizedfarray.get_extent()!=oldfarraysize) delete_old=true;
    ODINLOG(odinlog,normalDebug) << "newdim/olddim/newsize/oldsize/delete_old=" << newdim << "/" << olddim << "/" << newsize << "/" << oldsize << "/" << delete_old << STD_endl;
    if(delete_old) {
      if(floatArrempty)   {floatArrempty->hide();   delete floatArrempty;   floatArrempty=0;}
      if(floatArrwidget1) {floatArrwidget1->hide(); delete floatArrwidget1; floatArrwidget1=0;}
      if(floatArredit)    {floatArredit->hide();    delete floatArredit;    floatArredit=0;}
      if(floatArrwidget2) {floatArrwidget2->hide(); delete floatArrwidget2; floatArrwidget2=0;}
    }
  }


  if(newdim==0) {
    if(!floatArrempty) {
      floatArrempty=new QLabel(this);
      floatArrempty->setText((ldrlabel+"(Empty)").c_str());
      set_widget(floatArrempty);
    }
  }


  if(newdim==1) {
    if(newsize==1) {
      rows=1;
      cols=1;
      if(floatArredit) {
        emit newfloatval( sizedfarray[0] );
      } else {
        floatArredit=new floatLineBox(sizedfarray[0],_FLOAT_DIGITS_,vport,ldrlabel.c_str());
        set_widget(floatArredit);
        connect(floatArredit,SIGNAL(floatLineBoxValueChanged( float )),this,SLOT(changeLDRfloat( float )));
        connect(this,SIGNAL(newfloatval( float )),floatArredit,SLOT(setfloatLineBoxValue( float )));
      }
    }

    if(newsize>1) {
      rows=3;
      ODINLOG(odinlog,normalDebug) << "x_label/unit=" << x_axprops.label << "/" << x_axprops.unit << STD_endl;
      ODINLOG(odinlog,normalDebug) << "y_label/unit=" << y_axprops.label << "/" << y_axprops.unit << STD_endl;
      if(floatArrwidget1) {
        emit newfloatArr1( sizedfarray.c_array(), sizedfarray.length(), x_axprops.minval,x_axprops.maxval );
      } else {
        floatArrwidget1=new floatBox1D(sizedfarray.c_array(),sizedfarray.length(),vport,ldrlabel.c_str(),val.get_gui_props().fixedsize,
                                       x_axprops.get_label_with_unit().c_str(),y_axprops.get_label_with_unit().c_str(),
                                       x_axprops.minval,x_axprops.maxval,val.get_gui_props().fixedsize);
        set_widget(floatArrwidget1, GuiGridLayout::Default, true);
        connect(this,SIGNAL(newfloatArr1( const float*, int, float, float)), floatArrwidget1,SLOT(refresh( const float*, int, float, float)));
      }
    }
  }

  if(newdim==2) {
    ArrayScale dispscale=val.get_gui_props().scale[displayScale];
    ODINLOG(odinlog,normalDebug) << "dispscale.minval/maxval=" << dispscale.minval << "/" << dispscale.maxval << STD_endl;
    if(val.get_gui_props().pixmap.autoscale) {
      dispscale.minval=sizedfarray.minvalue();
      dispscale.maxval=sizedfarray.maxvalue();
      ODINLOG(odinlog,normalDebug) << "Autoscaling with min/max=" << dispscale.minval << "/" << dispscale.maxval << STD_endl;
      sizedfarray.normalize();
    }
    if(floatArrwidget2) {
      ODINLOG(odinlog,normalDebug) << "Emitting new c_array" << STD_endl;
      emit newfloatArr2( sizedfarray.c_array(),dispscale.minval,dispscale.maxval);

      mapfarray=val.get_gui_props().pixmap.overlay_map;
//      mapfarray.normalize();
      if(mapfarray.total()) {
        ODINLOG(odinlog,normalDebug) << "New overlay_map" << STD_endl;
        float mapmin, mapmax;
        val.get_gui_props().pixmap.get_overlay_range(mapmin, mapmax);
        emit newfloatArrMap(mapfarray.c_array(), mapmin, mapmax, val.get_gui_props().pixmap.overlay_rectsize);
      }

    } else {
      unsigned int nx,ny,nz;
      unsigned int factor=get_sizedfarray_size_and_factor(nx,ny,nz);
      bool colormap=val.get_gui_props().pixmap.color;
      overlay_map=val.get_gui_props().pixmap.overlay_map; // create local copy for c_array()
      bool olm_firescale=val.get_gui_props().pixmap.overlay_firescale;
      float olm_rectsize=val.get_gui_props().pixmap.overlay_rectsize;
      const float* olm=0;
      int olm_x=1;
      int olm_y=1;
      int olm_z=1;
      float olm_upp=0.0;
      float olm_low=0.0;
      if(overlay_map.length()) {
        val.get_gui_props().pixmap.get_overlay_range(olm_low, olm_upp);

//        olm_upp=overlay_map.maxvalue();
//        olm_low=overlay_map.minvalue();
//        overlay_map.normalize();
        olm=overlay_map.c_array();
        int olm_dim=overlay_map.dim();
        if(olm_dim>2) olm_z=overlay_map.size(olm_dim-3);
        if(olm_dim>1) olm_y=overlay_map.size(olm_dim-2);
        if(olm_dim>0) olm_x=overlay_map.size(olm_dim-1);
        ODINLOG(odinlog,normalDebug) << "olm/olm_z/olm_y/olm_x=" << olm << "/" << olm_z << "/" << olm_y << "/" << olm_x << STD_endl;
      }


      floatArrwidget2=new floatBox3D(sizedfarray.c_array(),dispscale.minval,dispscale.maxval,nx,ny,nz,
        !dispscale.enable,factor,vport,ldrlabel.c_str(),
        olm,olm_low,olm_upp,olm_x,olm_y,olm_z,olm_firescale,olm_rectsize,colormap);
      set_widget(floatArrwidget2, GuiGridLayout::Default, true);
      connect(floatArrwidget2,SIGNAL(clicked(int,int,int)),this,SLOT(emitClicked(int, int, int)));
      connect(floatArrwidget2,SIGNAL(newProfile(const float*, int, bool, int)),this,SLOT(emitNewProfile(const float*, int, bool, int)));
      connect(floatArrwidget2,SIGNAL(newMask(const float*, int)),this,SLOT(emitNewMask(const float*, int)));
      connect(this,SIGNAL(newfloatArr2( const float*, float, float)),floatArrwidget2,SLOT(refresh( const float*, float, float)));
      connect(this,SIGNAL(newfloatArrMap( const float*, float, float, float)),floatArrwidget2,SLOT(refreshMap( const float*, float, float, float )));
    }
  }

  oldfarraysize=sizedfarray.get_extent();
}




void LDRwidget::emitValueChanged() {
  emit valueChanged();
}


void LDRwidget::emitDone() {
  emit doneButtonPressed();
}


void LDRwidget::updateWidget() {
  Log<OdinQt> odinlog(&val,"updateWidget");
  emit updateSubWidget();

  if(blockwidget) blockwidget->updateWidget();

  // for all 1D/2D plots, get again, might have been changed
  ArrayScale x_axprops=val.get_gui_props().scale[xPlotScale];
  PixmapProps pixprops=val.get_gui_props().pixmap;

  if(intslider || intedit) {
    ODINLOG(odinlog,normalDebug) << "newintval(" << val.get_label() << ")=" << val.printvalstring() << STD_endl;
    emit newintval( atoi(val.printvalstring().c_str()) );
  }

  if(floatslider || floatedit) {
    float* dumfloat=0;
    dumfloat=val.cast(dumfloat);
    double* dumdouble=0;
    dumdouble=val.cast(dumdouble);
    float floatval=0.0;
    if(dumfloat) floatval=(*dumfloat);
    if(dumdouble) floatval=(*dumdouble);
    emit newfloatval( floatval );
  }

  if(enumwidget) {
   LDRenum* dumenum=0;
   dumenum=val.cast(dumenum);
   if(dumenum) emit newenumval( dumenum->get_item_index() );
  }

  if(boolwidget) {
   bool* dumbool=0;
   dumbool=val.cast(dumbool);
   if(dumbool) emit newboolval(*dumbool);
  }

  if(complexArrwidget) {
    carray* dumcarray=0;
    dumcarray=val.cast(dumcarray);
    if(dumcarray) {
      if(dumcarray->dim()==1) {
        fvector ampvec=amplitude(*dumcarray);
        fvector phavec=phase(*dumcarray);
        emit newcomplexArr( ampvec.c_array(), phavec.c_array(), dumcarray->length(), x_axprops.minval,x_axprops.maxval);
      }
    }
  }

  if(floatArrempty || floatArrwidget1 || floatArrwidget2 || floatArredit) {
    farray* dumfarray=0;
    dumfarray=val.cast(dumfarray);
    if(dumfarray) {
      create_or_update_floatArrwidget(*dumfarray,false);
    }
    darray* dumdarray=0;
    dumdarray=val.cast(dumdarray);
    if(dumdarray) {
      ODINLOG(odinlog,normalDebug) << "dumdarray=" << (void*)dumdarray << "/" << STD_string(dumdarray->get_extent()) << STD_endl;
      farray farr(dumdarray->get_extent());
      for(unsigned int i=0; i<farr.total(); i++) farr[i]=(*dumdarray)[i];
      create_or_update_floatArrwidget(farr,false);
    }
  }

  if(funcwidget) {
    LDRfunction* dumfunc=0;
    dumfunc=val.cast(dumfunc);
    if(dumfunc) emit newfuncval( dumfunc->get_function_index() );
    ODINLOG(odinlog,normalDebug) << "get_function_index()=" << dumfunc->get_function_index() << STD_endl;

    for(STD_list<LDRwidgetDialog*>::iterator it=subdialogs.begin(); it!=subdialogs.end(); ++it) {
      (*it)->updateWidget();
    }
  }

  if(stringwidget) {
    STD_string* dumstring=0;
    dumstring=val.cast(dumstring);
    if(dumstring) {
      const char* str=dumstring->c_str();
      ODINLOG(odinlog,normalDebug) << "str=" << str << STD_endl;
      emit newstringval(str);
    }
  }

  if(filenamewidget) {
    LDRfileName* dumfileName=0;
    dumfileName=val.cast(dumfileName);
    if(dumfileName) emit newfilenameval( dumfileName->c_str() );
  }

  if(formulawidget) {
    LDRformula* dumformula=0;
    dumformula=val.cast(dumformula);
    if(dumformula) emit newformulaval( dumformula->c_str() );
  }

  if(triplewidget) {
    LDRtriple* dumtriple=0;
    dumtriple=val.cast(dumtriple);
    if(dumtriple) emit newtripleval( (*dumtriple)[0],(*dumtriple)[1],(*dumtriple)[2] );
  }

}

void LDRwidget::deleteDialogs() {
  emit deleteSubDialogs();
  for(STD_list<LDRwidgetDialog*>::iterator it=subdialogs.begin(); it!=subdialogs.end(); ++it) {
    (*it)->hide();
//    delete (*it);
  }
  subdialogs.clear();
}


void LDRwidget::changeLDRint( int newval ) {
  int* dumint=0;
  dumint=val.cast(dumint);
  if(dumint) (*dumint)=newval;
  long* dumlong=0;
  dumlong=val.cast(dumlong);
  if(dumlong) (*dumlong)=newval;
  emit valueChanged();
}


void LDRwidget::changeLDRfloat( float newval ) {
  Log<OdinQt> odinlog(&val,"changeLDRfloat");
  ODINLOG(odinlog,normalDebug) << "newval=" << newval << STD_endl;
  float* dumfloat=0;
  dumfloat=val.cast(dumfloat);
  if(dumfloat) (*dumfloat)=newval;
  double* dumdouble=0;
  dumdouble=val.cast(dumdouble);
  if(dumdouble) (*dumdouble)=newval;
  farray* dumfarray=0;
  dumfarray=val.cast(dumfarray);
  if(dumfarray && dumfarray->length()) dumfarray[0]=newval;
  darray* dumdarray=0;
  dumdarray=val.cast(dumdarray);
  if(dumdarray && dumdarray->length()) dumdarray[0]=newval;
 //  STD_string dummy=LDRdouble(newval).printvalstring();
//  val.parsevalstring(dummy);
  ODINLOG(odinlog,normalDebug) << "val=" << val.printvalstring() << STD_endl;
  emit valueChanged();
}


void LDRwidget::changeLDRenum( int newval ) {
  LDRenum* dumenum=0;
  dumenum=val.cast(dumenum);
  if(dumenum) dumenum->set_item_index(newval);
  emit valueChanged();
}


void LDRwidget::changeLDRbool( bool newval ) {
  bool* dumbool=0;
  dumbool=val.cast(dumbool);
  if(dumbool) (*dumbool)=newval;
  emit valueChanged();
}


void LDRwidget::changeLDRaction() {
  LDRaction* dumaction=0;
  dumaction=val.cast(dumaction);
  if(dumaction) dumaction->trigger_action();
  emit valueChanged();
}


void LDRwidget::changeLDRfunction( int newval ) {
  Log<OdinQt> odinlog(&val,"changeLDRfunction");
  ODINLOG(odinlog,normalDebug) << "newval=" << newval << STD_endl;
  deleteDialogs();
  LDRfunction* dumfunc=0;
  dumfunc=val.cast(dumfunc);
  if(dumfunc) dumfunc->set_function(newval);
  emit valueChanged();
}


void LDRwidget::changeLDRstring( const char* newval ) {
  Log<OdinQt> odinlog(&val,"changeLDRstring");
  ODINLOG(odinlog,normalDebug) << "newval=" << newval << STD_endl;
  STD_string* dumstring=0;
  dumstring=val.cast(dumstring);
  if(dumstring) {
    STD_string newval_str=STD_string(newval); // direct assignment gives bogus results on MacOS 10.4 with GCC4 and std::string
    (*dumstring)=newval_str;
    ODINLOG(odinlog,normalDebug) << "newval_str/dumstring=" << newval_str << "/" << (*dumstring) << STD_endl;
  }
  emit valueChanged();
}


void LDRwidget::changeLDRfileName( const char* newval ) {
  LDRfileName* dumfilename=0;
  dumfilename=val.cast(dumfilename);
  if(dumfilename) {
    STD_string newvalstr(newval);
    (*dumfilename)=newvalstr;
  }
  emit valueChanged();
}


void LDRwidget::changeLDRformula( const char* newval ) {
  LDRformula* dumformula=0;
  dumformula=val.cast(dumformula);
  if(dumformula) (*dumformula)=newval;
  emit valueChanged();
}


void LDRwidget::changeLDRtriple( float xval,float yval,float zval ) {
  LDRtriple* dumtriple=0;
  dumtriple=val.cast(dumtriple);
  if(dumtriple) {
    (*dumtriple)[0]=xval;
    (*dumtriple)[1]=yval;
    (*dumtriple)[2]=zval;
  }
  emit valueChanged();
}


void LDRwidget::browseLDRfileName() {
  Log<OdinQt> odinlog(&val,"browseLDRfileName");
  LDRfileName* dumfilename=0;
  dumfilename=val.cast(dumfilename);
  if(dumfilename) {
    STD_string suffix=dumfilename->get_suffix();
    ODINLOG(odinlog,normalDebug) << "suffix=" << suffix << STD_endl;
    if(suffix!="") suffix=STD_string(val.get_label())+" (*."+suffix+")";
    STD_string startdir=dumfilename->get_defaultdir(); // local copy for c_str()
    ODINLOG(odinlog,normalDebug) << "startdir/suffix=" << startdir << "/" << suffix << STD_endl;
    STD_string fname;
    if(dumfilename->is_dir())  fname=get_directory("Please select a directory", startdir.c_str(), vport);
    else                       fname=get_open_filename("Please select a file", startdir.c_str(), suffix.c_str(), vport);
    if (fname!="") {
      (*dumfilename)=fname;
      filenamewidget->setstringBoxText(fname.c_str());
    }
  }

  emit valueChanged();
}


void LDRwidget::infoLDRformula() {
  LDRformula* dumformula=0;
  dumformula=val.cast(dumformula);
  if(dumformula) {
    message_question((justificate(dumformula->get_syntax(),0,false,_INFOBOX_LINEWIDTH_)).c_str(), val.get_label().c_str(), vport);
  }
  emit valueChanged();
}


void LDRwidget::editLDRfunction() {
  LDRfunction* dumfunc=0;
  dumfunc=val.cast(dumfunc);
  if(dumfunc) {
    LDRwidgetDialog* dlg=new LDRwidgetDialog(*(dumfunc->get_funcpars_block()),1,vport);
    subdialogs.push_back(dlg);
    connect(dlg,SIGNAL(valueChanged()), this,SLOT(emitValueChanged()));
  }
  emit valueChanged();
}


void LDRwidget::infoLDRfunction() {
  LDRfunction* dumfunc=0;
  dumfunc=val.cast(dumfunc);
  if(dumfunc) {
    message_question((justificate(dumfunc->get_funcdescription(),0,false,_INFOBOX_LINEWIDTH_)).c_str(), (dumfunc->get_label()+STD_string(" info")).c_str(), vport);
  }
}

