/***************************************************************************
                          float3d.h  -  description
                             -------------------
    begin                : Thu Nov 10 2005
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef FLOAT3D_H
#define FLOAT3D_H

#include <qgroupbox.h>

#include "float2d.h"


/**
  * QGroupBox that contains a floatLabel2D and a slider to display 3D float arrays
  */
class floatBox3D : public QGroupBox  {
   Q_OBJECT

 public:
  floatBox3D(const float *data, float lowbound, float uppbound, long int nx, long int ny, long int nz,
             bool disable_scale, int coarseFactor, QWidget *parent, const char *name,
             const float *overlay_map=0, float lowbound_map=0.0, float uppbound_map=0.0, unsigned int nx_map=0, unsigned int ny_map=0, unsigned int nz_map=0, bool map_firescale=false, float map_rectsize=0.8,
             bool colormap=false);
  ~floatBox3D();


  void  write_pixmap(const char* fname, const char* format, bool dump_all=false) const;

  QLabel* get_map_legend(QWidget* parent) const {return label->get_map_legend(parent);}
  void write_legend(const char* fname, const char* format) const {label->write_legend(fname, format);}
  void write_map_legend(const char* fname, const char* format) const {label->write_map_legend(fname, format);}


 public slots:
  void refresh(const float* data, float lowbound, float uppbound);
  void refreshMap(const float* map, float lowbound_map, float uppbound_map, float rectsize) {map_cache=map; label->refreshMap(map_cache+onemapsize*get_current_z(),lowbound_map,uppbound_map,rectsize);}

 private slots:
  void emitClicked(int xposition, int yposition) {emit clicked(xposition,yposition,get_current_z());}
  void emitNewProfile(const float *data, int npts, bool horizontal, int position) {emit newProfile(data, npts, horizontal, position);}
  void emitNewMask(const float *data) {emit newMask(data,get_current_z());}

 protected slots:
  void changez(int iz);


 signals:
  void clicked(int x, int y, int z);
  void newProfile(const float *data, int npts, bool horizontal, int position);
  void newMask(const float* data, int slice);

 protected:

  floatLabel2D* label;  // GeoEditLabel needs access to it

  int get_current_z() const;

 private:

  virtual void repaint() {} // overload this function to draw additional stuff after changez

  void repaint_slice(int iz) const;

  GuiSlider* zslider;
  QLabel* zval;

  GuiGridLayout *grid;

  QLabel* maplegend;

  const float* data_cache;
  unsigned int oneimagesize;
  unsigned int nz_cache;
  float lowbound_cache;
  float uppbound_cache;

  const float* map_cache;
  float lowbound_map_cache;
  float uppbound_map_cache;
  float rectsize_map_cache;

  unsigned int onemapsize;

  float* mask3d;
};

#endif
