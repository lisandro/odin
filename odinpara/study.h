/***************************************************************************
                          study.h  -  description
                             -------------------
    begin                : Mon Mar 3 2003
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef STUDY_H
#define STUDY_H

#include <odinpara/ldrblock.h>
#include <odinpara/ldrtypes.h>
#include <odinpara/ldrnumbers.h>


#define ODIN_DATE_LENGTH 8
#define ODIN_DATE_FORMAT "%Y%m%d"
#define ODIN_TIME_LENGTH 6
#define ODIN_TIME_FORMAT "%H%M%S"


/**
  * @ingroup odinpara
  *
  * \brief Study information
  *
  * This class is used to hold study information
  */
class Study : public LDRblock {

 public:

/**
  * Constructs a Study object with the given label
  */
  Study(const STD_string& label="unnamedStudy");

/**
  * Constructs a copy of 'sp'
  */
  Study(const Study& s);


/**
  * Specifies the date/time of the scan:
  * -date: date in DICOM format, i.e yyyymmdd
  * -time: date in DICOM format, i.e hhmmss
  */
  Study& set_DateTime(const STD_string& date, const STD_string& time);

/**
  * Returns the date/time of the scan in the reference parameters, see set_DateTime() for their description
  */
  void get_DateTime(STD_string& date, STD_string& time) const;

/**
  * Set date/time of the scan to the current date/time
  */
  void set_timestamp();


/**
  * Specifies the patient information:
  * -id:         The unique ID of the patient
  * -full_name:  Patients full name
  * -birth_date: Patients date of birth in DICOM format, i.e yyyymmdd
  * -sex:        Patients sex (M,F,O)
  * -weight:     Patients weight [kg]
  * -size:       Patients size/height [mm]
  */
  Study& set_Patient(const STD_string& id, const STD_string& full_name, const STD_string& birth_date, char sex, float weight, float size);

/**
  * Returns the patient information in the reference parameters, see set_Patient() for their description
  */
  void get_Patient(STD_string& id, STD_string& full_name, STD_string& birth_date, char& sex, float& weight, float& size) const;


/**
  * Specifies the study context:
  * -description: Study description
  * -scientist:   Scientists name
  */
  Study& set_Context(const STD_string& description, const STD_string& scientist);

/**
  * Returns the study context in the reference parameters, see set_Context() for their description
  */
  void get_Context(STD_string& description, STD_string& scientist) const;


/**
  * Specifies the series context:
  * -description: Series description
  * -number:      Series number
  */
  Study& set_Series(const STD_string& description, int number);

/**
  * Returns the series context in the reference parameters, see set_Series() for their description
  */
  void get_Series(STD_string& description, int& number) const;


/**
  * Assignment operator
  */
  Study& operator = (const Study& s);



 private:
  void append_all_members();

  static void format_date(STD_string& result, const STD_string& date);
  static void format_time(STD_string& result, const STD_string& time);

  LDRstring ScanDate;
  LDRstring ScanTime;

  LDRstring PatientId;
  LDRstring PatientName;
  LDRstring PatientBirthDate;
  LDRenum   PatientSex;
  LDRfloat  PatientWeight;
  LDRfloat  PatientSize;

  LDRstring Description;
  LDRstring ScientistName;

  LDRstring SeriesDescription;
  LDRint    SeriesNumber;

};


#endif
