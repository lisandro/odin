/***************************************************************************
                          ldrtypes.h  -  description
                             -------------------
    begin                : Sun Jun 6 2004
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef LDRTYPES_H
#define LDRTYPES_H


#include <odinpara/ldrbase.h>


/**
  * @addtogroup ldr
  * @{
  */

//////////////////////////////////////////////////////////////////

/**
  *
  *  Labeled Data Record (LDR) class for representing strings
  */
class LDRstring : public STD_string, public virtual LDRbase  {

 public:

/**
  *  Default constructor
  */
  LDRstring () : STD_string() {}

/**
  *  Constructor with the following arguments:
  * - ss:            Initial value for the string
  * - name:          The label of the parameter
  */
  LDRstring (const STD_string& ss, const STD_string& name="");


/**
  *  Constructs a string of length i with c as their initial content
  */
  LDRstring (int i,const char c=' ') : STD_string(i,c) {}

/**
  *  Copy constructor from a C-string
  */
  LDRstring (const char *charptr) : STD_string(charptr) {}

/**
  *  Copy constructor
  */
  LDRstring (const LDRstring& str) {LDRstring::operator = (str);}

/**
  *  Assigns the string ss to the parameter
  */
  LDRstring& operator = (const STD_string& ss) {STD_string::operator = (ss); return *this;}

/**
  *  Assigns the C-string charptr to the parameter
  */
  LDRstring& operator = (const char *charptr) {STD_string::operator = (charptr); return *this;}

/**
  *  Copy assignment
  */
  LDRstring& operator = (const LDRstring& ss);

/**
  *  Final overrider for stream output
  */
  friend STD_ostream& operator << (STD_ostream& s,const LDRstring& t) {return s << STD_string(t);}


  // overwriting virtual functions from LDRbase
  bool parsevalstring(const STD_string& parstring, const LDRserBase* ser=0);
  STD_string printvalstring(const LDRserBase* ser=0) const;
  STD_string get_parx_code(parxCodeType type) const;
  STD_string get_typeInfo(bool parx_equivtype=false) const {if(parx_equivtype) return "char"; return "string";}
  LDRbase* create_copy() const {return new LDRstring(*this);}
  STD_string* cast(STD_string*) {return this;}


};



//////////////////////////////////////////////////////////////////

/**
  *
  *  Labeled Data Record (LDR) class for representing Boolean values
  */
class LDRbool : public virtual LDRbase  {

 public:

/**
  * Default constructor
  */
  LDRbool () : val(false) {}

/**
  *  Constructor with the following arguments:
  * - flag:          Initial value for the Boolean value
  * - name:          The label of the parameter
  */
  LDRbool(bool flag, const STD_string& name="");

/**
  * Copy constructor
  */
  LDRbool(const LDRbool& jb) {LDRbool::operator = (jb);}

/**
  * Assigns the value of flag to the parameter
  */
  LDRbool& operator = (bool flag) {val=flag; return *this;}

/**
  * Assigns the value of s to the parameter, s may contain "yes" or "true"
  * (upper- or lowercase) to indicate a value of true, otherwise false
  */
  LDRbool& operator = (const STD_string& s) {parsevalstring(s); return *this;}

/**
  * Copy assignment
  */
  LDRbool& operator = (const LDRbool& jb);

/**
  * type conversion operator of the current value of the parameter
  */
  operator bool () const {return val;}

  // overwriting virtual functions from LDRbase
  bool parsevalstring(const STD_string& parstring, const LDRserBase* ser=0);
  STD_string printvalstring(const LDRserBase* ser=0) const;
  STD_string get_typeInfo(bool parx_equivtype=false) const {if(parx_equivtype) return "YesNo"; return "bool";}
  LDRbase* create_copy() const {return new LDRbool(*this);}
  bool* cast(bool*) {return &val;}

 private:
  bool val;
};


//////////////////////////////////////////////////////////////////

/**
  *
  *  Labeled Data Record (LDR) class to represent enumerations
  */
class LDRenum : public virtual LDRbase  {

 public:

/**
  * Default constructor
  */
  LDRenum() {actual=entries.end();}

/**
  *  Constructor with the following arguments:
  * - first_entry:   Initial value for the first entry of the enumeration
  * - name:          The label of the parameter
  */
  LDRenum(const STD_string& first_entry, const STD_string& name="");

/**
  * Copy constructor
  */
  LDRenum(const LDRenum& je) {LDRenum::operator = (je);}


/**
  * Sets the current value of the enumeration to the given item
  */
  LDRenum& operator = (const char* item) {set_actual(STD_string(item)); return *this;}

/**
  * Sets the current value of the enumeration to the given item
  */
  LDRenum& operator = (const STD_string& item) {set_actual(item); return *this;}

/**
  * Sets the current value of the enumeration to the given item
  */
  LDRenum& operator = (int item) {set_actual(item); return *this;}

/**
  * Assignment operator
  */
  LDRenum& operator = (const LDRenum& je);

/**
  * Appends an item to the the list of items in the enumeration.
  * If index is non-negative, the item will be inserted at the
  * position indicated by index.
  */
  LDRenum& add_item(const STD_string& item, int index=-1);

/**
  * Sets the current value of the enumeration to the given item
  */
  LDRenum& set_actual(const STD_string& item);

/**
  * Sets the current value of the enumeration to the given item
  */
  LDRenum& set_actual(int index);

/**
  * Clears the list of items in the enumeration
  */
  LDRenum& clear();

/**
  * type conversion operator of the current value of the enumeration
  */
  operator int () const;

/**
  * type conversion operator of the current value of the enumeration
  */
  operator STD_string () const;

/**
  * Compares the current value with s for equality
  */
  bool operator == (const STD_string& s) const {return (operator STD_string ())==s;}

/**
  * Compares the current value with s for equality
  */
  bool operator == (const char* s) const {return (operator STD_string ())==STD_string(s);}

/**
  * Compares the current index with i for equality
  */
  bool operator == (int i) const {return (operator int ())==i;}

/**
  * Compares the current value with s for inequality
  */
  bool operator != (const STD_string& s) const {return (operator STD_string ())!=s;}

/**
  * Compares the current value with s for inequality
  */
  bool operator != (const char* s) const {return (operator STD_string ())!=STD_string(s);}

/**
  * Compares the current index with i for inequality
  */
  bool operator != (int i) const {return (operator int ())!=i;}


/**
  * Returns the number of items in the enumeration
  */
  unsigned int n_items() const {return entries.size();}

/**
  * Returns the item at the position 'index' in the label-index map
  */
  const STD_string& get_item(unsigned int index) const;

/**
  * Returns the current position in the label-index map
  */
  unsigned int get_item_index() const;

/**
  * Sets the current position 'index' in the label-index map
  */
  LDRenum& set_item_index(unsigned int index);


  // overwriting virtual functions from LDRbase
  bool parsevalstring(const STD_string& parstring, const LDRserBase* ser=0);
  STD_string printvalstring(const LDRserBase* ser=0) const;
  svector get_alternatives() const;
  STD_string get_typeInfo(bool parx_equivtype=false) const {if(parx_equivtype) toupperstr(get_label()); return "enum";}
  LDRbase* create_copy() const {return new LDRenum(*this);}
  LDRenum* cast(LDRenum*) {return this;}

 private:
  STD_map<int,STD_string> entries;
  STD_map<int,STD_string>::const_iterator actual;

  STD_string parxtype_cache;

};

//////////////////////////////////////////////////////////////////

/**
  *
  *  Labeled Data Record (LDR) class to trigger actions from the GUI
  */
class LDRaction : public virtual LDRbase  {

 public:

/**
  * Default constructor
  */
  LDRaction() : state(false) {set_filemode(exclude);}

/**
  *  Constructor with the following arguments:
  * - init_state:    Initial value for the action
  * - name:          The label of the parameter
  */
  LDRaction(bool init_state, const STD_string& name="");

/**
  * Copy constructor
  */
  LDRaction(const LDRaction& ja) {LDRaction::operator = (ja);}

/**
  * Copy assignment
  */
  LDRaction& operator = (const LDRaction& ja);

/**
  * Returns whether an action should be triggered and resets the action flag
  */
  operator bool () const;

/**
  * After calling this function, the next type conversion to bool will return true
  */
  LDRaction& trigger_action() {state=true; return *this;}
  
  // overwriting virtual functions from LDRbase
  bool parsevalstring(const STD_string& parstring, const LDRserBase* ser=0);
  STD_string printvalstring(const LDRserBase* ser=0) const;
  STD_string get_typeInfo(bool parx_equivtype=false) const {return "action";}
  LDRbase* create_copy() const {return new LDRaction(*this);}
  LDRaction* cast(LDRaction*) {return this;}

 private:
  mutable bool state;
};




//////////////////////////////////////////////////////////////////


/**
  *
  * Labeled Data Record (LDR) class for representing file names. Besides using LDRfileName
  * as a convenient way within the UI to display/retrieve/browse file names,
  * it can be used to analyze file paths, for example in the following way:
  *   \verbatim
  STD_string myfile="/somedir/anotherdir/file.txt";

  STD_string myfile_base=LDRfileName(myfile).get_basename();
  STD_cout << "myfile_base=" << myfile_base << STD_endl;

  STD_string myfile_base_nosuffix=LDRfileName(myfile).get_basename_nosuffix();
  STD_cout << "myfile_base_nosuffix=" << myfile_base_nosuffix << STD_endl;

  STD_string myfile_dir= LDRfileName(myfile).get_dirname();
  STD_cout << "myfile_dir=" << myfile_dir << STD_endl;
      \endverbatim
  *
  */
class LDRfileName : public LDRstring {

public:

/**
  * Default constructor
  */
  LDRfileName () {common_init();}

/**
  *  Constructor with the following arguments:
  * - filename:      Initial value for the file name
  * - name:          The label of the parameter
  */
  LDRfileName (const STD_string& filename, const STD_string& name="");

/**
  * Copy constructor
  */
  LDRfileName(const LDRfileName& jf);

/**
  * Assignment from a string
  */
  LDRfileName& operator = (const STD_string& filename);

/**
  * Assignment operator
  */
  LDRfileName& operator = (const LDRfileName& jf);

/**
  * Returns 'true' only if the file/directory exists
  */
  bool exists() const;

/**
  * Returns the filename without preceeding directories
  */
  STD_string get_basename() const {return basename_cache;}

/**
  * Returns the filename without preceeding directories and without
  * a previously specified suffix (file extension)
  */
  STD_string get_basename_nosuffix() const;

/**
  * Returns the directory of the filename
  */
  STD_string get_dirname() const {return dirname_cache;}

/**
  * Returns the suffix (file extension) of the filename
  */
  STD_string get_suffix() const {return suffix_cache;}

/**
  * Sets/overwrites the suffix (file extension) of the filename
  */
  LDRfileName& set_suffix(const STD_string& suff) {suffix_cache=suff; return *this;}

/**
  * Returns the default location of the filename
  */
  STD_string get_defaultdir() const {return defaultdir;}

/**
  * Sets the default location of the filename
  */
  LDRfileName& set_defaultdir(const STD_string& defdir);


/**
  * Returns whether used exclusively for directory names
  */
  bool is_dir() const {return dir;}

/**
  * Specifies whether used exclusively for directory names
  */
  LDRfileName& set_dir(bool flag) {dir=flag; return *this;}


  // overwriting virtual functions from LDRbase
  bool parsevalstring(const STD_string& parstring, const LDRserBase* ser=0);
  STD_string get_typeInfo(bool parx_equivtype=false) const {return "fileName";}
  LDRbase* create_copy() const {return new LDRfileName(*this);}
  LDRfileName* cast(LDRfileName*) {return this;}

private:

  static void normalize(const STD_string& fname, bool dir, STD_string& result, STD_string& result_dirname, STD_string& result_basename, STD_string& result_suffix);

  void common_init() {dir=false;}

  STD_string defaultdir;
  STD_string dirname_cache;
  STD_string basename_cache;
  STD_string suffix_cache;

  bool dir;

};

//////////////////////////////////////////////////////////////////

/**
  *
  *  Labeled Data Record (LDR) class for representing mathematic formulas
  */
class LDRformula : public LDRstring {

public:

/**
  * Default constructor
  */
  LDRformula () : LDRstring() {}

/**
  *  Constructor with the following arguments:
  * - formula:      Initial value for the formula
  * - name:         The label of the parameter
  */
  LDRformula (const STD_string& formula, const STD_string& name="");

/**
  * Copy constructor
  */
  LDRformula (const LDRformula& jf) {LDRformula::operator = (jf);}

/**
  * Assignment from a formula string
  */
  LDRformula& operator = (const STD_string& formula) {LDRstring::operator = (formula); return *this;}

/**
  * Copy assignment
  */
  LDRformula& operator = (const LDRformula& jf);

/**
  * Sets a string describing the formulas syntax
  */
  LDRformula& set_syntax(const STD_string& syn) {syntax=syn; return *this;}

/**
  * Returns a string describing the formulas syntax
  */
  STD_string get_syntax() const {return syntax;}

  // overwriting virtual functions from LDRbase
  STD_string get_typeInfo(bool parx_equivtype=false) const {return "formula";}
  LDRbase* create_copy() const {return new LDRformula(*this);}
  LDRformula* cast(LDRformula*) {return this;}

private:
  STD_string syntax;
};



/** @}
  */

#endif

