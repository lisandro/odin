/***************************************************************************
                          ldrblock.h  -  description
                             -------------------
    begin                : Sun Jun 6 2004
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef LDRBLOCK_H
#define LDRBLOCK_H

#include <tjutils/tjstatic.h>
#include <tjutils/tjlist.h>

#include <odinpara/ldrbase.h>


/**
  * @addtogroup ldr
  * @{
  */


typedef List<LDRbase,LDRbase*,LDRbase&> LDRlist; // alias


/**
  *
  *  This class represents a block of parameters. Parameters can be
  *  added or removed from the list of parameters within the block.
  *  The whole block may be written to or read from a file.
  */
class LDRblock : public virtual LDRbase, public LDRlist, public StaticHandler<LDRblock> {

 public:

/**
  *
  *  Constructs an empty block of parameters with the label 'title'
  */
  LDRblock(const STD_string& title="Parameter List");


/**
  *
  *  Copy constructor
  */
  LDRblock(const LDRblock& block);


/**
  *
  *  Destructor
  */
  ~LDRblock();


/**
  *
  *  Assignment operator
  */
  LDRblock& operator = (const LDRblock& block);

/**
  *
  *  Merges all parameters found in the specified 'block' into this block
  */
  LDRblock& merge(LDRblock& block, bool onlyUserPars=true);

/**
  *
  *  Removes all parameters found in the specified 'block' from this block
  */
  LDRblock& unmerge(LDRblock& block);


/**
  *
  *  Print the value of the parameter that has the label 'parameterName' as a formated string.
  *  If applicable, append the unit of the number in case 'append_unit' is set to 'true'.
  */
  STD_string printval(const STD_string& parameterName, bool append_unit=false) const;

/**
  *
  * Set the value of the parameter that has the label 'parameterName' to 'value'.
  * Returns 'true' if sucessful.
  */
  bool parseval(const STD_string& parameterName, const STD_string& value);


/**
  *
  *  Parse values of this block from the string 'src' using format 'serializer',
  * returns the number of parameters sucessfully parsed.
  */
  int parseblock(const STD_string& source, const LDRserBase& serializer=LDRserJDX());

/**
  *
  *  Returns the current number of parameters in the block
  */
  unsigned int numof_pars() const;


/**
  * Returns a pointer to parameter 'label', or zero if not found
  */
  LDRbase* get_parameter(const STD_string& ldrlabel);


/**
  *
  *  Returns true if a parameter with the given label already exists
  *  in the block
  */
  bool parameter_exists(const STD_string& ldrlabel) const;

/**
  *
  *  Prefixes all parameters labels with the given string. If the prefix
  *  is already at the beginning of the label, it is not added again.
  */
  LDRblock& set_prefix(const STD_string& prefix);

/**
  *
  *  If embedded is true, the block's widget will be displayed in a subwidget
  *  otherwise an 'Edit' button that will open a subdialog is generated
  */
  LDRblock& set_embedded(bool embedded) {embed=embedded; return *this;}

/**
  *
  *  Returns whether the block's widget will be displayed in a subwidget
  *  or an 'Edit' button that will open a subdialog is generated
  */
  bool is_embedded() const {return embed;}




/**
  * Returns the i'th parameter in the block
  */
  LDRbase& operator [] (unsigned int i);

/**
  * Returns the const i'th parameter in the block
  */
  const LDRbase& operator [] (unsigned int i) const;

/**
  * Returns the first parameter in the block with the given 'id'
  */
  LDRbase& get_parameter_by_id(int id);


/**
  * Makes this become a copy of 'src', including its parameters by creating temporary objects.
  * These temporary objects will be deleted automatically upon destruction.
  */
  LDRblock& create_copy(const LDRblock& src);

/**
  * Appends a deep copy of src (not a reference).
  * This copy will be deleted automatically upon destruction.
  */
  LDRblock& append_copy(const LDRbase& src);

/**
  * Copy parameter values from 'src' to the equivalent parameters in this block
  */
  LDRblock& copy_ldr_vals(const LDRblock& src);


/**
  * Compares 'rhs' with 'this' with respect to whether all parameters with
  * the same label mutually exist in the other block
  * and whether these parameters have the same type, label and value.
  */
  bool operator == (const LDRblock& rhs) const {return !( ((*this)<rhs) || ((rhs<(*this))) );}



/**
  * Comparison operator which
  * - (1st) compares number of parameters
  * - (2nd) the type of parameters existing in both blocks
  * - (3rd) the value of parameters existing in both blocks
  */
  bool operator < (const LDRblock& rhs) const {return compare(rhs);}


/**
  * Equivalent to operator <. In addition, a list of parameters
  * can be given in 'exclude' which are excluded from the comparison.
  * When comparing float or double numbers, parametrs are considered equal
  * if they differe by no more than the given 'accuracy'.
  */
  bool compare(const LDRblock& rhs, const STD_list<STD_string>* exclude=0, double accuracy=0.0) const;


#ifndef NO_CMDLINE
  LDRblock& parse_cmdline_options(int argc, char *argv[], bool modify=true);
  STD_map<STD_string,STD_string> get_cmdline_options() const;
  STD_string get_cmdline_usage(const STD_string& lineprefix="") const;
#endif


  // functions for StaticHandler
  static void init_static();
  static void destroy_static();

  // overwriting virtual functions from LDRbase
  STD_string print(const LDRserBase& serializer=LDRserJDX()) const;
  LDRbase& set_parmode(parameterMode parameter_mode);
  LDRbase& set_filemode(fileMode file_mode);
  STD_string get_parx_code(parxCodeType type) const;
  STD_ostream& print2stream(STD_ostream& os, const LDRserBase& serializer) const;
  bool parsevalstring(const STD_string&, const LDRserBase* ser=0) {return true;}
  bool parse(STD_string& parstring, const LDRserBase& serializer);
  STD_string printvalstring(const LDRserBase* ser=0) const {return "";}
  LDRbase* create_copy() const {LDRblock* result=new LDRblock; result->create_copy(*this); return result;}
  STD_string get_typeInfo(bool parx_equivtype=false) const {return "LDRblock";}
  LDRblock* cast(LDRblock*) {return this;}
  int load(const STD_string &filename, const LDRserBase& serializer=LDRserJDX());
  int write(const STD_string &filename, const LDRserBase& serializer=LDRserJDX()) const;

 protected:

/**
  *
  *  Use this functions to append members of derived classes
  */
  LDRblock&  append_member(LDRbase& ldr,const STD_string ldrlabel="");




 private:
  friend class LDRwidget;


  LDRlist::constiter ldr_exists(const STD_string& label) const;
  int parse_ldr_list(STD_string& parstring, const LDRserBase& serializer);

  STD_list<LDRbase*>* garbage;
  bool embed;

};



/** @}
  */



#endif


