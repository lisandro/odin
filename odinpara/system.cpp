#include "system.h"

#include <tjutils/tjhandler_code.h>

Nuclei::Nuclei() {

//  nuclist.push_back(Nucleus("1n", 183.247215));
//  nuclist.push_back(Nucleus("1H", 267.522322));
  nuclist.push_back(Nucleus("1H", 267.50998));
  nuclist.push_back(Nucleus("1Hb",267.50998));
  nuclist.push_back(Nucleus("2H", 41.066271));
  nuclist.push_back(Nucleus("3H", 285.349604));
  nuclist.push_back(Nucleus("3He", 203.801399));
  nuclist.push_back(Nucleus("6Li", 39.371067));
  nuclist.push_back(Nucleus("7Li", 103.976035));
  nuclist.push_back(Nucleus("9Be", 37.599838));
  nuclist.push_back(Nucleus("10B", 28.746829));
  nuclist.push_back(Nucleus("11B", 85.847161));
  nuclist.push_back(Nucleus("13C", 67.282862));
  nuclist.push_back(Nucleus("14N", 19.337759));
  nuclist.push_back(Nucleus("15N", 27.126396));
//  nuclist.push_back(Nucleus("17O", 36.280369));
  nuclist.push_back(Nucleus("17O",36.265437));
  nuclist.push_back(Nucleus("19F", 251.814987));
  nuclist.push_back(Nucleus("21Ne", 21.130981));
  nuclist.push_back(Nucleus("23Na", 70.803959));
  nuclist.push_back(Nucleus("25Mg", 16.388432));
  nuclist.push_back(Nucleus("27Al", 69.762835));
  nuclist.push_back(Nucleus("29Si", 53.190305));
//  nuclist.push_back(Nucleus("31P", 108.394371));
  nuclist.push_back(Nucleus("31P",108.28945));
  nuclist.push_back(Nucleus("33S", 20.556697));
  nuclist.push_back(Nucleus("35Cl", 26.241723));
  nuclist.push_back(Nucleus("37Cl", 21.843494));
  nuclist.push_back(Nucleus("37Ar", 36.561855));
  nuclist.push_back(Nucleus("39Ar", 21.739821));
  nuclist.push_back(Nucleus("39K", 12.499141));
  nuclist.push_back(Nucleus("40K", 15.542715));
  nuclist.push_back(Nucleus("41K", 6.860610));
  nuclist.push_back(Nucleus("43Ca", 18.025202));
  nuclist.push_back(Nucleus("45Sc", 65.088145));
  nuclist.push_back(Nucleus("47Ti", 15.105406));
  nuclist.push_back(Nucleus("49Ti", 15.109804));
  nuclist.push_back(Nucleus("50V", 26.706679));
  nuclist.push_back(Nucleus("51V", 70.455242));
  nuclist.push_back(Nucleus("53Cr", 15.151901));
  nuclist.push_back(Nucleus("55Mn", 66.452853));
  nuclist.push_back(Nucleus("57Fe", 8.680849));
  nuclist.push_back(Nucleus("59Co", 63.315658));
  nuclist.push_back(Nucleus("61Ni", 23.947732));
  nuclist.push_back(Nucleus("63Cu", 70.988684));
  nuclist.push_back(Nucleus("65Cu", 76.045392));
  nuclist.push_back(Nucleus("67Zn", 16.772335));
  nuclist.push_back(Nucleus("69Ga", 64.388826));
  nuclist.push_back(Nucleus("71Ga", 81.812099));
  nuclist.push_back(Nucleus("73Ge", 9.360061));
  nuclist.push_back(Nucleus("75As", 45.961501));
  nuclist.push_back(Nucleus("77Se", 51.252571));
  nuclist.push_back(Nucleus("79Br", 67.256472));
  nuclist.push_back(Nucleus("81Br", 72.497905));
  nuclist.push_back(Nucleus("83Kr", 10.330813));
  nuclist.push_back(Nucleus("85Rb", 25.920653));
  nuclist.push_back(Nucleus("87Rb", 87.845842));
  nuclist.push_back(Nucleus("87Sr", 11.639601));
  nuclist.push_back(Nucleus("89Y", 13.162645));
  nuclist.push_back(Nucleus("91Zr", 24.974405));
  nuclist.push_back(Nucleus("93Nb", 65.673738));
  nuclist.push_back(Nucleus("95Mo", 17.513751));
  nuclist.push_back(Nucleus("97Mo", 17.883830));
  nuclist.push_back(Nucleus("99Tc", 60.503305));
  nuclist.push_back(Nucleus("99Ru", 12.285512));
  nuclist.push_back(Nucleus("101Ru", 13.770229));
  nuclist.push_back(Nucleus("103Rh", 8.467849));
  nuclist.push_back(Nucleus("105Pd", 12.296194));
  nuclist.push_back(Nucleus("107Ag", 10.889388));
  nuclist.push_back(Nucleus("109Ag", 12.518618));
  nuclist.push_back(Nucleus("111Cd", 56.983464));
  nuclist.push_back(Nucleus("113Cd", 59.609207));
  nuclist.push_back(Nucleus("113In", 58.845172));
  nuclist.push_back(Nucleus("115In", 58.971464));
  nuclist.push_back(Nucleus("115Sn", 88.012975));
  nuclist.push_back(Nucleus("117Sn", 95.887691));
  nuclist.push_back(Nucleus("119Sn", 100.317337));
  nuclist.push_back(Nucleus("121Sb", 64.434694));
  nuclist.push_back(Nucleus("123Sb", 34.891785));
  nuclist.push_back(Nucleus("123Te", 70.590959));
  nuclist.push_back(Nucleus("125Te", 85.108258));
  nuclist.push_back(Nucleus("127I", 53.895907));
  nuclist.push_back(Nucleus("129Xe", 74.521091));
  nuclist.push_back(Nucleus("131Xe", 22.091051));
  nuclist.push_back(Nucleus("133Cs", 35.332864));
  nuclist.push_back(Nucleus("135Ba", 26.755060));
  nuclist.push_back(Nucleus("137Ba", 29.929325));
  nuclist.push_back(Nucleus("138La", 35.572254));
  nuclist.push_back(Nucleus("139La", 38.083643));
  nuclist.push_back(Nucleus("137Ce", 30.661944));
  nuclist.push_back(Nucleus("139Ce", 33.866369));
  nuclist.push_back(Nucleus("141Ce", 14.891149));
  nuclist.push_back(Nucleus("141Pr", 81.906975));
  nuclist.push_back(Nucleus("143Nd", 14.570707));
  nuclist.push_back(Nucleus("145Nd", 8.978672));
  nuclist.push_back(Nucleus("143Pm", 72.822118));
  nuclist.push_back(Nucleus("147Pm", 35.311501));
  nuclist.push_back(Nucleus("147Sm", 11.151397));
  nuclist.push_back(Nucleus("149Sm", 9.192928));
  nuclist.push_back(Nucleus("151Eu", 66.511286));
  nuclist.push_back(Nucleus("153Eu", 29.370750));
  nuclist.push_back(Nucleus("155Gd", 8.243539));
  nuclist.push_back(Nucleus("157Gd", 10.807079));
  nuclist.push_back(Nucleus("159Tb", 64.276986));
  nuclist.push_back(Nucleus("161Dy", 9.207380));
  nuclist.push_back(Nucleus("163Dy", 12.885556));
  nuclist.push_back(Nucleus("165Ho", 57.103473));
  nuclist.push_back(Nucleus("167Er", 7.716380));
  nuclist.push_back(Nucleus("169Tm", 22.185927));
  nuclist.push_back(Nucleus("171Yb", 47.287881));
  nuclist.push_back(Nucleus("173Yb", 13.025043));
  nuclist.push_back(Nucleus("175Lu", 30.552617));
  nuclist.push_back(Nucleus("176Lu", 21.683272));
  nuclist.push_back(Nucleus("177Hf", 10.858601));
  nuclist.push_back(Nucleus("179Hf", 6.821026));
  nuclist.push_back(Nucleus("180Ta", 25.679378));
  nuclist.push_back(Nucleus("181Ta", 32.438201));
  nuclist.push_back(Nucleus("183W", 11.282716));
  nuclist.push_back(Nucleus("185Re", 61.057482));
  nuclist.push_back(Nucleus("187Re", 61.682030));
  nuclist.push_back(Nucleus("187Os", 6.192707));
  nuclist.push_back(Nucleus("189Os", 21.071290));
  nuclist.push_back(Nucleus("191Ir", 4.811663));
  nuclist.push_back(Nucleus("191Ir", 5.226982));
  nuclist.push_back(Nucleus("195Pt", 58.384615));
  nuclist.push_back(Nucleus("197Au", 4.653327));
  nuclist.push_back(Nucleus("199Hg", 48.457810));
  nuclist.push_back(Nucleus("201Hg", 17.887600));
  nuclist.push_back(Nucleus("203Tl", 155.393226));
  nuclist.push_back(Nucleus("205Tl", 156.921925));
  nuclist.push_back(Nucleus("207Pb", 56.762296));
  nuclist.push_back(Nucleus("209Bi", 43.749819));
  nuclist.push_back(Nucleus("209Po", 73.513268));
  nuclist.push_back(Nucleus("211Rn", 57.553977));
  nuclist.push_back(Nucleus("223Fr", 37.384953));
  nuclist.push_back(Nucleus("223Ra", 8.636867));
  nuclist.push_back(Nucleus("225Ra", 70.289994));
  nuclist.push_back(Nucleus("227Ac", 35.185838));
  nuclist.push_back(Nucleus("229Th", 8.796459));
  nuclist.push_back(Nucleus("231Pa", 64.088490));
  nuclist.push_back(Nucleus("235U", 5.215044));
  nuclist.push_back(Nucleus("237Np", 60.130083));
  nuclist.push_back(Nucleus("239Pu", 19.415043));
  nuclist.push_back(Nucleus("243Am", 28.902652));
}


double Nuclei::get_gamma(const STD_string& nucName) const {
  NucList::const_iterator resultit=nuclist.begin();
  for(NucList::const_iterator it=nuclist.begin();it!=nuclist.end();++it) {
    if(it->first == nucName) resultit=it;
  }
  return resultit->second;
}


double Nuclei::get_nuc_freq(const STD_string& nucName,float B0) const {
  float freq;
  float b0=B0;
  if (b0==0.0) b0=-1.0;
  freq= ( get_gamma(nucName) *b0) / (2.0*PII);
  if(freq==0.0) freq=-1.0;
  return freq;
}


LDRenum Nuclei::get_nuc_enum() const {
  LDRenum result;
  for(STD_list<Nucleus>::const_iterator it=nuclist.begin();it!=nuclist.end();++it) {
    result.add_item(it->first);
  }
  return result;
}

/////////////////////////////////////////////////////////////////////////////


System::System(const STD_string& object_label) : LDRblock(object_label) {
  Log<Para> odinlog(this, "System(...)");


  platformstr.set_parmode(hidden).set_description("The current platform");

  main_nucleus=get_nuc_enum();
  main_nucleus.set_actual(0);
  main_nucleus.set_description("The main nucleus for transmit/receive");

  delay_rastertime=0.0;
  delay_rastertime.set_unit(ODIN_TIME_UNIT).set_description("Delay duration must be multiple of this interval");
  grad_rastertime=0.0;
  grad_rastertime.set_unit(ODIN_TIME_UNIT).set_description("Gradient duration must be multiple of this interval");
  min_grad_rastertime=0.005;
  min_grad_rastertime.set_unit(ODIN_TIME_UNIT).set_description("Minimum gradient raster time possible");
  rf_rastertime=0.0;
  rf_rastertime.set_unit(ODIN_TIME_UNIT).set_description("RF pulse duration must be multiple of this interval");
  acq_rastertime=0.0;
  acq_rastertime.set_unit(ODIN_TIME_UNIT).set_description("Acquisition duration must be multiple of this interval");

  max_rf_samples=3000;
  max_rf_samples.set_description("Maximum number of points in RF waveform");

  max_grad_samples=-1;
  max_grad_samples.set_description("Maximum number of points in gradient waveform");

  reference_gain=20.0;
  reference_gain.set_unit("dB").set_description("RF reference gain");

  transmit_coil_name="Unknown";
  transmit_coil_name.set_cmdline_option("tcname").set_description("Name of transmit coil");

  receive_coil_name="Unknown";
  receive_coil_name.set_cmdline_option("rcname").set_description("Name of receive coil");

  inter_grad_delay=0.0;
  inter_grad_delay.set_unit(ODIN_TIME_UNIT).set_description("Minimum delay between gradient objects");

  max_grad=_DEFAULT_MAX_SYSTEM_GRADIENT_;
  max_grad.set_unit(ODIN_GRAD_UNIT).set_description("Maximum gradient strength");

  max_slew_rate=_DEFAULT_MAX_SYSTEM_SLEWRATE_;
  max_slew_rate.set_unit(STD_string(ODIN_GRAD_UNIT)+"/"+ODIN_TIME_UNIT).set_description("Maximum gradient slew rate");

  grad_shift=0.0;
  grad_shift.set_unit(ODIN_TIME_UNIT).set_description("Latency difference between RF/acquistion and gradient channels");

  B0=_DEFAULT_B0_;
  B0.set_unit(ODIN_FIELD_UNIT).set_description("Main field strength");

  datatype=TypeTraits::type2label(float(0));
  datatype.set_description("Digital representation of raw data");

  grad_reson_center.set_unit(ODIN_FREQ_UNIT).set_description("Center of gradient resonance frequencies");
  grad_reson_width.set_unit(ODIN_FREQ_UNIT).set_description("Width of gradient resonance frequencies");

  min_duration.resize(endObj);
  min_duration=0.0;

  System::append_all_members();
}


System& System::operator = (const System& s) {
  LDRblock::operator = (s);

  System::append_all_members();

  // copy only visible members of LDRblock
  copy_ldr_vals(s);


  // preserve parmode and filemode which varies across platforms
  grad_shift         .set_filemode(s.grad_shift         .get_filemode()).set_parmode(s.grad_shift         .get_parmode());
  inter_grad_delay   .set_filemode(s.inter_grad_delay   .get_filemode()).set_parmode(s.inter_grad_delay   .get_parmode());
  reference_gain     .set_filemode(s.reference_gain     .get_filemode()).set_parmode(s.reference_gain     .get_parmode());
  transmit_coil_name .set_filemode(s.transmit_coil_name .get_filemode()).set_parmode(s.transmit_coil_name .get_parmode());
  receive_coil_name  .set_filemode(s.receive_coil_name  .get_filemode()).set_parmode(s.receive_coil_name  .get_parmode());
  delay_rastertime   .set_filemode(s.delay_rastertime   .get_filemode()).set_parmode(s.delay_rastertime   .get_parmode());
  rf_rastertime      .set_filemode(s.rf_rastertime      .get_filemode()).set_parmode(s.rf_rastertime      .get_parmode());
  grad_rastertime    .set_filemode(s.grad_rastertime    .get_filemode()).set_parmode(s.grad_rastertime    .get_parmode());
  min_grad_rastertime.set_filemode(s.min_grad_rastertime.get_filemode()).set_parmode(s.min_grad_rastertime.get_parmode());
  acq_rastertime     .set_filemode(s.acq_rastertime     .get_filemode()).set_parmode(s.acq_rastertime     .get_parmode());
  max_rf_samples     .set_filemode(s.max_rf_samples     .get_filemode()).set_parmode(s.max_rf_samples     .get_parmode());
  max_grad_samples   .set_filemode(s.max_grad_samples   .get_filemode()).set_parmode(s.max_grad_samples   .get_parmode());
  datatype           .set_filemode(s.datatype           .get_filemode()).set_parmode(s.datatype           .get_parmode());
  grad_reson_center  .set_filemode(s.grad_reson_center  .get_filemode()).set_parmode(s.grad_reson_center  .get_parmode());
  grad_reson_width   .set_filemode(s.grad_reson_width   .get_filemode()).set_parmode(s.grad_reson_width   .get_parmode());




  return *this;
}


odinPlatform System::get_platform() const {
  return SystemInterface::get_current_pf();
}



System& System::set_main_nucleus(const STD_string& nucname) {
  main_nucleus.set_actual(nucname);
  return *this;
}

System& System::set_transmit_coil_name(const STD_string& tcname) {
  transmit_coil_name=tcname;
  return *this;
}

System& System::set_receive_coil_name(const STD_string& rcname) {
  receive_coil_name=rcname;
  return *this;
}

float System::get_grad_switch_time(float graddiff) const {
  return ( secureDivision(fabs(graddiff),max_slew_rate) + get_inter_grad_delay() );
}

System& System::set_reference_gain(float refgain) {
  reference_gain=refgain;
  return *this;
}


System& System::set_B0_from_freq(double freq, const STD_string& nucName) {
  B0=2.0*PII*secureDivision(freq,get_gamma(nucName));
  return *this;
}

double System::get_gamma(const STD_string& nucName) const {
  return nuc.get_gamma(nucName);
}


System& System::set_scandir(const STD_string& dir) {
  Log<Para> odinlog(this,"set_scandir");
#ifndef NO_FILEHANDLING
  if(!checkdir(dir.c_str())) {
    ODINLOG(odinlog,errorLog) << "scan directory " << dir << " does not exist" << STD_endl;
  }
#endif
  scandir=dir;
  return *this;
}

STD_string System::get_scandir() const {return scandir+"/";}


double System::get_nuc_freq(const STD_string& nucName) const {
  return nuc.get_nuc_freq(nucName,B0);
}


LDRenum System::get_nuc_enum() const {return nuc.get_nuc_enum();}


double System::get_rastertime(objCategory cat) const {
  if(cat==pulsObj)  return rf_rastertime;
  if(cat==gradObj)  return grad_rastertime;
  if(cat==delayObj) return delay_rastertime;
  if(cat==acqObj)   return acq_rastertime;
  if(cat==freqObj)  return rf_rastertime;
  if(cat==syncObj)  return grad_rastertime;
  return 0.0;
}


double System::get_rasteredtime(objCategory cat, double time) const {
  double rastime=get_rastertime(cat);

  if(rastime) {
    int eventtime_round=int(time/rastime+0.5);
    return double(eventtime_round)*rastime;
  } else return time;
}


bool System::allowed_grad_freq(double freq, double& low, double& upp) const {
  low=upp=freq;
  for(unsigned int i=0; i<grad_reson_center.length(); i++) {
    double center=grad_reson_center[i];
    double width=grad_reson_width[i];
    double lowreson=center-0.5*width;
    double uppreson=center+0.5*width;

    if( lowreson<=freq && freq<=uppreson ) {
      low=lowreson;
      upp=uppreson;
      return false;
    }
  }
  return true;
}



int System::load(const STD_string &filename, const LDRserBase& serializer) {
  Log<Para> odinlog(this,"load");
  ODINLOG(odinlog,errorLog) << "called" << STD_endl;
  return -1;
}

int System::append_all_members() {
  append_member(platformstr,"Platform");
  append_member(main_nucleus,"MainNucleus");
  append_member(max_grad,"MaximumGradientStrength");
  append_member(max_slew_rate,"MaximumGradientSlewRate");
  append_member(grad_shift,"GradientChannelShiftDelay");
  append_member(B0,"MagneticFieldStrength");
  append_member(reference_gain,"ReferenceGain");
  append_member(transmit_coil_name,"TransmitCoilName");
  append_member(receive_coil_name,"ReceiveCoilName");
  append_member(inter_grad_delay,"InterGradientSwitchingDelay");
  append_member(delay_rastertime,"DelayRasterTime");
  append_member(grad_rastertime,"GradientRasterTime");
  append_member(min_grad_rastertime,"MinGradientRasterTime");
  append_member(rf_rastertime,"RFRasterTime");
  append_member(acq_rastertime,"AcquisitionRasterTime");
  append_member(max_rf_samples,"MaxNumOfSamplesPerRF");
  append_member(max_grad_samples,"MaxNumOfSamplesPerGradWave");
  append_member(grad_reson_center,"GradResonCenter");
  append_member(grad_reson_width,"GradResonWidth");
  append_member(datatype,"Datatype");
  return 0;
}


/////////////////////////////////////////////////////////////////////////////

System* SystemInterface::get_sysinfo_ptr() {
  int pfint=current_pf->operator int ();
  return systemInfo_platform[pfint].unlocked_ptr();
}


const System* SystemInterface::get_const_sysinfo_ptr() {
  int pfint=current_pf->operator int ();
  const SingletonHandler<System,false>& shs=systemInfo_platform[pfint];
  return shs.operator -> ();
//  return &(const System&)(*shs);
}


void SystemInterface::init_static() {
  current_pf.init("current_pf");
  systemInfo_platform= new SingletonHandler<System,false>[numof_platforms];
  for(unsigned int i=0; i<numof_platforms; i++) {
    systemInfo_platform[i].init(("systemInfo"+itos(i)).c_str()); // unique label for each singleton
    systemInfo_platform[i]->set_label("systemInfo");
  }
}


void SystemInterface::destroy_static() {
  for(unsigned int i=0; i<numof_platforms; i++) {
    systemInfo_platform[i].destroy();
  }
  delete[] systemInfo_platform;
  current_pf.destroy();
}


void SystemInterface::set_current_pf(odinPlatform pf) {
  Log<Para> odinlog("SystemInterface","set_current_pf");
  ODINLOG(odinlog,normalDebug) << "pf=" << pf << STD_endl;
  if(current_pf) current_pf->operator = (pf);
  else ODINLOG(odinlog,errorLog) << "current_pf not yet initialized" << STD_endl;
}


template class SingletonHandler<LDRint,false>;
SingletonHandler<LDRint,false> SystemInterface::current_pf;

template class SingletonHandler<System,false>;
SingletonHandler<System,false>* SystemInterface::systemInfo_platform=0;

EMPTY_TEMPL_LIST bool StaticHandler<SystemInterface>::staticdone=false;
