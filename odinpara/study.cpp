#include "study.h"

#include <odinpara/odinpara.h>

// for current date/time
#ifdef HAVE_TIME_H
#include <time.h>
#endif


void Study::format_date(STD_string& result, const STD_string& date) {
  Log<Para> odinlog("Study","format_date");

  if(date.length()!=ODIN_DATE_LENGTH) {
    ODINLOG(odinlog,warningLog) << "Wrong length of date string >" << date << "<" << STD_endl;
    return;
  }

  // Parsing date/time manually since strptime is not available on all platforms (MinGW)
  result=itos(atoi(date.substr(0,4).c_str()),9999) + itos(atoi(date.substr(4,2).c_str()),12) + itos(atoi(date.substr(6,2).c_str()),31);
}


void Study::format_time(STD_string& result, const STD_string& time) {
  Log<Para> odinlog("Study","format_time");


  if(time.length()!=ODIN_TIME_LENGTH) {
    ODINLOG(odinlog,warningLog) << "Wrong length of time string >" << time << "<" << STD_endl;
    return;
  }

  // Parsing date/time manually since strptime is not available on all platforms (MinGW)
  result=itos(atoi(time.substr(0,2).c_str()),24) + itos(atoi(time.substr(2,2).c_str()),60) + itos(atoi(time.substr(4,2).c_str()),60);
}




Study::Study(const STD_string& label) : LDRblock(label) {

  set_timestamp(); // set current date/time
  ScanDate.set_cmdline_option("date").set_unit("yyyymmdd").set_description("Date of scan");
  ScanTime.set_cmdline_option("time").set_unit("hhmmss").set_description("Time of scan");

  PatientId="Unknown";
  PatientId.set_cmdline_option("pid").set_description("Unique patient identifier");

  PatientName="Unknown";
  PatientName.set_cmdline_option("pname").set_description("Full patient name");

  PatientBirthDate=STD_string(ODIN_DATE_LENGTH,'0');
  PatientBirthDate.set_cmdline_option("pbirth").set_unit("yyyymmdd").set_description("Patients date of birth");

  PatientSex.add_item("M");
  PatientSex.add_item("F");
  PatientSex.add_item("O");
  PatientSex.set_cmdline_option("psex").set_description("Patients sex");

  PatientWeight=50.0;
  PatientWeight.set_cmdline_option("pweight").set_unit("kg").set_description("Patients weight");

  PatientSize=2000.0;
  PatientSize.set_cmdline_option("psize").set_unit(ODIN_SPAT_UNIT).set_description("Patients size/height");

  Description="Unknown";
  Description.set_cmdline_option("stud").set_description("Study Description");

  ScientistName="Unknown";
  ScientistName.set_cmdline_option("scient").set_description("Scientist Name");

  SeriesDescription="Unknown";
  SeriesDescription.set_cmdline_option("serd").set_description("Series Description");

  SeriesNumber=1;
  SeriesNumber.set_cmdline_option("serno").set_description("Series Number");

  append_all_members();
}

Study::Study(const Study& s) {
  Study::operator = (s);
}


Study& Study::operator = (const Study& s) {
  LDRblock::operator = (s);
  append_all_members();

  // copy only visible members of LDRblock
  copy_ldr_vals(s);

  return *this;
}


Study& Study::set_DateTime(const STD_string& date, const STD_string& time) {
  format_date(ScanDate,date);
  format_time(ScanTime,time);
  return *this;
}

void Study::get_DateTime(STD_string& date, STD_string& time) const {
  date=ScanDate;
  time=ScanTime;
}

void Study::set_timestamp() {

  ScanDate=STD_string(ODIN_DATE_LENGTH,'0');
  ScanTime=STD_string(ODIN_TIME_LENGTH,'0');

#if defined(HAVE_TIME_H) && defined(HAVE_STRFTIME)
  time_t tt=time(NULL);

  char datebuff[ODIN_DATE_LENGTH+1];
  if(strftime(datebuff, ODIN_DATE_LENGTH+1, ODIN_DATE_FORMAT, localtime(&tt))) ScanDate=datebuff;

  char timebuff[ODIN_TIME_LENGTH+1];
  if(strftime(timebuff, ODIN_TIME_LENGTH+1, ODIN_TIME_FORMAT, localtime(&tt))) ScanTime=timebuff;
#endif
}



Study& Study::set_Patient(const STD_string& id, const STD_string& full_name, const STD_string& birth_date, char sex, float weight, float size) {
  PatientId=id;
  PatientName=full_name;
  format_date(PatientBirthDate,birth_date);
  PatientSex=STD_string(1,toupper(sex));
  PatientWeight=weight;
  PatientSize=size;
  return *this;
}

void Study::get_Patient(STD_string& id, STD_string& full_name, STD_string& birth_date, char& sex, float& weight, float& size) const {
  id=PatientId;
  full_name=PatientName;
  birth_date=PatientBirthDate;
  sex=PatientSex.LDRenum::operator STD_string()[0];
  weight=PatientWeight;
  size=PatientSize;
}

Study& Study::set_Context(const STD_string& description, const STD_string& scientist) {
  Description=description;
  ScientistName=scientist;
  return *this;
}


void Study::get_Context(STD_string& description, STD_string& scientist) const {
  description=Description;
  scientist=ScientistName;
}


Study& Study::set_Series(const STD_string& description, int number) {
  SeriesDescription=description;
  SeriesNumber=number;
  return *this;
}


void Study::get_Series(STD_string& description, int& number) const {
  description=SeriesDescription;
  number=SeriesNumber;
}


void Study::append_all_members() {
  LDRblock::clear();

  append_member(ScanDate,"ScanDate");
  append_member(ScanTime,"ScanTime");

  append_member(PatientId,"PatientId");
  append_member(PatientName,"PatientName");
  append_member(PatientBirthDate,"PatientBirthDate");
  append_member(PatientSex,"PatientSex");
  append_member(PatientWeight,"PatientWeight");
  append_member(PatientSize,"PatientSize");

  append_member(Description,"Description");
  append_member(ScientistName,"ScientistName");

  append_member(SeriesDescription,"SeriesDescription");
  append_member(SeriesNumber,"SeriesNumber");
}

