/***************************************************************************
                          ldrbase.h  -  description
                             -------------------
    begin                : Sun Jun 6 2004
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef LDRBASE_H
#define LDRBASE_H


#include <tjutils/tjutils.h>
#include <tjutils/tjlabel.h>
#include <tjutils/tjlist.h>
#include <tjutils/tjarray.h>

#include <odinpara/ldrser.h>




/**
  * @addtogroup ldr
  * @{
  */


/**
  * This enum determines whether the parameter can be seen/edited in the graphical user interface
  * - edit: Parameter is visible and can be edited
  * - noedit: Parameter is visible but cannot be edited
  * - hidden: Parameter is invisible
  */
enum parameterMode {edit,noedit,hidden};


/**
  * This enum determines whether the parameter will be included/compressed/excluded when reading/writing a file
  * - include: Parameter is included in the file
  * - compressed: Parameter is included in the file in compressed ASCII-format (e.g. Base64 encoding)
  *   if its representation exceeds a certain size
  * - exclude: Parameter is excluded from the file
  */
enum fileMode {include,compressed,exclude};



///////////////////////////////////////////////////////////////////////


class LDRblock; // Forward Declaration
class LDRfunction; // Forward Declaration
class LDRtriple; // Forward Declaration
class LDRaction;  // Forward Declaration
class LDRenum;  // Forward Declaration
class LDRfileName; // Forward Declaration
class LDRformula; // Forward Declaration
class LDRrecoIndices; // Forward Declaration


///////////////////////////////////////////////////////////////////////

/**
  *
  *  Helper class for debugging the LDR component
  */
class LDRcomp {
 public:
  static const char* get_compName();
};

///////////////////////////////////////////////////////////////////////

/**
  * Enum for plot/display scales/axes in the GUI
  * - displayScale: Scale for 2D/3D display
  * - xPlotScale:  x-axis (bottom)
  * - yPlotScaleLeft:  First y-axis (left)
  * - yPlotScaleRight:  Second y-axis (left)
  */
enum scaleType {displayScale=0,xPlotScale,yPlotScaleLeft,yPlotScaleRight,n_ScaleTypes};


/**
  * Data structure to store info about scales of LDRarrays in GUI
  */
struct ArrayScale {

  ArrayScale() : minval(0), maxval(0), enable(true) {}

  ArrayScale(const STD_string& scalelabel, const STD_string& scaleunit, float scalemin=0.0, float scalemax=0.0, bool enablescale=true);

  STD_string get_label_with_unit() const;

/**
  * Axis label
  */
  STD_string label;

/**
  * Axis physical unit
  */
  STD_string unit;

/**
  * Lower and upper bound of scale
  */
  float minval,maxval;

/**
  * Whether scale is enabled
  */
  bool enable;
};

///////////////////////////////////////////////////////////////////////

/**
  * Data structure to store info about 2D/3D plots of LDRarrays
  */
struct PixmapProps {

  PixmapProps() : minsize(128), maxsize(1024), autoscale(true), color(false), overlay_minval(0), overlay_maxval(0), overlay_firescale(false), overlay_rectsize(0.8f) {}


  void get_overlay_range(float& min, float& max) const;

/**
  * The min and max width/height in the GUI
  */
  unsigned int minsize, maxsize;

/**
  * Do auto-windowing/scaling of values
  */
  bool autoscale;

/**
  * Display values color encoded
  */
  bool color;


/**
  * Extra map to be overlaid on plot in color code
  */
  farray overlay_map;

/**
  * Range of displayed values of overlay map. If both are zero, autoscaling is applied.
  */
  float  overlay_minval, overlay_maxval;

/**
  * Use only yellow to red from color wheel
  */
  bool overlay_firescale;

/**
  * The relative size (between 0 and 1) of rectangles representing overlayed voxels
  */
  float overlay_rectsize;
};


///////////////////////////////////////////////////////////////////////

/**
  * Data structure to store info about GUI display of LDRarrays
  */
struct GuiProps {
  GuiProps() : fixedsize(true) {}

/**
  * The scales in 1D or 2D display
  */
  ArrayScale scale[n_ScaleTypes];

/**
  * Whether the size is fixed in 1D mode. If so, it is possible to detach the widget
  * to display it in larger size.
  */
  bool fixedsize;

/**
  * Properties of the map in 2D mode
  */
  PixmapProps pixmap;
};

///////////////////////////////////////////////////////////////////////




/**
  *
  * Enum for PARX code generation:
  * - parx_def: Parameter definitions
  * - parx_passval: Parameter assignment
  * - parx_passval_head: Function head for parameter assignment
  * - parx_parclass_def: Definition of parameter class
  * - parx_parclass_init: Initialization of parameter class
  * - parx_parclass_passval: Assignment of parameter class
  */
enum parxCodeType {parx_def, parx_passval, parx_passval_head, parx_parclass_def, parx_parclass_init, parx_parclass_passval};




/**
  * Data structure to store JCAMP-DX-specific properties of a parameter
  */
struct JcampDxProps {

/**
  * Convenience constructor
  */
  JcampDxProps(bool user_defined_parameter=true,
               const STD_string& parx_name="", double parx_assign_factor=1.0, double parx_assign_offset=0.0)
    : userdef_parameter(user_defined_parameter),
      parx_equiv_name(parx_name), parx_equiv_factor(parx_assign_factor), parx_equiv_offset(parx_assign_offset) {}

/**
  *
  *  Determines whether the JCAMP-DX parameter is user defined, see [1,2] for details.
  */
  bool userdef_parameter;

/**
  * Equivalent parx parameter name
  */
  STD_string parx_equiv_name;

/**
  * Scaling factor in assignment to equivalent parx parameter
  */
  double parx_equiv_factor;

/**
  * Offset in assignment to equivalent parx parameter
  */
  double parx_equiv_offset;

};




///////////////////////////////////////////////////////////////////////

/**
  *
  *  This is the (virtual) base class
  *  that carries the Labeled Data Record (LDR) specific interface,
  *  i.e. all common functions that are specific to LDR.
  *  Most of the elements show a simple default behaviour that will
  *  be overwritten by a reasonable behaviour in the derived classes.
  */
class LDRbase : public ListItem<LDRbase>, public virtual Labeled {

 public:

  virtual ~LDRbase();

/**
  *
  *  This function returns the parameter in ASCII using format 'serializer'
  */
  virtual STD_string print(const LDRserBase& serializer=LDRserJDX()) const;

/**
  *  Passes the parameter in ASCII format to the ostream 'os' using format 'serializer'
  */
  virtual STD_ostream& print2stream(STD_ostream& os, const LDRserBase& serializer) const;

/**
  *  Stream operator
  */
  friend STD_ostream& operator << (STD_ostream& s, const LDRbase& value) { return value.print2stream(s,LDRserJDX());}


/**
  *
  * Writes the parameter(s) to file  using format 'serializer'. In case of calling this function
  * of a single paramter, the file will consist of only this parameter. In case
  * of a parameter block, all parameter values are written to the file.
  * If an error occurs, -1 is returned.
  */
  virtual int write(const STD_string &filename, const LDRserBase& serializer=LDRserJDX()) const;

/**
  *
  *  Returns the value of the parameter as a string, optionally using format 'ser'
  */
  virtual STD_string printvalstring(const LDRserBase* ser=0) const = 0;



/**
  *  Parses the parameter using format 'serializer'.
  *  For blocks, a re-implemented 'parse' function is called.
  *  Returns 'true' if the parameter was parsed successfully.
  */
  virtual bool parse(STD_string& parstring, const LDRserBase& serializer=LDRserJDX());

/**
  *
  * Loads the parameter(s) from a file using format 'serializer'. In case of calling this function
  * of a single paramter, the file is searched for this parameter and the value
  * is assigned. In case of a parameter block, all parameter values are taken
  * from the file.
  * The return value is the number of parameters which are successfully parsed.
  * If an error occurs, -1 is returned.
  */
  virtual int load(const STD_string &filename, const LDRserBase& serializer=LDRserJDX());


/**
  *
  *  Parses and assigns the value in the given string, optionally using format 'ser'
  */
  virtual bool parsevalstring(const STD_string&, const LDRserBase* ser=0) = 0;





/**
  *  Returns a string describing the type of the parameter.
  *  If 'parx_equivtype' is 'true', returns the equivalent PARX parameter type
  */
  virtual STD_string get_typeInfo(bool parx_equivtype=false) const = 0;


/**
  * Returns the minimum allowed value of the parameter.
  * Only used when editing the parameter in a GUI
  */
  virtual double get_minval() const {return 0.0;}


/**
  * Returns the maximum allowed value of the parameter
  * Only used when editing the parameter in a GUI
  */
  virtual double get_maxval() const {return 0.0;}

/**
  * Returns whether the paramerer has a valid interval of allowed values.
  * Only used when editing the parameter in a GUI
  */
  bool has_minmax() const {return get_maxval()>get_minval();}


/**
  *  Returns a deep copy of the parameter
  */
  virtual LDRbase* create_copy() const = 0;


/**
  *
  *  Returns the description of the parameter, used in the text of tooltips in a GUI 
  */
  const STD_string& get_description() const {return description;}

/**
  *
  *  Sets the description of the parameter, used in the text of tooltips in a GUI
  */
  LDRbase&  set_description(const STD_string& descr) {description=descr; return *this;}


/**
  * Returns possible alternative values of the parameter, i.e. the item strings of an enum
  */
  virtual svector get_alternatives() const {return svector();}


/**
  *
  *  Returns the physical unit of the parameter
  */
  const STD_string& get_unit() const {return unit;}

/**
  *
  *  Sets the physical unit of the parameter
  */
  LDRbase& set_unit(const STD_string& un) {unit=un; return *this;}

  
/**
  *
  *  Returns the parameterMode
  */
  virtual parameterMode get_parmode() const {return parmode;}

/**
  *
  *  Sets the parameterMode
  */
  virtual LDRbase& set_parmode(parameterMode parameter_mode) {parmode=parameter_mode; return *this;}

/**
  *
  *  Returns the fileMode
  */
  virtual fileMode get_filemode() const {return filemode;}

/**
  *
  *  Sets the fileMode
  */
  virtual LDRbase& set_filemode(fileMode file_mode) {filemode=file_mode; return *this;}



/**
  *
  *  Returns the properties of axis display in the GUI, only useful for LDRarrays
  */
  virtual GuiProps get_gui_props() const {return GuiProps();}

/**
  *
  *  Sets the properties of axis display in the GUI, only useful for LDRarrays
  */
  virtual LDRbase& set_gui_props(const GuiProps&) {return *this;}



/**
  *
  * Returns C code that can be used together with the PARX(Bruker) compiler
  */
  virtual STD_string get_parx_code(parxCodeType type) const;


/**
  *
  *  Returns the JCAMP-DX-specific properties of this parameter
  */
  JcampDxProps get_jdx_props() const {return jdx_props;}

/**
  *
  *  Sets the JCAMP-DX-specific properties of this parameter
  */
  LDRbase& set_jdx_props(const JcampDxProps& jp) {jdx_props=jp; return *this;}



  // virtual functions to emulate a dynamic cast
  virtual LDRblock*     cast(LDRblock*)     {return 0;}
  virtual int*          cast(int*)          {return 0;}
  virtual long*         cast(long*)         {return 0;}
  virtual float*        cast(float*)        {return 0;}
  virtual double*       cast(double*)       {return 0;}
  virtual bool*         cast(bool*)         {return 0;}
  virtual STD_complex*  cast(STD_complex*)  {return 0;}
  virtual LDRenum*      cast(LDRenum*)      {return 0;}
  virtual STD_string*   cast(STD_string*)   {return 0;}
  virtual LDRfileName*  cast(LDRfileName*)  {return 0;}
  virtual LDRformula*   cast(LDRformula*)   {return 0;}
  virtual LDRaction*    cast(LDRaction*)    {return 0;}
  virtual iarray*       cast(iarray*)       {return 0;}
  virtual darray*       cast(darray*)       {return 0;}
  virtual farray*       cast(farray*)       {return 0;}
  virtual carray*       cast(carray*)       {return 0;}
  virtual sarray*       cast(sarray*)       {return 0;}
  virtual LDRtriple*    cast(LDRtriple*)    {return 0;}
  virtual LDRfunction*  cast(LDRfunction*)  {return 0;}


  // with these functions it is possible to assign a unique id to each paramerer
  int set_parameter_id(int newid) {id=newid; return 0;}
  int get_parameter_id() const {return id;}

  LDRbase& set_cmdline_option(const STD_string& opt) {cmdline_option=opt; return *this;}
  STD_string get_cmdline_option() const {return cmdline_option;}


 protected:

  LDRbase();
  LDRbase(const LDRbase& ldr);
  LDRbase& operator = (const LDRbase& ldr);

  STD_string get_parx_def_string(const STD_string type, unsigned int dim) const;



 private:
  friend class LDRwidget;
  friend class LDRblock;

  JcampDxProps jdx_props;


  parameterMode parmode;
  fileMode filemode;

  STD_string description;
  STD_string unit;

  int id;

  STD_string cmdline_option;

};



///////////////////////////////////////////////////////////////
//
//  Some interface classes which help editing parameters in a GUI

class LDReditWidget {

 public:

  virtual void updateWidget() = 0;

  virtual ~LDReditWidget() {} // to avoid compiler warnings
};



class LDReditCaller {

 public:

  virtual void parameter_relations(LDReditWidget* editwidget) = 0;

  virtual ~LDReditCaller() {} // to avoid compiler warnings
};



/** @}
  */











#endif
