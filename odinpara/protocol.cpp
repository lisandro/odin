#include "protocol.h"

#include <tjutils/tjtest.h>

Protocol::Protocol(const STD_string& label) : LDRblock(label),
  system(label+"_system"),
  geometry(label+"_geometry"),
  seqpars(label+"_seqpars"),
  methpars(label+"_methpars"),
  study(label+"_study") {
  append_all_members();
}


Protocol& Protocol::operator = (const Protocol& p) {
  LDRblock::operator = (p);
  system=p.system;
  geometry=p.geometry;
  seqpars=p.seqpars;
  methpars.create_copy(p.methpars);
  study=p.study;
  append_all_members();
  return *this;
}

bool Protocol::operator < (const Protocol& rhs) const {
  STD_list<STD_string> exclude;
  exclude.push_back("AcquisitionStart");
  exclude.push_back("offsetSlice");
  exclude.push_back("Datatype");
  exclude.push_back("ReceiveCoilName"); // can be different in composed data
  
  // account for different TRs in gated mode
  if(seqpars.get_PhysioTrigger() || rhs.seqpars.get_PhysioTrigger()) {
    exclude.push_back("PhysioTrigger");
    exclude.push_back("RepetitionTime");
    exclude.push_back("ExpDuration");
  }

  double accuracy=0.01; // This will make geometries equal which are approximately the same
  return LDRblock::compare(rhs,&exclude,accuracy);
}


void Protocol::append_all_members() {
  LDRblock::clear();
  merge(study);
  merge(system);
  merge(geometry);
  merge(seqpars);
  merge(methpars);
}


///////////////////////////////////////////////////////////////////////


#ifndef NO_UNIT_TEST
class ProtocolTest : public UnitTest {

 public:
  ProtocolTest() : UnitTest("Protocol") {}

 private:
  bool check() const {
    Log<UnitTest> odinlog(this,"check");

    // Testing == operator (of LDRblock)
    Protocol p1;
    Protocol p2;

    if(!(p1==p2)) {
      ODINLOG(odinlog,errorLog) << "Protocol::operator == (equal) failed: p1=" << p1 << "p2=" << p2 << STD_endl;
      return false;
    }

    p1.seqpars.set_AcquisitionStart(123); // should still be equal
    if(!(p1==p2)) {
      ODINLOG(odinlog,errorLog) << "Protocol::operator == (AcquisitionStart) failed: p1=" << p1 << "p2=" << p2 << STD_endl;
      return false;
    }

    p1.seqpars.set_RepetitionTime(12345);
    if(p1==p2) {
      ODINLOG(odinlog,errorLog) << "Protocol::operator == (unequal) failed: p1=" << p1 << "p2=" << p2 << STD_endl;
      return false;
    }

    if(p1<p2) {
      ODINLOG(odinlog,errorLog) << "Protocol::operator < failed: p1=" << p1 << "p2=" << p2 << STD_endl;
      return false;
    }

    SeqPars sp;
    if( p1.LDRblock::operator == (sp) ) {
      ODINLOG(odinlog,errorLog) << "Protocol::operator == (SeqPars) failed: p1=" << p1 << "sp=" << sp << STD_endl;
      return false;
    }


    // Testing copying of custom parameters
    LDRint testint(7,"testint");
    p1.methpars.append(testint);
    p2=p1;
    LDRbase* pldr=p2.get_parameter("testint");
    if( !pldr ) {
      ODINLOG(odinlog,errorLog) << "Protocol::operator =  methpars.testint not found" << STD_endl;
      return false;
    }
    int* intdummy=0; // avoid compiler warning
    intdummy=pldr->cast(intdummy);
    if( !intdummy ) {
      ODINLOG(odinlog,errorLog) << "Protocol::operator =  methpars.testint cannot be casted" << STD_endl;
      return false;
    }
    if( (*intdummy)!=7 ) {
      ODINLOG(odinlog,errorLog) << "Protocol::operator =  intdummy=" << (*intdummy) << STD_endl;
      return false;
    }

    return true;
  }

};

void alloc_ProtocolTest() {new ProtocolTest();} // create test instance
#endif

