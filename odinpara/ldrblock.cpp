#include "ldrblock.h"
#include <tjutils/tjlist_code.h>

#ifdef HAVE_LOCALE_H
#include <locale.h>
#endif

void set_c_locale() {
  Log<LDRcomp> odinlog("LDRblock","set_c_locale");

#ifdef HAVE_LOCALE_H
 // set locale to default before reading/writing blocks
 // to get decimal point on all platforms
  const char* retval=setlocale(LC_NUMERIC, "C");
  ODINLOG(odinlog,normalDebug) << "LC_NUMERIC=" << retval << STD_endl;
#endif
}


/////////////////////////////////////////////////////////////////////////////////////////


LDRblock::LDRblock(const STD_string& title) : garbage(0), embed(true) {
  Log<LDRcomp> odinlog(title.c_str(),"LDRblock(title)");
  set_label(title);
}

LDRblock::LDRblock(const LDRblock& block) : garbage(0) {
  LDRblock::operator = (block);
}

LDRblock::~LDRblock() {
  Log<LDRcomp> odinlog(this,"~LDRblock");
  if(garbage) {
    ODINLOG(odinlog,normalDebug) << "Removing " << LDRlist::size() << " parameters from block" << STD_endl;
    LDRlist::clear(); // remove before deleting
    ODINLOG(odinlog,normalDebug) << "Deleting " << garbage->size() << " parameters in garbage" << STD_endl;
    for(STD_list<LDRbase*>::iterator it=garbage->begin(); it!=garbage->end(); ++it) {
      ODINLOG(odinlog,normalDebug) << "Deleting >" << (*it)->get_label() << "<" << STD_endl;
      delete (*it);
    }
    delete garbage;
  }
}


LDRblock& LDRblock::operator = (const LDRblock& block) {
  Log<LDRcomp> odinlog(this,"LDRblock::operator = ");
  LDRbase::operator = (block);
//  LDRlist::operator = (block); // Unneccessary since the copied list would be cleared in the next line
  LDRlist::clear(); // start with empty block so that members are copied by value and not by pointer (as done in List)
  embed=block.embed;
  return *this;
}


LDRlist::constiter LDRblock::ldr_exists(const STD_string& label) const {
  Log<LDRcomp> odinlog(this,"ldr_exists");
  for(constiter it=get_const_begin(); it!=get_const_end(); ++it) {

    ODINLOG(odinlog,normalDebug) << "comparing LDR  >" << label << "< and >" << (*it)->get_label() << "<" << STD_endl;

    if( (*it)->get_label()==label ) {
      ODINLOG(odinlog,normalDebug) <<  "LDR found" << STD_endl;
      return it;
    }
  }
  ODINLOG(odinlog,normalDebug) << "LDR not found" << STD_endl;
  return get_const_end();
}



int LDRblock::parse_ldr_list(STD_string& parstring, const LDRserBase& serializer) {
  Log<LDRcomp> odinlog(this,"parse_ldr_list");

  ODINLOG(odinlog,normalDebug) << "parstring=>" << parstring << "<" << STD_endl;

  // get 1st parameter before starting while loop
  STD_string parlabel(serializer.get_parlabel(parstring));
  ODINLOG(odinlog,normalDebug) << "initial parlabel: >" << parlabel << "<" << STD_endl;

  constiter ldr_it=get_const_begin();
  int n_parsed=0;
  while(parlabel!="") {
    if( (ldr_it=ldr_exists(parlabel) )!=get_const_end() ) {
      if(!(*ldr_it)->parse(parstring,serializer)) {
        ODINLOG(odinlog,normalDebug) << "parsing of >" << parlabel << "< failed" << STD_endl;
        return -1;
      } else {
        ODINLOG(odinlog,normalDebug) << "parsing of >" << parlabel << "< successful" << STD_endl;
        n_parsed++;
      }
    } else { // ignore parameter
      ODINLOG(odinlog,normalDebug) << "ignoring >" << parlabel << "<" << STD_endl;
      serializer.remove_next_ldr(parstring);
    }

    ODINLOG(odinlog,normalDebug) << "parstring after parsing " << parlabel << ":" << parstring << STD_endl;

    // get next parameter
    parlabel=serializer.get_parlabel(parstring);
  }
  return n_parsed;
}



bool LDRblock::parse(STD_string& parstring, const LDRserBase& serializer) {
  Log<LDRcomp> odinlog(this,"parse");
  if(parseblock(parstring,serializer)<0) return false;
  ODINLOG(odinlog,normalDebug) << "parstring(pre)=>" << parstring << "<" << STD_endl;

  // eat up my part from parstring
  STD_string blockbody(serializer.get_blockbody(parstring,true));
  parstring=replaceStr(parstring,blockbody,""); // remove me from parstring
  ODINLOG(odinlog,normalDebug) << "parstring(post)=>" << parstring << "<" << STD_endl;

  return true;
}

int LDRblock::load(const STD_string &filename, const LDRserBase& serializer) {
  Log<LDRcomp> odinlog(this,"load");
  set_c_locale(); // Make sure we have right locale
  STD_string blockstr;
  int retval=::load(blockstr,filename);
  ODINLOG(odinlog,normalDebug) <<  "retval(" << filename << ")=" << retval << STD_endl;
  if(retval<0) return -1;
  return parseblock(dos2unix(blockstr),serializer);
}


int LDRblock::parseblock(const STD_string& source, const LDRserBase& serializer) {
  Log<LDRcomp> odinlog(this,"parseblock");
  int result=-1;
  STD_string source_without_comments=serializer.remove_comments(source);
  STD_string blocklabel=serializer.get_blocklabel(source_without_comments);
  if(blocklabel!="") {
    set_label(blocklabel);
    STD_string blockbody(serializer.get_blockbody(source_without_comments,false));
    result=parse_ldr_list(blockbody,serializer);
  }
  return result;
}






STD_string LDRblock::print(const LDRserBase& serializer) const {
  Log<LDRcomp> odinlog(this,"print");

  STD_string result;

  bool top_level_cache=serializer.top_level;
  if(serializer.top_level) {
    result+=serializer.get_top_header();
    serializer.top_level=false;
  }
  result+=serializer.get_prefix(*this);
  ODINLOG(odinlog,normalDebug) << "printing " << numof_pars() << " parameters" << STD_endl;
  for(constiter it=get_const_begin(); it!=get_const_end(); ++it) {
    result+=(*it)->print(serializer);
  }
  result+=serializer.get_postfix(*this);
  serializer.top_level=top_level_cache; // reset to old state
  return result;
}


STD_ostream& LDRblock::print2stream(STD_ostream& os, const LDRserBase& serializer) const {

  bool top_level_cache=serializer.top_level;
  if(serializer.top_level) {
    os << serializer.get_top_header();
    serializer.top_level=false;
  }
  
  os << serializer.get_prefix(*this);
  LDRblock* pblock=0;
  for(constiter it=get_const_begin(); it!=get_const_end(); ++it) {
    if((*it)->get_filemode()!=exclude) {
      bool isblock=(*it)->cast(pblock);
      if(!isblock) os << serializer.get_prefix(**it);
      (*it)->print2stream(os,serializer);
      if(!isblock) os << serializer.get_postfix(**it);
    }
  }
  os << serializer.get_postfix(*this);
  serializer.top_level=top_level_cache; // reset to old state
  return os;
}


int LDRblock::write(const STD_string &filename, const LDRserBase& serializer) const {
  Log<LDRcomp> odinlog(this,"write");

  set_c_locale(); // Make sure we have right locale

  STD_ofstream outfile(filename.c_str());
  LDRblock::print2stream(outfile,serializer);
  outfile.close();

  return 0;
}

STD_string LDRblock::printval(const STD_string& parameterName, bool append_unit) const {
  Log<LDRcomp> odinlog(this,"printval");
  STD_string result;
  constiter it=ldr_exists(parameterName);
  if(it!=get_const_end()) {
    result=(*it)->printvalstring();
    if(append_unit) result+=(*it)->get_unit();
  }
  return result;
}


bool LDRblock::parseval(const STD_string& parameterName, const STD_string& value) {
  Log<LDRcomp> odinlog(this,"parseval");
  bool result=false;
  constiter it=ldr_exists(parameterName);
  if(it!=get_const_end()) {
    STD_string valdummy=value;
    result=(*it)->parsevalstring(valdummy);
  }
  return result;
}


STD_string LDRblock::get_parx_code(parxCodeType type) const {
#ifdef PARAVISION_PLUGIN
  STD_string result=LDRbase::get_parx_code(type);
  if(type==parx_def) result=""; // omit results from LDRbase

  if(type==parx_def) {
    for(constiter it=get_const_begin(); it!=get_const_end(); ++it) {
      if((*it)->get_jdx_props().userdef_parameter) result+=(*it)->get_parx_code(parx_def);
    }
  }

  if(type==parx_parclass_def) {
    result="parclass {\n";
    for(constiter it=get_const_begin(); it!=get_const_end(); ++it) {
      if((*it)->get_jdx_props().userdef_parameter) result+=STD_string((*it)->get_label())+";\n";
    }
    result+="}  "+STD_string(get_label())+";\n";
  }

  if(type==parx_parclass_init) {
    STD_string visabilityString;
    result="PARX_hide_class (NOT_HIDDEN, \""+STD_string(get_label())+"\");\n";
    for(constiter it=get_const_begin(); it!=get_const_end(); ++it) {
      if((*it)->get_jdx_props().userdef_parameter) {
        if( (*it)->get_parmode() == hidden) visabilityString="HIDDEN";
        else visabilityString="NOT_HIDDEN";
        result+="PARX_hide_pars ("+visabilityString+", \""+(*it)->get_label()+"\");\n";
        result+="PARX_hide_pars (HIDE_IN_FILE, \""+STD_string((*it)->get_label())+"\");\n";
        if( (*it)->get_parmode() == noedit) {
          result+="PARX_noedit (TRUE, \""+STD_string((*it)->get_label())+"\");\n";
        }
      }
    }
  }

  if(type==parx_parclass_passval) {
    for(constiter it=get_const_begin(); it!=get_const_end(); ++it) {
      if((*it)->get_jdx_props().userdef_parameter && (*it)->get_jdx_props().parx_equiv_name!="" ) {
        result+=(*it)->get_parx_code(parx_passval_head);
      }
    }
    result+="ParxRelsReadClass(\""+STD_string(get_label())+"\",parx_filename);\n"; // hardcoded filename variable
    for(constiter it=get_const_begin(); it!=get_const_end(); ++it) {
      if((*it)->get_jdx_props().userdef_parameter && (*it)->get_jdx_props().parx_equiv_name!="" ) {
        result+=(*it)->get_parx_code(parx_passval);
      }
    }
  }

  return result;
#else
  return "";
#endif
}





unsigned int LDRblock::numof_pars() const {
  Log<LDRcomp> odinlog(this,"numof_pars");
  unsigned int n=0;
  ODINLOG(odinlog,normalDebug) << "size()=" << size() << STD_endl;
  for(constiter it=get_const_begin(); it!=get_const_end(); ++it) {
    ODINLOG(odinlog,normalDebug) << "trying to count parameter ..." << STD_endl;
    if((*it)->get_jdx_props().userdef_parameter) n++;
    ODINLOG(odinlog,normalDebug) << "counted parameter >" << (*it)->get_label() << "<" << STD_endl;
  }
  return n;
}



LDRblock& LDRblock::merge(LDRblock& block, bool onlyUserPars) {
  Log<LDRcomp> odinlog(this,"merge");
  for(iter it=block.get_begin(); it!=block.get_end(); ++it) {
    if( onlyUserPars ) {
      if( (*it)->get_jdx_props().userdef_parameter ) append(**it);
    } else append(**it);
  }
  return *this;
}


LDRblock& LDRblock::unmerge(LDRblock& block) {
  Log<LDRcomp> odinlog(this,"unmerge");
  for(iter it=block.get_begin(); it!=block.get_end(); ++it) {
    remove(**it);
  }
  return *this;
}


LDRbase* LDRblock::get_parameter(const STD_string& ldrlabel) {
  Log<LDRcomp> odinlog(this,"get_parameter");
  for(iter it=get_begin(); it!=get_end(); ++it) {
    if( (*it)->get_label()==ldrlabel ) {
      return (*it);
    }
  }
  return 0;
}


bool LDRblock::parameter_exists(const STD_string& ldrlabel) const {
  Log<LDRcomp> odinlog(this,"parameter_exists");
    bool result=false;
    if(ldr_exists(ldrlabel)!=get_const_end()) result=true;
    return result;
}


LDRblock& LDRblock::set_prefix(const STD_string& prefix) {
  Log<LDRcomp> odinlog(this,"set_prefix");
  if( STD_string(get_label()).find(prefix) == STD_string::npos ) set_label(prefix+"_"+get_label());

  for(iter it=get_begin(); it!=get_end(); ++it) {
    if((*it)->get_jdx_props().userdef_parameter) {
      if( STD_string((*it)->get_label()).find(prefix) != 0 )
              (*it)->set_label(prefix+"_"+(*it)->get_label());
    }
  }
  return *this;
}


LDRbase& LDRblock::set_parmode(parameterMode parameter_mode) {
  LDRbase::set_parmode(parameter_mode);
  for(iter it=get_begin(); it!=get_end(); ++it) (*it)->set_parmode(parameter_mode);
  return *this;
}


LDRbase& LDRblock::set_filemode(fileMode file_mode) {
  LDRbase::set_filemode(file_mode);
  for(iter it=get_begin(); it!=get_end(); ++it) (*it)->set_filemode(file_mode);
  return *this;
}


LDRbase& LDRblock::operator [] (unsigned int i) {
  Log<LDRcomp> odinlog(this,"operator []");
  unsigned int n=numof_pars();
  if(i>=n) return *this;
  unsigned int j=0;
  for(iter it=get_begin(); it!=get_end(); ++it) {
    if((*it)->get_jdx_props().userdef_parameter) {
      if(j==i) return *(*it);
      j++;
    }
  }
  return *this;
}


const LDRbase& LDRblock::operator [] (unsigned int i) const {
  Log<LDRcomp> odinlog(this,"operator [] const");
  unsigned int n=numof_pars();
  if(i>=n) return *this;
  unsigned int j=0;
  for(constiter it=get_const_begin(); it!=get_const_end(); ++it) {
    if((*it)->get_jdx_props().userdef_parameter) {
      if(j==i) return *(*it);
      j++;
    }
  }
  return *this;
}


LDRbase& LDRblock::get_parameter_by_id(int id) {
  for(iter it=get_begin(); it!=get_end(); ++it) {
    if((*it)->get_parameter_id()==id) return *(*it);
  }
  return *this;
}

LDRblock& LDRblock::append_copy(const LDRbase& src) {
  if(!garbage) garbage=new STD_list<LDRbase*>;
  LDRbase* ldrcopy=src.create_copy();
  garbage->push_back(ldrcopy);
  append(*ldrcopy);
  return *this;
}

LDRblock& LDRblock::create_copy(const LDRblock& src) {
  LDRblock::operator = (src);
  if(!garbage) garbage=new STD_list<LDRbase*>;
  for(constiter ldr_it=src.get_const_begin(); ldr_it!=src.get_const_end(); ++ldr_it) {
    if((*ldr_it)->get_jdx_props().userdef_parameter)
      append_copy(**ldr_it);
  }
  return *this;
}


LDRblock& LDRblock::copy_ldr_vals(const LDRblock& src) {
  Log<LDRcomp> odinlog(this,"copy_ldr_vals");

  for(constiter ldr_it=src.get_const_begin(); ldr_it!=src.get_const_end(); ++ldr_it) {
    ODINLOG(odinlog,normalDebug) << "copying " << (*ldr_it)->get_label() << STD_endl;
    constiter it;
    if( (it=ldr_exists((*ldr_it)->get_label()))!=get_const_end() ) {
      STD_string valstring((*ldr_it)->printvalstring());
      ODINLOG(odinlog,normalDebug) << "valstring=" << valstring << STD_endl;
      (*it)->parsevalstring(valstring);
    }
  }
  return *this;
}



bool LDRblock::compare(const LDRblock& rhs, const STD_list<STD_string>* exclude, double accuracy) const {
  Log<LDRcomp> odinlog(this,"compare");

  unsigned int mynpars=numof_pars();
  unsigned int rhnpars=rhs.numof_pars();
  ODINLOG(odinlog,normalDebug) << "mynpars/rhnpars=" << mynpars << "/" << rhnpars << STD_endl;
  if(mynpars!=rhnpars) return mynpars<rhnpars;

  constiter my_it;

  // Instances required for STL replacement
  STD_list<STD_string>::const_iterator exclude_begin,exclude_end;
  if(exclude) {
    exclude_begin=exclude->begin();
    exclude_end=exclude->end();
  }

  for(constiter rh_it=rhs.get_const_begin(); rh_it!=rhs.get_const_end(); ++rh_it) {
    STD_string parlabel((*rh_it)->get_label());
    my_it=ldr_exists(parlabel);

    if( exclude && (STD_find(exclude_begin, exclude_end, parlabel) != exclude_end ) ) {
      ODINLOG(odinlog,normalDebug) << "Excluding " << parlabel << " from comparison" << STD_endl;
    } else {

      if(my_it==get_const_end()) return true; // 'this' does not include all pars of 'rhs' -> it is 'smaller'

      // check for type
      STD_string mytype=(*my_it)->get_typeInfo();
      STD_string rhtype=(*rh_it)->get_typeInfo();
      ODINLOG(odinlog,normalDebug) << "mytype/rhtype(" << parlabel << ")=" << mytype << "/" << rhtype << STD_endl;
      if( mytype!=rhtype ) return mytype<rhtype;


      // Cast to double/float if possible
      double myfloat=0.0;
      double rhfloat=0.0;
      float* dumfloat=0;
      double* dumdouble=0;
      bool my_is_float=false;
      bool rh_is_float=false;

      if( (dumfloat=(*my_it)->cast(dumfloat)) )   {myfloat=(*dumfloat);  my_is_float=true;}
      if( (dumdouble=(*my_it)->cast(dumdouble)) ) {myfloat=(*dumdouble); my_is_float=true;}
      if( (dumfloat=(*rh_it)->cast(dumfloat)) )   {rhfloat=(*dumfloat);  rh_is_float=true;}
      if( (dumdouble=(*rh_it)->cast(dumdouble)) ) {rhfloat=(*dumdouble); rh_is_float=true;}

      ODINLOG(odinlog,normalDebug) << "my_is_float/rh_is_float(" << parlabel << ")=" << my_is_float << "/" << rh_is_float << STD_endl;

      if(my_is_float && rh_is_float) {
        ODINLOG(odinlog,normalDebug) << "myfloat/rhfloat(" << parlabel << ")=" << myfloat << "/" << rhfloat << STD_endl;

        if(fabs(myfloat-rhfloat)>accuracy) { // Otherwise, values are considered equal
          ODINLOG(odinlog,normalDebug) << "Unequal myfloat/rhfloat(" << parlabel << ")=" << myfloat << "/" << rhfloat << STD_endl;
          return myfloat<rhfloat;
        }
      } else {

        // compare value by string
        STD_string myval=(*my_it)->printvalstring();
        STD_string rhval=(*rh_it)->printvalstring();
        ODINLOG(odinlog,normalDebug) << "myval/rhval(" << parlabel << ")=" << myval << "/" << rhval << STD_endl;
        if( myval!=rhval) {
          ODINLOG(odinlog,normalDebug) << "Unequal myval/rhval(" << parlabel << ")=" << myval << "/" << rhval << STD_endl;
          return myval<rhval;
        }
      }
    }
  }

  return false; // all equal
}


LDRblock& LDRblock::append_member(LDRbase& ldr,const STD_string ldrlabel) {
  Log<LDRcomp> odinlog(this,"append_member");
  if(ldrlabel!=STD_string("")) ldr.set_label(ldrlabel);
  append(ldr);
  return *this;
}



#ifndef NO_CMDLINE
LDRblock& LDRblock::parse_cmdline_options(int argc, char *argv[], bool modify) {
  char optval[ODIN_MAXCHAR];
  for(constiter ldr_it=get_const_begin(); ldr_it!=get_const_end(); ++ldr_it) {
    STD_string opt((*ldr_it)->get_cmdline_option());
    if(opt!="") {
      STD_string optstr("-"+opt);
      bool* booldummy=0; // avoid compiler warning
      booldummy=(*ldr_it)->cast(booldummy);
      if(booldummy) {
        if(isCommandlineOption(argc,argv,optstr.c_str())) (*booldummy)=true;
      } else {
        if(getCommandlineOption(argc,argv,optstr.c_str(),optval,ODIN_MAXCHAR,modify)) (*ldr_it)->parsevalstring(optval);
      }
    }
  }
  return *this;
}

STD_map<STD_string,STD_string> LDRblock::get_cmdline_options() const {
  STD_map<STD_string,STD_string> result;
  for(constiter ldr_it=get_const_begin(); ldr_it!=get_const_end(); ++ldr_it) {
    STD_string opt((*ldr_it)->get_cmdline_option());
    if(opt!="") {
      STD_string descr=(*ldr_it)->get_description();

      STD_string unit=(*ldr_it)->get_unit();
      if(unit!="") descr+=" ["+unit+"]";

      // Exclude default value for Boolean types
      STD_string defval;
      bool* booldummy=0; // avoid compiler warning
      booldummy=(*ldr_it)->cast(booldummy);
      if(!booldummy) defval=(*ldr_it)->printvalstring();

      svector alt=(*ldr_it)->get_alternatives();

      if(defval!="" || alt.size()) {
        descr+=" (";
        if(alt.size()) descr+="options="+tokenstring(alt,0)+", ";
        if(defval!="") descr+="default="+defval+unit;
        descr+=")";
      }

      result[opt]=descr;
    }
  }
  return result;
}


STD_string LDRblock::get_cmdline_usage(const STD_string& lineprefix) const {
  STD_string result;
  STD_map<STD_string,STD_string> optmap=get_cmdline_options();
  for(STD_map<STD_string,STD_string>::const_iterator it=optmap.begin(); it!=optmap.end(); ++it) {
    result += lineprefix+"-"+it->first+": "+it->second+"\n";
  }
  return result;
}
#endif


void LDRblock::init_static() {
  Log<LDRcomp> odinlog("LDRblock","init_static");
  set_c_locale();
}


void LDRblock::destroy_static() {
  Log<LDRcomp> odinlog("LDRblock","destroy_static");

}



EMPTY_TEMPL_LIST bool StaticHandler<LDRblock>::staticdone=false;


// Template instantiations
template class ListItem<LDRbase>;
template class List<LDRbase,LDRbase*,LDRbase&>;


