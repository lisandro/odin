/***************************************************************************
                          ldrarrays.h  -  description
                             -------------------
    begin                : Mon Jul 5 2004
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef LDRARRAYS_H
#define LDRARRAYS_H

#include <tjutils/tjarray.h>
#include <odinpara/ldrbase.h>
#include <odinpara/ldrnumbers.h>
#include <odinpara/ldrtypes.h>


#define COMPRESSION_THRESHOLD_SIZE 256


/**
  * @addtogroup ldr
  * @{
  */


   
/**
  *
  *  Labeled Data Record (LDR) template class for representing multi-dimensional arrays
  */
template<class A,class J>
class LDRarray : public A, public virtual LDRbase  {

 public:

/**
  *  Default constructor
  */
  LDRarray  () : A() {common_init();}

/**
  *  Constructor with the following arguments:
  * - a:             Initial value for the array base class
  * - name:          The label of the parameter
  */
  LDRarray (const A& a, const STD_string& name="");

/**
  *  Assignment from scalar, i.e. fill all elements with this value
  */
//  LDRarray (const J& jv) {common_init(); A::operator = (jv);}

/**
  *  Copy constructor
  */
  LDRarray (const LDRarray<A,J>& ja) {common_init(); LDRarray<A,J>::operator = (ja);}

/**
  *  Assignment operator from an array
  */
  LDRarray<A,J>& operator = (const A& a);

/**
  *  Assignment operator from scalar, i.e. fill all elements with this value
  */
  LDRarray<A,J>& operator = (const J& jv) {A::operator = (jv); return *this;}

/**
  *  Copy assignment
  */
  LDRarray<A,J>& operator = (const LDRarray<A,J>& ja);


  // overwriting virtual functions from LDRbase
  STD_ostream& print2stream(STD_ostream& os, const LDRserBase& serializer) const;
  STD_string printvalstring(const LDRserBase* ser=0) const;
  bool parsevalstring(const STD_string& parstring, const LDRserBase* ser=0);
  STD_string get_parx_code(parxCodeType type) const;
  GuiProps get_gui_props() const {return guiprops;}
  LDRbase& set_gui_props(const GuiProps& gp) {guiprops=gp; return *this;}
  STD_string get_typeInfo(bool parx_equivtype=false) const;
  LDRbase* create_copy() const {return new LDRarray<A,J>(*this);}
  A* cast(A*) {return this;}


  // final overrider
  friend STD_ostream& operator << (STD_ostream& s,const LDRarray<A,J>& ja) { return ja.print2stream(s,LDRserJDX());}
  int write(const STD_string& filename, const LDRserBase& serializer=LDRserJDX()) const {return LDRbase::write(filename,serializer);}
  int load(const STD_string &filename, const LDRserBase& serializer=LDRserJDX()) {return LDRbase::load(filename,serializer);}


 private:
  void common_init();
  STD_string get_dim_str(const LDRserBase* ser) const;

  bool encode(STD_string* ostring, STD_ostream* ostream) const;

  bool use_compression() const {return (get_filemode()==compressed)&&(A::total()>COMPRESSION_THRESHOLD_SIZE);}

  GuiProps guiprops;

  mutable STD_string typeInfo_cache;

};

//////////////////////////////////////////////////////////////////////////////////
//
// Aliases:
//

/**
  * A Labeled Data Record (LDR) array of strings
  */
typedef LDRarray<sarray,LDRstring>   LDRstringArr;

/**
  * A Labeled Data Record (LDR) array of integer numbers
  */
typedef LDRarray<iarray,LDRint>   LDRintArr;

/**
  * A Labeled Data Record (LDR) array of single-precision floating point numbers
  */
typedef LDRarray<farray,LDRfloat>   LDRfloatArr;

/**
  * A Labeled Data Record (LDR) array of double-precision floating point numbers
  */
typedef LDRarray<darray,LDRdouble>   LDRdoubleArr;

/**
  * A Labeled Data Record (LDR) array of single-precision complex numbers
  */
typedef LDRarray<carray,LDRcomplex>   LDRcomplexArr;



/////////////////////////////////////////////////////////////////////////////
//    Specialised array classes:


/**
  *  A Labeled Data Record (LDR) vector of 3 float values which is used to store spatial positions, FOVs, etc.
  */
class LDRtriple : public LDRfloatArr {

 public:

/**
  *  Default constructor
  */
  LDRtriple () : LDRfloatArr(farray(3)) {}

/**
  *  Constructor with the following arguments:
  * - xpos: Spatial position in the first dimension
  * - ypos: Spatial position in the second dimension
  * - zpos: Spatial position in the third dimension
  * - name:          The label of the parameter
  */
  LDRtriple (float xpos, float ypos, float zpos, const STD_string& name="");

/**
  *  Copy constructor
  */
  LDRtriple (const LDRtriple& pos) {LDRtriple::operator = (pos);}

/**
  *  Copy assignment
  */
  LDRtriple& operator = (const LDRtriple& pos) {
    LDRfloatArr::operator = (pos);
    return *this;
  }

  // overwriting virtual functions from LDRbase
  STD_string get_typeInfo(bool parx_equivtype=false) const {return "triple";}
  LDRbase* create_copy() const {return new LDRtriple(*this);}
  LDRtriple* cast(LDRtriple*) {return this;}
  farray* cast(farray*) {return 0;} // disable casting to base class

};

/** @}
  */

#endif

