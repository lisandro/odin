/***************************************************************************
                          ldrnumbers.h  -  description
                             -------------------
    begin                : Mon Jul 12 2004
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef LDRNUMBERS_H
#define LDRNUMBERS_H


#include <odinpara/ldrbase.h>
#include <tjutils/tjtypes.h>


/**
  * @addtogroup ldr
  * @{
  */
//////////////////////////////////////////////////////////////////

/**
  *
  *  Labeled Data Record (LDR) template class for representing built-in numbers
  */
template<class T>
class LDRnumber : public virtual LDRbase  {

 public:

/**
  *  Default constructor
  */
  LDRnumber() {common_init();}

/**
  *  Constructor with the following arguments:
  * - v:             Initial value for the number
  * - name:          The label of the parameter
  */
  LDRnumber(T v, const STD_string& name="");


/**
  *  Copy constructor
  */
  LDRnumber(const LDRnumber<T>& bi) {LDRnumber<T>::operator = (bi);}

/**
  *  Assignment operator from a built-in number
  */
  LDRnumber<T>& operator = (T v) {val=v; return *this;}

/**
  *  Copy assignment
  */
  LDRnumber<T>& operator = (const LDRnumber<T>& bi);

  // code to make the number appear like a built-in number
  operator T () const {return val;}
  T operator += (T rhsval) {val+=rhsval; return *this;}
  T operator -= (T rhsval) {val-=rhsval; return *this;}
  T operator *= (T rhsval) {val*=rhsval; return *this;}
  T operator /= (T rhsval) {val/=rhsval; return *this;}
  T operator ++ ()    {val=val+(T)1; return val;}            // prefix
  T operator ++ (int) {T tmp=val; val=val+(T)1; return tmp;} // postfix
  T operator -- ()    {val=val-(T)1; return val;}            // prefix
  T operator -- (int) {T tmp=val; val=val-(T)1; return tmp;} // postfix

/**
  * Specifies the minimum and maximum allowed value for this number.
  * Only useful when editing the parameter in a GUI
  */
  LDRnumber<T>& set_minmaxval(double min,double max) {minval=min; maxval=max; return *this;}

  // overwriting virtual functions from LDRbase
  STD_string printvalstring(const LDRserBase* ser=0) const;
  bool parsevalstring(const STD_string& parstring, const LDRserBase* ser=0);
  STD_string get_parx_code(parxCodeType type) const;
  double get_minval() const {return minval;}
  double get_maxval() const {return maxval;}
  STD_string get_typeInfo(bool parx_equivtype=false) const {return TypeTraits::type2label(val);}
  LDRbase* create_copy() const {return new LDRnumber<T>(*this);}
  T* cast(T*) {return &val;}

 private:

  void common_init();

  T val;
  double minval,maxval;
};

/////////////////////////////////////////////////////////////////////////////
//
// Aliases:

/**
  * An integer number
  */
typedef LDRnumber<int>   LDRint;

/**
  * An single-precision floating point number
  */
typedef LDRnumber<float>   LDRfloat;

/**
  * An double-precision floating point number
  */
typedef LDRnumber<double>   LDRdouble;

/**
  * Complex number
  */
typedef LDRnumber<STD_complex>   LDRcomplex;


/** @}
  */

#endif

