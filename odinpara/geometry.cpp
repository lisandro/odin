#include "geometry.h"

#include <tjutils/tjtest.h>


RotMatrix::RotMatrix(const STD_string& label) {
  int i,j;
  set_label(label);
  for( i = 0; i < 3; i++)
    for( j = 0; j < 3; j++)
      if ( i == j) matrix[i][j] = 1;
      else matrix[i][j] = 0;

};

RotMatrix::RotMatrix(const RotMatrix& sct) {
  RotMatrix::operator =(sct);
};

RotMatrix& RotMatrix::operator =(const RotMatrix& sct){
  set_label(sct.get_label());
  int i,j;
  for( i = 0; i < 3; i++)
    for( j = 0; j < 3; j++)
      matrix[i][j] = sct[i][j];
//  check_and_correct();
  return *this;
};

bool RotMatrix::operator == (const RotMatrix& srm) const{
  for(int i=0; i < 3; i++)
    for(int j=0; j < 3; j++)
      if(fabs(srm[i][j]-matrix[i][j])>ODIN_GEO_CHECK_LIMIT) return false;
  return true;
};

bool RotMatrix::operator < (const RotMatrix& srm) const{
  for(int i=0; i < 3; i++)
    for(int j=0; j < 3; j++)
      if(!(srm[i][j] < matrix[i][j])) return false;
  return true;
};

dvector RotMatrix::operator * (const dvector& vec) const {
  dvector result(3);

  result=0;

  for(int i=0; i < 3; i++)
    for(int j=0; j < 3; j++)
      result[i]+=matrix[i][j]*vec[j];

  return result;
}

RotMatrix RotMatrix::operator * (const RotMatrix& matrix) const {
  RotMatrix result;
  int i,j,k;
  double scalprod;


  for(j=0; j<3; j++) {
    for(i=0; i<3; i++) {

      scalprod=0.0;
      for(k=0; k<3; k++) {
        scalprod+= matrix [k][i]*(*this)[j][k];
      }
      result[j][i]=scalprod;
    }
  }

  return result;
}


RotMatrix::operator farray () const {
  farray result(3,3);
  for(int i=0; i < 3; i++) {
    for(int j=0; j < 3; j++) {
      result(i,j)=matrix[i][j];
    }
  }
  return result;
}


STD_string RotMatrix::print() const {

  // make C-style 9-array so that it can be reused in code generator
  STD_string result("{");
  for(int i=0; i < 3; i++) {
    for(int j=0; j < 3; j++) {
      if(fabs(matrix[i][j])>10e-5) result+=ftos(matrix[i][j]);
      else result+="0";
      if(i!=2 || j!=2) {
        result+=",";
        if(j==2) result+="  ";
      }
    }
  }
  result+="}";

  return result;
}


RotMatrix& RotMatrix::set_inplane_rotation(float phi) {
  Log<Para> odinlog(this,"set_inplane_rotation");
  ODINLOG(odinlog,normalDebug) << "phi=" << phi << STD_endl;
  float si=sin(phi);
  float co=cos(phi);
  (*this)[0][0]=co;  (*this)[0][1]=-si; (*this)[0][2]=0.0;
  (*this)[1][0]=si;  (*this)[1][1]=co;  (*this)[1][2]=0.0;
  (*this)[2][0]=0.0; (*this)[2][1]=0.0; (*this)[2][2]=1.0;

  return *this;
}

////////////////////////////////////////////////////////////////////////////

Geometry& Geometry::reset() {
  FOVread=ODIN_DEFAULT_FOV;
  offsetRead=0.0;
  FOVphase=ODIN_DEFAULT_FOV;
  offsetPhase=0.0;
  FOVslice=ODIN_DEFAULT_FOV;
  offsetSlice=0.0;
  nSlices=1;
  sliceThickness=5.0;
  sliceDistance=10.0;
  heightAngle=0.0;
  azimutAngle=0.0;
  inplaneAngle=0.0;
  reverseSlice=false;
  return *this;
}


Geometry::Geometry(const STD_string& label) : LDRblock(label) {
  Log<Para> odinlog(this,"Geometry(const STD_string&)");

  Mode.add_item("SlicePack",slicepack);
  Mode.add_item("Voxel/3D",voxel_3d);
  Mode.set_actual(slicepack);
  Mode.set_description("Acquisition mode, i.e. whether sequence is multi-slice- or voxel/3D-selective");

  FOVread.set_unit(ODIN_SPAT_UNIT).set_description("FOV in read direction").set_cmdline_option("fr");
  offsetRead.set_unit(ODIN_SPAT_UNIT).set_description("Spatial offset in read direction relative to isocenter");
  FOVphase.set_unit(ODIN_SPAT_UNIT).set_description("FOV in phase direction").set_cmdline_option("fp");
  offsetPhase.set_unit(ODIN_SPAT_UNIT).set_description("Spatial offset in phase direction relative to isocenter");
  FOVslice.set_unit(ODIN_SPAT_UNIT).set_description("FOV in slice direction").set_cmdline_option("fs");
  offsetSlice.set_unit(ODIN_SPAT_UNIT).set_description("Spatial offset in slice direction relative to isocenter");
  nSlices.set_description("Number of sices");
  sliceThickness.set_unit(ODIN_SPAT_UNIT).set_cmdline_option("st").set_description("Slice thickness");
  sliceDistance.set_unit(ODIN_SPAT_UNIT).set_cmdline_option("sd").set_description("Inter-slice distance (from center to center)");
  heightAngle.set_description("height rotation angle").set_cmdline_option("ah");
  azimutAngle.set_description("azimuthal rotation angle").set_cmdline_option("aa");
  inplaneAngle.set_description("inplane rotation angle").set_cmdline_option("ai");
  reverseSlice.set_description("Reverse direction of slice vector");
  Reset.set_description("Reset to default values");
  Transpose.set_description("Transpose in-plane");

  reset(); // set default values

  FOVread.set_minmaxval(0.0,2.0*ODIN_DEFAULT_FOV);
  offsetRead.set_minmaxval(0.5*-ODIN_DEFAULT_FOV,0.5*ODIN_DEFAULT_FOV);
  FOVphase.set_minmaxval(0.0,2.0*ODIN_DEFAULT_FOV);
  offsetPhase.set_minmaxval(0.5*-ODIN_DEFAULT_FOV,0.5*ODIN_DEFAULT_FOV);
  FOVslice.set_minmaxval(0.0,2.0*ODIN_DEFAULT_FOV);
  offsetSlice.set_minmaxval(0.5*-ODIN_DEFAULT_FOV,0.5*ODIN_DEFAULT_FOV);
  nSlices.set_minmaxval(1,ODIN_MAX_NUMOF_SLICES);
  sliceThickness.set_minmaxval(0.0,50.0);
  sliceDistance.set_minmaxval(0.0,0.5*ODIN_DEFAULT_FOV);
  heightAngle.set_minmaxval(-180.0,180.0);
  azimutAngle.set_minmaxval(-180.0,180.0);
  inplaneAngle.set_minmaxval(-180.0,180.0);

  Reset.set_filemode(exclude);
  Transpose.set_filemode(exclude);

  append_all_members();

  update();
}

Geometry::Geometry(const Geometry& ia) {
  Log<Para> odinlog(this,"Geometry(const Geometry&)");
  Geometry::operator = (ia);
}


Geometry& Geometry::operator = (const Geometry& ia) {
  Log<Para> odinlog(this,"Geometry::operator =");
  LDRblock::operator = (ia);

  append_all_members();

  // copy only visible members of LDRblock
  copy_ldr_vals(ia);
  update();
  return *this;
}


Geometry& Geometry::set_Mode(geometryMode mode) {
  Log<Para> odinlog(this,"set_Mode");
  Mode.set_actual(mode);
  ODINLOG(odinlog,normalDebug) << "Mode/mode=" << Mode.LDRenum::operator STD_string() << "/" << mode << STD_endl;
  update();
  return *this;
}



Geometry& Geometry::update() {
  Log<Para> odinlog(this,"update");
  if(Reset) reset();
  if(Transpose) transpose_inplane();

  FOVslice.set_parmode(edit);
  nSlices.set_parmode(edit);
  sliceThickness.set_parmode(edit);
  sliceDistance.set_parmode(edit);

  if(int(Mode)==slicepack) {
    FOVslice=sliceThickness+(double)(nSlices-1)*sliceDistance;
    FOVslice.set_parmode(hidden);
  }

  if(int(Mode)==voxel_3d) {
    nSlices=1;
    nSlices.set_parmode(hidden);
    sliceThickness=double(FOVslice);
    sliceThickness.set_parmode(hidden);
    sliceDistance=0.0;
    sliceDistance.set_parmode(hidden);
  }

  cache_up2date=false;

  return *this;
}

/*
int Geometry::load(const STD_string &filename) {
  int stat=LDRblock::load(filename); // read once to set correct mode
  if(stat<=0) return -1;
  update(); // set new mode
  return LDRblock::load(filename);
}
*/


Geometry& Geometry::set_FOV(direction dir,double fov) {
  if(dir==readDirection) FOVread=fov;
  if(dir==phaseDirection) FOVphase=fov;
  if(dir==sliceDirection) FOVslice=fov;
  update();
  return *this;
}

double Geometry::get_FOV(direction dir) const {
  if(dir==readDirection) return FOVread;
  if(dir==phaseDirection) return FOVphase;
  if(dir==sliceDirection) return FOVslice;
  return 0.0;
}

Geometry& Geometry::set_offset(direction dir,double offset) {
  if(dir==readDirection) offsetRead=offset;
  if(dir==phaseDirection) offsetPhase=offset;
  if(dir==sliceDirection) offsetSlice=offset;
  update();
  return *this;
}

double Geometry::get_offset(direction dir) const {
  if(dir==readDirection)  return offsetRead;
  if(dir==phaseDirection) return offsetPhase;
  if(dir==sliceDirection) return offsetSlice;
  return 0.0;
}



Geometry& Geometry::set_nSlices(unsigned int nslices) {nSlices=nslices; update(); return *this;}
Geometry& Geometry::set_sliceThickness(double thick) {sliceThickness=thick; update(); return *this;}
Geometry& Geometry::set_sliceDistance(double dist) {sliceDistance=dist; update(); return *this;}


dvector Geometry::get_readVector_inplane() const {
  Log<Para> odinlog(this,"get_readVector_inplane");
  double phiplus=deg2rad(azimutAngle)+0.5*PII;
  ODINLOG(odinlog,normalDebug) << "azimutAngle=" << double(azimutAngle) << STD_endl;
  ODINLOG(odinlog,normalDebug) << "phiplus=" << phiplus << STD_endl;
  dvector result(3);
  result[xAxis]=sin(phiplus);
  result[yAxis]=0.0;
  result[zAxis]=cos(phiplus);
  ODINLOG(odinlog,normalDebug) << "=" << result.printbody() << STD_endl;
  return result;
}

dvector Geometry::get_phaseVector_inplane() const {
  Log<Para> odinlog(this,"get_phaseVector_inplane");
  double tetaplus=deg2rad(heightAngle)+0.5*PII;
  double phi=deg2rad(azimutAngle);
  dvector result(3);
  result[xAxis]=cos(tetaplus)*sin(phi);
  result[yAxis]=sin(tetaplus);
  result[zAxis]=cos(tetaplus)*cos(phi);
  ODINLOG(odinlog,normalDebug) << "=" << result.printbody() << STD_endl;
  return result;
}

dvector Geometry::get_readVector() const {
  double cos_inplane=cos(deg2rad(inplaneAngle));
  double sin_inplane=sin(deg2rad(inplaneAngle));
  return cos_inplane*get_readVector_inplane()-sin_inplane*get_phaseVector_inplane();
}

dvector Geometry::get_phaseVector() const {
  return sin(deg2rad(inplaneAngle))*get_readVector_inplane()+cos(deg2rad(inplaneAngle))*get_phaseVector_inplane();
}



dvector Geometry::get_sliceVector() const {
  double teta=deg2rad(heightAngle);
  double phi=deg2rad(azimutAngle);
  dvector result(3);
  result[xAxis]=cos(teta)*sin(phi);
  result[yAxis]=sin(teta);
  result[zAxis]=cos(teta)*cos(phi);
  return pow(-1.0,reverseSlice)*result;
}


dvector Geometry::get_sliceOffsetVector() const {
  dvector result(nSlices);
  double range=(double)(nSlices-1)*sliceDistance;
  result.fill_linear(offsetSlice-0.5*range,offsetSlice+0.5*range);
  return result;
}


darray Geometry::get_cornerPoints(const Geometry& background, unsigned int backgrslice) const {
  Log<Para> odinlog(this,"get_cornerPoints");

  unsigned int n_boundaries_slice=n_boundaries;
  if(Mode==slicepack) n_boundaries_slice=1;

  darray result(nSlices,n_boundaries,n_boundaries,n_boundaries_slice,n_axes);

  dvector sliceoffset=get_sliceOffsetVector();
  ODINLOG(odinlog,normalDebug) << "sliceoffset=" << sliceoffset.printbody() << STD_endl;

  dvector slicevector(3);
  dvector readvector(3);
  dvector phasevector(3);
  dvector totalvector(3);

  double signread,signphase,signslice;

  double backgrslicedelta=background.get_sliceOffsetVector()[backgrslice]-background.get_offset(sliceDirection);
  ODINLOG(odinlog,normalDebug) << "backgrslicedelta[" << backgrslice << "]=" << backgrslicedelta << STD_endl;

  ODINLOG(odinlog,normalDebug) << background.get_label() << "=" << background.get_gradrotmatrix().print() << STD_endl;


  for(unsigned int j=0;j<sliceoffset.length();j++) { // loop over slices

    for(unsigned int iread=0;iread<n_boundaries;iread++) {
      for(unsigned int iphase=0;iphase<n_boundaries;iphase++) {
        for(unsigned int islice=0;islice<n_boundaries_slice;islice++) {

          // upper or lower boundary
          signread=2.0*(double)iread-1.0;
          signphase=2.0*(double)iphase-1.0;
          signslice=2.0*(double)islice-1.0;

          readvector=(signread*0.5*FOVread+offsetRead)*get_readVector();
          phasevector=(signphase*0.5*FOVphase+offsetPhase)*get_phaseVector();

          if(Mode==slicepack) slicevector=sliceoffset[j]*get_sliceVector();
          if(Mode==voxel_3d)  slicevector=(signslice*0.5*FOVslice+offsetSlice)*get_sliceVector();
          ODINLOG(odinlog,normalDebug) << "slicevector(" << j << ")=" << slicevector.printbody() << STD_endl;

          totalvector=slicevector+readvector+phasevector;
          ODINLOG(odinlog,normalDebug) << "pre_transform =" << totalvector.printbody() << STD_endl;

          totalvector=background.transform(totalvector,true);
          ODINLOG(odinlog,normalDebug) << "post_transform=" << totalvector.printbody() << STD_endl;

          result(j,iread,iphase,islice,xAxis)=totalvector[xAxis];
          result(j,iread,iphase,islice,yAxis)=totalvector[yAxis];
          result(j,iread,iphase,islice,zAxis)=totalvector[zAxis]-backgrslicedelta;
        }
      }
    }
  }

  return result;
}


Geometry& Geometry::set_orientation(sliceOrientation orientation) {
  reverseSlice=false;
  if(orientation==sagittal) {
    heightAngle=0.0;
    azimutAngle=90.0;
    inplaneAngle=0.0;
  }
  if(orientation==coronal) {
    heightAngle=90.0;
    azimutAngle=0.0;
    inplaneAngle=-90.0;
//    inplaneAngle=0.0;
  }
  if(orientation==axial) {
    heightAngle=0.0;
    azimutAngle=0.0;
    inplaneAngle=0.0;
  }
  update();
  return *this;
}

sliceOrientation Geometry::get_slice_orientation(const dvector& svec) {
  Log<Para> odinlog("Geometry","get_slice_orientation");
  sliceOrientation result=axial;
  double xabs=fabs(svec[xAxis]);
  double yabs=fabs(svec[yAxis]);
  double zabs=fabs(svec[zAxis]);
  if((yabs>=xabs) && (yabs>=zabs)) result=coronal;
  if((xabs>=yabs) && (xabs>=zabs)) result=sagittal;
  ODINLOG(odinlog,normalDebug) << "result("<< svec.printbody() << ")=" << result << STD_endl;
  return result;
}

void Geometry::get_orientation(double& heightAng, double& azimutAng, double& inplaneAng, bool& revSlice) const {
  heightAng=heightAngle;
  azimutAng=azimutAngle;
  inplaneAng=inplaneAngle;
  revSlice=reverseSlice;
}


Geometry& Geometry::set_orientation(double heightAng, double azimutAng, double inplaneAng, bool revSlice) {
  heightAngle=heightAng;
  azimutAngle=azimutAng;
  inplaneAngle=inplaneAng;
  reverseSlice=revSlice;
  update();
  return *this;
}

Geometry& Geometry::set_orientation_and_offset(const dvector& readvec, const dvector& phasevec, const dvector& slicevec, const dvector& centervec) {
  Log<Para> odinlog(this,"set_orientation_and_offset");

  dvector rvec(3);
  dvector pvec(3);
  dvector svec(3);

  // calculate length 1 vectors
  double inv_length;
  inv_length=secureInv(norm3(readvec[0], readvec[1], readvec[2]));  rvec=inv_length*readvec;
  inv_length=secureInv(norm3(phasevec[0],phasevec[1],phasevec[2])); pvec=inv_length*phasevec;
  inv_length=secureInv(norm3(slicevec[0],slicevec[1],slicevec[2])); svec=inv_length*slicevec;

  // check orthogonality
  double deviation=0.0;
  deviation=STD_max(deviation, fabs((rvec*pvec).sum()));
  deviation=STD_max(deviation, fabs((pvec*svec).sum()));
  deviation=STD_max(deviation, fabs((svec*rvec).sum()));
  if(deviation>ODIN_GEO_CHECK_LIMIT) {
    ODINLOG(odinlog,errorLog) << "Non-orthogonal read/phase/slice-system provided, deviation=" << deviation << STD_endl;
    return *this;
  }


  // calculate handness
  dvector rp_crossproduct(3);
  rp_crossproduct[0] = rvec[1]*pvec[2] - rvec[2]*pvec[1];
  rp_crossproduct[1] = rvec[2]*pvec[0] - rvec[0]*pvec[2];
  rp_crossproduct[2] = rvec[0]*pvec[1] - rvec[1]*pvec[0];
  // check handness of coordinate system
  double handness_product=(svec*rp_crossproduct).sum();
  ODINLOG(odinlog,normalDebug) << "handness_product=" << handness_product << STD_endl;
  reverseSlice=(handness_product<0.0);


  // calculate polar orientation angles, use rp_crossproduct which has positive handness
  azimutAngle=180.0/PII*atan2(rp_crossproduct[0],rp_crossproduct[2]);
  heightAngle=180.0/PII*asin(rp_crossproduct[1]);
  ODINLOG(odinlog,normalDebug) << "azimutAngle/heightAngle=" << azimutAngle << "/" << heightAngle << STD_endl;


  // calculate inplane rotation
  dvector rvec_inplane(get_readVector_inplane());
  dvector pvec_inplane(get_phaseVector_inplane());

  double rr_scalarproduct=(rvec*rvec_inplane).sum();
  double rp_scalarproduct=(rvec*pvec_inplane).sum();
  if(rr_scalarproduct>1.0) rr_scalarproduct=1.0; if(rr_scalarproduct<-1.0) rr_scalarproduct=-1.0;
  if(rp_scalarproduct>1.0) rp_scalarproduct=1.0; if(rp_scalarproduct<-1.0) rp_scalarproduct=-1.0;
  ODINLOG(odinlog,normalDebug) << "rr/rp_scalarproduct=" << rr_scalarproduct << "/" << rp_scalarproduct << STD_endl;

  inplaneAngle=180.0/PII*atan2(-rp_scalarproduct,rr_scalarproduct); // clock-wise angle
  ODINLOG(odinlog,normalDebug) << "inplaneAngle=" << inplaneAngle << STD_endl;


  // calculate spatial offsets
  offsetRead =(centervec*rvec).sum(); //scalar product
  offsetPhase=(centervec*pvec).sum(); //scalar product
  offsetSlice=(centervec*svec).sum(); //scalar product
  ODINLOG(odinlog,normalDebug) << "offset=" << offsetRead << "/" << offsetPhase << "/" << offsetSlice << STD_endl;

  update();
  return *this;
}

dvector Geometry::get_center() const {
  return get_offset(readDirection) *get_readVector()
       + get_offset(phaseDirection)*get_phaseVector()
       + get_offset(sliceDirection)*get_sliceVector();
}



RotMatrix Geometry::get_gradrotmatrix(bool transpose) const {
  Log<Para> odinlog(this,"get_gradrotmatrix");
  RotMatrix result;

  dvector perpvector(3);
//  double s1,s2,s3,s4,s5,s6;
  unsigned int i;

  perpvector=get_readVector();
  ODINLOG(odinlog,normalDebug) << "perpRead=" << perpvector.printbody() << STD_endl;
  for(i=0;i<3;i++) {
    if(transpose) result[0][i]=perpvector[i];
    else result[i][0]=perpvector[i];
  }

/*
  s1=perpvector[0];
  s2=perpvector[2];
  s3=perpvector[1];
  s4=perpvector[2];
  s5=perpvector[1];
  s6=perpvector[0];
*/

  perpvector=get_phaseVector();
  ODINLOG(odinlog,normalDebug) << "perpPhase=" << perpvector.printbody() << STD_endl;
  for(i=0;i<3;i++) {
    if(transpose) result[1][i]=perpvector[i];
    else result[i][1]=perpvector[i];
  }

/*
  s1*=perpvector[1];
  s2*=perpvector[0];
  s3*=perpvector[2];
  s4*=perpvector[1];
  s5*=perpvector[0];
  s6*=perpvector[2];
*/

  perpvector=get_sliceVector();
  ODINLOG(odinlog,normalDebug) << "perpSlice=" << perpvector.printbody() << STD_endl;
  for(i=0;i<3;i++) {
    if(transpose) result[2][i]=perpvector[i];
    else result[i][2]=perpvector[i];
  }

/*
  s1*=perpvector[2];
  s2*=perpvector[1];
  s3*=perpvector[0];
  s4*=perpvector[0];
  s5*=perpvector[2];
  s6*=perpvector[1];

  ODINLOG(odinlog,normalDebug) << "determinante=" << s1+s2+s3-s4-s5-s6 << STD_endl;

  ODINLOG(odinlog,normalDebug) << "0=" << result(0,0) << " " << result(0,1) << " " << result(0,2) << STD_endl;
  ODINLOG(odinlog,normalDebug) << "1=" << result(1,0) << " " << result(1,1) << " " << result(1,2) << STD_endl;
  ODINLOG(odinlog,normalDebug) << "2=" << result(2,0) << " " << result(2,1) << " " << result(2,2) << STD_endl;
*/

  return result;
}



dvector Geometry::transform(const dvector& rpsvec, bool inverse) const {
  Log<Para> odinlog(this,"transform");
  int ichan;
  dvector result(3);
  if(rpsvec.size()!=3) {
    ODINLOG(odinlog,errorLog) << "Size of input vector != 3" << STD_endl;
    return result;
  }

  if(!cache_up2date || (inverse!=inv_trans_cache)) {
    for(ichan=0; ichan<n_directions; ichan++) offset_cache[ichan]=get_offset(direction(ichan));
    RotMatrix rm=get_gradrotmatrix(inverse);
    for(int i=0; i < 3; i++)
      for(int j=0; j < 3; j++)
        rotmat_cache[i][j]=rm[i][j];
    inv_trans_cache=inverse;
    cache_up2date=true;
  }

  result=0;

  float inputvec[3];
  if(inverse) for(ichan=0; ichan<n_directions; ichan++) inputvec[ichan]=rpsvec[ichan];
  else        for(ichan=0; ichan<n_directions; ichan++) inputvec[ichan]=rpsvec[ichan]+offset_cache[ichan];

  for(int i=0; i < 3; i++)
    for(int j=0; j < 3; j++)
      result[i]+=rotmat_cache[i][j]*inputvec[j];

  if(inverse) for(ichan=0; ichan<n_directions; ichan++) result[ichan]-=offset_cache[ichan];

  return result;
}


Geometry& Geometry::transpose_inplane(bool reverse_read, bool reverse_phase) {

  double sign_phase= pow(-1.0,double(reverse_phase));
  double sign_read = pow(-1.0,double(reverse_read));

  // swap read and phase vector, the center of the area remains the same
  set_orientation_and_offset( sign_phase*get_phaseVector(), sign_read*get_readVector(), get_sliceVector(), get_center());

  // swap FOV
  double fovread=get_FOV(readDirection);
  double fovphase=get_FOV(phaseDirection);
  set_FOV(readDirection,fovphase);
  set_FOV(phaseDirection,fovread);

  return *this;
}




void  Geometry::append_all_members() {
  Log<Para> odinlog(this,"append_all_members");

  LDRblock::clear();

  append_member(Mode,"Mode");
  append_member(Reset,"Reset");

  append_member(FOVread,"FOVread");
  append_member(offsetRead,"offsetRead");
  append_member(FOVphase,"FOVphase");
  append_member(offsetPhase,"offsetPhase");
  append_member(FOVslice,"FOVslice");
  append_member(offsetSlice,"offsetSlice");

  append_member(nSlices,"nSlices");
  append_member(sliceThickness,"sliceThickness");
  append_member(sliceDistance,"sliceDistance");

  append_member(heightAngle,"heightAngle");
  append_member(azimutAngle,"azimutAngle");
  append_member(inplaneAngle,"inplaneAngle");
  append_member(reverseSlice,"reverseSlice");

  append_member(Transpose,"Transpose");
}





////////////////////////////////////////////////////////////////////////////

#ifndef NO_UNIT_TEST
class GeometryTest : public UnitTest {

 public:
  GeometryTest() : UnitTest("Geometry") {}

 private:
  bool check() const {
    Log<UnitTest> odinlog(this,"check");

    int i;

    Geometry ia;

    // testing self-consistency of orientation with some arbitrary angles and offsets
    ia.set_orientation(-66.7, 78.2, -124.7, true);
    ia.set_offset(readDirection,22.7);
    ia.set_offset(phaseDirection,-5.9);
    ia.set_offset(sliceDirection,99.9);

    RotMatrix matrix_ang(ia.get_gradrotmatrix());
    dvector offset(3);
    for(i=0; i<3; i++) offset[i]=ia.get_offset(direction(i));

    //////////////////////////////////////////////////////
    // Self-consistency check of set_orientation_and_offset

    Geometry ia2;
    ia2.set_orientation_and_offset(ia.get_readVector(),ia.get_phaseVector(),ia.get_sliceVector(),ia.get_center());

    RotMatrix matrix_vec(ia2.get_gradrotmatrix());
    if(!(matrix_ang==matrix_vec)) {
      ODINLOG(odinlog,errorLog) << "  matrix_ang=" << matrix_ang.print() << STD_endl
               << "  matrix_vec=" << matrix_vec.print() << STD_endl;
      return false;
    }

    dvector offset2(3);
    for(i=0; i<3; i++) offset2[i]=ia2.get_offset(direction(i));
    if((offset-offset2).sum()>ODIN_GEO_CHECK_LIMIT) {
      ODINLOG(odinlog,errorLog) << "  offset =" << offset  << STD_endl
               << "  offset2=" << offset2 << STD_endl;
      return false;
    }

    //////////////////////////////////////////////////////
    // Check othogonality of rotation matrix

    RotMatrix matprod=ia.get_gradrotmatrix()*ia.get_gradrotmatrix(true);
    RotMatrix unitmat;
    if(!(matprod==unitmat)) {
      ODINLOG(odinlog,errorLog) << "  matprod=" << matprod.print() << STD_endl
               << "  unitmat=" << unitmat.print() << STD_endl;
      return false;
    }



    //////////////////////////////////////////////////////
    // Check whether 'transform' preserves length and perpendicularity

    double norm1_expected,norm2_expected;

    dvector origin(3); origin=0.0;

    dvector testvec1(3);
    testvec1[0]=norm1_expected=5.0;
    testvec1[1]=0.0;
    testvec1[2]=0.0;

    dvector testvec2(3);
    testvec2[0]=0.0;
    testvec2[1]=norm2_expected=6.0;
    testvec2[2]=0.0;

    dvector origin_tr=ia.transform(origin);
    dvector testvec1_tr=ia.transform(testvec1)-origin_tr;
    dvector testvec2_tr=ia.transform(testvec2)-origin_tr;

    double norm1=norm3(testvec1_tr[0],testvec1_tr[1],testvec1_tr[2]);

    // check length after 'transform'
    if(fabs(norm1-norm1_expected)>ODIN_GEO_CHECK_LIMIT) {
      ODINLOG(odinlog,errorLog) << "  testvec1/testvec1_tr =" << testvec1.printbody() << "/" << testvec1_tr.printbody() << STD_endl
               << "  norm_expected1/norm1=" << norm1_expected << "/" << norm1 << STD_endl;
      return false;
    }

    // check scalar product after 'transform'
    double scalprod=(testvec1_tr*testvec2_tr).sum();
    if(fabs(scalprod)>ODIN_GEO_CHECK_LIMIT) {
      ODINLOG(odinlog,errorLog) << "  scalprod =" << scalprod << STD_endl;
      return false;
    }


    //////////////////////////////////////////////////////
    // Testing == operator

    Geometry geo1;
    Geometry geo2;
    if(!(geo1==geo2)) {
      ODINLOG(odinlog,errorLog) << "Geometry::operator == (equal) failed: geo1=" << geo1 << "geo2=" << geo2 << STD_endl;
      return false;
    }

    geo1.set_nSlices(3);
    if(geo1==geo2) {
      ODINLOG(odinlog,errorLog) << "Geometry::operator == (unequal) failed: geo1=" << geo1 << "geo2=" << geo2 << STD_endl;
      return false;
    }

    //////////////////////////////////////////////////////
    // Testing copy constructor

    Geometry geo3(geo1);
    if(!(geo1==geo3)) {
      ODINLOG(odinlog,errorLog) << "Geometry(const Geometry&) failed: geo1=" << geo1 << "geo3=" << geo3 << STD_endl;
      return false;
    }




    return true;
  }



};

void alloc_GeometryTest() {new GeometryTest();} // create test instance
#endif
