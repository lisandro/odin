/***************************************************************************
                          sample.h  -  description
                             -------------------
    begin                : Mon Jul 11 2005
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SAMPLE_H
#define SAMPLE_H

#include <odinpara/ldrblock.h>
#include <odinpara/ldrnumbers.h>
#include <odinpara/ldrarrays.h>
#include <odinpara/odinpara.h>

/**
  * @addtogroup odinpara
  * @{
  */



/**
  * Dimension inidices of the 5-dim sample grid
  * - frameDim: time dimension
  * - freqDim: frequency direction
  * - zDim: axial direction of the scanner coordinate system
  * - yDim: 2nd transversal direction of the scanner coordinate system
  * - xDim: 1st transversal direction of the scanner coordinate system
  */
enum sampleDim { frameDim=0, freqDim, zDim, yDim, xDim, n_sampleDim };


////////////////////////////////////////////////////////////////////////////////////////////////////


/**
  * \brief Virtual Sample for Simulation
  *
  * Class to store properties of a virtual sample which is used as input to
  * simulation. The arrays are 4-dimensional: one frequency dimension
  * and 3 spatial dimensions. Coordinates are in the laboratory frame of
  * reference.
  */
class Sample : public LDRblock {

 public:

/**
  * Constructs a sample consisting of single voxel with the given label and the parameters:
  * - uniformFOV:  A uniform FOV is used in each spatial dimension
  * - uniformT1T2: Uniform relaxation constants T1 and T2 are used, set_T1/2map has no effect
  */
  Sample(const STD_string& label="unnamedSample", bool uniformFOV=true, bool uniformT1T2=false);

/**
  * Constructs a copy of 'ss'
  */
  Sample(const Sample& ss);

/**
  * Assignment operator that makes this Sample become a copy of 'ss'
  */
  Sample& operator = (const Sample& ss);

/**
  * Sets the uniform FOV of the sample
  */
  Sample& set_FOV(float fov);

/**
  * Sets the FOV of the sample in the given direction
  */
  Sample& set_FOV(axis direction, float fov);

/**
  * Returns the FOV of the sample in the given direction
  */
  float get_FOV(axis direction) const;

/**
  * Sets the spatial offset in the given direction
  */
  Sample& set_spatial_offset(axis direction, float offs) {offset[direction]=offs; return *this;}

/**
  * Returns the spatial offset in the given direction
  */
  float get_spatial_offset(axis direction) const {return offset[direction];}

/**
  * Sets the frequency range
  */
  Sample& set_freqrange(float range) {freqrange=range; return *this;}

/**
  * Returns the frequency range
  */
  float get_freqrange() const {return freqrange;}

/**
  * Sets the frequency offset
  */
  Sample& set_freqoffset(float offs) {freqoffset=offs; return *this;}

/**
  * Returns the frequency offset
  */
  float get_freqoffset() const {return freqoffset;}


/**
  * Sets a vector of time intervals to cycle through frames periodically.
  * For maps with a time dimension, the index of the current interval
  * will also be used to index the time dimension of the map.
  */
  Sample& set_frame_durations(const dvector& intervals) {frameDurations=intervals; return *this;}

/**
  * Returns the vector of time intervals of the frames.
  */
  const dvector& get_frame_durations() const {return frameDurations;}


/**
  * Resize the sample in the four dimensions according to the given sizes:
  * - framesize: Number of frames (time intervals)
  * - freqsize: Number of frequency intervals
  * - zsize:  Spatial extent in the axial direction of the scanner coordinate system
  * - ysize:  Spatial extent in the 2nd transversal direction of the scanner coordinate system
  * - xsize:  Spatial extent in the 1st transversal direction of the scanner coordinate system
  */
  Sample& resize(unsigned int framesize, unsigned int freqsize, unsigned int zsize, unsigned int ysize, unsigned int xsize);

/**
  * Returns the extent vector
  */
  const ndim& get_extent() const {return spinDensity.get_extent();}

/**
  * Sets a uniform longitudinal relaxation time
  */
  Sample& set_T1(float relaxation_time) {T1=relaxation_time; haveT1map=false; return *this;}

/**
  * Sets a uniform transverse relaxation time
  */
  Sample& set_T2(float relaxation_time) {T2=relaxation_time; haveT2map=false; return *this;}

/**
  * Sets an array for the longitudinal relaxation time
  */
  Sample& set_T1map(const farray& t1map);

/**
  * Returns the array for the longitudinal relaxation time
  */
  const farray& get_T1map() const;

/**
  * Sets an array for the transverse relaxation time
  */
  Sample& set_T2map(const farray& t2map);

/**
  * Returns the array for the transverse relaxation time
  */
  const farray& get_T2map() const;

/**
  * Sets an array for the relative frequency offset (in ppm)
  */
  Sample& set_ppmMap(const farray& ppmmap);

/**
  * Returns an array for the relative frequency offset (in ppm)
  */
  const farray& get_ppmMap() const;

/**
  * Sets an array for the spin density of each voxel
  */
  Sample& set_spinDensity(const farray& sd);

/**
  * Returns an array for the spin density of each voxel
  */
  const farray& get_spinDensity() const;

/**
  * Sets the diffusion coefficient map
  */
  Sample& set_DcoeffMap(const farray& dmap);

/**
  * Returns the diffusion coefficient map
  */
  const farray& get_DcoeffMap() const;

/**
  * Updates all parameter relations
  */
  Sample& update();


  // overwriting virtual functions from LDRbase
  int load(const STD_string& filename, const LDRserBase& serializer=LDRserJDX());


 private:
  friend class SeqSimMagsi;

  int append_all_members();

  bool check_and_correct(const char* mapname, const farray& srcmap, farray& dstmap);


  LDRfloat FOVall;
  LDRtriple FOV;
  bool uniFOV;
  LDRtriple offset;

  LDRfloat freqrange;
  LDRfloat freqoffset;

  LDRdoubleArr frameDurations;

  mutable LDRfloatArr spinDensity;

  bool uniT1T2;
  LDRfloat T1;
  LDRfloat T2;
  mutable LDRfloatArr T1map;
  mutable LDRfloatArr T2map;
  mutable bool haveT1map;
  mutable bool haveT2map;

  mutable LDRfloatArr ppmMap;
  mutable bool have_ppmMap;

  mutable LDRfloatArr DcoeffMap;
  mutable bool have_DcoeffMap;

};


/** @}
  */

#endif
