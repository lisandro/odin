/***************************************************************************
                          ldrfilter.h  -  description
                             -------------------
    begin                : Thu Jul 6 2006
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#ifndef LDRFILTER_H
#define LDRFILTER_H

#include <odinpara/ldrfunction.h>

/**
  * @addtogroup ldr
  * @{
  */

/**
  *
  * Wrapper class for pulse shape functions.
  */
class LDRfilter : public LDRfunction,  public StaticHandler<LDRfilter> {

 public:
  LDRfilter(const STD_string& ldrlabel="unnamedLDRfilter") : LDRfunction(filterFunc,ldrlabel) {}

  LDRfilter(const LDRfilter& jf) : LDRfunction(jf) {}

  LDRfilter& operator = (const LDRfilter& jf) {LDRfunction::operator = (jf); return *this;}

  float calculate (float rel_kradius) const {
    if(allocated_function) return allocated_function->calculate_filter(rel_kradius);
    else return 0.0;
  }


  static void init_static();
  static void destroy_static();

};


/** @}
  */

#endif


