#include "ldrfunction.h"


LDRfunctionPlugIn& LDRfunctionPlugIn::register_function(funcType type, funcMode mode) {
  LDRfunctionEntry entry(this,type,mode);
  LDRfunction dummy(type,"dummy"); // make sure that LDRfunction::init_static is called once
  dummy.registered_functions->push_back(entry);
  return *this;
}


kspace_coord LDRfunctionPlugIn::coord_retval;
//shape_vals   LDRfunctionPlugIn::shape_retval;
shape_info   LDRfunctionPlugIn::shape_info_retval;
traj_info    LDRfunctionPlugIn::traj_info_retval;


////////////////////////////////////////

bool LDRfunctionEntry::operator == (const LDRfunctionEntry& jfe) const {
  bool result=true;
  if(plugin!=jfe.plugin) result=false;
  if(type!=jfe.type) result=false;
  if(mode!=jfe.mode) result=false;
  return result;
}

bool LDRfunctionEntry::operator < (const LDRfunctionEntry& jfe) const {
  bool result=true;
  if(!(plugin<jfe.plugin)) result=false;
  if(!(type<jfe.type)) result=false;
  if(!(mode<jfe.mode)) result=false;
  return result;
}

////////////////////////////////////////

LDRfunction::LDRfunction(funcType function_type, const STD_string& ldrlabel)
 :  mode(funcMode(0)), allocated_function(0), type(function_type) {
  Log<LDRcomp> odinlog(ldrlabel.c_str(),"LDRfunction(funcType ...)");
  set_label(ldrlabel);
  set_function(0);
}


LDRfunction::LDRfunction(const LDRfunction& jf) : allocated_function(0), type(jf.type) {
  Log<LDRcomp> odinlog(this,"LDRfunction(const LDRfunction&)");
  LDRfunction::operator = (jf);
}



LDRfunction& LDRfunction::operator = (const LDRfunction& jf) {
  LDRbase::operator = (jf);
  Log<LDRcomp> odinlog(this,"operator = (...)");
  if(jf.type!=type) {
    ODINLOG(odinlog,normalDebug) << "wrong functype" << STD_endl;
    return *this;
  }
  mode=jf.mode;
  if(jf.allocated_function) {
    LDRfunctionPlugIn* pi=jf.allocated_function->clone();
    pi->copy_ldr_vals(*jf.allocated_function);
    new_plugin(pi);
  }
  return *this;
}


LDRfunction& LDRfunction::set_function_mode(funcMode newmode) {
  if(mode==newmode) return *this;
  mode=newmode;
  new_plugin(0);
  set_function(0);
  return *this;
}


svector LDRfunction::get_alternatives() const {
  svector result;
  for(STD_list<LDRfunctionEntry>::const_iterator it=registered_functions->begin(); it!=registered_functions->end(); ++it) {
    if( it->type==type && it->mode==mode ) result.push_back(it->plugin->get_label());
  }
  return result;
}


LDRfunction& LDRfunction::set_function(const STD_string& funclabel) {
  Log<LDRcomp> odinlog(this,"set_function");

  if( allocated_function && (funclabel==allocated_function->get_label())) {
    ODINLOG(odinlog,normalDebug) << "function " << funclabel << " already set" << STD_endl;
    return *this;
  }

  STD_list<LDRfunctionEntry>::const_iterator it;

  for(it=registered_functions->begin(); it!=registered_functions->end(); ++it) {
    LDRfunctionPlugIn* plugin_template=it->plugin;
    ODINLOG(odinlog,normalDebug) << "checking " << funclabel << "/" << plugin_template->get_label() << STD_endl;
    if( it->type==type && it->mode==mode ) {
      ODINLOG(odinlog,normalDebug) << "type and mode match" << STD_endl;
      if(funclabel==plugin_template->get_label()) {
        ODINLOG(odinlog,normalDebug) << "label match" << STD_endl;
        new_plugin(plugin_template->clone());
        break;
      }
    }
  }
  return *this;
}


static STD_string nofunc("none");

const STD_string& LDRfunction::get_function_label(unsigned int index) const {
  Log<LDRcomp> odinlog(this,"get_function_label");

  unsigned int i=0;
  for(STD_list<LDRfunctionEntry>::const_iterator it=registered_functions->begin(); it!=registered_functions->end(); ++it) {
    if( it->type==type && it->mode==mode ) {
      ODINLOG(odinlog,normalDebug) << "type and mode match" << STD_endl;
      if(i==index) {
        ODINLOG(odinlog,normalDebug) << "index match" << STD_endl;
        return it->plugin->get_label();
      }
      i++;
    }
  }
  return nofunc;
}


LDRfunction& LDRfunction::set_function(unsigned int index) {
  Log<LDRcomp> odinlog(this,"set_function");

  if( allocated_function && index==get_function_index() ) {
    return *this;
    ODINLOG(odinlog,normalDebug) << "function " << index << " already set" << STD_endl;
  }

  unsigned int i=0;
  for(STD_list<LDRfunctionEntry>::const_iterator it=registered_functions->begin(); it!=registered_functions->end(); ++it) {
    LDRfunctionPlugIn* plugin_template=it->plugin;
    ODINLOG(odinlog,normalDebug) << "checking " << index << "/" << plugin_template->get_label() << STD_endl;
    if( it->type==type && it->mode==mode ) {
      ODINLOG(odinlog,normalDebug) << "type and mode match" << STD_endl;
      if(i==index) {
        ODINLOG(odinlog,normalDebug) << "index match" << STD_endl;
        new_plugin(plugin_template->clone());
        break;
      }
      i++;
    }
  }
  return *this;
}


unsigned int LDRfunction::get_function_index() const {
  unsigned int result=0;
  unsigned int i=0;

  if(!allocated_function) return result;

  STD_list<LDRfunctionEntry>::const_iterator it;

  for(it=registered_functions->begin(); it!=registered_functions->end(); ++it) {
    if( it->type==type && it->mode==mode ) {
      if( STD_string(allocated_function->get_label()) == it->plugin->get_label() ) {result=i; break;}
      i++;
    }
  }
  return result;
}

LDRblock* LDRfunction::get_funcpars_block() {
  Log<LDRcomp> odinlog(this,"get_funcpars_block");
  if(allocated_function)  ODINLOG(odinlog,normalDebug) << "allocated_function=" << allocated_function->get_label() << STD_endl;
  else  ODINLOG(odinlog,normalDebug) << "allocated_function=0" << STD_endl;
  return allocated_function;
}



LDRfunction& LDRfunction::set_funcpars(const svector& funcpars) {
  Log<LDRcomp> odinlog(this,"set_funcpars");
  if(funcpars.size()<1) return *this;
  set_function(funcpars[0]);
  if(allocated_function) {
    ODINLOG(odinlog,normalDebug) << "allocated_function=" << allocated_function->get_label() << STD_endl;

    unsigned int n_pars=allocated_function->LDRblock::numof_pars();

    unsigned int n_pars_min = funcpars.size()-1;
    if(n_pars<n_pars_min) n_pars_min = n_pars;

    for(unsigned int i=0;i<n_pars_min;i++) {
      STD_string valstring=funcpars[i+1];
      (*allocated_function)[i].parsevalstring(valstring);
    }
  }
  return *this;
}




svector LDRfunction::get_funcpars() const {
  Log<LDRcomp> odinlog(this,"get_funcpars");

  svector funcpars;

  if(allocated_function) {
    unsigned int n_pars=allocated_function->LDRblock::numof_pars();
    ODINLOG(odinlog,normalDebug) << "n_pars=" << n_pars << STD_endl;

    funcpars.resize(n_pars+1);
    funcpars[0]=allocated_function->get_label();
    for(unsigned int i=0;i<n_pars;i++) {
      funcpars[i+1]=(*allocated_function)[i].printvalstring();
    }
  }
  return funcpars;
}


bool LDRfunction::set_parameter(const STD_string& parameter_label, const STD_string& value) {
  if(allocated_function) return allocated_function->LDRblock::parseval(parameter_label,value);
  return false;
}



STD_string LDRfunction::get_parameter(const STD_string& parameter_label) const {
  STD_string result;
  if(allocated_function) result=allocated_function->LDRblock::printval(parameter_label);
  return result;
}

STD_string LDRfunction::get_function_name() const {
  STD_string result("noFunction");
  if(allocated_function) result=allocated_function->get_label();
  return result;
}

const STD_string& LDRfunction::get_funcdescription() const {
  if(allocated_function) return allocated_function->get_description();
  return LDRbase::get_description();
}

bool LDRfunction::parsevalstring (const STD_string& parstring, const LDRserBase*) {
  Log<LDRcomp> odinlog(this,"parsevalstring");

  svector funcpars;

  STD_string argstring=extract(parstring,"(",")",true);

  if(argstring!="") {
    ODINLOG(odinlog,normalDebug) << "argstring=" << argstring << STD_endl;
    funcpars.push_back(extract(parstring,"","("));
    argstring=shrink(argstring);
    svector args=tokens(argstring,',','(',')');
    for(unsigned int i=0;i<args.size();i++) funcpars.push_back(args[i]);
  } else{
    funcpars.push_back(parstring); // No function args
  }

  ODINLOG(odinlog,normalDebug) << "funcpars=" << funcpars.printbody() << STD_endl;
  set_funcpars(funcpars);

  return true;
}


STD_string LDRfunction::printvalstring(const LDRserBase*) const {
  Log<LDRcomp> odinlog(this,"printvalstring");

  STD_string result;

  if(allocated_function) {
    svector pars=get_funcpars();

//    ODINLOG(odinlog,normalDebug) << "pars=" << pars.printbody() << STD_endl;

    unsigned int npars=pars.size();
    if(npars) result+=pars[0];
    if(npars>1) {
      result+="(";
      for(unsigned int i=1;i<npars;i++) {
        STD_string item=pars[i];
        result+=pars[i];
        if(i!=npars-1) result+=",";
      }
      result+=")";
    }
  } else result="noFunction";
  return result;
}


void LDRfunction::init_static() {
  registered_functions=new STD_list<LDRfunctionEntry>;
}


void LDRfunction::destroy_static() {

  // plugin might be used for different modes so we will make sure that delete is only called once for each pointer
  STD_list<LDRfunctionPlugIn*> tobedeleted;
  for(STD_list<LDRfunctionEntry>::iterator it=registered_functions->begin(); it!=registered_functions->end(); ++it) {
    tobedeleted.push_back(it->plugin);
  }
  tobedeleted.sort();
  tobedeleted.unique();

  for(STD_list<LDRfunctionPlugIn*>::iterator delit=tobedeleted.begin(); delit!=tobedeleted.end(); ++delit) {
    delete (*delit);
  }
  delete registered_functions;
}


void LDRfunction::new_plugin(LDRfunctionPlugIn* pi) {
  Log<LDRcomp> odinlog(this,"new_plugin");

  if(allocated_function) delete allocated_function;

  if(pi) {
    ODINLOG(odinlog,normalDebug) << "plugin=" << pi->get_label() << STD_endl;
  }

  allocated_function=pi;
}

EMPTY_TEMPL_LIST bool StaticHandler<LDRfunction>::staticdone=false;

STD_list<LDRfunctionEntry>* LDRfunction::registered_functions=0;

