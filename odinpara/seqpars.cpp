#include "seqpars.h"

SeqPars::SeqPars(const STD_string& label) : LDRblock(label) {

  ExpDuration.set_parmode(noedit);
  ExpDuration.set_description("The overall duration of the sequence");
  ExpDuration.set_unit("min");

  Sequence="Unknown";
  Sequence.set_parmode(hidden);
  Sequence.set_description("The MR sequence used");

  AcquisitionStart.set_filemode(exclude).set_parmode(hidden);
  AcquisitionStart.set_unit(ODIN_TIME_UNIT).set_description("Starting time point of the sequence");

  MatrixSizeRead=128;
  MatrixSizeRead.set_cmdline_option("nx").set_description("Number of points in read direction");

  MatrixSizePhase=128;
  MatrixSizePhase.set_cmdline_option("ny").set_description("Number of points in phase direction");

  MatrixSizeSlice=1;
  MatrixSizeSlice.set_cmdline_option("nz").set_description("Number of points in slice direction");

  RepetitionTime=1000.0;
  RepetitionTime.set_unit(ODIN_TIME_UNIT).set_cmdline_option("tr").set_description("Time between consecutive excitations");

  NumOfRepetitions=1;
  NumOfRepetitions.set_cmdline_option("nr").set_description("Number of consecutive measurements");

  EchoTime=80.0;
  EchoTime.set_unit(ODIN_TIME_UNIT).set_cmdline_option("te").set_description("Time-to-echo of the sequence");

//  AcqSweepWidth=25.6; // fails as default on Siemens
  AcqSweepWidth=25.0;
  AcqSweepWidth.set_unit(ODIN_FREQ_UNIT).set_description("Receiver bandwidth");

  FlipAngle=90.0;
  FlipAngle.set_unit(ODIN_ANGLE_UNIT).set_description("Excitation flipangle");

  ReductionFactor=1;
  ReductionFactor.set_description("Reduction factor for parallel imaging");

  PartialFourier=0.0;
  PartialFourier.set_description("Partial Fourier acquisition in phase encoding direction (0.0 = full k-space, 1.0 = half k-space)");

  RFSpoiling=true;
  RFSpoiling.set_description("RF Spoiling by phase cycling");

  GradientIntro=true;
  GradientIntro.set_description("Gradient intro which will be played out prior to sequence");

  PhysioTrigger=false;
  PhysioTrigger.set_description("Pysiological triggering");


  append_all_members();
}


SeqPars& SeqPars::set_MatrixSize(direction dir,unsigned int size,parameterMode parmode) {
  Log<Para> odinlog(this,"set_MatrixSize");
  switch(dir){
    case readDirection: MatrixSizeRead=size; MatrixSizeRead.set_parmode(parmode);break;
    case phaseDirection: MatrixSizePhase=size; MatrixSizePhase.set_parmode(parmode);break;
    case sliceDirection: MatrixSizeSlice=size; MatrixSizeSlice.set_parmode(parmode);break;
    default:
      ODINLOG(odinlog,errorLog) << "Direction " << dir << " is not available." << STD_endl;
  }
  return *this;
}


unsigned int  SeqPars::get_MatrixSize(direction dir) const {
  unsigned int result=0;
  if(dir==readDirection) result=MatrixSizeRead;
  if(dir==phaseDirection) result=MatrixSizePhase;
  if(dir==sliceDirection) result=MatrixSizeSlice;
  return result;
}

SeqPars&  SeqPars::set_RepetitionTime(double time,parameterMode parmode) {
  RepetitionTime=time;
  RepetitionTime.set_parmode(parmode);
  return *this;
}

SeqPars& SeqPars::set_NumOfRepetitions(unsigned int times,parameterMode parmode) {
  NumOfRepetitions=times;
  NumOfRepetitions.set_parmode(parmode);
  return *this;
}


SeqPars&  SeqPars::set_EchoTime(double time,parameterMode parmode) {
  EchoTime=time;
  EchoTime.set_parmode(parmode);
  return *this;
}


SeqPars&  SeqPars::set_AcqSweepWidth(double sw,parameterMode parmode) {
  AcqSweepWidth=sw;
  AcqSweepWidth.set_parmode(parmode);
  return *this;
}


SeqPars&  SeqPars::set_FlipAngle(double fa,parameterMode parmode) {
  FlipAngle=fa;
  FlipAngle.set_parmode(parmode);
  return *this;
}


SeqPars&  SeqPars::set_ReductionFactor(unsigned int factor, parameterMode parmode) {
  ReductionFactor=factor;
  ReductionFactor.set_parmode(parmode);
  return *this;
}


SeqPars& SeqPars::set_PartialFourier(float factor, parameterMode parmode) {
  PartialFourier=factor;
  PartialFourier.set_parmode(parmode);
  return *this;
}


SeqPars& SeqPars::set_RFSpoiling(bool flag, parameterMode parmode) {
  RFSpoiling=flag;
  RFSpoiling.set_parmode(parmode);
  return *this;
}


SeqPars& SeqPars::set_GradientIntro(bool flag, parameterMode parmode) {
  GradientIntro=flag;
  GradientIntro.set_parmode(parmode);
  return *this;
}

SeqPars& SeqPars::set_PhysioTrigger(bool flag, parameterMode parmode) {
  PhysioTrigger=flag;
  PhysioTrigger.set_parmode(parmode);
  return *this;
}


SeqPars& SeqPars::operator = (const SeqPars& pars2) {
  LDRblock::operator = (pars2);
  append_all_members();

  // copy only visible members of LDRblock
  copy_ldr_vals(pars2);

  return *this;
}



void SeqPars::append_all_members() {
  LDRblock::clear();
  append_member(ExpDuration,"ExpDuration");
  append_member(Sequence,"Sequence");
  append_member(AcquisitionStart,"AcquisitionStart");
  append_member(MatrixSizeRead,"MatrixSizeRead");
  append_member(MatrixSizePhase,"MatrixSizePhase");
  append_member(MatrixSizeSlice,"MatrixSizeSlice");
  append_member(RepetitionTime,"RepetitionTime");
  append_member(NumOfRepetitions,"NumOfRepetitions");
  append_member(EchoTime,"EchoTime");
  append_member(AcqSweepWidth,"AcqSweepWidth");
  append_member(FlipAngle,"FlipAngle");
  append_member(ReductionFactor,"ReductionFactor");
  append_member(PartialFourier,"PartialFourier");
  append_member(RFSpoiling,"RFSpoiling");
  append_member(GradientIntro,"GradientIntro");
  append_member(PhysioTrigger,"PhysioTrigger");
}


