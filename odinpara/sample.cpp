#include "sample.h"

Sample::Sample(const STD_string& label, bool uniformFOV, bool uniformT1T2) : LDRblock(label) {

  uniFOV=uniformFOV;
  uniT1T2=uniformT1T2;

  resize(1,1,1,1,1);

  FOVall=20.0; FOVall.set_minmaxval(0.0,ODIN_DEFAULT_FOV).set_unit(ODIN_SPAT_UNIT).set_description("Uniform Field-Of-View (spatial extent) in all spatial dimensions");

  FOV[xAxis]=20.0;
  FOV[yAxis]=20.0;
  FOV[zAxis]=20.0;
  FOV.set_unit(ODIN_SPAT_UNIT).set_description("Spatial extent");

  offset.set_unit(ODIN_SPAT_UNIT).set_description("Spatial offset");

  freqrange=10.0; freqrange.set_minmaxval(0.0,50.0).set_unit(ODIN_FREQ_UNIT).set_description("Extent in frequency dimension");

  freqoffset=0.0; freqoffset.set_minmaxval(-100.0,100.0).set_unit(ODIN_FREQ_UNIT).set_description("Frequency offset");


  frameDurations.set_parmode(hidden).set_unit(ODIN_TIME_UNIT).set_description("Time intervals to cycle through frames periodically");


  T1=0.0; T1.set_minmaxval(0.0,2000.0).set_unit(ODIN_TIME_UNIT).set_description("Uniform longitudinal relaxation constant");
  T2=0.0; T2.set_minmaxval(0.0,500.0).set_unit(ODIN_TIME_UNIT).set_description("Uniform transverse relaxation constant");

  T1map.set_filemode(compressed).set_parmode(hidden).set_description("Longitudinal relaxation constant as a function of position, frequency and time frame");
  T2map.set_filemode(compressed).set_parmode(hidden).set_description("Transverse relaxation constant as a function of position, frequency and time frame");
  ppmMap.set_filemode(compressed).set_parmode(hidden).set_description("Frequency offset as a function of position, frequency and time frame");
  spinDensity.set_filemode(compressed).set_parmode(hidden).set_description("Spin density as a function of position, frequency and time frame");
  DcoeffMap.set_filemode(compressed).set_parmode(hidden).set_description("Diffusion coefficient as a function of position, frequency and time frame");

  Sample::append_all_members();
}


Sample::Sample(const Sample& ss) {
  Sample::operator = (ss);
}


Sample& Sample::operator = (const Sample& ss) {
  LDRblock::operator = (ss);

  FOVall=ss.FOVall;
  FOV=ss.FOV;
  uniFOV=ss.uniFOV;
  offset=ss.offset;

  freqrange=ss.freqrange;
  freqoffset=ss.freqoffset;

  frameDurations=ss.frameDurations;

  uniT1T2=ss.uniT1T2;
  T1=ss.T1;
  T2=ss.T2;
  T1map=ss.T1map;
  T2map=ss.T2map;
  haveT1map=ss.haveT1map;
  haveT2map=ss.haveT2map;

  ppmMap=ss.ppmMap;
  have_ppmMap=ss.have_ppmMap;

  spinDensity=ss.spinDensity;

  DcoeffMap=ss.DcoeffMap;
  have_DcoeffMap=ss.have_DcoeffMap;

  Sample::append_all_members();

  return *this;
}


Sample& Sample::set_FOV(float fov) {
  FOVall=fov;
  FOV.farray::operator = (fov);
  uniFOV=true;
  return *this;
}


Sample& Sample::set_FOV(axis direction, float fov) {
  FOV[direction]=fov;
  uniFOV=false;
  return *this;
}


float Sample::get_FOV(axis direction) const {
  if(uniFOV) return FOVall;
  return FOV[direction];
}


Sample& Sample::resize(unsigned int framesize, unsigned int freqsize, unsigned int zsize, unsigned int ysize, unsigned int xsize) {
  Log<Para> odinlog(this,"resize");

  haveT1map=false;
  haveT2map=false;
  have_ppmMap=false;
  have_DcoeffMap=false;


  ndim newsize(n_sampleDim);
  newsize[frameDim]=framesize;
  newsize[freqDim]=freqsize;
  newsize[zDim]=zsize;
  newsize[yDim]=ysize;
  newsize[xDim]=xsize;

  ODINLOG(odinlog,normalDebug) << "newsize=" << newsize << STD_endl;
  ODINLOG(odinlog,normalDebug) << "spinDensity.get_extent()=" << spinDensity.get_extent() << STD_endl;

  // leave spinDensity untouched if it has already the right size (required for load)
  if(newsize!=spinDensity.get_extent()) {
    ODINLOG(odinlog,normalDebug) << "resizing" << STD_endl;
    spinDensity.redim (newsize);
    spinDensity.farray::operator = (1.0);
  }

  ODINLOG(odinlog,normalDebug) << "extent=" << get_extent() << STD_endl;
  return *this;
}



bool Sample::check_and_correct(const char* mapname, const farray& srcmap, farray& dstmap) {
  Log<Para> odinlog(this,"check_and_correct");
  ndim nn_map=srcmap.get_extent();
  unsigned int n_sampleDim_old=n_sampleDim-1; // for backwards compatability

  if(nn_map.dim()!=n_sampleDim && nn_map.dim()!=n_sampleDim_old) {
    ODINLOG(odinlog,normalDebug) << "Map " << mapname << " has incorrect shape size=" << nn_map.dim() << STD_endl;
    return false;
  }

  bool reshape=false;
  if(nn_map.dim()==n_sampleDim_old) {
    ODINLOG(odinlog,warningLog) << "Reshaping map " << mapname << " for backwards compatability" << STD_endl;
    nn_map.add_dim(1,true);
    reshape=true;
  }

  ndim nn=get_extent();
  nn[frameDim]=nn_map[frameDim]=1; // ignore size of frame dim
  if(nn!=nn_map) {
    ODINLOG(odinlog,errorLog) << "Map " << mapname << " has incorrect shape " << nn_map << STD_endl;
    return false;
  }

  dstmap=srcmap;
  if(reshape) dstmap.redim(nn_map);

  return true;
}



Sample& Sample::set_T1map(const farray& t1map) {
  if(uniT1T2) return *this;
  if(!check_and_correct("T1",t1map,T1map)) return *this;
  haveT1map=true;
  return *this;
}


const farray& Sample::get_T1map() const {
  if(!haveT1map) {
    T1map.redim(get_extent());
    T1map.farray::operator = (T1);
    haveT1map=true;
  }
  return T1map;
}


Sample& Sample::set_T2map(const farray& t2map) {
  if(uniT1T2) return *this;
  if(!check_and_correct("T2",t2map,T2map)) return *this;
  haveT2map=true;
  return *this;
}


const farray& Sample::get_T2map() const {
  if(!haveT2map) {
    T2map.redim(get_extent());
    T2map.farray::operator = (T2);
    haveT2map=true;
  }
  return T2map;
}


Sample& Sample::set_ppmMap(const farray& ppmmap) {
  if(!check_and_correct("ppmMap",ppmmap,ppmMap)) return *this;
  have_ppmMap=true;
  return *this;
}


const farray& Sample::get_ppmMap() const {
  Log<Para> odinlog(this,"get_ppmMap");
  ODINLOG(odinlog,normalDebug) << "get_extent()=" << get_extent() << STD_endl;
  if(!have_ppmMap) {
    ODINLOG(odinlog,normalDebug) << "Creating zero ppmMap" << STD_endl;
    ppmMap.redim(get_extent());
    ppmMap.farray::operator = (0.0);
    have_ppmMap=true;
  }
  ODINLOG(odinlog,normalDebug) << "ppmMap.get_extent()=" << ppmMap.get_extent() << STD_endl;
  return ppmMap;
}


Sample& Sample::set_spinDensity(const farray& sd) {
  if(!check_and_correct("spinDensity",sd,spinDensity)) return *this;
  return *this;
}


const farray& Sample::get_spinDensity() const {
  return spinDensity;
}


Sample& Sample::set_DcoeffMap(const farray& dmap) {
  if(!check_and_correct("Dcoeff",dmap,DcoeffMap)) return *this;
  have_DcoeffMap=true;
  return *this;
}


const farray& Sample::get_DcoeffMap() const {
  if(!have_DcoeffMap) {
    DcoeffMap.redim(get_extent());
    DcoeffMap.farray::operator = (0.0);
    have_DcoeffMap=true;
  }
  return DcoeffMap;
}


Sample& Sample::update() {

  // outdate cache in interactive mode
  if(uniT1T2) {
    haveT1map=false;
    haveT2map=false;
  }

  if(T1!=0.0 || T2!=0.0) {
    if(T1>0.0 && T2>T1) T2=double(T1);
  }
  return *this;
}


int Sample::load(const STD_string& filename, const LDRserBase& serializer) {
  Log<Para> odinlog(this,"load");
  int errval=LDRblock::load(filename,serializer);

  ndim nn=spinDensity.get_extent();

  if(nn.dim()==(n_sampleDim-1)) {
    ODINLOG(odinlog,normalDebug) << "Reshaping map for backwards compatability" << STD_endl;
    nn.add_dim(1,true);
    spinDensity.redim(nn);
  }

  ODINLOG(odinlog,normalDebug) << "nn=" << nn << STD_endl;
  ODINLOG(odinlog,normalDebug) << "spinDensity.sum()=" << spinDensity.sum() << STD_endl;


  if(nn.dim()!=n_sampleDim || nn.total()==0) {
    ODINLOG(odinlog,errorLog) << "spinDensity has invalid extent=" << STD_string(nn) << STD_endl;
    return -1;
  }
  uniFOV=false;
  uniT1T2=false;

  ODINLOG(odinlog,normalDebug) << "resizing to " << STD_string(nn) << STD_endl;
  resize(nn[frameDim],nn[freqDim],nn[zDim],nn[yDim],nn[xDim]);

  ODINLOG(odinlog,normalDebug) << "ppmMap.get_extent()=" << STD_string(ppmMap.get_extent()) << STD_endl;

  haveT1map=check_and_correct("T1",T1map,T1map);
  haveT2map=check_and_correct("T2",T2map,T2map);
  have_ppmMap=check_and_correct("ppmMap",ppmMap,ppmMap);
  have_DcoeffMap=check_and_correct("Dcoeff",DcoeffMap,DcoeffMap);

  ODINLOG(odinlog,normalDebug) << "have_ppmMap=" << have_ppmMap << STD_endl;

  return errval;
}


int Sample::append_all_members() {

  if(uniFOV) {
    append_member(FOVall,"FOVall");
  } else {
    append_member(FOV,"FOV");
  }
  append_member(freqrange,"FrequencyRange");
  append_member(freqoffset,"FrequencyOffset");

  append_member(frameDurations,"FrameDurations");

  append_member(T1,"RelaxationT1");
  append_member(T2,"RelaxationT2");
  append_member(T1map,"T1map");
  append_member(T2map,"T2map");
  append_member(ppmMap,"ppmMap");
  append_member(spinDensity,"spinDensity");
  append_member(DcoeffMap,"DcoeffMap");

  return 0;
}
