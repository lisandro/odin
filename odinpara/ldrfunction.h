/***************************************************************************
                          ldrfunction.h  -  description
                             -------------------
    begin                : Mon Jun 24 2002
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#ifndef LDRFUNCTION_H
#define LDRFUNCTION_H

#include <odinpara/ldrblock.h>

/**
  * @addtogroup ldr
  * @{
  */

enum funcType {shapeFunc=0, trajFunc, filterFunc};

/**
  *
  * Dimension Mode Enum:
  *
  * - zeroDeeMode :        Only RF waveform will be calculated
  * - oneDeeMode :         RF and gradient shape will be calculated according
  *                       to the selected shape/trajectory for 1D pulses
  * - twoDeeMode :         RF and gradient shape will be calculated according
  *                       to the selected shape/trajectory for 2D pulses
  */
enum funcMode {zeroDeeMode=0,oneDeeMode,twoDeeMode,n_dimModes};

///////////////////////////////////////////

class OdinPulse; // forward declaration

///////////////////////////////////////////

/**
  *
  * Structure to return k-space coordinates, gradient strengts and sampling density
  * compensations of trajectories.
  */
struct kspace_coord {

  kspace_coord() : index(-1), traj_s(0.0), kx(0), ky(0), kz(0), Gx(0), Gy(0), Gz(0), denscomp(1.0) {}

/**
  * The current index in the array
  */
  mutable int index;

/**
  * The dimension-less time parameter which will be used for the shape and filter, ranging from 0 to 1
  */
  float traj_s;

/**
  * The dimension-less x-coordinate in k-space, ranging from -1 to 1
  */
  float kx;

/**
  * The dimension-less y-coordinate in k-space, ranging from -1 to 1
  */
  float ky;

/**
  * The dimension-less z-coordinate in k-space, ranging from -1 to 1
  */
  float kz;

/**
  * The dimension-less x-gradient, i.e. the derivative of kx by s (the dimension-less time parameter)
  */
  float Gx;

/**
  * The dimension-less y-gradient, i.e. the derivative of kx by s (the dimension-less time parameter)
  */
  float Gy;

/**
  * The dimension-less z-gradient, i.e. the derivative of kx by s (the dimension-less time parameter)
  */
  float Gz;

/**
  * Compensation for differing sampling density, RF shape or RF signal will be multiplied by this function
  */
  float denscomp;
};


///////////////////////////////////////////

struct shape_info {

  shape_info() : ref_x_pos(0), ref_y_pos(0), ref_z_pos(0), adiabatic(false), fixed_size(-1), spatial_extent(0.0) {}

  float ref_x_pos; // Reference position to adjust flip angle
  float ref_y_pos;
  float ref_z_pos;
  bool adiabatic; // Whether pulse is adiabatic
  int fixed_size; // Pulse has fixed size (for imported pulses)
  float spatial_extent;
};

///////////////////////////////////////////

struct traj_info {

  traj_info() : rel_center(0), max_kspace_step(0) {}

  float rel_center;
  float max_kspace_step;

};

///////////////////////////////////////////

class LDRfunctionPlugIn : public LDRblock {

 public:
  LDRfunctionPlugIn(const STD_string& funclabel) : LDRblock(funclabel) {}

  LDRfunctionPlugIn(const LDRfunctionPlugIn& jfp) : LDRblock(jfp) {}

  virtual ~LDRfunctionPlugIn() {}

  LDRfunctionPlugIn& operator = (const LDRfunctionPlugIn& jfp) {LDRblock::operator = (jfp); return *this;}

  LDRfunctionPlugIn& register_function(funcType type, funcMode mode);

  // virtual functions for pulse shapes
  virtual void init_shape() {}
  virtual STD_complex calculate_shape(const kspace_coord& coord ) const {return STD_complex(0.0);}
  virtual STD_complex calculate_shape(float s, float Tp) const {return STD_complex(0.0);}
  virtual const shape_info& get_shape_properties() const {return shape_info_retval;}


  // virtual functions for pulse trajectories
  virtual void init_trajectory(OdinPulse* pls) {}
  virtual const kspace_coord& calculate_traj(float s) const {return coord_retval;}
  virtual const traj_info& get_traj_properties() const {return traj_info_retval;}

  // virtual functions for filters
  virtual float calculate_filter(float rel_kradius) const {return 0;}



  // virtual constructor
  virtual LDRfunctionPlugIn* clone() const = 0;

  // local statics do not work on IDEA
  static kspace_coord coord_retval;
//  static shape_vals   shape_retval;
  static shape_info   shape_info_retval;
  static traj_info    traj_info_retval;

};


///////////////////////////////////////////

// Wrapper to bundle LDRfunctionPlugIn and funcType/funcMode together so it can be inserted into lists
// multiple times with different funcTypes and funcModes
struct LDRfunctionEntry {

  LDRfunctionEntry(LDRfunctionPlugIn* func_plugin, funcType func_type, funcMode func_mode)
    : plugin(func_plugin), type(func_type),  mode(func_mode) {}

  LDRfunctionPlugIn* plugin;

  const funcType type;
  const funcMode mode;

  bool operator == (const LDRfunctionEntry& jfe) const;
  bool operator <  (const LDRfunctionEntry& jfe) const;

};

///////////////////////////////////////////


class LDRfunction : public LDRbase, public StaticHandler<LDRfunction> {

 public:

  ~LDRfunction() {new_plugin(0);}

  LDRfunction& set_function_mode(funcMode newmode);
  funcMode get_function_mode() const {return mode;}

  const STD_string& get_function_label(unsigned int index) const;
  LDRfunction& set_function(const STD_string& funclabel);
  LDRfunction& set_function(unsigned int index);
  unsigned int get_function_index() const;

  LDRblock* get_funcpars_block();

  LDRfunction& set_funcpars(const svector& funcpars);
  LDRfunction& set_funcpars(const STD_string& funcpars) {STD_string tt(funcpars); parsevalstring(tt); return *this;}
  svector get_funcpars() const;

  bool set_parameter(const STD_string& parameter_label, const STD_string& value);
  STD_string get_parameter(const STD_string& parameter_label) const;

  STD_string get_function_name() const;

  const STD_string& get_funcdescription() const;


  // overwriting virtual functions from LDRbase
  LDRfunction* cast(LDRfunction*) {return this;}
  bool parsevalstring(const STD_string& parstring, const LDRserBase* ser=0);
  STD_string printvalstring(const LDRserBase* ser=0) const;
  svector get_alternatives() const;
  STD_string get_typeInfo(bool parx_equivtype=false) const {return "function";}
  LDRbase* create_copy() const {return new LDRfunction(*this);}


  // stuff for StaticHandler
  static void init_static();
  static void destroy_static();


 protected:

  // Make it impossible to create/use instance of LDRfunction directly
  LDRfunction(funcType function_type, const STD_string& ldrlabel);
  LDRfunction(const LDRfunction& jf);
  LDRfunction& operator = (const LDRfunction& jf);

  funcMode mode;
  LDRfunctionPlugIn* allocated_function;

 private:
  friend class LDRfunctionPlugIn;




  void new_plugin(LDRfunctionPlugIn* pi);

  const funcType type;


  static STD_list<LDRfunctionEntry>* registered_functions;

};


/** @}
  */

#endif


