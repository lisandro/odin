/***************************************************************************
                          system.h  -  description
                             -------------------
    begin                : Wed Jun 29 2005
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SYSTEM_H
#define SYSTEM_H

#include <tjutils/tjhandler.h>

#include <odinpara/ldrblock.h>
#include <odinpara/ldrnumbers.h>
#include <odinpara/ldrarrays.h>
#include <odinpara/odinpara.h>


// defaults for systemInfo
#define _DEFAULT_B0_                  3000 // 3 Tesla
#define _DEFAULT_MAX_SYSTEM_GRADIENT_ 0.04
#define _DEFAULT_MAX_SYSTEM_SLEWRATE_ 0.2


/**
  * @ingroup odinpara
  * This enum is used to specify the scanner system type
  */
enum odinPlatform {standalone=0, paravision, numaris_4, epic, numof_platforms};

/**
  * @ingroup odinpara
  * This enum is used to distinguish different properties of sequence objects according to
  * their category. Examples are minimum duration or rastertime.
  */
enum objCategory {unknownObj=0,delayObj,pulsObj,gradObj,loopObj,acqObj,freqObj,syncObj,endObj};

/////////////////////////////////////////////////////////////////////////

/**
  * @ingroup odinpara
  * This class is a small database which maps nuclei names to gyromagnetic ratios.
  */
class Nuclei {

 public:
  Nuclei();

/**
  * Returns the gyromagnetic ratio of the nucleus with name 'nucName'
  */
  double get_gamma(const STD_string& nucName) const;

/**
  * Returns the base frequency of the nucleus with name 'nucName' in a static field with the strength 'B0'
  */
  double get_nuc_freq(const STD_string& nucName,float B0) const;


/**
  * Returns an enum with all registered nuclei as entries
  */
  LDRenum get_nuc_enum() const;

 private:
  typedef STD_pair<STD_string, double> Nucleus;
  typedef STD_list<Nucleus> NucList; // do not use std::map since we do not want unsorted list

  NucList nuclist;
};

/////////////////////////////////////////////////////////////////////////

/**
  * @ingroup odinpara
  *
  * \brief System proxy
  *
  * This class is used to hold all information/configuration of the spectrometer
  */
class System : public LDRblock {

 public:

/**
  * Constructs a System with the given label
  */
   System(const STD_string& object_label="unnamedSystem");

/**
  * Copy constructor
  */
   System(const System& s) {System::operator = (s);}

/**
  * Assignment operator
  */
  System& operator = (const System& s);

/**
  * Returns the platform identifier for the scanner, works only in the context of sequence programming
  */
  odinPlatform get_platform() const;

/**
  * Returns a string identifying the platform
  */
  STD_string get_platform_str() const {return platformstr;}

/**
  * Returns a string identifying the transmit coil
  */
  STD_string get_transmit_coil_name() const {return transmit_coil_name;}

/**
  * Sets the transmit coil name
  */
  System& set_transmit_coil_name(const STD_string& tcname);

/**
  * Returns a string identifying the receive coil
  */
  STD_string get_receive_coil_name() const {return receive_coil_name;}

/**
  * Sets the receive coil name
  */
  System& set_receive_coil_name(const STD_string& rcname);

/**
  * Returns the current main nucleus of the sequence, i.e. the nucleus for which both, excitation and acquisition, will be performed
  */
  STD_string get_main_nucleus() const {return main_nucleus;}

/**
  * Specifies the current main nucleus of the scanner, i.e. the nucleus for which both, excitation and acquisition, will be performed
  */
  System& set_main_nucleus(const STD_string& nucname);

/**
  * Returns the maximum gradient strength of the scanner
  */
  float get_max_grad() const {return max_grad;}

/**
  * Returns the maximum slew rate
  */
  float get_max_slew_rate() const {return max_slew_rate;}

/**
  * Returns the time required to change the gradient strength by the specified amount
  */
  float get_grad_switch_time(float graddiff) const;

/**
  * Returns the minimum time gap between two gradient commands
  */
  float get_inter_grad_delay() const {return inter_grad_delay;}

/**
  * Sets the directory where the experimental data files of the current scans are stored
  */
  System& set_scandir(const STD_string& dir);

/**
  * Returns the directory where the experimental data files of the current scans are stored
  */
  STD_string get_scandir() const;


/**
  * Returns the time shift between RF and gradient channel
  */
  float get_grad_shift_delay() const {return grad_shift;}

/**
  * Returns the field strength of the static magnetic field
  */
  double get_B0() const {return B0;}

/**
  * Returns the field strength of the static magnetic field
  */
  System& set_B0(double b0) {B0=b0; return *this;}

/**
  * Specifies the field strength of the static magnetic field by giving the resonance frequency
  * for the specified nucleus
  */
  System& set_B0_from_freq(double freq, const STD_string& nucName="");

/**
  * Returns the gyromagnetic ratio of the given nucleus, default returns the ratio of protons
  */
  double get_gamma(const STD_string& nucName="") const;

/**
  * Sets the reference gain in dB
  */
  System& set_reference_gain(float refgain);

/**
  * Returns the reference gain in dB
  */
  float get_reference_gain() const {return reference_gain;}

/**
  * Returns the resonance for the given nucleus
  */
  double get_nuc_freq(const STD_string& nucName="") const;


/**
  * Returns an enum with all registered nuclei as entries
  */
  LDRenum get_nuc_enum() const;

/**
  * Returns the minimum duration for the given object category
  */
  double get_min_duration(objCategory c) const {return min_duration[c];}

/**
  * Returns the minimum gradient raster time
  */
  double get_min_grad_rastertime() const {return min_grad_rastertime;}

/**
  * Returns the raster time for the given object category, if no such time exists, it returns 0
  */
  double get_rastertime(objCategory cat) const;

/**
  * Returns the rastered time for the given object type and non-rastered time point/period
  */
  double get_rasteredtime(objCategory cat, double time) const;

/**
  * Returns the maximum number of samples per RF pulse
  */
  int get_max_rf_samples() const {return  max_rf_samples;}

/**
  * Returns the maximum number of samples per gradient waveform
  */
  int get_max_grad_samples() const {return  max_grad_samples;}

/**
  * Sets the data represention type of the image data
  */
  System& set_data_type(const STD_string& type) {datatype=type;  return *this;}

/**
  * Returns the data represention type of the image data
  */
  STD_string get_data_type() const {return datatype;}


/**
  * Returns 'true' if gradient switching frequency 'freq' does not cause
  * acoustic resonance, otherwise, 'low' and 'upp' will contain the next
  * possible lower and upper frequencies, respectively.
  */
  bool allowed_grad_freq(double freq, double& low, double& upp) const;


 private:
  friend class SeqMethod;
  friend class SeqIdea;
  friend class SeqParavision;
  friend class SeqStandAlone;
  friend class SeqEpic;

  // overwriting virtual functions from LDRbase
  int load(const STD_string &filename, const LDRserBase& serializer=LDRserJDX());




  int append_all_members();

  Nuclei nuc;

  LDRstring platformstr;
  LDRenum   main_nucleus;
  LDRdouble max_grad;
  LDRdouble max_slew_rate;
  LDRdouble grad_shift;
  LDRdouble inter_grad_delay;
  LDRdouble B0;
  LDRdouble reference_gain;
  LDRstring transmit_coil_name;
  LDRstring receive_coil_name;

  LDRdouble delay_rastertime;
  LDRdouble rf_rastertime;
  LDRdouble grad_rastertime;
  LDRdouble min_grad_rastertime;
  LDRdouble acq_rastertime;

  LDRint max_rf_samples;
  LDRint max_grad_samples;
  LDRstring datatype;

  LDRdoubleArr grad_reson_center;
  LDRdoubleArr grad_reson_width;


  STD_string scandir;

  dvector min_duration;

};


//////////////////////////////////////////////////////////////////////////////

/**
  * @ingroup odinpara
  *  An interface class that makes the global systemInfo look like a pointer,
  *  but requests will be delegated to the actual System object in the platform object
  */
class SystemInterface : public StaticHandler<SystemInterface> {
 public:

  SystemInterface()  {}

  System& operator *  () {return *get_sysinfo_ptr();}
  const System& operator * () const {return *get_const_sysinfo_ptr();}

  System* operator -> () {return get_sysinfo_ptr();}
  const System* operator -> () const {return get_const_sysinfo_ptr();}

  // functions to initialize/delete static members by the StaticHandler template class
  static void init_static();
  static void destroy_static();

  static void set_current_pf(odinPlatform pf);

  static odinPlatform get_current_pf() { // make it inline to speed up driver access
    if(current_pf) return odinPlatform(current_pf->operator int ());
//    else STD_cerr << "ERROR: SystemInterface::get_current_pf: current_pf not yet initialized" << STD_endl;
    return odinPlatform(0);
  }


 private:
//  friend class System;
  friend class SeqIdea;
  friend class SeqParavision;
  friend class SeqStandAlone;
  friend class SeqEpic;
//  friend class SeqPlatformInstances;


  static System* get_sysinfo_ptr();
  static const System* get_const_sysinfo_ptr();

  // the actual platform
  static SingletonHandler<LDRint,false> current_pf;

  // separate systemInfo for each platform
  static SingletonHandler<System,false>* systemInfo_platform;
};



#endif
