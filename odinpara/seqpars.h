/***************************************************************************
                          seqpars.h  -  description
                             -------------------
    begin                : Mon Mar 3 2003
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQPARS_H
#define SEQPARS_H

#include <odinpara/ldrblock.h>
#include <odinpara/ldrtypes.h>
#include <odinpara/ldrnumbers.h>
#include <odinpara/ldrarrays.h>
#include <odinpara/odinpara.h>


/**
  * @ingroup odinpara
  *
  * \brief Sequence Parameter proxy
  *
  * This class is used to hold all common sequence parameters
  */
class SeqPars : public LDRblock {

 public:

/**
  * Constructs a SeqPars with the given label
  */
  SeqPars(const STD_string& label="unnamedSeqPars");

/**
  * Constructs a copy of 'sp'
  */
  SeqPars(const SeqPars& sp) {SeqPars::operator = (sp);}


/**
  * Specifies the size of the image in the given direction 'dir' to 'size'
  */
  SeqPars& set_MatrixSize(direction dir,unsigned int size, parameterMode parmode=edit);

/**
  * Returns the size of the image in the given direction
  */
  unsigned int get_MatrixSize(direction dir) const;

/**
  * Specifies the repetition time of the sequence
  */
  SeqPars& set_RepetitionTime(double time, parameterMode parmode=edit);

/**
  * Returns the repetition time of the sequence
  */
  double get_RepetitionTime() const {return RepetitionTime;}

/**
  * Specifies the number of times the sequence will be repeated
  */
  SeqPars& set_NumOfRepetitions(unsigned int times, parameterMode parmode=edit);

/**
  * Returns the number of times the sequence will be repeated
  */
  unsigned int get_NumOfRepetitions() const {return NumOfRepetitions;}


/**
  * Specifies the echo time of the sequence
  */
  SeqPars& set_EchoTime(double time, parameterMode parmode=edit);

/**
  * Returns the echo time of the sequence
  */
  double get_EchoTime() const {return EchoTime;}



/**
  * Specifies the sweep width of the acquisition window
  */
  SeqPars& set_AcqSweepWidth(double sw, parameterMode parmode=edit);

/**
  * Returns the sweep width of the acquisition window
  */
  double get_AcqSweepWidth() const {return AcqSweepWidth;}

/**
  * Specifies the flip angle of the excitation pulse
  */
  SeqPars& set_FlipAngle(double fa, parameterMode parmode=edit);

/**
  * Returns the flip angle of the excitation pulse
  */
  double get_FlipAngle() const {return FlipAngle;}

/**
  * Specifies the reduction factor for parallel imaging
  */
  SeqPars& set_ReductionFactor(unsigned int factor, parameterMode parmode=edit);

/**
  * Returns the reduction factor for parallel imaging
  */
  unsigned int get_ReductionFactor() const {return ReductionFactor;}

/**
  * Specifies the partial Fourier factor (0.0 = full k-space, 1.0 = half k-space)
  */
  SeqPars& set_PartialFourier(float factor, parameterMode parmode=edit);

/**
  * Returns the partial Fourier factor (0.0 = full k-space, 1.0 = half k-space)
  */
  float get_PartialFourier() const {return PartialFourier;}

/**
  * Specifies whether RF spoiling is used
  */
  SeqPars& set_RFSpoiling(bool flag, parameterMode parmode=edit);

/**
  * Returns whether RF spoiling is used
  */
  bool get_RFSpoiling() const {return RFSpoiling;}

/**
  * Specifies whether gradient intro will be played out prior to sequence
  */
  SeqPars& set_GradientIntro(bool flag, parameterMode parmode=edit);

/**
  * Returns whether gradient intro will be played out prior to sequence
  */
  bool get_GradientIntro() const {return GradientIntro;}

/**
  * Specifies whether a physiological trigger is used
  */
  SeqPars& set_PhysioTrigger(bool flag, parameterMode parmode=edit);

/**
  * Returns whether a physiological trigger is used
  */
  bool get_PhysioTrigger() const {return PhysioTrigger;}

/**
  * Sets the label of the sequence used
  */
  SeqPars& set_Sequence(const STD_string& seqid) {Sequence=seqid; return *this;}

/**
  * Returns the label of the sequence used
  */
  const STD_string& get_Sequence() const {return Sequence;}


/**
  * Sets the total duration of the sequence in minutes
  */
  SeqPars& set_ExpDuration(double dur) {ExpDuration=dur; return *this;}

/**
  * Returns the total duration of the sequence in minutes
  */
  double get_ExpDuration() const {return ExpDuration;}

/**
  * Sets the starting time point of the sequence
  */
  SeqPars& set_AcquisitionStart(double time) {AcquisitionStart=time; return *this;}

/**
  * Returns the starting time point of the sequence
  */
  double get_AcquisitionStart() const {return AcquisitionStart;}

/**
  * Assignment operator
  */
  SeqPars& operator = (const SeqPars& pars2);



 private:
  void append_all_members();

  LDRdouble ExpDuration;
  LDRstring Sequence;
  LDRdouble AcquisitionStart;

  LDRint    MatrixSizeRead;
  LDRint    MatrixSizePhase;
  LDRint    MatrixSizeSlice;
  LDRdouble RepetitionTime;
  LDRint    NumOfRepetitions;
  LDRdouble EchoTime;
  LDRdouble AcqSweepWidth;
  LDRdouble FlipAngle;
  LDRint    ReductionFactor;
  LDRfloat  PartialFourier;
  LDRbool   RFSpoiling;
  LDRbool   GradientIntro;
  LDRbool   PhysioTrigger;
};

#endif
