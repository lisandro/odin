/***************************************************************************
                          odinpara.h  -  description
                             -------------------
    begin                : Tue Jun 28 2005
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef ODINPARA_H
#define ODINPARA_H



#include <tjutils/tjtools.h>

/**
  * @addtogroup odinpara
  * @{
  */

/**
  * @ingroup odinpara
  * This enum is used to specify directions in the coordinate system
  * of the sequence gradients, i.e. gradient channel:
  * - readDirection: The first gradient channel
  * - phaseDirection: The second gradient channel
  * - sliceDirection: The third gradient channel
  */
enum direction                      { readDirection=0, phaseDirection,  sliceDirection, n_directions};
static const char* directionLabel[]={"readDirection", "phaseDirection","sliceDirection"};
AVOID_CC_WARNING(directionLabel)

/**
  * Enum for the direction of spatial axes in the laboratory system of reference:
  * - xAxis:  The first spatial direction
  * - yAxis:  The second spatial direction
  * - zAxis:  The third spatial direction
  */
enum axis {xAxis=0,yAxis,zAxis,n_axes};



// Units within ODIN
#define ODIN_TIME_UNIT "ms"
#define ODIN_FREQ_UNIT "kHz"
#define ODIN_SPAT_UNIT "mm"
#define ODIN_FIELD_UNIT "mT"
#define ODIN_GRAD_UNIT "mT/mm"
#define ODIN_ANGLE_UNIT "deg"


// some common stuff for geometry and sample
#define ODIN_DEFAULT_FOV 220.0
#define ODIN_MAX_NUMOF_SLICES 50
#define ODIN_GEO_CHECK_LIMIT 1e-6


// for debugging odinpara component
class Para {
 public:
  static const char* get_compName();
};


/** @}
  */



#endif
