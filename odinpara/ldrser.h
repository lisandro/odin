/***************************************************************************
                          ldrser.h  -  description
                             -------------------
    begin                : Wed Aug 26 2015
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef LDRSER_H
#define LDRSER_H

#include <tjutils/tjutils.h>


// some defines for Bruker-type strings
#define _BRUKER_MODE_STRING_CAP_START_ 1000
#define _BRUKER_MODE_STRING_MIN_SIZE_ 256
#define _BRUKER_MODE_STRING_CAP_FACTOR_ 3


class LDRbase; // forward declaration
class LDRblock; // forward declaration

///////////////////////////////////////////////////////////////////////

/**
  *
  * This enum performs some fine tuning with the JCAMP-DX input/output according
  * to its value:
  * - bruker: Strings are treated as arrays of characters (size after '=' and contents on next line).
  *           Used to be compatible with Brukers JCAMP-DX implementation.
  * - notBroken: Strings are written directly into the file (after '=').
  */
enum compatMode {bruker,notBroken};

///////////////////////////////////////////////////////////////////////

/**
  *
  *  Virtual base class for serializers of Labeled Data Records (LDR)
  */
class LDRserBase {

 public:
  LDRserBase() : top_level(true), compmode(notBroken) {}

  virtual STD_string get_default_file_prefix() const = 0;
  virtual STD_string get_description() const = 0;

  virtual STD_string get_top_header() const = 0;

  virtual STD_string get_prefix(const LDRbase& ldr) const = 0;
  virtual STD_string get_postfix(const LDRbase& ldr) const = 0;

  virtual STD_string extract_valstring(const STD_string& parstring) const = 0;

  virtual STD_string remove_comments(const STD_string& parstring) const = 0;

  virtual STD_string escape_characters(const STD_string& parstring) const = 0;
  virtual STD_string deescape_characters(const STD_string& parstring) const = 0;

  virtual STD_string print_string(const STD_string& str) const = 0;
  virtual STD_string parse_string(const STD_string& parstring) const = 0;

  virtual char left_string_quote() const = 0;
  virtual char right_string_quote() const = 0;

  virtual STD_string get_blocklabel(const STD_string& parstring) const = 0;
  virtual STD_string get_blockbody(const STD_string& parstring, bool including_delimiters) const = 0;

  virtual STD_string get_parlabel(const STD_string& parstring) const = 0;

  virtual void remove_next_ldr(STD_string& parstring) const = 0;


  compatMode get_jdx_compatmode() const {return compmode;}

  // for XML
  mutable bool top_level;

  // for JCAMP-DX
  compatMode compmode;
};


///////////////////////////////////////////////////////////////////////


/**
  *
  *  JCAMP-DX serializer
  */
class LDRserJDX : public LDRserBase {

 public:

  LDRserJDX(compatMode compat_mode=notBroken) {LDRserBase::compmode=compat_mode;}

  // overloading virtual functions from LDRserBase
  STD_string get_default_file_prefix() const {return "jdx";}
  STD_string get_description() const {return "JCAMP-DX (Joint Committee on Atomic and Molecular Physical Data)";}
  STD_string get_top_header() const {return "";}
  STD_string get_prefix(const LDRbase& ldr) const;
  STD_string get_postfix(const LDRbase& ldr) const;
  STD_string extract_valstring(const STD_string& parstring) const;
  STD_string remove_comments(const STD_string& parstring) const;
  STD_string escape_characters(const STD_string& parstring) const;
  STD_string deescape_characters(const STD_string& parstring) const;
  STD_string print_string(const STD_string& str) const;
  STD_string parse_string(const STD_string& parstring) const;
  char left_string_quote() const {return '<';}
  char right_string_quote() const {return '>';}
  STD_string get_blocklabel(const STD_string& parstring) const;
  STD_string get_blockbody(const STD_string& parstring, bool including_delimiters) const;
  STD_string get_parlabel(const STD_string& parstring) const;
  void remove_next_ldr(STD_string& parstring) const;


};

///////////////////////////////////////////////////////////////////////


/**
  *
  *  XML serializer
  */
class LDRserXML : public LDRserBase {

 public:

  // overloading virtual functions from LDRserBase
  STD_string get_default_file_prefix() const {return "xml";}
  STD_string get_description() const {return "XML (Extensible Markup Language)";}
  STD_string get_top_header() const {return "<?xml version=\"1.0\"?>\n";}
  STD_string get_prefix(const LDRbase& ldr) const;
  STD_string get_postfix(const LDRbase& ldr) const;
  STD_string extract_valstring(const STD_string& parstring) const;
  STD_string remove_comments(const STD_string& parstring) const;
  STD_string escape_characters(const STD_string& parstring) const;
  STD_string deescape_characters(const STD_string& parstring) const;
  STD_string print_string(const STD_string& str) const {return str;}
  STD_string parse_string(const STD_string& parstring) const;
  char left_string_quote() const {return '\'';}
  char right_string_quote() const {return '\'';}
  STD_string get_blocklabel(const STD_string& parstring) const;
  STD_string get_blockbody(const STD_string& parstring, bool including_delimiters) const;
  STD_string get_parlabel(const STD_string& parstring) const;
  void remove_next_ldr(STD_string& parstring) const;

 private:
  STD_string str_between_delimiters(const STD_string& parstring, STD_string& startdelim, STD_string& enddelim) const;
  static STD_string create_well_formed_tag(const STD_string& ldrlabel);

};



#endif
