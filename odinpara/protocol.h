/***************************************************************************
                          protocol.h  -  description
                             -------------------
    begin                : Tue Jul 5 2005
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef PROTOCOL_H
#define PROTOCOL_H

#include <odinpara/geometry.h>
#include <odinpara/seqpars.h>
#include <odinpara/study.h>
#include <odinpara/system.h>

/**
  * @ingroup odinpara
  *
  * \brief Protcol proxy
  *
  * Class to hold the whole MR Protocol, that is system, geometry and seqpars
  */
class Protocol : public LDRblock {


 public:

/**
  * Create a default protocol with the given label for the sequence with label 'seqid'
  */
  Protocol(const STD_string& label="unnamedProtocol");

/**
  * Copy constructor
  */
  Protocol(const Protocol& p) {Protocol::operator = (p);}


/**
  * Assignment operator
  */
  Protocol& operator = (const Protocol& p);


/**
  * Special comparison operator for Protocol which does not consider
  * 'AcquisitionStart' and 'offsetSlice' so that tempo-spatial data has the
  * 'same' protocol. This behaviour is required in FileIO::autoread.
  */
  bool operator < (const Protocol& rhs) const;

/**
  * Special comparison operator for Protocol which does not consider
  * 'AcquisitionStart' and 'offsetSlice' so that tempo-spatial data has the
  * 'same' protocol. This behaviour is required in FileIO::autoread.
  */
  bool operator == (const Protocol& rhs) const {return !( ((*this)<rhs) || ((rhs<(*this))) );}


/**
  * system configuration
  */
  System system;

/**
  * geometry
  */
  Geometry geometry;

/**
  * sequence parameters
  */
  SeqPars seqpars;

/**
  * method-specific parameters
  */
  LDRblock methpars;

/**
  * study information
  */
  Study study;


 private:
  friend class SeqMethod;

  void append_all_members();

};

#endif
