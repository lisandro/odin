#include "ldrarrays.h"
#include "ldrblock.h" // for UnitTest
#include <tjutils/tjtypes.h>
#include <tjutils/tjtest.h>


#define ENCODING_ID_STR "Encoding:"
#define BASE64_LINELEN 72

class Base64 {

 public:

  Base64() {
    int i;
    for (i = 0; i < 9; i++) {
      dtable_encode[i] = 'A' + i;
      dtable_encode[i + 9] = 'J' + i;
      dtable_encode[26 + i] = 'a' + i;
      dtable_encode[26 + i + 9] = 'j' + i;
    }
    for (i = 0; i < 8; i++) {
      dtable_encode[i + 18] = 'S' + i;
      dtable_encode[26 + i + 18] = 's' + i;
    }
    for (i = 0; i < 10; i++) {
      dtable_encode[52 + i] = '0' + i;
    }
    dtable_encode[62] = '+';
    dtable_encode[63] = '/';


    for (i = 0; i < 255; i++) dtable_decode[i] = 0x80;
    for (i = 'A'; i <= 'I'; i++) dtable_decode[i] = 0  + (i - 'A');
    for (i = 'J'; i <= 'R'; i++) dtable_decode[i] = 9  + (i - 'J');
    for (i = 'S'; i <= 'Z'; i++) dtable_decode[i] = 18 + (i - 'S');
    for (i = 'a'; i <= 'i'; i++) dtable_decode[i] = 26 + (i - 'a');
    for (i = 'j'; i <= 'r'; i++) dtable_decode[i] = 35 + (i - 'j');
    for (i = 's'; i <= 'z'; i++) dtable_decode[i] = 44 + (i - 's');
    for (i = '0'; i <= '9'; i++) dtable_decode[i] = 52 + (i - '0');
    dtable_decode[(int)'+'] = 62;
    dtable_decode[(int)'/'] = 63;
    dtable_decode[(int)'='] = 0;
  }


  static const char* get_encoding_id() {return "base64";}

  bool encode(STD_string* ostring, STD_ostream* ostream, unsigned char* data, unsigned int nbytes) const {

    int i;
    bool hiteof=false;

    unsigned int counter=0;
    unsigned int linelength=0;

    while (!hiteof) {
      unsigned char igroup[3], ogroup[4];
      int n;
      unsigned char c;

      igroup[0] = igroup[1] = igroup[2] = 0;
      for (n = 0; n < 3; n++) {
        if(counter<nbytes) {
          c = data[counter];
          counter++;
        } else {
          hiteof = true;
          break;
        }
        igroup[n] = c;
      }
      if (n > 0) {
        ogroup[0] = dtable_encode[igroup[0] >> 2];
        ogroup[1] = dtable_encode[((igroup[0] & 3) << 4) | (igroup[1] >> 4)];
        ogroup[2] = dtable_encode[((igroup[1] & 0xF) << 2) | (igroup[2] >> 6)];
        ogroup[3] = dtable_encode[igroup[2] & 0x3F];

        if (n < 3) {
          ogroup[3] = '=';
          if (n < 2) {
            ogroup[2] = '=';
          }
        }
        for (i = 0; i < 4; i++) {

          if (linelength >= BASE64_LINELEN) {
            if(ostream) (*ostream) << STD_endl;
            if(ostring) (*ostring)+="\n";
            linelength = 0;
          }
          if(ostream) (*ostream) << ogroup[i];
          if(ostring) (*ostring)+=STD_string(1,ogroup[i]);
          linelength++;
        }

      }

    }

    return true;
  }


  bool decode(const STD_string& istring, unsigned char* data, unsigned int reserved_nbytes) const {
    Log<LDRcomp> odinlog("Base64","decode");

    int string_counter=0;
    int output_counter=0;

    int stringsize=istring.length();
    if(!stringsize) {
      if(reserved_nbytes) {
        ODINLOG(odinlog,errorLog) << "empty string" << STD_endl;
        return false;
      }
      return true;
    }

    unsigned char c;
    unsigned char a[4], b[4], o[3];

    int i;

    string_counter=textbegin(istring,string_counter);

    while (string_counter>=0 && string_counter<stringsize ) {

      for (i = 0; i < 4; i++) {

        if(string_counter<0 || string_counter>=stringsize ) {
          ODINLOG(odinlog,errorLog) << "string has illegal size: string_counter/stringsize=" << string_counter << "/" << stringsize << STD_endl;
          return false;
        }

        c = istring[string_counter];
        string_counter++;
        string_counter=textbegin(istring,string_counter);

        if (dtable_decode[c] & 0x80) {
          ODINLOG(odinlog,errorLog) << "Illegal character >" << c << "< in input string" << STD_endl;
          return false;
        }
        a[i] = c;
        b[i] = (unsigned char) dtable_decode[c];
      }
      o[0] = (b[0] << 2) | (b[1] >> 4);
      o[1] = (b[1] << 4) | (b[2] >> 2);
      o[2] = (b[2] << 6) | b[3];
      int nbytes2copy = a[2] == '=' ? 1 : (a[3] == '=' ? 2 : 3);

      for (i = 0; i < nbytes2copy; i++) {
        if(output_counter<int(reserved_nbytes)) data[output_counter]=o[i];
        output_counter++;
      }
    }
    return true;
  }


 private:
  unsigned char dtable_encode[256];
  unsigned char dtable_decode[256];


};

/////////////////////////////////////////////////////////////////////////////////////

struct LDRendianess : public LDRenum {
  LDRendianess() : LDRenum("Endianess") {
    add_item("littleEndian",int(true));
    add_item("bigEndian",int(false));
    LDRenum::operator = (int(little_endian_byte_order()));
  }
};

/////////////////////////////////////////////////////////////////////////////////////

void swabdata(unsigned char* data, unsigned int elsize, unsigned int nelements) {
  unsigned char* onelement= new unsigned char[elsize];
  for(unsigned int i=0; i<nelements; i++) {
    unsigned int j;
    for(j=0; j<elsize; j++) onelement[j]=data[i*elsize+j];
    for(j=0; j<elsize; j++) data[i*elsize+j]=onelement[elsize-j-1];
  }
  delete[] onelement;
}

/////////////////////////////////////////////////////////////////////////////////////

template<class A,class J>
void LDRarray<A,J>::common_init() {
  Log<LDRcomp> odinlog(this,"common_init");
  LDRbase::set_parmode(noedit); // Do NOT call virtual function in constructor!
  guiprops.scale[xPlotScale].label="Data Point";
}

template<class A,class J>
LDRarray<A,J>::LDRarray (const A& a,const STD_string& name) : A(a) {
  Log<LDRcomp> odinlog(name.c_str(),"LDRarray(const A&)");
  common_init();
  set_label(name);
}

template<class A,class J>
LDRarray<A,J>& LDRarray<A,J>::operator = (const A& a) {
  Log<LDRcomp> odinlog(this,"operator = (const A&)");
  A::operator = (a);
  return *this;
}

template<class A,class J>
LDRarray<A,J>& LDRarray<A,J>::operator = (const LDRarray<A,J>& ja) {
  LDRbase::operator = (ja);
  A::operator = (ja);
  return *this;
}



template<class A,class J>
STD_string LDRarray<A,J>::get_dim_str(const LDRserBase* ser) const {
  ndim nn(A::get_extent());
  J basetype;
  if( ser && ser->get_jdx_compatmode()==bruker && basetype.get_typeInfo()==STD_string("string") ) {
    if(nn.dim()==1 && nn[0]==1) --nn;
    unsigned int l=0; // val.length()*_BRUKER_MODE_STRING_CAP_FACTOR_;
    if(l<_BRUKER_MODE_STRING_CAP_START_) l=_BRUKER_MODE_STRING_CAP_START_;
    nn.add_dim(l,false);
  }
  return STD_string(nn);
}

template<class A,class J>
bool LDRarray<A,J>::encode(STD_string* ostring, STD_ostream* ostream) const {
  Base64 b64;
  unsigned char* data=(unsigned char*)A::c_array();
  if(!data) return false;

  LDRendianess endianess;
  J ldrdummy;
  
  STD_string header=STD_string(ENCODING_ID_STR)
                    +b64.get_encoding_id()+","
                    +endianess.LDRenum::operator STD_string ()+","
                    +ldrdummy.get_typeInfo()+"\n";

  if(ostring) (*ostring) += header;
  if(ostream) (*ostream) << header;
  return b64.encode(ostring,ostream,data,A::length()*A::elementsize());
}




template<class A,class J>
STD_ostream& LDRarray<A,J>::print2stream(STD_ostream& os, const LDRserBase& serializer) const {
  os << get_dim_str(&serializer) << "\n";
  if( !(use_compression() && encode(0,&os)) ) {

    unsigned long n=A::length();

    J typedummy;
    bool stringarr=(TypeTraits::type2label(typedummy)==STD_string("string"));

    STD_string left_quote(1,serializer.left_string_quote());
    STD_string right_quote(1,serializer.right_string_quote());

    unsigned int width=0;

    for(unsigned long i=0; i<n; i++) {
      if(width>_DEFAULT_LINEWIDTH_) {os << "\n"; width=0;}
      if(stringarr) {os << left_quote; width++;}
      STD_string val(TypeTraits::type2string((*this)[i]));
      os << val;  width+=val.length();
      if(stringarr) {os << right_quote; width++;}
      if(i!=(n-1)) {os << " "; width++;}
    }
  }
  return os;
}


template<class A,class J>
STD_string LDRarray<A,J>::printvalstring(const LDRserBase* ser) const {
  Log<LDRcomp> odinlog(this,"printvalstring");
  STD_string result;
  if(get_filemode()==exclude) return result;
  result+=get_dim_str(ser)+"\n";
  if( !(use_compression() && encode(&result,0)) ) {

    J typedummy;
    bool stringarr=(TypeTraits::type2label(typedummy)==STD_string("string"));
    ODINLOG(odinlog,normalDebug) << "stringarr=" << stringarr << STD_endl;

    STD_string left_quote="\"";
    STD_string right_quote="\"";
    if(ser) {
      left_quote=STD_string(1,ser->left_string_quote());
      right_quote=STD_string(1,ser->right_string_quote());
    }

    unsigned long n=A::length();
    svector tokens; tokens.resize(n);
    STD_string val;
    for(unsigned long i=0;i<n;i++) {
      val=TypeTraits::type2string((*this)[i]);
      if(stringarr) val=left_quote+val+right_quote;
      tokens[i]=val;
    }
    result+=tokenstring(tokens,_DEFAULT_LINEWIDTH_);
  }
  return result;
}


template<class A,class J>
bool LDRarray<A,J>::parsevalstring(const STD_string& parstring, const LDRserBase* ser) {
  Log<LDRcomp> odinlog(this,"parsevalstring");

  ODINLOG(odinlog,normalDebug) << "parstring: >" << parstring << "<" << STD_endl;

  J basetype;

  STD_string sizestring="("+extract(parstring,"(",")")+")";
  ndim nn(sizestring);
  if( ser && ser->get_jdx_compatmode()==bruker && basetype.get_typeInfo()==STD_string("string") ) nn--;
  ODINLOG(odinlog,normalDebug) << "sizestring=>" << sizestring << "<" << STD_endl;

  STD_string valstring=extract(parstring,"\n","");
  ODINLOG(odinlog,normalDebug) << "valstring=" << valstring << STD_endl;

  unsigned long lnn=nn.total();

  if(valstring.find(ENCODING_ID_STR)==0) { // array is encoded
    ODINLOG(odinlog,normalDebug) << "array data is encoded, lnn=" << lnn << STD_endl;

    Base64 b64;

    STD_string header=extract(valstring,STD_string(ENCODING_ID_STR),"\n");
    svector headertokens(tokens(header,','));
    if(headertokens.size()!=3) {
      ODINLOG(odinlog,errorLog) << "Invalid encoding header" << STD_endl;
      return false;
    }

    STD_string encodingtype=shrink(headertokens[0]);
    if(encodingtype!=b64.get_encoding_id()) {
      ODINLOG(odinlog,errorLog) << "Unknown encoding type " << encodingtype << STD_endl;
      return false;
    }

    LDRendianess endianess;
    STD_string endianess_string=shrink(headertokens[1]);
    endianess.LDRenum::operator = (endianess_string);
    ODINLOG(odinlog,normalDebug) << "header/encodingtype/endianess_string=" << header << "/" << encodingtype << "/" << endianess_string << STD_endl;

    STD_string typestring=shrink(headertokens[2]);
    if(typestring!=basetype.get_typeInfo()) {
     // avoid confusing error message in miview
//      ODINLOG(odinlog,errorLog) << "Type mismatch of encoded data: " << typestring << "!=" << ldrdummy.get_typeInfo() << STD_endl;
      return false;
    }


    STD_string encoded_string=extract(valstring,header,"");

    unsigned int elsize=A::elementsize();
    unsigned int nbytes=lnn*elsize;
    ODINLOG(odinlog,normalDebug) << "elsize/lnn/nbytes=" << elsize << "/" << lnn << "/"  << nbytes << STD_endl;

    // allocate correctly aligned memory
    unsigned char* data=(unsigned char*)A::allocate_memory(lnn);
    if(data) {
      bool encresult;
      if( (encresult=b64.decode(encoded_string,data,nbytes)) ) {

        if(int(endianess)!=int(little_endian_byte_order())) {
          ODINLOG(odinlog,normalDebug) << "swapping data" << STD_endl;
          swabdata(data,elsize,lnn);
        }

        A::redim(nn);
        ODINLOG(odinlog,normalDebug) << "nn=" << STD_string(nn) << STD_endl;
        A::set_c_array(data,lnn);
      }
      delete[] data;
      return encresult;
    } else return false;
  }


  // array is plain ASCII

  char left_quote='"';
  char right_quote='"';
  if(ser) {
    left_quote=ser->left_string_quote();
    right_quote=ser->right_string_quote();
    ODINLOG(odinlog,normalDebug) << "left_quote/right_quote=" << left_quote << "/" << right_quote << STD_endl;
  }


  svector tokenvector(tokens(valstring,0,left_quote,right_quote));
  unsigned long lstr=tokenvector.size();
  if(lstr<1) {
    A::resize(0);
    return true;
  }
  if(lstr!=lnn) {
     ODINLOG(odinlog,errorLog) << "size mismatch ("<< lstr << "!=" << lnn << ")" << STD_endl;
    return false;
  }
  ODINLOG(odinlog,normalDebug) << "analyzed " << lnn << " tokens" << STD_endl;
  A::redim(nn);
  for(unsigned long i=0;i<lnn;i++) {
    ODINLOG(odinlog,normalDebug) << "tokenvector[" << i << "]=" << tokenvector[i] << STD_endl;
    basetype.parsevalstring(tokenvector[i],ser);
    (*this)[i]=basetype;
  }

  return true;
}


template<class A,class J>
STD_string LDRarray<A,J>::get_parx_code(parxCodeType type) const {
#ifdef PARAVISION_PLUGIN
  Log<LDRcomp> odinlog(this,"get_parx_code");
  STD_string result=LDRbase::get_parx_code(type);
  int d;

  if(type==parx_def) {
    J ldrdummy;
    d=A::dim();
    if( /*compat_mode==bruker &&*/ ldrdummy.get_typeInfo()==STD_string("string") ) d++; // expecting bruker compatMode mode
    if(d<=0) d=1;
    STD_string parx_type(ldrdummy.get_typeInfo(true));
    if(parx_type=="float") parx_type="double";
    return get_parx_def_string(parx_type,d);
  }


  if(type==parx_passval) {
    if(!A::dim()) return "";
    STD_string src,dst,transformation;

    result="";

    dst=get_jdx_props().parx_equiv_name;
    src=get_label();

    STD_string dstpostfix,srcelement,dstelement;
    STD_string dstprefix(dst);

    if(dstprefix.find("[]")!=STD_string::npos) {
      dstprefix=replaceStr(dstprefix,"[]","");
      result+="PARX_change_dims(\""+dstprefix+dstpostfix+"\",";
      for(d=0;d<int(A::dim());d++) {
        result+="PARX_get_dim(\""+src+"\","+itos(d+1)+")";
        if( d != (int(A::dim())-1) ) result+=",";
      }
      result+=");\n";
    }

    if(dst.find("[###]")!=STD_string::npos) {
      STD_string tmp((STD_string)"__beginmark__"+dstprefix+"__endmark__");
      dstprefix=extract(tmp,"__beginmark__","[###]");
      dstpostfix=extract(tmp,".","__endmark__");
    }

    STD_string iterator=(STD_string)"i"+dstprefix+dstpostfix;
    if(dstpostfix!=(STD_string)"") dstpostfix="."+dstpostfix;

    for(d=0;d<int(A::dim());d++) {
      result+=(STD_string)"for("+iterator+itos(d)+"=0;"+iterator+itos(d)+"<PARX_get_dim(\""+dstprefix+"\","+itos(d+1)+") && "+iterator+itos(d)+"<PARX_get_dim(\""+src+"\","+itos(d+1)+") ;"+iterator+itos(d)+"++) {\n";
    }
    dstelement+=dstprefix;
    for(d=0;d<int(A::dim());d++) {
      dstelement+=(STD_string)"[";
      dstelement+=iterator+itos(d);
      dstelement+=(STD_string)"]";
    }

    dstelement+=(STD_string)dstpostfix;
    srcelement+=src;
    for(d=0;d<int(A::dim());d++) srcelement+=(STD_string)"["+iterator+itos(d)+"]";


    J ldrdummy;
    ldrdummy.set_label(srcelement);

    JcampDxProps jp=get_jdx_props();
    jp.parx_equiv_name=dstelement;
    ldrdummy.set_jdx_props(jp);

    result+=ldrdummy.get_parx_code(parx_passval);

    for(d=0;d<int(A::dim());d++) {
      result+="}\n";
    }
  }


  if(type==parx_passval_head) {
    if(!A::dim()) return "";
    STD_string dsttmp(rmblock(get_jdx_props().parx_equiv_name,"[###]",".",true,true,false,false));
    dsttmp=replaceStr(dsttmp,"[]","");
    result+="unsigned int ";
    for(d=0;d<int(A::dim());d++) {
      result+="i"+dsttmp+itos(d);
      if(d!=(int(A::dim())-1)) result+=",";
    }
    result+=";\n";
  }

  return result;
#else
  return "";
#endif
}



template<class A,class J>
STD_string LDRarray<A,J>::get_typeInfo(bool parx_equivtype) const {
  J ldrdummy;
  typeInfo_cache=STD_string(ldrdummy.get_typeInfo())+"Arr";
  return typeInfo_cache.c_str();
}

////////////////////////////////////////////////////////////


template class LDRarray<sarray,LDRstring>;
template class LDRarray<iarray,LDRint>;
template class LDRarray<farray,LDRfloat>;
template class LDRarray<darray,LDRdouble>;
template class LDRarray<carray,LDRcomplex>;

////////////////////////////////////////////////////////////

LDRtriple::LDRtriple (float xpos,float ypos, float zpos, const STD_string& name)
 : LDRfloatArr(farray(3),name) {
  (*this)[0]=xpos; (*this)[1]=ypos; (*this)[2]=zpos;
}


///////////////////////////////////////////////////////////////////

#ifndef NO_UNIT_TEST

class LDRstringArrTest : public UnitTest {

 public:
  LDRstringArrTest() : UnitTest("LDRstringArr") {}

 private:
  bool check() const {
    Log<UnitTest> odinlog(this,"check");

    sarray sarr(3);
    sarr(0)="item1";
    sarr(1)="it em2";
    sarr(2)="item3";
    LDRstringArr teststrarr1(sarr,"teststrarr1");
    LDRstringArr teststrarr2(sarr,"teststrarr2");

    // XML test
    STD_string expected="<teststrarr1>( 3 )\n'item1' 'it em2' 'item3'</teststrarr1>\n";
    STD_string printed=teststrarr1.print(LDRserXML());
    if(printed!=expected) {
      ODINLOG(odinlog,errorLog) << "LDRstringArr::print(XML) failed: got >" << printed << "<, but expected >" << expected << "<" << STD_endl;
      return false;
    }

    LDRstringArr xmlparsetestarr;
    xmlparsetestarr.parse(printed,LDRserXML());
    if(STD_vector<STD_string>(teststrarr1)!=STD_vector<STD_string>(xmlparsetestarr)) { // cast required for vxworks
      ODINLOG(odinlog,errorLog) << "after parse(XML): " << teststrarr1 << "!=" << xmlparsetestarr << STD_endl;
      return false;
    }


    // testing LDRstringArr::print
    expected="##$teststrarr1=( 3 )\n<item1> <it em2> <item3>\n";
    printed=teststrarr1.print(LDRserJDX(notBroken));
    if(printed!=expected) {
      ODINLOG(odinlog,errorLog) << "LDRstringArr::print(notBroken) failed: got >" << printed << "<, but expected >" << expected << "<" << STD_endl;
      return false;
    }
    expected="##$teststrarr2=( 3, "+itos(_BRUKER_MODE_STRING_CAP_START_)+" )\n<item1> <it em2> <item3>\n";
    printed=teststrarr2.print(LDRserJDX(bruker));
    if(printed!=expected) {
      ODINLOG(odinlog,errorLog) << "LDRstringArr::print(bruker) failed: got >" << printed << "<, but expected >" << expected << "<" << STD_endl;
      return false;
    }


    // testing LDRstringArr::parse
    LDRblock arrblock;
    arrblock.append(teststrarr1);
    arrblock.append(teststrarr2);
    int parseresult_arr=arrblock.parseblock("##TITLE=arrblock\n##$teststrarr1=(2)\n<st r1> <str2>\n##$teststrarr2=(2)\n<st r1> <str2>\n##END=",LDRserJDX(notBroken));
    if(parseresult_arr!=2) {
      ODINLOG(odinlog,errorLog) << "LDRblock::parseblock(notBroken) failed: parseresult_arr=" << parseresult_arr << "!=" << 2 << STD_endl;
      return false;
    }
    if(STD_string(arrblock.get_label())!="arrblock") {
      ODINLOG(odinlog,errorLog) << "LDRblock::get_label() failed: " << arrblock.get_label() << "!=arrblock" << STD_endl;
      return false;
    }
    if(STD_string(teststrarr2[1])!="str2") {
      ODINLOG(odinlog,errorLog) << "element 1 of LDRstringArr: " << STD_string(teststrarr1[1]) << "!=" << "str2" << STD_endl;
      ODINLOG(odinlog,errorLog) << "teststrarr2=" << teststrarr2.printbody() << STD_endl;
      return false;
    }


//    JcampDxProps jp=teststrarr2.get_jdx_props();
//    jp.compat_mode=notBroken;
//    teststrarr2.set_jdx_props(jp);
//    teststrarr2.set_compatmode(notBroken);
    if(STD_vector<STD_string>(teststrarr1)!=STD_vector<STD_string>(teststrarr2)) { // cast required for vxworks
      ODINLOG(odinlog,errorLog) << "after arrblock.parseblock(notBroken): " << teststrarr1 << "!=" << teststrarr2 << STD_endl;
      return false;
    }

    teststrarr1.resize(0);
    teststrarr2.resize(0);
    parseresult_arr=arrblock.parseblock("##TITLE=arrblock\n##$teststrarr1=(2,100)\n<st r1> <str2>\n##$teststrarr2=(2,50)\n<st r1> <str2>  $$ Comment \n##dummypar=dd\n##END=",LDRserJDX(bruker));
    if(parseresult_arr!=2) {
      ODINLOG(odinlog,errorLog) << "LDRblock::parseblock(bruker) failed: parseresult_arr=" << parseresult_arr << "!=" << 2 << STD_endl;
      return false;
    }
    if(teststrarr1.length()!=2) { // cast required for vxworks
      ODINLOG(odinlog,errorLog) << "after arrblock.parseblock(notBroken): " << "teststrarr1.size()!=2" << STD_endl;
      return false;
    }
    if(STD_vector<STD_string>(teststrarr1)!=STD_vector<STD_string>(teststrarr2)) { // cast required for vxworks
      ODINLOG(odinlog,errorLog) << "after arrblock.parseblock(notBroken): " << teststrarr1 << "!=" << teststrarr2 << STD_endl;
      return false;
    }


    return true;
  }

};

void alloc_LDRstringArrTest() {new LDRstringArrTest();} // create test instance

///////////////////////////////////////////////////////////////////


class LDRintArrTest : public UnitTest {

 public:
  LDRintArrTest() : UnitTest("LDRintArr") {}

 private:
  bool check() const {
    Log<UnitTest> odinlog(this,"check");

    // testing LDRintArr
    LDRintArr testintarr(iarray(2,2),"testintarr");
    testintarr(0,0)=1; testintarr(0,1)=2;
    testintarr(1,0)=3; testintarr(1,1)=4;
    STD_string expected="##$testintarr=( 2, 2 )\n1 2 3 4\n";
    STD_string printed=testintarr.print();
    if(printed!=expected) {
      ODINLOG(odinlog,errorLog) << "LDRintArr::print() failed: got >" << printed << "<, but expected >" << expected << "<" << STD_endl;
      return false;
    }
    LDRblock intarrblock;
    intarrblock.append(testintarr);
    intarrblock.parseblock("##TITLE=intarrblock\n##$testintarr=(2,2)\n3 4 5 6\n##END=");
    if(testintarr.sum()!=18) {
      ODINLOG(odinlog,errorLog) << "after intarrblock.parseblock(): " << testintarr.sum() << "!=" << 18 << STD_endl;
      return false;
    }
    testintarr=testintarr*2;
    if(testintarr.sum()!=36) {
      ODINLOG(odinlog,errorLog) << "LDRintArr *= " << testintarr.sum() << "!=" << 36 << STD_endl;
      return false;
    }

    return true;
  }

};

void alloc_LDRintArrTest() {new LDRintArrTest();} // create test instance


///////////////////////////////////////////////////////////////////

class LDRcomplexArrTest : public UnitTest {

 public:
  LDRcomplexArrTest() : UnitTest("LDRcomplexArr") {}

 private:
  bool check() const {
    Log<UnitTest> odinlog(this,"check");

    // testing compression of LDRcomplexArr
    LDRcomplexArr testcarr(carray(100,20),"testcarr");
    testcarr.set_filemode(compressed);
    for(unsigned int i=0; i<testcarr.length(); i++) {
      testcarr[i]=STD_complex(sqrt(float(i)),float(i));
    }
    STD_string expected=testcarr.print();

    carray ca;
    LDRcomplexArr testcarr2(ca,"testcarr");
    STD_string parsestring(expected);
    testcarr2.parse(parsestring);
    testcarr2.set_filemode(compressed);
    STD_string printed=testcarr2.print();

    if(printed!=expected) {
      ODINLOG(odinlog,errorLog) << "LDRcomplexArr::print() failed: got >" << printed << "<, but expected >" << expected << "<" << STD_endl;
      return false;
    }

    return true;
  }

};

void alloc_LDRcomplexArrTest() {new LDRcomplexArrTest();} // create test instance


#endif

