#include "ldrtypes.h"
#include "ldrblock.h" // for UnitTest
#include <tjutils/tjtest.h>


LDRstring::LDRstring(const STD_string& ss,const STD_string& name)
  : STD_string(ss) {
  set_label(name);
}


LDRstring& LDRstring::operator = (const LDRstring& ss) {
  LDRbase::operator = (ss);
  STD_string::operator = (ss);
  return *this;
}


STD_string LDRstring::printvalstring(const LDRserBase* ser) const {
  if(ser) return ser->print_string(*this);
  return *this;
}



bool LDRstring::parsevalstring(const STD_string& str, const LDRserBase* ser) {
  if(ser) *this=ser->parse_string(str);
  else *this=str;
  return true;
}



STD_string LDRstring::get_parx_code(parxCodeType type) const {
#ifdef PARAVISION_PLUGIN
  STD_string result=LDRbase::get_parx_code(type);
  if(type==parx_def) result=get_parx_def_string(get_typeInfo(true),1);
  if(type==parx_passval) result="sprintf("+get_jdx_props().parx_equiv_name+",\"%s\","+get_label()+");\n";
  return result;
#else
  return "";
#endif
}


#ifndef NO_UNIT_TEST
class LDRstringTest : public UnitTest {

 public:
  LDRstringTest() : UnitTest("LDRstring") {}

 private:
  bool check() const {
    Log<UnitTest> odinlog(this,"check");

    LDRstring teststr1("value","teststr1");
    LDRstring teststr2("value","teststr2");

    // testing LDRstring::print
    STD_string expected="##$teststr1=value\n";
    STD_string printed=teststr1.print(LDRserJDX(notBroken));
    if(printed!=expected) {
      ODINLOG(odinlog,errorLog) << "print() failed: got >" << printed << "<, but expected >" << expected << "<" << STD_endl;
      return false;
    }
    expected="##$teststr2=( "+itos(_BRUKER_MODE_STRING_MIN_SIZE_)+" )\n<value>\n";
    printed=teststr2.print(LDRserJDX(bruker));
    if(printed!=expected) {
      ODINLOG(odinlog,errorLog) << "print() failed: got >" << printed << "<, but expected >" << expected << "<" << STD_endl;
      return false;
    }

    // testing LDRstring::parse
    LDRblock block;
    block.append(teststr1);
    block.append(teststr2);


    int parseresult=block.parseblock("##TITLE=block\n##$teststr1=( 64 )\nparseval_bruker\n##$teststr2=( 64 )\n<parseval_bruker>\n$$ Comment\n##$dummy=dummyval\n##END=", LDRserJDX(bruker));
    if(parseresult!=2) {
      ODINLOG(odinlog,errorLog) << "LDRblock::parseblock(bruker) failed: parseresult=" << parseresult << "!=" << 2 << STD_endl;
      return false;
    }
    if(STD_string(block.get_label())!="block") {
      ODINLOG(odinlog,errorLog) << "LDRblock::get_label() failed: >" << block.get_label() << "< != >block<" << STD_endl;
      return false;
    }
    if(STD_string(teststr1)!=STD_string(teststr2)) {
      ODINLOG(odinlog,errorLog) << "after block.parseblock(): >" << STD_string(teststr1) << "< != >" << STD_string(teststr2) << "<" << STD_endl;
      return false;
    }


    parseresult=block.parseblock("##TITLE=block\n##$teststr1=parseval_notBroken\n##$teststr2=<parseval_notBroken>\n##END=", LDRserJDX(notBroken));
    if(parseresult!=2) {
      ODINLOG(odinlog,errorLog) << "LDRblock::parseblock(notBroken) failed: parseresult=" << parseresult << "!=" << 2 << STD_endl;
      return false;
    }
    if(STD_string(teststr1)!=STD_string(teststr2)) {
      ODINLOG(odinlog,errorLog) << "after block.parseblock(): >" << STD_string(teststr1) << "< != >" << STD_string(teststr2) << "<" << STD_endl;
      return false;
    }

    return true;
  }

};


void alloc_LDRstringTest() {new LDRstringTest();} // create test instance
#endif


//////////////////////////////////////////////////////////////////

LDRbool::LDRbool(bool flag, const STD_string& name)
  : val(flag) {
  set_label(name);
}


LDRbool& LDRbool::operator = (const LDRbool& jb) {
  LDRbase::operator = (jb);
  val=jb.val;
  return *this;
}

bool LDRbool::parsevalstring(const STD_string& parstring, const LDRserBase*) {
  Log<LDRcomp> odinlog(this,"parsevalstring");
  STD_string yesnostr(shrink(tolowerstr(parstring)));
  ODINLOG(odinlog,normalDebug) << "yesnostr=" << yesnostr << STD_endl;
  if(yesnostr=="yes" || yesnostr=="true") val=true;
  if(yesnostr=="no" || yesnostr=="false") val=false;
  ODINLOG(odinlog,normalDebug) << "this=" << bool(*this) << STD_endl;
  return true;
}

STD_string LDRbool::printvalstring(const LDRserBase*) const {
  if(val) return "Yes";
  else return "No";
}


#ifndef NO_UNIT_TEST
class LDRboolTest : public UnitTest {

 public:
  LDRboolTest() : UnitTest("LDRbool") {}

 private:
  bool check() const {
    Log<UnitTest> odinlog(this,"check");

    LDRbool testbool(false,"testbool");
    testbool=true;
    STD_string expected="##$testbool=Yes\n";
    STD_string printed=testbool.print();
    if(printed!=expected) {
      ODINLOG(odinlog,errorLog) << "print() failed: got >" << printed << "<, but expected >" << expected << "<" << STD_endl;
      return false;
    }
    LDRblock boolblock;
    boolblock.append(testbool);
    boolblock.parseblock("##TITLE=boolblock\n##$testbool=No$$ ##mycomment \n##END="); // also test JCAMP-DX comments (remainder of line starting with '$$')
    if(bool(testbool)!=false) {
      ODINLOG(odinlog,errorLog) << "after boolblock.parseblock(): for bool " << bool(testbool) << "!=" << false << STD_endl;
      return false;
    }

    return true;
  }

};

void alloc_LDRboolTest() {new LDRboolTest();} // create test instance
#endif

///////////////////////////////////////////////////////////////

LDRenum::LDRenum(const STD_string& first_entry, const STD_string& name) {
  add_item(first_entry);
  set_label(name);
}


LDRenum& LDRenum::operator = (const LDRenum& je) {
  LDRbase::operator = (je);
  entries=je.entries;
  for(STD_map<int,STD_string>::const_iterator it=entries.begin();it!=entries.end();++it) {
    if ( (it->first) == (je.actual->first) ) actual=it;
  }
  return *this;
}
  
LDRenum& LDRenum::add_item(const STD_string& item, int index) {
  if(item=="") return *this; 
  int n=0;
  if(index<0) {
    for(STD_map<int,STD_string>::const_iterator it=entries.begin();it!=entries.end();++it) {
      if ( it->first > n ) n=it->first;
    }
    if(entries.size()) n++;
  } else n=index;

  entries[n]=item;
  actual=entries.find(n);
  return *this;
}
  
LDRenum& LDRenum::set_actual(const STD_string& item) {
  STD_map<int,STD_string>::const_iterator it;
  for(it=entries.begin();it!=entries.end();++it) {
    if ( (it->second) == item ) actual=it;
  }
  return *this;
}

LDRenum& LDRenum::set_actual(int index) {
  STD_map<int,STD_string>::const_iterator it;
  for(it=entries.begin();it!=entries.end();++it) {
    if ( (it->first) == index ) actual=it;
  }
  return *this;
}


LDRenum& LDRenum::clear() {
  entries.clear();
  actual=entries.end();
  return *this;
}

LDRenum::operator int () const {
  if(actual!=entries.end()) return actual->first;
  else return 0;
}

LDRenum::operator STD_string () const {
  if(actual!=entries.end()) return actual->second;
  else return "";
}

static STD_string notext("none");
  
const STD_string& LDRenum::get_item(unsigned int index) const {
  STD_map<int,STD_string>::const_iterator it=entries.begin();
  for(unsigned int i=0; i<index; i++) {
    ++it;
    if(it==entries.end()) return notext;
  }
  return it->second;
}

unsigned int LDRenum::get_item_index() const {
  unsigned int result=0;
  for(STD_map<int,STD_string>::const_iterator it=entries.begin();it!=entries.end();++it) {
    if ( it==actual ) return result;
    result++;
  }
  return 0;
}

LDRenum& LDRenum::set_item_index(unsigned int index) {
  STD_map<int,STD_string>::const_iterator it=entries.begin();
  for(unsigned int i=0; i<index; i++) {
    if(it==entries.end()) return *this;
    ++it;
  }
  actual=it;
  return *this;    
}



bool LDRenum::parsevalstring(const STD_string& parstring, const LDRserBase*) {
  STD_string newpar(parstring);

  // check whether parstring is already there:
  bool already_there=false;
  STD_map<int,STD_string>::const_iterator it;
  for(it=entries.begin();it!=entries.end();++it) {
    if ( (it->second) == newpar ) {
      already_there=true;
      actual=it;
    }
  }
  if(!already_there && (entries.size()==0) ) add_item(newpar);
  return true;
}


svector LDRenum::get_alternatives() const {
  unsigned int n=n_items();
  svector result; result.resize(n);
  for(unsigned int i=0; i<n; i++) {
    result[i]=get_item(i);
  }
  return result;
}



STD_string LDRenum::printvalstring(const LDRserBase*) const {
  if(actual!=entries.end()) return (actual->second);
  else return "emptyEnum";
}
  

#ifndef NO_UNIT_TEST
class LDRenumTest : public UnitTest {

 public:
  LDRenumTest() : UnitTest("LDRenum") {}

 private:
  bool check() const {
    Log<UnitTest> odinlog(this,"check");

    // testing LDRenum
    LDRenum testenum("","testenum");
    testenum.add_item("item7",7);
    testenum.add_item("item0",0);
    testenum.add_item("item5",5);
    testenum.add_item("item1",1);
    testenum="item5";
    STD_string expected="##$testenum=item5\n";
    STD_string printed=testenum.print();
    if(printed!=expected) {
      ODINLOG(odinlog,errorLog) << "testenum::print() failed: got >" << printed << "<, but expected >" << expected << "<" << STD_endl;
      return false;
    }
    testenum=7;
    expected="##$testenum=item7\n";
    printed=testenum.print();
    if(printed!=expected) {
      ODINLOG(odinlog,errorLog) << "testenum::print() failed: got >" << printed << "<, but expected >" << expected << "<" << STD_endl;
      return false;
    }
    LDRblock enumblock;
    enumblock.append(testenum);
    enumblock.parseblock("##TITLE=enumblock\n##testenum=item1\n$$##testenum=item5\n##END="); // also test JCAMP-DX comments (remainder of line starting with '$$')
    if((int)testenum!=1) {
      ODINLOG(odinlog,errorLog) << "after enumblock.parseblock(): for LDRenum " << (int)testenum << "!=" << 1 << STD_endl;
      return false;
    }

    return true;
  }

};

void alloc_LDRenumTest() {new LDRenumTest();} // create test instance
#endif


///////////////////////////////////////////////////////////////

LDRaction::LDRaction(bool init_state, const STD_string& name)
  : state(init_state) {
  LDRbase::set_filemode(exclude); // Do NOT call virtual function in constructor!
  set_label(name);
}

LDRaction& LDRaction::operator = (const LDRaction& ja) {
  LDRbase::operator = (ja);
  return *this;
}

LDRaction::operator bool () const {
  bool result=state;
  state=false;
  return result;
}

bool LDRaction::parsevalstring(const STD_string& parstring, const LDRserBase*) {
  STD_string actionstr(shrink(tolowerstr(parstring)));
  if(actionstr=="busy") state=true;
  else state=false;
  return true;
}

STD_string LDRaction::printvalstring(const LDRserBase*) const {
  if(state) return "CLICK_HERE";
  else return "NOW";
}


///////////////////////////////////////////////////////////////

LDRfileName::LDRfileName(const STD_string& filename, const STD_string& name)
 :   LDRstring(filename, name) {
  common_init();
  normalize(filename, dir, *this, dirname_cache, basename_cache, suffix_cache);
}


LDRfileName::LDRfileName (const LDRfileName& jf) {
  common_init();
  LDRfileName::operator = (jf);
}


LDRfileName& LDRfileName::operator = (const STD_string& filename) {
  normalize(filename, dir, *this, dirname_cache, basename_cache, suffix_cache);
  return *this;
}


LDRfileName& LDRfileName::operator = (const LDRfileName& jf) {
  LDRstring::operator = (jf);
  dir=jf.dir;
  normalize(jf, dir, *this, dirname_cache, basename_cache, suffix_cache);
  defaultdir=jf.defaultdir;
  return *this;
}


bool LDRfileName::exists() const {
  Log<LDRcomp> odinlog("LDRfileName","exists");
  bool result=false;
#ifndef NO_FILEHANDLING
  if(is_dir()) {
    result=checkdir(c_str());
    ODINLOG(odinlog,normalDebug) << "file >" << c_str() << "<, result(dir)=" << result << STD_endl;
  } else {
    LONGEST_INT fsize=filesize(c_str());
    ODINLOG(odinlog,normalDebug) << "file >" << c_str() << "<, fsize=" << fsize << STD_endl;
    result=(fsize>=0);
  }
#endif
  return result;
}



STD_string LDRfileName::get_basename_nosuffix() const {
  STD_string result(get_basename());
  if(get_suffix()!="") return replaceStr(result,"."+get_suffix(),"");
  return result;
}


LDRfileName& LDRfileName::set_defaultdir(const STD_string& defdir) {
  STD_string dummystr;
  normalize(defdir, true, defaultdir, dummystr, dummystr, dummystr);
  return *this;
}

bool LDRfileName::parsevalstring(const STD_string& parstring, const LDRserBase*) {
  normalize(parstring, dir, *this, dirname_cache, basename_cache, suffix_cache);
  return true;
}


void LDRfileName::normalize(const STD_string& fname, bool dir, STD_string& result, STD_string& result_dirname, STD_string& result_basename, STD_string& result_suffix) {
  Log<LDRcomp> odinlog("LDRfileName","normalize");

  STD_string tmpstr(fname); // mutable copy

  // remove quotes
  tmpstr=replaceStr(tmpstr, "\"","");
  tmpstr=replaceStr(tmpstr, "\'","");

   // remove preceeding blanks
  int beginpos=textbegin(tmpstr,0);
  if(beginpos<0) beginpos=0;
  tmpstr=tmpstr.substr(beginpos,tmpstr.length()-beginpos);

  // check whether we have absolute or relative path
  bool abspath=false;

  STD_string drive;
#ifdef USING_WIN32
  // replace slashes that are left on Windows
  tmpstr=replaceStr(tmpstr,"/",SEPARATOR_STR);
  if(tmpstr.length()>=2 && (tmpstr[1]==':') ) {
    drive=STD_string(1,tmpstr[0])+":"; // store 
    tmpstr=tmpstr.substr(2,tmpstr.length()-2); // and strip off drive letter
    abspath=true; // accept drive-letter notation
  }
#endif

  if(tmpstr.length()>=1 && (tmpstr[0]==SEPARATOR_CHAR) ) abspath=true;

  ODINLOG(odinlog,normalDebug) << "tmpstr/abspath=>" << tmpstr << "</" << abspath << STD_endl;

  // Decompose path
  svector toks=tokens(tmpstr,SEPARATOR_CHAR);
  int ntoks=toks.size();
  ODINLOG(odinlog,normalDebug) << "ntoks/toks=" << ntoks << "/" << toks.printbody() << STD_endl;

  // set suffix, only change/reset suffix if we actually have a file
  if(ntoks) {
    result_suffix="";
    STD_string fname="XXX"+toks[ntoks-1]; // Quick hack in case filename consists only of '.' and the suffix
    svector sufftoks=tokens(fname,'.');
    if(sufftoks.size()>1) result_suffix=tolowerstr(sufftoks[sufftoks.size()-1]);
  }

  if(dir) result_suffix=""; // No suffix for dirs

  // set defaults
  result=drive;
  result_dirname=drive;
  result_basename="";

  if(!abspath && (ntoks==1)) result_dirname="."; // File is in PWD

  // Recompose path and store dirname/basename
  if(ntoks) {

    if(abspath) {
      result+=SEPARATOR_STR;
      result_dirname+=SEPARATOR_STR;
    }

    ODINLOG(odinlog,normalDebug) << "result/result_dirname(pre loop)=>" << result << "<>" << result_dirname << "<" << STD_endl;

    for(int i=0; i<ntoks; i++) {
      result+=toks[i];

      if(i==(ntoks-1)) {
        result_basename=toks[i]; // Store last token in basename
      } else {
        result_dirname+=toks[i];
        result+=SEPARATOR_STR;
        if(i<(ntoks-2)) result_dirname+=SEPARATOR_STR;
      }
    }

    ODINLOG(odinlog,normalDebug) << "result/result_dirname(post loop)=>" << result << "<>" << result_dirname << "<" << STD_endl;
  }

}



#ifndef NO_UNIT_TEST
class LDRfileNameTest : public UnitTest {

 public:
  LDRfileNameTest() : UnitTest("LDRfileName") {}

 private:
  bool check() const {
    Log<UnitTest> odinlog(this,"check");

    LDRfileName teststr1=STD_string("image.bmp");

    STD_string expected="image.bmp";
    STD_string printed=teststr1;
    if(printed!=expected) {
      ODINLOG(odinlog,errorLog) << "LDRfileName::STD_string failed: got >" << printed << "<, but expected >" << expected << "<" << STD_endl;
      return false;
    }


    expected="bmp";
    printed=teststr1.get_suffix();
    if(printed!=expected) {
      ODINLOG(odinlog,errorLog) << "LDRfileName::get_suffix()[from filename] failed: got >" << printed << "<, but expected >" << expected << "<" << STD_endl;
      return false;
    }


    expected="cpp";
    teststr1.set_suffix(expected);
    printed=teststr1.get_suffix();
    if(printed!=expected) {
      ODINLOG(odinlog,errorLog) << "LDRfileName::get_suffix()[when set] failed: got >" << printed << "<, but expected >" << expected << "<" << STD_endl;
      return false;
    }

    teststr1=STD_string(".bmp");
    expected="bmp";
    printed=teststr1.get_suffix();
    if(printed!=expected) {
      ODINLOG(odinlog,errorLog) << "LDRfileName::get_suffix()[empty basename] failed: got >" << printed << "<, but expected >" << expected << "<" << STD_endl;
      return false;
    }


    teststr1=STD_string("//tmp///test");
    expected=STD_string(SEPARATOR_STR)+"tmp";
    printed=teststr1.get_dirname();
    if(printed!=expected) {
      ODINLOG(odinlog,errorLog) << "LDRfileName::get_dirname() failed: got >" << printed << "<, but expected >" << expected << "<" << STD_endl;
      return false;
    }

    teststr1=STD_string("subdir///file");

    expected="subdir"+STD_string(SEPARATOR_STR)+"file";
    printed=teststr1;
    if(printed!=expected) {
      ODINLOG(odinlog,errorLog) << "LDRfileName::STD_string failed: got >" << printed << "<, but expected >" << expected << "<" << STD_endl;
      return false;
    }

    expected="file";
    printed=teststr1.get_basename();
    if(printed!=expected) {
      ODINLOG(odinlog,errorLog) << "LDRfileName::get_basename() failed: got >" << printed << "<, but expected >" << expected << "<" << STD_endl;
      return false;
    }

    expected="subdir";
    printed=teststr1.get_dirname();
    if(printed!=expected) {
      ODINLOG(odinlog,errorLog) << "LDRfileName::get_dirname() failed: got >" << printed << "<, but expected >" << expected << "<" << STD_endl;
      return false;
    }


#ifdef USING_WIN32
    teststr1=STD_string("C:");
    expected="C:";
    printed=STD_string(teststr1);
    if(printed!=expected) {
      ODINLOG(odinlog,errorLog) << "LDRfileName(WIN32): failed: got >" << printed << "<, but expected >" << expected << "<" << STD_endl;
      return false;
    }
#endif

    return true;
  }

};

void alloc_LDRfileNameTest() {new LDRfileNameTest();} // create test instance
#endif


///////////////////////////////////////////////////////////////

LDRformula::LDRformula(const STD_string& formula, const STD_string& name)
 : LDRstring(formula,name) {
}

LDRformula& LDRformula::operator = (const LDRformula& jf) {
  LDRstring::operator = (jf);
  syntax = jf.syntax;
  return *this;
}

