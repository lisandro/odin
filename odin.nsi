; Script to generate a distributable Odin package for Windows
; using the Nullsoft installer

; odin.nsi

;--------------------------------

; The name of the installer
Name "Odin"

; The file to write
OutFile "odin-VERSION.exe"

SetCompressor bzip2

RequestExecutionLevel admin  ; fixes uninstall problem of shortcuts on Windows Vista/7

; The default installation directory
;InstallDir $PROGRAMFILES\Odin
InstallDir C:\Odin
DirText "IMPORTANT: Please uninstall any previous version of Odin and chose a directory path name WITHOUT space characters for installation!"

;--------------------------------

Function IsUserAdmin
Push $R0
Push $R1
Push $R2
 
ClearErrors
UserInfo::GetName
IfErrors Win9x
Pop $R1
UserInfo::GetAccountType
Pop $R2
 
StrCmp $R2 "Admin" 0 Continue
StrCpy $R0 "true"
Goto Done
 
Continue:
StrCmp $R2 "" Win9x
StrCpy $R0 "false"
;MessageBox MB_OK 'User "$R1" is in the "$R2" group'
Goto Done
 
Win9x:
;MessageBox MB_OK "Error! This DLL can't run under Windows 9x!"
StrCpy $R0 "true"
 
Done:
;MessageBox MB_OK 'User= "$R1"  AccountType= "$R2"  IsUserAdmin= "$R0"'
 
Pop $R2
Pop $R1
Exch $R0
FunctionEnd

;--------------------------------


; Registry key to check for directory (so if you install again, it will 
; overwrite the old one automatically)
InstallDirRegKey HKLM "Software\Odin" "Install_Dir"

;--------------------------------

; Pages

Page components
Page directory
Page instfiles

UninstPage uninstConfirm
UninstPage instfiles

;--------------------------------

Section ""
  Call IsUserAdmin
  Pop $R0   ; at this point $R0 is "true" or "false"
  StrCmp $R0 "false" 0 +3
    MessageBox MB_OK|MB_ICONEXCLAMATION "Please install with Administrator rights."
    Quit
SectionEnd


;--------------------------------

; The stuff to install
Section "Odin (required)"

  SectionIn RO

  ; Set output path to the installation directory.
  SetOutPath $INSTDIR

  ; Put file there
  File /r "bin"
  File /r "include"
  File /r "lib"
  File /r "share"
  File /r "src"
  File /r "MINGW32NAME"
  File /r "msys"


  ; Write the installation path into the registry
  WriteRegStr HKLM SOFTWARE\Odin "Install_Dir" "$INSTDIR"


  ; Write the uninstall keys for Windows
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Odin" "DisplayName" "NSIS Odin"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Odin" "UninstallString" '"$INSTDIR\uninstall.exe"'
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Odin" "NoModify" 1
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Odin" "NoRepair" 1
  WriteUninstaller "uninstall.exe"

SectionEnd

; Optional section (can be disabled by the user)
Section "Start Menu Shortcuts"

  CreateDirectory "$SMPROGRAMS\Odin"
  CreateShortCut "$SMPROGRAMS\Odin\Uninstall.lnk" "$INSTDIR\uninstall.exe"
  CreateShortCut "$SMPROGRAMS\Odin\Odin.lnk" "$INSTDIR\bin\odin.exe"
  CreateShortCut "$SMPROGRAMS\Odin\Pulsar.lnk" "$INSTDIR\bin\pulsar.exe"
;  CreateShortCut "$SMPROGRAMS\Odin\MiView.lnk" "$INSTDIR\bin\miview.exe"
;  CreateShortCut "$SMPROGRAMS\Odin\GeoEdit.lnk" "$INSTDIR\bin\geoedit.exe"
  CreateShortCut "$SMPROGRAMS\Odin\Manual.lnk" "$INSTDIR\share\doc\odin\manual\html\index.html"
  CreateShortCut "$SMPROGRAMS\Odin\Shell.lnk" "$INSTDIR\bin\shell.bat" "$INSTDIR"

SectionEnd

;--------------------------------

; Uninstaller

Section "Uninstall"

  ; Remove registry keys
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Odin"
  DeleteRegKey HKLM SOFTWARE\Odin

  ; Remove files and uninstaller
  Delete "$INSTDIR\*.*"
  RMDir  /r "$INSTDIR\*"

  ; Remove shortcuts, if any
  Delete "$SMPROGRAMS\Odin\*.*"

  ; Remove directories used
  RMDir "$SMPROGRAMS\Odin"

  RMDir "$INSTDIR"

SectionEnd
