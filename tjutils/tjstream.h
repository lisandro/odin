/***************************************************************************
                          tjstream.h  -  description
                             -------------------
    begin                : Sun Jun 13 2004
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef TJSTREAM_H
#define TJSTREAM_H


#include <tjutils/tjstd.h>

// Macros, includes and typedefs are handled in tjstd.h to avoid circular dependencies


typedef STD_ostream& (*streammanipulator)(STD_ostream&);


/**
  * @addtogroup tjutils
  * @{
  */

#ifdef STREAM_REPLACEMENT

class tjstd_ostream {

 public:
  tjstd_ostream() {}
  virtual ~tjstd_ostream() {}

  inline tjstd_ostream& operator<<(char v)          {append2buff(STD_string(1,v)); return *this;}
  inline tjstd_ostream& operator<<(unsigned char v) {append2buff(STD_string(1,v)); return *this;}
  inline tjstd_ostream& operator<<(signed char v)   {append2buff(STD_string(1,v)); return *this;}
  inline tjstd_ostream& operator<<(const char* v)   {append2buff(v);               return *this;}
  inline tjstd_ostream& operator<<(int v)           {buff_int(v);                  return *this;}
  inline tjstd_ostream& operator<<(unsigned int v)  {buff_int(v);                  return *this;}
  inline tjstd_ostream& operator<<(long v)          {buff_int(v);                  return *this;}
  inline tjstd_ostream& operator<<(unsigned long v) {buff_int(v);                  return *this;}
#ifdef HAVE_LONG_LONG
  inline tjstd_ostream& operator<<(long long v)     {buff_int(v);                  return *this;}
#endif
  inline tjstd_ostream& operator<<(short v)         {buff_int(v);                  return *this;}
  inline tjstd_ostream& operator<<(unsigned short v){buff_int(v);                  return *this;}
  inline tjstd_ostream& operator<<(bool v)          {if(v) append2buff("true"); else append2buff("false"); return *this;}
  inline tjstd_ostream& operator<<(double v)        {buff_float(v);                return *this;}
  inline tjstd_ostream& operator<<(float v)         {buff_float(v);                return *this;}
  inline tjstd_ostream& operator<<(streammanipulator v) {v(*this);                 return *this;}


 private:

  // disable copying
  tjstd_ostream(const tjstd_ostream&) {}
  tjstd_ostream& operator = (const tjstd_ostream&) {return *this;}

  virtual void append2buff(const STD_string& str) {} // do nothing by default, i.e. for cout/cerr

  void buff_float(float f);
  void buff_int(int i);

};

////////////////////////////////////////////////////////////////////////////

class tjstd_ostringstream : public tjstd_ostream {

 public:
  tjstd_ostringstream() {}
  ~tjstd_ostringstream() {}

  const STD_string& str() const {return buff;}

 private:

  // disable copying
  tjstd_ostringstream(const tjstd_ostringstream&) {}
  tjstd_ostringstream& operator = (const tjstd_ostringstream&) {return *this;}

  // overloaded from tjstd_ostream
  void append2buff(const STD_string& str) {buff+=str;}

  STD_string buff;
};

////////////////////////////////////////////////////////////////////////////

class tjstd_ofstream : public tjstd_ostream {

 public:

  tjstd_ofstream(const char* filename="");
  ~tjstd_ofstream() {close();}

  void close();

  bool bad() {return !file_ptr;}

 private:

  // disable default construction and copying
  tjstd_ofstream(const tjstd_ofstream&) {}
  tjstd_ofstream& operator = (const tjstd_ofstream&) {return *this;}

  // overloaded from tjstd_ostream
  void append2buff(const STD_string& str);

  void* file_ptr;

};

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////


class tjstd_istream {

 public:
  tjstd_istream() {}

  inline tjstd_istream& operator>>(char)          {return *this;}
  inline tjstd_istream& operator>>(unsigned char) {return *this;}
  inline tjstd_istream& operator>>(signed char)   {return *this;}
  inline tjstd_istream& operator>>(const char *)  {return *this;}
  inline tjstd_istream& operator>>(int)           {return *this;}
  inline tjstd_istream& operator>>(unsigned int)  {return *this;}
  inline tjstd_istream& operator>>(long)          {return *this;}
  inline tjstd_istream& operator>>(unsigned long) {return *this;}
#ifdef HAVE_LONG_LONG
  inline tjstd_istream& operator>>(long long)     {return *this;}
#endif
  inline tjstd_istream& operator>>(short)         {return *this;}
  inline tjstd_istream& operator>>(unsigned short){return *this;}
  inline tjstd_istream& operator>>(bool)          {return *this;}
  inline tjstd_istream& operator>>(double)        {return *this;}
  inline tjstd_istream& operator>>(float)         {return *this;}

  inline tjstd_istream& get(char&) {return *this;}
  inline tjstd_istream& putback(char) {return *this;}

  operator void* () const {return 0;} // stream is always empty

};

inline tjstd_istream& tjstd_getline(tjstd_istream& is, STD_string&) {return is;}



////////////////////////////////////////////////////////////////////////////

class tjstd_ifstream : public tjstd_istream {

 public:
  tjstd_ifstream(const char*  ="") {}
  void close() {}
  bool bad() {return false;}
};

////////////////////////////////////////////////////////////////////////////


static tjstd_ostream tjstd_cout;
static tjstd_ostream tjstd_cerr;
static tjstd_istream tjstd_cin;

inline STD_ostream& tjstd_endl(STD_ostream& os) {return os << "\n";}
inline STD_ostream& tjstd_flush(STD_ostream& os) {return os;}
inline const char* tjstd_setprecision(unsigned int) {return "";}

#endif

/** @}
  */
#endif

