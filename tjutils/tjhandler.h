/***************************************************************************
                          tjhandler.h  -  description
                             -------------------
    begin                : Mon Aug 19 2002
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef TJHANDLER_H
#define TJHANDLER_H

#include <tjutils/tjutils.h>
#include <tjutils/tjthread.h>

/**
  * @addtogroup tjutils
  * @{
  */


class HandlerComponent {
 public:
  static const char* get_compName();
};


//////////////////////////////////////////////////////////////

template<class I> class Handled; // forawrd declaration



/////////////////////////////////////////////

template<class I>
class Handler {


 public:

  Handler();

  Handler(const Handler& handler);

  Handler& operator = (const Handler& handler);

  ~Handler();

  const Handler& clear_handledobj() const;

  const Handler& set_handled(I handled) const;

  I get_handled() const;


 private:
  friend class Handled<I>;

  const Handler& handled_remove(Handled<I>* handled) const;

  mutable I handledobj;

};


/////////////////////////////////////////////


template<class I>
class Handled {

 public:
  Handled();
  ~Handled();

  bool is_handled() const {return bool(handlers.size());}
  
 private:
  friend class Handler<I>;
  
  const Handled& set_handler(const Handler<I>& handler) const;
  const Handled& erase_handler(const Handler<I>& handler) const;

  mutable STD_list< const Handler<I>* > handlers;
};


////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////

class SingletonBase; // forward declaration

// mapping between singletons and labels
typedef STD_map<STD_string, SingletonBase*> SingletonMap;


class SingletonBase {

 public:
  virtual void* get_ptr() const = 0;

  static SingletonMap* get_singleton_map();
  static void set_singleton_map_external(SingletonMap* extmap);

 protected:
  SingletonBase();
  virtual ~SingletonBase() {}

  static STD_string get_singleton_label(SingletonBase* sing_ptr);
  static void* get_external_map_ptr(const STD_string& sing_label);

  static SingletonMap* singleton_map;
  static SingletonMap* singleton_map_external;
};


////////////////////////////////////////////////////////////////////


/**
  * Helper class to enable locking of resources while accessing them with pointer-like syntax.
  */
template<class T>
class LockProxy {

 public:
  LockProxy(volatile T* r, Mutex* m) : presource(const_cast<T*>(r)), pmutex(m) {if(pmutex) pmutex->lock();}
  ~LockProxy() {if(pmutex) pmutex->unlock();}
  T* operator -> () {return presource;}

 private:
  T* presource;
  Mutex* pmutex;
};


////////////////////////////////////////////////////////////////////



/**
  * Template class to manage singleton objects of type T,
  * i.e. those objects which are exist only once in the
  * program. The instantiation must be a static field of
  * of a certain class. The init/destroy member functions
  * are used to create/destroy the singleton.
  * If 'thread_safe' is set to 'true', use a mutex to manage multi-threaded
  * access to the singleton object.
  *
  * Its main purpose is to
  * share singletons across DLL boundaries on Windows
  * and/or thread-safe access to global resources.
  * This class has an pointer-to-T-like interface.
  */
template<class T, bool thread_safe> // VxWorks does not support default-template args
class SingletonHandler : public SingletonBase {

 public:

  SingletonHandler() {
    // do nothing because members are already initialized by init()
  }


/**
  * Initialize the singleton with the unique identifier 'unique_label'.
  * The singleton object will be allocated if it does not already exist
  * somewhere in the program.
  */
  void init (const char* unique_label);

/**
  * Deletes the singleton
  */
  void destroy ();


/**
  * Copies this to 'destination'
  */
  void copy(T& destination) const;

/**
  * Returns unlocked ptr to object (know what you are doing)
  */
  T* unlocked_ptr() {return get_map_ptr();}


  // emulate pointer syntax:

/**
  * Returns mutex-locked pointer to handled singleton
  */
  LockProxy<T> operator -> () {return LockProxy<T>(get_map_ptr(),mutex);}

/**
  * Returns read-only pointer to handled singleton
  */
  const T* operator -> () const {return get_map_ptr();}

/**
  * Returns whether singleton is already initialized
  */
  operator bool () const {return (bool)get_map_ptr();}



 private:

  // implement virtual function of SingletonBase
  void* get_ptr() const {return ptr;}

  T* get_map_ptr() const;

  // Use pointers to avoid the need for static initialization
  mutable T* ptr;
  STD_string* singleton_label;
  Mutex* mutex;

};



/** @}
  */
#endif
