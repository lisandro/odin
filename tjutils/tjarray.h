 /***************************************************************************
                          tjvector.h  -  description
                             -------------------
    begin                : Thu Feb 22 2001
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/


#ifndef TJARRAY_H
#define TJARRAY_H



#include <tjutils/tjvector.h>
#include <tjutils/tjlog.h>


/**
  * @addtogroup tjutils
  * @{
  */

/**
  *
  * This class is an extent vector for arrays, i.e. it is used to hold
  * dimension information of multidimensional arrays.
  * It is a vector of integer numbers which describe the extent of the
  * array in each direction/dimension.
  * It can also be used to index a certain element in the multidimensional array.
  */
class ndim : public STD_vector<unsigned long> {

public:

/**
  *
  * Constructs an extent vector of size 'd'.
  */
  ndim (unsigned long d=0);

/**
  *
  * Copy constructor.
  */
  ndim (const ndim& mm) : STD_vector<unsigned long>(mm) {}

/**
  *
  * Constructs an extent vector by parsing the string 's'. The string
  * must have the syntax (i,j,...) e.g. (3,2,5) to describe a 3-dimensional array.
  */
  ndim (const STD_string& s);

/**
  *
  * The number of dimensions, i.e. the length of the extent vector.
  */
  unsigned long dim() const {return size();}

/**
  *
  * Returns the extent vector as a formatted string, e.g. (3,2,5).
  */
  operator STD_string () const;

/**
  *  Stream operator
  */
  friend STD_ostream& operator << (STD_ostream& s,const ndim& nn) { return s << STD_string(nn);}

/**
  *
  * Prefix -- operator: Strips of the first dimension/index.
  */
  ndim& operator -- ( );

/**
  *
  * Postfix -- operator: Strips of the last dimension/index.
  */
  ndim& operator -- (int);

/**
  *
  * Add new dimension with extent 'e' either as last
  * or first dim, depending on 'first'
  */
  ndim& add_dim ( unsigned long e, bool first=false );

/**
  *
  * Returns the total product of the vector.
  */
  unsigned long total() const;

/**
  *
  * Converts an index vector to the linear index in the array.
  */
  unsigned long extent2index(const ndim& mm) const;

/**
  *
  * Converts the linear index in the array to an index vector.
  */
  ndim index2extent(unsigned long index) const;

/**
  *
  * Returns true if nn and this are equal, otherwise false.
  */
  bool operator == (const ndim& nn) const;

/**
  *
  * Returns true if nn and this are unequal, otherwise false.
  */
  bool operator != (const ndim& nn) const;

/**
  *
  * Reduce to dimension dim, e.g. ( 3, 4, 7).reduce(2) -> ( 12, 7 )
  */
  ndim& reduce ( unsigned long dim );

/**
  *
  * Resize to essential dims, e.g. ( 1, 3, 1, 7).autosize() -> ( 3, 7 )
  */
  ndim& autosize ();
};



/////////////////////////////////////////////////////////////////////////////


/**
  *
  * This class represents a multidimensional array.
  * It is implemented by deriving it from a one dimensional
  * vector which holds the data and adding a member of type 'ndim'
  * that contains the information about the extent in each dimension.
  */
template<class V, class T> class tjarray : public V {

public:


/**
  *
  * default constructor
  */
  tjarray ();


/**
  *
  * constructs an array of dimensionality 'dim' and allocates memory for i,j,... values
  */
  tjarray (unsigned long n1);
  tjarray (unsigned long n1, unsigned long n2);
  tjarray (unsigned long n1, unsigned long n2, unsigned long n3);
  tjarray (unsigned long n1, unsigned long n2, unsigned long n3, unsigned long n4);
  tjarray (unsigned long n1, unsigned long n2, unsigned long n3, unsigned long n4, unsigned long n5);


/**
  *
  * copy constructor
  */
  tjarray (const tjarray<V,T> &ta);


/**
  *
  * conversion from fvector to 1-dim array
  */
  tjarray (const V& sv);


/**
  *
  * constructs an array according to an extent 'nn'
  */
  tjarray (const ndim &nn);


/**
  *  Copy assignment
  */
  tjarray<V,T>& operator = (const tjarray<V,T>& ta);

/**
  *
  * assigns 'value' to all elements
  */
  tjarray<V,T>& operator = (const T& value);


/**
  *
  * Resize the array, that is create a 1-dim array with the given new size (virtual)
  */
  V& resize(unsigned int newsize);


/**
  *
  * Redimensionalize and resize the array according to the given extent vector.
  * If the total number of values does not change, the array is simply reshaped
  * but the values are preserved.
  */
  tjarray<V,T>& redim (const ndim &nn);


/**
  *
  * Redimensionalize and resize the array according to the given extent vector
  * If the total number of values does not change, the array is simply reshaped
  * but the values are preserved.
  */
  tjarray<V,T>& redim (unsigned long n1);
  tjarray<V,T>& redim (unsigned long n1, unsigned long n2);
  tjarray<V,T>& redim (unsigned long n1, unsigned long n2, unsigned long n3);
  tjarray<V,T>& redim (unsigned long n1, unsigned long n2, unsigned long n3, unsigned long n4);
  tjarray<V,T>& redim (unsigned long n1, unsigned long n2, unsigned long n3, unsigned long n4, unsigned long n5);


/**
  *
  * Remove all dimensions with a size of 1 from the extent vector
  */
  tjarray<V,T>& autosize () {extent.autosize(); return *this;}


/**
  *
  * Get the extent vector, i.e. the information about the dimensions
  */
  const ndim& get_extent () const;


/**
  *
  * returns the element indexed by the extent vector ii
  */
  T& operator () (const ndim& ii);


/**
  *
  * returns the readonly element indexed by the extent vector ii
  */
  const T& operator () (const ndim& ii) const;


/**
  *
  * returns the element with index firstIndex,...
  */
  T& operator () (unsigned long n1);
  T& operator () (unsigned long n1, unsigned long n2);
  T& operator () (unsigned long n1, unsigned long n2, unsigned long n3);
  T& operator () (unsigned long n1, unsigned long n2, unsigned long n3, unsigned long n4);
  T& operator () (unsigned long n1, unsigned long n2, unsigned long n3, unsigned long n4, unsigned long n5);


/**
  *
  * returns the readonly element with index firstIndex,...
  */
  const T& operator () (unsigned long n1) const;
  const T& operator () (unsigned long n1, unsigned long n2) const;
  const T& operator () (unsigned long n1, unsigned long n2, unsigned long n3) const;
  const T& operator () (unsigned long n1, unsigned long n2, unsigned long n3, unsigned long n4) const;
  const T& operator () (unsigned long n1, unsigned long n2, unsigned long n3, unsigned long n4, unsigned long n5) const;


/**
  *
  * returns the dimensionality
  */
  unsigned long dim() const;


/**
  *
  * returns the extent in the i'th dimension
  */
  unsigned long size(unsigned long i) const;


/**
  *
  * returns the total number of elements
  */
  unsigned long total() const;


/**
  *
  * returns the total number of elements
  */
  unsigned long length() const;

/**
  *
  * returns the size of each element in bytes
  */
  unsigned int elementsize() const;

/**
  *
  * If the size of the given array 'ta' matches this object, all values from 'ta' will be assigned to this
  */
  tjarray<V,T>& assignValues(const tjarray<V,T> &ta) ;


/**
  *
  * The array will be resized to the size of ta and all values from 'ta' will be assigned to this
  */
  tjarray<V,T>& copy(const tjarray<V,T> &ta) ;


/**
  *
  * prints array to the stream s
  */
  friend STD_ostream& operator << (STD_ostream& s, const tjarray<V,T> &ta) {
    return s << ta.get_extent() << "=" << ta.printbody();
  }


/**
  *
  * Converts a linear index to a tuple of indices in the array
  */
  ndim create_index(unsigned long index) const;



 private:

  static ndim create_extent(unsigned long n1);
  static ndim create_extent(unsigned long n1,unsigned long n2);
  static ndim create_extent(unsigned long n1,unsigned long n2,unsigned long n3);
  static ndim create_extent(unsigned long n1,unsigned long n2,unsigned long n3,unsigned long n4);
  static ndim create_extent(unsigned long n1,unsigned long n2,unsigned long n3,unsigned long n4,unsigned long n5);

  ndim extent;

  T element_dummy;
};


/////////////////////////////////////////////////////////////////////////////
//    Aliases:

/**
  *  An array of floating point numbers
  */
typedef tjarray<fvector,float>       farray;

/**
  *  An array of double precision floating point numbers
  */
typedef tjarray<dvector,double>     darray;

/**
  *  An array of integer numbers
  */
typedef tjarray<ivector,int>        iarray;


/**
  *  An array of complex numbers
  */
typedef tjarray<cvector,STD_complex> carray;


/**
  *  An array of strings
  */
typedef tjarray<svector,STD_string>   sarray;



/////////////////////////////////////////////////////////////////////////////
// Helper functions:

/**
  *
  * Prints the 2-dimensional array 'table' (rows x cols) with the column widths
  * adjusted to the maximum width (string length) in each column.
  */
  STD_string print_table(const sarray& table);


/**
  *
  * Parses the string 'str' as a table and returns result as a 2-dim array (rows x cols).
  */
  sarray parse_table(const STD_string& str);


/** @}
  */
#endif
