/***************************************************************************
                          tjprofiler.h  -  description
                             -------------------
    begin                : Mon Aug 5 2002
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef TJPROFILER_H
#define TJPROFILER_H


#include <tjutils/tjutils.h>
#include <tjutils/tjstatic.h>
#include <tjutils/tjlabel.h>
#include <tjutils/tjhandler.h>

/**
  * @addtogroup tjutils
  * @{
  */

/**
  * A class to profile your program, i.e. to see how much time is spent
  * in each function. Create a local object of this class in each function
  * you want to analyze with the functions name as the constructors argument.
  * The function name serves as a key to measure the execution time in
  * each function if this function is called successively.
  * the result of can be obtained by the 'dump_final_result()' function.
  * In addition, a function is provided to retrieve the current memory usage.
  */
class Profiler : public StaticHandler<Profiler> {

 public:

  Profiler(const STD_string& func_name);
  ~Profiler();

/**
  * Resets the time counters.
  */
  static void reset();

/**
  * Dumps the results to the current log and resets the time counters.
  */
  static void dump_final_result();

/**
  * Returns the current memory usage as a readable string.
  */
  static STD_string get_memory_usage();


  // functions to initialize/delete static members by the StaticHandler template class
  static void init_static();
  static void destroy_static();


  // for debugging List
  static const char* get_compName();


 private:

  STD_string func_label;
  double starttime;

  struct elapsed {
    elapsed() : time_spent(0.0) {}
    double time_spent;
  };

  struct FuncMap : public STD_map<STD_string, elapsed>, public Labeled {};

  static SingletonHandler<FuncMap, true> func_map;
};

/** @}
  */
#endif
