/***************************************************************************
                          tjcomplex.h  -  description
                             -------------------
    begin                : Thu Jul 13 2000
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/


#ifndef TJCOMPLEX_H
#define TJCOMPLEX_H


#include <tjutils/tjutils.h>
#include <tjutils/tjstring.h>


/**
  * @addtogroup tjutils
  * @{
  */

/**
  * Returns the complex number 'z' as a formatted string
  */
STD_string ctos(const STD_complex& z);

/**
  * Parses an returns string 's' as complex number
  */
STD_complex stoc(const STD_string& s);


////////////////////////////////////////////////////////////
// functions for using complex numbers with Blitz++,
// even if the STD_complex class is not std::complex
inline STD_complex float2real(float a) {return STD_complex(a,0.0f);}
inline STD_complex float2imag(float b) {return STD_complex(0.0f,b);}
inline float creal(STD_complex c) {return c.real();}
inline float cimag(STD_complex c) {return c.imag();}
inline float cabs(STD_complex c) {return float(sqrt(c.real()*c.real()+c.imag()*c.imag()));}  //{return STD_abs(c);} // std::abs produces inf with GCC4 on amd64
inline float phase(STD_complex c) {return STD_arg(c);}
inline STD_complex logc(STD_complex c) {return STD_log(c);}
inline STD_complex expc(STD_complex c) {return STD_exp(c);}
inline STD_complex conjc(STD_complex c) {return STD_conj(c);}



/**
  * Comparison operator for the norm of the complex numbers c1 and c2
  */
inline bool operator < (const STD_complex& c1, const STD_complex& c2) {return STD_abs(c1)<STD_abs(c2);} 

/**
  * Comparison operator for the norm of the complex numbers c1 and c2
  */
inline bool operator > (const STD_complex& c1, const STD_complex& c2) {return STD_abs(c1)>STD_abs(c2);}



/** @}
  */
#endif


