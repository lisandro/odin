/***************************************************************************
                          tjstatic.h  -  description
                             -------------------
    begin                : Sun Jul 7 2002
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef TJSTATIC_H
#define TJSTATIC_H


#include <tjutils/tjutils.h>
#include <tjutils/tjthread.h>

/**
  * @addtogroup tjutils
  * @{
  */


////////////////////////////////////////////////////////////////////

class Static {

 public:

  Static() {}
  virtual ~Static() {}

  // destroy all static objects manually
  static void destroy_all();

  static void append_to_destructor_list(Static* sp);

 protected:

  static STD_list<Static*>* destructor_list;
};

////////////////////////////////////////////////////////////////////


template<class T>
struct StaticAlloc : public virtual Static {

  StaticAlloc() {
  }

  ~StaticAlloc() {
    T::destroy_static();
  }
};


////////////////////////////////////////////////////////////////////

/**
  * This template class handles static members of all  classes that
  * must be initialised exactly once. The rationale behind this
  * is that some of the classes posses static members on which
  * a certain action has to be performed EXACTLY once BEFORE
  * anything else happens with that class. The 'StaticHandler' serves as
  * as a base class for classes that require this feature.
  * It makes this easier by keeping track of how often the static
  * members have been initialised so that this is done only once.
  * The following steps must be performed to make use of the
  * StaticHandler:
  *
  * -# Deriving the class C from the template class StaticHandler
  *    by adding 'public StaticHandler<C>' to the list of base
  *    classes
  * -# Adding the static member function
  *     'static void init_static();'
  *    to the public section of the class. This function should
  *    contain the actions that will be performed only once for
  *    the static members.
  * -# Adding the static member function
  *     'static void destroy_static();'
  *    to the public section of the class. This function should
  *    contain the actions to deallocate memory allocated by init_static()
  * -# Adding the line 'bool StaticHandler<C>::staticdone=false;'
  *    to the source code of the class C
  */
template<class T>
class StaticHandler {

 public:
  StaticHandler() {
    if(!staticdone) { // fast check
      staticdone=true; // must be assigned before call to T::init_static() to avoid infinite loops
      Static* sp=new StaticAlloc<T>;
      Static::append_to_destructor_list(sp);
      T::init_static();
    }
  }


 private:
  static bool staticdone;

};

/** @}
  */
#endif
