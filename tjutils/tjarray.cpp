#include "tjarray.h"
#include "tjtypes.h"
#include "tjtest.h"


ndim::ndim (unsigned long d) : STD_vector<unsigned long>(d) { }  // ndim nn(3);



ndim::ndim (const STD_string& s) : STD_vector<unsigned long>() {
  Log<VectorComp> odinlog("ndim","ndim(const STD_string&)");
  unsigned long incosistent=0;

  STD_string tt=shrink(s);
  ODINLOG(odinlog,normalDebug) << "ndim::ndim (const STD_string& s): after shrinking >" << tt << "<" << STD_endl;

  if(tt[(unsigned int)0] != '(') incosistent++;
  if(tt[tt.length()-1] != ')') incosistent++;
  STD_string innertext=replaceStr(extract(tt,"(",")",true),",","");
//  if (typeofcontent(innertext)!=intnumbers) incosistent++;

  if(!incosistent) {
    ODINLOG(odinlog,normalDebug) << "ndim::ndim (const STD_string& s): no inconsistences in >" << tt << "<" << STD_endl;
    tt=replaceStr(tt,"(",",");
    tt=replaceStr(tt,")",",");
    ODINLOG(odinlog,normalDebug) << "ndim::ndim (const STD_string& s): string used for dim extraction: >" << tt << "<" << STD_endl;

    svector extends(tokens(tt,','));
    unsigned int n=extends.size();
    resize(n);
    for(unsigned int i=0;i<n;i++) (*this)[i]=atoi(extends[i].c_str());
  }
}



ndim::operator STD_string () const {
  unsigned long n=size();
  STD_string result;
  result="( ";
  if(!n) result+="0";
  for (unsigned long i=0;i<n;i++) {
    result+=itos((*this)[i]);
    if (i<(n-1)) result+=", ";
  }
  result+=" )";
  return result;
}



ndim&  ndim::operator -- () {
  Log<VectorComp> odinlog("ndim","--()");
  unsigned long n=size();
  if (n <= 0) ODINLOG(odinlog,errorLog) << "reduce to negative dimension ?!" << STD_endl;
  else {
    ndim tt(*this);
    n--;
    resize(n);
    for(unsigned long i=0;i<n;i++)  (*this)[i]=tt[i+1];
  }
  return *this;
}



ndim&  ndim::operator -- (int) {
  Log<VectorComp> odinlog("ndim","--(int)");
  unsigned long n=size();
  if (n <= 0) ODINLOG(odinlog,errorLog) << "reduce to negative dimension ?!" << STD_endl;
  else {
    ndim tt(*this);
    n--;
    resize(n);
    for(unsigned long i=0;i<n;i++)  (*this)[i]=tt[i];
  }
  return *this;
}



ndim& ndim::add_dim ( unsigned long e, bool first ) {
  unsigned long n=size();
  ndim tt(*this);
  n++;
  resize(n);
  for(unsigned long i=0;i<n-1;i++)  (*this)[i+int(first)]=tt[i];
  if(first) (*this)[0]=e;
  else      (*this)[n-1]=e;
  return *this;
}




unsigned long ndim::total() const {
  unsigned long n=size();
  unsigned long t=1;
  for(unsigned long i=0;i<n;i++) t*=(*this)[i];
  if(!n) t=0;
  return(t);
}



unsigned long ndim::extent2index(const ndim& mm) const {
  Log<VectorComp> odinlog("ndim","extent2index");
  unsigned long totalIndex=0,subsize;
  if (dim() != mm.dim()) {
    ODINLOG(odinlog,errorLog) <<"dimension mismatch: dim()!=mm.dim()=" << dim() << "!=" << mm.dim() << STD_endl;
    return 0;
  }
  if(!dim()) return 0;
  ndim nn(*this);
  for(unsigned long i=0;i<dim();i++) {
    --nn;
    subsize=nn.total();
    if(!subsize) subsize=1;
    totalIndex+=subsize*mm[i];
  }
  return totalIndex;
}

ndim ndim::index2extent(unsigned long index) const {
  ndim nn(dim());
  unsigned long temp=index;
  for(long i=dim()-1;i>=0;i--){
    nn[i]=temp%((*this)[i]);
    temp=temp/(*this)[i];
  }
  return nn;
}




bool ndim::operator == (const ndim& nn) const {
  unsigned long i,flag=0;
  if (dim()==nn.dim()) {
    for(i=0;i<dim();i++) flag+=!((*this)[i]==nn[i]);
    return bool(!flag);
  } else return false;
}


bool ndim::operator != (const ndim& nn) const {
  unsigned long i,flag=0;
  if (dim()!=nn.dim()) return true;
  else {
    for(i=0;i<dim();i++) flag+=((*this)[i]!=nn[i]);
    return bool(flag);
  }
}


ndim& ndim::reduce ( unsigned long newdim ) {
  unsigned long n=size();
  if( newdim >= n ) return *this;
  ndim oldndim(*this);
  resize(newdim);

  unsigned long extent_factor=1;
  while(oldndim.dim()>newdim) {
    extent_factor=oldndim[0];
    --oldndim;
    oldndim[0]*=extent_factor;
  }
  for(unsigned int i=0;i<newdim;i++)  (*this)[i]=oldndim[i];
  return *this;
}


ndim& ndim::autosize () {
  bool at_least_one_element=bool(total());
#ifdef STL_REPLACEMENT
  STD_list<unsigned long> indexlist;
  unsigned long i;
  for(i=0;i<size();i++) {
    if((*this)[i]>1) indexlist.push_back((*this)[i]);
  }
  resize(indexlist.size());
  i=0;
  for(STD_list<unsigned long>::iterator it=indexlist.begin(); it!=indexlist.end(); ++it) {
    (*this)[i]=(*it);
    i++;
  }
#else
  STD_vector<unsigned long>::iterator new_end;
  new_end=std::remove(begin(),end(),(unsigned long)(1));
  erase(new_end,end());
#endif
  if((size()==0) && at_least_one_element) {
    resize(1);
    (*this)[0]=1;
  }
  return *this;
}



#ifndef NO_UNIT_TEST
class NdimTest : public UnitTest {

 public:
  NdimTest() : UnitTest("ndim") {}

 private:

  bool check() const {
    Log<UnitTest> odinlog(this,"check");
    ndim nn_reference(3);
    nn_reference[0]=4;
    nn_reference[1]=7;
    nn_reference[2]=9;

    nn_reference.add_dim(2,true);
    nn_reference.add_dim(3,false);

    ndim nn_parse(" ( 2, 4, 7, 9, 3 )");
    if(nn_parse!=nn_reference) {
      ODINLOG(odinlog,errorLog) << "Mismatch: nn_reference/nn_parse=" << STD_string(nn_reference) << "/" << STD_string(nn_parse) << STD_endl;    
      return false;
    }

    return true;
  }

};

void alloc_NdimTest() {new NdimTest();} // create test instance
#endif




/**************************************************************/
/******************       Template code     *******************/
/**************************************************************/

/*------------------------------------------------------*/

template<class V, class T>
tjarray<V,T>::tjarray () : V() {
  extent.resize(1);
  extent[0] = 0;
}


template<class V, class T>
tjarray<V,T>::tjarray (unsigned long n1) {redim(create_extent(n1));}

template<class V, class T>
tjarray<V,T>::tjarray (unsigned long n1, unsigned long n2) {redim(create_extent(n1,n2));}

template<class V, class T>
tjarray<V,T>::tjarray (unsigned long n1, unsigned long n2, unsigned long n3) {redim(create_extent(n1,n2,n3));}

template<class V, class T>
tjarray<V,T>::tjarray (unsigned long n1, unsigned long n2, unsigned long n3, unsigned long n4) {redim(create_extent(n1,n2,n3,n4));}

template<class V, class T>
tjarray<V,T>::tjarray (unsigned long n1, unsigned long n2, unsigned long n3, unsigned long n4, unsigned long n5) {redim(create_extent(n1,n2,n3,n4,n5));}



template<class V, class T>
tjarray<V,T>::tjarray (const tjarray<V,T> &ta) : V(ta) {
  extent=ta.extent;
}


template<class V, class T>
tjarray<V,T>::tjarray (const V& sv) : V(sv) {
  extent.resize(1);
  extent[0]=sv.size();
}


template<class V, class T>
tjarray<V,T>::tjarray (const ndim &nn) {
  V::resize(nn.total());
  extent=nn;
}

template<class V, class T>
tjarray<V,T>& tjarray<V,T>::operator = (const tjarray<V,T>& ta) {
  Log<VectorComp> odinlog("tjarray","operator = (const tjarray<V,T>&)");
  V::operator = (ta);
  extent=ta.extent;
  return *this;
}



template<class V, class T>
  tjarray<V,T>& tjarray<V,T>::operator = (const T& value) {
    for(unsigned int i=0; i<extent.total(); i++) (*this)[i]=value;
//    V::operator = (value);
    return *this;
  }



template<class V, class T>
V& tjarray<V,T>::resize (unsigned int newsize) {
  Log<VectorComp> odinlog("tjarray","resize");
  extent.resize (1);
  extent[0]=newsize;
  V::resize(extent.total());
  return *this;
}


template<class V, class T>
tjarray<V,T>& tjarray<V,T>::redim (const ndim &nn) {
  Log<VectorComp> odinlog("tjarray","redim");
  ODINLOG(odinlog,normalDebug) << "nn=" << STD_string(nn) << STD_endl;
  unsigned int newtot=nn.total();
  if(total()!=newtot) V::resize(newtot);
  extent=nn;
  return *this;
}


template<class V, class T>
tjarray<V,T>& tjarray<V,T>::redim (unsigned long n1) {redim(create_extent(n1)); return *this;}

template<class V, class T>
tjarray<V,T>& tjarray<V,T>::redim (unsigned long n1, unsigned long n2) {redim(create_extent(n1,n2)); return *this;}

template<class V, class T>
tjarray<V,T>& tjarray<V,T>::redim (unsigned long n1, unsigned long n2, unsigned long n3) {redim(create_extent(n1,n2,n3)); return *this;}

template<class V, class T>
tjarray<V,T>& tjarray<V,T>::redim (unsigned long n1, unsigned long n2, unsigned long n3, unsigned long n4) {redim(create_extent(n1,n2,n3,n4)); return *this;}

template<class V, class T>
tjarray<V,T>& tjarray<V,T>::redim (unsigned long n1, unsigned long n2, unsigned long n3, unsigned long n4, unsigned long n5) {redim(create_extent(n1,n2,n3,n4,n5)); return *this;}
//#endif


template<class V, class T>
const ndim& tjarray<V,T>::get_extent () const {
  return extent;
}





template<class V, class T>
T& tjarray<V,T>::operator () (const ndim& ii) {
  Log<VectorComp> odinlog("tjarray","operator ()");
  unsigned long totalIndex=extent.extent2index(ii);
  unsigned long totalExtent=extent.total();
  ODINLOG(odinlog,normalDebug) << "totalIndex/totalExtent=" << totalIndex << "/" << totalExtent << STD_endl;
  if(totalIndex>=totalExtent) return element_dummy;
  else return (*this)[totalIndex];
}

template<class V, class T>
const T& tjarray<V,T>::operator () (const ndim& ii) const {
  Log<VectorComp> odinlog("tjarray","operator () const");
  unsigned long totalIndex=extent.extent2index(ii);
  unsigned long totalExtent=extent.total();
  ODINLOG(odinlog,normalDebug) << "totalIndex/totalExtent=" << totalIndex << "/" << totalExtent << STD_endl;
  if(totalIndex>=totalExtent) return element_dummy;
  else return (*this)[totalIndex];
}


template<class V, class T>
T& tjarray<V,T>::operator () (unsigned long n1) {return tjarray<V,T>::operator () (create_extent(n1));}

template<class V, class T>
T& tjarray<V,T>::operator () (unsigned long n1, unsigned long n2) {return tjarray<V,T>::operator () (create_extent(n1,n2));}

template<class V, class T>
T& tjarray<V,T>::operator () (unsigned long n1, unsigned long n2, unsigned long n3) {return tjarray<V,T>::operator () (create_extent(n1,n2,n3));}

template<class V, class T>
T& tjarray<V,T>::operator () (unsigned long n1, unsigned long n2, unsigned long n3, unsigned long n4) {return tjarray<V,T>::operator () (create_extent(n1,n2,n3,n4));}

template<class V, class T>
T& tjarray<V,T>::operator () (unsigned long n1, unsigned long n2, unsigned long n3, unsigned long n4, unsigned long n5) {return tjarray<V,T>::operator () (create_extent(n1,n2,n3,n4,n5));}



template<class V, class T>
const T& tjarray<V,T>::operator () (unsigned long n1) const {return tjarray<V,T>::operator () (create_extent(n1));}

template<class V, class T>
const T& tjarray<V,T>::operator () (unsigned long n1, unsigned long n2) const {return tjarray<V,T>::operator () (create_extent(n1,n2));}

template<class V, class T>
const T& tjarray<V,T>::operator () (unsigned long n1, unsigned long n2, unsigned long n3) const {return tjarray<V,T>::operator () (create_extent(n1,n2,n3));}

template<class V, class T>
const T& tjarray<V,T>::operator () (unsigned long n1, unsigned long n2, unsigned long n3, unsigned long n4) const {return tjarray<V,T>::operator () (create_extent(n1,n2,n3,n4));}

template<class V, class T>
const T& tjarray<V,T>::operator () (unsigned long n1, unsigned long n2, unsigned long n3, unsigned long n4, unsigned long n5) const {return tjarray<V,T>::operator () (create_extent(n1,n2,n3,n4,n5));}




template<class V, class T>
unsigned long tjarray<V,T>::dim() const {return extent.dim();}

template<class V, class T>
unsigned long tjarray<V,T>::size(unsigned long i) const {return extent[i];}

template<class V, class T>
unsigned long tjarray<V,T>::total() const {return extent.total();}

template<class V, class T>
unsigned long tjarray<V,T>::length() const {return total();}

template<class V, class T>
unsigned int tjarray<V,T>::elementsize() const {return sizeof(T);}

template<class V, class T>
tjarray<V,T>& tjarray<V,T>::assignValues(const tjarray<V,T> &ta) {
  Log<VectorComp> odinlog("tjvector","assignValues");
  if(ta.length()==length()) {
    for(unsigned int i=0; i<length(); i++) (*this)[i]=ta[i];
  } else ODINLOG(odinlog,normalDebug) << "size mismatch " << itos(length()) << "!=" << itos(length()) << STD_endl;
  return *this;
}

template<class V, class T>
tjarray<V,T>& tjarray<V,T>::copy(const tjarray<V,T> &ta) {
  redim (ta.get_extent());
  assignValues(ta);
  return *this;
}




template<class V, class T>
ndim  tjarray<V,T>::create_extent(unsigned long n1) {
  ndim nn(1); nn[0]=n1;
  return nn;
}

template<class V, class T>
ndim  tjarray<V,T>::create_extent(unsigned long n1,unsigned long n2) {
  ndim nn(2); nn[0]=n1; nn[1]=n2;
  return nn;
}

template<class V, class T>
ndim  tjarray<V,T>::create_extent(unsigned long n1,unsigned long n2,unsigned long n3) {
  ndim nn(3); nn[0]=n1; nn[1]=n2; nn[2]=n3;
  return nn;
}

template<class V, class T>
ndim  tjarray<V,T>::create_extent(unsigned long n1,unsigned long n2,unsigned long n3,unsigned long n4) {
  ndim nn(4); nn[0]=n1; nn[1]=n2; nn[2]=n3; nn[3]=n4;
  return nn;
}

template<class V, class T>
ndim  tjarray<V,T>::create_extent(unsigned long n1,unsigned long n2,unsigned long n3,unsigned long n4,unsigned long n5) {
  ndim nn(5); nn[0]=n1; nn[1]=n2; nn[2]=n3; nn[3]=n4; nn[4]=n5;
  return nn;
}


template<class V, class T>
ndim tjarray<V,T>::create_index(unsigned long index) const {return extent.index2extent(index);}


/////////////////////////////////////////////////////////////////////////////////


#ifndef NO_UNIT_TEST
class ArrayTest : public UnitTest {

 public:
  ArrayTest() : UnitTest("array") {}

 private:
  bool check() const {
    Log<UnitTest> odinlog(this,"check");

    farray fa(1,2,3,4,5);

    STD_string expected="( 1, 2, 3, 4, 5 )";
    STD_string printed=fa.get_extent();

    if(printed!=expected) {
      ODINLOG(odinlog,errorLog) << "farray(...) failed: got extent >" << printed << "<, but expected >" << expected << "<" << STD_endl;
      return false;
    }

    fa.redim(3,2,1);
    expected="( 3, 2, 1 )";
    printed=fa.get_extent();

    if(printed!=expected) {
      ODINLOG(odinlog,errorLog) << "farray.redim(...) failed: got extent >" << printed << "<, but expected >" << expected << "<" << STD_endl;
      return false;
    }

    float testval=44.0;
    fa(2,1,0)=testval;
    if(fa.sum()!=44.0) {
      ODINLOG(odinlog,errorLog) << "farray.operator (...) failed: " << fa.sum() << "!=" << testval << STD_endl;
      return false;
    }

    return true;
  }

};

void alloc_ArrayTest() {new ArrayTest();} // create test instance
#endif



/**************************************************************/
/******************       Template instantiations     *********/
/**************************************************************/



template class tjarray<fvector,float>;
template class tjarray<dvector,double>;
template class tjarray<ivector,int>;
template class tjarray<svector,STD_string>;
template class tjarray<cvector,STD_complex>;



/////////////////////////////////////////////////////////////////////////////
// Helper functions:

STD_string print_table(const sarray& table) {
  Log<VectorComp> odinlog("","print_table");
  STD_string result;

  if(table.dim()!=2) {
    ODINLOG(odinlog,errorLog) << "Dimension of input array != 2" << STD_endl;
    return result;
  }

  int ncols=table.size(1);
  int nrows=table.size(0);

  int icol, irow;

  // Get max width of each column
  ivector width(ncols); width=0;
  for(irow=0; irow<nrows; irow++) {
    for(icol=0; icol<ncols; icol++) {
      width[icol]=STD_max(int(table(irow,icol).length()), width[icol]);
    }
  }

  // Print formatted table
  for(irow=0; irow<nrows; irow++) {
    for(icol=0; icol<ncols; icol++) {
      int nfillspaces=width[icol]-table(irow,icol).length()+1;
      if(icol==(ncols-1)) nfillspaces=0; // do not pad at end
      result+=table(irow,icol)+STD_string(nfillspaces,' ');
    }
    result+="\n";
  }

  return result;
}


sarray parse_table(const STD_string& str) {

  svector rows(tokens(str,'\n'));

  unsigned int nrows=rows.size();
  unsigned int ncols=0;
  if(nrows) ncols=tokens(rows[0]).size();

  sarray result(nrows,ncols);

  for(unsigned int irow=0; irow<nrows; irow++) {
    svector toks(tokens(rows[irow]));
    for(unsigned int icol=0; (icol<toks.size() && icol<ncols); icol++) {
      result(irow,icol)=toks[icol];
    }
  }

  return result;
}
