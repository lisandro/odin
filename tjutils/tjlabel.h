/***************************************************************************
                          tjlabel.h  -  description
                             -------------------
    begin                : Fri Aug 2 2002
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef TJLABEL_H
#define TJLABEL_H

#include <tjutils/tjutils.h>

/**
  * @addtogroup tjutils
  * @{
  */

/**
  * This is the base class for all classes which can have a (unique) label
  */
class Labeled {

 public:

/**
  * Constructor with initialization of label
  */
  Labeled(const STD_string& label="unnamed") : objlabel(label) {}

/**
  * Assigns a label to the object
  */
  Labeled& set_label(const STD_string& label) {objlabel=label; return *this;}

/**
  * Returns the label of the object
  */
  const STD_string& get_label() const {return objlabel;}


/**
  * Assignment operator
  */
  Labeled& operator = (const Labeled& l) {
    objlabel=l.objlabel;
    return *this;
  }

 private:
  STD_string objlabel;
};

/** @}
  */
#endif
