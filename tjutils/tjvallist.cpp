#include "tjvallist.h"
#include "tjlog.h"
#include "tjtypes.h"
#include "tjtest.h"


template <class T>
ValList<T>::ValList(const STD_string& object_label, unsigned int repetitions)
  :  Labeled(), data(new ValListData) {
  set_label(object_label);
  data->times=repetitions;
  data->references=1;
}

template <class T>
ValList<T>::ValList(T value) :  Labeled(), data(new ValListData) {
  data->val=new T(value);
  data->elements_size_cache=1;
  data->references=1;
}


template <class T>
ValList<T>::ValList(const ValList<T>& svl) : Labeled(svl), data(svl.data) {
  data->references++;
}

template <class T>
ValList<T>::~ValList() {
  clear();
  data->references--;
  if(!(data->references)) delete data;
}


template <class T>
ValList<T>& ValList<T>::operator = (const ValList<T>& svl) {
  Labeled::operator = (svl);
  set_label(svl.get_label());

  data->references--;
  if(!(data->references)) delete data;

  data=svl.data;
  data->references++;

  return *this;
}

template <class T>
bool ValList<T>::operator ==  (const ValList<T>& vl) const {
  return ( (get_elements_flat()==vl.get_elements_flat()) && (data->times==vl.data->times) );
}

template <class T>
bool ValList<T>::operator < (const ValList<T>& vl) const {
  bool result=true;
  if(! (get_elements_flat()<vl.get_elements_flat()) ) result=false;
  if(! (data->times<vl.data->times) ) result=false;
  return result;
}


template <class T>
bool ValList<T>::equalelements (const ValList<T>& svl) const {
  Log<VectorComp> odinlog(this,"equalelements");
  ODINLOG(odinlog,normalDebug) << "comparing with " << svl.get_label() << STD_endl;

  unsigned int thissize=data->elements_size_cache;
  unsigned int svlsize=svl.data->elements_size_cache;
  ODINLOG(odinlog,normalDebug) << "thissize/svlsize=" << thissize << "/" << svlsize << STD_endl;

  if(thissize!=svlsize) return false;
  if(thissize==0 && svlsize==0) return false;

  ODINLOG(odinlog,normalDebug) << "getting elements of this" << STD_endl;
  STD_vector<T> thiselements=get_elements_flat();

  ODINLOG(odinlog,normalDebug) << "getting elements of " << svl.get_label() << STD_endl;
  STD_vector<T> svlelements=svl.get_elements_flat();

  ODINLOG(odinlog,normalDebug) << "comparing vectors" << STD_endl;
  if(thiselements.size()==0 && svlelements.size()==0) return false;
  return (thiselements==svlelements);
}


template <class T>
ValList<T>& ValList<T>::set_value(T value) {
  copy_on_write();
  if(data->sublists) delete data->sublists; data->sublists=0;
  if(data->val) *(data->val)=value;
  else data->val=new T(value);
  data->elements_size_cache=1;
  return *this;
}

template <class T>
ValList<T>& ValList<T>::add_sublist(const ValList<T>& svl) {
  Log<VectorComp> odinlog(this,"add_sublist");

  copy_on_write();
  
  if( !(svl.data->val || svl.data->sublists) ) {
    ODINLOG(odinlog,normalDebug) << "discarding empty list"  << STD_endl;
    return *this;
  }

  ODINLOG(odinlog,normalDebug) << "checking for equal elements"  << STD_endl;
  if(equalelements(svl)) {
    increment_repetitions(svl.get_repetitions());
    ODINLOG(odinlog,normalDebug) << "incrementing repetitions"  << STD_endl;
    return *this;
  }

  if(data->sublists && data->val) {
    ODINLOG(odinlog,errorLog) << "sublists and value allocated" << STD_endl;
    return *this;
  }

  if(!data->sublists && !data->val) {
    ODINLOG(odinlog,normalDebug) << "assigning"  << STD_endl;
    if(svl.data->val) {
      ODINLOG(odinlog,normalDebug) << "svl has val"  << STD_endl;
    }
    if(svl.data->sublists) {
      ODINLOG(odinlog,normalDebug) << "svl has sublists"  << STD_endl;
    }
    STD_string label_cache(get_label());
    (*this)=svl;
    set_label(label_cache);
    return *this;
  }

  if(!data->sublists && data->val) {
    ODINLOG(odinlog,normalDebug) << "allocating new sublists"  << STD_endl;
    data->sublists=new STD_list< ValList<T> >;
    for(unsigned int i=0; i<data->times; i++) data->sublists->push_back(ValList(*data->val)); // add element by element
    data->elements_size_cache=data->times;
    delete data->val; data->val=0; data->times=1;
    data->sublists->push_back(svl);
    data->elements_size_cache+=svl.size();
    return *this;
  }

  if(data->sublists && !data->val) {
    ODINLOG(odinlog,normalDebug) << "appending"  << STD_endl;
    if(data->times!=1) flatten_sublists();
    data->sublists->push_back(svl);
    data->elements_size_cache+=svl.size();
    return *this;
  }

  return *this;
}

template <class T>
void ValList<T>::flatten_sublists() {
  Log<VectorComp> odinlog(this,"flatten_sublists");
  copy_on_write();
  STD_vector<T> vals_flat=get_values_flat();
  if(data->sublists) data->sublists->clear();
  else data->sublists=new STD_list< ValList<T> >;
  for(unsigned int i=0; i<vals_flat.size(); i++) data->sublists->push_back(ValList(vals_flat[i]));
  data->times=1;
  data->elements_size_cache=vals_flat.size();
}


template <class T>
STD_vector<T> ValList<T>::get_elements_flat() const {

  STD_list<T> vallist;
  if(data->val) vallist.push_back(*data->val);
  if(data->sublists) {
    for(typename STD_list< ValList<T> >::const_iterator it=data->sublists->begin(); it!=data->sublists->end(); ++it) {
      STD_vector<T> subelements=it->get_values_flat();
      for(unsigned int i=0; i<subelements.size(); i++) vallist.push_back(subelements[i]);
    }
  }

  unsigned int listsize=vallist.size();
  STD_vector<T> result; result.resize(listsize);
  unsigned int index=0;
  for(typename STD_list<T>::const_iterator valit=vallist.begin(); valit!=vallist.end(); ++valit) {
    result[index]=(*valit);
    index++;
  }
  return result;
}




template <class T>
STD_vector<T> ValList<T>::get_values_flat() const {
  STD_vector<T> elements=get_elements_flat();
  unsigned int listsize=elements.size();
  STD_vector<T> result; result.resize(listsize*data->times);
  for(unsigned int itimes=0; itimes<data->times; itimes++) {
    for(unsigned int i=0; i<listsize; i++) {
      result[itimes*listsize+i]=elements[i];
    }
  }
  return result;
}


template <class T>
T ValList<T>::operator [] (unsigned int i) const {

  if(data->val) {
    if(i==0) return (*data->val);
    else i--;
  }

  if(data->sublists) {
    for(unsigned j=0; j<data->times; j++) {
      for(typename STD_list< ValList<T> >::const_iterator it=data->sublists->begin(); it!=data->sublists->end(); ++it) {
        unsigned int subsize=it->ValList<T>::size();
        if(i<subsize) return  it->operator [] (i);
        else i-=subsize;
      }
    }
  }

  return 0;
}

/*
template <class T>
unsigned int ValList<T>::elements_size() const {

  unsigned int result=0;
  if(data->val) result++;
  if(data->sublists) {
    for(typename STD_list< ValList<T> >::const_iterator it=data->sublists->begin(); it!=data->sublists->end(); ++it) {
      result+=it->ValList<T>::size();
    }
  }
  return result;
}
*/


template <class T>
STD_string ValList<T>::printvallist() const {

  Log<VectorComp> odinlog(this,"printvallist");
  STD_string result;
  if(data->val) {
    result+=TypeTraits::type2string(*data->val)+" ";
  }
  if(data->sublists) {
    for(typename STD_list< ValList<T> >::const_iterator it=data->sublists->begin(); it!=data->sublists->end(); ++it) {
      result+=it->printvallist();
    }
  }
  if(data->times>1) result="{"+itos(data->times)+"| "+result+"} ";
  return result;
}


template <class T>
STD_ostream& ValList<T>::print2stream(STD_ostream& os) const {

  if(data->times>1) os << "{" << itos(data->times) << "| ";

  if(data->val) os << (*data->val) << " ";

  if(data->sublists) {
    for(typename STD_list< ValList<T> >::const_iterator it=data->sublists->begin(); it!=data->sublists->end(); ++it) {
      it->print2stream(os);
    }
  }

  if(data->times>1) os << "} ";

  return os;
}



template <class T>
bool ValList<T>::parsevallist(const STD_string& str) {
  Log<VectorComp> odinlog(this,"parsevallist");
  copy_on_write();
  svector toks(tokens(str));
  unsigned int ntoks=toks.size();
  ODINLOG(odinlog,normalDebug) << "ntoks=" << ntoks << STD_endl;

  unsigned int itok=0;
  while(itok<ntoks) {
    ODINLOG(odinlog,normalDebug) << "toks[" << itok << "]=" << toks[itok] << STD_endl;
    ValList<T> slist;
    if(toks[itok].find("{")!=STD_string::npos) {
      unsigned int rep=atoi(extract(toks[itok],"{","|").c_str()); // no space characters allowed between '{' and '|'
      ODINLOG(odinlog,normalDebug) << "rep=" << rep << STD_endl;
      int brace_balance=1;
      itok++;
      STD_string sublist_str;
      while(itok<ntoks && brace_balance) {
        if(toks[itok].find("}")!=STD_string::npos) brace_balance--;
        if(toks[itok].find("{")!=STD_string::npos) brace_balance++;
        if(brace_balance) sublist_str+=toks[itok]+" ";
        itok++;
      }
      slist.parsevallist(sublist_str);
      if(rep>=1) slist.increment_repetitions(rep-1);
    } else {
      T valdummy;
      TypeTraits::string2type(toks[itok],valdummy);
      ODINLOG(odinlog,normalDebug) << "valdummy=" << valdummy << STD_endl;
      slist.set_value(T(valdummy));
      itok++;
    }
    add_sublist(slist);
  }
  return true;
}

template <class T>
void ValList<T>::clear() {
  copy_on_write();
  if(data->sublists) delete data->sublists; data->sublists=0;
  if(data->val) delete data->val; data->val=0;
  data->elements_size_cache=0;
}

template <class T>
void ValList<T>::copy_on_write() {
  Log<VectorComp> odinlog(this,"copy_on_write");
  ODINLOG(odinlog,normalDebug) << "references=" << data->references << STD_endl;
  if(data->references>1) {
    data->references--;
    data=new ValListData(*data);
    data->references++;
  }
}


// instantiations reuired for ODIN
template class ValList<double>;
template class ValList<int>;


/////////////////////////////////////////////////////////////////////////////////////////

#ifndef NO_UNIT_TEST

typedef ValList<int> ValListInt;


class ValListTest : public UnitTest {

 public:
  ValListTest() : UnitTest("vallist") {}

 private:

  bool check() const {
    Log<UnitTest> odinlog(this,"check");

    ValListInt v1(1);
    ValListInt v2;
    v2.set_value(2);

    ValListInt vl;
    vl.add_sublist(v1);
    vl.add_sublist(v2);

    STD_string expected="1 2 ";
    STD_string printed=vl.printvallist();
    if( expected!=printed ) {
      ODINLOG(odinlog,errorLog) << "add_sublist(v1,v2) failed, got >" << printed << "< but expected >" << expected << "<" << STD_endl;
      return false;
    }

    /////////////////////////////////////////

    ValListInt vl2;
    vl2.add_sublist(vl);
    vl2.add_sublist(vl);
    vl2.add_sublist(vl);

    expected="{3| 1 2 } ";
    printed=vl2.printvallist();
    if( expected!=printed ) {
      ODINLOG(odinlog,errorLog) << "add_sublist(3*vl) failed, got >" << printed << "< but expected >" << expected << "<" << STD_endl;
      return false;
    }
    expected="1 2 1 2 1 2";
    printed=ivector(vl2.get_values_flat()).printbody();
    if( expected!=printed ) {
      ODINLOG(odinlog,errorLog) << "get_values_flat failed, got >" << printed << "< but expected >" << expected << "<" << STD_endl;
      return false;
    }
    expected="3";
    printed=itos(vl2.get_repetitions());
    if( expected!=printed ) {
      ODINLOG(odinlog,errorLog) << "get_repetitions failed, got >" << printed << "< but expected >" << expected << "<" << STD_endl;
      return false;
    }

    /////////////////////////////////////////

    vl2.clear();

    vl.multiply_repetitions(7);
    vl2.add_sublist(vl);
    vl.multiply_repetitions(9);
    vl2.add_sublist(vl);
    vl2.multiply_repetitions(2);

    expected="{140| 1 2 } "; // (7 + 7*9)*2=140
    printed=vl2.printvallist();
    if( expected!=printed ) {
      ODINLOG(odinlog,errorLog) << "multiply_repetitions failed, got >" << printed << "< but expected >" << expected << "<" << STD_endl;
      return false;
    }


    /////////////////////////////////////////


    v1.clear();
    v1.parsevallist("{3| 1 2 {3| 4 5 } }");
    unsigned int expected_size=24;
    unsigned int calculated_size=v1.size();
    if( expected_size!=calculated_size ) {
      ODINLOG(odinlog,errorLog) << "size() failed, got >" << calculated_size << "< but expected >" << expected_size << "<" << STD_endl;
      return false;
    }

    v1.clear();
    expected_size=0;
    calculated_size=v1.size();
    if( expected_size!=calculated_size ) {
      ODINLOG(odinlog,errorLog) << "size() failed, got >" << calculated_size << "< but expected >" << expected_size << "<" << STD_endl;
      return false;
    }

    return true;
  }

};


void alloc_ValListTest() {new ValListTest();} // create test instance
#endif
