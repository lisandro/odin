/***************************************************************************
                          tjheap.h  -  description
                             -------------------
    begin                : Thu Jul 31 2003
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef TJHEAP_H
#define TJHEAP_H

#ifndef TJUTILS_CONFIG_H
#define TJUTILS_CONFIG_H
#include <tjutils/config.h>
#endif

// pointer for external trace function
typedef void (*heaptracefunction)(const char*);


class Heap {
 public:
  static void malloc_stats();
  static void set_trace_function(heaptracefunction func) {tracefunc=func;}

  static heaptracefunction tracefunc;
};


//#ifdef CUSTOM_HEAP

//#include <tjutils/tjcstd.h>

//#ifndef STL_REPLACEMENT
//#include <new>
//#endif


// overloading new/delete if a custom heap (block of static memory) is used

//#ifndef STL_REPLACEMENT
//void* operator new(size_t size) throw (std::bad_alloc);
//#else
//void* operator new(size_t size);
//#endif
//void* operator new[](size_t size);

//void operator delete(void* mptr);
//void operator delete[](void* mptr);

//#endif


#endif
