/***************************************************************************
                          tjtools.h  -  description
                             -------------------
    begin                : Thu Jul 13 2000
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/



// This is a collection of frequently used C-routines


#ifndef TJTOOLS_H
#define TJTOOLS_H

#include <tjutils/tjutils.h>

/**
  * @addtogroup tjutils
  * @{
  */

#ifndef PII
#define PII 3.14159265358979323846264338327950288
#endif

#ifndef ODIN_MAXCHAR
#define ODIN_MAXCHAR 4096
#endif

#ifdef HAVE_LONG_LONG
#define LONGEST_INT long long
#else
#define LONGEST_INT long
#endif


// functions of large file extension, define them here so
// these macros can re-used in other modules
#ifdef HAVE_STAT64
#define ODIN_STAT stat64
#else
#ifdef HAVE_STAT 
#define ODIN_STAT stat
#endif
#endif

#ifdef HAVE_OPEN64
#define ODIN_OPEN open64
#else
#define ODIN_OPEN open
#endif

#ifdef HAVE_FOPEN64
#define ODIN_FOPEN fopen64
#else
#define ODIN_FOPEN fopen
#endif

#ifdef HAVE_FSEEKO64
#define ODIN_FSEEK fseeko64
#else
#ifdef HAVE_FSEEKO
#define ODIN_FSEEK fseeko
#else
#define ODIN_FSEEK fseek
#endif
#endif

#ifdef HAVE_FTELLO64
#define ODIN_FTELL ftello64
#else
#ifdef HAVE_FTELLO
#define ODIN_FTELL ftello
#else
#define ODIN_FTELL ftell
#endif
#endif

#ifdef HAVE_MMAP64
#define ODIN_MMAP mmap64
#else
#define ODIN_MMAP mmap
#endif


// macro to avoid compiler warnings about unused static string arrays
#define AVOID_CC_WARNING(strArray) inline const char* strArray##_dummyfunc() {return strArray[0];}

/**
  *
  * Mode flag when reading/writing raw data to disk:
  * - readMode: Read-only access
  * - overwriteMode: If a file with the same name already exists, it is overwritten
  * - appendMode: If a file with the same name already exists, the data is appended
  */
enum fopenMode {readMode,overwriteMode,appendMode};


/**
  * Norm of a double vector
  */
double norm(double x, double y);


/**
  * Norm of a triple vector
  */
double norm3(double x,double y,double z);



/**
  *
  * Maximum value of 3 numbers
  */
double maxof3(double f1, double f2, double f3);


#ifndef NO_FILEHANDLING


/**
  * 
  * Returns mode string suitable for fopen according to 'mode'
  */
const char* modestring(fopenMode mode);


/**
  * 
  * Returns file size in bytes if file exists, otherwise -1
  */
LONGEST_INT filesize(const char *filename);


/**
  *
  * Returns the current directory of the process
  */
const char* getpwd();


/**
  *
  * Changes the current directory of the process to dirname. On success, zero is returned.  On error, -1 is returned.
  */
int chpwd(const char *dirname);


/**
  *
  * Creates the given directory. On success, zero is returned.  On error, -1 is returned.
  */
int createdir(const char *dirname);


/**
  *
  * Returns true if directory 'dirname' exists, otherwise false
  */
bool checkdir(const char *dirname);


/**
  *
  * Moves a file 'src' to destination 'dst'. On success, zero is returned.  On error, -1 is returned.
  */
int movefile(const char *src, const char* dst);


/**
  *
  * Copies a file 'src' to destination 'dst'. On success, zero is returned.  On error, -1 is returned.
  */
int copyfile(const char *src, const char* dst);


/**
  *
  * Removes the file or directory 'fname'. On success, zero is returned.  On error, -1 is returned.
  */
int rmfile(const char *fname);


/**
  *
  * Changes permissions of file 'fname' to 'perm'. On success, zero is returned.  On error, -1 is returned.
  */
int chperm(const char *fname, int perm);



/**
  *
  * Creates a file mapping of file 'filename' with 'nbytes' length starting at 'offset' bytes into the file,
  * optionally in 'readonly' mode.
  * The 'fd' will hold the file descriptor (handle) of the mapped file.
  * Returns a pointer to the mapped memory region.
  */
void* filemap(const STD_string& filename, LONGEST_INT nbytes, LONGEST_INT offset, bool readonly, int& fd);


/**
  *
  * Deletes a file mapping of file descriptor 'fd' which maps a region at memory location 'start' of 'nbytes' length
  * starting at 'offset' bytes into the file.
  */
void fileunmap(int fd, void* start, LONGEST_INT nbytes, LONGEST_INT offset);


/**
  *
  * Return a unique name for a temporary file.
  */
STD_string tempfile();


/**
  *
  * Create an empty file, filled with zeroes. Overwrite or append according to 'mode'.
  *   On error, -1 is returned.
  */
int create_empty_file(const STD_string& filename, LONGEST_INT nbytes, fopenMode mode=overwriteMode);


#endif


/**
  *
  * Returns the last system error as a string
  */
const char* lasterr();


/**
  *
  * Halt process/thread for 'ms' miliseconds.
  */
void sleep_ms(unsigned int ms);


/**
  *
  * Returns the current time in s
  */
double current_time_s();


/**
  *
  * If denominator is zero the functions returns 0, otherwise numerator/denominator
  */
double secureDivision(double numerator, double denominator);

/**
  *
  * If denominator is zero the functions returns 0, otherwise 1/denominator
  */
inline double secureInv(double denominator) {return secureDivision(1.0, denominator); }



/**
  *
  * Same as getenv, but returns empty string instead of NULL if variable is not found
  */
const char* getenv_nonnull(const char* variable_name);


/**
  *
  * Returns true if the current platform has little endian byte order, otherwise false
  */
bool little_endian_byte_order();

/**
  *
  * Returns the number of (virtual) CPUs
  */
unsigned int numof_cores();

/**
  *
  * Returns sinc function with accurate results for small x
  * (in contrast to using sin(x)/x directly)
  */
double sinc(double x);


#ifndef NO_CMDLINE

/**
  *
  * Copies maximum of 'maxchar' characters from the argv[] which
  * follows an argv[] that matches 'option' into 'returnvalue';
  * returns TRUE if successful; 'maxchar' should NOT be greater
  * than the number of bytes allocated for 'returnvalue' even if
  * the number of chars in argv[] is smaller.
  * If 'modify' is true, remove argument from the list.
  */
int getCommandlineOption(int argc, char *argv[], const char *option, char *returnvalue, int maxchar,bool modify=true);


/**
  *
  * Returns TRUE if 'option' is found in argv[]
  * If 'modify' is true, remove argument from the list.
  */
int isCommandlineOption(int argc, char *argv[], const char *option,bool modify=true);


/**
  *
  * Copies maximum of 'maxchar' characters from the last argv[]
  * into 'returnvalue';
  * returns TRUE if successful; 'maxchar' should NOT be greater
  * than the number of bytes allocated for 'returnvalue' even if
  * the number of chars in argv[] is smaller
  */
int getLastArgument(int argc, char *argv[], char *returnvalue, int maxchar,bool modify=true);


/**
  *
  * Returns whether one of the options '-h, -help, --help or --version' was given
  */
int hasHelpOption(int argc, char *argv[]);

/**
  *
  * Returns description of the standard help, version options
  */
const char* helpUsage();

#endif


/** @}
  */
#endif

