/***************************************************************************
                          tjprocess.h  -  description
                             -------------------
    begin                : Sat May 5 2007
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef TJPROCESS_H
#define TJPROCESS_H

#include <tjutils/tjstring.h>
#include <tjutils/tjarray.h>

/**
  * @addtogroup tjutils
  * @{
  */

////////////////////////////////////////////////////////////////////

// for debugging
class ProcessComponent {
 public:
  static const char* get_compName();
};

////////////////////////////////////////////////////////////////////


/**
  * Class to for process abstraction.
  */
class Process {

 public:

/**
  * Create non-running process.
  */
  Process() {reset();}

/**
  * Start new process with by executing 'cmdline'. If 'block_till_finished' is true,
  * return only after process has terminated.
  * If 'log_std_streams' is true, the output of stdout/stderr is catched and can
  * be queried later by finished().
  * Returns 'true' if no error occured.
  */
  bool start(const STD_string& cmdline, bool block_till_finished=false, bool log_std_streams=true);

/**
  * Return whether process has terminated, returns return value and stdout/stderr
  * output in 'proc_return_value' and 'stdout_result/stderr_result', respectively.
  * If 'block_till_finished' is true return only after process has terminated.
  */
  bool finished(int& proc_return_value, STD_string& stdout_result, STD_string& stderr_result, bool block_till_finished=false);

/**
  * Return whether process has terminated, returns return value in 'proc_return_value'.
  * Writes stdout/stderr of child process to the log.
  * If 'block_till_finished' is true return only after process has terminated.
  */
  bool finished(int& proc_return_value, bool block_till_finished=false);

/**
  * Kill process. Additionlly, kill child processes whose program name is in 'extra_kill'.
  */
  bool kill(const svector& extra_kill=svector());

/**
  * Start process by executing 'cmdline'. Return after process has terminated with
  * stdout/stderr output in 'stdout_result/stderr_result'. Returns return code of the process.
  */
  static int system(const STD_string& cmdline, STD_string& stdout_result, STD_string& stderr_result);


  // Comparison operators for list of processes with STL replacement
  bool operator == (const Process& op) const {return pid==op.pid;}
  bool operator <  (const Process& op) const {return pid!=op.pid;}
  bool operator != (const Process& op) const {return !(*this==op);}

 private:

  static bool read_pipe(int fd, STD_string& result);
  void reset() {pid=0; stdout_child=-1; stderr_child=-1;}

  int pid;
  int stdout_child;
  int stderr_child;
};



/** @}
  */
#endif
