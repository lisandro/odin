#include "tjlog.h"
#include "tjvector.h"

#include "tjhandler_code.h"


STD_string LogMessage::str(unsigned int maxwidth, bool include_comp) const {

  // crop beginning of strings if too long
  STD_string objCrop;
  unsigned int len=obj.length();
  if(len>MAX_LOG_STRINGSIZE) objCrop=obj.substr(len-MAX_LOG_STRINGSIZE, MAX_LOG_STRINGSIZE);
  else objCrop=obj;
  STD_string funcCrop;
  len=func.length();
  if(len>MAX_LOG_STRINGSIZE) funcCrop=func.substr(len-MAX_LOG_STRINGSIZE, MAX_LOG_STRINGSIZE);
  else funcCrop=func;

  STD_string line;

  if(include_comp) line+=comp+STD_string(MAX_COMPONENT_SIZE-comp.length(),' ')+"|";

  if(level==errorLog) line+="ERROR: ";
  if(level==warningLog) line+="WARNING: ";

  line+=objCrop;

  if(obj.length()) line+=".";

  line+=funcCrop+" : "+txt;

  if( maxwidth && (line.length()>maxwidth) ) { // zero linewidth indicates infinite length
    line=line.substr(0,maxwidth);
    line+=STD_string("...");
  }
  return line;
}

//////////////////////////////////////////////////////////////////////////////////////////////////

void default_tracefunction(const LogMessage& msg) {
     // Use stderr because 'real' cerr is not available with STREAM_REPLACEMENT
    fprintf(stderr,"%s",msg.str().c_str()); // Extra '%s' format string required for Debian compiler hardening
    fflush(stderr); // print immediately
  }


//////////////////////////////////////////////////////////////////////////////////////////////////


struct LogBaseAlloc : public LogBase {};


void LogBase::set_log_level(const char* compname, logPriority level) {
  LogBaseAlloc alloc; // allocate global

  // modify level of component already registered
  STD_map<STD_string,log_component_fptr>::const_iterator it=global->components.find(compname);
  if(it!=global->components.end()) {
    log_component_fptr fp=it->second;
    fp(level);
  }

  // store initialization level
  global->init_level[compname]=level;

  // disable uniform initialization level
  global->uniform_init_level=ignoreArgument;
}



void LogBase::set_uniform_log_level(logPriority level) {
  LogBaseAlloc alloc; // allocate global

  // modify levels of components already registered
  for(STD_map<STD_string,log_component_fptr>::const_iterator it=global->components.begin();it!=global->components.end();++it) {
    log_component_fptr fp=it->second;
    if(fp) fp(level);
  }

  // set all initialization levels to the uniform level
  for(STD_map<STD_string,logPriority>::iterator it2=global->init_level.begin();it2!=global->init_level.end();++it2) {
    it2->second=level;
  }

  global->uniform_init_level=level;
}


#ifndef NO_CMDLINE

void LogBase::parse_log_cmdline_options(int argc, char *argv[], const char* opt, logPriority base) {
  char value[ODIN_MAXCHAR];

  // parse cmdline opts
  while(getCommandlineOption(argc,argv,opt,value,ODIN_MAXCHAR)) {
    STD_string arg(value);

    STD_string::size_type pos=arg.find(":");
    if(pos==STD_string::npos) {
      set_uniform_log_level(logPriority(base+atoi(arg.c_str())));
    } else {
      STD_string compname=extract(arg,"",":");
      STD_string levelstr=extract(arg,":","");
      set_log_level(compname.c_str(),logPriority(base+atoi(levelstr.c_str())));
    }
  }

}

bool LogBase::set_log_levels(int argc, char *argv[], bool trigger_error) {
  LogBaseAlloc alloc; // allocate global


  if(trigger_error && global && global->components.size()) {
    STD_cerr << "ERROR: LogBase::set_log_levels: global already initialized with the following components:" << STD_endl; // trigger error if static members have already initialized log handlers
    for(STD_map<STD_string,log_component_fptr>::iterator it=global->components.begin();it!=global->components.end();++it) {
      STD_cerr << "  " << it->first << STD_endl;
    }
    return true;
  }

  parse_log_cmdline_options(argc,argv, "-v", noLog);
  parse_log_cmdline_options(argc,argv, "-d", RELEASE_LOG_LEVEL); // backwards compatability

  return false;
}
#endif


STD_string LogBase::get_usage() {
  STD_string result;
  result+="-v <loglevel> or <component:loglevel> for debugging/tracing all components or a single component, respectively. ";
  result+="Possible values for loglevel are: ";
  int upperLevel=numof_log_priorities;
#ifndef ODIN_DEBUG
  upperLevel=RELEASE_LOG_LEVEL+1;
#endif
  for(int i=0; i<upperLevel; i++) {
    result+=itos(i)+"("+logPriorityLabel[i]+")";
    if(i<(upperLevel-1)) result+=", ";
  }
  result+=".";
  return result;
}


bool LogBase::register_component(const char* name, log_component_fptr func) {
  LogBaseAlloc alloc; // allocate global

//  STD_cout << "LogBase::register_component: name/func=" << name << "/" << (void*)func << STD_endl;

  if(global) { // might not be initialized yet when bootstrapping Log component
    (global->components)[name]=func;

    // set initial log level from cache, if available
    if(global->uniform_init_level!=ignoreArgument) {
      func(global->uniform_init_level);
    } else {
      STD_map<STD_string,logPriority>::iterator it2=global->init_level.find(name);
      if(it2!=global->init_level.end()) func(it2->second);
    }
    return true;
  }
  return false;
}


void LogBase::unregister_component(const char* name) {
//  STD_cout << "LogBase::unregister_component: name=" << name << STD_endl;
  if(global) {
    STD_map<STD_string,log_component_fptr>::iterator it=global->components.find(name);
    if(it!=global->components.end()) global->components.erase(it);
  }
}


static STD_string levels_retval;


const char* LogBase::get_levels() {
  if(!global) return "";
  levels_retval="";
  for(STD_map<STD_string,log_component_fptr>::iterator it=global->components.begin(); it!=global->components.end(); ++it) {
//    STD_cout << "LogBase::get_levels: it->first=" << (void*)(it->first) << STD_endl;
    levels_retval+=STD_string(it->first)+" ";
    log_component_fptr fp=it->second;
    if(fp) levels_retval+=itos(fp(ignoreArgument))+"\n";
  }
  return levels_retval.c_str();
}


void LogBase::set_levels(const char* str) {
  svector lines(tokens(str,'\n'));
  for(unsigned int i=0; i<lines.size(); i++) {
    svector tokens_per_line(tokens(lines[i]));
    if(tokens_per_line.size()>=2) {
      set_log_level(tokens_per_line[0].c_str(), logPriority(atoi(tokens_per_line[1].c_str())));
    }
  }
}


void LogBase::flush_oneline(const STD_string& txt, logPriority level) {
  if(global && global->tracefunc) { // check for global because it might be called after Static::destroy_all
    LogMessage msg;
    msg.level=level;
    msg.comp=compLabel;
    if(objLabel) msg.obj=objLabel;
    if(namedObj) msg.obj=namedObj->get_label();
    msg.func=funcName;
    msg.txt=txt;
    global->tracefunc(msg);
  }
}


void LogBase::set_log_output_function(tracefunction func) {
  LogBaseAlloc alloc; // allocate global
  global->tracefunc=func;
}



void LogBase::init_static() {
  global.init("LogBaseGlobal");
}

void LogBase::destroy_static() {
  global.destroy();
}

template class SingletonHandler<LogBase::Global,true>;
SingletonHandler<LogBase::Global,true> LogBase::global;


EMPTY_TEMPL_LIST bool StaticHandler<LogBase>::staticdone=false;
