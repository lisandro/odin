/***************************************************************************
                          tjvallist.h  -  description
                             -------------------
    begin                : Wed Apr 14 2004
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef TJVALLIST_H
#define TJVALLIST_H

#include <tjutils/tjvector.h>
#include <tjutils/tjlabel.h>

/**
  * @addtogroup tjutils
  * @{
  */

/**
  * This class holds nested lists (tree) of values.
  * Each node can contain a single value, or a list of other nested lists.
  * In addition, it contains a repetition counter for this node.
  * Its main purpose is to retrieve value lists  (e.g. frequency lists, delay lists, reco indices) from the sequence tree.
  */
template <class T>
class ValList : public virtual Labeled {


/**
  * Actual data with reference counting
  */
  struct ValListData {

    ValListData() : val(0), times(1), sublists(0), elements_size_cache(0), references(0) {}

    ValListData(const ValListData& vld) : times(vld.times), elements_size_cache(vld.elements_size_cache), references(0) {
      if(vld.val) val=new T(*vld.val);
      else val=0;
      if(vld.sublists) sublists=new STD_list< ValList<T> >(*vld.sublists);
      else sublists=0;
    }

    ~ValListData() {
      if(sublists) delete sublists;
      if(val) delete val;
    }


    T* val; // zero pointer indicates 'value not set'
    unsigned int times;
    STD_list< ValList<T> >* sublists; // zero pointer indicates 'list not set'
    unsigned int elements_size_cache; // cache for the number of values of one repetition of this node
    unsigned short references; // reference counter
  };


 public:

/**
  * Empty node with given label and repetitions
  */
  ValList(const STD_string& object_label="unnamedValList", unsigned int repetitions=1);

/**
  * Node with a single value
  */
  ValList(T value);

/**
  * Copy constructor
  */
  ValList(const ValList<T>& vl);

  ~ValList();

/**
  * Assignment operator
  */
  ValList& operator = (const ValList<T>& vl);

/**
  * Make node single value
  */
  ValList& set_value(T value);

/**
  * Append another node to this
  */
  ValList& add_sublist(const ValList<T>& vl);

/**
  * Multiply number of repetitions of this node by 'reptimes'
  */
  ValList& multiply_repetitions(unsigned int reptimes) {copy_on_write(); data->times*=reptimes; return *this;}

/**
  * Returns unrolled values of nested list
  */
  STD_vector<T> get_values_flat() const;

/**
  * Returns unrolled values of nested list for one repetition of this list
  */
  STD_vector<T> get_elements_flat() const;

/**
  * Equal operator
  */
  bool operator ==  (const ValList<T>& vl) const;

/**
  * Comparison operator
  */
  bool operator < (const ValList<T>& vl) const;

/**
  * Acess i'th value in the nested list
  */
  T operator [] (unsigned int i) const;


/**
  * Returns number of values of one repetition of this node
  */
//  unsigned int elements_size() const;

/**
  * Returns number of values
  */
  unsigned int size() const {return data->times*data->elements_size_cache;}

/**
  * Serializes list to a string
  */
  STD_string printvallist() const;

/**
  * Parses list from string
  */
  bool parsevallist(const STD_string& str);

/**
  * Prints value list to stream
  */
  STD_ostream& print2stream(STD_ostream& os) const;

/**
  * Clears the value list
  */
  void clear();

 private:
  friend class ValListTest;

  bool equalelements (const ValList<T>& vl) const;

  ValList& increment_repetitions(unsigned int reptimes) {copy_on_write(); data->times+=reptimes; return *this;}

  unsigned int get_repetitions() const {return data->times;}

  void flatten_sublists();

  void copy_on_write();
  
  ValListData* data;
};

///////////////////////////////////////////////////

/** @}
  */
#endif
