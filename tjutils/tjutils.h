/***************************************************************************
                          tjutils.h  -  description
                             -------------------
    begin                : Thu Dec 5 2002
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef TJUTILS_H
#define TJUTILS_H

#ifndef TJUTILS_CONFIG_H
#define TJUTILS_CONFIG_H
#include <tjutils/config.h>
#endif

// boilerplate includes for a clean C++ environment
#include <tjutils/tjheap.h>
#include <tjutils/tjstd.h>
#include <tjutils/tjstream.h>


////////////////////////////////////////////////////////////////////////////


#endif
