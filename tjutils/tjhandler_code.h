#include <tjutils/tjhandler.h>
#include <tjutils/tjlog.h>


//////////////////////////////////////////////////////////////

template<class I>
Handled<I>::Handled() {}

template<class I>
 Handled<I>::~Handled() {
    Log<HandlerComponent> odinlog("Handled","~Handled");
    for(typename STD_list< const Handler<I>* >::const_iterator it=handlers.begin(); it!=handlers.end(); ++it) {
      (*it)->handled_remove(this); // do not call L::remove here because it will modify handlers list
    }
  }


template<class I>
  const  Handled<I>& Handled<I>::set_handler(const Handler<I>& handler) const {
    handlers.push_back(&handler);
    return *this;
  }

template<class I>
  const  Handled<I>& Handled<I>::erase_handler(const Handler<I>& handler) const {
    handlers.remove(&handler);
    return *this;
  }


/////////////////////////////////////////////


template<class I>
  Handler<I>::Handler() {
    handledobj=0;
  }

template<class I>
  Handler<I>::Handler(const Handler& handler) {
    Handler<I>::operator = (handler);
  }

template<class I>
  Handler<I>& Handler<I>::operator = (const Handler& handler) {
    clear_handledobj();
    I hd=handler.get_handled();
    if(hd) set_handled(hd);
    return *this;
  }


template<class I>
  Handler<I>::~Handler() {
    Log<HandlerComponent> odinlog("Handler","~Handler");
    clear_handledobj();
  }

template<class I>
  const Handler<I>& Handler<I>::clear_handledobj() const {
    Log<HandlerComponent> odinlog("Handler","clear_handledobj");
    ODINLOG(odinlog,normalDebug) << "handledobj=" << (void*)handledobj << STD_endl;
    if(handledobj) handledobj->Handled<I>::erase_handler(*this);
    handledobj=0;
    return *this;
  }

template<class I>
  const Handler<I>& Handler<I>::set_handled(I handled) const {
    Log<HandlerComponent> odinlog("Handler","set_handled");
    ODINLOG(odinlog,normalDebug) << "handled=" << (void*)handled << STD_endl;
    clear_handledobj();
    handled->Handled<I>::set_handler(*this);
    handledobj=handled;
    return *this;
  }



template<class I>
  I Handler<I>::get_handled() const {
    return handledobj;
  }


template<class I>
  const Handler<I>& Handler<I>::handled_remove(Handled<I>* handled) const {
    Log<HandlerComponent> odinlog("Handler","handled_remove");
    I handledtype=static_cast<I>(handled);
    ODINLOG(odinlog,normalDebug) << "handledtype=" << (void*)handledtype << STD_endl;
    if(handledtype) handledobj=0;
    else ODINLOG(odinlog,errorLog) << "Unable to remove handled!" << STD_endl;
    return *this;
  }

////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////



template<class T, bool thread_safe>
  void SingletonHandler<T,thread_safe>::init (const char* unique_label) {
    // NOTE:  Debug uses SingletonHandler -> do not use Debug in here
    singleton_label=new STD_string;
    mutex=0;
    if(thread_safe) mutex=new Mutex();
    (*singleton_label)=unique_label;
    if(!get_external_map_ptr(unique_label)) {
      ptr=new T;
      ptr->set_label(unique_label);
      (*get_singleton_map())[unique_label]=this; // make sure singleton_map is allocated
    } else {
      ptr=0;
    }
  }

template<class T, bool thread_safe>
  void SingletonHandler<T,thread_safe>::destroy () {
    if(ptr) delete ptr; ptr=0;
    delete singleton_label;
   if(mutex) delete mutex;
  }

template<class T, bool thread_safe>
  void SingletonHandler<T,thread_safe>::copy(T& destination) const {
    T* p=get_map_ptr();
    if(p) destination=(*p);
  }


template<class T, bool thread_safe>
  T* SingletonHandler<T,thread_safe>::get_map_ptr() const {
    if(ptr) return ptr; // fast return without touching the map
    if(singleton_map_external) {
      T* ext_ptr=(T*)get_external_map_ptr(*singleton_label);
      if(ext_ptr) ptr=ext_ptr;
    }
    return ptr;
  }
