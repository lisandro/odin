/***************************************************************************
                          tjfeedback.h  -  description
                             -------------------
    begin                : Fri Jun 6 2003
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/


#ifndef TJFEEDBACK_H
#define TJFEEDBACK_H

#include <tjutils/tjutils.h>
#include <tjutils/tjthread.h>


/**
  * @addtogroup tjutils
  * @{
  */


/**
  *
  *  Base class for all display drivers of a progress, e.g. console, widget, ...
  */
class ProgressDisplayDriver {

 public:

  virtual ~ProgressDisplayDriver() {}

/**
  * Initializes the driver to display to expect 'nsteps' calls to 'increase()'.
  * The message 'txt' will be displayed prior to the progress report.
  */
  virtual void init(unsigned int nsteps, const char* txt) = 0;

/**
  * Increments progress display.
  * The message 'subj', if non-null, will be displayed.
  */
  virtual void increase(const char* subj) = 0;

/**
  * Refreshes display without increasing progress.
  * Returns true if cancel is requested by the driver.
  */
  virtual bool refresh() = 0;

};

//////////////////////////////////////////////////////


/**
  *
  * Class to preset the progress of an operation to the user.
  * The implementation is thread-safe.
  */
class ProgressMeter {

 public:

/**
  * Creates progress meter using display driver 'disp' which should
  * exist for the whole life time of the progress meter.
  */
  ProgressMeter(ProgressDisplayDriver& disp) : display(&disp) {}

/**
  * Initialize new task with 'total_steps' increments.
  * The message 'txt' will be displayed prior to the progress report.
  */
  ProgressMeter& new_task(unsigned int total_steps, const char* txt = 0);

/**
  * Increments progress display.
  * The message 'subj', if non-null, will be displayed.
  * Returns true if cancel is requested by the driver.
  */
  bool increase_counter(const char* subj = 0);

/**
  * Refreshes display without increasing progress.
  * Returns true if cancel is requested by the driver.
  */
  bool refresh_display();


 private:

  // disable copy construction and copying
  ProgressMeter(const ProgressMeter&) {}
  ProgressMeter& operator = (const ProgressMeter&) {return *this;}

  ProgressDisplayDriver* display;
  Mutex mutex;


};


//////////////////////////////////////////////////////

#ifndef NO_CMDLINE

class ProgressDisplayConsole : public virtual ProgressDisplayDriver {

 public:
  ProgressDisplayConsole() : counter(0), nsteps_cache(0), done(false) {}

  // Implementing virtual functions of ProgressDisplayDriver
  void init(unsigned int nsteps, const char* txt);
  void increase(const char*);
  bool refresh() {return false;}

 private:
  unsigned int counter, nsteps_cache, old_perc;
  bool done;
};

#endif

/** @}
  */
#endif
