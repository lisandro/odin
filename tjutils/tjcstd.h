/***************************************************************************
                          tjcstd.h  -  description
                             -------------------
    begin                : Thu Jun 16 2004
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef TJCSTD_H
#define TJCSTD_H

#ifndef TJUTILS_CONFIG_H
#define TJUTILS_CONFIG_H
#include <tjutils/config.h>
#endif


// boilerplate C headers
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <errno.h>


#ifdef HAVE_CTYPE_H
#include <ctype.h>
#endif




// missing C functions

#ifndef HAVE_CTYPE_H
int isdigit(int c);
int isalpha(int c);
int isalnum(int c);
int isspace(int c);
int islower(int c);
int tolower(int c);
int isupper(int c);
int toupper(int c);
#endif


#ifndef HAVE_J1
double j1(double x);
#endif

#ifndef HAVE_ACOSH
double acosh(double x);
#endif


#ifdef VXWORKS
// fix broken exp function
#define exp exp4vxworks
double exp4vxworks(double x);
#endif


#ifndef HAVE_DL
void *dlopen (const char *filename, int flag);
const char *dlerror(void);
void *dlsym(void *handle, char *symbol);
int dlclose (void *handle);
#endif

//make VStudio 2005 happy with snprintf
#ifdef USING_WIN32
#ifndef HAVE_SNPRINTF
#define snprintf _snprintf
#endif //HAVE_SNPRINTF
#endif //USING_WIN32


#endif
