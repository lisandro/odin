/***************************************************************************
                          tjstate.h  -  description
                             -------------------
    begin                : Sun Sep 28 2003
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef TJSTATE_H
#define TJSTATE_H

#include <tjutils/tjutils.h>
#include <tjutils/tjlabel.h>
#include <tjutils/tjlog.h>

/**
  * @addtogroup tjutils
  * @{
  */

///////////////////////////////////////////////////////////////

class StateComponent {
 public:
  static const char* get_compName();
};

///////////////////////////////////////////////////////////////


template <class T> class State : public Labeled {

 public:
  typedef bool (T::* transition) ();

  State(T* statemachine, const char* statelabel, State* prerequired_state, transition tr)
    : Labeled(statelabel), machine(statemachine),  pre_state(prerequired_state), trans(tr) {
    Log<StateComponent> odinlog(this,"State()");    
  }

  bool obtain_state() {
    Log<StateComponent> odinlog(this,"obtain_state");


    // return if we are already in the desired state
    if(machine->current_state==this) return true;

    // try a registered direct transition
    if(machine->direct_transition(this)) return true;


    if(pre_state) { // check whether a prerequisite state exists and obtain that state
      if(!pre_state->obtain_state()) return false;
    }

    bool result=(machine->*trans)(); // perform the transition
    if(result) machine->current_state=this;
    return result;
  }

 private:
  T* machine;
  State* pre_state;
  transition trans;
};

///////////////////////////////////////////////////////////////

template <class T>
struct DirectTransition {

  DirectTransition(State<T>* init, State<T>* dest, typename State<T>::transition tr) : init_state(init), dest_state(dest), trans(tr) {
    Log<StateComponent> odinlog("DirectTransition","DirectTransition()");
  }

  bool operator == (const DirectTransition<T>& dt) const {return init_state==dt.init_state && dest_state==dt.dest_state && trans==dt.trans;}
  bool operator != (const DirectTransition<T>& dt) const {return !(*this==dt);}
  bool operator <  (const DirectTransition<T>& dt) const {return init_state<dt.init_state && dest_state<dt.dest_state;}

  State<T>* init_state;
  State<T>* dest_state;

  typename State<T>::transition trans;


};


///////////////////////////////////////////////////////////////


template <class T>
class StateMachine {

 protected:
  StateMachine(State<T>* first_state) : current_state(first_state) {
    Log<StateComponent> odinlog("StateMachine","StateMachine()");
  }

  const STD_string& get_current_state_label() const {return current_state->get_label();}

  void register_direct_trans(State<T>* init, State<T>* dest, typename State<T>::transition tr) {
    direct_trans.push_back( DirectTransition<T>(init,dest,tr) );
  }

  bool direct_transition(State<T>* dest) {
   typename STD_list<DirectTransition<T> >::iterator it;
   for(it=direct_trans.begin(); it!=direct_trans.end(); ++it) {
     if( (it->init_state==current_state) && (it->dest_state==dest) ) {
       typename State<T>::transition tt=it->trans;
       bool result=((T*)(this)->*tt)();
       if(result) current_state=dest;
       return result;
     }
   }
   return false;
  }

 private:
  friend class State<T>;

  STD_list<DirectTransition<T> > direct_trans;

  State<T>* current_state;
};


/** @}
  */
#endif
