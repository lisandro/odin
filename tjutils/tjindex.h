/***************************************************************************
                          tjindex.h  -  description
                             -------------------
    begin                : Tue Aug 13 2002
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef TJINDEX_H
#define TJINDEX_H

#include <tjutils/tjutils.h>
#include <tjutils/tjstatic.h>
#include <tjutils/tjlabel.h>
#include <tjutils/tjhandler.h>



/**
  * @addtogroup tjutils
  * @{
  */


class Index {
 public:
  static const char* get_compName();
};


//////////////////////////////////////////////////////////

// mapping between UniquIndex types and typenames
struct UniqueIndexMap : public STD_map<STD_string, STD_list<unsigned int> >, public Labeled {

  UniqueIndexMap() : contiguous(true) {}

  unsigned int get_index(STD_list<unsigned int>::iterator& index, const STD_string& type, unsigned int max_instances);

  void remove_index(const STD_list<unsigned int>::iterator& index, const STD_string& type);


 private:
  bool contiguous; // cache whether list of indices is contiguous for fast assignment of indices
  unsigned int assign_index(STD_list<unsigned int>::iterator& index, const STD_string& type); // Get the next lowest index which is available
};


//////////////////////////////////////////////////////////

// keeps track of all index lists
class UniqueIndexBase : public StaticHandler<UniqueIndexBase> {

 public:

  // for StaticHandler
  static void init_static()    {indices_map.init("indices_map");}
  static void destroy_static() {indices_map.destroy();}

 protected:

  UniqueIndexBase() {} // Use only as base class

  static SingletonHandler<UniqueIndexMap,true> indices_map; // global lists of indices

};


//////////////////////////////////////////////////////////


/**
  * This template class provides a unique index, i.e. an unique
  * unsigned integer number for all instances of the (derived) class.
  * A new index for the object is created whenever a call to 'get_index()'
  * is issued the first time. The assigned index is removed from the list
  * of assigned indices if the object is destroyed.
  * Please note that the index of temporary objects is deleted from
  * the list of indices, even it is assigned to another object.
  * The class T must have two static member functions, 'get_typename()'
  * and 'get_max_instances()' to return a unique label and the
  * maximum number of instances per class, respectively.
  */
template<class T>
class UniqueIndex : public UniqueIndexBase {

 public:
  UniqueIndex() {init();}

  UniqueIndex(const UniqueIndex<T>&) {init();}

  UniqueIndex<T>& operator = (const UniqueIndex<T>&) {erase(); init(); return *this;}

  ~UniqueIndex() {erase();}


/**
  * Returns a unique index for this object.
  */
  unsigned int get_index() const {
    return indices_map->get_index(index, T::get_typename(), T::get_max_instances());
  }


 private:
  void init() { // Start with unassigned index (pointing to end())
    index=indices_map->operator[](T::get_typename()).end();
  }

  void erase() { // Remove index
    indices_map->remove_index(index, T::get_typename());
  }


  mutable STD_list<unsigned int>::iterator index;


};

/** @}
  */
#endif
