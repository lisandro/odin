/***************************************************************************
                          tjtest.h  -  description
                             -------------------
    begin                : Mon Dec 12 2005
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef TJTEST_H
#define TJTEST_H

#include <tjutils/tjstatic.h>
#include <tjutils/tjlabel.h>

/**
  * @addtogroup tjutils
  * @{
  */

#ifndef NO_UNIT_TEST

/**
  * Base class for unit tests. Derive from this, implement
  * the check() function and place a static instanve in the
  * source file to register it for the test run.
  */
class UnitTest : public StaticHandler<UnitTest>, public Labeled {

 public:

  static int check_all();

  static void init_static();
  static void destroy_static();

  static const char* get_compName();

 protected:

  UnitTest(const STD_string& testlabel);
  virtual ~UnitTest() {}

 private:

  virtual bool check() const = 0;

  static STD_list<UnitTest*>* tests;

};

#endif

/** @}
  */
#endif
