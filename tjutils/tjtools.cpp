#include "tjtools.h"
#include "tjutils.h"
#include "tjcstd.h"
#include "tjlog.h"
#include "tjlog_code.h"
#include "tjstring.h"
#include "tjtest.h"
#include "tjvector.h" // for testing browse_dir
#include "tjstd.h" // for unit test



#include <stdio.h>


#ifdef HAVE_UNISTD_H
#include <unistd.h>
#include <errno.h>
#endif



#ifdef HAVE_SYS_STAT_H
#include <sys/stat.h>
#endif

#ifdef HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif

#ifdef HAVE_DIRENT_H
#include <dirent.h>
#endif

#ifdef USING_WIN32
#include <direct.h>
#include <windows.h>
#include <io.h>
#endif

#ifdef HAVE_TIME_H
#include <time.h>
#endif

#ifdef HAVE_SYS_TIME_H
#include <sys/time.h>
#endif


// for mmap
#ifdef HAVE_SYS_MMAN_H
#include <sys/mman.h>
#include <fcntl.h>
#endif



class TjTools {
 public:
  static const char* get_compName() {return "tjtools";}
};
LOGGROUNDWORK(TjTools)


////////////////////////////////////////////////////////////////////

double norm (double x, double y) {
  double result;
  result = sqrt (x * x + y * y);
  return (result);
}

/////////////////////////////////////////////////////////////

double norm3 (double x, double y, double z) {
  double result;
  result = sqrt (x * x + y * y + z * z);
  return (result);
}

/////////////////////////////////////////////////////////////

double maxof3(double f1,double f2,double f3) {
  double result =0.0;
  if (f1>=f2) result=f1; else result=f2;
  if (result>=f3) return(result); else return(f3);
}


/////////////////////////////////////////////////////////////

double secureDivision(double numerator, double denominator) {
  if(denominator==0.0) return 0.0;
  return numerator/denominator;
}

/////////////////////////////////////////////////////////////

const char* getenv_nonnull(const char* variable_name) {
  Log<TjTools> odinlog("","getenv_nonnull");
  const char* result="";
  const char* env=getenv(variable_name);
  if(env) result=env;
  ODINLOG(odinlog,normalDebug) << "result(" << variable_name << ")=" << result << STD_endl;
  return result;
}

/////////////////////////////////////////////////////////////

bool little_endian_byte_order() {
  int one_bit_set_in_least_significant=1;
  unsigned char* first_byte=(unsigned char*)&one_bit_set_in_least_significant;
  return (*first_byte)!=0;
}

/////////////////////////////////////////////////////////////

unsigned int numof_cores() {
  unsigned int result=1;

#ifdef USING_WIN32

  SYSTEM_INFO sysinfo;
  GetSystemInfo( &sysinfo );
  result = sysinfo.dwNumberOfProcessors;

#else

#if defined(HAVE_UNISTD_H) && defined(HAVE_SYSCONF)
#ifdef _SC_NPROCESSORS_ONLN
  result=sysconf(_SC_NPROCESSORS_ONLN);
#endif
#endif

#endif
  return result;
}

/////////////////////////////////////////////////////////////


#ifndef NO_FILEHANDLING

const char* modestring(fopenMode mode) {
  if(mode==readMode) return "rb";
  if(mode==overwriteMode) return "w+b";
  if(mode==appendMode) return "a+b";
  return "";
}

LONGEST_INT filesize(const char *filename) {
  Log<TjTools> odinlog("","filesize");
  LONGEST_INT result=-1;

#ifdef ODIN_STAT
  struct ODIN_STAT st;
  if(!ODIN_STAT(filename, &st)) result=st.st_size;
  else if(errno!=ENOENT) {
    ODINLOG(odinlog,errorLog) << "stat(" << filename << "): " << lasterr() << STD_endl;
  }
#else

  FILE *file_ptr;

  file_ptr = ODIN_FOPEN(filename, modestring(readMode));
  if(file_ptr == NULL) {
    if(errno!=ENOENT) {
      ODINLOG(odinlog,errorLog) << "fopen(" << filename << "): " << lasterr() << STD_endl;
    }
    return result;
  }
  ODINLOG(odinlog,normalDebug) << "file_ptr(" << filename << ")=" << file_ptr << STD_endl;

  if(fgetc(file_ptr) == EOF) {
    result=0;
  } else {
    if(ODIN_FSEEK(file_ptr, 0, SEEK_END)) {
       if(errno!=ENOENT) {
         ODINLOG(odinlog,errorLog) << "fseek(" << filename << "): " << lasterr() << STD_endl;
       }
    } else result=ODIN_FTELL(file_ptr);
  }

  fclose(file_ptr);
#endif

  return result;
}

/////////////////////////////////////////////////////////////


char getpwd_buff[ODIN_MAXCHAR];

const char* getpwd() {
  Log<TjTools> odinlog("","getpwd");
  const char* result="";
#ifdef USING_WIN32
  result=_getcwd(getpwd_buff,ODIN_MAXCHAR);
#else
#ifdef HAVE_UNISTD_H
  result=getcwd(getpwd_buff,ODIN_MAXCHAR);
#endif
#endif
  if(result==NULL) {
    ODINLOG(odinlog,errorLog) << lasterr() << STD_endl;
  }
  return result;
}


/////////////////////////////////////////////////////////////

int chpwd(const char *dirname) {
  Log<TjTools> odinlog("","chpwd");
  int result=0;
#ifdef USING_WIN32
  result=_chdir(dirname); // will consider drive letter
#else
#ifdef HAVE_UNISTD_H
  result=chdir(dirname);
#endif
#endif
  if(result<0) {
    ODINLOG(odinlog,errorLog) << "(" << dirname << ")" << lasterr() << STD_endl;
  }
  return result;
}

/////////////////////////////////////////////////////////////

int createdir(const char *dirname) {
  Log<TjTools> odinlog("","createdir");
  int result=0;
  if(checkdir(dirname)) return result; // already there
#ifdef USING_WIN32
  result=_mkdir(dirname);
  ODINLOG(odinlog,normalDebug) << "created USING_WIN32 dir" << dirname << STD_endl;
#else
#if defined(HAVE_SYS_STAT_H) && defined(HAVE_SYS_TYPES_H) && defined(HAVE_UNISTD_H)
  result=mkdir(dirname, S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH); // permission: 0755
#endif
#endif
  if(result<0) {
    ODINLOG(odinlog,errorLog) << "(" << dirname << ") " << lasterr() << STD_endl;
  }
  return result;
}

/////////////////////////////////////////////////////////////

bool checkdir(const char *dirname) {
  Log<TjTools> odinlog("","checkdir");
#ifdef USING_WIN32
  _finddata_t fileinfo;
  STD_string dirmatch=STD_string(dirname)+"\\*";
  long findhandle=_findfirst(dirmatch.c_str(), &fileinfo );
  if(findhandle!=-1) {
    _findclose(findhandle);
    return true;
  } else {
    ODINLOG(odinlog,normalDebug) << "(" << dirname << ") " << lasterr() << STD_endl;
  }
#else
#ifdef HAVE_DIRENT_H
  DIR* dp = opendir(dirname);
  if (dp != NULL) {
    closedir(dp);
    return true;
  }
#endif
#endif
  return false;
}

/////////////////////////////////////////////////////////////

int movefile(const char *src, const char* dst) {
  int result=0;
#ifdef USING_WIN32
  result=rename(src,dst);
#else
  result=system((STD_string("mv ")+src+" "+dst).c_str()); // rename() does not work across filesystems
#endif
  return result;
}

/////////////////////////////////////////////////////////////

int copyfile(const char *src, const char* dst) {
  Log<TjTools> odinlog("","copyfile");
  int result=0;
#ifdef USING_WIN32
  if(!CopyFileA(src,dst,FALSE)) result=-1;
#else
  result=system((STD_string("cp ")+src+" "+dst).c_str());
#endif
  if(result<0) {
    ODINLOG(odinlog,errorLog) << "(" << src << "," << dst << ") " << lasterr() << STD_endl;
  }
  return result;
}

/////////////////////////////////////////////////////////////

int rmfile(const char *fname) {
  Log<TjTools> odinlog("","rmfile");
  int result=0;
  if(filesize(fname)<0) return 0;

#ifdef USING_WIN32
  if(checkdir(fname)) {
    result=_rmdir(fname);
    if(result<0) {
      ODINLOG(odinlog,errorLog) << "_rmdir(" << fname << ") " << lasterr() << STD_endl;
    }
  } else {
    result=remove(fname);
    if(result<0) {
      ODINLOG(odinlog,errorLog) << "remove(" << fname << ") " << lasterr() << STD_endl;
    }
  }
#else
  result=remove(fname); // Works for both, files and directories
  if(result<0) {
    ODINLOG(odinlog,errorLog) << "(" << fname << ") " << lasterr() << STD_endl;
  }
#endif
  return result;
}

/////////////////////////////////////////////////////////////

int chperm(const char *fname, int perm) {
#ifdef USING_WIN32
  return _chmod(fname,perm);
#else
#if defined(HAVE_SYS_STAT_H) && defined(HAVE_SYS_TYPES_H) && defined(HAVE_UNISTD_H)
  return chmod(fname,perm);
#endif
#endif
  return 0;
}


/////////////////////////////////////////////////////////////


#define BLOCKSIZE 4096 // constant experession needed for VC++ 6
int create_empty_file(const STD_string& filename, LONGEST_INT nbytes, fopenMode mode) {
  Log<TjTools> odinlog("","create_empty_file");
  LONGEST_INT nblocks=nbytes/BLOCKSIZE;
  LONGEST_INT rest=nbytes%BLOCKSIZE;
  char buff[BLOCKSIZE];
  LONGEST_INT i;
  for(i=0; i<BLOCKSIZE; i++) buff[i]=0;

  FILE* fptr=ODIN_FOPEN(filename.c_str(),modestring(mode));
  if(fptr==NULL) {
    ODINLOG(odinlog,errorLog) << "fopen: " << lasterr() << STD_endl;
    return -1;
  }
  for(i=0; i<nblocks; i++) {
    if(!fwrite(buff,sizeof(char),BLOCKSIZE,fptr)) {
      fclose(fptr);
      ODINLOG(odinlog,errorLog) << "fwrite: " << lasterr() << STD_endl;
      return -1;
    }
  }
  if(rest) {
    if(!fwrite(buff,sizeof(char),rest,fptr)) {
      fclose(fptr);
      ODINLOG(odinlog,errorLog) << "fwrite(rest): " << lasterr() << STD_endl;
      return -1;
    }
  }
  fclose(fptr);
  return 0;
}

///////////////////////////////////////////////

#ifdef HAVE_SYS_MMAN_H // We need this helper function only for mmap

void offset_pagesize(LONGEST_INT offset, LONGEST_INT& result_offset, int& result_rest) {
  Log<TjTools> odinlog("","offset_pagesize");
  result_offset=offset;
  result_rest=0;
#ifdef HAVE_UNISTD_H
  int psize=getpagesize();
  if(psize) {
    result_offset=(offset/psize)*psize;
    result_rest=offset%psize;
  }
#endif
  ODINLOG(odinlog,normalDebug) << "offset/result_offset/result_rest=" << offset << "/" << result_offset << "/" << result_rest << STD_endl;
}
#endif

///////////////////////////////////////////////

void* filemap(const STD_string& filename, LONGEST_INT nbytes, LONGEST_INT offset, bool readonly, int& fd) {
  Log<TjTools> odinlog("","filemap");

  void* result=0;
  fd=-1;

  LONGEST_INT fsize=filesize(filename.c_str());

  LONGEST_INT total=nbytes+offset;

  ODINLOG(odinlog,normalDebug) << "nbytes/fsize/total/readonly = " << nbytes << "/" << fsize << "/" << total << "/" << readonly << STD_endl;

  // check size in readonly mode
  if(readonly) {
    if(fsize<total) {
      ODINLOG(odinlog,errorLog) << "File too small in readonly mode: filesize(" << filename << ")=" << fsize << ", total=" << total  << STD_endl;
      return result;
    }
  }

  // create empty file in non-readonly mode
  if(!readonly) {
    if(fsize<total) {
      ODINLOG(odinlog,normalDebug) << "creating file" << STD_endl;
      if(create_empty_file(filename,total)) {
        ODINLOG(odinlog,errorLog) << "Unable to create empty file >" << filename << "<" << STD_endl;
        return result;
      }
    }
  }


#ifdef HAVE_SYS_MMAN_H // The Unix (mmap) way of file mapping
  ODINLOG(odinlog,normalDebug) << "Using mmap" << STD_endl;

  LONGEST_INT offset_psize;
  int rest_psize;
  offset_pagesize(offset, offset_psize, rest_psize);

  int flags=O_RDWR;
  if(readonly) flags=O_RDONLY;
  fd = ODIN_OPEN( filename.c_str(), flags );
  ODINLOG(odinlog,normalDebug) << "fd = " << fd << STD_endl;

  if (fd < 0) {
    ODINLOG(odinlog,errorLog) << "unable to open file >" << filename << "< - " << lasterr() << STD_endl;
    return result;
  }

  if(filesize(filename.c_str())<total) {
    ODINLOG(odinlog,errorLog) << "Size of file >" << filename << "< to small for filemap" << STD_endl;
    close(fd); fd=-1;
    return result;
  }


  int prot=PROT_READ | PROT_WRITE;
  if(readonly) prot=PROT_READ;
  char* byte_ptr=(char*)ODIN_MMAP( 0, nbytes+rest_psize, prot, MAP_SHARED, fd, offset_psize );
  if ( byte_ptr==MAP_FAILED ) {
    ODINLOG(odinlog,errorLog) << "Cannot filemap file >" << filename << "< - " << lasterr() << STD_endl;
    close(fd); fd=-1;
    return 0;
  }
  result=byte_ptr+rest_psize;


#else // Emulate mmap by allocating memory and reading it from file
  ODINLOG(odinlog,normalDebug) << "Using fread" << STD_endl;

  const char* modeString=modestring(overwriteMode);
  if(readonly) modeString=modestring(readMode);

  fd=(int)ODIN_FOPEN(filename.c_str(), modeString);
  if(!fd) {
    ODINLOG(odinlog,errorLog) << "fopen: " << lasterr() << STD_endl;
    return result;
  }

  if(offset>0) {
    if(ODIN_FSEEK((FILE*)fd, offset, SEEK_SET)) {
      ODINLOG(odinlog,errorLog) << "fseek: " << lasterr() << STD_endl;
      fclose((FILE*)fd);
      return result;
    }
  }


  result=(void*)new char[nbytes];

  fread( result, nbytes, sizeof(char), (FILE*)fd);

  if(readonly) {
    // We close and reset the file pointer here so that fileunmap() knows that the file is readonly
    fclose((FILE*)fd);
    fd=0; // Zero indicates readonly mode
  }

#endif


  return result;
}

///////////////////////////////////////////////

void fileunmap(int fd, void* start, LONGEST_INT nbytes, LONGEST_INT offset) {
  Log<TjTools> odinlog("","fileunmap");
  ODINLOG(odinlog,normalDebug) << "fd/start/length=" << fd << "/" << start << "/" << nbytes << STD_endl;

#ifdef HAVE_SYS_MMAN_H // The Unix (mmap) way of file mapping

  LONGEST_INT offset_psize;
  int rest_psize;
  offset_pagesize(offset, offset_psize, rest_psize);

  void* mstart=(char*)start-rest_psize;
  LONGEST_INT mlength=nbytes+rest_psize;

  if ( msync(mstart, mlength, MS_SYNC) ) {
    ODINLOG(odinlog,errorLog) << "msync: "  << lasterr() << STD_endl;
  }
  if ( munmap(mstart, mlength) ) {
    ODINLOG(odinlog,errorLog) << "munmap: " << lasterr() << STD_endl;
  }
  close(fd);

#else // Emulate munmap by writing memory to file, de-allocating memory and closing the file

  if(fd>0) { // File is mapped with read/write access
    if(offset>0) {
      if(ODIN_FSEEK((FILE*)fd, offset, SEEK_SET)) {
        ODINLOG(odinlog,errorLog) << "fseek: " << lasterr() << STD_endl;
        fclose((FILE*)fd);
        delete[] (char*)start;
        return;
      }
    }
    if(!fwrite(start, sizeof(char), nbytes, (FILE*)fd)) {
      ODINLOG(odinlog,errorLog) << "fwrite: " << lasterr() << STD_endl;
    }
    fclose((FILE*)fd);
  }

  delete[] (char*)start;

#endif
}

///////////////////////////////////////////////

STD_string tempfile() {
  STD_string result;
  const char* ptr=tmpnam(NULL);
  if(ptr) result=ptr;
#ifdef USING_WIN32
  result=STD_string(getenv_nonnull("TEMP"))+SEPARATOR_STR+result;
#endif
  return result;
}


#endif // #ifndef NO_FILEHANDLING

/////////////////////////////////////////////////////////////

const char* lasterr() {
#ifdef USING_WIN32
  LPVOID msgbuf;
  FormatMessage(
    FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
    NULL,
    GetLastError(),
    MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
    (LPTSTR) &msgbuf,
    0,
    NULL);

  static STD_string result=(const char *)msgbuf;
  LocalFree(msgbuf);
  return result.c_str();
#else
#ifdef HAVE_UNISTD_H
  return strerror(errno);
#endif
#endif
  return "";
}


/////////////////////////////////////////////////////////////


void sleep_ms(unsigned int ms) {
  if(!ms) return;
#ifdef USING_WIN32
  Sleep(ms);
#else
#if defined(HAVE_TIME_H) && defined(HAVE_NANOSLEEP)
  timespec ts;
  ts.tv_sec=ms/1000;
  ts.tv_nsec=(ms%1000)*1000000;
  nanosleep(&ts,NULL);
#endif
#endif
}

/////////////////////////////////////////////////////////////

double current_time_s() {
  Log<TjTools> odinlog("","current_time_s");
  double result=-1;
#ifdef USING_WIN32
  FILETIME ft;
  GetSystemTimeAsFileTime(&ft);
  SYSTEMTIME currenttime;
  if(!FileTimeToSystemTime(&ft,&currenttime)) {
    ODINLOG(odinlog,errorLog) << lasterr() << STD_endl;
    return result;
  }
  result=3600.0*double(currenttime.wHour) + 60.0*double(currenttime.wMinute) + double(currenttime.wSecond) + 1e-3*double(currenttime.wMilliseconds);
#else
#if defined(HAVE_SYS_TIME_H) && defined(HAVE_TIME_H) && defined(HAVE_GETTIMEOFDAY)
  timeval currenttime;
  gettimeofday(&currenttime,0);
  result=double(currenttime.tv_sec) + 1e-6*double(currenttime.tv_usec);
#endif
#endif
  return result;
}



//////////////////////////////////////////////////////////

#ifndef NO_CMDLINE


int getCommandlineOption(int argc, char *argv[], const char *option, char *returnvalue, int maxchar,bool modify) {
  int i,b=0;
  for (i=1;i<argc;i++) {
    if(!strcmp(argv[i],option) && i<(argc-1)) {
      strncpy(returnvalue,argv[i+1],maxchar-1);
      returnvalue[maxchar-1]='\0';
      if(modify) {
        argv[i][0]='\0';
        argv[i+1][0]='\0';
      }
      b++;
      break;
    }
  }
  return(b);
}

int isCommandlineOption(int argc, char *argv[], const char *option,bool modify) {
  int i,b=0;
  for (i=1;i<argc;i++) {
    if(!strcmp(argv[i],option)) {
      b++;
      if(modify) {
        argv[i][0]='\0';
      }
    }
  }
  return(b);
}



int getLastArgument(int argc, char *argv[], char *returnvalue, int maxchar,bool modify)
{
  returnvalue[0]='\0';
  if (argc>1)
  {
/*   if( *argv[argc-2]!='-')
   {
*/
      strncpy(returnvalue,argv[argc-1],maxchar-1);returnvalue[maxchar-1]='\0';
      if(modify) argv[argc-1][0]='\0';
      return(1);
/*
   }
*/
  }
  return(0);
}


int hasHelpOption(int argc, char *argv[]) {
  if(isCommandlineOption(argc,argv,"--version")) {
    STD_cout << VERSION << STD_endl;
    exit(0); // return immediately for help2man
  }

  return isCommandlineOption(argc,argv,"-h")+
         isCommandlineOption(argc,argv,"--help")+
         isCommandlineOption(argc,argv,"-help");
}

const char* helpUsage() {
  return "-h, --help, -help, --version : Print help text or version information";
}


#endif



//////////////////////////////////////////////////////////////////////////////////////////////
// From GSL, i.e. from gsl_sf_bessel_j0()

double sinc(double x) {
  if(x==0.0) return 1.0;
   if(fabs(x) < 0.5) {
    const double y = x*x;
    const double c1 = -1.0/6.0;
    const double c2 =  1.0/120.0;
    const double c3 = -1.0/5040.0;
    const double c4 =  1.0/362880.0;
    const double c5 = -1.0/39916800.0;
    const double c6 =  1.0/6227020800.0;
    return 1.0 + y*(c1 + y*(c2 + y*(c3 + y*(c4 + y*(c5 + y*c6)))));
  } else {
    return sin(x)/x;
  }
}


//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////


#ifndef NO_UNIT_TEST
class TjToolsTest : public UnitTest {

 public:
  TjToolsTest() : UnitTest(TjTools::get_compName()) {}

 private:

  bool check() const {
    Log<UnitTest> odinlog(this,"check");

#ifndef NO_FILEHANDLING

    STD_string dir(tempfile());
    if(createdir(dir.c_str())) {
      ODINLOG(odinlog,errorLog) << "createdir failed" << STD_endl;
      return false;
    }

    STD_list<STD_string> contentlist;
    contentlist.push_back("content");
    contentlist.push_back("");

    for(STD_list<STD_string>::iterator it=contentlist.begin(); it!=contentlist.end(); ++it) {

      STD_string testcont=*it;
      STD_string srcfile=dir+SEPARATOR_STR+"src";
      if(write(testcont, srcfile)) {
        ODINLOG(odinlog,errorLog) << "write(testcont=>" << testcont << "<) failed" << STD_endl;
        return false;
      }

      int ntestfiles=5;
      for(int i=0; i<ntestfiles; i++) {
        STD_string dstfile=dir+SEPARATOR_STR+itos(i);
        if(copyfile(srcfile.c_str(), dstfile.c_str())) {
          ODINLOG(odinlog,errorLog) << "copyfile(testcont=>" << testcont << "<) failed" << STD_endl;
          return false;
        }
      }

      if(rmfile(srcfile.c_str())) {
        ODINLOG(odinlog,errorLog) << "rmfile(srcfile, testcont=>" << testcont << "<) failed" << STD_endl;
        return false;
      }

      svector browsefiles=browse_dir(dir,false,true);
      int nbrowsefiles=browsefiles.size();
      if(nbrowsefiles!=ntestfiles) {
        ODINLOG(odinlog,errorLog) << "ntestfiles/nbrowsefiles(testcont=>" << testcont << "<)=" << ntestfiles << "/" << nbrowsefiles << STD_endl;
        return false;
      }


      for(int i=0; i<nbrowsefiles; i++) {
        STD_string fname=dir+SEPARATOR_STR+browsefiles[i];

        // testing filesize
        int fsize=filesize(fname.c_str());
        int strsize=testcont.length();
        if(fsize!=strsize) {
          ODINLOG(odinlog,errorLog) << "filesize(testcont=>" << testcont << "<) mismatch, fsize/strsize=" << fsize << "/" << strsize << STD_endl;
          return false;
        }

        STD_string filecont;
        if(load(filecont, fname)) {
          ODINLOG(odinlog,errorLog) << "load(testcont=>" << testcont << "<) failed" << STD_endl;
          return false;
        }
        if(filecont!=testcont) {
          ODINLOG(odinlog,errorLog) << "filecont(testcont=>" << testcont << "<)=" << filecont << STD_endl;
          return false;
        }
        if(rmfile(fname.c_str())) {
          ODINLOG(odinlog,errorLog) << "rmfile(browsefiles, testcont=>" << testcont << "<) failed" << STD_endl;
          return false;
        }
      }

    }

    // Check whether directory is empty
    svector browsefiles_check=browse_dir(dir,false,true);
    if(browsefiles_check.size()) {
      ODINLOG(odinlog,errorLog) << "dir not empty, browsefiles=" << browsefiles_check.printbody() << STD_endl;
      return false;
    }

//#ifndef USING_WIN32 // Cannot delete dir because it is still opened by another process !?
    if(rmfile(dir.c_str())) {
      ODINLOG(odinlog,errorLog) << "rmfile(dir) failed" << STD_endl;
      return false;
    }
//#endif

#endif

    return true;
  }

};

void alloc_TjToolsTest() {new TjToolsTest();} // create test instance
#endif



