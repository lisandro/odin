/***************************************************************************
                          tjstring.h  -  description
                             -------------------
    begin                : Mon Mar 10 2003
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/


#ifndef TJSTRING_H
#define TJSTRING_H


#include <tjutils/tjutils.h>
//#include <tjutils/tjstream.h>
#include <tjutils/tjtools.h>

/**
  * @addtogroup tjutils
  * @{
  */

#define _DEFAULT_DIGITS_ 5
#define _DEFAULT_LINEWIDTH_ 74

#ifdef USING_WIN32
#define SEPARATOR_STR  "\\"
#define SEPARATOR_CHAR '\\'
#else
#define SEPARATOR_STR  "/"
#define SEPARATOR_CHAR '/'
#endif


//////////////////////////////////////////////////////////////

// class for debugging string component
class StringComp {
 public:
  static const char* get_compName();
};

//////////////////////////////////////////////////////////////


/**
  * Enum to flag replacement policy:
  * - allOccurences:  All occurences will be replaced
  * - firstOccurence:  Only the first occurence is replaced
  */
enum whichOccurences {allOccurences,firstOccurence};


///////////////////////////////////////////////////////////////////////////////
//  String helper functions


/**
  *
  * Returns a string where occurences of 'searchstring' are replaced by 'replacement' in s.
  * The 'mode' parameter determines whether only the first or all occurences should be replaced.
  */
  STD_string replaceStr(const STD_string& s, const STD_string& searchstring, const STD_string& replacement, whichOccurences mode=allOccurences);

/**
  *
  * returns a block of characters within s that is enclosed by 'blockbegin' and
  * 'blockend'; the delimiters itself are not returned; If hierachical
  * is true the nesting is considered, e.g. extract("(a(b)c)", "(", ")", true)
  * will return "a(b)c" and not "a(b" ; beginpos is the position
  * to start searching;
  * If 'blockbegin' is a zero-length string, extraction will start at the beginning of the string;
  * If 'blockend' is a zero-length string, extraction will stop at the end of the string;
  */
  STD_string extract(const STD_string& s, const STD_string& blockbegin, const STD_string& blockend, bool hierachical=false, int beginpos=0);


/**
  *
  * Returns s whereby a block of characters that is enclosed by 'blockbegin' and
  * 'blockend' is removed; the flags rmbegin/rmend specify whether the delimiters
  * itself should be removed.
  * If 'rmall' is true, the last possible end delimiter will be used, otherwise the first.
  * If 'hierachical' is true the nesting is considered, e.g. extract("(",")",true) applied
  * to "(a(b)c)" will return "a(b)c" and not "a(b" 
  */
  STD_string rmblock(const STD_string& s, const STD_string& blockbegin, const STD_string& blockend,
                                     bool rmbegin=true, bool rmend=true, bool rmall=true, bool hierachical=false);

  
/**
  *
  * returns s, but with all lowercase characters transformed to uppercase
  */
  STD_string toupperstr(const STD_string& s);


/**
  *
  * returns s, but with all uppercase characters transformed to lowercase
  */
  STD_string tolowerstr(const STD_string& s);

/**
  * Enum to flag exponential printing of floating point numbers:
  * - autoExp:  Automatic
  * - alwaysExp:  Always use exponential format
  * - neverExp:  Never use exponential format
  */
enum expFormat {autoExp, alwaysExp, neverExp};


/**
  *
  * returns a string that contains the formatted floating point number 'f' with precision 'digits' and exponential usage 'eformat'
  */
  STD_string ftos (double f, unsigned int digits=_DEFAULT_DIGITS_, expFormat eformat=autoExp);


/**
  *
  * returns a string that contains the formatted integer 'i'.
  * If 'maxabs' is non-zero, the string will be formatted to
  * have the same number of (possibly zero-padded) digits as 'maxabs'.
  */
  STD_string itos (int i, unsigned int maxabs=0);

/**
  *
  * returns a string that contains the address of pointer 'p'
  */
  STD_string ptos (const void* p);

/**
  *
  * Returns n times s, e.g. n_times("3",3)  --->   "333"
  */
  STD_string n_times(const STD_string& s, unsigned int n);

/**
  *
  * returns a justification of s with the specified indention and linewidth
  */
  STD_string justificate(const STD_string& s, unsigned int indention=0,bool ignore_firstline=false, unsigned int linewidth=_DEFAULT_LINEWIDTH_);


/**
  *
  * Gives the position of the first non-speparator character in s.
  * Search starts at 'startpos'.
  * Blanks, tabs and newlines or 'custom_separator' if non-zero are considered as separators.
  */
  int textbegin(const STD_string& s, int startpos=0, const char custom_separator=0);

/**
  *
  * Gives the position of the first speparator character in s.
  * Search starts at 'startpos'.
  * Blanks, tabs and newlines or 'custom_separator' if non-zero are considered as separators.
  */
  int sepbegin(const STD_string& s, int startpos=0, const char custom_separator=0);

/**
  *
  * Remove all blanks, tabs and newlines from the string s
  */
  STD_string shrink(const STD_string& s);

/**
  *
  * searches for number of occurences of 'searchstring' in s, returns number of matches
  * otherwise 0
  */
  int noccur(const STD_string& s, const STD_string& searchstring);

/**
  *
  * Replaces all '0d0a' sequences by '0a' in the string
  */
  STD_string dos2unix(const STD_string& s);

/**
  *
  * Loads 'str' from the ASCII-file 'filename'.
  * Returns 0 on success, -1 otherwise.
  */
  int load(STD_string& str, const STD_string& filename);

/**
  *
  * Writes 'str' to the ASCII-file 'filename', the 'mode' determines
  * whether data will be appended/overwritten if the file already exists.
  * Returns 0 on success, -1 otherwise.
  */
  int write(const STD_string& str, const STD_string& filename, fopenMode mode=overwriteMode);



/** @}
  */
#endif
