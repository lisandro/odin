#include "tjthread.h"
#include "tjtools.h"
#include "tjtest.h"
#include "tjindex.h"
#include "tjthread_code.h"
#include "tjlog_code.h"

#ifndef NO_THREADS


#ifdef USING_WIN32
#include <windows.h>
#define USE_WINTHREADS
#else
#ifdef HAVE_PTHREAD
#include <pthread.h>
#ifdef HAVE_UNISTD_H
#include <unistd.h> // for sysconf
#endif
#define USE_PTHREADS
#endif
#endif


#endif



const char* ThreadComponent::get_compName() {return "Thread";}
LOGGROUNDWORK(ThreadComponent)

////////////////////////////////////////////////////////////////////

#ifdef USE_PTHREADS
const char* pthread_err(int code) {
  if(code==EAGAIN) return "not enough system resources to create a process for the new thread.";
  if(code==ESRCH) return "No thread could be found corresponding to that specified by |th|.";
  if(code==EINVAL) return "The |th| thread has been detached./the mutex has not been properly initialized.";
  if(code==EINVAL) return "Another thread is already waiting on termination of |th|.";
  if(code==EDEADLK) return "The |th| argument refers to the calling thread./the mutex is already locked by the calling thread.";
  if(code==EBUSY) return "the mutex could not be acquired because it was currently locked./some threads are currently waiting on |cond|";
  if(code==EPERM) return "the calling thread does not own the mutex.";
  if(code==ETIMEDOUT) return "the condition variable was not signaled until the timeout specified by |abstime|";
  if(code==EINTR) return "!pthread_cond_timedwait! was interrupted by a signal";
  if(code==ENOMEM) return "Out of memory";
  return "Unknown error";
}
#endif

////////////////////////////////////////////////////////////////////

Mutex::Mutex() : id(0) {
  // Do not use Log in here since SingletonHandler uses Mutex
#ifdef USE_PTHREADS
#ifdef MACOS // the Mac OS way of creating recursive threads
  pthread_mutexattr_t pma_recursive;
  int errcode=pthread_mutexattr_init(&pma_recursive);
  if(errcode) {
    STD_cerr << "ERROR: Mutex: " << pthread_err(errcode) << STD_endl;
    return;
  }
  errcode=pthread_mutexattr_settype(&pma_recursive, PTHREAD_MUTEX_RECURSIVE);
  if(errcode) {
    STD_cerr << "ERROR: Mutex: " << pthread_err(errcode) << STD_endl;
    return;
  }
  id=(void*)new pthread_mutex_t;
  errcode=pthread_mutex_init((pthread_mutex_t*)id, &pma_recursive);
  if(errcode) {
    STD_cerr << "ERROR: Mutex: " << pthread_err(errcode) << STD_endl;
    return;
  }
#else  // the Linux way of creating recursive threads
  pthread_mutex_t pm_recursive=PTHREAD_RECURSIVE_MUTEX_INITIALIZER_NP;
  id=(void*)new pthread_mutex_t(pm_recursive);
#endif
#endif
#ifdef USE_WINTHREADS
  CRITICAL_SECTION* cs=new CRITICAL_SECTION;
  InitializeCriticalSection(cs);
  id=(void*)cs;
#endif
}


Mutex::~Mutex() {
  // Do not use Log in here since SingletonHandler uses Mutex
  if(id) {
#ifdef USE_PTHREADS
    int errcode=pthread_mutex_destroy((pthread_mutex_t*)id);
    if(errcode) STD_cerr << "ERROR: ~Mutex: " << pthread_err(errcode) << STD_endl;
    delete (pthread_mutex_t*)id;
#endif
#ifdef USE_WINTHREADS
    DeleteCriticalSection((CRITICAL_SECTION*)id);
    delete (CRITICAL_SECTION*)id;
#endif
  }
}


void Mutex::lock() {
  // Do not use Log in here since SingletonHandler uses Mutex
  if(id) {
#ifdef USE_PTHREADS
    int errcode=pthread_mutex_lock((pthread_mutex_t*)id);
    if(errcode) STD_cerr << "ERROR: Mutex::lock: " << pthread_err(errcode) << STD_endl;
#endif
#ifdef USE_WINTHREADS
    EnterCriticalSection((CRITICAL_SECTION*)id);
#endif
  }
}

void Mutex::unlock() {
  // Do not use Log in here since SingletonHandler uses Mutex
  if(id) {
#ifdef USE_PTHREADS
    int errcode=pthread_mutex_unlock((pthread_mutex_t*)id);
    if(errcode) STD_cerr << "ERROR: Mutex::unlock: " << pthread_err(errcode) << STD_endl;
#endif
#ifdef USE_WINTHREADS
    LeaveCriticalSection((CRITICAL_SECTION*)id);
#endif
  }
}

////////////////////////////////////////////////////////////////////


Event::Event() : id(0), active(false) {
  Log<ThreadComponent> odinlog("Event","Event");
#ifdef USE_PTHREADS
  pthread_cond_t cond=PTHREAD_COND_INITIALIZER;
  id=(void*)new pthread_cond_t(cond);
#endif
#ifdef USE_WINTHREADS
  id=(void*)new HANDLE;
  *((HANDLE*)id)=CreateEvent(NULL,TRUE,FALSE,NULL);
#endif
}

Event::~Event() {
  Log<ThreadComponent> odinlog("Event","~Event");
  if(id) {
#ifdef USE_PTHREADS
    int errcode=pthread_cond_destroy((pthread_cond_t*)id);
    if(errcode) {
      ODINLOG(odinlog,errorLog) << pthread_err(errcode) << STD_endl;
    }
    delete (pthread_cond_t*)id;
#endif
#ifdef USE_WINTHREADS
    CloseHandle(*((HANDLE*)id));
    delete (HANDLE*)id;
#endif
  }
}


void Event::wait() {
  Log<ThreadComponent> odinlog("Event","wait");
#ifdef USE_PTHREADS
  MutexLock lock(mutex);
  while(!active) {
    int errcode=pthread_cond_wait((pthread_cond_t*)id, (pthread_mutex_t*)(mutex.id));
    if(errcode) {
      ODINLOG(odinlog,errorLog) << pthread_err(errcode) << STD_endl;
      break;
    }
  }
#endif
#ifdef USE_WINTHREADS
  WaitForSingleObject(*((HANDLE*)id), INFINITE);
#endif
}


void Event::signal() {
  Log<ThreadComponent> odinlog("Event","signal");
#ifdef USE_PTHREADS
  MutexLock lock(mutex);
  active=true;
  int errcode=pthread_cond_broadcast((pthread_cond_t*)id);
  if(errcode) {
    ODINLOG(odinlog,errorLog) << pthread_err(errcode) << STD_endl;
  }
#endif
#ifdef USE_WINTHREADS
  SetEvent(*((HANDLE*)id));
#endif
}

void Event::reset() {
  Log<ThreadComponent> odinlog("Event","reset");
#ifdef USE_PTHREADS
  MutexLock lock(mutex);
  active=false;
#endif
#ifdef USE_WINTHREADS
  ResetEvent(*((HANDLE*)id));
#endif
}



////////////////////////////////////////////////////////////////////



// pthread_t cannot be converted to an integer on all platforms (e.g. Darwin) so we need a separate map
#ifdef USE_PTHREADS
static STD_map<int,pthread_t> pthreadindexmap;
static Mutex pthreadindexmutex;

struct ThreadIndex  : public UniqueIndex<ThreadIndex> {

  // functions for UniqueIndex
  static const char* get_typename() {return "ThreadIndex";}
  static unsigned int get_max_instances() {return 0;}
};
#endif

//////////////////////////


#ifdef USE_WINTHREADS
DWORD WINAPI start_thread(LPVOID t) {
#else
void* start_thread(void* t) {
#endif
  ((Thread*)t)->run();
  return 0;
}

//////////////////////////


Thread::Thread() : id(0) {
#ifdef USE_PTHREADS
  index=new ThreadIndex;
#endif
}

Thread::~Thread() {
  clear_id();
#ifdef USE_PTHREADS
  delete index;
#endif
}


void Thread::clear_id() {
  if(id) {
#ifdef USE_PTHREADS
    delete ((pthread_t*)id);
#endif
#ifdef USE_WINTHREADS
    delete ((HANDLE*)id);
#endif
  }
  id=0;
}

bool Thread::start(unsigned int stack_size) {
  Log<ThreadComponent> odinlog("Thread","start");
  wait(); // will clear id

#ifdef USE_PTHREADS
  id=(void*)new pthread_t;
  pthread_attr_t pt_attr;
  int errcode=pthread_attr_init (&pt_attr);
  if(errcode) {
    ODINLOG(odinlog,errorLog) << "pthread_attr_init: " << pthread_err(errcode) << STD_endl;
    return false;
  }
  if(stack_size)  {
    errcode=pthread_attr_setstacksize (&pt_attr, stack_size);
    if(errcode) {
      ODINLOG(odinlog,errorLog) << "pthread_attr_setstacksize: " << pthread_err(errcode) << STD_endl;
      return false;
    }
  }
  errcode=pthread_create((pthread_t*)id, &pt_attr, &start_thread, this);
  if(errcode) {
    ODINLOG(odinlog,errorLog) << "pthread_create: " << pthread_err(errcode) << STD_endl;
#ifdef HAVE_SYSCONF
    ODINLOG(odinlog,errorLog) << "PTHREAD_THREADS_MAX=" << sysconf(_SC_THREAD_THREADS_MAX) << STD_endl;
#endif
    return false;
  }
  pthreadindexmutex.lock(); // lock because other threads might already be running
  pthreadindexmap[index->get_index()]=*((pthread_t*)id);
  pthreadindexmutex.unlock();
#else
#ifdef USE_WINTHREADS
  HANDLE* handle=new HANDLE;
  (*handle)=CreateThread(NULL,stack_size,&start_thread,this,0,NULL);
  id=(void*)handle;
  if(*handle==NULL) {
    ODINLOG(odinlog,errorLog) << "CreateThread: " << lasterr() << STD_endl;
    return false;
  }
#else
  this->run(); // no threads
#endif
#endif
  return true;
}


bool Thread::wait() {
  Log<ThreadComponent> odinlog("Thread","wait");
#ifdef USE_PTHREADS
  int errcode=0;
  if(id) {
    void* returnval;
    errcode=pthread_join(*((pthread_t*)id), &returnval);
    ODINLOG(odinlog,normalDebug) << "errcode=" << errcode << STD_endl;
  }
  clear_id();
  if(errcode) {
    ODINLOG(odinlog,errorLog) << pthread_err(errcode) << STD_endl;
    return false;
  }
#endif
#ifdef USE_WINTHREADS
  if(id) {
    DWORD errcode=WaitForSingleObject(*((HANDLE*)id),INFINITE);
    clear_id();
    if(errcode==WAIT_FAILED) {
      ODINLOG(odinlog,errorLog) << lasterr() << STD_endl;
      return false;
    }
  }
#endif
  return true;
}

int Thread::self() {
  Log<ThreadComponent> odinlog("Thread","self");
  int result=-1; // the master thread gets this id
#ifdef USE_PTHREADS
  pthread_t ptself=pthread_self();
  pthreadindexmutex.lock();
  for(STD_map<int,pthread_t>::const_iterator it=pthreadindexmap.begin(); it!=pthreadindexmap.end(); ++it) {
    if(pthread_equal(ptself, it->second)) result=it->first;
  }
  pthreadindexmutex.unlock();
#endif
#ifdef USE_WINTHREADS
  result=GetCurrentThreadId();
#endif
  return result;
}




////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////

#ifndef NO_UNIT_TEST

#include "tjvector.h"

#define VECSIZE 256
#define NITER 10000
#define NTHREADS 16

class TestThread : public Thread {

 public:

  void init(int* v, Mutex* m) {
    vec=v;
    mutex=m;
  }


 protected:

  void run() {
    for(int i=0; i<NITER; i++) {
      MutexLock lock1(*mutex);
      MutexLock lock2(*mutex); // testing recursive mutex
      for(int ivec=0; ivec<VECSIZE; ivec++) {
        vec[ivec]++;
      }
    }
  }


 private:
  int* vec;
  Mutex* mutex;

};


//////////////////////////////////////////////

class TestEventThread1 : public Thread {

 public:

  void init(double* r, Event* e, int d) {
    res=r;
    event=e;
    delay=d;
  }


 protected:

  void run() {
    sleep_ms(delay);
    (*res)=0.0;
    for(int i=0; i<NITER; i++) {
      (*res)+=sqrt(sqrt(double(i)));
    }
    event->signal();
  }


 private:
  double* res;
  Event* event;
  int delay;

};

//////////////////////////////////////////////

class TestEventThread2 : public Thread {

 public:

  void init(double* r, Event* e, int d) {
    res=r;
    event=e;
    delay=d;
  }


 protected:

  void run() {
    sleep_ms(delay);
    (*res)=123.4;
    event->wait();
  }


 private:
  double* res;
  Event* event;
  int delay;

};

//////////////////////////////////////////////

class ThreadedLoopTest : public ThreadedLoop<STD_string,STD_string,int> {

 private:
  bool kernel(const STD_string& in, STD_string& out, int&, unsigned int begin, unsigned int end) {
    out="";
    for(unsigned int i=begin; i<end; i++) {
      out+=in;
    }
    return true;
  }

};

//////////////////////////////////////////////


class ThreadTest : public UnitTest {

 public:
  ThreadTest() : UnitTest(ThreadComponent::get_compName()) {}

 private:

  bool check() const {
    Log<UnitTest> odinlog(this,"check");

    ///////////////////////////////////
    // Testing Thread / MutexLock

    TestThread thread[NTHREADS];

    Mutex mutex;

    int vec[VECSIZE];
    for(int ivec=0; ivec<VECSIZE; ivec++) vec[ivec]=0;

    int i,j;
    for(i=0; i<NTHREADS; i++) thread[i].init(vec,&mutex);
    for(i=0; i<NTHREADS; i++) thread[i].start();
    for(i=0; i<NTHREADS; i++) thread[i].wait();

    ivector iv(vec, VECSIZE);
    if( iv.minvalue()!=NITER*NTHREADS || iv.maxvalue()!=NITER*NTHREADS ) {
      ODINLOG(odinlog,errorLog) << "Mutex failed, iv=" << iv.printbody() << STD_endl;
      return false;
    }


    ///////////////////////////////////
    // Testing Event

    TestEventThread1 eventthread1[NTHREADS];
    Event event[NTHREADS];
    double res[NTHREADS];

    // Testing recycling of events / events with variable delay
    for(j=0; j<10; j++) {
      for(i=0; i<NTHREADS; i++) eventthread1[i].init(res+i,event+i,j); // with variable delays
      for(i=0; i<NTHREADS; i++) event[i].reset();
      for(i=0; i<NTHREADS; i++) eventthread1[i].start();
      sleep_ms(2);
      for(i=0; i<NTHREADS; i++) {

        event[i].wait();
        double expected=79994.7;
        if( fabs(res[i]-expected)>0.1 ) {
          ODINLOG(odinlog,errorLog) << "Event1 failed, res[" << i<< "]=" << res[i] << STD_endl;
          return false;
        }
      }
      for(i=0; i<NTHREADS; i++) eventthread1[i].wait();
    }


    // Testing multiple threads waiting on one signal
    TestEventThread2 eventthread2[NTHREADS];
    Event event2;
    for(j=0; j<10; j++) {
      for(i=0; i<NTHREADS; i++) eventthread2[i].init(res+i,&event2,j); // with variable delays
      for(i=0; i<NTHREADS; i++) eventthread2[i].start();

      ODINLOG(odinlog,normalDebug) << "[" << j << "] threads started" << STD_endl;

      sleep_ms(2);
      event2.signal();
      ODINLOG(odinlog,normalDebug) << "[" << j << "] signal sent" << STD_endl;

      for(i=0; i<NTHREADS; i++) {
        eventthread2[i].wait();
        double expected=123.4;
        if( res[i]!=expected ) {
          ODINLOG(odinlog,errorLog) << "Event2 failed, res[" << i<< "]=" << res[i] << STD_endl;
          return false;
        }
      }
    }




    ///////////////////////////////////
    // Testing Event


    unsigned int testsize=43536;

    for(unsigned int nthreads=1; nthreads<100; nthreads+=33) {

      ThreadedLoopTest tlt;
      tlt.init(nthreads, testsize);

      svector outvec;
      tlt.execute("T", outvec);

      STD_string result;
      for(unsigned int i=0; i<outvec.size(); i++) {
        result+=outvec[i];
      }
      if(result.length()!=testsize) {
        ODINLOG(odinlog,errorLog) << "result/testsize(" << nthreads << ")=" << result.length() << "/" << testsize << STD_endl;
        return false;
      }


      tlt.execute("N", outvec);

      result="";
      for(unsigned int i=0; i<outvec.size(); i++) {
        result+=outvec[i];
      }
      if(result.length()!=testsize) {
        ODINLOG(odinlog,errorLog) << "result/testsize(" << nthreads << ")=" << result.length() << "/" << testsize << STD_endl;
        return false;
      }
   }


    return true;
  }

};


void alloc_ThreadTest() {new ThreadTest();} // create test instance
#endif
