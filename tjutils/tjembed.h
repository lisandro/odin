/***************************************************************************
                          tjembed.h  -  description
                             -------------------
    begin                : Tue Mar 11 2003
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef TJEMBED_H
#define TJEMBED_H

#include <tjutils/tjutils.h>
#include <tjutils/tjstring.h> // for itos

/**
  * @addtogroup tjutils
  * @{
  */

template<class T,class E>
class Embed {

 public:
  Embed() {}

  ~Embed() {
    clear_instances();
  }

  void clear_instances() {
    for(typename STD_list<T*>::iterator it=instances.begin();it!=instances.end();++it) delete (*it);
    instances.clear();
  }

  T& set_embed_body (const E& embeddedBody) {
    T* embedptr=static_cast<T*>(this);
    T* newinst;
    if(embedptr) newinst=new T(*embedptr);
    else         newinst=new T();
    newinst->set_body(embeddedBody);
    newinst->set_label(STD_string(newinst->get_label())+itos(instances.size()));
    instances.push_back(newinst);
    return *newinst;
  }

  T& set_embed_body (E& embeddedBody) {
    T* embedptr=static_cast<T*>(this);
    T* newinst;
    if(embedptr) newinst=new T(*embedptr);
    else         newinst=new T();
    newinst->set_body(embeddedBody);
    newinst->set_label(STD_string(newinst->get_label())+itos(instances.size()));
    instances.push_back(newinst);
    return *newinst;
  }


  typedef typename STD_list<T*>::iterator institer;
  institer get_inst_begin() {return instances.begin();}
  institer get_inst_end() {return instances.end();}

  typedef typename STD_list<T*>::const_iterator constinstiter;
  constinstiter get_const_inst_begin() const {return instances.begin();}
  constinstiter get_const_inst_end() const {return instances.end();}

 private:
  STD_list<T*> instances;
};

/** @}
  */
#endif
