/***************************************************************************
                          tjlog.h  -  description
                             -------------------
    begin                : Fri Aug 2 2002
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef TJLOG_H
#define TJLOG_H

#ifndef TJUTILS_CONFIG_H
#define TJUTILS_CONFIG_H
#include <tjutils/config.h>
#endif

#include <tjutils/tjutils.h>
#include <tjutils/tjstream.h>
#include <tjutils/tjhandler.h>
#include <tjutils/tjlabel.h>
#include <tjutils/tjstatic.h>
#include <tjutils/tjstring.h>
#include <tjutils/tjcstd.h>

/**
  * @addtogroup tjutils
  * @{
  */

#define MAX_COMPONENT_SIZE 10
#define MAX_LOG_STRINGSIZE 25

/**
  * This enum is used to assign a priority to the log messages
  * If a given logging level is selected by the user (via GUI or command
  * line argument), all messages with an equal or lesser priority are
  * shown.
  */
enum logPriority {                      noLog=0, errorLog,   warningLog,   infoLog,   significantDebug,   normalDebug,   verboseDebug, numof_log_priorities, ignoreArgument};
static const char* logPriorityLabel[]={"noLog", "errorLog", "warningLog", "infoLog", "significantDebug", "normalDebug", "verboseDebug"};
AVOID_CC_WARNING(logPriorityLabel)

//////////////////////////////////////////////////////////////////////////////////////

/**
  * Message token which is passed to the trace function
  */
struct LogMessage {

/**
  * The priority level of the message
  */
  logPriority level;

/**
  * Component, i.e. subsystem, from which message originates
  */
  STD_string comp;

/**
  * Object (label) from which message originates
  */
  STD_string obj;

/**
  * Function (name) from which message originates
  */
  STD_string func;

/**
  * Message text
  */
  STD_string txt;


/**
  * Produce single string from message with at most 'maxwidth' characters
  * including the component label if 'include_comp' is 'true'.
  * If 'maxwidth' is zero, the length is unlimited.
  */
  STD_string str(unsigned int maxwidth=0, bool include_comp=true) const;
};

//////////////////////////////////////////////////////////////////////////////////////

/**
  * Trace function signature
  */
typedef void (*tracefunction)(const LogMessage& msg);


/**
  * Default trace function
  */
void default_tracefunction(const LogMessage& msg);

//////////////////////////////////////////////////////////////////////////////////////


// function pointer to get/set log levels in global map of logLevels
typedef logPriority (*log_component_fptr) (logPriority);

//////////////////////////////////////////////////////////////////////////////////////

/**
  * Base class for all Log-component types which manages global settings
  */
class LogBase : public virtual StaticHandler<LogBase> {

 public:

  LogBase() {}  // for LogBaseoc

  LogBase(const char* component, const char* object, const Labeled* labeledObject, const char* function)
    : compLabel(component), objLabel(object), namedObj(labeledObject), funcName(function) {}

/**
  * Set the log output level of component 'compname' to 'level'
  */
  static void set_log_level(const char* compname, logPriority level);

/**
  * Set uniform log output level for all components
  */
  static void set_uniform_log_level(logPriority level);

#ifndef NO_CMDLINE
/**
  * Set log levels from command line
  */
  static bool set_log_levels(int argc, char *argv[], bool trigger_error=true);
#endif

/**
  * Get command line usage for setting log levels
  */
  static STD_string get_usage();

  // Functions to used by Log-component types
  static bool register_component(const char*,log_component_fptr);
  static void unregister_component(const char*);

/**
  * Serialize log-level settings to string
  */
  static const char* get_levels();

/**
  * Set log levels from string
  */
  static void set_levels(const char* str);

/**
  * Set trace function
  */
  static void set_log_output_function(tracefunction func);


  // functions to initialize/delete static members by the StaticHandler template class
  static void init_static();
  static void destroy_static();



 protected:
  friend class LogOneLine;

  // Cache only plain pointers for efficiency
  const char* compLabel;
  const char* objLabel;    // Cache either the string
  const Labeled* namedObj; // or the object to retrieve the label from
  const char* funcName;


  void flush_oneline(const STD_string& txt, logPriority level);

#ifndef NO_CMDLINE
  static void parse_log_cmdline_options(int argc, char *argv[], const char* opt, logPriority base);
#endif


  // global (singleton) data
  struct Global : public Labeled {
    Global() : tracefunc(default_tracefunction), uniform_init_level(ignoreArgument) {}
    tracefunction tracefunc;
    STD_map<STD_string,log_component_fptr> components;
    STD_map<STD_string,logPriority> init_level;
    logPriority uniform_init_level;
  };

  static SingletonHandler<Global,true> global; // Thread-safe singleton for global data


 private:
  friend class DebugDialog;

};


//////////////////////////////////////////////////////////////////////////////////////


/**
  * Logging class to store logging information of the current scope (object, function, ...).
  * The template parameter specifies the component in which
  * logging is placed, e.g. Log<Seq> for tracing sequence code.
  */
template<class C>
class Log : public virtual LogBase {

 public:


/**
  * Constructs a logging stream for object 'objectLabel' and function 'functionName'.
  * Both strings will precede the actual message in the log. The third argument specifies
  * a uniform logging priority for the constructor and destructor.
  * This constructor will automatically generate a START tag in the log for the function
  * it is used in.
  */
  Log(const char* objectLabel, const char* functionName, logPriority level=verboseDebug);


/**
  * Constructs a logging stream for labeled object 'labeledObject' and function 'functionName'.
  * Both the label and the function name will precede the actual message in the log. The third argument specifies
  * a uniform logging priority for the constructor and destructor.
  * This constructor will automatically generate a START tag in the log for the function
  * it is used in.
  */
  Log(const Labeled* labeledObject, const char* functionName, logPriority level=verboseDebug);


/**
  * This destructor will automatically generate an END tag in the log for the function
  * its object is used in.
  */
  ~Log();


  static logPriority set_log_level(logPriority level); // to be called via log_component_fptr function pointer
  inline static logPriority get_log_level() {return logLevel;} // to be used by LogOneLine


 private:

  logPriority constrLevel;

  void register_comp();

  static logPriority logLevel;
  static bool registered; // call register_component only in 1st constructor

};

//////////////////////////////////////////////////////////////////////////////////////

// Temporary object to log one line via ostringstream
// This ingenious technique was presented by Petru Marginean in Dr.Dobb's under the title 'Logging in C++'
struct LogOneLine {

 public:
  LogOneLine(LogBase& logobj, logPriority level) : log(logobj), lev(level) {}

  ~LogOneLine() {log.flush_oneline(oss.str(),lev);}

  STD_ostream& get_stream() {return oss;}

 private:
  LogBase& log;
  logPriority lev;
  STD_ostringstream oss;
};

//////////////////////////////////////////////////////////////////////////////////////

// Min level which is included in release compilation
#define RELEASE_LOG_LEVEL infoLog


#ifdef ODIN_DEBUG

#define ODINLOG(logobj,level) \
if ((level) > (logobj).get_log_level()) ; \
else LogOneLine(logobj,level).get_stream()

#else

#define ODINLOG(logobj,level) \
if ((level) > RELEASE_LOG_LEVEL) ; \
else if ((level) > (logobj).get_log_level()) ; \
     else LogOneLine(logobj,level).get_stream()

#endif


/** @}
  */
#endif
