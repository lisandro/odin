/***************************************************************************
                          tjnumeric.h  -  description
                             -------------------
    begin                : Mon May 5 2003
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef TJNUMERIC_H
#define TJNUMERIC_H

#include <tjutils/tjutils.h>
#include <tjutils/tjvector.h>

/**
  * @addtogroup tjutils
  * @{
  */

/**
  *
  * Solve cubic equation    x^3 + a x^2 + b x + c = 0 , copied from the GSL-lib
  */
int solve_cubic (double a, double b, double c,
                      double *x0, double *x1, double *x2);

////////////////////////////////////////////////////////////////////////////////////////


/**
  * Calculate random number distributions, the generator is seeded once upon construction
  * using the system time.
  */
class RandomDist {

 public:
  RandomDist();
  ~RandomDist();

/**
  * Calculate Gaussian distribution with given standard deviation, uses GSL random number generator
  */
  double gaussian(double stdev) const;

/**
  * Calculate uniformly distributed random number in range [0,1), uses GSL random number generator
  */
  double uniform() const;

 private:
  void* rng;
};

////////////////////////////////////////////////////////////////////////////////////////

/**
  * Function which is used for optimization
  */
class MinimizationFunction {

 public:

/**
  * Returns the number of independent fitting parameters
  */
  virtual unsigned int numof_fitpars() const = 0;

/**
  * Multi-dimensional function to be minimized
  */
  virtual float evaluate(const fvector&) const = 0;
};


////////////////////////////////////////////////////////////////////////////////////////


/**
  * one-dimensional brute-force minimizer in given interval [low,upp].
  * Returns coordinates of minimum.
  */
fvector bruteforce_minimize1d(const MinimizationFunction& f, float low, float upp);


/** @}
  */
#endif
