 /***************************************************************************
                          tjvector.h  -  description
                             -------------------
    begin                : Thu Feb 22 2001
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/


#ifndef TJVECTOR_H
#define TJVECTOR_H

#include <tjutils/tjutils.h>
#include <tjutils/tjcomplex.h>
#include <tjutils/tjtools.h>
#include <tjutils/tjstring.h>
#include <tjutils/tjlog.h>

/**
  * @addtogroup tjutils
  * @{
  */



class VectorComp {
 public:
  static const char* get_compName();
};




/**
  *
  * This class represents a basic vector which is derived
  * from the Standard Template Library (STL) vector class.
  * it adds some useful functionality to the STL base class,
  * for example element-wise arithmetics and reading/writing.
  */
template<class T> class tjvector : public STD_vector<T> {

 public:

/**
  *
  * constructs a vector and allocates memory for n values
  * which are initialised to zero.
  */
  tjvector(unsigned int n=0);


/**
  *
  * constructs a vector of length n and gets n values from array
  * No bounds checking possible, so take care that the pointer array
  * has memory allocated which is in fact n values long.
  */
  tjvector(const T *array, unsigned int n);


/**
  *
  * constructs a copy from std::vector
  */
  tjvector(const STD_vector<T>& v);

/**
  *
  * Copy constructor
  */
  tjvector(const tjvector<T>& tv);

/**
  *
  * destructor
  */
  virtual ~tjvector();


/**
  *
  * assign value to all elements of vector
  */
  tjvector<T>& operator = (const T& value);

/**
  *
  * assignment from std::vector
  */
  tjvector<T>& operator = (const STD_vector<T>& vec);

/**
  *
  * copy assignment
  */
  tjvector<T>& operator = (const tjvector<T>& tv);


/**
  *
  * returns the element-wise sum of two tjvectors
  */
  tjvector<T> operator +  (const STD_vector<T>& w) const {
    tjvector<T> result(*this);
    for(unsigned int i=0; i<length(); i++) result[i]+=w[i];
    return result;
  }

/**
  *
  * returns the element-wise difference of two tjvectors
  */
  tjvector<T> operator -  (const STD_vector<T>& w) const {
    tjvector<T> result(*this);
    for(unsigned int i=0; i<length(); i++) result[i]-=w[i];
    return result;
  }

/**
  *
  * returns the element-wise product of two tjvectors
  */
  tjvector<T> operator *  (const STD_vector<T>& w) const {
    tjvector<T> result(*this);
    for(unsigned int i=0; i<length(); i++) result[i]*=w[i];
    return result;
  }

/**
  *
  * returns the element-wise division of two tjvectors
  */
  tjvector<T> operator /  (const STD_vector<T>& w) const {
    tjvector<T> result(*this);
    for(unsigned int i=0; i<length(); i++) result[i]/=w[i];
    return result;
  }

/**
  *
  * returns the negation of a tjvector
  */
  tjvector<T> operator -  () const {
    tjvector<T> result(*this);
    for(unsigned int i=0; i<length(); i++) result[i]=-result[i];
    return result;
  }

/**
  *
  * returns the element-wise sum of a tjvector with a scalar
  */
  friend tjvector<T> operator +  (const T& s, const STD_vector<T>& v) {
    tjvector<T> result(v);
    for(unsigned int i=0; i<v.size(); i++) result[i]+=s;
    return result;
  }

/**
  *
  * returns the element-wise sum of a tjvector with a scalar
  */
  tjvector<T> operator +  (const T& s) const {
    return s+(*this);
  }

/**
  *
  * returns the element-wise difference of a tjvector with a scalar
  */
  friend tjvector<T> operator -  (const T& s, const STD_vector<T>& v) {
    tjvector<T> result(v);
    for(unsigned int i=0; i<v.size(); i++) result[i]=s-result[i];
    return result;
  }

/**
  *
  * returns the element-wise difference of a tjvector with a scalar
  */
  tjvector<T> operator -  (const T& s) const {
    return -s+(*this);
  }


/**
  *
  * returns the element-wise product of a tjvector with a scalar
  */
  friend tjvector<T> operator *  (const T& s, const STD_vector<T>& v) {
    tjvector<T> result(v);
    for(unsigned int i=0; i<v.size(); i++) result[i]=result[i]*s;
    return result;
  }

/**
  *
  * returns the element-wise product of a tjvector with a scalar
  */
  tjvector<T> operator *  (const T& s) const {
    return s*(*this);
  }


/**
  *
  * returns the element-wise division of a tjvector with a scalar
  */
  friend tjvector<T> operator /  (const T& s, const STD_vector<T>& v) {
    tjvector<T> result(v);
    for(unsigned int i=0; i<v.size(); i++) result[i]=s/result[i];
    return result;
  }

/**
  *
  * returns the element-wise division of a tjvector with a scalar
  */
  tjvector<T> operator /  ( const T& s) const {
    return (T(1)/s)*(*this);
  }


/**
  * Equivalent to: this = this + v
  */
  tjvector<T>& operator += ( const STD_vector<T>& v) {
    (*this)=(*this)+v;
    return (*this);
  }

/**
  * Equivalent to: this = this + s
  */
  tjvector<T>& operator += ( const T& s) {
    (*this)=(*this)+s;
    return (*this);
  }

/**
  * Equivalent to: this = this - v
  */
  tjvector<T>& operator -= ( const STD_vector<T>& v) {
    (*this)=(*this)-v;
    return (*this);
  }

/**
  * Equivalent to: this = this - s
  */
  tjvector<T>& operator -= ( const T& s) {
    (*this)=(*this)-s;
    return (*this);
  }


/**
  * Equivalent to: this = this * v
  */
  tjvector<T>& operator *= ( const STD_vector<T>& v) {
    (*this)=(*this)*v;
    return (*this);
  }

/**
  * Equivalent to: this = this * s
  */
  tjvector<T>& operator *= ( const T& s) {
    (*this)=(*this)*s;
    return (*this);
  }


/**
  * Equivalent to: this = this / v
  */
  tjvector<T>& operator /= ( const STD_vector<T>& v) {
    (*this)=(*this)/v;
    return (*this);
  }

/**
  * Equivalent to: this = this / s
  */
  tjvector<T>& operator /= ( const T& s) {
    (*this)=(*this)/s;
    return (*this);
  }


/**
  *
  * Resizes the vector to 'newsize'.
  * If the 'newsize' is smaller than the actual size,
  * the starting values will be preserved in the smaller vector.
  * If the 'newsize' is larger than the actual size,
  * the vector will be zero padded.
  */
  virtual tjvector<T>& resize(unsigned int newsize);


/**
  *
  * returns the length
  */
  unsigned int length () const;


/**
  *
  * fill the array linearly from value min to value max, return number of fills
  */
  unsigned int fill_linear(const T& min,const T& max);


/**
  *
  * Returns the sum of all elements
  */
  T sum() const;


/**
  *
  * Returns the given range of elements starting at startindex and ending at endindex-1
  */
  tjvector<T> range(unsigned int startindex, unsigned int endindex) const;


/**
  *
  * Return the greatest value
  */
  T maxvalue() const;


/**
  *
  * Return the smallest value
  */
  T minvalue() const;


/**
  *
  * Maximum absolute value
  */
  T maxabs() const;


/**
  *
  * Returns an allocated C-style array of Ts with the values of this vector.
  * The array is deleted automatically at the next call of c_array() or if
  * this vector is destroyed.
  */
  const T* c_array() const;

/**
  *
  * Gets n values from 'array' (raw memory), no resizing is done
  * prior to assignment, this has to be manually beforehand.
  * No bounds checking possible, so take care that the pointer array
  * has memory allocated which is in fact n values long.
  */
  tjvector& set_c_array(const unsigned char* array, unsigned int n);


/**
  *
  * Normalize the vector, i.e. make its smallest/largest non-zero value become -1 or 1.
  * Returns the maximum absolute value which was used to achieve the normalization.
  */
  T normalize();


/**
  *
  * interpolate values to new size 'newsize'. Add extra shift 'subpixel_shift'
  * in units of pixels on the destination grid.
  */
  tjvector& interpolate(unsigned int newsize, float subpixel_shift=0.0);


/**
  *
  * Loads the values from disk, the file is expected
  * to contain the same data type as the array.
  * Returns 0 on success, -1 otherwise.
  */
  int load(const STD_string& fname);

/**
  *
  * Writes the first 'nelements' values to disk as raw data, the 'mode' determines
  * whether data will be appended/overwritten if the file already exists.
  * If 'nelements' is negative, the whole vector will we written.
  * Returns 0 on success, -1 otherwise.
  */
  int write(const STD_string& fname, fopenMode mode=overwriteMode, LONGEST_INT nelements=-1) const;


/**
  *
  * returns the elements of the vector as a formatted string
  */
  STD_string printbody() const;

/**
  *
  * prints v to the stream s
  */
  friend STD_ostream& operator << (STD_ostream& s,const tjvector<T>& v) {
    return s << "(" << v.size() << ")=" << v.printbody();
  }


 protected:

  // allocate aligned memory for type T (to avoid bus errors)
  static T* allocate_memory(unsigned int nelements) {return new T[nelements];}
  

 private:
  mutable T* c_array_cache;

};


/////////////////////////////////////////////////////////////////////////////
//    Aliases:

/**
  *  A vector of floating point numbers
  */
typedef tjvector<float>       fvector;

/**
  *  A vector of double precision floating point numbers
  */
typedef tjvector<double>     dvector;

/**
  *  A vector of integer numbers
  */
typedef tjvector<int>        ivector;


/**
  *  A vector of complex numbers
  */
typedef tjvector<STD_complex> cvector;


/**
  *  A vector of strings
  */
struct svector : public STD_vector<STD_string> {

  // some extra stuff to be compatible with STD_vector
  svector() {}
  svector(const STD_vector<STD_string>& sv) {svector::operator = (sv);}
  svector& operator = (const STD_vector<STD_string>& sv) {STD_vector<STD_string>::operator = (sv); return *this;}

  // interface functions for LDRarray
  unsigned char* c_array() const {return 0;}
  svector& set_c_array(const unsigned char*, unsigned int) {return *this;}
  static unsigned char* allocate_memory(unsigned int) {return 0;}

  // other useful functions, imitating a tjvector
  STD_string printbody() const;
};

/////////////////////////////////////////////////////////////////////////////

// Stuff to re-used in odindata
STD_complex* interpolate1D(const STD_complex* data,unsigned int oldsize,unsigned int newsize, float subpixel_shift);
float* interpolate1D(const float* olddata,unsigned int oldsize,unsigned int newsize, float subpixel_shift);
double* interpolate1D(const double* olddata,unsigned int oldsize,unsigned int newsize, float subpixel_shift);
int* interpolate1D(const int* olddata,unsigned int oldsize,unsigned int newsize, float subpixel_shift);

/////////////////////////////////////////////////////////////////////////////
//    Specialised helper functions:

/**
  *
  * Converts a list into a vector
  */
template<class T>
STD_vector<T> list2vector(const STD_list<T>& src) {
  STD_vector<T> result; result.resize(src.size());
  unsigned int i=0;
  for(typename STD_list<T>::const_iterator it=src.begin(); it!=src.end(); ++it) {
    result[i]=(*it);
    i++;
  }
  return result;
}

/**
  *
  * Returns the real portion of a complex array
  */
  fvector real(const cvector& cv);

/**
  *
  * Returns the imaginary portion of a complex array
  */
  fvector imag(const cvector& cv);

/**
  *
  * Returns the amplitude of a complex array
  */
  fvector amplitude(const cvector& cv);

/**
  *
  * Returns the phase of a complex array
  */
  fvector phase(const cvector& cv);

/**
  *
  * Returns a complex array with 'fv' in the real component
  */
  cvector real2complex(const fvector& fv);

/**
  *
  * Converts a vector of floats to a vector of doubles
  */
  dvector fvector2dvector(const fvector& fv);

/**
  *
  * Converts a vector of doubles to a vector of floats
  */
  fvector dvector2fvector(const dvector& dv);


/**
  *
  * Splits the string into tokens, which are substrings separated by blanks,
  * tabs and newlines or custom_separator (if custom_separator!=0) and returns them as a vector of strings.
  * Parts of the string enclosed by 'escape_begin' and 'escape_end' will not be decomposed into tokens.
  */
  svector tokens(const STD_string& tokenstring, char custom_separator=0, char escape_begin='"',  char escape_end='"');


/**
  *
  * Produces a string with linebreaks if width exceeds 'linewidth' from a vector of single strings.
  * A 'linewidth' of zero will produce no linebreaks.
  */
  STD_string tokenstring(const svector& tokens,unsigned int linewidth=_DEFAULT_LINEWIDTH_);


#ifndef NO_FILEHANDLING
/**
  *
  * Returns all entries in the given directory, if 'only_dirs' is 'true'
  * only subdirectories will be reported. Files starting with a dot
  * are discarded when setting 'discard_dotfiles' to 'true'.
  * The resulting vector is sorted alphanumerically according to the file names.
  */
  svector browse_dir(const STD_string& dirname,bool only_dirs=false, bool discard_dotfiles=false);
#endif



/** @}
  */
#endif
