#include "tjstream.h"
#include "tjstring.h"

#ifdef STREAM_REPLACEMENT

void tjstd_ostream::buff_float(float f) {append2buff(ftos(f));}
void tjstd_ostream::buff_int(int i)     {append2buff(itos(i));}


////////////////////////////////////////////////////////////////


tjstd_ofstream::tjstd_ofstream(const char* filename) : file_ptr(0) {
#ifndef NO_FILEHANDLING
  if(STD_string(filename)!="") {
    FILE* fp=ODIN_FOPEN(filename, modestring(overwriteMode));
    if(fp!=NULL) {
      file_ptr=(void*)fp;
    }
  }
#endif
}


void tjstd_ofstream::close() {
#ifndef NO_FILEHANDLING
  if(file_ptr) fclose((FILE*)file_ptr);
  file_ptr=0;
#endif
}


void tjstd_ofstream::append2buff(const STD_string& str) {
#ifndef NO_FILEHANDLING
  if(file_ptr) fwrite(str.c_str(), sizeof(char), str.length(), (FILE*)file_ptr);
#endif
}


#endif
