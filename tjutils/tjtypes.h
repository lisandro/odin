/***************************************************************************
                          tjtypes.h  -  description
                             -------------------
    begin                : Thu Jul 24 2001
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef TJTYPES_H
#define TJTYPES_H

#include <tjutils/tjutils.h>
#include <tjutils/tjcomplex.h>


/**
  * @addtogroup tjutils
  * @{
  */


// Get appropriate data types for signed/unsigned 8, 16 and 32 bit data on this platform

#if SIZEOF_CHAR == 1
typedef char            s8bit;
typedef unsigned char   u8bit;
#else
#error "sizeof(char)!=1"
#endif

#if SIZEOF_SHORT == 2
typedef short           s16bit;
typedef unsigned short  u16bit;
#else
#error "sizeof(short)!=2"
#endif

#if SIZEOF_INT == 4
typedef int             s32bit;
typedef unsigned int    u32bit;
#else
#if SIZEOF_LONG == 4
typedef long            s32bit;
typedef unsigned long   u32bit;
#else
#error "sizeof(int)!=4 and sizeof(long)!=4"
#endif
#endif


/**
  * Class to manage string conversions and properties of
  * simple (i.e. atomic) types. To be used in higher level
  * template classes to dispatch functionality according
  * to type.
  */
class TypeTraits {

 public:

  // Convert a type's value to a string
  static STD_string type2string(s8bit v)  {return itos(v);}
  static STD_string type2string(u8bit v)  {return itos(v);}
  static STD_string type2string(s16bit v) {return itos(v);}
  static STD_string type2string(u16bit v) {return itos(v);}
  static STD_string type2string(s32bit v) {return itos(v);}
  static STD_string type2string(u32bit v) {return itos(v);}
  static STD_string type2string(float v)  {return ftos(v);}
  static STD_string type2string(double v) {return ftos(float(v));}
  static STD_string type2string(const STD_complex& v) {return ctos(v);}
  static STD_string type2string(const STD_string& v)  {return v;}

  // Parses type's value from string
  static void string2type(const STD_string& s, s8bit& v)  {v=(s8bit)atoi(s.c_str());}
  static void string2type(const STD_string& s, u8bit& v)  {v=(u8bit)atoi(s.c_str());}
  static void string2type(const STD_string& s, s16bit& v) {v=(s16bit)atoi(s.c_str());}
  static void string2type(const STD_string& s, u16bit& v) {v=(u16bit)atoi(s.c_str());}
  static void string2type(const STD_string& s, s32bit& v) {v=(s32bit)atoi(s.c_str());}
  static void string2type(const STD_string& s, u32bit& v) {v=(u32bit)atoi(s.c_str());}
  static void string2type(const STD_string& s, float& v)  {v=(float)atof(s.c_str());}
  static void string2type(const STD_string& s, double& v) {v=atof(s.c_str());}
  static void string2type(const STD_string& s, STD_complex& v) {v=stoc(s);}
  static void string2type(const STD_string& s, STD_string& v)  {v=s;}

  // Type label (identifier) as string
  static const char* type2label(s8bit)  {return "s8bit";}
  static const char* type2label(u8bit)  {return "u8bit";}
  static const char* type2label(s16bit) {return "s16bit";}
  static const char* type2label(u16bit) {return "u16bit";}
  static const char* type2label(s32bit) {return "s32bit";}
  static const char* type2label(u32bit) {return "u32bit";}
  static const char* type2label(float)  {return "float";}
  static const char* type2label(double) {return "double";}
  static const char* type2label(const STD_complex&) {return "complex";}
  static const char* type2label(const STD_string&)  {return "string";}

/**
  * Returns size of data type
  */
  static unsigned int typesize(const STD_string& typelabel);


};


/** @}
  */
#endif
