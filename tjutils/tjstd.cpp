#include "tjutils.h"
#include "tjcstd.h"
#include "tjstream.h"
#include "tjtest.h"
#include "tjlog.h"


////////////////////////////////////////////////////////////////////////////

#ifdef STL_REPLACEMENT

STD_ostream & operator << (STD_ostream & s, tjstd_complex c) {
  char sign[2];
  if (c.im>=0.0) { sign[0]='+';sign[1]='\0';}
  else sign[0]='\0';
  return s << c.re << sign << c.im << "i";
}

#endif


////////////////////////////////////////////////////////////////////////////


#ifdef STRING_REPLACEMENT

tjstd_string::tjstd_string () : sdata(0), length_cache(0) {}

tjstd_string::tjstd_string (unsigned int n,const char c) : sdata(new char [n+1]), length_cache(n) {
  for(unsigned int i=0;i<n;i++) sdata[i]=c;
  sdata[n] = '\0';
}


tjstd_string& tjstd_string::operator = (const tjstd_string& str) {
  length_cache=str.length_cache;
  if(sdata) delete[] sdata;
  sdata=new char[length_cache+1];
  for(unsigned int i=0; i<length_cache; i++) sdata[i]=str.sdata[i];
  sdata[length_cache]='\0';
  return *this;
}


tjstd_string& tjstd_string::operator = (const char *charptr) {
  if(sdata) delete[] sdata;
  length_cache=0;
  if(charptr) while(charptr[length_cache]) length_cache++;
  sdata=new char[length_cache+1];
  for(unsigned int i=0; i<length_cache; i++) sdata[i]=charptr[i];
  sdata[length_cache]='\0';
  return *this;
}


tjstd_string::~tjstd_string () {
  if(sdata) delete[] sdata;
}


const char * tjstd_string::c_str() const {
  if(sdata) return (sdata);
  else return &nullchar;
}

char& tjstd_string::operator [] (unsigned int i) {
  unsigned int n=length();
  if( i && (i<0 || i>=n)) {
    return nullchar;
  } else {
    return sdata[i];
  }
}



const char& tjstd_string::operator [] (unsigned int i) const {
  unsigned int n=length();
  if( i<0 || i>=n) {
    return nullchar;
  } else {
    return sdata[i];
  }
}

tjstd_string operator + (const tjstd_string& s1, const tjstd_string& s2) {
  tjstd_string result;
  result.concat2strings(s1,s2);
  return result;
}

tjstd_string operator + (const tjstd_string& s1, const char* s2) {
  tjstd_string result;
  result.concat2strings(s1,tjstd_string(s2));
  return result;
}

tjstd_string operator + (const char* s1, const tjstd_string& s2) {
  tjstd_string result;
  result.concat2strings(tjstd_string(s1),s2);
  return result;
}

tjstd_string& tjstd_string::operator += (const tjstd_string& s2) {
  concat2strings(*this,s2);
  return *this;
}

bool operator == (const tjstd_string& s, const tjstd_string& t) {return tjstd_string::compare2strings(s.sdata, t.sdata);}
bool operator == (const tjstd_string& s, const char* t) {return tjstd_string::compare2strings(s.sdata, t);}
bool operator == (const char* s, const tjstd_string& t) {return tjstd_string::compare2strings(s, t.sdata);}


bool operator != (const tjstd_string& s, const tjstd_string& t) {return !tjstd_string::compare2strings(s.sdata, t.sdata);}
bool operator != (const tjstd_string& s, const char* t) {return !tjstd_string::compare2strings(s.sdata, t);}
bool operator != (const char* s, const tjstd_string& t) {return !tjstd_string::compare2strings(s, t.sdata);}

bool operator < (const tjstd_string& s, const tjstd_string& t) {
  unsigned int p=0;
  unsigned int q=0;

  while( (p!=s.length()) && (q!=t.length()) && (toupper(s[p]) == toupper(t[q])) ) {
    ++p;
    ++q;
  }
  if(p==s.length()) return q!=t.length();
  if(q==t.length()) return false;
  return ( toupper(s[p]) < toupper(t[q]) );
}


bool operator > (const tjstd_string& s, const tjstd_string& t) {
  if(s==t) return false;
  return !(s<t);
}


STD_ostream& operator << (STD_ostream& s,const tjstd_string& t) {
  if(t.sdata) s << t.sdata;
  return s;
}


#define STREAM2PARSE_CHUNK_SIZE 100
STD_istream& operator >> (STD_istream& s, tjstd_string& t) {
  char c=0;

  // remove trailing whitespaces
  while(s.get(c)) {
    if(!isspace(c)) {
      s.putback(c);
      break;
    }
  }

  tjstd_string buffer(STREAM2PARSE_CHUNK_SIZE,'\0');
  t="";

  int ibuff=0;
  while(s.get(c) && !isspace(c)) {
    buffer[(unsigned int)ibuff]=c;
    ibuff++;
    if(ibuff==STREAM2PARSE_CHUNK_SIZE) {
      t+=buffer;
      buffer=tjstd_string(STREAM2PARSE_CHUNK_SIZE,'\0');
      ibuff=0;
    }
  }
  t+=buffer;

  // remove following whitespaces
  while(s.get(c)) {
    if(!isspace(c)) {
      s.putback(c);
      break;
    }
  }

  return s;
}


tjstd_string tjstd_string::substr(unsigned int beginpos,unsigned int sublength) const {
  unsigned int end=beginpos+sublength;

  if(beginpos > end) return "";
  if(beginpos<0) beginpos=0;
  if( end <0) end=0;
  unsigned int len=length();
  if(beginpos>=len) beginpos=len;
  if(end  >=len) end=len;

  unsigned int resultlength=end-beginpos;
  tjstd_string result(end-beginpos);
  for(unsigned int i=0; i<resultlength; i++) result[i]=(*this)[beginpos+i];

  return result;
}

void tjstd_string::erase(unsigned int begin,unsigned int end) {
  (*this)=substr(0,begin)+substr(end,length());
}


unsigned int tjstd_string::find(const tjstd_string& searchstring, int startpos) const {
  unsigned int searchlength=searchstring.length();
  if(!searchlength) return npos;
  unsigned int thislength=length();
  if(searchlength>(thislength-startpos)) return npos;

  const char* thisstr=(const char*)c_str();
  const char* searchstr=(const char*)searchstring.c_str();

  unsigned int nshifts=(thislength-startpos)-searchlength+1;
  for(unsigned int ishift=0; ishift<nshifts; ishift++) {
    unsigned int i=0;
    while(searchstr[i]==thisstr[ishift+startpos+i] && i<searchlength) {
      i++;
    }
    if(i==searchlength) {
      return (ishift+startpos);
    }
  }
  return npos;
}



void tjstd_string::concat2strings(const tjstd_string& srcstring1, const tjstd_string& srcstring2) {
    unsigned int length1=srcstring1.length();
    unsigned int length2=srcstring2.length();

    length_cache=length1+length2;

    char* news=new char[length_cache+1];

    unsigned int i;
    for(i=0; i<length1; i++) news[i]=srcstring1.sdata[i];
    for(i=0; i<length2; i++) news[i+length1]=srcstring2.sdata[i];

    news[length_cache]='\0';

    if(sdata) delete[] sdata;
    sdata=news;
  }


bool tjstd_string::compare2strings(const char* str1, const char* str2) {
  const char *s1,*s2;

  if(str1) s1=str1;
  else s1=&nullchar;

  if(str2) s2=str2;
  else s2=&nullchar;

  bool result=true;
  bool cont=true;
  unsigned int i=0;

  while(cont && result) {
    if(s1[i]!=s2[i]) result=false;
    cont = s1[i] && s2[i];
    i++;
  }
  return result;
}

unsigned int  tjstd_string::npos=(unsigned int)(-1);

char tjstd_string::nullchar='\0';


#endif // ifdef STRING_REPLACEMENT


/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////



#ifndef NO_UNIT_TEST


#define MAX_LISTITEMS 10
#define MAX_LISTCHECKS 1000

class StlTest : public UnitTest {

 public:
  StlTest() : UnitTest("STL") {}

 private:

  void listtest_dump_list(const STD_list<int>& ilist) const {
    Log<UnitTest> odinlog(this,"listtest_dump_list");
    ODINLOG(odinlog,errorLog) << "list=" << STD_endl;
    for(STD_list<int>::const_iterator it=ilist.begin(); it!=ilist.end(); ++it) {
      ODINLOG(odinlog,errorLog) << (*it) << STD_endl;
    }
  }

  ///////////////////////////////////////////////////////////////////////////

  void listtest_init_list(STD_list<int>& ilist) const {
    int i;
    ilist.clear();

    ///////////// preparing list of random number between 0 and 9

    for(i=0; i<MAX_LISTITEMS; i++) {
      int randval=int(double(i)*9.435656)%10; // default
  #ifdef HAVE_SRAND
      randval=int(10.0*double(rand())/double(RAND_MAX));
  #endif
      if(randval<0) randval=0;
      if(randval>9) randval=9;
      if(i%3) ilist.push_back(randval);
      else ilist.push_front(randval);
    }
  }

  ///////////////////////////////////////////////////////////////////////////

  void listtest_digit_count(STD_list<int>& ilist, int* digit_count) const {
    STD_list<int>::const_iterator it;
    int i;

    for(i=0; i<10; i++) digit_count[i]=0;
    for(it=ilist.begin(); it!=ilist.end(); ++it) {
      digit_count[*it]++;
    }
  }

  ///////////////////////////////////////////////////////////////////////////

  bool listtest_check_list() const {
    Log<UnitTest> odinlog(this,"listtest_check_list");
    STD_list<int>::const_iterator it;
    int i;

    STD_list<int> ilist;
    int digit_count[10];

    listtest_init_list(ilist);
    listtest_digit_count(ilist,digit_count);


    ///////////// checking sort()

    ilist.sort();

    // checking linear ordering with ++ operator
    int lastdigit=-1;
    for(it=ilist.begin(); it!=ilist.end(); ++it) {
      if(lastdigit>=0) {
        if(lastdigit>(*it)) {
          ODINLOG(odinlog,errorLog) << "(sort) with ++: inconsistent ordering" << STD_endl;
          listtest_dump_list(ilist);
          return false;
        }
      }
      lastdigit=(*it);
    }

    // checking linear ordering with -- operator
    lastdigit=-1;
    it=ilist.end();
    while(it!=ilist.begin()) {
      --it;
      if(lastdigit>=0) {
        if(lastdigit<(*it)) {
          ODINLOG(odinlog,errorLog) << "(sort) with --: inconsistent ordering" << STD_endl;
          listtest_dump_list(ilist);
          return false;
        }
      }
      lastdigit=(*it);
    }    

    // checking digit_count
    int digit_count_test[10];
    for(i=0; i<10; i++) digit_count_test[i]=0;

    for(it=ilist.begin(); it!=ilist.end(); ++it) {
      digit_count_test[*it]++;
    }

    for(i=0; i<10; i++) {
      if(digit_count_test[i]!=digit_count[i]) {
        ODINLOG(odinlog,errorLog) << "(sort) inconsistent digit_count" << STD_endl;
        listtest_dump_list(ilist);
        return false;
      }
    }


    ///////////// checking unique()

    ilist.unique();

    unsigned int n_different_digits=0;
    for(i=0; i<10; i++) {
      if(digit_count[i]) n_different_digits++;
    }
    ODINLOG(odinlog,normalDebug) << "n_different_digits=" << n_different_digits << STD_endl;

    if(ilist.size()!=n_different_digits) {
      ODINLOG(odinlog,errorLog) << "(unique) inconsistent list after unique" << STD_endl;
      listtest_dump_list(ilist);
      return false;
    }


    ///////////// checking remove()

    listtest_init_list(ilist);

    ilist.remove(3);

    listtest_digit_count(ilist,digit_count);

    if(digit_count[3]) {
      ODINLOG(odinlog,errorLog) << "(remove) elements still there after remove" << STD_endl;
      listtest_dump_list(ilist);
      return false;
    }



    ///////////// checking clear()

    ilist.clear();
    if(ilist.size()) {
      ODINLOG(odinlog,errorLog) << "(clear) elements still there after clear" << STD_endl;
      listtest_dump_list(ilist);
      return false;
    }


    /////////// checking STD_find

    ilist.clear();
    ilist.push_back(1);
    ilist.push_back(3);
    ilist.push_back(4);

    if(STD_find(ilist.begin(), ilist.end(), 3) == ilist.end()) {
      ODINLOG(odinlog,errorLog) << "STD_find does not find existing element 3" << STD_endl;
      listtest_dump_list(ilist);
      return false;
    }

    if(STD_find(ilist.begin(), ilist.end(), 2) != ilist.end()) {
      ODINLOG(odinlog,errorLog) << "STD_find finds non-existing element 2" << STD_endl;
      listtest_dump_list(ilist);
      return false;
    }

    return true;
  }

  ///////////////////////////////////////////////////////////////////////////

  bool check_map() const {
    Log<UnitTest> odinlog(this,"check_map");
    STD_map<STD_string, int> imap;

    imap["12"]=12;
    imap["34"]=34;
    imap["56"]=56;

    int expected, returned;

    expected=34;
    returned=imap["34"];
    if(expected!=returned) {
      ODINLOG(odinlog,errorLog) << "testing [] operator: expected/returned=" << expected << "/" << returned << STD_endl;    
      return false;
    }

    STD_map<STD_string, int>::iterator it=imap.find("56");
    expected=56;
    returned=it->second;
    if(expected!=returned) {
      ODINLOG(odinlog,errorLog) << "testing find: expected/returned=" << expected << "/" << returned << STD_endl;    
      return false;
    }

    imap.erase(imap.find("34"));
    expected=2;
    returned=imap.size();
    if(imap.size()!=2) {
      ODINLOG(odinlog,errorLog) << "testing erase: expected/returned=" << expected << "/" << returned << STD_endl;    
      return false;
    }

    return true;
  }


  bool check() const {
    Log<UnitTest> odinlog(this,"check");
    for(unsigned int i=0; i<MAX_LISTCHECKS; i++) {
#ifdef HAVE_SRAND
      srand(time(NULL)+i);
#endif
      if(!listtest_check_list()) {
        ODINLOG(odinlog,errorLog) << "check_list() failed" << STD_endl;
        return false;
      }
    }

    if(!check_map()) {
      ODINLOG(odinlog,errorLog) << "check_map() failed" << STD_endl;
      return false;
    }

    return true;
  }

};

void alloc_StlTest() {new StlTest();} // create test instance

#endif


