/***************************************************************************
                          tjthread.h  -  description
                             -------------------
    begin                : Mon Dec 12 2005
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef TJTHREAD_H
#define TJTHREAD_H


#include <tjutils/tjutils.h>

/**
  * @addtogroup tjutils
  * @{
  */



////////////////////////////////////////////////////////////////////

// for debugging
class ThreadComponent {
 public:
  static const char* get_compName();
};


////////////////////////////////////////////////////////////////////


/**
  * Lock for mutually exclusive (mutex) access in threads. The mutex is recursive.
  */
class Mutex {

 public:

/**
  * Constructs unlocked mutex object.
  */
  Mutex();

  ~Mutex();

/**
  * Locks the mutex.
  */
  void lock();

/**
  * Unlocks the mutex.
  */
  void unlock();


 private:
  friend class Event;

  // disable copying
  Mutex(const Mutex&) {}
  Mutex& operator = (const Mutex&) {return *this;}

  void* id;

};

////////////////////////////////////////////////////////////////////

/**
  * Lock object using a mutex.
  */
class MutexLock {

 public:

/**
  * Constructs a lock using 'mutex'. The mutex is locked for the lifetime of the object.
  */
  MutexLock(Mutex& mutex) : m(mutex) {m.lock();}

  ~MutexLock() {m.unlock();}

 private:

  // disable copying
  MutexLock(const MutexLock& ml) : m(ml.m) {} // bogus
  MutexLock& operator = (const MutexLock&) {return *this;}

  Mutex& m;
};


////////////////////////////////////////////////////////////////////


/**
  * Manual-reset event to synchronize threads.
  */
class Event {

 public:

/**
  * Constructs an event object.
  */
  Event();

  ~Event();

/**
  * Waits until any other thread calls signal().
  * Returns immediately if this has already happended.
  */
  void wait();

/**
  * Signal event to all waiting threads.
  */
  void signal();

/**
  * Manually reset event.
  */
  void reset();


 private:

  // disable copying
  Event(const Event&) {}
  Event& operator = (const Event&) {return *this;}

  void* id;

  // Extra stuff for pthread
  Mutex mutex;
  bool active;

};


////////////////////////////////////////////////////////////////////


class ThreadIndex; // forward declaration

/**
  * Base class for threads. Derive from this and implement
  * the run() function to use it.
  */
class Thread {

 public:

/**
  * Constructs thread object with the given label.
  */
  Thread();

  virtual ~Thread();

/**
  * Starts the thread.
  * 'stack_size' is used as the threads stack size if not zero (in which case the
  * systems default stack size is used).
  * Returns 'true' only on sucess.
  */
  bool start(unsigned int stack_size=0);

/**
  * Waits for the thread to finish. Returns 'true' only on sucess.
  */
  bool wait();


/**
  * Implement to run threads execution path.
  */
  virtual void run() = 0;


/**
  * Returns thread-unique ID of the current thread.
  */
  static int self();


 private:

  // disable copying
  Thread(const Thread&) {}
  Thread& operator = (const Thread&) {return *this;}


  void clear_id();

  void* id;

  ThreadIndex* index;  // give each thread a unique index

};


////////////////////////////////////////////////////////////////////


/**
  * Class to parallelize loops.
  */
template<typename In, typename Out, typename Local>
class ThreadedLoop {

 public:

/**
  * Default constructor.
  */
  ThreadedLoop() : mainbegin(0), mainend(0) {}

  virtual ~ThreadedLoop() {destroy();}

/**
  * Initialize to use 'numof_threads' executing 'loopsize' jobs in parallel.
  * Starts the worker threads.
  */
  bool init(unsigned int numof_threads, unsigned int loopsize);

/**
  * Stops the worker threads.
  */
  void destroy();

/**
  * Execute loop using input 'in' and storing result from each thread in 'outvec'.
  */
  bool execute(const In& in, STD_vector<Out>& outvec);

 private:

/**
  * Overload this function to do the work of one thread using input 'in',
  * generating output 'out' with thread-local storage 'local' for the the loop inidices (begin <= i < end).
  */
  virtual bool kernel(const In& in, Out& out, Local& local, unsigned int begin, unsigned int end) = 0;


  class WorkThread : public Thread {

    friend class ThreadedLoop<In,Out,Local>;

    WorkThread(ThreadedLoop<In,Out,Local>* tl) : tloop(tl) {}

    void run();

    ThreadedLoop<In,Out,Local>* tloop;
    unsigned int begin;
    unsigned int end;

    Event process;
    Event finished;
    volatile bool status;

    Out* out_cache;
    
    Local local;

  };


  unsigned int mainbegin;
  unsigned int mainend;
  Local mainlocal;
  friend class WorkThread;
  STD_vector<WorkThread*> threads;
  const In* in_cache;
  volatile bool cont;

};




/** @}
  */
#endif
