/***************************************************************************
                          tjlist.h  -  description
                             -------------------
    begin                : Mon Aug 5 2002
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef TJLIST_H
#define TJLIST_H

#include <tjutils/tjutils.h>

/**
  * @addtogroup tjutils
  * @{
  */

// for debugging List
class ListComponent {
 public:
  static const char* get_compName();
};

//////////////////////////////////////////////////////////////

class ListItemBase {};


class ListBase  {

 public:
  virtual ~ListBase() {}
  virtual void objlist_remove(ListItemBase* item) = 0;
};



//////////////////////////////////////////////////////////////

template<class I>
class ListItem : public ListItemBase {

 public:

  ListItem() {}
  ~ListItem();

  // empty copy and assignment operator to avoid copying of 'objhandlers' member
  ListItem(const ListItem&) {}
  ListItem& operator = (const ListItem&) {return *this;}


  unsigned int numof_references() const {return objhandlers.size();}

  const ListItemBase& append_objhandler(ListBase& objhandler) const;
  const ListItemBase& remove_objhandler(ListBase& objhandler) const;


 private:
  mutable STD_list<ListBase*> objhandlers;

};



/////////////////////////////////////////////

/**
  * This template class handles a list of references to other objects.
  * In contrast to the STL list, it stores only pointers to the members
  * of the list instead of storing copies. That means, if a member of the
  * list is modified, the property of the whole list changes. This list
  * is well suited to build blocks of objects that will be modified after
  * they have been inserted into the list, e.g. a list of parameters.
  *
  * If you want to use this list, i.e. create an instantiation for a specific
  * data type T, you must first decide whether to use const objects, i.e. which
  * will not changed via the list, or non-const objects that can be modified
  * via the list. In the former case, create a list by instatiating
  *
  * List<T,const T*,const T&>
  *
  * otherwise
  *
  * List<T,T*,T&>
  *
  * Classes which are to be inserted into the list must be derived
  * from the base class
  *
  * ListItem<T>
  *
  * This construct ensures that whenever an item of the list is deleted,
  * it deregisters itself from the list.
  */
template<class I,class P,class R> class List : public ListBase {


 public:

/**
  * Constructs an empty List
  */
  List();

/**
  * Destructor
  */
  ~List();

/**
  * Assignment operator that will result in a list that holds references to all objects for which
  * 'l' contains a reference
  */
  List& operator = (const List& l);


/**
  * Removes all items from the list
  */
  List& clear();

/**
  * Appends a reference to 'item'
  */
  List& append(R item);

/**
  * Removes all references to 'item'
  */
  List& remove(R item);

/**
  * Returns the number of items in the list
  */
  unsigned int size() const {return objlist.size();}


/**
  * Iterator for mutable lists
  */
  typedef typename STD_list<P>::iterator iter;

/**
  * Iterator for const lists
  */
  typedef typename STD_list<P>::const_iterator constiter;

/**
  * returns an iterator that points to the head of the list (for mutable lists)
  */
  iter get_begin() {return objlist.begin();}

/**
  * returns an iterator that points to the end of the list (for mutable lists)
  */
  iter get_end() {return objlist.end();}

/**
  * returns an iterator that points to the head of the list (for const lists)
  */
  constiter get_const_begin() const {return objlist.begin();}

/**
  * returns an iterator that points to the end of the list (for const lists)
  */
  constiter get_const_end() const {return objlist.end();}


 private:

  void objlist_remove(ListItemBase* item);

  void link_item(P ptr);
  void unlink_item(P ptr);


  STD_list<P> objlist;

};


/** @}
  */
#endif
