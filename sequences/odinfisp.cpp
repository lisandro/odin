#include <odinseq/seqall.h>


class METHOD_CLASS : public SeqMethod {

 private:
  SeqPulsar exc;
  SeqPulsarReph exc_reph;

  SeqPulsar exc_prep;
  SeqGradTrapez exc_prep_reph;

  SeqDelay prepdelay;
  SeqDelay postdelay;
  SeqDelay relaxdelay;

  SeqDelay grechdummy;

  SeqGradEcho grech;

  SeqGradConstPulse crusher;

  SeqObjList kernel;
  SeqObjList dummykernel;

  SeqObjList readout;
  SeqObjList shot;
  SeqObjList scan;

  SeqObjLoop reploop;
  SeqObjLoop sliceloop;
  SeqObjLoop peloop;
  SeqObjLoop phaseloop;
  SeqObjLoop dummyloop;


  LDRint  DummyCycles;
  LDRenum PhaseEncoding;

 public:

  ////////////////////////////////////////////////////

  METHOD_CLASS(const STD_string& label) : SeqMethod(label) {
    set_description("Single-shot TrueFISP (balanced SSFP) sequence. See for example Scheffler et.al. Eur Radiol (2003) 13:2409-2418 ");

  }

  ////////////////////////////////////////////////////

  void method_pars_init() {

    commonPars->set_EchoTime(0.0,noedit);
    commonPars->set_FlipAngle(60.0);

    DummyCycles=3;
    DummyCycles.set_description("Number of dummy cycles before actual acquisition");

    PhaseEncoding.add_item("Linear",linearEncoding);
    PhaseEncoding.add_item("CenterOut",centerOutEncoding);
    PhaseEncoding=linearEncoding;
    PhaseEncoding.set_description("Phase encoding order");

    append_parameter(DummyCycles,"DummyCycles");
    append_parameter(PhaseEncoding,"PhaseEncoding");
  }



  void method_seq_init(){

    double gamma=systemInfo->get_gamma();

    ///////////////// Pulses: /////////////////////

    float slicethick=geometryInfo->get_sliceThickness();
    float spatres=slicethick/4.0;

    // Excitation Pulse
    exc=SeqPulsarSinc("exc",slicethick,false,2.0,commonPars->get_FlipAngle(),spatres);
    dvector freqlist=gamma * exc.get_strength() / (2.0*PII) *  geometryInfo->get_sliceOffsetVector();

    exc.set_freqlist(freqlist);
    exc.set_pulse_type(excitation);

    // +/- flipangles for FISP
    dvector phaselist(2);
    phaselist[0]=0.0;
    phaselist[1]=180.0;
    exc.set_phaselist(phaselist);

    // rephasing lobe for excitation pulse
    exc_reph=SeqPulsarReph("exc_reph",exc);

    // initial preparation pulse
    exc_prep=exc;
    exc_prep.set_label("exc_prep");
    exc_prep.set_flipangle(0.5*commonPars->get_FlipAngle());
    exc_prep.set_phase(180.0);

    exc_prep_reph=SeqGradTrapez("exc_prep_reph",2.0*exc_reph.get_gradintegral().sum(),0.8*systemInfo->get_max_grad(),sliceDirection);


    ////////////////// Geometry: /////////////////////////////////

    // calculate the resolution in the read Channel and set the number of phase encoding
    // steps so that we will obtain a uniform resolution in read and phase Channel:
    float resolution=secureDivision(geometryInfo->get_FOV(readDirection),commonPars->get_MatrixSize(readDirection));
    commonPars->set_MatrixSize(phaseDirection,int(secureDivision(geometryInfo->get_FOV(phaseDirection),resolution)+0.5),noedit);



    //////////////// Balanced Gradient Echo Module (2D mode): //////////////

    grech=SeqGradEcho("grech", exc, commonPars->get_AcqSweepWidth(),
                commonPars->get_MatrixSize(readDirection), geometryInfo->get_FOV(readDirection),
                commonPars->get_MatrixSize(phaseDirection), geometryInfo->get_FOV(phaseDirection),
                encodingScheme(int(PhaseEncoding)), interleavedSegmented, 2,
                commonPars->get_ReductionFactor(),DEFAULT_ACL_BANDS,true,commonPars->get_PartialFourier());

    grech.set_phaselist(phaselist); // +/- phase switching for SSFP


    //////////////// crusher Gradient: //////////////////////////////

    double crusher_strength=0.5*systemInfo->get_max_grad();
    double crusher_dur=4.0;
    crusher=SeqGradConstPulse("crusher",readDirection,crusher_strength,crusher_dur);


    //////////////// Loops ////////////////////

    reploop=SeqObjLoop("reploop");
    sliceloop=SeqObjLoop("sliceloop");
    peloop=SeqObjLoop("peloop");
    phaseloop=SeqObjLoop("phaseloop");


    //////////////// Timing Delays ////////////////////

    prepdelay=SeqDelay("prepdelay");
    postdelay=SeqDelay("postdelay");
    relaxdelay=SeqDelay("relaxdelay");


    grechdummy=SeqDelay("grechdummy",grech.get_duration()-exc.get_duration()-exc_prep_reph.get_duration());

    //////////////// building the sequence //////////////////////////////

    kernel=SeqObjList("kernel");
    kernel= grech + postdelay;

    dummykernel=SeqObjList("dummykernel");
    dummykernel = exc + exc_prep_reph + grechdummy + postdelay;


    readout=SeqObjList("readout");
    readout = dummyloop (
                phaseloop(
                  dummykernel
                )[exc.get_phaselist_vector()]
              )[DummyCycles]
              +
              peloop(
                phaseloop(
                  kernel
                )[exc.get_phaselist_vector()][grech.get_phaselist_vector()][grech.get_pe_reorder_vector()]
              )[grech.get_pe_vector()];


    shot=SeqObjList("shot");
    shot = exc_prep  + exc_prep_reph + prepdelay + readout + crusher;

    scan=SeqObjList("scan");
    scan += reploop(
              sliceloop(
                shot + relaxdelay
              )[exc][exc_prep]
            )[commonPars->get_NumOfRepetitions()];


    set_sequence( scan );
  }



  //////////////////////////////////////////////////

  void method_rels(){
    Log<Seq> odinlog(this,"method_rels");

    // TrueFISP timings, use minimum TE

    double TEprep=exc_prep.get_duration()-exc_prep.get_magnetic_center()+
                   exc_prep_reph.get_duration()+
                   exc.get_magnetic_center();

    double TEpre=grech.get_acquisition_center()-exc.get_magnetic_center();

    double TEpost=grech.get_duration()-grech.get_acquisition_center()+
                   exc.get_magnetic_center();

    double TEmax=maxof3(TEprep,TEpre,TEpost);

    ODINLOG(odinlog,significantDebug) << "TEprep/pre/post/max=" << TEprep << "/" << TEpre << "/" << TEpost << "/" << TEmax << STD_endl;

    prepdelay=TEmax-TEprep;
    postdelay=TEmax-TEpost;

    commonPars->set_EchoTime(TEmax,noedit);


    // calculate relaxdelay to get the desired repetition time
    float scandur=shot.get_duration()*float(geometryInfo->get_nSlices());
    if(scandur>commonPars->get_RepetitionTime()) commonPars->set_RepetitionTime(scandur);
    relaxdelay=(commonPars->get_RepetitionTime()-scandur)/float(geometryInfo->get_nSlices());

  }


  //////////////////////////////////////////////////

  void method_pars_set(){
  }

};



//////////////////////////////////////////////////

// entry point for the sequence module
ODINMETHOD_ENTRY_POINT


