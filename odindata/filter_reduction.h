/***************************************************************************
                          filter_reduction.h  -  description
                             -------------------
    begin                : Fri Feb 26 2010
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef FILTER_REDUCTION_H
#define FILTER_REDUCTION_H


#include <odindata/filter_step.h>

enum                      reductionOp { minip=0, maxip,   meanp,   sump };
static const char* reductionOpLabel[]={"minip", "maxip", "proj",  "sum"};

template<int Op>
class FilterReduction : public FilterStep
{
  LDRenum dir;

  STD_string label() const {return reductionOpLabel[Op];}

  STD_string description() const {
    STD_string opstr;
    if(Op==minip) opstr="minimum intensity ";
    if(Op==maxip) opstr="maximum intensity ";
    if(Op==meanp)  opstr="mean ";
    if(Op==sump)   opstr="sum ";
    return "Perform "+opstr+"projection over given direction";
  }

  void init() {
    for(int i=0; i<n_dataDim; i++) dir.add_item(dataDimLabel[i]);
    dir.add_item("none"); // default to throw error when no dim is given
    dir.set_actual(dir.n_items()-1);
    dir.set_cmdline_option("dir").set_description("direction");
    append_arg(dir,"dir");
  }

  bool process(Data<float,4>& data, Protocol& prot) const {
    Log<Filter> odinlog(c_label(),"process");

    if(dir=="none"){
      ODINLOG(odinlog,errorLog) << "no valid dimension given" << STD_endl;
      return false;
    }

    TinyVector<int,4> inshape=data.shape();

    TinyVector<int,4> outshape=data.shape();
    outshape(dir)=1;

    Data<float,4> outdata(outshape);

    for(unsigned int i=0; i<outdata.size(); i++) {
      TinyVector<int,4> index=outdata.create_index(i);

      TinyVector<int,4> lowerBounds=index;
      TinyVector<int,4> upperBounds=index;
      upperBounds(dir)=inshape(dir)-1;
    
      if(Op==minip) outdata(index)=min (data(RectDomain<4>(lowerBounds, upperBounds)));
      if(Op==maxip) outdata(index)=max (data(RectDomain<4>(lowerBounds, upperBounds)));
      if(Op==meanp) outdata(index)=mean(data(RectDomain<4>(lowerBounds, upperBounds)));
      if(Op==sump)  outdata(index)=sum (data(RectDomain<4>(lowerBounds, upperBounds)));
    }

    data.reference(outdata);

    // Adjusting protocol, center of slicepack and FOV remains the same
    if(dir==timeDim) {
      prot.seqpars.set_NumOfRepetitions(1);
    } else {
      if(dir==sliceDim) prot.geometry.set_nSlices(1);
      prot.seqpars.set_MatrixSize(direction(3-dir),1);
    }
    return true;
  }

  FilterStep*  allocate() const {return new FilterReduction<Op>();}
};

#endif
