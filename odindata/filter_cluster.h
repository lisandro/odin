/***************************************************************************
                          filter_cluster.h  -  description
                             -------------------
    begin                : Tue Jan 14 2014
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef FILTER_CLUSTER_H
#define FILTER_CLUSTER_H

#include <odindata/filter_step.h>

class FilterCluster: public FilterStep {
  STD_string label() const {return "cluster";}
  STD_string description() const {return "Create clusters of non-zero adjacent/next-neighbours voxels, sorted by size";}
  bool process(Data<float,4>& data, Protocol& prot) const;
  FilterStep*  allocate() const {return new FilterCluster();}
  void init() {}
};

#endif
