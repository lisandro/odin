/***************************************************************************
                          filter_tile.h  -  description
                             -------------------
    begin                : Wed Nov 5 2008
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef FILTER_TILE_H
#define FILTER_TILE_H

#include <odindata/filter_step.h>

class FilterTile : public FilterStep {

  LDRint cols;

  STD_string label() const {return "tile";}
  STD_string description() const {return "Combine slices into a square 2D image";}
  bool process(Data<float,4>& data, Protocol& prot) const;
  FilterStep*  allocate() const {return new FilterTile();}
  void init();
};

#endif
