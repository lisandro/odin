/***************************************************************************
                          filter_mask.h  -  description
                             -------------------
    begin                : Wed Nov 5 2008
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef FILTER_MASK_H
#define FILTER_MASK_H

#include <odindata/filter_step.h>

class FilterGenMask : public FilterStep {

  LDRfloat min;
  LDRfloat max;


  STD_string label() const {return "genmask";}
  STD_string description() const {return "Create mask including all voxels with value in given range";}
  bool process(Data<float,4>& data, Protocol& prot) const;
  FilterStep*  allocate() const {return new FilterGenMask();}
  void init();
};


///////////////////////////////////////////////////////////////////////////

class FilterAutoMask : public FilterStep {

//  LDRfileName dump_histogram_fname;

  STD_string label() const {return "automask";}
  STD_string description() const {return "Create mask using automatic histogram-based threshold";}
  bool process(Data<float,4>& data, Protocol& prot) const;
  FilterStep*  allocate() const {return new FilterAutoMask();}
  void init();
};


///////////////////////////////////////////////////////////////////////////

class FilterQuantilMask : public FilterStep {

  LDRfloat fraction;

  STD_string label() const {return "quantilmask";}
  STD_string description() const {return "Create mask including all voxels above the given fractional threshold";}
  bool process(Data<float,4>& data, Protocol& prot) const;
  FilterStep*  allocate() const {return new FilterQuantilMask();}
  void init();
};


///////////////////////////////////////////////////////////////////////////

class FilterSphereMask : public FilterStep {

  LDRstring pos;
  LDRfloat radius;

  STD_string label() const {return "spheremask";}
  STD_string description() const {return "Create spherical mask";}
  bool process(Data<float,4>& data, Protocol& prot) const;
  FilterStep*  allocate() const {return new FilterSphereMask();}
  void init();
};


///////////////////////////////////////////////////////////////////////////

class FilterUseMask : public FilterStep {

  LDRfileName fname;

  STD_string label() const {return "usemask";}
  STD_string description() const {return "Create 1D dataset including all values within mask from file";}
  bool process(Data<float,4>& data, Protocol& prot) const;
  FilterStep*  allocate() const {return new FilterUseMask();}
  void init();
};

#endif
