/***************************************************************************
                          integration.h  -  description
                             -------------------
    begin                : Fri Apr 6 2001
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef INTEGRATION_H
#define INTEGRATION_H

#include <odindata/data.h>


/**
  * @addtogroup odindata
  * @{
  */

/**
  * Base class of all function classes which should be integrated.
  * To use this class, derive from it and overload the virtual
  * function 'evaluate'.
  */
class Integrand {

 public:

/**
  * Returns the function value at position 'x'.
  */
  virtual double evaluate(double x) const = 0;

/**
  * Returns the integral from 'xmin' to 'xmax'.
  * Integration is performed with 'max_subintervals' at which the function
  * will be calculated and the specified relative 'error_limit'.
  */
  double get_integral(double xmin, double xmax, unsigned int max_subintervals=1000, double error_limit=1e-7) const;


 protected:
   Integrand() {}
   virtual ~Integrand() {}
};

////////////////////////////////////////////////////////////

class GslData4Integr; // forward declaration

/**
  * Class which is used for integration of functions.
  */
class FunctionIntegral {

 public:

/**
  * Prepare an integration of function 'func'
  * with 'max_subintervals' at which the function
  * will be calculated and the specified relative 'error_limit'.
  */
  FunctionIntegral(const Integrand& func, unsigned int max_subintervals=1000, double error_limit=1e-7);

/**
  * Destructor
  */
  ~FunctionIntegral();

/**
  * Returns the integral from 'xmin' to 'xmax'.
  */
  double get_integral(double xmin, double xmax) const;
  

 private:
  static double integrand(double x, void *params);
  
  const Integrand& f;
  unsigned int n_intervals;
  double errlimit;
  GslData4Integr* gsldata;

};

/** @}
  */


#endif

