//
// C++ Implementation: filter_nan
//
// Description: 
//
//
// Author:  <Enrico Reimer>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "filter_nan.h"

void FilterNaN::init(){
  replace=0;
  replace.set_description("Replacement value");
  append_arg(replace,"replace");
}

bool FilterNaN::process(Data<float,4>& data, Protocol& prot)const{
  //replace all elements by replace where element does NOT equal itself (which is only true for NaN)
  data=where(Array<float,4>(data) == Array<float,4>(data), Array<float,4>(data), (float)replace );
  return true;
}
