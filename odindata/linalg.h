/***************************************************************************
                          linalg.h  -  description
                             -------------------
    begin                : Sun May 22 2005
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef LINALG_H
#define LINALG_H

#include<odindata/complexdata.h>


/**
  * @addtogroup odindata
  * @{
  */

/**
  * Solves the linear system A x = b and returns x.
  * All singular values less than sv_truncation * max_singular_value
  * will be set to zero.
  * The algorithm uses the singular value decomposition from LAPACK (or from GSL if LAPACK is not available).
  *
  */
Data<float,1> solve_linear(const Data<float,2>& A, const Data<float,1>& b, float sv_truncation=0.0);


/**
  * Solves the complex linear system A x = b and returns x.
  * All singular values less than sv_truncation * max_singular_value
  * will be set to zero.
  * The algorithm uses the singular value decomposition from LAPACK (or from GSL if LAPACK is not available).
  *
  */
ComplexData<1> solve_linear(const ComplexData<2>& A, const ComplexData<1>& b, float sv_truncation=0.0);


/**
  * Computes the eigenvalues of symmetric matrix A using LAPACK (or with GSL if LAPACK is not available).
  *
  */
Data<float,1> eigenvalues(const Data<float,2>& A);

/** @}
  */

#endif

