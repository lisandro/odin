/***************************************************************************
                          filter_inv.h  -  description
                             -------------------
    begin                : Thu Mar 12 2015
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef FILTER_INV_H
#define FILTER_INV_H

#include <odindata/filter_step.h>

class FilterInvert : public FilterStep {

  STD_string label() const {return "inv";}
  STD_string description() const {return "Invert image values, i.e. lowest to highest and vice versa";}
  bool process(Data<float,4>& data, Protocol& prot) const;
  FilterStep*  allocate() const {return new FilterInvert();}
  void init() {}
};

#endif
