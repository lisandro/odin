/***************************************************************************
                          filter_scale.h  -  description
                             -------------------
    begin                : Thu Jan 31 2008
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef FILTER_CLIP_H
#define FILTER_CLIP_H

#include <odindata/filter_step.h>

class FilterMin : public FilterStep {

  LDRfloat thresh;

  STD_string label() const {return "min";}
  STD_string description() const {return "Clip all values below mininum value";}
  bool process(Data<float,4>& data, Protocol& prot) const;
  FilterStep*  allocate() const {return new FilterMin();}
  void init();
};

////////////////////////////////////////////////////////////////////////////

class FilterMax : public FilterStep {

  LDRfloat thresh;

  STD_string label() const {return "max";}
  STD_string description() const {return "Clip all values above maximum value";}
  bool process(Data<float,4>& data, Protocol& prot) const;
  FilterStep*  allocate() const {return new FilterMax();}
  void init();
};

////////////////////////////////////////////////////////////////////////////

class FilterType : public FilterStep {
protected:
  LDRstring type;
  float getThresh(bool upper)const;
  void init();
};

class FilterTypeMin : public FilterType {
  STD_string label() const {return "typemin";}
  STD_string description() const {return "Clip all values below mininum of a specific datatype";}
  bool process(Data<float,4>& data, Protocol& prot) const;
  FilterStep*  allocate() const {return new FilterTypeMin();}
};

class FilterTypeMax : public FilterType {
  STD_string label() const {return "typemax";}
  STD_string description() const {return "Clip all values above maximum of a specific datatype";}
  bool process(Data<float,4>& data, Protocol& prot) const;
  FilterStep*  allocate() const {return new FilterTypeMax();}
};

#endif
