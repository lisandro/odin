/***************************************************************************
                          filter_merge.h  -  description
                             -------------------
    begin                : Mon May 25 2009
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef FILTER_MERGE_H
#define FILTER_MERGE_H

#include <odindata/filter_step.h>

class FilterMerge: public FilterStep {
  STD_string label() const {return "merge";}
  STD_string description() const {return "Merge datasets into a single dataset by expanding the time dimension";}
  bool process(FileIO::ProtocolDataMap& pdmap) const;
  FilterStep*  allocate() const {return new FilterMerge();}
  void init() {}
};

#endif
