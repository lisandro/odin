#include "filter.h"
#include "filter_step.h"

struct FilterChainData {
  FilterFactory factory;
  STD_list<FilterStep*> filters;
};

/////////////////////////////////////////////////////////

FilterChain::FilterChain() : filterchaindata(new FilterChainData) {}


FilterChain::~FilterChain() {
  delete filterchaindata;
}


bool FilterChain::create(const svector& args) {
  Log<Filter> odinlog("FilterChain","create");

  filterchaindata->filters.clear();

  ODINLOG(odinlog,normalDebug) << "args=" << args.printbody() << STD_endl;

  unsigned int nargs=args.size();
  for(unsigned int i=0; i<nargs; i++){
    if(args[i].length() && args[i][0]=='-') {
      STD_string filterlabel=args[i].substr(1,args[i].length()-1);
      FilterStep* filter=filterchaindata->factory.create(filterlabel);
      if(filter) {
        if(i<(nargs-1) && filter->numof_args()) {
          STD_string argstr=args[i+1];
          ODINLOG(odinlog,normalDebug) << "argstr=" << argstr << STD_endl;
          if(argstr.length()) filter->set_args(argstr);
          i++;
        }
        filterchaindata->filters.push_back(filter);
      }
    }
  }
  return true;
}


FilterChain::FilterChain(const STD_string& argstr) : filterchaindata(new FilterChainData) {
init(argstr);
}


FilterChain::FilterChain(int argc,char *argv[]) : filterchaindata(new FilterChainData) {
  Log<Filter> odinlog("FilterChain","FilterChain");

  int nargs=argc-1;
  if(nargs<=0) return;

  svector args; args.resize(nargs);
  for(int i=0; i<nargs; i++) args[i]=argv[i+1];

  create(args);
}


bool FilterChain::apply(FileIO::ProtocolDataMap& pdmap) const {
  Log<Filter> odinlog("FilterChain","apply");
  for(STD_list<FilterStep*>::const_iterator fit=filterchaindata->filters.begin();fit!=filterchaindata->filters.end();fit++){
    ODINLOG(odinlog,normalDebug) << "Applying " << (*fit)->label() << " to the whole Dataset" << STD_endl;
    FilterStep *step=*fit;
    if(!step->process(pdmap)) return false;
  }
  return true;
}


bool FilterChain::apply(Protocol& prot, Data<float,4>& data) const {
  Log<Filter> odinlog("FilterChain","apply");

  for(STD_list<FilterStep*>::const_iterator fit=filterchaindata->filters.begin();fit!=filterchaindata->filters.end();fit++){
    ODINLOG(odinlog,normalDebug) << "Applying " << (*fit)->label() << STD_endl;
    if(!(*fit)->process(data, prot)) return false;
  }

  return true;
}


STD_string FilterChain::get_cmdline_usage(const STD_string& lineprefix) const {
  return filterchaindata->factory.get_cmdline_usage(lineprefix);
}

