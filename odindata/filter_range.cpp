#include "filter_range.h"

bool str2range(const STD_string& str, Range& range, int srcsize) {
  Log<Filter> odinlog("","str2range");
  if(str=="") return false;
  int increment=1;
  svector tokscol=tokens(str,':');
  int colsize=tokscol.size();
  if(colsize<1 || colsize>2) return false;
  if(colsize==2) increment=atoi(tokscol[1].c_str());
  STD_string fromto=tokscol[0];
  bool result=false;
  if(fromto=="all") {
    range=Range::all();
    result=true;
  } else {
    svector toks=tokens(fromto,'-');
    if(toks.size()==2) {range=Range(atoi(toks[0].c_str()),atoi(toks[1].c_str()),increment); result=true;}
    if(toks.size()==1) {
      int singleval=atoi(toks[0].c_str());
      int low=singleval;
      int upp=singleval;
      if(fromto.length()) {
        if(fromto[0]=='-') low=0;
        if(fromto[fromto.length()-1]=='-') upp=srcsize-1;
      }
      range=Range(low,upp,increment);
      result=true;
    }
  }

  if(!result) {
    ODINLOG(odinlog,errorLog) << "Error parsing range string >" << str << "<" << STD_endl;
  }

  if(result) {
    if(range.first()>range.last()) result=false;
    if(range.first()<0 || range.first()>=srcsize) result=false;
    if(range.last()<0  || range.last()>=srcsize)  result=false;
    if(!result) {
      ODINLOG(odinlog,errorLog) << "selected " << range << " out of valid range (0," << (srcsize-1) << ")" << STD_endl;
    }
  }
  return result;
}
