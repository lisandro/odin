#include <odindata/step.h>


template <class T>
T* Step<T>::clone() const {
  T* result=allocate();
  result->init();
  result->args.copy_ldr_vals(args);
  return result;
}


template <class T>
void Step<T>::set_args(const STD_string& argstr) {
  Log<OdinData> odinlog(c_label(),"set_args");

  unsigned int nargs=args.numof_pars();
  if(!nargs) return; // Just a flag

  svector toks(tokens(argstr,',','(',')'));
  for(unsigned int i=0; i<toks.size(); i++) {
    STD_string oneargstr=replaceStr(toks[i], "\"", ""); // remove parenthesis at this point
    ODINLOG(odinlog,normalDebug) << "oneargstr[" << i << "]=" << oneargstr << STD_endl;
    if(i<nargs) {
      args[i].parsevalstring(oneargstr);
    } else {
      ODINLOG(odinlog,warningLog) << "More arguments provided than parameters in step - argument: " << toks[i] << STD_endl;
    }
  }
}


template <class T>
STD_string Step<T>::args_description() const {
  int nargs=args.numof_pars();
  STD_string result;
  for(int i=0; i<nargs; i++) {
    result+=args[i].get_description();

    STD_string unit=args[i].get_unit();
    if(unit!="") result+=" ["+unit+"]";

    svector alt=args[i].get_alternatives();
    if(alt.size()) result+=" ("+tokenstring(alt,0)+")";
    if(i<(nargs-1)) result+=",";
  }
  return result;
}


template <class T>
void Step<T>::append_opts(LDRblock& parblock) {
  parblock.merge(args);
}


template <class T>
void Step<T>::append_arg(LDRbase& arg, const STD_string& arglabel) {
  arg.set_label(label()+"_"+arglabel);
  args.append(arg);
}



///////////////////////////////////////////////////////////////////////////////////


template <class T>
StepFactory<T>::StepFactory(LDRblock* parblock) {
  STD_list<T*> steplist;
  T::create_templates(steplist);

  for(typename STD_list<T*>::const_iterator it=steplist.begin(); it!=steplist.end(); ++it) {
    T* p=(*it);
    p->init();
    if(parblock) p->append_opts(*parblock);
    templates[p->label()]=p;
  }
}


template <class T>
StepFactory<T>::~StepFactory() {
  for(typename StepMap::iterator it=templates.begin(); it!=templates.end(); ++it) delete it->second;
  for(typename STD_list<T*>::iterator it=garbage.begin(); it!=garbage.end(); ++it) delete (*it);
}


template <class T>
T* StepFactory<T>::create(const STD_string& label) const {
  Log<OdinData> odinlog("StepFactory","create");
  T* result=0;
  typename StepMap::const_iterator it=templates.find(label);
  if(it!=templates.end()) {
    result=it->second->clone();
  } else {
    ODINLOG(odinlog,errorLog) << "Step with label >" << label << "< not found" << STD_endl;
    return 0;
  }
  garbage.push_back(result);
  return result;
}


// Helper class for managing step entries in the manual
struct StepDoc {
  STD_string label;
  STD_string description;
  STD_string in;
  STD_string out;
  STD_string options;
};


template <class T>
STD_string StepFactory<T>::manual() const {
  STD_string result;

  STD_map<STD_string, STD_list<StepDoc> > strmap;

  for(typename StepMap::const_iterator it=templates.begin(); it!=templates.end(); ++it) {

    const char* classtype=typeid(*(it->second)).name();
    while( (*classtype) >='0' && (*classtype) <='9' ) classtype++; // strip off leading numbers (GCC)

    STD_string classname(classtype);
    classname=rmblock(classname,"IL",""); // strip off template parameters (GCC)

    StepDoc doc;
    doc.label=it->first;
    doc.description=it->second->description();
    T::interface_description(it->second, doc.in, doc.out);

    LDRblock parblock;
    it->second->append_opts(parblock);
    int nargs=parblock.numof_pars();
    for(int i=0; i<nargs; i++) {
      doc.options+="    Option "+itos(i+1)+": "+parblock[i].get_description();
      STD_string cmdlineopt=parblock[i].get_cmdline_option();
      if(cmdlineopt!="") doc.options+=", command-line option: -"+cmdlineopt;
      doc.options+="\n";
    }
    strmap[classname].push_back(doc);
  }


  result+="/**\n";
  result+="  * @addtogroup "+T::manual_group()+"\n";
  result+="  * @{\n";
  result+="  */\n\n";

  for(STD_map<STD_string, STD_list<StepDoc> >::const_iterator it=strmap.begin(); it!=strmap.end(); ++it) {

    result+="/**\n";
    result+="  * \\class "+it->first+"\n";

    STD_string labels;
    STD_string descr; // assume the same for all
    for(STD_list<StepDoc>::const_iterator docit=it->second.begin(); docit!=it->second.end(); ++docit) {
      if(labels!="") labels+=", ";
      labels+="\\b "+docit->label;
      descr=docit->description;
    }
    result+="  * \\brief "+labels+": "+descr+"\n\n";

    for(STD_list<StepDoc>::const_iterator docit=it->second.begin(); docit!=it->second.end(); ++docit) {
      result+="  * Functor label: \\b "+docit->label+"\n\n";
      if(docit->in!="" || docit->out!="") {
        result+="  * Interface of \\b "+docit->label+":\n";
        result+="  * - input: "+docit->in+"\n";
        result+="  * - output: "+docit->out+"\n";
      }
      result+="  *\n";
      result+="  *\n";
      if(docit->options!="") {
        result+="  * Options of \\b "+docit->label+":\n";
        result+="  * \\verbatim \n";
        result+=docit->options;
        result+="    \\endverbatim \n\n";
      }
    }
    result+="  */\n\n";
  }

  result+="/** @}\n";
  result+="  */\n";

  return result;
}


template <class T>
STD_string StepFactory<T>::get_cmdline_usage(const STD_string& lineprefix)const{
  STD_string ret;
  for(typename StepMap::const_iterator it=templates.begin(); it!=templates.end(); ++it) {
    const T* st=it->second;
    ret += lineprefix+"-"+st->label();
    STD_string argsdescr=st->args_description();
    if(argsdescr!="") ret += " <"+argsdescr+">";
    ret += " : "+st->description()+"\n";
  }
  return ret;
}
