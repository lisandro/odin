/***************************************************************************
                          filter_detrend.h  -  description
                             -------------------
    begin                : Sun May 17 2009
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef FILTER_DETREND_H
#define FILTER_DETREND_H

#include <odindata/filter_step.h>

class FilterDeTrend : public FilterStep {

  LDRint nlow;
  LDRbool zeromean;

  STD_string label() const {return "detrend";}
  STD_string description() const {return "Remove slow drift over time";}
  bool process(Data<float,4>& data, Protocol& prot) const;
  FilterStep*  allocate() const {return new FilterDeTrend();}
  void init();
};

#endif
