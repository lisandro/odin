/***************************************************************************
                          filter_edit.h  -  description
                             -------------------
    begin                : Thu Jan 30 2014
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef FILTER_EDIT_H
#define FILTER_EDIT_H

#include <odindata/filter_step.h>
#include <odindata/filter_range.h> // for str2range

class FilterEdit : public FilterStep {

  LDRstring pos;
  LDRfloat val;


  STD_string label() const {return "edit";}
  STD_string description() const {return "Edit single voxel values";}
  bool process(Data<float,4>& data, Protocol& prot) const;
  FilterStep*  allocate() const {return new FilterEdit();}
  void init();
};

#endif
