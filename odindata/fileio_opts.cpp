#include "fileio_opts.h"
#include "fileio.h"



FileReadOpts::FileReadOpts() {

  format.add_item(AUTODETECTSTR);
  svector formats(FileIO::autoformats());
  for(unsigned int i=0; i<formats.size(); i++) format.add_item(formats[i]);
  format.set_actual(0);
  format.set_cmdline_option("rf").set_description("Read format, use it to override file extension");
  append_member(format,"format");

  ldr="";
  ldr.set_cmdline_option("ldr").set_description("If multiple LDR (labeled data record) arrays are present, select this");
  append_member(ldr,"ldr");


  cplx.add_item("none");
  cplx.add_item("abs");
  cplx.add_item("pha");
  cplx.add_item("real");
  cplx.add_item("imag");
  cplx.set_actual(0);
  cplx.set_cmdline_option("cplx").set_description("Treat data as complex and extract the given component");
  append_member(cplx,"cplx");

  skip=false;
  skip.set_cmdline_option("skip").set_description("Skip this amount of bytes before reading the raw data");
  append_member(skip,"skip");

  dset.set_cmdline_option("ds").set_description("Dataset index to extract if multiple datasets are read");
  append_member(dset,"dset");

  filter.set_cmdline_option("filter").set_description("Read only those datasets which protocol parameter 'key' contains the string 'value' (given in the format 'key=value')");
  append_member(filter,"filter");

  dialect.set_cmdline_option("rdialect").set_description("Read data using given dialect of the format. (default is no dialect)");
  append_member(dialect,"rdialect");

  fmap=false;
  fmap.set_cmdline_option("fmap").set_description("For reduced memory usage, keep filemapping after reading (raw) data, but writing into the array will result in a crash");
  append_member(fmap,"fmap");
}

//////////////////////////////////////////////////////////////

FileWriteOpts::FileWriteOpts() {

  format.add_item(AUTODETECTSTR);
  svector formats(FileIO::autoformats());
  for(unsigned int i=0; i<formats.size(); i++) format.add_item(formats[i]);
  format.set_actual(0);
  format.set_cmdline_option("wf").set_description("Write format, use it to override file extension");
  append_member(format,"format");

  noscale=false;
  noscale.set_cmdline_option("noscale").set_description("Do not rescale values when storing integers");
  append_member(noscale,"noscale");

  append=false;
  append.set_cmdline_option("append").set_description("Append to existing file, only for raw data");
  append_member(append,"append");

  wprot.set_cmdline_option("wp").set_description("Store the protocol separately to this file.");
  append_member(wprot,"wprot");


  split=false;
  split.set_cmdline_option("split").set_description("Force splitting of protocol-data pairs into separate files.");
  append_member(split,"split");

  dialect.set_cmdline_option("wdialect").set_description("Write data using given dialect of the format. (default is no dialect)");
  append_member(dialect,"wdialect");

  datatype.add_item(AUTOTDATAYPESTR);
  datatype.add_item(TypeTraits::type2label((float)0));
  datatype.add_item(TypeTraits::type2label((double)0));
  datatype.add_item(TypeTraits::type2label((s32bit)0));
  datatype.add_item(TypeTraits::type2label((u32bit)0));
  datatype.add_item(TypeTraits::type2label((s16bit)0));
  datatype.add_item(TypeTraits::type2label((u16bit)0));
  datatype.add_item(TypeTraits::type2label((s8bit)0));
  datatype.add_item(TypeTraits::type2label((u8bit)0));
  datatype.set_actual(0);
  datatype.set_cmdline_option("type").set_description("Image representation type");
  append_member(datatype,"type");

  fnamepar.set_cmdline_option("fnamepar").set_description("Space-separated list of protocol parameters to include when creating unique file names");
  append_member(fnamepar,"fnamepar");
}
