#include "filter_mask.h"
#include "utils.h" // for TinyVector comparison

void FilterGenMask::init(){
  min.set_description("lower threshold");
  append_arg(min,"min");
  max.set_description("upper threshold");
  append_arg(max,"max");
}

bool FilterGenMask::process(Data<float,4>& data, Protocol& prot) const {

/*
TinyVector<int,4> datashape=data.shape();
  TinyVector<int,4> maskshape=data.shape();
  maskshape(timeDim)=1;

  Data<float,4> mask(maskshape); mask=1.0;
  TinyVector<int,4> maskindex;
  TinyVector<int,4> dataindex;
  for(int i=0; i<mask.size(); i++) {
    maskindex=mask.create_index(i);
    dataindex=maskindex;
    for(int irep=0; irep<datashape(timeDim); irep++) { // Condition must be met for all time steps
      dataindex(timeDim)=irep;
      float val=data(dataindex);
      if(val<min || val>max) mask(maskindex)=0.0;
    }
  }

  data.reference(mask);
*/

  data=where(Array<float,4>(data)>=min && Array<float,4>(data)<=max, float(1.0), float(0.0));
  return true;
}


///////////////////////////////////////////////////////////////////////////

void FilterAutoMask::init() {

//  dump_histogram_fname.set_description("dump histogram");
//  append_arg(dump_histogram_fname,"dump_histogram_fname");
}

bool FilterAutoMask::process(Data<float,4>& data, Protocol& prot) const {
  Log<Filter> odinlog(c_label(),"process");

  int nslots=100;

  float maxval=max(data);
  float step=secureDivision(maxval,nslots);

  ODINLOG(odinlog,normalDebug) << "maxval/nslots/step=" << maxval << "/" << nslots << "/" << step << STD_endl; 

  Data<float,1> hist(nslots); hist=0.0;

  // create histogram
  for(unsigned int i=0; i<data.size(); i++) {
    float val=data(data.create_index(i));
    int slot=int(secureDivision(val,step));
    ODINLOG(odinlog,normalDebug) << "val/slot=" << val << "/" << slot << STD_endl; 
    if(slot>=0 && slot<nslots) hist(slot)++;
  }

//  ODINLOG(odinlog,infoLog) << "dump_histogram_fname=" << dump_histogram_fname << STD_endl; 
//  if(dump_histogram_fname!="") hist.write_asc_file(dump_histogram_fname);


  // find first minimum
  float thresh=0.0;
  for(int islot=0; islot<(nslots-1); islot++) {
    if(hist(islot+1)>hist(islot)) {
      thresh=(islot+1)*step; // use one step after min for threshold
      break;
    }
  }
  ODINLOG(odinlog,normalDebug) << "thresh=" << thresh << STD_endl; 

  data=where(Array<float,4>(data)>thresh, float(1.0), float(0.0));

  return true;
}


///////////////////////////////////////////////////////////////////////////


typedef STD_list<TinyVector<int,4> > QuantilIndexList;
typedef STD_map<float, QuantilIndexList > QuantilIndexMap;


void FilterQuantilMask::init(){
  fraction.set_minmaxval(0.0,1.0).set_description("quantil");
  append_arg(fraction,"fraction");
}

bool FilterQuantilMask::process(Data<float,4>& data, Protocol& prot) const {
  Log<Filter> odinlog(c_label(),"process");

  int ntotal=data.size();

  float frac=fraction;
  check_range<float>(frac,0.0,1.0);
  int nmask=int((1.0-frac)*ntotal+0.5);

  ODINLOG(odinlog,normalDebug) << "ntotal/nmask=" << ntotal << "/" << nmask << STD_endl; 

  Data<float,4> mask(data.shape()); mask=0.0;

  QuantilIndexMap indexmap;

  for(int i=0; i<ntotal; i++) {
    TinyVector<int,4> index=data.create_index(i);
    indexmap[data(index)].push_back(index);
  }
  int nmap=indexmap.size();

  ODINLOG(odinlog,normalDebug) << "ntotal/nmap=" << ntotal << "/" << nmap << STD_endl; 

  QuantilIndexMap::const_iterator mapiter=indexmap.end();
  int j=0;
  while(j<nmask) {
    if(mapiter==indexmap.begin()) break;
    --mapiter;
    const QuantilIndexList& indexlist=mapiter->second;
    ODINLOG(odinlog,normalDebug) << "indexmap(" << mapiter->first << ")=" << indexlist.size() << STD_endl; 
    for(QuantilIndexList::const_iterator listiter=indexlist.begin(); listiter!=indexlist.end(); ++listiter) {
      mask(*listiter)=1.0;
      j++;
    }
  }

  data.reference(mask);

  return true;
}

///////////////////////////////////////////////////////////////////////////


void FilterSphereMask::init(){
  pos.set_description("Position string in the format (slicepos,phasepos,readpos)");
  append_arg(pos,"pos");
  radius.set_unit(ODIN_SPAT_UNIT).set_description("radius");
  append_arg(radius,"radius");
}

bool FilterSphereMask::process(Data<float,4>& data, Protocol& prot) const {
  Log<Filter> odinlog(c_label(),"process");


  Range all=Range::all();


  svector toks=tokens(extract(pos, "(", ")", true),',');
  ODINLOG(odinlog,normalDebug) << "toks=" << toks.printbody() << STD_endl;

  if(toks.size()!=3) {
    ODINLOG(odinlog,errorLog) << "Wrong size (" << toks.size() << "!=3) of position string >" << pos << "<" << STD_endl;
    return false;
  }

  TinyVector<int,3> centerpos(atoi(toks[0].c_str()), atoi(toks[1].c_str()), atoi(toks[2].c_str()));

  TinyVector<int,4> shape=data.shape();
  TinyVector<int,4> maskshape(shape);
  maskshape(timeDim)=1;

  Data<float,4> mask(maskshape); mask=0.0;

  TinyVector<int,3> spatshape(shape(sliceDim), shape(phaseDim), shape(readDim));

  TinyVector<float,3> voxel_spacing;
  voxel_spacing(0)=FileFormat::voxel_extent(prot.geometry, sliceDirection, data.extent(sliceDim));
  voxel_spacing(1)=FileFormat::voxel_extent(prot.geometry, phaseDirection, data.extent(phaseDim));
  voxel_spacing(2)=FileFormat::voxel_extent(prot.geometry, readDirection,  data.extent(readDim));
  ODINLOG(odinlog,normalDebug) << "voxel_spacing=" << voxel_spacing << STD_endl;


  for(unsigned int i=0; i<mask.size(); i++) {

    TinyVector<int,4> index=mask.create_index(i);

    TinyVector<int,3> spatindex(index(1),index(2),index(3));

    TinyVector<float,3> dist=voxel_spacing*(spatindex-centerpos);

    float r=sqrt(double(sum(dist*dist)));

    if(r<=radius) mask(index)=1.0;

  }

  data.reference(mask);

  return true;
}



///////////////////////////////////////////////////////////////////////////

void FilterUseMask::init() {

  fname.set_description("filename");
  append_arg(fname,"fname");
}


bool FilterUseMask::process(Data<float,4>& data, Protocol& prot) const {
  Log<Filter> odinlog(c_label(),"process");

  // Load external file
  Data<float,4> maskdata;
  if(maskdata.autoread(fname)<0) return false;
  TinyVector<int,4> maskshape=maskdata.shape();
  TinyVector<int,4> datashape=data.shape();

  maskshape(timeDim)=datashape(timeDim)=1;
  if(maskshape!=datashape) {
    ODINLOG(odinlog,errorLog) << "shape mismatch: " << maskshape << "!=" << datashape << STD_endl;
    return false;
  }

  fvector vals;
  for(unsigned int i=0; i<data.size(); i++) {
    TinyVector<int,4> index=data.create_index(i);
    float val=data(index);
    index(timeDim)=0;
    if(maskdata(index)) vals.push_back(val);
  }

  data.resize(1,vals.size(),1,1);
  data(0,Range::all(),0,0)=Data<float,1>(vals);

  return true;
}
