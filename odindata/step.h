/***************************************************************************
                          step.h  -  description
                             -------------------
    begin                : Sat Dec 30 2006
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef STEP_H
#define STEP_H

#include <odindata/data.h>

/**
  * @addtogroup odindata
  * @{
  */

///////////////////////////////////////////////////////////////////////////////////

/**
  * Base class for all steps (functors).
  * The template argument is the type of the specialized, derived class.
  */
template<class T>
class Step {

 public:

  virtual ~Step() {}

/**
  * Overload this function to return a unique label for the functor.
  */
  virtual STD_string label() const = 0;

/**
  * Overload this function to return a brief description of the functor.
  */
  virtual STD_string description() const = 0;

/**
  * Overload this function to allocate an empty object of the step.
  */
  virtual T* allocate() const = 0;

/**
  * Initialize the step, i.e. initiliaze its parameters and append them by 'append_arg'.
  */
  virtual void init() = 0;

/**
  * Clone this step (including its arguments).
  */
  T* clone() const;

/**
  * Overrides the arguments of the functor using a string with comma-separated argument values.
  */
  void set_args(const STD_string& argstr);


/**
  * Returns description of arguments as comma-separated list.
  */
  STD_string args_description() const;


/**
  * Returns the number of arguments.
  */
  unsigned int numof_args() const {return args.numof_pars();}


/**
  * Append arguments of the functor to parblock.
  */
  void append_opts(LDRblock& parblock);


 protected:


/**
  * Append step argument, to be used in init() of each step.
  */
  void append_arg(LDRbase& arg, const STD_string& arglabel);


  const char* c_label() const {if(!label_cache.size()) label_cache=label(); return label_cache.c_str();} // For Debug

 private:

  LDRblock args;
  mutable STD_string label_cache;
};


///////////////////////////////////////////////////////////////////////////////////

/**
  * Factory for steps (functors).
  * The template argument is the type of the step to be created by the factory.
  */
template<class T>
class StepFactory {

 public:

/**
  * Set up factory (creates template objects). Appends parameters of all steps to 'parblock'.
  */
  StepFactory(LDRblock* parblock=0);

  ~StepFactory();

/**
  * Returns allocated step functor which matches given 'label'.
  */
  T* create(const STD_string& label) const;

/**
  * Returns documention 'code' for doxygen.
  */
  STD_string manual() const;

/**
  * Returns documention for command line.
  */
  STD_string get_cmdline_usage(const STD_string& lineprefix) const;

 private:

  typedef STD_map<STD_string,T*> StepMap;
  StepMap templates;

  mutable STD_list<T*> garbage;

};


/** @}
  */
#endif

