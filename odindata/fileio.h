/***************************************************************************
                          fileio.h  -  description
                             -------------------
    begin                : Fri Apr 6 2001
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef FILEIO_H
#define FILEIO_H

//shortcut to check for type from string (templated function doesn work well with mingw - so we use macro)
#define IS_TYPE(type, name) (name==TypeTraits::type2label((type)0))

#include <odindata/data.h>
#include <odindata/fileio_opts.h>


#define AUTODETECTSTR "autodetect"
#define AUTOTDATAYPESTR "automatic"


/**
  * @addtogroup odindata
  * @{
  */


/**
  * The 4 dimensions of data arrays used in FileIO and Filter components, these dimensions are
  * - timeDim: Time direction
  * - sliceDim: Slice direction
  * - phaseDim: Phase direction
  * - readDim: Read direction
  */
enum                      dataDim { timeDim=0, sliceDim, phaseDim, readDim, n_dataDim };
static const char* dataDimLabel[]={"time",    "slice",  "phase",  "read"};
AVOID_CC_WARNING(dataDimLabel)


/**
  * Structure to accomodate autoread/autwrite functionality. Please refer to (\ref fileio_doc ) for a detailed description of this module.
  */
class FileIO {

 public:

  /**
    * Type to associate protocols with tempo-spatial raw data via std::map
    */
  typedef STD_map<Protocol, Data<float,4> > ProtocolDataMap;

  /**
    * Reads in file 'filename' and appends all data sets found to protocol-data map 'pdmap', autodetects file format according to file extension.
    * Read options are given by 'opts' and 'protocol_template' contains defaults used by the decoder/encoder of the particular file format.
    * The progress meter 'progmeter' can be optionally specified to monitor the progress of the operation.
    * Returns number of slices, or -1 if it fails.
    */
  static int autoread(ProtocolDataMap& pdmap, const STD_string& filename, const FileReadOpts& opts, const Protocol& protocol_template,ProgressMeter* progmeter=0);

  /**
    * Writes out protocol-data map 'pdmap' to file(s) filename, autodetects file format according to file extension.
    * Write options are given by 'opts'.
    * Returns number of slices sucessfully written, or -1 if it fails.
    */
  static int autowrite(const ProtocolDataMap& pdmap, const STD_string& filename, const FileWriteOpts& opts);

  /**
    * Returns possible file formats/extensions to autoread autowrite
    */
  static svector autoformats();

  /**
    * Returns possible file formats as string with comment
    */
  static STD_string autoformats_str(const STD_string& indent="");


  /**
    * Returns whether tracing via stdout is enabled/disabled
    */
  static bool get_trace_status() {return do_trace;}

  /**
    * Enables/disables tracing via stdout
    */
  static void set_trace_status(bool stat) {do_trace=stat;}


  static const char* get_compName(); // For debugging FilieIO component

 private:
  static logPriority loglevel() {if(do_trace) return infoLog; else return normalDebug;}
  static bool do_trace;

  static int read_dir(ProtocolDataMap& pdmap, const STD_string& dirname, const FileReadOpts& opts, const Protocol& protocol_template,ProgressMeter* progmeter=0);

};

//////////////////////////////////////////////////////////////////////////


/**
  * Base class for file formats
  */
class FileFormat {

 public:

/**
  * Use this function on a static instance to register the corresponding file format
  */
  void register_format();

/**
  *  creates unique filenames based on 'filename' for the protocol-data pairs in 'pdmap' using comma-separated list of additional parameters in 'par'
  */
  static svector create_unique_filenames(const STD_string& filename, const FileIO::ProtocolDataMap& pdmap, const STD_string& par);

/**
  *  returns the voxel extent of the 'geometry' in the given 'direction' using a matrix size 'size'.
  */
  static float voxel_extent(const Geometry& geometry, direction direction, int size);



protected:
  virtual ~FileFormat() {} // avoid compiler warnings

/**
  *  Select the appropriate data type when writing binary data
  */
  static STD_string select_write_datatype(const Protocol &prot,const FileWriteOpts& opts);

 private:

  // limit access
  friend class FileIO;

  virtual STD_string description() const = 0;

  virtual svector suffix() const = 0;

  virtual svector dialects() const = 0;

  virtual int read(Data<float,4>& data, const STD_string& filename, const FileReadOpts& opts, Protocol& prot);
  virtual int read(FileIO::ProtocolDataMap& pdmap, const STD_string& filename, const FileReadOpts& opts, const Protocol& protocol_template);

  virtual int write(const Data<float,4>& data, const STD_string& filename, const FileWriteOpts& opts, const Protocol& prot);
  virtual int write(const FileIO::ProtocolDataMap& pdmap, const STD_string& filename, const FileWriteOpts& opts);

  typedef STD_list<FileFormat*> FormatList;
  typedef STD_map<STD_string, FormatList> FormatMap;
  static FormatMap formats;

  static STD_string analyze_suffix(const STD_string& filename);

  static FileFormat* get_format(const STD_string& filename, const STD_string& override_suffix);

  static svector possible_formats();

  static STD_string formats_str(const STD_string& indent);

  static void format_error(const STD_string& filename);

};



/** @}
  */




#endif
