/***************************************************************************
                          converter.h  -  description
                             -------------------
    begin                : Tue Dec 4 2001
    copyright            : (C) 2000-2015 by Michael von Mengershausen
    email                : mengers@cns.mpg.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CONVERTER_H
#define CONVERTER_H

#include <limits>
#include <assert.h>
#include <stdint.h>

#include <tjutils/tjcomplex.h>
#include <tjutils/tjtypes.h>
#include <tjutils/tjlog.h>

/**
  * @addtogroup odindata
  * @{
  */

// helper class used for debugging the odindata component
class OdinData {
 public:
  static const char* get_compName();
};


////////////////////////////////////////////////////////////////////

/**
  * Helper class for conversion between numbers
  */
class Converter {

public:



//////////////////////////////////////////////////////////
// get_elements function overloading

//  specializations for complex type
static unsigned int get_elements(const STD_complex&) {
  return 2;
}

/**
  * Returns number of scalar numbers in this number type, e.g. complex has 2
  */
template<typename T>
static unsigned int get_elements(const T&) {
  return 1;
}


//////////////////////////////////////////////////////////


/**
  * Converts array 'src' with 'srcsize' elements to array 'dst' with 'dstsize' elements
  * Will scale "around 0" if 0 is part of the source value range. Elsewise values will be offset towards 0.
  * If destination is unsigned values will be offset to be positive domain if necessary.
  * If autoscale is "false" or if destination is floating point no scaling is done at all.
  */
template<typename Src, typename Dst>
static void convert_array(const Src* src, Dst* dst, unsigned int srcsize, unsigned int dstsize, bool autoscale=true) {
  Log<OdinData> odinlog("Converter","convert_array");
  unsigned int srcstep=get_elements(*dst);
  unsigned int dststep=get_elements(*src);
  ODINLOG(odinlog,normalDebug) << "srcstep/dststep=" << srcstep << "/" << dststep << STD_endl;

  bool doScale = (autoscale && std::numeric_limits<Dst>::is_integer);
  ODINLOG(odinlog,normalDebug) << "doScale=" << doScale << STD_endl;

  if(dststep*srcsize != srcstep*dstsize) {
    ODINLOG(odinlog,warningLog) << "size mismatch: dststep(" << dststep << ") * srcsize(" << srcsize << ") != srcstep(" << srcstep << ") * dstsize(" << dstsize << ")" << STD_endl;
  }

  double scale=1.0;
  double offset=0.0;
  if(doScale) {
    const double domain_minus=creal(std::numeric_limits<Dst>::min());//negative value domain of this dst
    const double domain_plus =creal(std::numeric_limits<Dst>::max());//positive value domain of this dst
    double minval=std::numeric_limits<double>::min();
    double maxval=std::numeric_limits<double>::max();
    if(srcsize>0) minval=maxval=creal(src[0]);
    for(unsigned int i=1; i<srcsize; i++) {
      if(src[i]<minval) minval=creal(src[i]);
      if(src[i]>maxval) maxval=creal(src[i]);
    }

    ODINLOG(odinlog,normalDebug) << "domain_minus/domain_plus=" << domain_minus << "/" << domain_plus << STD_endl;
    ODINLOG(odinlog,normalDebug) << "minval/maxval=" << minval << "/" << maxval << STD_endl;

    scale=secureDivision(domain_plus-domain_minus, maxval-minval);
    offset=0.5*(  (domain_plus+domain_minus) - secureDivision(maxval+minval, maxval-minval) * (domain_plus-domain_minus) );
    ODINLOG(odinlog,normalDebug) << "scale/offset=" << scale << "/" << offset << STD_endl;

  }


  if(srcstep==dststep) {//use common convert for data of same complexity
    unsigned int count=srcsize < dstsize?srcsize:dstsize;
    for(unsigned int i=0; i<count; i++)  convert(src+i, dst+i,scale,offset);
  } else { //do generic convert for data of different complexity
    unsigned int srcindex=0, dstindex=0;
    while(srcindex<srcsize && dstindex<dstsize) {
      convert(src+srcindex, dst+dstindex,scale,offset);
      srcindex+=srcstep;
      dstindex+=dststep;
    }
  }
}



private:

//////////////////////////////////////////////////////////
// convert



// Implementing our own round functions since lround/lroundf do not work for unsigned numbers, and they are flawed in MinGW
template<typename T> static T round(double x) {
  double tobecasted=x < 0 ? x - 0.5 : x + 0.5;
  if(std::numeric_limits<T>::is_integer) {
    if(tobecasted<std::numeric_limits<T>::min()) tobecasted=std::numeric_limits<T>::min();
    if(tobecasted>std::numeric_limits<T>::max()) tobecasted=std::numeric_limits<T>::max();
  }
  return (T)(tobecasted);
}

// specialized converter functions

// to complex
static void convert(const s8bit* src, STD_complex* dst,float scale,float offset) {
  (*dst)=STD_complex(src[0]*scale+offset,src[1]*scale);
}
static void convert(const u8bit* src, STD_complex* dst,float scale,float offset) {
  (*dst)=STD_complex(src[0]*scale+offset,src[1]*scale);
}
static void convert(const u16bit* src, STD_complex* dst,float scale,float offset) {
  (*dst)=STD_complex(src[0]*scale+offset,src[1]*scale);
}
static void convert(const s16bit* src, STD_complex* dst,float scale,float offset) {
  (*dst)=STD_complex(src[0]*scale+offset,src[1]*scale);
}
static void convert(const u32bit* src, STD_complex* dst,float scale,float offset) {
  (*dst)=STD_complex(src[0]*scale+offset,src[1]*scale);
}
static void convert(const s32bit* src, STD_complex* dst,float scale,float offset) {
  (*dst)=STD_complex(src[0]*scale+offset,src[1]*scale);
}
static void convert(const float *src, STD_complex* dst,float scale,float offset) {
  dst[0]=STD_complex(src[0]*scale+offset,src[1]*scale);
}
static void convert(const double *src, STD_complex* dst,float scale,float offset) {
  dst[0]=STD_complex(src[0]*scale+offset,src[1]*scale);
}

// from complex
static void convert(const STD_complex* src, s8bit* dst,float scale,float offset) {
  dst[0]=round<s8bit>(src->real()*scale+offset);
  dst[1]=round<s8bit>(src->imag()*scale);
}
static void convert(const STD_complex* src, u8bit* dst,float scale,float offset) {
  dst[0]=round<u8bit>(src->real()*scale+offset);
  dst[1]=round<u8bit>(src->imag()*scale);
}
static void convert(const STD_complex* src, s16bit* dst,float scale,float offset) {
  dst[0]=round<s16bit>(src->real()*scale+offset);
  dst[1]=round<s16bit>(src->imag()*scale);
}
static void convert(const STD_complex* src, u16bit* dst,float scale,float offset) {
  dst[0]=round<u16bit>(src->real()*scale+offset);
  dst[1]=round<u16bit>(src->imag()*scale);
}
static void convert(const STD_complex* src, s32bit* dst,float scale,float offset) {
  dst[0]=round<s32bit>(src->real()*scale+offset);
  dst[1]=round<s32bit>(src->imag()*scale);
}
static void convert(const STD_complex* src, u32bit* dst,float scale,float offset) {
  dst[0]=round<u32bit>(src->real()*scale+offset);
  dst[1]=round<u32bit>(src->imag()*scale);
}
static void convert(const STD_complex* src, float* dst,float scale,float offset) {
  dst[0]=src->real()*scale+offset;
  dst[1]=src->imag()*scale;
}
static void convert(const STD_complex* src, double* dst,float scale,float offset) {
  dst[0]=src->real()*scale+offset;
  dst[1]=src->imag()*scale;
}

// from float to integer
static void convert(const float* src, s8bit* dst,float scale,float offset) {
  dst[0]=round<s8bit>(src[0]*scale+offset);
}
static void convert(const float* src, u8bit* dst,float scale,float offset) {
  dst[0]=round<u8bit>(src[0]*scale+offset);
}
static void convert(const float* src, s16bit* dst,float scale,float offset) {
  dst[0]=round<s16bit>(src[0]*scale+offset);
}
static void convert(const float* src, u16bit* dst,float scale,float offset) {
  dst[0]=round<u16bit>(src[0]*scale+offset);
}
static void convert(const float* src, s32bit* dst,float scale,float offset) {
  dst[0]=round<s32bit>(src[0]*scale+offset);
}
static void convert(const float* src, u32bit* dst,float scale,float offset) {
  dst[0]=round<u32bit>(src[0]*scale+offset);
}

// from double to integer
static void convert(const double* src, s8bit* dst,float scale,float offset) {
  dst[0]=round<s8bit>(src[0]*scale+offset);
}
static void convert(const double* src, u8bit* dst,float scale,float offset) {
  dst[0]=round<u8bit>(src[0]*scale+offset);
}
static void convert(const double* src, s16bit* dst,float scale,float offset) {
  dst[0]=round<s16bit>(src[0]*scale+offset);
}
static void convert(const double* src, u16bit* dst,float scale,float offset) {
  dst[0]=round<u16bit>(src[0]*scale+offset);
}
static void convert(const double* src, s32bit* dst,float scale,float offset) {
  dst[0]=round<s32bit>(src[0]*scale+offset);
}
static void convert(const double* src, u32bit* dst,float scale,float offset) {
  dst[0]=round<u32bit>(src[0]*scale+offset);
}



//default
template<typename Src, typename Dst>
    static void convert(const Src* src, Dst* dst,float scale,float offset) {
      dst[0]=(Dst)(src[0]*scale+offset);
}

//////////////////////////////////////////////////////////

Converter() {} // Do not allow instances

};

/** @}
  */

#endif
