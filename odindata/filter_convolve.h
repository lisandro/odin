/***************************************************************************
                          filter_convolve.h  -  description
                             -------------------
    begin                : Thu Jun 13 2012
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef FILTER_CONVOLVE_H
#define FILTER_CONVOLVE_H

#include <odindata/filter_step.h>
#include <odinpara/ldrfilter.h>

class FilterConvolve : public FilterStep {

  LDRfilter kernel;
  LDRfloat kerneldiameter;

  STD_string label() const {return "convolve";}
  STD_string description() const {return "Convolution in spatial dimensions";}
  bool process(Data<float,4>& data, Protocol& prot) const;
  FilterStep*  allocate() const {return new FilterConvolve();}
  void init();
};

#endif
