/***************************************************************************
                          fileio_opts.h  -  description
                             -------------------
    begin                : Mon Nov 13 2006
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef FILEIO_OPTS_H
#define FILEIO_OPTS_H

#include <odinpara/ldrblock.h>
#include <odinpara/ldrtypes.h>
#include <odinpara/ldrnumbers.h>

/**
  * @addtogroup odindata
  * @{
  */

/**
  * Options for autoread
  */
struct FileReadOpts : LDRblock {

  FileReadOpts();

  LDRenum   format;
  LDRstring ldr;
  LDRenum   cplx;
  LDRint    skip;
  LDRstring dset;
  LDRstring filter;
  LDRstring dialect;
  LDRbool   fmap;
};

//////////////////////////////////////////////////////////////////////////

/**
  * Options for autowrite
  */
struct FileWriteOpts : LDRblock {

  FileWriteOpts();

  LDRenum   format;
  LDRbool   noscale;
  LDRbool   append;
  LDRstring wprot;
  LDRbool   split;
  LDRstring dialect;
  LDRenum   datatype;
  LDRstring fnamepar;
};



/** @}
  */



#endif
