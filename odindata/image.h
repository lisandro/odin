/***************************************************************************
                          image.h  -  description
                             -------------------
    begin                : Fr Feb 25 2005
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef IMAGE_H
#define IMAGE_H


#include <odinpara/protocol.h>


class Sample; // forward declaration

/**
  * @addtogroup odindata
  * @{
  */

/////////////////////////////////////////////////////////////////

/**
  * Class to hold reconstructed images of one Geometry
  */
class Image : public LDRblock {

 public:

/**
  * Default constructor
  */
  Image(const STD_string& label="unnamedImage");

/**
  * Copy constructor
  */
  Image(const Image& i) {Image::operator = (i);}

/**
  * Assignment operator
  */
  Image& operator = (const Image& i);

/**
  * Sets the magnitude data
  */
  Image& set_magnitude(const farray& magn) {magnitude=magn; return *this;}

/**
  * Scale magnitude to values between 0 and 1
  */
  Image& normalize_magnitude()  {magnitude.normalize(); return *this;}

/**
  * Returns the magnitude data
  */
  const farray& get_magnitude() const {return magnitude;}

/**
  * Interpret data as 3D volume data and return size for given axis
  */
  unsigned int size(axis ax) const;

/**
  * Sets the parameter set which describes the area imaged
  */
  Image& set_geometry(const Geometry& g) {geo=g; return *this;}

/**
  * Returns the parameter set which describes the area imaged
  */
  const Geometry& get_geometry() const {return geo;}

/**
  * Transpose image in-plane, 'reverse_read' and 'reverse_phase'
  * can be used to reverse read/phase direction before transposing.
  */
  Image& transpose_inplane(bool reverse_read=false, bool reverse_phase=false);

  // dummy comparison operator for lists
  bool operator < (const Image& img) const {return STD_string(get_label())<STD_string(img.get_label());}

 private:

  void append_all_members();

  Geometry geo;
  LDRfloatArr magnitude;

};

/////////////////////////////////////////////////////////////////

/**
  * Class to hold a whole list of Image's
  */
class ImageSet : public LDRblock {

 public:

/**
  * Default constructor
  */
  ImageSet(const STD_string& label="unnamedImageSet");

/**
  * Copy constructor
  */
  ImageSet(const Image& is) {ImageSet::operator = (is);}

/**
  * Create image set from a sample file
  */
  ImageSet(const Sample& smp);

/**
  * Assignment operator
  */
  ImageSet& operator = (const ImageSet& is);


/**
  * Append another image to the set
  */
  ImageSet& append_image(const Image& img);


/**
  * Clear the list of images in this set
  */
  ImageSet& clear_images();


/**
  * Returns the number of images in this set
  */
  int get_numof_images() const {return Content.length();}

/**
  * Returns reference to the image at position 'index'
  */
  Image& get_image(unsigned int index=0);


  // overloading virtual functions of LDRblock
  int load(const STD_string& filename, const LDRserBase& serializer=LDRserJDX());


 private:

  void append_all_members();

  LDRstringArr Content;
  STD_list<Image> images;

  Image dummy;
};

/** @}
  */

#endif

