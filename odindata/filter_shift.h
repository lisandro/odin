/***************************************************************************
                          filter_shift.h  -  description
                             -------------------
    begin                : Fri Nov 21 2008
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef FILTER_SHIFT_H
#define FILTER_SHIFT_H

#include <odindata/filter_step.h>

class FilterShift : public FilterStep {

  LDRfloat shift[n_directions];

  STD_string label() const {return "shift";}
  STD_string description() const {return "Shift data spatially";}
  bool process(Data<float,4>& data, Protocol& prot) const;
  FilterStep*  allocate() const {return new FilterShift();}
  void init();
};


////////////////////////////////////////////////////


class FilterTimeShift : public FilterStep {

  LDRfloat shiftframes;

  STD_string label() const {return "tshift";}
  STD_string description() const {return "Shift data in time";}
  bool process(Data<float,4>& data, Protocol& prot) const;
  FilterStep*  allocate() const {return new FilterTimeShift();}
  void init();
};

#endif
