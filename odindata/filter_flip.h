/***************************************************************************
                          filter_flip.h  -  description
                             -------------------
    begin                : Tue Jan 22 2008
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef FILTER_FLIP_H
#define FILTER_FLIP_H

#include <odindata/filter_step.h>

template<int Dir>
class FilterFlip : public FilterStep {

  STD_string label() const {return STD_string(1,STD_string(dataDimLabel[Dir])[0])+"flip";} // Use only 1st letter
  STD_string description() const {return "Flip data in "+STD_string(dataDimLabel[Dir])+" direction";}
  bool process(Data<float,4>& data, Protocol& prot) const {
    data.reverseSelf(Dir);

    // Adjust protocol
    dvector sign(3);
    sign=1.0;
    sign[3-Dir]=-1.0;
    prot.geometry.set_orientation_and_offset(
      sign[0]*prot.geometry.get_readVector(),
      sign[1]*prot.geometry.get_phaseVector(),
      sign[2]*prot.geometry.get_sliceVector(),
      prot.geometry.get_center());

    return true;
  }
  FilterStep*  allocate() const {return new FilterFlip<Dir>();}
  void init() {}
};

#endif
