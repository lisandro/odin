/***************************************************************************
                          filter_scale.h  -  description
                             -------------------
    begin                : Thu Jan 31 2008
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef FILTER_SCALE_H
#define FILTER_SCALE_H

#include <odindata/filter_step.h>

class FilterScale : public FilterStep {

  LDRfloat slope;
  LDRfloat offset;

  STD_string label() const {return "scale";}
  STD_string description() const {return "Rescale image values";}
  bool process(Data<float,4>& data, Protocol& prot) const;
  FilterStep*  allocate() const {return new FilterScale();}
  void init();
};

#endif
