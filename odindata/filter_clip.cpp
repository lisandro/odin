#include "filter_clip.h"

void FilterMin::init(){
  thresh.set_description("Minumum value");
  append_arg(thresh,"thresh");
}


bool FilterMin::process(Data<float,4>& data, Protocol& prot) const {
  data=where(Array<float,4>(data)<thresh, float(thresh), Array<float,4>(data));
  return true;
}

///////////////////////////////////////////////////


void FilterMax::init(){
  thresh.set_description("Maximum value");
  append_arg(thresh,"thresh");
}


bool FilterMax::process(Data<float,4>& data, Protocol& prot) const {
  data=where(Array<float,4>(data)>thresh, float(thresh), Array<float,4>(data));
  return true;
}


///////////////////////////////////////////////////

void FilterType::init(){
  type.set_description("Datatype");
  append_arg(type,"type");
}

float FilterType::getThresh(bool upper)const{
  if(     IS_TYPE( u8bit,type)) return upper ? std::numeric_limits<u8bit>::max(): std::numeric_limits<u8bit>::min();
  else if(IS_TYPE( s8bit,type)) return upper ? std::numeric_limits<s8bit>::max(): std::numeric_limits<s8bit>::min();
  else if(IS_TYPE(u16bit,type)) return upper ? std::numeric_limits<u16bit>::max():std::numeric_limits<u16bit>::min();
  else if(IS_TYPE(s16bit,type)) return upper ? std::numeric_limits<s16bit>::max():std::numeric_limits<s16bit>::min();
  else if(IS_TYPE(u32bit,type)) return upper ? std::numeric_limits<u32bit>::max():std::numeric_limits<u32bit>::min();
  else if(IS_TYPE(s32bit,type)) return upper ? std::numeric_limits<s32bit>::max():std::numeric_limits<s32bit>::min();
  else if(IS_TYPE(float,type))  return upper ? std::numeric_limits<float>::max(): std::numeric_limits<float>::min();
  else if(IS_TYPE(double,type)) return upper ? std::numeric_limits<double>::max():std::numeric_limits<double>::min();
  return 0.0;
}

bool FilterTypeMax::process(Data<float,4>& data, Protocol& prot) const {
  float thresh=getThresh(true);
  data=where(Array<float,4>(data)>thresh, thresh, Array<float,4>(data));
  return true;
}

bool FilterTypeMin::process(Data<float,4>& data, Protocol& prot) const {
  float thresh=getThresh(false);
  data=where(Array<float,4>(data)<thresh, thresh, Array<float,4>(data));
  return true;
}

