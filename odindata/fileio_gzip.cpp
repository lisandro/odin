#include "fileio.h"

/*
this based on the minigz-example by Jean-loup Gailly
of the zlib distribution so hail him :-)
(and see below for copyright notice)
*/

/* zlib.h -- interface of the 'zlib' general purpose compression library
  version 1.2.3, July 18th, 2005

  Copyright (C) 1995-2005 Jean-loup Gailly and Mark Adler

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.

Jean-loup Gailly        Mark Adler
jloup@gzip.org          madler@alumni.caltech.edu

The data format used by the zlib library is described by RFCs (Request for
Comments) 1950 to 1952 in the files http://www.ietf.org/rfc/rfc1950.txt
(zlib format), rfc1951.txt (deflate format) and rfc1952.txt (gzip format).
*/

#ifdef HAVE_LIBZ
#ifndef STREAM_REPLACEMENT

#include <stdio.h>
#include <zlib.h>

struct GzipFormat : public FileFormat
{
  STD_string description() const {return "GNU-Zip container for other formats";}
  svector suffix() const
  {
    svector result; result.resize(1);
    result[0]="gz";
    return result;
  }
  svector dialects() const {return svector();}

  bool gz_compress(STD_ifstream &in, gzFile out) {
    Log<FileIO> odinlog("GzipFormat","gz_compress");
    char *buf = new char[2048*1024];
    int len;
    int err;
    bool ret=true;

    try
    {
      for (
        in.read(buf, 2048*1024);
        (len = in.gcount());
        in.read(buf, 2048*1024)
      ) {
        if (gzwrite(out, buf, len) != len) {
          ODINLOG(odinlog,errorLog) << gzerror(out, &err) << STD_endl;
          return false;
        }
      }
      if (in.bad()) {
        ODINLOG(odinlog,errorLog) <<"file read" << STD_endl;return false;
      }
    }
    catch(...) {
      delete[]  buf;
      throw;
    }
    delete[] buf;
    return ret;
  }

  bool file_compress(STD_string infile, STD_string outfile) {
    Log<FileIO> odinlog("GzipFormat","file_compress");
    STD_ifstream in(infile.c_str(), ios::binary);
    if (in.bad()) {
      ODINLOG(odinlog,errorLog) << infile.c_str() << STD_endl;
      return false;
    }
    gzFile out = gzopen(outfile.c_str(), "wb");
    if (out == NULL) {
      ODINLOG(odinlog,errorLog) << "gzopen " << outfile << " failed" << STD_endl;
      return false;
    }
    bool ret=gz_compress(in, out);
    if (gzclose(out) != Z_OK) {
      ODINLOG(odinlog,errorLog) <<  "gzclose " << outfile << " failed" << STD_endl;
      return false;
    }
    else return ret;
  }

  bool gz_uncompress(gzFile in, STD_ofstream &out) {
    Log<FileIO> odinlog("GzipFormat","gz_uncompress");
    char *buf=new char[2048*1024];
    int len;
    int err;

    try
    {
      for (
        len = gzread(in, buf, 2048*1024);
        len;
        len = gzread(in, buf, 2048*1024)
      ) {
        if (len < 0){ODINLOG(odinlog,errorLog) <<  gzerror(in, &err) << STD_endl;return false;}
        else {
          out.write(buf,len);
          if (out.bad()) {ODINLOG(odinlog,errorLog) << "file write" << STD_endl;return false;}
        }
      }
    }
    catch(...) {
      delete[] buf;
      throw;
    }
    delete[] buf;
    return true;
  }

  bool file_uncompress(STD_string infile,STD_string outfile) {
    Log<FileIO> odinlog("GzipFormat","file_uncompress");
    gzFile in = gzopen(infile.c_str(), "rb");
    if (in == NULL) {
      ODINLOG(odinlog,errorLog) << "gzopen " << infile << " failed" << STD_endl;
      return false;
    }
    STD_ofstream out(outfile.c_str(), ios::binary);
    if (out.bad()) {
      ODINLOG(odinlog,errorLog) <<  infile.c_str() << STD_endl;
      return false;
    }
    bool ret=gz_uncompress(in, out);
    if (gzclose(in) != Z_OK) {
      ODINLOG(odinlog,errorLog) << "gclose " << outfile << " failed" << STD_endl;
      return false;
    }
    return ret;
  }

  static STD_string tempfilename(STD_string filename) {
    const STD_string unzipped_suffix=LDRfileName(LDRfileName(filename).get_basename_nosuffix()).get_suffix();
    return tempfile()+"."+unzipped_suffix;
  }

  int read(FileIO::ProtocolDataMap& pdmap, const STD_string& filename, const FileReadOpts& opts, const Protocol& protocol_template) {
    Log<FileIO> odinlog("GzipFormat","read");
    const STD_string tmpfile=tempfilename(filename);
    ODINLOG(odinlog,normalDebug) << "tmpfile=" << tmpfile << STD_endl;

    if(file_uncompress(filename,tmpfile)) {
      bool tracestat_cache=FileIO::get_trace_status();
      FileIO::set_trace_status(false);            // disable temporarily
      int result=FileIO::autoread(pdmap, tmpfile, opts, protocol_template);
      FileIO::set_trace_status(tracestat_cache);
      rmfile(tmpfile.c_str());
      return result;
    }
    else {
      return -1;
    }
  }

  int write(const FileIO::ProtocolDataMap& pdmap, const STD_string& filename, const FileWriteOpts& opts) {
    Log<FileIO> odinlog("GzipFormat","write");
    const STD_string tmpfile=tempfilename(filename);
    ODINLOG(odinlog,normalDebug) << "tmpfile=" << tmpfile << STD_endl;

    bool tracestat_cache=FileIO::get_trace_status();
    FileIO::set_trace_status(false);              // disable temporarily
    int result=FileIO::autowrite(pdmap, tmpfile, opts);
    FileIO::set_trace_status(tracestat_cache);

    if(result>=0) {
      if(file_compress(tmpfile,filename)) {
        rmfile(tmpfile.c_str());
      }
      else {
        const LDRfileName fname(filename);
        STD_string realname = fname.get_dirname() + SEPARATOR_STR +fname.get_basename_nosuffix();
        ODINLOG(odinlog,infoLog) << " saving " << realname << STD_endl;
        movefile(tmpfile.c_str(), realname.c_str());
      }
    }
    return result;
  }
};
#endif
#endif

//////////////////////////////////////////////////////////////

void register_gzip_format() {
#ifdef HAVE_LIBZ
#ifndef STREAM_REPLACEMENT
  static GzipFormat gf;
  gf.register_format();
#endif
#endif
}
