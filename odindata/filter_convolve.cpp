#include "filter_convolve.h"
#include "complexdata.h"


void FilterConvolve::init() {
  kernel.set_description("convolution kernel");
  append_arg(kernel,"kernel");

  kerneldiameter.set_unit(ODIN_SPAT_UNIT).set_description("kernel diameter");
  append_arg(kerneldiameter,"kerneldiameter");
}

bool FilterConvolve::process(Data<float,4>& data, Protocol& prot) const {
  Log<Filter> odinlog(c_label(),"process");

  Range all=Range::all();

  TinyVector<int,4> shape=data.shape();
  TinyVector<int,3> spatshape(shape(sliceDim), shape(phaseDim), shape(readDim));

  TinyVector<float,3> voxel_spacing;
  voxel_spacing(0)=FileFormat::voxel_extent(prot.geometry, sliceDirection, data.extent(sliceDim));
  voxel_spacing(1)=FileFormat::voxel_extent(prot.geometry, phaseDirection, data.extent(phaseDim));
  voxel_spacing(2)=FileFormat::voxel_extent(prot.geometry, readDirection,  data.extent(readDim));
  ODINLOG(odinlog,normalDebug) << "voxel_spacing=" << voxel_spacing << STD_endl;


  int oversampling=2;
  TinyVector<int,3> kernelshape=oversampling*spatshape;

  TinyVector<float,3> voxel_spacing_kernelgrid(voxel_spacing/oversampling);
  ODINLOG(odinlog,normalDebug) << "voxel_spacing_kernelgrid=" << voxel_spacing_kernelgrid << STD_endl;

  ComplexData<3> kernelgrid(kernelshape);
  kernelgrid=STD_complex(0.0);

  // fill with gridding kernel
  for(unsigned int i=0; i<kernelgrid.size(); i++) {
    TinyVector<int,3> index=kernelgrid.create_index(i);
    TinyVector<float,3> dist=voxel_spacing_kernelgrid*(index-kernelshape/2);


    float relradius=secureDivision( sqrt(double(sum(dist*dist))), 0.5*kerneldiameter);

    if(relradius<=1.0) {
      float kernelval=kernel.calculate(relradius);
      kernelgrid(index)=STD_complex(kernelval);
    }
  }

  kernelgrid.fft(true);

//  Data<float,3>(cabs(kernelgrid)).autowrite("kernelgrid.nii");

  TinyVector<int,3> lowerBounds(kernelshape/4);
  TinyVector<int,3> upperBounds(lowerBounds+spatshape-1);
  ComplexData<3> kernelmult(spatshape);
  kernelmult=kernelgrid(RectDomain<3>(lowerBounds, upperBounds));

  float kernelmax=max(creal(kernelmult));
  ODINLOG(odinlog,normalDebug) << "kernelmax=" << kernelmax << STD_endl;

  if(kernelmax>0.0) kernelmult=kernelmult/STD_complex(kernelmax);

//  Data<float,3>(cabs(kernelmult)).autowrite("kernelmult.nii");


  for(int irep=0; irep<shape(timeDim); irep++) {
    ComplexData<3> onevol(float2real(data(irep,all,all,all)));
    onevol.fft(true);
    onevol=onevol*kernelmult;
    onevol.fft(false);
    data(irep,all,all,all)=creal(onevol);
  }

  return true;
}
