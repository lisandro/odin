#include "data.h"
#include "fileio.h"
#include "utils.h"

#include <tjutils/tjtest.h>


//////////////////////////////////////////////////

int fileio_autowrite(const Data<float,4>& data, const STD_string& filename, const FileWriteOpts& opts, const Protocol* prot) {
  Log<OdinData> odinlog("","fileio_autowrite");
  FileIO::ProtocolDataMap pdmap;
  ODINLOG(odinlog,normalDebug) << "filename:opts.format:opts.datatype=" << filename << ":" << opts.format << ":" << opts.datatype << STD_endl;

  if(prot) {
    pdmap[*prot].reference(data);
  } else {
    Protocol prot;

    // set extensions in the protocol
    prot.seqpars.set_NumOfRepetitions(data.extent(0));
    prot.geometry.set_nSlices(data.extent(1));
    prot.seqpars.set_MatrixSize(phaseDirection,data.extent(2));
    prot.seqpars.set_MatrixSize(readDirection,data.extent(3));

    pdmap[prot].reference(data);
  }
  return FileIO::autowrite(pdmap, filename, opts);
}

int fileio_autoread(Data<float,4>& data, const STD_string& filename, const FileReadOpts& opts, Protocol* prot, ProgressMeter* progmeter) {
  Log<OdinData> odinlog("","fileio_autoread");
  FileIO::ProtocolDataMap pdmap;

  ODINLOG(odinlog,normalDebug) << "data/filename/prot=" << data.shape() << "/" << filename << "/" << prot << STD_endl;

  Protocol protocol_template;

  // set defaults for reading raw data
  protocol_template.seqpars.set_MatrixSize(readDirection,1);
  protocol_template.seqpars.set_MatrixSize(phaseDirection,1);
  protocol_template.seqpars.set_MatrixSize(sliceDirection,1);

  if(prot) protocol_template=(*prot);
  int result=FileIO::autoread(pdmap, filename, opts, protocol_template, progmeter);
  if(result<0) return -1;
  FileIO::ProtocolDataMap::const_iterator it=pdmap.begin();
  if(it!=pdmap.end()) {
    if(prot) (*prot)=it->first;
    data.reference(it->second);
  } else {
    ODINLOG(odinlog,errorLog) << "Empty protocol-data map" << STD_endl;
    return -1;
  }
  return result;
}


//////////////////////////////////////////////////
// Unit Test

#ifndef NO_UNIT_TEST
class DataTest : public UnitTest {

 public:
  DataTest() : UnitTest("Data") {}

 private:

  template <typename Type, int Rank>
  bool conversion_test(const Data<float,2>& testarray) const {
    Log<UnitTest> odinlog(this,"conversion_test");
    Data<Type,Rank> dst; testarray.convert_to(dst);

    STD_string prefix=STD_string("convert_to<")+TypeTraits::type2label((Type)0)+","+itos(Rank)+"> failed, ";

    TinyVector<int,Rank> expected_shape; expected_shape=1;
    expected_shape(Rank-1)*=testarray.extent(1);
    expected_shape(STD_max(0,Rank-2))*=testarray.extent(0);
    if(expected_shape!=dst.shape()) {
      ODINLOG(odinlog,errorLog) << prefix << "wrong shape=" << dst.shape() << ", but expected " << expected_shape << STD_endl;
      return false;
    }

    if(std::numeric_limits<Type>::is_integer) {

       // just check whether numeric range is fully covered for full-scale mode
      float minnum=std::numeric_limits<Type>::min();
      float maxnum=std::numeric_limits<Type>::max();
      float deltanum=maxnum-minnum;
      float minval=min(dst);
      float maxval=max(dst);
      float relmaxdiff_plus=fabs(maxval-maxnum)/deltanum;
      float relmaxdiff_minus=fabs(minval-minnum)/deltanum;
      if(relmaxdiff_plus>.02 && relmaxdiff_minus>.02) {
        ODINLOG(odinlog,errorLog) << prefix << "auto-scale range relmaxdiff=" << relmaxdiff_minus << "/" << relmaxdiff_plus << STD_endl;
        ODINLOG(odinlog,errorLog) << "minval/maxval=" << minval << "/" << maxval << STD_endl;
        ODINLOG(odinlog,errorLog) << "minnum/maxnum=" << minnum << "/" << maxnum << STD_endl;
        return false;
      }

      // Convert back to float
      Data<float,2> dstfloat;
      dst.convert_to(dstfloat);
      minval=min(dstfloat);
      maxval=max(dstfloat);
      relmaxdiff_plus=fabs(maxval-maxnum)/deltanum;
      relmaxdiff_minus=fabs(minval-minnum)/deltanum;
      if(relmaxdiff_plus>.02 && relmaxdiff_minus>.02) {
        ODINLOG(odinlog,errorLog) << prefix << "convert-back relmaxdiff=" << relmaxdiff_minus << "/" << relmaxdiff_plus << STD_endl;
        ODINLOG(odinlog,errorLog) << "minval/maxval=" << minval << "/" << maxval << STD_endl;
        ODINLOG(odinlog,errorLog) << "minnum/maxnum=" << minnum << "/" << maxnum << STD_endl;
        return false;
      }

      // Test down-scaling separately
      Data<float,2> largevals(testarray.copy());largevals(3,3)=minnum-100.0;largevals(2,2)=maxnum+100.0; // Make sure data is out of range of Type
      largevals.convert_to(dst);
      minval=min(dst);
      maxval=max(dst);
      relmaxdiff_plus=fabs(maxval-maxnum)/deltanum;
      relmaxdiff_minus=fabs(minval-minnum)/deltanum;
      if(relmaxdiff_plus>.02 && relmaxdiff_minus>.02) {
        ODINLOG(odinlog,errorLog) << prefix << "down-scale range relmaxdiff=" << relmaxdiff_minus << "/" << relmaxdiff_plus << STD_endl;
        ODINLOG(odinlog,errorLog) << "minval/maxval=" << minval << "/" << maxval << STD_endl;
        ODINLOG(odinlog,errorLog) << "minnum/maxnum=" << minnum << "/" << maxnum << STD_endl;
        return false;
      }


      // Test up-scaling
      Data<float,2> smallvals(testarray.copy()); smallvals*=0.001/STD_max(fabs(min(testarray)),fabs(max(testarray)));
      smallvals.convert_to(dst);
      minval=min(dst);
      maxval=max(dst);
      relmaxdiff_plus=fabs(maxval-maxnum)/deltanum;
      relmaxdiff_minus=fabs(minval-minnum)/deltanum;
      if(relmaxdiff_plus>.02 && deltanum>.02) {
        ODINLOG(odinlog,errorLog) << prefix << "up-scale range relmaxdiff=" << relmaxdiff_minus << "/" << relmaxdiff_plus << STD_endl;
        ODINLOG(odinlog,errorLog) << "smallvals=" << smallvals << STD_endl;
        ODINLOG(odinlog,errorLog) << "minval/maxval=" << minval << "/" << maxval << STD_endl;
        ODINLOG(odinlog,errorLog) << "minnum/maxnum=" << minnum << "/" << maxnum << STD_endl;
        return false;
      }

/*
      // Test non-upscaled conversion of small numbers
      smallvals.convert_to(dst,noupscale);
      minval=min(dst);
      maxval=max(dst);
      if(minval || maxval) { // should be zero
        ODINLOG(odinlog,errorLog) << prefix << "noupscale failed" << STD_endl;
        ODINLOG(odinlog,errorLog) << "smallvals=" << smallvals << STD_endl;
        ODINLOG(odinlog,errorLog) << "minval/maxval=" << minval << "/" << maxval << STD_endl;
        ODINLOG(odinlog,errorLog) << "minnum/maxnum=" << minnum << "/" << maxnum << STD_endl;
        return false;
      }
*/

      // do an additional test without auto-scaling if data is signed
      if(std::numeric_limits<Type>::is_signed) {
        testarray.convert_to(dst,false);
        float sumdiff=sum(dst)-sum(testarray);
        if(fabs(sumdiff)>0.1) {
          ODINLOG(odinlog,errorLog) << prefix << "no-scale sum sumdiff=" << sumdiff << STD_endl;
          ODINLOG(odinlog,errorLog) << "dst=" << dst << STD_endl;
          ODINLOG(odinlog,errorLog) << "testarray=" << testarray << STD_endl;
          return false;
        }
      }

    } else {
      for(unsigned int i=0; i<testarray.size(); i++) {
        TinyVector<int,2> testindex=testarray.create_index(i);
        TinyVector<int,Rank> dstindex=dst.create_index(i);
        if(testarray(testindex)!=dst(dstindex)) {
          ODINLOG(odinlog,errorLog) << prefix << "value mismatch at index " << testindex << STD_endl;
          ODINLOG(odinlog,errorLog) << testarray(testindex) << " != " << dst(dstindex) << STD_endl;
          return false;
        }
      }
    }
    return true;
  }


  template <typename Type>
  bool readwrite_mmap_test(const Data<float,2>& testarray) const {
    Log<UnitTest> odinlog(this,"readwrite_mmap_test");
    Data<Type,2> dst; testarray.convert_to(dst);

    STD_string prefix=STD_string("read/write/mmap<")+TypeTraits::type2label((Type)0)+"> failed, ";

    STD_string testfname(tempfile());

    // Testing (f)write against filemap
    int testoffset=10000; // bytes
    Data<u8bit,1>(testfname, false, testoffset); // Create offset
    if(dst.write(testfname, appendMode)) { // append to offset
      ODINLOG(odinlog,errorLog) << prefix << "write(" << testfname <<")" << STD_endl;
      return false;
    }
    Data<Type,2> mmappedarray(testfname, true, testarray.shape(), testoffset);
    if(!mmappedarray.is_filemapped()) {
      ODINLOG(odinlog,errorLog) << prefix << "filemap of >" << testfname << "<" << STD_endl;
      return false;
    }
    if(mmappedarray.shape()!=dst.shape()) {
      ODINLOG(odinlog,errorLog) << prefix << "wrong shape=" << mmappedarray.shape() << ", but expected " << dst.shape() << STD_endl;
      return false;
    }
    for(unsigned int i=0; i<dst.size(); i++) {
      TinyVector<int,2> index=dst.create_index(i);
      if(mmappedarray(index)!=dst(index)) {
        ODINLOG(odinlog,errorLog) << prefix << "mmap value mismatch at index " << index << STD_endl;
        ODINLOG(odinlog,errorLog) << mmappedarray(index) << " != " << dst(index) << STD_endl;
        return false;
      }
    }

    // Cross-wise testing of template vs. format read/write
    if(testarray.write(TypeTraits::type2label((Type)0),testfname,true)) { // implicit conversion, use full range of integer
      ODINLOG(odinlog,errorLog) << prefix << "write(" << TypeTraits::type2label((Type)0) << "," << testfname <<")" << STD_endl;
      return false;
    }
#if __GNUC__ > 3 // The following gives parse error on earlier GCC versions
    Data<float,2> testread(testarray.shape());
    testread=0.0;
    if(testread.read<Type>(testfname)) {
      ODINLOG(odinlog,errorLog) << "read<" << TypeTraits::type2label((Type)0) << ">(" << testfname <<")" << STD_endl;
      return false;
    }

    if(std::numeric_limits<Type>::is_integer) {

      // Check wheter full range was used
      float minnum=std::numeric_limits<Type>::min();
      float maxnum=std::numeric_limits<Type>::max();
      float deltanum=maxnum-minnum;
      float minval=min(testread);
      float maxval=max(testread);
      float relmaxdiff_plus=fabs(maxval-maxnum)/deltanum;
      float relmaxdiff_minus=fabs(minval-minnum)/deltanum;
      if(relmaxdiff_plus>.02 && relmaxdiff_minus>.02) {
        ODINLOG(odinlog,errorLog) << prefix << "read relmaxdiff=" << relmaxdiff_minus << "/" << relmaxdiff_plus << STD_endl;
        ODINLOG(odinlog,errorLog) << "minval/maxval=" << minval << "/" << maxval << STD_endl;
        ODINLOG(odinlog,errorLog) << "minnum/maxnum=" << minnum << "/" << maxnum << STD_endl;
        return false;
      }

    } else {

      if(testarray.shape()!=testread.shape()) {
        ODINLOG(odinlog,errorLog) << prefix << "shape mismatch: " << testarray.shape() << " != " << testread.shape() << STD_endl;
        return false;
      }

      for(unsigned int i=0; i<testarray.size(); i++) { // convert both implicitely and explicitely converted arrays
        TinyVector<int,2> index=testarray.create_index(i);
        if(testarray(index)!=testread(index)) {
          ODINLOG(odinlog,errorLog) << prefix << "read/write value mismatch at index " << index << STD_endl;
          ODINLOG(odinlog,errorLog) << testarray(index) << " != " << testread(index) << STD_endl;
          return false;
        }
      }
    }
#endif

    return true;
  }



  bool check() const {
    Log<UnitTest> odinlog(this,"check");

    int testsize=10;

    Data<float,2> testarray(testsize,testsize);

    // checking create_index against create_linear_index, thereby filling testarray
    TinyVector<int,2> indexvec;
    for(unsigned int i=0; i<testarray.numElements(); i++) {
      indexvec=testarray.create_index(i);
      unsigned int linindex=testarray.create_linear_index(indexvec);
      if(linindex!=i) {
        ODINLOG(odinlog,errorLog) << "linindex/i/indexvec=" << linindex << "/" << i << "/" << indexvec << STD_endl;
        ODINLOG(odinlog,errorLog) << "indexvec test failed" << STD_endl;
        return false;
      }
      testarray(indexvec)=pow(-1.0,indexvec(0))*sqrt(float(sum(indexvec))); // some real-valued pos and neg values
    }

    Data<float,2> testarray_copy(testarray);
    testarray_copy.makeUnique();


    // test cyclical identitiy shift
    testarray.shift(1,3);
    float diff=sum(abs(testarray-testarray_copy));
    if(!diff) {
      ODINLOG(odinlog,errorLog) << "shift ineffective, zero diff" << STD_endl;
      return false;
    }
    testarray.shift(1,4);
    testarray.shift(1,3);
    diff=sum(abs(testarray-testarray_copy));
    if(diff) {
      ODINLOG(odinlog,errorLog) << "cyclical shift failed, diff=" << diff << STD_endl;
      return false;
    }



    if(!conversion_test<float,2>(testarray)) return false;
    if(!conversion_test<float,1>(testarray)) return false;
    if(!conversion_test<float,4>(testarray)) return false;
    if(!conversion_test<u8bit,3>(testarray)) return false;
    if(!conversion_test<s8bit,3>(testarray)) return false;
    if(!conversion_test<u16bit,3>(testarray)) return false;
    if(!conversion_test<s16bit,3>(testarray)) return false;
    if(!conversion_test<u32bit,3>(testarray)) return false;
    if(!conversion_test<s32bit,3>(testarray)) return false;


    // Testing complex conversion and convert_from_ptr() simultaneously
    Data<STD_complex,2> cmplxarr; testarray.convert_to(cmplxarr);
    Data<float,2> testarray2;
    convert_from_ptr(testarray2, cmplxarr.c_array(), testarray.shape());
    diff=sum(testarray-testarray2);
    if(diff) {
      ODINLOG(odinlog,errorLog) << "convert_to/from_ptr failed, diff=" << diff << STD_endl;
      ODINLOG(odinlog,errorLog) << "testarray=" << testarray << STD_endl;
      ODINLOG(odinlog,errorLog) << "cmplxarr=";
      for(unsigned int i=0; i<cmplxarr.size(); i++) ODINLOG(odinlog,errorLog) << cmplxarr[i] << "\t";
      ODINLOG(odinlog,errorLog) << STD_endl;
      ODINLOG(odinlog,errorLog) << "testarray2=" << testarray2 << STD_endl;
      return false;
    }


    if(!readwrite_mmap_test<float>(testarray)) return false;
    if(!readwrite_mmap_test<double>(testarray)) return false;
    if(!readwrite_mmap_test<u8bit>(testarray)) return false;
    if(!readwrite_mmap_test<s8bit>(testarray)) return false;
    if(!readwrite_mmap_test<u16bit>(testarray)) return false;
    if(!readwrite_mmap_test<s16bit>(testarray)) return false;
    if(!readwrite_mmap_test<u32bit>(testarray)) return false;
    if(!readwrite_mmap_test<s32bit>(testarray)) return false;



    // testing conversion to from tjarray
    farray fa(testarray);
    Data<float,4> testarray4d(fa); // should be padded with ones at front

    return true;
  }

};

void alloc_DataTest() {new DataTest();} // create test instance
#endif

