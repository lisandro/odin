/***************************************************************************
                          filter_rot.h  -  description
                             -------------------
    begin                : Sun Jan 20 2008
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef FILTER_ROT_H
#define FILTER_ROT_H

#include <odindata/filter_step.h>

class FilterRot : public FilterStep {

  LDRdouble angle;
  LDRdouble kernel;

  STD_string label() const {return "rot";}
  STD_string description() const {return "In-plane rotation";}
  bool process(Data<float,4>& data, Protocol& prot) const;
  FilterStep*  allocate() const {return new FilterRot();}
  void init();
};

#endif
