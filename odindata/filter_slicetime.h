/***************************************************************************
                          filter_slicetime.h  -  description
                             -------------------
    begin                : Fri Aug 3 2012
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef FILTER_SLICETIME_H
#define FILTER_SLICETIME_H

#include <odindata/filter_step.h>

class FilterSliceTime : public FilterStep {

  LDRstring sliceorderstr;

  STD_string label() const {return "slicetime";}
  STD_string description() const {return "Correct for different acquisition time points of slices";}
  bool process(Data<float,4>& data, Protocol& prot) const;
  FilterStep*  allocate() const {return new FilterSliceTime();}
  void init();
};

#endif
