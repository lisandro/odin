/***************************************************************************
                          filter_align.h  -  description
                             -------------------
    begin                : Fri Jan 16 2009
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef FILTER_ALIGN_H
#define FILTER_ALIGN_H

#include <odindata/filter_step.h>

class FilterAlign : public FilterStep {

  LDRfileName fname;
  LDRint      blowup;

  STD_string label() const {return "align";}
  STD_string description() const {return "Align data to the geometry (voxel locations) of an external file";}
  bool process(Data<float,4>& data, Protocol& prot) const;
  FilterStep*  allocate() const {return new FilterAlign();}
  void init();
};

#endif
