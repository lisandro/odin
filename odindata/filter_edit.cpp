#include "filter_edit.h"

void FilterEdit::init(){
  pos.set_description("Position/range string in the format (timeframe,slicepos,phasepos,readpos)");
  append_arg(pos,"pos");
  val.set_description("new value of voxel");
  append_arg(val,"val");
}

bool FilterEdit::process(Data<float,4>& data, Protocol& prot) const {
  Log<Filter> odinlog(c_label(),"process");

  TinyVector<int,4> shape=data.shape();
  ODINLOG(odinlog,normalDebug) << "shape=" << shape << STD_endl;

  ODINLOG(odinlog,normalDebug) << "pos=" << pos << STD_endl;
  ODINLOG(odinlog,normalDebug) << "val=" << val << STD_endl;


  svector toks=tokens(extract(pos, "(", ")", true),',');
  ODINLOG(odinlog,normalDebug) << "toks=" << toks.printbody() << STD_endl;

  if(toks.size()!=4) {
    ODINLOG(odinlog,errorLog) << "Wrong size (" << toks.size() << "!=4) of position string >" << pos << "<" << STD_endl;
    return false;
  }

  Range rng[4];
  for(int i=0; i<4; i++) {
    rng[i]=Range::all();
    if(!str2range(toks[i],rng[i],shape(i))) return false;
  }

  data(rng[timeDim], rng[sliceDim], rng[phaseDim], rng[readDim])=val;

  return true;
}
