#include "fileio.h"

#ifndef STREAM_REPLACEMENT

struct HFSSFormat : public FileFormat {
  STD_string description() const {return "Ansoft HFSS ASCII";}
  svector suffix() const  {
    svector result; result.resize(1);
    result[0]="reg";
    return result;
  }
  svector dialects() const {return svector();}

  int read(Data<float,4>& data, const STD_string& filename, const FileReadOpts& opts, Protocol& prot) {
    Log<FileIO> odinlog("HFSSFormat","read");

    STD_ifstream ifs(filename.c_str());
    if(ifs.bad()) {
      ODINLOG(odinlog,errorLog) << "Cannot open file " << filename << STD_endl;
      return -1;
    }

    // Read header
    STD_string firstline,secondline;
    if(!(getline(ifs,firstline) && getline(ifs,secondline)) ) {
      ODINLOG(odinlog,errorLog) << "Cannot read header" << STD_endl;
      return -1;
    }
    ODINLOG(odinlog,normalDebug) << "firstline=" << firstline << STD_endl;
    ODINLOG(odinlog,normalDebug) << "secondline=" << secondline << STD_endl;

    svector dimstrings=tokens(firstline,':');
    if(dimstrings.size()!=4) {
      ODINLOG(odinlog,errorLog) << "Format error in 1st line" << STD_endl;
      return -1;
    }

    svector minstring =tokens(extract(dimstrings[1],"[","]"));
    svector maxstring =tokens(extract(dimstrings[2],"[","]"));
    svector gridstring=tokens(extract(dimstrings[3],"[","]"));

    TinyVector<float,3> hfss_minpos, hfss_maxpos, hfss_res, hfss_FOV;
    TinyVector<int,3> hfss_size;

    float scale_factor=1000.0; // convert m to mm

    for(int i=0; i<3; i++) {
      hfss_res[2-i]=scale_factor*atof(gridstring[i].c_str());
      hfss_minpos[2-i]=scale_factor*atof(minstring[i].c_str())-0.5*hfss_res[2-i];
      hfss_maxpos[2-i]=scale_factor*atof(maxstring[i].c_str())+0.5*hfss_res[2-i];
      hfss_FOV[2-i]=fabs(hfss_maxpos[2-i]-hfss_minpos[2-i]);
      hfss_size(2-i)=int( hfss_FOV[2-i]/hfss_res[2-i]+0.5);
    }
    ODINLOG(odinlog,normalDebug) << "hfss_minpos=" << hfss_minpos << STD_endl;
    ODINLOG(odinlog,normalDebug) << "hfss_maxpos=" << hfss_maxpos << STD_endl;
    ODINLOG(odinlog,normalDebug) << "hfss_res=" << hfss_res << STD_endl;
    ODINLOG(odinlog,normalDebug) << "hfss_FOV=" << hfss_FOV << STD_endl;
    ODINLOG(odinlog,normalDebug) << "hfss_size=" << hfss_size << STD_endl;


    // Fill Grid
    data.resize(1,hfss_size(0),hfss_size(1),hfss_size(2)); data=0.0;
    TinyVector<int,4> index; index(0)=0;
    TinyVector<float,3> pos;
    float val;
    while(ifs >> pos[2] >> pos[1] >> pos[0] >> val) {
      for(int i=0; i<3; i++) index(1+i)=int((scale_factor*pos[i]-hfss_minpos[i])/hfss_res[i]);
      data(index)=val;
    }

    // Adjust protocol
    prot.geometry.set_Mode(voxel_3d)
    .set_FOV(sliceDirection, hfss_FOV[0])
    .set_FOV(phaseDirection, hfss_FOV[1])
    .set_FOV(readDirection,  hfss_FOV[2])
    .set_offset(sliceDirection, 0.5*(hfss_minpos[0]+hfss_maxpos[0]))
    .set_offset(phaseDirection, 0.5*(hfss_minpos[1]+hfss_maxpos[1]))
    .set_offset(readDirection,  0.5*(hfss_minpos[2]+hfss_maxpos[2]));

    return hfss_size(0);
  }
};

#endif

//////////////////////////////////////////////////////////////

void register_hfss_format() {
#ifndef STREAM_REPLACEMENT
  static HFSSFormat hf;
  hf.register_format();
#endif
}

