/***************************************************************************
                          filter_resize.h  -  description
                             -------------------
    begin                : Fri Feb 1 2008
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef FILTER_RESIZE_H
#define FILTER_RESIZE_H

#include <odindata/filter_step.h>

class FilterResize : public FilterStep {

  LDRint newsize[3];

  STD_string label() const {return "resize";}
  STD_string description() const {return "Spatial resize of image data";}
  bool process(Data<float,4>& data, Protocol& prot) const;
  FilterStep*  allocate() const {return new FilterResize();}
  void init();
};

/////////////////////////////////////////////////////////////////////////

class FilterResample : public FilterStep {

  LDRint newsize;

  STD_string label() const {return "resample";}
  STD_string description() const {return "Temporal resize of image data";}
  bool process(Data<float,4>& data, Protocol& prot) const;
  FilterStep*  allocate() const {return new FilterResample();}
  void init();
};

/////////////////////////////////////////////////////////////////////////

class FilterIsotrop : public FilterStep {
  LDRfloat size;
  STD_string label() const {return "isotrop";}
  STD_string description() const {return "make image voxels isotrop through interpolation (image geometry will not change)";}
  bool process(Data<float,4>& data, Protocol& prot) const;
  FilterStep*  allocate() const {return new FilterIsotrop();}
  void init();
};

#endif
