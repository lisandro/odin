/***************************************************************************
                          filter_lowpass.h  -  description
                             -------------------
    begin                : Thu Apr 22 2010
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef FILTER_LOWPASS_H
#define FILTER_LOWPASS_H

#include <odindata/filter_step.h>

class FilterLowPass : public FilterStep {

  LDRfloat freq;


  STD_string label() const {return "lowpass";}
  STD_string description() const {return "Lowpass filtering";}
  bool process(Data<float,4>& data, Protocol& prot) const;
  FilterStep*  allocate() const {return new FilterLowPass();}
  void init();
};

#endif
