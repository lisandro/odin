#include "filter_cluster.h"

typedef STD_list< STD_list<unsigned int> > ClusterList;

/////////////////////////////////////////////////////////////////////

void print_clusterlist(const ClusterList& clustelist) {
  Log<Filter> odinlog("","print_clusterlist");
  unsigned int i=0;
  for(ClusterList::const_iterator it=clustelist.begin(); it!=clustelist.end(); ++it) {
    ODINLOG(odinlog,normalDebug) << i << " : ";
    for(STD_list<unsigned int>::const_iterator it2=it->begin(); it2!=it->end(); ++it2) {
      ODINLOG(odinlog,normalDebug) << *it2 << " ";
    }
    ODINLOG(odinlog,normalDebug) << STD_endl;
    i++;
  }
}

/////////////////////////////////////////////////////////////////////

bool FilterCluster::process(Data<float,4>& data, Protocol& prot) const {
  Log<Filter> odinlog(c_label(),"process");

  Range all=Range::all();

  TinyVector<int,4> shape=data.shape();
  TinyVector<int,3> spatshape(shape(sliceDim), shape(phaseDim), shape(readDim));
  ODINLOG(odinlog,normalDebug) << "shape=" << shape << STD_endl;

  Data<unsigned int, 3> indexarray(spatshape); // linear index in every voxel
  for(unsigned int i=0; i<indexarray.size(); i++) {
    TinyVector<int,3> index=indexarray.create_index(i);
    indexarray(index)=i;
  }

  STD_vector<TinyVector<int,3> > next_neighb;
  int n=1;
  for(int k=-n; k<=n; k++) {
    for(int j=-n; j<=n; j++) {
      for(int i=-n; i<=n; i++) {
        int abssum=abs(i)+abs(j)+abs(k);
        if(abssum<=1) { // only adjacent voxels (with surface-to-surface contact)
          next_neighb.push_back(TinyVector<int,3>( k, j, i));
        }
      }
    }
  }
  unsigned int nn=next_neighb.size();


  Data<float,4> outdata(shape);
  outdata=0.0;

  Data<float,3> voldata(spatshape);

  for(int irep=0; irep<shape(timeDim); irep++) {

    ClusterList clusterlist;

    voldata(all,all,all)=data(irep,all,all,all);

    TinyVector<int,3> centerindex;
    TinyVector<int,3> neighbindex;
    for(unsigned int i=0; i<voldata.size(); i++) {
      centerindex=voldata.create_index(i);
      if(voldata(centerindex)>0.0) {

        STD_list<unsigned int> indexlist;

        // find next neighbours
        for(unsigned int in=0; in<nn; in++) {
          neighbindex=centerindex+next_neighb[in];
          if(neighbindex(0)>=0 && neighbindex(0)<spatshape(0)
          && neighbindex(1)>=0 && neighbindex(1)<spatshape(1)
          && neighbindex(2)>=0 && neighbindex(2)<spatshape(2) ) {
            if(voldata(neighbindex)>0.0) {
              indexlist.push_back(indexarray(neighbindex));
            }
          }
        }
        clusterlist.push_back(indexlist); // add new cluster

      }
    }

//  print_clusterlist(clusterlist);

    ODINLOG(odinlog,normalDebug) << clusterlist.size() << " initial clusters" << STD_endl;

    for(ClusterList::iterator it=clusterlist.begin(); it!=clusterlist.end(); ++it) {

      // sort and make unique before going through the elements
      it->sort();
      it->unique();

      ODINLOG(odinlog,normalDebug) << "clustersize=" << it->size() << STD_endl;

      for(STD_list<unsigned int>::const_iterator it2=it->begin(); it2!=it->end(); ++it2) {

        unsigned int i=*it2; // find this index in following clusters
        ODINLOG(odinlog,normalDebug) << "finding index " << i << STD_endl;

        ClusterList::iterator nextcluster=it; ++nextcluster;
        for(ClusterList::iterator itf=nextcluster; itf!=clusterlist.end(); ++itf) {

          bool found_in_cluster=false;
          for(STD_list<unsigned int>::const_iterator it3=itf->begin(); it3!=itf->end(); ++it3) {
            if(i==(*it3)) {
              found_in_cluster=true;
              break;
            }
          }
          if(found_in_cluster) {
            it->merge(*itf); // sort into list, itf will be emptied
            it->unique();
            ODINLOG(odinlog,normalDebug) << "found " << i << " ..." << STD_endl;
          } else {
            ODINLOG(odinlog,normalDebug) << "not found " << i << " ..." << STD_endl;
          }
        }
      }
    }


    ClusterList clusterlist_sort;
    for(ClusterList::iterator it=clusterlist.begin(); it!=clusterlist.end(); ++it) {
      if(it->size()) {

        STD_list<unsigned int> indexlist;
        indexlist.push_back(it->size()); // store size of cluster as first element for sorting

        for(STD_list<unsigned int>::const_iterator it2=it->begin(); it2!=it->end(); ++it2) {
          indexlist.push_back(*it2);
        }

        clusterlist_sort.push_back(indexlist);

      }

    }
    clusterlist_sort.sort();

//  print_clusterlist(clusterlist_sort);



    unsigned int ncluster=clusterlist_sort.size();
    unsigned int iclust=0;
    for(ClusterList::iterator it=clusterlist_sort.begin(); it!=clusterlist_sort.end(); ++it) {
      for(STD_list<unsigned int>::const_iterator it2=it->begin(); it2!=it->end(); ++it2) {
        if(it2!=it->begin()) { // omit 1st element which contains list size
          TinyVector<int,3> spatindex=voldata.create_index(*it2);
          outdata(irep,spatindex(0),spatindex(1),spatindex(2))=ncluster-iclust;
        }
      }
      iclust++;
    }

  }

  data.reference(outdata);

  return true;
}
