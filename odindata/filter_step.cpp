#include "filter.h"
#include "filter_step.h"

#include "filter_align.h"
#include "filter_clip.h"
#include "filter_cluster.h"
#include "filter_convolve.h"
#include "filter_detrend.h"
#include "filter_edit.h"
#include "filter_inv.h"
#include "filter_lowpass.h"
#include "filter_mask.h"
#include "filter_merge.h"
#include "filter_nan.h"
#include "filter_resize.h"
#include "filter_rot.h"
#include "filter_flip.h"
#include "filter_range.h"
#include "filter_reduction.h"
#include "filter_reslice.h"
#include "filter_scale.h"
#include "filter_shift.h"
#include "filter_slicetime.h"
#include "filter_tile.h"
#include "filter_splice.h"

#include <tjutils/tjlog_code.h>

#include <odindata/step_code.h>

// Instantiate template classes
template class Step<FilterStep>;
template class StepFactory<FilterStep>;

const char* Filter::get_compName() {return "Filter";}
LOGGROUNDWORK(Filter);


void FilterStep::create_templates(STD_list<FilterStep*>& result) {

  result.push_back(new FilterAlign());

  result.push_back(new FilterMin());
  result.push_back(new FilterMax());
  result.push_back(new FilterTypeMax());
  result.push_back(new FilterTypeMin());

  result.push_back(new FilterDeTrend());
  result.push_back(new FilterSliceTime());
  result.push_back(new FilterLowPass());
  result.push_back(new FilterConvolve());

  result.push_back(new FilterCluster());

  result.push_back(new FilterGenMask());
  result.push_back(new FilterAutoMask());
  result.push_back(new FilterQuantilMask());
  result.push_back(new FilterSphereMask());
  result.push_back(new FilterUseMask());

  result.push_back(new FilterNaN());
  result.push_back(new FilterRot());

  result.push_back(new FilterFlip<sliceDim>());
  result.push_back(new FilterFlip<phaseDim>());
  result.push_back(new FilterFlip<readDim>());

  result.push_back(new FilterRange<timeDim>());
  result.push_back(new FilterRange<sliceDim>());
  result.push_back(new FilterRange<phaseDim>());
  result.push_back(new FilterRange<readDim>());

  result.push_back(new FilterScale());
  result.push_back(new FilterShift());
  result.push_back(new FilterTimeShift());
  result.push_back(new FilterReSlice());
  result.push_back(new FilterSwapdim());

  result.push_back(new FilterResize());
  result.push_back(new FilterResample());
  result.push_back(new FilterIsotrop());

  result.push_back(new FilterTile());

  result.push_back(new FilterEdit());

  result.push_back(new FilterInvert());

  result.push_back(new FilterMerge());
  result.push_back(new FilterSplice());

  result.push_back(new FilterReduction<minip>());
  result.push_back(new FilterReduction<maxip>());
  result.push_back(new FilterReduction<meanp>());
  result.push_back(new FilterReduction<sump>());

}

bool FilterStep::process(Data<float,4>& data, Protocol& prot)const{
  Log<Filter> odinlog("FilterStep","process");
  ODINLOG(odinlog,errorLog) << "process seems not to be implemented for " << label() << STD_endl;
  return false;
}

bool FilterStep::process(FileIO::ProtocolDataMap& pdmap)const{
  Log<Filter> odinlog("FilterStep","process");
  bool ret=true;
  FileIO::ProtocolDataMap pdmap_out;
  while(!pdmap.empty()){
    STD_pair<Protocol, Data<float,4> > pdpair=*pdmap.begin();
    pdmap.erase(pdmap.begin());//in case we modify the protocol it is better to completly remove the pd-pair and insert it cleanly afterwards
    if(process(pdpair.second,pdpair.first)){
      pdmap_out.insert(pdpair);//.. and insert it into the result-map
    } else {
      int number;STD_string desc;
      pdpair.first.study.get_Series(desc,number);
      ODINLOG(odinlog,errorLog) << "processing " << label() << " on S" << number << " failed"<< STD_endl;
      ret=false;
    }
  }
  pdmap=pdmap_out;
  return ret;
}
