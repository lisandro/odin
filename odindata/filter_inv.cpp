#include "filter_inv.h"



bool FilterInvert::process(Data<float,4>& data, Protocol& prot) const {
  data=max(data)-data;
  return true;
}
