//
// C++ Interface: filter_reslice
//
// Description:
//
//
// Author:  <>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef FILTER_RESLICE_H
#define FILTER_RESLICE_H

#include <odindata/filter_step.h>

class FilterReSlice : public FilterStep
{
  LDRenum orient;
  STD_string label() const {return "reslice";}
  STD_string description() const {return "reslices the image to a given orientation";}
  bool process(Data<float,4>& data, Protocol& prot)const;
  FilterStep*  allocate() const {return new FilterReSlice();}
  void init();
};

/////////////////////////////////////////////////////////////////////////////

class FilterSwapdim : public FilterStep
{
  LDRstring newread,newphase,newslice;

  STD_string label() const {return "swapdim";}
  STD_string description() const {return "swap/reflect dimensions by specifying a direction triple with optional reflection sign appended";}
  bool process(Data<float,4>& data, Protocol& prot)const;
  FilterStep*  allocate() const {return new FilterSwapdim();}
  static bool selChannel(STD_string name, direction &dir, int& sign);
  void init();
};

#endif
