//
// C++ Interface: filter
//
// Description: 
//
//
// Author:  <Enrico Reimer>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef FILTER_H
#define FILTER_H

#include <odindata/fileio.h>

/**
  * @addtogroup odindata
  * @{
  */


struct FilterChainData; // forward declaration

///////////////////////////////////////////////////////////////////////////////////

/**
  * A chain of filter functors.
  */
class FilterChain {

 public:

/**
  * Create empty filter chain
  */
  FilterChain();

/**
  * Destructor
  */
  ~FilterChain();

/**
  * Create filter chain from command line string.
  */
  FilterChain(const STD_string& argstr);

/**
  * Create filter chain from command line.
  */
  FilterChain(int argc,char *argv[]);

/**
  * Initialize filter chain using given command-line string.
  */
  bool init(const STD_string& argstr) {return create(tokens(argstr,' ','\"','\"'));}

/**
  * Apply filter chain to all elements in 'pdmap'.
  */
  bool apply(FileIO::ProtocolDataMap& pdmap) const;

/**
  * Apply filter chain to protocol-data pair.
  */
  bool apply(Protocol& prot, Data<float,4>& data) const;

/**
  * Returns documention for command line.
  */
  STD_string get_cmdline_usage(const STD_string& lineprefix) const;


 private:
  FilterChainData* filterchaindata;

  bool create(const svector& args);

};


/** @}
  */


#endif
