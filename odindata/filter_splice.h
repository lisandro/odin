/***************************************************************************
                          filter_splice.h  -  description
                             -------------------
    begin                : Fri Feb 17 2012
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef FILTER_SPLICE_H
#define FILTER_SPLICE_H

#include <odindata/filter_step.h>

class FilterSplice: public FilterStep {
  LDRenum dir;
  STD_string label() const {return "splice";}
  STD_string description() const {return "splices the image in the given direction";}
  bool process(FileIO::ProtocolDataMap& pdmap)const;
  FilterStep*  allocate() const {return new FilterSplice();}
  void init();
};

#endif
