//
// C++ Implementation: fileio_3Db
//
// Description:
//
//
// Author:  <>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "fileio.h"

//////////////////////////////////////////////////////////////

struct Iris3DFormat : public FileFormat {

  struct i3Dblock {
    short nx, ny, nz, veclen;
    float x0, y0, z0, dx, dy, dz;
  };

  STD_string description() const {return "Iris3D binary data";}
  svector suffix() const  {
    svector result; result.resize(1);
    result[0]="3db";
    return result;
  }
  svector dialects() const {return svector();}

  int read(Data<float,4>& data, const STD_string& filename, const FileReadOpts& opts, Protocol& prot) {
    Log<FileIO> odinlog("Iris3DFormat","read");
    ODINLOG(odinlog,errorLog) << "Read of Iris3D not yet supported, sorry"<< STD_endl;
    return -1;
  }

  int write(const Data<float,4>& data, const STD_string& filename, const FileWriteOpts& opts, const Protocol& prot) {
    Log<FileIO> odinlog("Iris3DFormat","write");

    FILE *out=ODIN_FOPEN( filename.c_str(), "w");
    if(!out){
      ODINLOG(odinlog,errorLog) << "could not open "<< filename <<" for writing"<< STD_endl;
      return -1;
    }

    Data<float,4> data_copy(data);
    const TinyVector<int,4> data_shape=data.shape();

    const dvector center(prot.geometry.get_center());

    const i3Dblock head={
      short(data_shape[3]),short(data_shape[2]),short(data_shape[1]),
      1,
      float(center[0]),float(center[2]),float(center[2]),
      voxel_extent(prot.geometry,readDirection, data.extent(3)),
      voxel_extent(prot.geometry,phaseDirection, data.extent(2)),
      float(prot.geometry.get_sliceDistance())
    };

    fwrite(&head,sizeof(i3Dblock),1,out);
    size_t cnt=fwrite(data_copy.c_array(),sizeof(float),product(data_shape),out);
    fclose(out);
    return int(cnt)==product(data_shape);
  }

};


//////////////////////////////////////////////////////////////

void register_Iris3D_format() {
  static Iris3DFormat  iris;
  iris.register_format();
}

