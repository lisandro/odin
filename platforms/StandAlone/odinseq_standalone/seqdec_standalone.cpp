#include "seqdec_standalone.h"

SeqDecouplingStandalone::SeqDecouplingStandalone(const SeqDecouplingStandalone& sds) {
  set_label(sds.get_label());
}


bool SeqDecouplingStandalone::prep_driver(double decdur, int channel, float decpower, const STD_string& program, double pulsedur) {
  Log<SeqStandAlone> odinlog(this,"prep_driver");

  curve.resize(4);
  curve.channel=B1re_plotchan;
  curve.label=get_label().c_str();
  curve.x[0]=0.0;
  curve.y[0]=0.0;
  curve.x[1]=ON_OFF_DELAY;
  curve.y[1]=decpower;
  curve.x[2]=decdur-ON_OFF_DELAY;
  curve.y[2]=decpower;
  curve.x[3]=decdur;
  curve.y[3]=0.0;

  if(dump2console) STD_cout << curve << STD_endl;

  return true;
}



void SeqDecouplingStandalone::event(eventContext& context, double start) const {
  Log<SeqStandAlone> odinlog(this,"event");
  ODINLOG(odinlog,normalDebug) << "start=" << start << STD_endl;
  append_curve2plot(start,&curve,current_rf_rec_freq,current_rf_rec_phase);
}
