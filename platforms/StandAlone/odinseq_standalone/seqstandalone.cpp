#include "seqstandalone.h"

#include "seqacq_standalone.h"
#include "seqacqepi_standalone.h"
#include "seqdec_standalone.h"
#include "seqdelay_standalone.h"
#include "seqfreq_standalone.h"
#include "seqgradchan_standalone.h"
#include "seqgradtrapez_standalone.h"
#include "seqlist_standalone.h"
#include "seqcounter_standalone.h"
#include "seqparallel_standalone.h"
#include "seqphase_standalone.h"
#include "seqpuls_standalone.h"
#include "seqtrigg_standalone.h"

#include <tjutils/tjlog_code.h>
#include <tjutils/tjhandler_code.h>


const char* SeqStandAlone::get_compName() {return "StandAlone";}
LOGGROUNDWORK(SeqStandAlone)



int SeqStandAlone::process(int argc, char *argv[]) {
#ifndef NO_CMDLINE
  Log<SeqStandAlone> odinlog("SeqStandAlone","process");
  SeqMethodProxy method;
  SeqPlatformProxy pfinterface;
  char value[ODIN_MAXCHAR];
  bool valid_command=false;

  STD_string action(argv[1]);

  if(action=="plot") {
    if(getCommandlineOption(argc,argv,"-p",value,ODIN_MAXCHAR)) method->load_protocol(value);
    pfinterface.set_current_platform(standalone); // overwrite result from load_protocol
    dump2console=true;
    if(method->prepare()) {
      eventContext context;
      context.action=seqRun;
      method->event(context);
      STD_cout << STD_endl;
    } else ODINLOG(odinlog,errorLog) << method->get_label() << +"->prepare() failed" << STD_endl;
    dump2console=false;
    valid_command=true;
  }

  if(action=="simulate") {

    STD_string samplefile;
    if(getCommandlineOption(argc,argv,"-s",value,ODIN_MAXCHAR)) samplefile=value;
    else {
      ODINLOG(odinlog,errorLog) << "(simulate): No virtual sample file specified" << STD_endl;
      return -1;
    }
    if(getCommandlineOption(argc,argv,"-p",value,ODIN_MAXCHAR)) method->load_protocol(value);
    pfinterface.set_current_platform(standalone); // overwrite result from load_protocol

    while(getCommandlineOption(argc,argv,"-m",value,ODIN_MAXCHAR)) {
      svector toks(tokens(value,'='));
      if(toks.size()!=2) ODINLOG(odinlog,errorLog) << "syntax error in " << value << STD_endl;
      else method->set_sequenceParameter(toks[0],toks[1]);
    }


    if(!method->prepare())  {
      ODINLOG(odinlog,errorLog) << "(simulate): method->prepare() failed" << STD_endl;
      return -1;
    }
    if(!method->prep_acquisition())  {
      ODINLOG(odinlog,errorLog) << "(simulate): method->prep_acquisition() failed" << STD_endl;
      return -1;
    }

    plotData->get_opts(true,true).parse_cmdline_options(argc,argv);

    ProgressDisplayConsole pdc;
    ProgressMeter progmeter(pdc);

    create_plot_events(&progmeter);

    STD_string fidfile=systemInfo->get_scandir()+get_rawfile();

    plotData->simulate(fidfile,samplefile,&progmeter,0);

    method->write_meas_contex(systemInfo->get_scandir());

    plotData->get_opts(true,true).write(systemInfo->get_scandir()+"simopts");

    valid_command=true;
  }

//  if(!valid_command) return -1;

  return int(valid_command);

#endif
  return 0;
}


SeqCmdlineActionList SeqStandAlone::get_actions_usage() const {
  SeqCmdlineActionList result;
#ifndef NO_CMDLINE

  SeqCmdlineAction plot("plot","Print plotting events to the console.");
  plot.add_opt_arg("p","The file with the measurement protocol");
  result.push_back(plot);

  SeqCmdlineAction simulate("simulate","Creates a virtual MR signal by simulating the sequence.");
  simulate.opt_args=plotData->get_opts(true,true).get_cmdline_options();
  simulate.add_req_arg("s","The virtual sample file");
  simulate.add_opt_arg("p","The file with the measurement protocol");
  simulate.add_opt_arg("m","protcol_parameter=value");
  result.push_back(simulate);


#endif
  return result;
}

void SeqStandAlone::pre_event(eventContext& context) const {
  Log<SeqStandAlone> odinlog(this,"pre_event");
  ODINLOG(odinlog,normalDebug) << "resetting plotData" << STD_endl;
  plotData->reset();
  new_plot_frame(context);
}

void SeqStandAlone::post_event(eventContext& context) const {
  flush_plot_frame(context);

  if(dump2console) {
    double totaldur=plotData->get_total_duration();

    STD_cout << "---------- Curves: ---------------------" << STD_endl;
    STD_list<Curve4Qwt>::const_iterator begin,end;
    plotData->get_curves(begin, end, 0.0, totaldur, totaldur);
    for(STD_list<Curve4Qwt>::const_iterator it=begin; it!=end; ++it) {
      if(it->size) {
        STD_cout << it->x[0] << "\t" <<  it->label;
        if(it->has_freq_phase) STD_cout << "\tfreq/phase=" << it->freq << "/" << it->phase;
        if(it->gradmatrix) STD_cout << "\tgradmatrix=" << it->gradmatrix->print();
        STD_cout << STD_endl;
      }
    }

    STD_cout << "---------- Markers: --------------------" << STD_endl;
    STD_list<Marker4Qwt>::const_iterator markbegin,markend;
    plotData->get_markers(markbegin, markend, 0.0, totaldur);
    for(STD_list<Marker4Qwt>::const_iterator markit=markbegin; markit!=markend; ++markit) {
      STD_cout << markit->x << "\t" <<  markit->label << STD_endl;
    }

  }
}

fvector SeqStandAlone::get_acq_channel_scale_factors() const {
  fvector result(numof_rec_channels());
  result=1.0;
  return result;
}

void SeqStandAlone::new_plot_frame(eventContext& context) const {
  context.elapsed=0.0;
}

void SeqStandAlone::flush_plot_frame(eventContext& context) const {
  plotData->flush_frame(context.elapsed);
  context.elapsed=0.0;
}


SeqAcqDriver*        SeqStandAlone::create_driver(SeqAcqDriver*) const {return new SeqAcqStandAlone;}
SeqEpiDriver*        SeqStandAlone::create_driver(SeqEpiDriver*) const {return new SeqEpiDriverDefault;}
SeqDecouplingDriver* SeqStandAlone::create_driver(SeqDecouplingDriver*) const {return new SeqDecouplingStandalone;}
SeqDelayDriver*      SeqStandAlone::create_driver(SeqDelayDriver*) const {return new SeqDelayStandAlone;}
SeqDelayVecDriver*   SeqStandAlone::create_driver(SeqDelayVecDriver*) const {return new SeqDelayVecStandAlone;}
SeqFreqChanDriver*   SeqStandAlone::create_driver(SeqFreqChanDriver*) const {return new SeqFreqChanStandAlone;}
SeqGradChanDriver*   SeqStandAlone::create_driver(SeqGradChanDriver*) const {return new SeqGradChanStandAlone;}
SeqGradChanParallelDriver*   SeqStandAlone::create_driver(SeqGradChanParallelDriver*) const {return new SeqGradChanParallelStandAlone;}
SeqGradTrapezDriver* SeqStandAlone::create_driver(SeqGradTrapezDriver*) const {return new SeqGradTrapezDefault;}
SeqListDriver*       SeqStandAlone::create_driver(SeqListDriver*) const {return new SeqListStandAlone;}
SeqCounterDriver*    SeqStandAlone::create_driver(SeqCounterDriver*) const {return new SeqCounterStandAlone;}
SeqParallelDriver*   SeqStandAlone::create_driver(SeqParallelDriver*) const {return new SeqParallelStandAlone;}
SeqPhaseDriver*      SeqStandAlone::create_driver(SeqPhaseDriver*) const {return new SeqPhaseStandAlone;}
SeqPulsDriver*       SeqStandAlone::create_driver(SeqPulsDriver*) const {return new SeqPulsStandAlone;}
SeqTriggerDriver*    SeqStandAlone::create_driver(SeqTriggerDriver*) const {return new SeqTriggerStandAlone;}

unsigned int SeqStandAlone::numof_rec_channels() const {
  return plotData->numof_rec_channels();
}

void SeqStandAlone::append_curve2plot(double starttime, const SeqPlotCurve* curve_ptr) const {
  plotData->append_curve(starttime,curve_ptr);
}

void SeqStandAlone::append_curve2plot(double starttime, const SeqPlotCurve* curve_ptr, double freq, double phase) const {
  plotData->append_curve(starttime,curve_ptr,freq,phase);
}

void SeqStandAlone::append_curve2plot(double starttime, const SeqPlotCurve* curve_ptr, const RotMatrix* gradrotmatrix) const {
  plotData->append_curve(starttime,curve_ptr,gradrotmatrix);
}


void SeqStandAlone::set_systemInfo_defaults() {
  Log<SeqStandAlone> odinlog(this,"set_systemInfo_defaults");
  SystemInterface::systemInfo_platform[standalone]->platformstr=get_label();

  // hide unused settings
  SystemInterface::systemInfo_platform[standalone]->reference_gain.set_parmode(hidden);
  SystemInterface::systemInfo_platform[standalone]->transmit_coil_name.set_parmode(hidden);
  SystemInterface::systemInfo_platform[standalone]->delay_rastertime.set_parmode(hidden);
  SystemInterface::systemInfo_platform[standalone]->rf_rastertime.set_parmode(hidden);
  SystemInterface::systemInfo_platform[standalone]->acq_rastertime.set_parmode(hidden);
  SystemInterface::systemInfo_platform[standalone]->grad_rastertime.set_parmode(hidden);
  SystemInterface::systemInfo_platform[standalone]->min_grad_rastertime.set_parmode(hidden);
  SystemInterface::systemInfo_platform[standalone]->inter_grad_delay.set_parmode(hidden);
  SystemInterface::systemInfo_platform[standalone]->max_rf_samples.set_parmode(hidden);
  SystemInterface::systemInfo_platform[standalone]->max_grad_samples.set_parmode(hidden);
  SystemInterface::systemInfo_platform[standalone]->datatype.set_parmode(hidden);
  SystemInterface::systemInfo_platform[standalone]->grad_reson_center.set_filemode(exclude).set_parmode(hidden);
  SystemInterface::systemInfo_platform[standalone]->grad_reson_width.set_filemode(exclude).set_parmode(hidden);

}

SeqPlotDataAbstract* SeqStandAlone::get_plot_data() {return plotData.unlocked_ptr();}

bool SeqStandAlone::create_plot_events(ProgressMeter* progmeter) {
  SeqMethodProxy method;

  eventContext context;

  // count events first
  if(progmeter) {
    context.action=countEvents;
    unsigned int eventscount=method->SeqMethod::event(context);
    context.event_progmeter=progmeter;
    context.event_progmeter->new_task(eventscount,"Creating sequence plot");
  }

  context.action=seqRun;
  method->SeqMethod::event(context);

  return true;
}



void SeqStandAlone::init_static() {
  plotData.init("plotData");
}

void SeqStandAlone::destroy_static() {
  plotData.destroy();
}

template class SingletonHandler<SeqPlotData,false>;
SingletonHandler<SeqPlotData,false> SeqStandAlone::plotData;

double SeqStandAlone::current_rf_rec_freq=0.0;
double SeqStandAlone::current_rf_rec_phase=0.0;

const RotMatrix* SeqStandAlone::current_rotmatrix=0;

bool SeqStandAlone::dump2console=false;


EMPTY_TEMPL_LIST bool StaticHandler<SeqStandAlone>::staticdone=false;
