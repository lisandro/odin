/***************************************************************************
                          seqcounter_standalone.h  -  description
                             -------------------
    begin                : Sat Apr 17 2004
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQCOUNTER_STANDALONE_H
#define SEQCOUNTER_STANDALONE_H

#include <odinseq/seqcounter.h>


//////////////////////////////////////////////////////////////////


/**
  * @ingroup odinseq_internals
  * The Standalone driver for counter objects
  */
class SeqCounterStandAlone : public SeqCounterDriver {

 public:

  SeqCounterStandAlone() {}

  SeqCounterStandAlone(const SeqCounterStandAlone& sls) {set_label(sls.get_label());}

  ~SeqCounterStandAlone() {}


  // overloading virtual functions from SeqCounterDriver
  bool prep_driver() {return true;}
  void update_driver(const SeqCounter* counter, const SeqObjList* seqlist, const List<SeqVector,const SeqVector*,const SeqVector&>* vectors) const {};
  double get_preduration () const {return 0.0;}
  double get_postduration() const {return 0.0;}
  double get_preduration_inloop() const  {return 0.0;}
  double get_postduration_inloop() const {return 0.0;}
  bool create_program(programContext& context, const STD_string& loopkernel) const {return true;}
  STD_string get_program_head(programContext& context, const STD_string& loopkernel, unsigned int times) const  {return "";}
  STD_string get_program_tail(programContext& context, const STD_string& loopkernel, unsigned int times) const  {return "";}
  STD_string get_program_head_unrolled(programContext& context, unsigned int index) const {return "";}
  STD_string get_program_iterator(programContext& context) const {return "";}
  void pre_vecprepevent (eventContext& context) const {}
  void post_vecprepevent(eventContext& context, int repcounter) const {}
  void outdate_cache() const {}
  bool unroll_program(const SeqCounter* counter, const SeqObjList* seqlist, const List<SeqVector,const SeqVector*,const SeqVector&>* vectors, programContext& context) const {return false;}
  SeqCounterDriver* clone_driver() const {return new SeqCounterStandAlone(*this);}

  // overloading virtual functions from SeqDriverBase
  odinPlatform get_driverplatform() const {return standalone;}
};


#endif

