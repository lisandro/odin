/***************************************************************************
                          seqtrigg_standalone.h  -  description
                             -------------------
    begin                : Mon Apr 12 2004
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQTRIGG_STANDALONE_H
#define SEQTRIGG_STANDALONE_H

#include <odinseq/seqtrigg.h>
#include "seqstandalone.h"

//////////////////////////////////////////////////////////////////


/**
  * @ingroup odinseq_internals
  * The Standalone driver for external trigger events
  */
class SeqTriggerStandAlone : public SeqTriggerDriver, public SeqStandAlone  {

 public:

  SeqTriggerStandAlone() {}

  SeqTriggerStandAlone(const SeqTriggerStandAlone& sts) {set_label(sts.get_label());}

  ~SeqTriggerStandAlone() {}


  // overloading virtual functions from SeqTriggerDriver
  double get_postduration() const {return 0.0;}
  bool prep_exttrigger(double duration);
  bool prep_halttrigger();
  bool prep_snaptrigger(const STD_string& snapshot_fname);
  bool prep_resettrigger();
  void event(eventContext& context, double startelapsed) const;
  STD_string get_program(programContext& context) const {return "";}
  SeqTriggerDriver* clone_driver() const {return new SeqTriggerStandAlone(*this);}

  // overloading virtual functions from SeqDriverBase
  odinPlatform get_driverplatform() const {return standalone;}

 private:
  SeqPlotCurve trigg_marker;
};

#endif

