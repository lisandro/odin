/***************************************************************************
                          seqacqepi_standalone.h  -  description
                             -------------------
    begin                : Fri Jul 23 2004
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQACQEPI_STANDALONE_H
#define SEQACQEPI_STANDALONE_H

#include <odinseq/seqacqepi.h>

// empty, because SeqEpiDriverDefault is used

#endif
