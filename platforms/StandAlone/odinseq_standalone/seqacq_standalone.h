/***************************************************************************
                          seqacq_standalone.h  -  description
                             -------------------
    begin                : Fri Jul 23 2004
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQACQ_STANDALONE_H
#define SEQACQ_STANDALONE_H

#include <odinseq/seqacq.h>
#include "seqstandalone.h"

/**
  * @ingroup odinseq_internals
  * The Standalone driver for acquisition windows
  */
class SeqAcqStandAlone : public SeqAcqDriver, public SeqStandAlone {

 public:
  SeqAcqStandAlone() {}
  ~SeqAcqStandAlone() {}

  SeqAcqStandAlone(const SeqAcqStandAlone& sas);


  // overloading virtual functions from SeqAcqDriver
  double adjust_sweepwidth(double desired_sweep_widht) const {return desired_sweep_widht;}
  bool prep_driver(kSpaceCoord& recoindex, double sweepwidth,unsigned int nAcqPoints, double acqcenter, int freqchannel);
  double get_predelay() const {return 0.0;}
  double get_postdelay(double) const {return 0.0;}
  void event(eventContext& context, double start) const;
  STD_string get_program(programContext& context, unsigned  int phaselistindex) const {return "";}
  STD_string get_instr_label() const {return "";}
  unsigned int get_numof_channels() const {return SeqStandAlone::numof_rec_channels();}
  SeqAcqDriver* clone_driver() const {return new SeqAcqStandAlone(*this);}


  // overloading virtual functions from SeqDriverBase
  odinPlatform get_driverplatform() const {return standalone;}

 private:
  SeqPlotCurve adc_curve;
  SeqPlotCurve adc_curve_nomark;
  SeqPlotCurve endacq_mark;

};

#endif

