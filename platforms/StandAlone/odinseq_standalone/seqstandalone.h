/***************************************************************************
                          seqstandalone.h  -  description
                             -------------------
    begin                : Fri Jul 23 2004
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQSTANDALONE_H
#define SEQSTANDALONE_H

#include <tjutils/tjhandler.h>

#include <odinseq/seqplatform.h>
#include <odinseq/seqplot.h>


class SeqPlotCurve; // forward declaration
class SeqPlotData; // forward declaration

////////////////////////////////////////////////////////////////////

/**
  * @ingroup odinseq_internals
  *  Base class for all sequence objects that deal with functionality specific to the ODIN stand-alone platform
  */
class SeqStandAlone : public SeqPlatform, public StaticHandler<SeqStandAlone> {

 public:

  SeqStandAlone() {}
  SeqStandAlone(const PlatformRegistration&) {set_label("StandAlone");set_systemInfo_defaults();}

  // name of debugging component
  static const char* get_compName();

  // overloadded functions from SeqPlatform
  void init() {}
  void reset_before_prep() {}
  void prepare_measurement(unsigned int) {}
  SeqAcqDriver*        create_driver(SeqAcqDriver*) const;
  SeqEpiDriver*        create_driver(SeqEpiDriver*) const;
  SeqDecouplingDriver* create_driver(SeqDecouplingDriver*) const;
  SeqDelayDriver*      create_driver(SeqDelayDriver*) const;
  SeqDelayVecDriver*   create_driver(SeqDelayVecDriver*) const;
  SeqFreqChanDriver*   create_driver(SeqFreqChanDriver*) const;
  SeqGradChanDriver*   create_driver(SeqGradChanDriver*) const;
  SeqGradChanParallelDriver*   create_driver(SeqGradChanParallelDriver*) const;
  SeqGradTrapezDriver* create_driver(SeqGradTrapezDriver*) const;
  SeqListDriver*       create_driver(SeqListDriver*) const;
  SeqCounterDriver*    create_driver(SeqCounterDriver*) const;
  SeqParallelDriver*   create_driver(SeqParallelDriver*) const;
  SeqPhaseDriver*      create_driver(SeqPhaseDriver*) const;
  SeqPulsDriver*       create_driver(SeqPulsDriver*) const;
  SeqTriggerDriver*    create_driver(SeqTriggerDriver*) const;
  int process(int argc, char *argv[]);
  SeqCmdlineActionList get_actions_usage() const;
  void pre_event (eventContext& context) const;
  void post_event(eventContext& context) const;
  fvector get_acq_channel_scale_factors() const;
  STD_string get_program(programContext& context) const {return "";}
  STD_string get_rawdatatype() const {return TypeTraits::type2label(float(0));}
  STD_string get_rawfile() const {return "signal.float";}
  unsigned int get_rawheader_size() const {return 0;}
  STD_string get_image_proc() const {return "";}
  bool create_recoInfo() const {return true;}
  int write_rf_waveform (const STD_string& filename, const cvector& waveform) const {return -1;}
  int load_rf_waveform (const STD_string& filename, cvector& result) const {return -1;}
  int get_max_methodname_length() const {return -1;}
  void set_eventlogging(eventLogging loggflag) {}
  void set_idea_pars(void* pmp,void* psl,void* pse, bool onlineReco) {}
  SeqPlotDataAbstract* get_plot_data();
  bool create_plot_events(ProgressMeter* progmeter);


  // functions to initialize/delete static members by the StaticHandler template class
  static void init_static();
  static void destroy_static();

  unsigned int numof_rec_channels() const;

 protected:

  void append_curve2plot(double starttime, const SeqPlotCurve* curve_ptr) const;
  void append_curve2plot(double starttime, const SeqPlotCurve* curve_ptr, double freq, double phase) const;
  void append_curve2plot(double starttime, const SeqPlotCurve* curve_ptr, const RotMatrix* gradrotmatrix) const;

  void new_plot_frame(eventContext& context) const;
  void flush_plot_frame(eventContext& context) const;

  
  static double current_rf_rec_freq;
  static double current_rf_rec_phase;

  static const RotMatrix* current_rotmatrix;

  static bool dump2console;
  
 private:

  void set_systemInfo_defaults();

  static SingletonHandler<SeqPlotData,false> plotData;

};


#endif
