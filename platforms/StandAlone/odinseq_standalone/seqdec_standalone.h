/***************************************************************************
                          seqdec_standalone.h  -  description
                             -------------------
    begin                : Fri Jul 23 2004
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQDEC_STANDALONE_H
#define SEQDEC_STANDALONE_H


#include <odinseq/seqdec.h>
#include "seqstandalone.h"

#define ON_OFF_DELAY 1.0e-6

/**
  * @ingroup odinseq_internals
  * The Standalone driver for decoupling periods
  */
class SeqDecouplingStandalone : public SeqDecouplingDriver, public SeqStandAlone {

 public:

  SeqDecouplingStandalone() {}

  SeqDecouplingStandalone(const SeqDecouplingStandalone& sds);

  ~SeqDecouplingStandalone() {}


  // overloading virtual functions from SeqDecouplingDriver
  bool prep_driver(double decdur, int channel, float decpower, const STD_string& program, double pulsedur);
  void event(eventContext& context, double start) const;
  double get_preduration() const {return 0.0;}
  double get_postduration() const {return 0.0;}
  STD_string get_preprogram(programContext& context, const STD_string& iteratorcommand) const {return "";}
  STD_string get_postprogram(programContext& context) const {return "";}
  SeqDecouplingDriver* clone_driver() const {return new SeqDecouplingStandalone(*this);}

  // overloading virtual functions from SeqDriverBase
  odinPlatform get_driverplatform() const {return standalone;}

 private:
  SeqPlotCurve curve;

};

#endif

