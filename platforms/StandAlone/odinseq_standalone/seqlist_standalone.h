/***************************************************************************
                          seqlist_standalone.h  -  description
                             -------------------
    begin                : Sat Apr 17 2004
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQLIST_STANDALONE_H
#define SEQLIST_STANDALONE_H

#include <odinseq/seqlist.h>
#include "seqstandalone.h"

//////////////////////////////////////////////////////////////////


/**
  * @ingroup odinseq_internals
  * The Standalone driver for lists of sequence objects
  */
class SeqListStandAlone : public SeqListDriver, public SeqStandAlone {

 public:

  SeqListStandAlone() {}

  SeqListStandAlone(const SeqListStandAlone& sls) {set_label(sls.get_label());}

  ~SeqListStandAlone() {}


  // overloading virtual functions from SeqListDriver
  STD_string pre_program (programContext& context, const SeqRotMatrixVector* rotmats) const {return "";}
  STD_string post_program(programContext& context, const SeqRotMatrixVector* rotmats) const {return "";}
  STD_string get_itemprogram(const SeqTreeObj* item, programContext& context) const {return "";}
  void pre_event (eventContext& context, const RotMatrix* rotmatrix) const;
  void post_event(eventContext& context, const RotMatrix* rotmatrix) const;
  void pre_itemevent (const SeqTreeObj* item, eventContext& context) const;
  void post_itemevent(const SeqTreeObj* item, eventContext& context) const;
  bool prep_driver() {return true;}
  SeqListDriver* clone_driver() const {return new SeqListStandAlone(*this);}

  // overloading virtual functions from SeqDriverBase
  odinPlatform get_driverplatform() const {return standalone;}
};


#endif

