#include "seqtrigg_standalone.h"

bool SeqTriggerStandAlone::prep_exttrigger(double duration) {
  trigg_marker.label=get_label().c_str();
  trigg_marker.marklabel=markLabel[exttrigger_marker];
  trigg_marker.marker=exttrigger_marker;
  trigg_marker.marker_x=0.0;

  if(dump2console) STD_cout << trigg_marker << STD_endl;
  return true;
}

bool SeqTriggerStandAlone::prep_halttrigger() {
  trigg_marker.label=get_label().c_str();
  trigg_marker.marklabel=markLabel[halttrigger_marker];
  trigg_marker.marker=halttrigger_marker;
  trigg_marker.marker_x=0.0;

  if(dump2console) STD_cout << trigg_marker << STD_endl;
  return true;
}

bool  SeqTriggerStandAlone::prep_snaptrigger(const STD_string& snapshot_fname) {
  trigg_marker.label=snapshot_fname.c_str();
  trigg_marker.marklabel=markLabel[snapshot_marker];
  trigg_marker.marker=snapshot_marker;
  trigg_marker.marker_x=0.0;
  rmfile(snapshot_fname.c_str());

  if(dump2console) STD_cout << trigg_marker << STD_endl;
  return true;
}

bool  SeqTriggerStandAlone::prep_resettrigger() {
  trigg_marker.label="Magnetization Reset";
  trigg_marker.marklabel=markLabel[reset_marker];
  trigg_marker.marker=reset_marker;
  trigg_marker.marker_x=0.0;
  if(dump2console) STD_cout << trigg_marker << STD_endl;
  return true;
}

void SeqTriggerStandAlone::event(eventContext& context, double startelapsed) const {
  append_curve2plot(startelapsed,&trigg_marker);
}

