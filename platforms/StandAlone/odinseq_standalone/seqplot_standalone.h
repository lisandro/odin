/***************************************************************************
                          seqplot_standalone.h  -  description
                             -------------------
    begin                : Fri Jul 23 2004
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQPLOT_STANDALONE_H
#define SEQPLOT_STANDALONE_H

#include <tjutils/tjfeedback.h>
#include <tjutils/tjnumeric.h>

#include <odinseq/seqplot.h>


#define TIMECOURSE_HASH_INTERVAL 100
#define EXTRA_UPDOWN_ITERATIONS 5
#define EXTRA_TIMECOURSE_POINTS 2

/////////////////////////////////////////////////////

/**
  * @ingroup odinseq_internals
  *  Data structure to hold parameters for timecourse generation
  */
struct SeqTimecourseOpts : public LDRblock {
  SeqTimecourseOpts();

  LDRdouble EddyCurrentAmpl;
  LDRdouble EddyCurrentTimeConst;
};

/////////////////////////////////////////////////////

/**
  * @ingroup odinseq_internals
  *  Data structure to hold parameters for simulation
  */
struct SeqSimulationOpts : public LDRblock {
  SeqSimulationOpts();
  ~SeqSimulationOpts() {outdate_coil_cache();}

  LDRint SimThreads;

  LDRbool IntraVoxelMagnGrads;
  LDRbool MagnMonitor;

  LDRdouble ReceiverNoise;

  LDRfileName TransmitterCoil;
  LDRfileName ReceiverCoil;

  LDRtriple InitialMagnVector;

  CoilSensitivity* get_transm_coil() const {update_coil_cache(); return transm_coil;}
  CoilSensitivity* get_receiv_coil() const {update_coil_cache(); return receiv_coil;}

 private:
  friend class SeqPlotData;
  void update_coil_cache() const;
  void outdate_coil_cache() const;

  mutable CoilSensitivity* transm_coil;
  mutable CoilSensitivity* receiv_coil;
  mutable bool coil_cache_up2date;
};

/////////////////////////////////////////////////////

/**
  * @ingroup odinseq_internals
  *  Data structure to hold data for sequence plotting of a single 'sequence atom', e.g. an RF pulse
  */
struct SeqPlotCurve {
  SeqPlotCurve() : label(0), channel(plotChannel(0)), spikes(false), marklabel(0), marker(no_marker), marker_x(0.0) {}

  void resize(unsigned int newsize) {x.resize(newsize); y.resize(newsize);}
  bool operator == (const SeqPlotCurve& spc) const {return (channel==spc.channel) && (x==spc.x) && (y==spc.y);}

  friend STD_ostream& operator << (STD_ostream& os, const SeqPlotCurve& spc);
  
  const char* label;
  plotChannel channel;
  STD_vector<double> x;
  STD_vector<double> y;
  bool spikes; // values will not be interpolated between points

  const char* marklabel;
  markType marker;
  double marker_x;
};

/////////////////////////////////////////////////////

/**
  * @ingroup odinseq_internals
  *  Data structure to hold a discrete value synchronously on all channels
  */
struct SeqPlotSyncPoint {
  SeqPlotSyncPoint(double timepoint) : timep(timepoint), marker(no_marker), marklabel(0) {for(int i=0; i<numof_plotchan; i++) val[i]=0.0;}
  bool operator == (const SeqPlotSyncPoint& sp2) const {return (timep==sp2.timep);}
  bool operator <  (const SeqPlotSyncPoint& sp2) const {return (timep< sp2.timep);}
  double timep;
  double val[numof_plotchan];
  markType marker;
  const char* marklabel;
};


/////////////////////////////////////////////////////

/**
  * @ingroup odinseq_internals
  *  Data structure to hold a reference and starttime of a sequence atom
  */
struct SeqPlotCurveRef {
  SeqPlotCurveRef(double starttime, const SeqPlotCurve* curve_ptr) : start(starttime), ptr(curve_ptr), has_freq_phase(false), freq(0.0), phase(0.0), gradmatrix(0)  {}
  SeqPlotCurveRef(double starttime, const SeqPlotCurve* curve_ptr, double curve_freq, double curve_phase) : start(starttime), ptr(curve_ptr), has_freq_phase(true), freq(curve_freq), phase(curve_phase), gradmatrix(0)  {}
  SeqPlotCurveRef(double starttime, const SeqPlotCurve* curve_ptr, const RotMatrix* gradrotmatrix) : start(starttime), ptr(curve_ptr), has_freq_phase(false), freq(0.0), phase(0.0), gradmatrix(gradrotmatrix)  {}
  bool operator == (const SeqPlotCurveRef& ref2) const {return (start==ref2.start);}
  bool operator <  (const SeqPlotCurveRef& ref2) const {return (start<ref2.start);}

  bool contains_timepoint(double timep) const;
  double interpolate_timepoint(double timep) const;

  void copy_to_syncpoint(SeqPlotSyncPoint& sp, double value) const;

  
  double start; // start time within the frame
  const SeqPlotCurve* ptr;
  bool has_freq_phase;
  double freq;
  double phase;
  const RotMatrix* gradmatrix;
};

/////////////////////////////////////////////////////


/**
  * @ingroup odinseq_internals
  *  Data structure to hold a frame of curves, i.e. a list of curves within a well-defined time interval
  */
struct SeqPlotFrame : public STD_list<SeqPlotCurveRef> {

  double frameduration;

  void append_syncpoints(STD_list<SeqPlotSyncPoint>& synclist, double framestart) const;

  double get_latest_point() const;

};

/////////////////////////////////////////////////////

template<class T>
class PlotList : public STD_list<T> {

 public:
  PlotList() {reset_subcache();}

  void reset_subcache() {
    subbegin_cache=STD_list<T>::begin();
    subend_cache  =STD_list<T>::end();
  }

  void reset() {
    STD_list<T>::clear();
    reset_subcache();
  }
  
  typename STD_list<T>::const_iterator& get_iterator(double timepoint, bool below ) const {
    Log<SeqStandAlone> odinlog("PlotList","get_iterator");

    typename STD_list<T>::const_iterator current_iterator;

    // take iterator from last call for fast scrolling through the the curves
    if(below) current_iterator=subbegin_cache;
    else      current_iterator=subend_cache;

    // go one item back if we are already at the end
    if(current_iterator==STD_list<T>::end()) --current_iterator;

    // maximum boundaries for the search
    typename STD_list<T>::const_iterator lower_bound=STD_list<T>::begin();
    typename STD_list<T>::const_iterator upper_bound=STD_list<T>::end();

    unsigned int niter=0;

    double current_timepoint=current_iterator->get_bounds(below);

    // iterate current_iterator while we are below the first boundary
    if(current_timepoint>timepoint) {
      while(current_iterator!=lower_bound && current_iterator->get_bounds(below)>timepoint) {
        --current_iterator;
        niter++;
      }
    }
    // iterate current_iterator while we are above the last boundary
    if(current_timepoint<timepoint) {
      while(current_iterator!=upper_bound && current_iterator->get_bounds(below)<timepoint) {
        ++current_iterator;
        niter++;
      }
    }
    
    ODINLOG(odinlog,normalDebug) << "niter=" << niter << STD_endl;

    // do some extra iterations backward/forward to make sure all curves are plotted
    int iter;
    if(below) {
      subbegin_cache=current_iterator;
      for(iter=0; iter<EXTRA_UPDOWN_ITERATIONS; iter++) {
        if(subbegin_cache==STD_list<T>::begin()) break;
        --subbegin_cache;
      }
      return subbegin_cache;
    } else {
      subend_cache=current_iterator;
      for(iter=0; iter<EXTRA_UPDOWN_ITERATIONS; iter++) {
        if(subend_cache==STD_list<T>::end()) break;
        ++subend_cache;
      }
      return subend_cache;
    }
  }


  void get_sublist(typename STD_list<T>::const_iterator& result_begin, typename STD_list<T>::const_iterator& result_end, double starttime, double endtime ) const {
    Log<SeqStandAlone> odinlog("PlotList","get_sublist");

    ODINLOG(odinlog,normalDebug) << "starttime/endtime=" << starttime << "/" << endtime << STD_endl;
    
    // defaults
    result_begin=STD_list<T>::end();
    result_end=  STD_list<T>::end();
    if(starttime>=endtime) return;
    if(STD_list<T>::begin() == STD_list<T>::end()) return;

    result_begin=get_iterator(starttime,true);
    result_end  =get_iterator(endtime,false);
  }

 private:
  mutable typename STD_list<T>::const_iterator subbegin_cache;
  mutable typename STD_list<T>::const_iterator subend_cache;
};


/////////////////////////////////////////////////////



/**
  * @ingroup odinseq_internals
  *  Class to calculate plain timecourses
  */
class SeqTimecourse : public SeqTimecourseData {

 public:

  SeqTimecourse(const STD_list<SeqPlotSyncPoint>& synclist, const SeqTimecourse* eddy_tcourse, ProgressMeter* progmeter);
  ~SeqTimecourse();

  const SeqTimecourseData* get_subtimecourse(double starttime, double endtime) const;

  void get_markers(STD_list<TimecourseMarker4Qwt>::const_iterator& result_begin, STD_list<TimecourseMarker4Qwt>::const_iterator& result_end, double starttime, double endtime  ) const;

  bool simulate(const STD_list<SeqPlotSyncPoint>& synclist, const STD_string& fidfile, const STD_string& samplefile, const SeqSimulationOpts& opts, ProgressMeter* progmeter, SeqSimFeedbackAbstract* feedback, const SeqPlotDataAbstract* plotdata) const;

  
 protected:
  void allocate(unsigned int allocsize);

  void create_marker_values(const STD_list<SeqPlotSyncPoint>& synclist, ProgressMeter* progmeter);
  
 private:
  unsigned int get_index(double timepoint) const;

  PlotList<TimecourseMarker4Qwt> markers;

  // signal data used for for curves in Qwt
  mutable double* signal_x;
  mutable double** signal_y; // ptr2ptr for multi-channel coils
  mutable unsigned int signal_nchan; // cache for numof channels
  mutable svector signal_label;

};


//////////////////////////////

/**
  * @ingroup odinseq_internals
  *  Class to calculate gradient moments
  */
template<int Nth_moment, bool ConstGrad=false>
class SeqGradMomentTimecourse : public SeqTimecourse {
 public:
  SeqGradMomentTimecourse(const STD_list<SeqPlotSyncPoint>& synclist, const SeqTimecourse& plain_tcourse, const STD_string& nucleus, ProgressMeter* progmeter);
};

//////////////////////////////

/**
  * @ingroup odinseq_internals
  *  Class to integrate the product of two functions, e.g. to calculate b-Values
  */
class SeqTwoFuncIntegralTimecourse : public SeqTimecourse {
 public:
  SeqTwoFuncIntegralTimecourse(const STD_list<SeqPlotSyncPoint>& synclist, const SeqTimecourse& ka_tcourse, const SeqTimecourse& kb_tcourse, ProgressMeter* progmeter);
};

//////////////////////////////

/**
  * @ingroup odinseq_internals
  *  Class to calculate gradient slew rates
  */
class SeqSlewRateTimecourse : public SeqTimecourse {
 public:
  SeqSlewRateTimecourse(const STD_list<SeqPlotSyncPoint>& synclist, const SeqTimecourse& plain_tcourse, ProgressMeter* progmeter);
};

//////////////////////////////


/**
  * @ingroup odinseq_internals
  *  Class to calculate gradients caused by eddy currents
  */
class SeqEddyCurrentTimecourse : public SeqTimecourse {
 public:
  SeqEddyCurrentTimecourse(const STD_list<SeqPlotSyncPoint>& synclist, const SeqTimecourse& slew_rate_tcourse, const SeqTimecourseOpts& opts, ProgressMeter* progmeter);
};



/////////////////////////////////////////////////////

/**
  * @ingroup odinseq_internals
  * Class which holds all frames of a sequence plot and which serves
  * as an interface to access the plotting data in various ways
  */
class SeqPlotData : public STD_list<SeqPlotFrame>, public virtual SeqPlotDataAbstract, public Labeled {

 public:

  SeqPlotData(const STD_string& objlabel="unnamedSeqPlotData");
  ~SeqPlotData() {reset();}


  void append_curve(double starttime, const SeqPlotCurve* curve_ptr) {
    current_frame.push_back(SeqPlotCurveRef(framestart_offset+starttime,curve_ptr));
  }

  void append_curve(double starttime, const SeqPlotCurve* curve_ptr, double freq, double phase) {
    current_frame.push_back(SeqPlotCurveRef(framestart_offset+starttime,curve_ptr,freq,phase));
  }

  void append_curve(double starttime, const SeqPlotCurve* curve_ptr, const RotMatrix* gradrotmatrix) {
    current_frame.push_back(SeqPlotCurveRef(framestart_offset+starttime,curve_ptr,gradrotmatrix));
  }

  void flush_frame(double framedur);

  void reset();


  void add_signal_curve(const Curve4Qwt& signal_curve) const;

  void get_curves(STD_list<Curve4Qwt>::const_iterator& result_begin, STD_list<Curve4Qwt>::const_iterator& result_end, double starttime, double endtime, double max_highres_interval  ) const;

  void get_signal_curves(STD_list<Curve4Qwt>::const_iterator& result_begin, STD_list<Curve4Qwt>::const_iterator& result_end, double starttime, double endtime  ) const;

  void get_markers(STD_list<Marker4Qwt>::const_iterator& result_begin, STD_list<Marker4Qwt>::const_iterator& result_end, double starttime, double endtime  ) const;

  bool timecourse_created(timecourseMode type) const {return bool(timecourse_cache[type]);}

  bool create_timecourses(timecourseMode type, const STD_string& nucleus, ProgressMeter* progmeter) const;

  const SeqTimecourseData* get_timecourse(timecourseMode type) const;
  
  const SeqTimecourseData* get_subtimecourse(timecourseMode type, double starttime, double endtime) const;

  void get_timecourse_markers(timecourseMode type, STD_list<TimecourseMarker4Qwt>::const_iterator& result_begin, STD_list<TimecourseMarker4Qwt>::const_iterator& result_end, double starttime, double endtime ) const;

  double get_total_duration() const;

  unsigned int numof_rec_channels() const;

  unsigned int n_frames() const {return size();}

  LDRblock& get_opts(bool include_timecourse_opts, bool include_simulation_opts) const;

  void set_coilsdir(const STD_string& coilsdir) const;

  bool monitor_simulation() const {return sim_opts.MagnMonitor;}

  bool simulate(const STD_string& fidfile, const STD_string& samplefile, ProgressMeter* progmeter, SeqSimFeedbackAbstract* feedback) const;

  bool has_curves_on_channel(plotChannel chan) const;


 private:

  void clear_curves4qwt_cache() const;
  void clear_markers4qwt_cache() const;
  void clear_synclist_cache() const;
  void clear_timecourse_cache(timecourseMode type) const;

  void create_curves4qwt_cache() const;
  void create_markers4qwt_cache() const;

  void create_synclist_cache(ProgressMeter* progmeter) const;
  void create_timecourse_cache(timecourseMode type, const STD_string& nucleus, ProgressMeter* progmeter) const;

  mutable SeqTimecourseOpts timecourse_opts;
  mutable SeqSimulationOpts sim_opts;
  mutable LDRblock      all_opts;

  SeqPlotFrame current_frame;
  double framestart_offset;

  mutable PlotList<Curve4Qwt>  curves4qwt_cache;
  mutable PlotList<Curve4Qwt>  curves4qwt_cache_lowres;
  mutable bool curves4qwt_cache_done;

  mutable PlotList<Marker4Qwt> markers4qwt_cache;
  mutable bool markers4qwt_cache_done;


  mutable STD_list<SeqPlotSyncPoint> synclist_cache;
  mutable bool synclist_cache_done;

  mutable SeqTimecourse* timecourse_cache[numof_tcmodes];

  mutable PlotList<Curve4Qwt>  signal_curves;

  mutable bool has_curves_on_channel_cache[numof_plotchan];

};


/////////////////////////////////////////////////////////////////


#endif


