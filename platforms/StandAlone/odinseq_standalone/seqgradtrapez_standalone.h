/***************************************************************************
                          seqgradtrapez_standalone.h  -  description
                             -------------------
    begin                : Tue Apr 27 2004
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQGRADTRAPEZ_STANDALONE_H
#define SEQGRADTRAPEZ_STANDALONE_H

#include <odinseq/seqgradtrapez.h>

// using default driver


#endif

