#include "seqgradchan_standalone.h"

void SeqGradChanStandAlone::common_int() {
  veccurve_cache=0;
  current_vec=-1;
}

void SeqGradChanStandAlone::common_prep(SeqGradPlotCurve& gradcurve) {
  for(int i=0; i<n_directions; i++) {
    gradcurve.grad[i].label=get_label().c_str();
  }
}
  

SeqGradChanStandAlone::SeqGradChanStandAlone() {
  common_int();
}

SeqGradChanStandAlone::~SeqGradChanStandAlone() {
  if(veccurve_cache) delete[] veccurve_cache;
}

SeqGradChanStandAlone::SeqGradChanStandAlone(const SeqGradChanStandAlone& sgcs)
 : SeqGradChanDriver(sgcs) {
  common_int();
  set_label(sgcs.get_label());
  curve=sgcs.curve;
}


bool SeqGradChanStandAlone::generate_constgrad(SeqGradPlotCurve& gradcurve, float strength, const fvector& strengthfactor, double gradduration) {
  common_prep(gradcurve);
  float slewrate=systemInfo->get_max_slew_rate();

  if(gradduration<0.0) gradduration=0.0;

  float maxstrength_dur=slewrate*gradduration;
  if(fabs(strength)>maxstrength_dur) {
    float sign=secureDivision(strength,fabs(strength));
    strength=sign*maxstrength_dur;
  }
  double rampdur=secureDivision(fabs(strength),slewrate);


  if(rampdur>0.0 && strength!=0.0) {
    for(int i=0; i<n_directions; i++) {
      double str=strength*strengthfactor[i];
      if(str) {
        int npts_ramp=2;
        gradcurve.grad[i].resize(2*npts_ramp);
        for(int j=0; j<npts_ramp; j++) {
          float s=secureDivision(j, npts_ramp-1);
          gradcurve.grad[i].x[j]=s*rampdur;
          gradcurve.grad[i].y[j]=s*str;

          gradcurve.grad[i].x[2*npts_ramp-1-j]=gradduration+rampdur-s*rampdur;
          gradcurve.grad[i].y[2*npts_ramp-1-j]=s*str;
        }
      }
    }
  }

  if(dump2console) {
    for(int i=0; i<n_directions; i++) STD_cout << gradcurve.grad[i] << STD_endl;
  }

  return true;
}

bool SeqGradChanStandAlone::prep_const(float strength, const fvector& strengthfactor, double gradduration) {
  return generate_constgrad(curve,strength,strengthfactor,gradduration);
}


bool SeqGradChanStandAlone::prep_onepoint(float strength, const fvector& strengthfactor, double gradduration) {
  return prep_const(strength,strengthfactor,gradduration);
}


bool SeqGradChanStandAlone::prep_wave(float strength, const fvector& strengthfactor, double gradduration, const fvector& wave) {
  common_prep(curve);
  unsigned int n=wave.size();

  double dt=secureDivision(gradduration,n);

  for(int i=0; i<n_directions; i++) {
    double str=strength*strengthfactor[i];
    if(str) {
      curve.grad[i].resize(n);
      for(unsigned int j=0; j<n; j++) {
        double timep=(0.5+double(j))*dt;
        curve.grad[i].x[j]=timep;
        curve.grad[i].y[j]=str*wave[j];
      }
    }
  }

  if(dump2console) {
    for(int i=0; i<n_directions; i++) STD_cout << curve.grad[i] << STD_endl;
  }

  return true;
}

bool SeqGradChanStandAlone::prep_vector(float strength, const fvector& strengthfactor, double gradduration, const fvector& gradvec, const iarray& index_matrix, nestingRelation nr) {
  common_prep(curve);
  Log<SeqStandAlone> odinlog(this,"prep_vector");

  unsigned int n=gradvec.size();
  ODINLOG(odinlog,normalDebug) << "gradvec(" << n << ")=" << gradvec.printbody() << STD_endl;

  veccurve_cache=new SeqGradPlotCurve[n];
  for(unsigned int j=0; j<n; j++) {
    generate_constgrad(veccurve_cache[j],strength*gradvec[j],strengthfactor,gradduration);
  }
  if(n) current_vec=0;
  return true;
}


bool SeqGradChanStandAlone::prep_vector_iteration(unsigned int count) {
  Log<SeqStandAlone> odinlog(this,"prep_vector_iteration");
  ODINLOG(odinlog,normalDebug) << "count=" << count << STD_endl;
  current_vec=count;
  return true;
}

bool SeqGradChanStandAlone::prep_trapez(float strength, const fvector& strengthfactor, double ruptime, const fvector& rupshape, double consttime, double rdowntime, const fvector& rdownshape) {
  common_prep(curve);
  unsigned int n_up  =rupshape.size();
  unsigned int n_down=rdownshape.size();
  unsigned int n_total=n_up+2+n_down;

  unsigned int j;

  for(int i=0; i<n_directions; i++) {
    double str=strength*strengthfactor[i];
    if(str) {
      curve.grad[i].resize(n_total);
      unsigned int index=0;

      // ramp up
      double dt=secureDivision(ruptime,double(n_up));
      double timep=0.5*dt;
      for(j=0; j<n_up; j++) {
        curve.grad[i].x[index]=timep;
        curve.grad[i].y[index]=str*rupshape[j];
        index++;
        timep+=dt;
      }

      // constant part
      timep=ruptime;
      curve.grad[i].x[index]=timep;
      curve.grad[i].y[index]=str;
      index++;
      timep+=consttime;
      curve.grad[i].x[index]=timep;
      curve.grad[i].y[index]=str;
      index++;

      // ramp down
      dt=secureDivision(rdowntime,double(n_down));
      timep+=0.5*dt;
      for(j=0; j<n_down; j++) {
        curve.grad[i].x[index]=timep;
        curve.grad[i].y[index]=str*rdownshape[j];
        index++;
        timep+=dt;
      }
    }
  }

  if(dump2console) {
    for(int i=0; i<n_directions; i++) STD_cout << curve.grad[i] << STD_endl;
  }

  return true;
}


void SeqGradChanStandAlone::event(eventContext& context, double start) const {
  Log<SeqStandAlone> odinlog(this,"event");
  ODINLOG(odinlog,normalDebug) << "start=" << start << STD_endl;
  for(int i=0; i<n_directions; i++) {
    if(veccurve_cache) {
      if(current_vec>=0) {
        ODINLOG(odinlog,normalDebug) << "current_vec=" << current_vec << STD_endl;
        if(veccurve_cache[current_vec].grad[i].x.size()) {
          append_curve2plot(start,&(veccurve_cache[current_vec].grad[i]),current_rotmatrix);
        }
      }
    } else {
      if(curve.grad[i].x.size()) {
        append_curve2plot(start,&(curve.grad[i]),current_rotmatrix);
      }
    }
  }
}

