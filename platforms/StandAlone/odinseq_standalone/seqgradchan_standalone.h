/***************************************************************************
                          seqgradchan_standalone.h  -  description
                             -------------------
    begin                : Fri Jul 23 2004
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQGRADCHAN_STANDALONE_H
#define SEQGRADCHAN_STANDALONE_H

#include <odinseq/seqgradchan.h>
#include <odinseq/seqgradchanparallel.h>
#include "seqstandalone.h"

//////////////////////////////////////////////////////////////////


struct SeqGradPlotCurve {
  SeqGradPlotCurve() {
    for(int i=0; i<n_directions; i++) {
      grad[i].channel=plotChannel(Gread_plotchan+i);
    }
  }
  SeqPlotCurve grad[n_directions];
};


/**
  * @ingroup odinseq_internals
  * The Standalone driver for gradient channel elements
  */
class SeqGradChanStandAlone : public SeqGradChanDriver, public SeqStandAlone {

 public:

  SeqGradChanStandAlone();

  SeqGradChanStandAlone(const SeqGradChanStandAlone& sgcs);

  ~SeqGradChanStandAlone();


  // overloading virtual functions from SeqGradChanDriver
  STD_string get_const_program   (float strength, float matrixfactor) const {return "";}
  STD_string get_onepoint_program(float strength, float matrixfactor) const {return "";}
  STD_string get_delay_program   (float strength, float matrixfactor) const {return "";}
  STD_string get_wave_program    (float strength, float matrixfactor) const {return "";}
  STD_string get_trapez_program  (float strength, float matrixfactor) const {return "";}
  virtual STD_string get_vector_program  (float strength, float matrixfactor, int reordercount) const {return "";}
  svector get_vector_commands(const STD_string& iterator) const {return svector();}
  svector get_reord_commands() const {return svector();}
  bool prep_const(float strength, const fvector& strengthfactor, double gradduration);
  bool prep_onepoint(float strength, const fvector& strengthfactor, double gradduration);
  bool prep_wave(float strength, const fvector& strengthfactor, double gradduration, const fvector& wave);
  void update_wave(const fvector& wave) {}
  bool prep_vector(float strength, const fvector& strengthfactor, double gradduration, const fvector& gradvec, const iarray& index_matrix, nestingRelation nr);
  bool prep_vector_iteration(unsigned int count);
  bool prep_trapez(float strength, const fvector& strengthfactor, double ruptime, const fvector& rupshape, double consttime, double rdowntime, const fvector& rdownshape);
  void event(eventContext& context, double starttime) const;
  float check_strength(float strength) const {return strength;}
  SeqGradChanDriver* clone_driver() const {return new SeqGradChanStandAlone(*this);}

  // overloading virtual functions from SeqDriverBase
  odinPlatform get_driverplatform() const {return standalone;}

 private:
  void common_int();
  void common_prep(SeqGradPlotCurve& gradcurve);

  bool generate_constgrad(SeqGradPlotCurve& gradcurve, float strength, const fvector& strengthfactor, double gradduration);

  SeqGradPlotCurve curve;

  SeqGradPlotCurve* veccurve_cache;
  int current_vec;
};


//////////////////////////////////////////////////////////////////

/**
  * @ingroup odinseq_internals
  * The Standalone driver for parallel lists of gradient channel elements
  */
class SeqGradChanParallelStandAlone : public SeqGradChanParallelDriver {

 public:
  SeqGradChanParallelStandAlone() {}

  SeqGradChanParallelStandAlone(const SeqGradChanParallelStandAlone& sgcpp) {}

  // overloading virtual functions from SeqGradChanParallelDriver
  bool prep_driver(SeqGradChanList* chanlists[3]) const {return true;}
  STD_string get_program(programContext& context) const {return "";}
  SeqGradChanParallelDriver* clone_driver() const {return new SeqGradChanParallelStandAlone(*this);}

  // overloading virtual functions from SeqDriverBase
  odinPlatform get_driverplatform() const {return standalone;}

};

#endif

