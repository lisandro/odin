/***************************************************************************
                          seqdelay_standalone.h  -  description
                             -------------------
    begin                : Fri Jul 23 2004
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQDELAY_STANDALONE_H
#define SEQDELAY_STANDALONE_H

#include <odinseq/seqdelay.h>
#include <odinseq/seqdelayvec.h>

//////////////////////////////////////////////////////////////////


/**
  * @ingroup odinseq_internals
  * The Standalone driver for delays
  */
class SeqDelayStandAlone : public SeqDelayDriver {

 public:

  SeqDelayStandAlone() {}

  SeqDelayStandAlone(const SeqDelayStandAlone& sds) {set_label(sds.get_label());}

  ~SeqDelayStandAlone() {}


  // overloading virtual functions from SeqDelayDriver
  SeqDelayDriver* clone_driver() const {return new SeqDelayStandAlone(*this);}
  STD_string get_program(programContext& context, double duration, const STD_string& cmd, const STD_string& durcmd) const {return "";}

  // overloading virtual functions from SeqDriverBase
  odinPlatform get_driverplatform() const {return standalone;}
};

//////////////////////////////////////////////////////////////////


/**
  * @ingroup odinseq_internals
  * The Standalone driver for delay vectors
  */
class SeqDelayVecStandAlone : public SeqDelayVecDriver {

 public:

  SeqDelayVecStandAlone() {}

  SeqDelayVecStandAlone(const SeqDelayVecStandAlone& sdvs) {set_label(sdvs.get_label());}

  ~SeqDelayVecStandAlone() {}


  // overloading virtual functions from SeqDelayVecDriver
  SeqDelayVecDriver* clone_driver() const {return new SeqDelayVecStandAlone(*this);}
  bool prep_driver() {return true;}
  STD_string get_program(programContext& context, double duration) const {return "";}
  bool unroll_program() const {return false;}

  // overloading virtual functions from SeqDriverBase
  odinPlatform get_driverplatform() const {return standalone;}
};


#endif

