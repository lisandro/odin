/***************************************************************************
                          seqpuls_standalone.h  -  description
                             -------------------
    begin                : Sat Apr 3 2004
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQPULS_STANDALONE_H
#define SEQPULS_STANDALONE_H

#include <odinseq/seqpuls.h>
#include "seqstandalone.h"

//////////////////////////////////////////////////////////////////


/**
  * @ingroup odinseq_internals
  * The Standalone driver for RF pulses
  */
class SeqPulsStandAlone : public SeqPulsDriver, public SeqStandAlone {

 public:
  SeqPulsStandAlone() {current_pls=0;}
  ~SeqPulsStandAlone() {}

  SeqPulsStandAlone(const SeqPulsStandAlone& sps);

  // overloading virtual functions from SeqPulsDriver
  bool prep_driver(const cvector& wave, double pulsduration, double pulscenter, float b1max, float power, float flipangle, const fvector& flipscales, pulseType plstype);
  void event(eventContext& context, double start) const;
  double get_rf_energy() const;
  bool prep_flipangle_iteration(unsigned int count);
  double get_predelay() const {return 0.0;}
  double get_postdelay() const {return 0.0;}
  STD_string get_program(programContext& context, unsigned int phaselistindex,int channel, const STD_string& iteratorcommand) const {return "";}
  STD_string get_instr_label() const {return "";}
  svector get_flipvector_commands(const STD_string& iterator) const {return svector();}
  void new_freq(double newfreq) const {}
  bool has_new_freq() const {return true;}
  SeqPulsDriver* clone_driver() const {return new SeqPulsStandAlone(*this);}

  // overloading virtual functions from SeqDriverBase
  odinPlatform get_driverplatform() const {return standalone;}

 private:
  STD_vector<SeqPlotCurve> B1re_curve;
  STD_vector<SeqPlotCurve> B1im_curve;

  dvector rf_energy;

  unsigned int current_pls;
  bool has_real;
  bool has_imag;

  STD_string relabel,imlabel;
};


#endif

