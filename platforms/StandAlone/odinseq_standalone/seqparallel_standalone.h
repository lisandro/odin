/***************************************************************************
                          seqparallel_standalone.h  -  description
                             -------------------
    begin                : Fri Apr 16 2004
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQPARALLEL_STANDALONE_H
#define SEQPARALLEL_STANDALONE_H

#include <odinseq/seqparallel.h>

//////////////////////////////////////////////////////////////////


/**
  * @ingroup odinseq_internals
  * The Standalone driver for parallel RF/gradient handling
  */
class SeqParallelStandAlone : public SeqParallelDriver, public SeqStandAlone {

 public:

  SeqParallelStandAlone() {}

  SeqParallelStandAlone(const SeqParallelStandAlone& sps) {set_label(sps.get_label());}

  ~SeqParallelStandAlone() {}


  // overloading virtual functions from SeqParallelDriver
  STD_string get_program(programContext& context, const SeqObjBase* soa, const SeqGradObjInterface* sgoa) const {return "";}
  double get_duration (const SeqObjBase* soa, const SeqGradObjInterface* sgoa) const {return 0.0;}
  double get_predelay (const SeqObjBase* soa, const SeqGradObjInterface* sgoa) const {return 0.0;}
  SeqParallelDriver* clone_driver() const {return new SeqParallelStandAlone(*this);}

  // overloading virtual functions from SeqDriverBase
  odinPlatform get_driverplatform() const {return standalone;}
};


#endif

