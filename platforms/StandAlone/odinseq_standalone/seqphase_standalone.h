/***************************************************************************
                          seqphase_standalone.h  -  description
                             -------------------
    begin                : Tue Apr 6 2004
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQPHASE_STANDALONE_H
#define SEQPHASE_STANDALONE_H

#include <odinseq/seqphase.h>


//////////////////////////////////////////////////////////////////

/**
  * @ingroup odinseq_internals
  * The Standalone driver for phase lists
  */
class SeqPhaseStandAlone : public SeqPhaseDriver  {

 public:
  SeqPhaseStandAlone() {}
  ~SeqPhaseStandAlone() {}

  SeqPhaseStandAlone(const SeqPhaseStandAlone& sps) {set_label(sps.get_label());}

  // overloading virtual functions from SeqPhaseDriver
  void prep_driver(const dvector& phaselist) {}
  unsigned int get_phaselistindex(const dvector& phaselist) const {return 0;}
  STD_string get_loopcommand(const dvector& phaselist) const {return "";}
  svector get_phasevec_commands(const STD_string& iterator, const STD_string& instr) const {return svector();}
  SeqPhaseDriver* clone_driver() const {return new SeqPhaseStandAlone(*this);}

  // overloading virtual functions from SeqDriverBase
  odinPlatform get_driverplatform() const {return standalone;}

};

#endif

