/***************************************************************************
                          seqfreq_standalone.h  -  description
                             -------------------
    begin                : Sat Apr 3 2004
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQFREQ_STANDALONE_H
#define SEQFREQ_STANDALONE_H

#include <odinseq/seqfreq.h>
#include "seqstandalone.h"

//////////////////////////////////////////////////////////////////


/**
  * @ingroup odinseq_internals
  * The Standalone driver for transmit/receive channels
  */
class SeqFreqChanStandAlone : public SeqFreqChanDriver, public SeqStandAlone {

 public:
  SeqFreqChanStandAlone() {}
  ~SeqFreqChanStandAlone() {}

  SeqFreqChanStandAlone(const SeqFreqChanStandAlone& sfcs)  {set_label(sfcs.get_label());}

  // overloading virtual functions from SeqFreqChanDriver
  bool prep_driver(const STD_string& nucleus, const dvector& freqlist) {return true;}
  void prep_iteration(double current_frequency, double current_phase, double freqchan_duration) const;
  int get_channel() const {return 0;}
  STD_string get_iteratorcommand(objCategory cat,int freqlistindex) const {return "";}
  svector get_freqvec_commands(const STD_string& iterator, const STD_string& instr) const {return svector();}
  STD_string get_pre_program(programContext& context, objCategory cat, const STD_string& instr_label, double default_frequency, double default_phase) const {return "";}
  void pre_event (eventContext& context,double starttime) const;
  void post_event(eventContext& context,double starttime) const {}
  SeqFreqChanDriver* clone_driver() const {return new SeqFreqChanStandAlone(*this);}

  // overloading virtual functions from SeqDriverBase
  odinPlatform get_driverplatform() const {return standalone;}

 private:
  mutable double freq_cache;
  mutable double phase_cache;
  

};

#endif

