#include "pulsar.h"

int main(int argc, char *argv[]) {

  // do this here so we do not have to query X server in order to get usage for manual
  if(hasHelpOption(argc, argv)) {PulsarView::usage();exit(0);}

  GuiApplication a(argc, argv);  // debug handler will be initialized here
  PulsarMain *pulsar=new PulsarMain();
  return a.start(pulsar->get_widget());
}


