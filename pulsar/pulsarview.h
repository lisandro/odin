/***************************************************************************
                          pulsarview.h  -  description
                             -------------------
    begin                : Thu Jun 20 19:02:26 CEST 2002
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef PULSARVIEW_H
#define PULSARVIEW_H

#include <odinqt/ldrwidget.h>
#include <odinqt/ldrblockwidget.h>

#include <odinseq/odinpulse.h>
#include <odinseq/seqsim.h>

////////////////////////////////////////

// for debugging Pulsar component
class PulsarComp {
 public:
  static const char* get_compName();
};

////////////////////////////////////////

struct PulsarCancel {}; // exception

////////////////////////////////////////


class PulsarView : public QWidget, public virtual LDReditWidget {
 Q_OBJECT

 public:
  PulsarView(QWidget *parent=0);
  ~PulsarView();

  void initPulsarView();

  static void usage();

 signals:
  void done();
  void newCaption(const char* text);

 public slots:
  void slotFileNew();
  void slotFileOpen();
  void slotFileSave();
  void slotFileSaveAs();
//  void swapSliderTracking(bool);
  void writeBrukerRf();
  void exportRfampASCII();
  void exportRfphaASCII();
  void exportGxASCII();
  void exportGyASCII();
  void exportGzASCII();
  void exportMampASCII();
  void exportMphaASCII();
  void exportMzASCII();
  void exportPulseLDR();
  void exportMagLDR();

  void edit_systemInfo();

 private slots:
  void updatePulse();
  void updateMagnetization();
  void emitDone();


 private:

  //  Implemented function of LDReditWidget
  void updateWidget();

  void vriteArray(const STD_string& filename, const fvector& data);

  void exportASCIIpulse(const fvector& data);
  void exportASCIImag(const fvector& data);

  OdinPulse* pulse;
  Sample* sample;
  SeqSimMagsi* mag;

  funcMode old_mode;
  STD_string fileName;

  GuiGridLayout *grid;

  LDRwidget* pulseWidget[n_dimModes];
  LDRwidget* sampleWidget[n_dimModes];
  LDRwidget* magnWidget[n_dimModes];

  GuiProgressDialog* progress;
  int progcount;
};

#endif
