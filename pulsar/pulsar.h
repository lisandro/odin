/***************************************************************************
                          pulsar.h  -  description
                             -------------------
    begin                : Fri Jul  7 13:38:24 /etc/localtime 2000
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/


#ifndef PULSAR_H
#define PULSAR_H


#include "pulsarview.h"


#define IDS_PULSAR_ABOUT            "Pulsar user interface\nVersion " VERSION \
                                    "\n(w) 2000-2015 by Thies Jochimsen\n\n"


class PulsarMain : public QObject, public GuiMainWindow {
  Q_OBJECT
  
 public:
  PulsarMain();

 public slots:
  void slotFileDone();
  void slotFileQuit();
  void slotHelpAbout();
  void changeCaption(const char* text);


 private:
  void initMenuBar();
  bool queryExit();

  PulsarView *view;

  GuiPopupMenu* fileMenu;
  GuiPopupMenu* importMenu;
  GuiPopupMenu* exportMenu;
  GuiPopupMenu* viewMenu;
  GuiPopupMenu* settingsMenu;
  GuiPopupMenu* helpMenu;

};
#endif 

