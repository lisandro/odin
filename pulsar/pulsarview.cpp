#include "pulsarview.h"

#include <odinseq/seqplatform.h>

#include <tjutils/tjlog_code.h>

const char* PulsarComp::get_compName() {return "Pulsar";}
LOGGROUNDWORK(PulsarComp)


void PulsarView::usage() {
   STD_cout << "pulsar: Pulse editor of ODIN" << STD_endl;
   STD_cout << "Usage: pulsar [options] [<pulsar_file>]" << STD_endl;
   STD_cout << "Options:" << STD_endl;
   STD_cout << "\t-noedit : View-only mode" << STD_endl;
   STD_cout << "\t -s <file>: Load system defaults" << STD_endl;
   STD_cout << "\t" << LogBase::get_usage() << STD_endl;
   STD_cout << "\t" << helpUsage() << STD_endl;
}


PulsarView::PulsarView(QWidget *parent) : QWidget(parent) {
  Log<PulsarComp> odinlog("PulsarView","PulsarView(...)");

  char buff[ODIN_MAXCHAR];


  pulse=new OdinPulse("Pulse",true);
  ODINLOG(odinlog,normalDebug) << "generation pars initialised" << STD_endl;

  sample=new Sample("Sample",true,true);
  ODINLOG(odinlog,normalDebug) << "sample pars initialised" << STD_endl;

  mag=new SeqSimMagsi("Magnetization");
  ODINLOG(odinlog,normalDebug) << "simulation pars initialised" << STD_endl;


  if(isCommandlineOption(GuiApplication::argc(),GuiApplication::argv(),"-noedit")) {
    pulse->set_parmode(noedit);
  }


  if(getCommandlineOption(GuiApplication::argc(),GuiApplication::argv(),"-s",buff,ODIN_MAXCHAR)) {
    SeqPlatformProxy::load_systemInfo(buff);
    ODINLOG(odinlog,normalDebug) << "systemInfo file >" << buff << "< loaded" << STD_endl;
  }

  if(getLastArgument(GuiApplication::argc(),GuiApplication::argv(),buff,ODIN_MAXCHAR)) {
    if(STD_string(buff)!="") {
      pulse->load(buff);
      fileName=buff;
      ODINLOG(odinlog,normalDebug) << "pulsar file >" << buff << "< loaded" << STD_endl;
    }
  }

  grid=new GuiGridLayout( this, 2, 2);

  ODINLOG(odinlog,normalDebug) << "layout created" << STD_endl;


  /*------------- general pulse parameters ------------------------*/


  old_mode=n_dimModes;

  unsigned int i;
  for(i=0; i<n_dimModes; i++) pulseWidget[i]=0;
  for(i=0; i<n_dimModes; i++) sampleWidget[i]=0;
  for(i=0; i<n_dimModes; i++) magnWidget[i]=0;

  ODINLOG(odinlog,normalDebug) << "calling updatePulse()" << STD_endl;

  updatePulse();

}

PulsarView::~PulsarView() {
  for(unsigned int i=0; i<n_dimModes; i++) {
    if(pulseWidget[i]) delete pulseWidget[i];
    if(sampleWidget[i]) delete sampleWidget[i];
    if(magnWidget[i]) delete magnWidget[i];
  }
  delete pulse;
  delete sample;
  delete mag;
  delete grid;
}

void PulsarView::initPulsarView() {
  emit newCaption(fileName.c_str());
}

void PulsarView::updateWidget() {
  Log<PulsarComp> odinlog("PulsarView","updateWidget()");

  if(!progress) {
    progress=new GuiProgressDialog(this, false,0);
    progress->set_text("Calculating Pulse");
    progress->show();
    progcount=0;
  } else {
    progress->set_progress(progcount);
    progcount++;
  }

  if(progress->was_cancelled()) throw PulsarCancel();


  pulseWidget[old_mode]->updateWidget();
  updateMagnetization();
  GuiApplication::process_events(); // Must be called manually because Qt event handler is blocked in optimization loop
}


void PulsarView::updatePulse() {
  Log<PulsarComp> odinlog("PulsarView","updatePulse()");

  ODINLOG(odinlog,normalDebug) << "calling pulse->update() ..." << STD_endl;
  progress=0;
  try {
    pulse->update();
  } catch(PulsarCancel) {
    ODINLOG(odinlog,normalDebug) << " cancelled" << STD_endl;
  }
  if(progress) delete progress;
  progress=0;
  ODINLOG(odinlog,normalDebug) << "done" << STD_endl;

  funcMode mode=pulse->get_dim_mode();
  ODINLOG(odinlog,normalDebug) << "old_mode/mode=" << old_mode << "/" << mode << STD_endl;
  if(old_mode!=mode) {
    if(old_mode!=n_dimModes) {
      ODINLOG(odinlog,normalDebug) << "Hiding old LDRwidget" << STD_endl;
      pulseWidget[old_mode]->deleteDialogs();
      pulseWidget[old_mode]->hide();
    }
    ODINLOG(odinlog,normalDebug) << "Allocating LDRwidget" << STD_endl;
    if(pulseWidget[mode]==0) pulseWidget[mode]=new LDRwidget(*pulse,2,this,true);
    ODINLOG(odinlog,normalDebug) << "Showing LDRwidget" << STD_endl;
    pulseWidget[mode]->show();
    grid->add_widget(pulseWidget[mode], 0, 0, GuiGridLayout::Default, 2, 1);
    connect(pulseWidget[mode],SIGNAL(valueChanged()),this,SLOT(updatePulse()));
    connect(pulseWidget[mode],SIGNAL(doneButtonPressed()),this,SLOT(emitDone()));
  }
  ODINLOG(odinlog,normalDebug) << "calling pulseWidget[" << mode << "]->updateWidget() ..." << STD_endl;
  pulseWidget[mode]->updateWidget();
  ODINLOG(odinlog,normalDebug) << "done" << STD_endl;
  updateMagnetization();
  old_mode=mode;
}


void PulsarView::updateMagnetization() {
  Log<PulsarComp> odinlog("PulsarView","updateMagnetization");

  funcMode mode=pulse->get_dim_mode();
  ODINLOG(odinlog,normalDebug) << "mode=" << mode << STD_endl;

  if(old_mode!=mode) {
    ODINLOG(odinlog,normalDebug) << "Changing mode" << STD_endl;
    bool onlineflag=true;
    unsigned int xsize=1;
    unsigned int ysize=1;
    unsigned int zsize=1;
    unsigned int freqsize=1;
    if(mode==zeroDeeMode) { freqsize=255; }
    if(mode==oneDeeMode) { zsize=255; }
    if(mode>=twoDeeMode) {xsize=31; ysize=31; onlineflag=false;}
    ODINLOG(odinlog,normalDebug) << "xsize/ysize/zsize/freqsize=" << xsize << "/" << ysize << "/" << zsize << "/" << freqsize << STD_endl;
    sample->resize(1,freqsize,zsize,ysize,xsize);
    mag->set_online_simulation(onlineflag);
    sample->update();
    mag->update();
    mag->prepare_simulation(*sample); // call once to resize mag
    pulse->simulate_pulse(*mag,*sample); // simulate once to get gray scale in 2D mode

    if(old_mode!=n_dimModes) {
      ODINLOG(odinlog,normalDebug) << "Deleting old widgets ..." << STD_endl;
      sampleWidget[old_mode]->deleteDialogs();
      sampleWidget[old_mode]->hide();
      magnWidget[old_mode]->deleteDialogs();
      magnWidget[old_mode]->hide();
      ODINLOG(odinlog,normalDebug) << "Deleting old widgets done" << STD_endl;
    }
    if(sampleWidget[mode]==0) {
      ODINLOG(odinlog,normalDebug) << "Creating new sampleWidget ..." << STD_endl;
      sampleWidget[mode]=new LDRwidget(*sample,1,this);
      sampleWidget[mode]->show();
      grid->add_widget(sampleWidget[mode], 0, 1);
      connect(sampleWidget[mode],SIGNAL(valueChanged()),this,SLOT(updateMagnetization()));
      ODINLOG(odinlog,normalDebug) << "Creating new sampleWidget done" << STD_endl;
    }
    if(magnWidget[mode]==0) {
      ODINLOG(odinlog,normalDebug) << "Creating new magnWidget ..." << STD_endl;
      magnWidget[mode]=new LDRwidget(*mag,1,this);
      magnWidget[mode]->show();
      grid->add_widget(magnWidget[mode], 1, 1);
      connect(magnWidget[mode],SIGNAL(valueChanged()),this,SLOT(updateMagnetization()));
      ODINLOG(odinlog,normalDebug) << "Creating new magnWidget done" << STD_endl;
    }
    sampleWidget[mode]->show();
    magnWidget[mode]->show();
  }

  sample->update();
  mag->update();

  if(mag->do_simulation()) pulse->simulate_pulse(*mag,*sample);

  sampleWidget[mode]->updateWidget();
  magnWidget[mode]->updateWidget();
}


void PulsarView::emitDone() {emit done();}


void PulsarView::slotFileNew() {
  if(message_question("Resetting to defaults will delete the current values.\n"
                      "Do you really want a fresh pulse ?", "New Pulse", this, true)) {
    (*pulse)=OdinPulse("Pulse",true);
    updatePulse();
  }
}

void PulsarView::slotFileOpen() {
  STD_string newfile=get_open_filename("Open Pulse", fileName.c_str(), "", this);
  if (newfile!="") {
    fileName=newfile;
    emit newCaption(fileName.c_str());
    pulse->load(fileName);
    updatePulse();
  }
}

void PulsarView::slotFileSave() {
  if (fileName!="") {
    pulse->write(fileName);
  } else slotFileSaveAs();
}

void PulsarView::slotFileSaveAs() {
  fileName=get_save_filename("Save Pulse", fileName.c_str(), "", this);
  if (fileName!="") {
    pulse->write(fileName);
  }
}

void PulsarView::writeBrukerRf() {
  STD_string fname=get_save_filename("Export Bruker Pulse", "", "", this);
  if (fname!="") {
    SeqPlatformProxy::set_current_platform(paravision);
    pulse->write_rf_waveform(fname);
    SeqPlatformProxy::set_current_platform(standalone);
  }
}


void PulsarView::vriteArray(const STD_string& filename, const fvector& data) {
  STD_string filestr;
  for(unsigned int i=0; i<data.length(); i++) {
    filestr+=ftos(data[i])+"\n";
  }
  ::write(filestr,filename);
}


void PulsarView::exportASCIIpulse(const fvector& data) {
  STD_string fname=get_save_filename("Export ASCII Pulse", "", "", this);
  if(fname!="") vriteArray(fname,data);
}

void PulsarView::exportASCIImag(const fvector& data) {
  STD_string fname=get_save_filename("Export ASCII Magnetization", "", "", this);
  if(fname!="") vriteArray(fname,data);
}



void PulsarView::exportRfampASCII() {exportASCIIpulse(amplitude(pulse->get_B1()));}
void PulsarView::exportRfphaASCII() {exportASCIIpulse(phase(pulse->get_B1()));}
void PulsarView::exportGxASCII() {exportASCIIpulse(pulse->get_Grad(readDirection));}
void PulsarView::exportGyASCII() {exportASCIIpulse(pulse->get_Grad(phaseDirection));}
void PulsarView::exportGzASCII() {exportASCIIpulse(pulse->get_Grad(sliceDirection));}


void PulsarView::exportMampASCII() {exportASCIImag(mag->get_Mamp());}
void PulsarView::exportMphaASCII() {exportASCIImag(mag->get_Mpha());}
void PulsarView::exportMzASCII() {exportASCIImag(mag->get_Mz());}

void PulsarView::exportPulseLDR() {
  STD_string fname=get_save_filename("Export JCAMP-DX Pulse", "", "", this);
  if(fname!="") {
    OdinPulse sd(*pulse);
    sd.set_filemode(include);
    sd.write(fname);
  }
}

void PulsarView::exportMagLDR() {
  STD_string fname=get_save_filename("Export JCAMP-DX Magnetization", "", "", this);
  if(fname!="") {
    SeqSimMagsi sm(*mag);
    sm.set_filemode(include);
    sm.write(fname);
  }
}

void PulsarView::edit_systemInfo() {
  LDRwidgetDialog* dlg=new LDRwidgetDialog(pulse->get_systemInfo(),1,this);
  connect(dlg,SIGNAL(valueChanged()),this,SLOT(updatePulse()));
}

