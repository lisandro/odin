/***************************************************************************
                          seqacq.h  -  description
                             -------------------
    begin                : Wed Aug 8 2001
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQACQ_H
#define SEQACQ_H

#include <tjutils/tjhandler.h>

#include <odinseq/seqobj.h>
#include <odinseq/seqfreq.h>
#include <odinseq/seqvec.h>
#include <odinseq/seqdriver.h>


class SeqGradChanParallel; // forward declaration


////////////////////////////////////////////////////////////////

/**
  * @ingroup odinseq_internals
  * This is the abstract base class for all objects that have the
  * property of acquiring a signal
  */
class SeqAcqInterface : public virtual SeqFreqChanInterface {

 public:

/**
  * Returns the duration of the acquisition window without padding delays.
  */
  virtual double get_acquisition_duration() const {if(marshall) return marshall->get_acquisition_duration(); else marshall_error(); return 0.0;}

/**
  * Returns the the duration from the the beginnig of the acquisition
  * object to the middle of the acquisition window.
  */
  virtual double get_acquisition_center() const {if(marshall) return marshall->get_acquisition_center(); else marshall_error(); return 0.0;}

/**
  * Returns the the duration from the the beginnig of the acquisition
  * object to the beginnig of the acquisition window (the point
  * of time where data starts to be acquired).
  */
  virtual double get_acquisition_start() const {if(marshall) return marshall->get_acquisition_start(); else marshall_error(); return 0.0;}


/**
  * Returns the total number of sampling points, without oversampling
  */
  virtual unsigned int get_npts() const {if(marshall) return marshall->get_npts(); else marshall_error(); return 0;}




/**
  * Sets the sweepwidth 'sw' without oversampling and oversampling factor 'os_factor' for this acquisition object, os_factor=1 means no oversampling.
  */
  virtual SeqAcqInterface& set_sweepwidth(double sw, float os_factor) {if(marshall) marshall->set_sweepwidth(sw,os_factor); else marshall_error(); return *this;}


/**
  * Returns the sweepwidth factor
  */
  virtual double get_sweepwidth() const {if(marshall) return marshall->get_sweepwidth(); else marshall_error(); return 0.0;}

/**
  * Returns the oversampling factor
  */
  virtual float get_oversampling() const {if(marshall) return marshall->get_oversampling(); else marshall_error(); return 0.0;}



/**
  * Specifies a regridding matrix for this ADC (for automatic reconstruction)
  */
  virtual SeqAcqInterface& set_readout_shape(const fvector& shape, unsigned int dstsize) {if(marshall) marshall->set_readout_shape(shape, dstsize); else marshall_error(); return *this;}


/**
  * Specifies a vector 'vec' which indexes the data dimension 'dim'.
  * The current index for each acquisition is retrieved from the vector.
  * This is relevant for automatic reconstruction.
  * An additional vector 'valvec' of double values can be given,
  * which is attached to this dimension, e.g. different TEs in the echo
  * dimension.
  */
  virtual SeqAcqInterface& set_reco_vector(recoDim dim, const SeqVector& vec, const dvector& valvec=dvector()) {if(marshall) marshall->set_reco_vector(dim, vec, valvec); else marshall_error(); return *this;}



/**
  * Sets the default value 'index' for the reco index in dimension 'dim'.
  * This is relevant for automatic reconstruction.
  */
  virtual SeqAcqInterface& set_default_reco_index(recoDim dim, unsigned int index) {if(marshall) marshall->set_default_reco_index(dim, index); else marshall_error(); return *this;}


/**
  * Sets the template mode of this acquisition object.
  * This is also relevant for automatic reconstruction.
  */
  virtual SeqAcqInterface& set_template_type(templateType type)  {if(marshall) marshall->set_template_type(type); return set_default_reco_index(templtype,type);}


/**
  * Specifies whether the data points should be reversed, only relevant
  * for automatic reconstruction
  */
  virtual SeqAcqInterface& set_reflect_flag(bool flag)  {if(marshall) marshall->set_reflect_flag(flag); else marshall_error(); return *this;}



 protected:
  SeqAcqInterface() : marshall(0) {}
  virtual ~SeqAcqInterface() {}

  void set_marshall(SeqAcqInterface* mymarshall) {marshall=mymarshall;} // to be used in constructor code

 private:
  friend class SeqAcqDeph;

  // to be used by SeqAcqDeph to retrieve pre-dephasing gradients from acquisition object.
  // Overload this function if your acq needs pre-dephasers, return them by appending them to 'dephobj'.
  // Returns any possible vector object as the return value.
  virtual const SeqVector* get_dephgrad(SeqGradChanParallel& dephobj, bool rephase) const {if(marshall) return marshall->get_dephgrad(dephobj,rephase); return 0;}

  SeqAcqInterface* marshall; // for marshalling member functions to a sub-object

};



/////////////////////////////////////////////////////////////////////////


/**
  * @ingroup odinseq_internals
  * The base class for platform specific drivers of acquisition windows
  */
class SeqAcqDriver : public SeqDriverBase {

 public:
  SeqAcqDriver() {}
  virtual ~SeqAcqDriver() {}

/**
  * Returns possible sweepwidth closest to 'desired_sweep_widht'
  */
  virtual double adjust_sweepwidth(double desired_sweep_widht) const = 0;

/**
  * Prepare driver, let driver adjust reco coordinate 'recoindex'
  */
  virtual bool prep_driver(kSpaceCoord& recoindex, double sweepwidth, unsigned int nAcqPoints, double acqcenter, int freqchannel) = 0;

/**
  * Delay prior to actual acquisition
  */
  virtual double get_predelay() const = 0;

/**
  * Delay following actual acquisition depending on 'sweepwidth'
  */
  virtual double get_postdelay(double sweepwidth) const = 0;

  virtual void event(eventContext& context, double start) const = 0;

  virtual STD_string get_program(programContext& context, unsigned  int phaselistindex) const = 0;

  virtual STD_string get_instr_label() const = 0;

  virtual unsigned int get_numof_channels() const = 0;

  virtual SeqAcqDriver* clone_driver() const = 0;
};


/////////////////////////////////////////////////////////////////////////


/**
  * @addtogroup odinseq
  * @{
  */

/**
  * \brief  Acquisition
  *
  * This class represents an acquisition window
  */
class SeqAcq : public virtual SeqAcqInterface, public SeqObjBase, public SeqFreqChan {

 public:

/**
  * Constructs an acquisition window labeled 'object_label' with the following properties:
  * - nAcqPoints:  The number of sampling points that will be acquired during the acquisition
  * - sweepwidth:  The sampling frequency without oversampling
  * - os_factor:   The oversampling factor, os_factor=1 means no oversampling
  * - nucleus:     The nucleus for which the acquisition should be performed. The default
  *                is to acquire on the protons resonance freuency.
  * - phaselist:   This is an array of phases at which the acquired signal will be
  *                demodulated. The actual phase for each acquisition is determined
  *                by cycling through this array of phases which are represented
  *                in degree.
  * - freqlist:    This is an array of frequency offsets at which the acquired signal
  *                will be demodulated.
  */
  SeqAcq(const STD_string& object_label, unsigned int nAcqPoints, double sweepwidth,
                              float os_factor=1.0, const STD_string& nucleus="",
                              const dvector& phaselist=0,const dvector& freqlist=0);



/**
  * Constructs an acquisition window which is a copy of 'sa'
  */
  SeqAcq(const SeqAcq& sa);

/**
  * Constructs an empty acquisition window with the given label.
  */
  SeqAcq(const STD_string& object_label="unnamedSeqAcq");

/**
  * Destructor
  */
  ~SeqAcq();


/**
  * Sets the number of sampling points that will be acquired during the acquisition
  */
  SeqAcq& set_npts(unsigned int nAcqPoints);


/**
  * Returns the sweep width
  */
  double get_sweep_width() const {return sweep_width;}


/**
  * This assignment operator will make this object become an exact copy of 'sa'.
  */
  SeqAcq& operator = (const SeqAcq& sa);


/**
  * Specifies a an explicit k-space Trajectory for this ADC (for automatic reconstruction)
  */
  void set_kspace_traj(const farray& kspaceTraj);


/**
  * Specifies a complex vector which is is used to weight each point in this ADC (for automatic reconstruction)
  */
  void set_weight_vec(const cvector& weightVec);


/**
  * Sets the relative point in time (between 0 and 1) when k-space center is passed, the default is 0.5.
  * Will be used in get_acquisition_center().
  */
  SeqAcq& set_rel_center(double relative_kspace_center) {rel_center=relative_kspace_center; return *this;}


/**
  * Returns the k-space index of this ADCs
  */
  const kSpaceCoord& get_kcoord() const;


  // overwriting virtual functions of SeqAcqInterface
  double get_acquisition_duration() const {return secureDivision(npts,sweep_width);}
  double get_acquisition_center() const;
  double get_acquisition_start() const {return acqdriver->get_predelay();}
  unsigned int get_npts() const {return npts;}
  SeqAcqInterface& set_sweepwidth(double sw, float os_factor);
  double get_sweepwidth() const {return sweep_width;}
  float get_oversampling() const {return oversampl;}
  SeqAcqInterface& set_readout_shape(const fvector& shape, unsigned int dstsize);
  SeqAcqInterface& set_reco_vector(recoDim dim, const SeqVector& vec, const dvector& valvec=dvector());
  SeqAcqInterface& set_default_reco_index(recoDim dim, unsigned int index);
  SeqAcqInterface& set_reflect_flag(bool flag) {reflect_flag=flag; return *this;}


  // overwriting virtual functions of SeqTreeObj
  STD_string get_program(programContext& context) const;
  double get_duration() const;
  SeqValList get_freqvallist(freqlistAction action) const;
  void query(queryContext& context) const {SeqTreeObj::query(context); context.numof_acqs=1;}
  RecoValList get_recovallist(unsigned int reptimes, LDRkSpaceCoords& coords) const;
  unsigned int event(eventContext& context) const;
  STD_string get_properties() const;

 private:
  friend class SeqMethod;
  friend class SeqEpiDriverParavision;
  friend class SeqEpiDriverEpic;

  void common_init();


  // overwriting virtual functions of SeqFreqChan
  double get_freqchan_duration() const {return get_acquisition_duration();}
  STD_string get_driver_instr_label() const {return acqdriver->get_instr_label();}


  // overwriting virtual functions of SeqClass
  bool prep();


  mutable kSpaceCoord kcoord;

  mutable SeqDriverInterface<SeqAcqDriver> acqdriver;

  double sweep_width;
  unsigned int npts;
  float oversampl;
  double rel_center;
  bool reflect_flag;
  int readoutIndex;
  int trajIndex;
  int weightIndex;

  // safe pointers to vectors to create indices for the ADC
  // pointer to pointer is used here to avoid delete[] bug in GCC2.9
  Handler<const SeqVector*>** dimvec;

  int default_recoindex[n_recoIndexDims];


};

/** @}
  */

#endif
