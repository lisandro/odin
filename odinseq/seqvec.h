/***************************************************************************
                          seqvec.h  -  description
                             -------------------
    begin                : Mon Aug 19 2002
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQVEC_H
#define SEQVEC_H

#include <odinseq/seqclass.h>

class SeqCounter; // forward declaration
class SeqReorderVector; // forward declaration

///////////////////////////////////////////////////////////////////////////////

/**
  * @ingroup odinseq
  * This enum can be used to determine the encoding scheme of a list of values:
  * - linearEncoding :     values are played out in the same order as specified
  * - reverseEncoding :    values are played out in the reverse order as specified
  * - centerOutEncoding :  encoding start in the center of values and moves outwards alternatingly in positive/negative direction
  * - centerInEncoding :  encoding starts outwards and moves inwards alternatingly in positive/negative direction
  * - maxDistEncoding :  Maximum distance between consecutive elements
  */
enum encodingScheme {linearEncoding=0, reverseEncoding, centerOutEncoding, centerInEncoding, maxDistEncoding};


/**
  * @ingroup odinseq
  * This enum can be used to determine the reordering scheme of a list of values:
  * - noReorder :     No reordering is performed, the number of reorderings is then equal one
  * - rotateReorder : The values will be rotated, the number of reorderings is then the number of values
  * - blockedSegmented : The values will be divided into segments, preserving their order
  * - interleavedSegmented : The values will be divided into segments like distributing game cards
  */
enum reorderScheme {noReorder=0,rotateReorder,blockedSegmented,interleavedSegmented};



/**
  * @ingroup odinseq_internals
  * This enum holds information about how a certain vector and its reordering
  * are nested in the sequence tree:
  * - noRelation :    They are not nested
  * - reorderInner :  The reorder vector is attached to an inner loop
  * - vecInner :      The reorder vector is attached to an outer loop
  * - unrolledLoop :  The loop which uses the vector will be unrolled in the program
  */
enum nestingRelation {noRelation=0, reorderInner, vecInner, unrolledLoop};

///////////////////////////////////////////////////////////////////////////////

/**
  * @ingroup odinseq_internals
  * This is the base class for all vector objects, i.e. objects that can
  * can take different values while looping over them.
  */
class SeqVector : public ListItem<SeqVector>, public Handled<const SeqVector*>, public virtual SeqClass {

 public:

/**
  * Default Constructor
  */
  SeqVector(const STD_string& object_label="unnamedSeqVector");

/**
  * Constructs an index vector labeled 'object_label' with the following properties:
  * - nindices:  the total number of iterations
  * - slope:  the number of indices per iteration
  * - offset:  the offset of the indices, i.e. the index of the first iteration
  */
  SeqVector(const STD_string& object_label, unsigned int nindices, int slope=1, int offset=0 );

/**
  * Copy Constructor
  */
  SeqVector(const SeqVector& sv);

/**
  * Destructor
  */
  virtual ~SeqVector();

/**
  * Assignment operator that makes this vector become a copy of 'sv'
  */
  SeqVector& operator = (const SeqVector& sv);


/**
  * Overload this function to return the number of elements in the vector
  * (regardless of reordering)
  */
  virtual unsigned int get_vectorsize() const {return indexvec.size();}


/**
  * Returns the number of iterations with proper
  * consideration of reordering, e.g. if the vector has N values distributed
  * over M reordering segments, this function will return N/M.
  */
  virtual unsigned int get_numof_iterations() const;


/**
  * Overload this function to trigger a check whether the loop must possibly be unrolled
  * in pulse or gradient program
  */
  virtual bool needs_unrolling_check() const {return false;}


/**
  * Overload this function to prepare objects at run-time before the execution of a loop repetition
  * return 'false' when preparation fails
  */
  virtual bool prep_iteration() const {return true;}


/**
  * Overload this function to return the current index, intended to use it in
  * combination with reordering of phase encoding steps or slices
  */
  virtual int get_current_index() const;

/**
  * Returns the current index relevant for acquisition (reco dimensions).
  * This is the value of the index vector (set_indexvec) at the position
  * returned by 'get_current_index'.
  */
  int get_acq_index() const;

/**
  * Overload this function to tell whether the vector qualitatively alters the
  * sequence concerning timings and RF power deposition.
  *  This is useful for accelerating duration and SAR calculations (Siemens).
  */
  virtual bool is_qualvector() const;

/**
  * Returns whether this vector is used to specify an acquisition dimension through the
  * set_*_vector(...) functions of all classes derived from SeqAcqInterface
  */
  virtual bool is_acq_vector() const;

/**
  * Returns whether this vector uses a different set of sequence objects at each repetition
  */
  virtual bool is_obj_vector() const {return false;}


/**
  * If the vector is used as a variable in the code-generated pulse program,
  * overload this function to return a unique label/command for the vector
  * which is either attached to the loop (Bruker), or executed at the beginning
  * of the loop (EPIC).
  */
  virtual svector get_vector_commands(const STD_string& iterator) const {return svector();}


/**
  * Overload this function to return the command to iterate to the next item at the end of the loop
  */
  virtual STD_string get_loopcommand() const {return "";}


/**
  * Sets the vector of indices 'iv' which will be returned by get_acq_index,
  * i.e. the assign reco indices (positions in k-space)
  */
  SeqVector& set_indexvec(const ivector& iv) {indexvec=iv; return *this;}

/**
  * Returns the vector of indices 'iv' which will be returned by get_acq_index,
  * i.e. the assign reco indices (positions in k-space)
  */
  ivector get_indexvec() const {return indexvec;}


/**
  * Sets the reordering scheme and the number of segments
  */
  SeqVector& set_reorder_scheme(reorderScheme scheme, unsigned int nsegments=1);

/**
  * Sets the encoding scheme
  */
  SeqVector& set_encoding_scheme(encodingScheme scheme);


/**
  * Returns the reordering vector (for loop insertion)
  */
  const SeqVector& get_reorder_vector() const;


/**
  * Returns the nesting relation between the vector and its reordering vector
  */
  virtual nestingRelation get_nesting_relation() const;


/**
  * Returns a matrix whereby the elements of rows (second dimension)
  * contain the reordered index for each iteration. The other
  * dimension (first dimension) represents reordering iterations.
  */
  iarray get_index_matrix() const;

/**
  * Returns the current index of the reordering vector
  */
  int get_current_reord_index() const;

/**
  * Returns the iterator as modified by the reorder vector as C code
  */
  STD_string get_reord_iterator(const STD_string& iterator) const;


 protected:

/**
  * Tell reorder vector what to do within a loop
  */
  virtual svector get_reord_vector_commands(const STD_string& iterator) const {return svector();}

/**
  * Returns true if vector is actually handled by a loop
  */
  bool is_handled() const {return vechandler.get_handled();}

 private:
  friend class SeqReorderVector;
  friend class SeqCounter;
  friend class SeqObjLoop;
  friend class SeqSimultanVector;

#ifdef SELF_FRIEND_CLASS
#ifndef USING_GCC
  friend class SeqVector; // making the class itself a friend is required for MSVC6 !!
#endif
#endif

/**
  * Returns whether the loop that controls this vector is currently iterating
  */
  bool loopcounter_is_active() const;

/**
  * Returns the current iteration
  */
  int get_loopcounter() const;


  void common_int();


  virtual const SeqVector& set_vechandler(const SeqCounter *sc) const;

  ivector indexvec;

  Handler<const SeqCounter*> vechandler;
  Handler<const SeqVector*>  simhandler;

  mutable SeqReorderVector* reordvec;

  mutable nestingRelation nr_cache;
  mutable bool nr_cache_up2date;
};

///////////////////////////////////////////////////////////////////////////////



/**
  * @ingroup odinseq_internals
  * This vector class is used to change the order of a list of values, e.g.
  * to change the order of frequencies to reorder slice selection
  */
class SeqReorderVector : public SeqVector {

 public:
  SeqReorderVector(const SeqVector* user, const SeqReorderVector* copy_templ=0);

  unsigned int get_reordered_size(unsigned int vecsize) const;

  int get_reordered_index(int counter) const {return get_reordered_index(counter,get_current_index());}

  
 private:
  friend class SeqVector;
 
  // implemented virtual functions of SeqVector
  unsigned int get_vectorsize() const;
  bool needs_unrolling_check() const {return reord_scheme!=noReorder;}
  bool prep_iteration() const {return reorder_user->prep_iteration();}
  bool is_qualvector() const {return reorder_user->is_qualvector();}
  bool is_acq_vector() const {return reorder_user->is_acq_vector();}
  svector get_vector_commands(const STD_string& iterator) const;


  int get_reordered_index(int counter, int reord_iteration) const;

  STD_string get_reordered_iterator(const STD_string& iterator) const;

  void clear_cache() {reord_iterator_cache="";}

  reorderScheme reord_scheme;
  unsigned int n_reord_segments;

  encodingScheme encoding_scheme;

  const SeqVector* reorder_user;

  mutable STD_string reord_iterator_cache;
};


#endif
