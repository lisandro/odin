/***************************************************************************
                          seqacqepi.h  -  description
                             -------------------
    begin                : Wed Aug 8 2001
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQACQEPI_H
#define SEQACQEPI_H

#include <odinseq/seqacq.h>
#include <odinseq/seqgrad.h>
#include <odinseq/seqlist.h>
#include <odinseq/seqloop.h>
#include <odinseq/seqdelay.h>
#include <odinseq/seqgradtrapez.h>
#include <odinseq/seqgradconst.h>
#include <odinseq/seqparallel.h>
#include <odinseq/seqdriver.h>


/**
  * @ingroup odinseq_internals
  * The base class for platform specific EPI drivers
  */
class SeqEpiDriver : public SeqDriverBase, public SeqObjList, public virtual SeqAcqInterface, public virtual SeqGradInterface {

 public:

  SeqEpiDriver() {}
  virtual ~SeqEpiDriver() {}

  virtual void init_driver(const STD_string& object_label,double sweepwidth,
            float kread_min,  float kread_max,  unsigned readntps,
            float kphase_min, float kphase_max, unsigned phasentps,int startindex_phase,
            bool ramp_sampling,rampType rampmode, float ramp_steepness,
            const STD_string& nucleus, const dvector& phaselist, const dvector& freqlist, unsigned int echo_pairs) = 0;

  virtual unsigned get_npts_read() const = 0;

  virtual unsigned int get_numof_gradechoes() const = 0;

  virtual double get_echoduration() const = 0;

  virtual double get_ramp_rastertime() const = 0;

  virtual float get_gradintegral2center_read() const = 0;

  virtual float get_gradintegral2center_phase() const = 0;

  virtual fvector get_readout_shape() const = 0;

  virtual const kSpaceCoord& get_kcoord_template(unsigned int& padded_zeroes) const = 0;

  virtual SeqEpiDriver* clone_driver() const = 0;

  unsigned int get_npts() const {return get_npts_read()*get_numof_gradechoes();}


 protected:
  SeqEpiDriver(const SeqEpiDriver& seda) : SeqObjList(seda)  {SeqEpiDriver::operator = (seda);}

  SeqEpiDriver& operator = (const SeqEpiDriver& seda) {
    SeqObjList::operator =(seda);
    return *this;
  }


};


/////////////////////////////////////////////////////////////////////////////////////

/**
  * @ingroup odinseq_internals
  * The default EPI driver
  */
class SeqEpiDriverDefault :  public SeqEpiDriver {

  // virtual functions of SeqFreqChanInterface are marhshalled to adc

 public:
  SeqEpiDriverDefault();
  ~SeqEpiDriverDefault() {}

  SeqEpiDriverDefault(const SeqEpiDriverDefault& sedi);

  void init_driver(const STD_string& object_label,double sweepwidth,
            float kread_min,  float kread_max,  unsigned readntps,
            float kphase_min, float kphase_max, unsigned phasentps,int startindex_phase,
            bool ramp_sampling, rampType rampmode, float ramp_steepness,
            const STD_string& nucleus,
            const dvector& phaselist, const dvector& freqlist, unsigned int echo_pairs);


  // overloading virtual functions from SeqDriverBase
  odinPlatform get_driverplatform() const {return standalone;}


  // implemented virtual functions from SeqEpiDriver
  unsigned int get_npts_read() const {return adc.get_npts();}
  unsigned int get_numof_gradechoes() const;
  double get_echoduration() const {return posread.get_gradduration();}
  double get_ramp_rastertime() const {return posread.get_timestep();}
  float get_gradintegral2center_read() const {return gradint2center_read;}
  float get_gradintegral2center_phase() const {return gradint2center_phase;}
  fvector get_readout_shape() const {return readshape;}
  const kSpaceCoord& get_kcoord_template(unsigned int& padded_zeroes) const {padded_zeroes=0; return adc.get_kcoord();}
  SeqEpiDriver* clone_driver() const {return new SeqEpiDriverDefault(*this);}


  // marshalling virtual functions of SeqAcqInterface to adc, except:
  double get_acquisition_center() const;
  double get_acquisition_start() const;
  SeqAcqInterface& set_template_type(templateType type);




  // implementing virtual functions of SeqGradInterface
  SeqGradInterface& set_strength(float gradstrength) {return *this;}
  SeqGradInterface& invert_strength() {kernel.invert_strength(); return *this;}
  float get_strength() const {return kernel.get_strength();}
  fvector get_gradintegral() const;
  double get_gradduration() const {return kernel.get_gradduration();}
  SeqGradInterface& set_gradrotmatrix(const RotMatrix& matrix) {kernel.set_gradrotmatrix(matrix); return *this;}


 private:
  void build_seq();

  SeqAcq adc;
  SeqDelay acqdelay_begin;
  SeqDelay acqdelay_middle;
  SeqDelay acqdelay_end;

  SeqGradTrapez posread;
  SeqGradTrapez negread;

  SeqGradTrapez phaseblip1st;
  SeqGradTrapez phaseblip2nd;
  SeqGradDelay phasezero1st;
  SeqGradDelay phasezero2nd;
  SeqGradDelay phasezero_lastblip;

  SeqGradChanParallel gradkernel;
  SeqGradChanParallel lastgradkernel;
  SeqObjList oneadckernel;
  SeqObjList adckernel;
  SeqObjList lastadckernel;
  SeqParallel kernel;
  SeqParallel lastkernel;

  SeqObjLoop loop;

  float gradint2center_read;
  float gradint2center_phase;
  int centerindex_phase;
  fvector readshape;
  templateType templtype;
  int echopairs;
  bool lastecho;
};



/////////////////////////////////////////////////////////////////////////////////////

class SeqAcqEPIdephObjs; // forward declaration

/**
  * @addtogroup odinseq
  * @{
  */

/**
  * \brief  Acquisition + echo-planar imaging readout
  *
  * This class represents an acquisition window with an EPI gradient.
  */
class SeqAcqEPI : public SeqObjBase, public virtual SeqAcqInterface, public virtual SeqGradInterface  {

 public:

/**
  * Constructs an acquisition window with an EPI gradient labeled 'object_label' with the
  * following properties:
  * - sweepwidth: The sampling frequency
  * - read_size:   The number of sampling points per gradient echo, with ramp_sampling enabled, this is the number of points after regridding
  * - FOVread:    The Field Of View in read direction
  * - phase_size: The final matrix size in phase direction without partial fourier undersampling
  * - FOVphase:   The Field Of View in phase direction
  * - shots:      Number of shots, a multi-shot (segmented) EPI readout will be created if 'shots' is larger than 1
  * - reduction:  Reduction factor (parallel imaging)
  * - os_factor:  The oversampling factor, os_factor=1 means no oversampling
  * - nucleus:    The nucleus to measure
  * - phaselist:  Phase list for acquisition
  * - freqlist:   Frequency list for acquisition
  * - rampmode:    This parameter determines the shape of the ramp waveform
  * - ramp_sampling: The EPI readout performs data acquisition during the ramps if this parameter is set to 'true'
  * - ramp_steepness: This parameter in the range of ]0,1] determines the relative rising
  *                  speed of the gradient strength, i.e. with 1 the gradients are switched
  *                  as fast as possible
  * - fourier_factor: The amount of undersampling (0=no undersampling, 1=half fourier)
  * - echo_pairs: Number of interleaved acquired pairs of echoes for one phase-encodin step, 0 means only one echo
  */
  SeqAcqEPI(const STD_string& object_label, double sweepwidth,
            unsigned int read_size, float FOVread,
            unsigned int phase_size, float FOVphase,
            unsigned int shots=1, unsigned int reduction=1, float os_factor=1.0, const STD_string& nucleus="",
            const dvector& phaselist=0, const dvector& freqlist=0,
            rampType rampmode = linear, bool ramp_sampling=false, float ramp_steepness=1.0,
            float fourier_factor=0.0, unsigned int echo_pairs=0, bool invert_partial_fourier=false);



/**
  * Constructs an acquisition window with an EPI gradient which is a copy of 'sae'
  */
  SeqAcqEPI(const SeqAcqEPI& sae);

/**
  * Constructs an empty acquisition window with an EPI gradient with the given label.
  */
  SeqAcqEPI(const STD_string& object_label="unnamedSeqAcqEPI");

/**
  * Destructor
  */
  ~SeqAcqEPI();

/**
  * Returns the number of points that will be acquired during each read gradient period,
  * including the ramp points.
  */
  unsigned int get_npts_read() const {return driver->get_npts_read();}

/**
  * Returns the number of gradient echoes, i.e. the number of k-space lines that will
  * be sampled
  */
  unsigned int get_numof_gradechoes() const {return driver->get_numof_gradechoes();}

/**
  * Returns the readout gradient shape that is used for regridding
  */
  fvector get_readout_shape() const {return driver->get_readout_shape();}

/**
  * This assignment operator will make this object become an exact copy of 'sae'.
  */
  SeqAcqEPI& operator = (const SeqAcqEPI& sae);


  // forwarding virtual functions of SeqAcqInterface to driver
  // we cannot use marshalling with a fixed pointer here because
  // driver may be changed when switching platform
  double get_acquisition_center() const {return driver->get_acquisition_center();}
  double get_acquisition_start() const {return driver->get_acquisition_start();}
  unsigned int get_npts() const {return driver->get_npts();}
  SeqAcqInterface& set_sweepwidth(double sw, float os_factor); // produce warning
  double get_sweepwidth() const {return driver->get_sweepwidth();}
  float get_oversampling() const {return os_factor_cache;}
  SeqAcqInterface& set_template_type(templateType type);
  SeqAcqInterface& set_reco_vector(recoDim dim, const SeqVector& vec, const dvector& valvec=dvector()) {driver->set_reco_vector(dim,vec,valvec); return *this;}
  SeqAcqInterface& set_default_reco_index(recoDim dim, unsigned int index) {driver->set_default_reco_index(dim,index); return *this;}

  // forwarding virtual functions of SeqFreqChanInterface to driver
  SeqFreqChanInterface& set_nucleus(const STD_string& nucleus) {driver->set_nucleus(nucleus); return *this;}
  SeqFreqChanInterface& set_freqlist(const dvector& freqlist) {driver->set_freqlist(freqlist); return *this;}
  SeqFreqChanInterface& set_phaselist(const dvector& phaselist) {driver->set_phaselist(phaselist); return *this;}
  const SeqVector& get_freqlist_vector() const {return driver->get_freqlist_vector();}
  const SeqVector& get_phaselist_vector() const {return driver->get_phaselist_vector();}

  // forwarding virtual functions of SeqGradInterface to driver
  SeqGradInterface& set_strength(float gradstrength) {driver->set_strength(gradstrength); return *this;}
  SeqGradInterface& invert_strength() {driver->invert_strength(); return *this;}
  float get_strength() const {return driver->get_strength();}
  fvector get_gradintegral() const {return driver->get_gradintegral();}
  double get_gradduration() const {return driver->get_gradduration();}
  SeqGradInterface& set_gradrotmatrix(const RotMatrix& matrix) {driver->set_gradrotmatrix(matrix); return *this;}


  // forwarding virtual functions of SeqTreeObj to driver
  STD_string get_program(programContext& context) const {return driver->get_program(context);}
  double get_duration() const {return driver->get_duration();}
  SeqValList get_freqvallist(freqlistAction action) const {return driver->get_freqvallist(action);}
  void query(queryContext& context) const {driver->query(context);}
  RecoValList get_recovallist(unsigned int reptimes, LDRkSpaceCoords& coords) const;
  bool contains(const SeqTreeObj* sto) const {return driver->contains(sto);}
  void tree(SeqTreeCallbackAbstract* display) const {driver->tree(display);}
  unsigned int event(eventContext& context) const {return driver->event(context);}
  STD_string get_properties() const {return driver->get_properties();}


 private:

  SeqVector& get_mutable_freqlist_vector() {return driver->get_mutable_freqlist_vector();}
  SeqVector& get_mutable_phaselist_vector() {return driver->get_mutable_phaselist_vector();}

  // overloaded for SeqAcqDeph
  const SeqVector* get_dephgrad(SeqGradChanParallel& dephobj, bool rephase) const;

  // overwriting virtual functions from SeqClass
  bool prep();

  void common_init();

  void create_deph_and_reph();


  unsigned int readsize_os_cache;
  float os_factor_cache;
  unsigned int phasesize_cache;
  unsigned int segments_cache;
  unsigned int reduction_cache;
  int echo_pairs_cache;
  float blipint_cache;
  templateType templtype_cache;
  rampType ramptype_cache;

  mutable SeqDriverInterface<SeqEpiDriver> driver;

  SeqAcqEPIdephObjs* dephobjs;
};


/** @}
  */

#endif
