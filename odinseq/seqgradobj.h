/***************************************************************************
                          seqgradobj.h  -  description
                             -------------------
    begin                : Tue Aug 13 2002
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQGRADOBJ_H
#define SEQGRADOBJ_H

#include <tjutils/tjhandler.h>

#include <odinseq/seqtree.h>
#include <odinseq/seqgrad.h>
#include <odinseq/seqlist.h>


class SeqObjList; // forward declaration
class SeqRotMatrixVector; // forward declaration

/**
  * @addtogroup odinseq_internals
  * @{
  */


/**
  * This is the abstract base class for all gradient objects that can be  part of a sequence
  */
class SeqGradObjInterface : public virtual SeqGradInterface, public virtual SeqTreeObj, public Handled<SeqGradObjInterface*>, public Handled<const SeqGradObjInterface*> {

 public:

  // overloading virtual functions from SeqTreeObj
  double get_duration() const;


  double get_pulprogduration() const; // Do we still need this?


  virtual bool need_gp_terminator() const {return false;}

 protected:
  SeqGradObjInterface(const STD_string& object_label="unnamedSeqGradObjInterface");
  SeqGradObjInterface(const SeqGradObjInterface& sgoa);
  SeqGradObjInterface& operator = (const SeqGradObjInterface& sgoa);

};



/** @}
  */


#endif
