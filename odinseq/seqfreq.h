/***************************************************************************
                          seqfreq.h  -  description
                             -------------------
    begin                : Wed Aug 14 2002
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/



#ifndef SEQFREQ_H
#define SEQFREQ_H

#include <odinseq/seqclass.h>
#include <odinseq/seqvec.h>
#include <odinseq/seqphase.h>
#include <odinseq/seqdriver.h>


///////////////////////////////////////////////////////////////////

/**
  * @ingroup odinseq_internals
  * Abstract interfac class for all sequence objects that occupy a
  * transmitter/receiver channel, e.g. pulse objects or acquisition objects 
  */
class SeqFreqChanInterface : public virtual SeqClass  {

 public:
/**
  * Specify the nucleus for this frequency channel
  */
  virtual SeqFreqChanInterface& set_nucleus(const STD_string& nucleus) {if(marshall) marshall->set_nucleus(nucleus); else marshall_error(); return *this;}


/**
  * Sets the freqlist for the object
  */
  virtual SeqFreqChanInterface& set_freqlist(const dvector& freqlist) {if(marshall) marshall->set_freqlist(freqlist); else marshall_error(); return *this;}

/**
  * Sets the phaselist for the object
  */
  virtual SeqFreqChanInterface& set_phaselist(const dvector& phaselist) {if(marshall) marshall->set_phaselist(phaselist); else marshall_error(); return *this;}

/**
  * Returns the frequency list vector (for loop insertion)
  */
  virtual const SeqVector& get_freqlist_vector() const {if(marshall) return marshall->get_freqlist_vector(); else marshall_error(); return get_dummyvec();}


/**
  * Returns the phaselist vector (for loop insertion)
  */
  virtual const SeqVector& get_phaselist_vector() const {if(marshall) return marshall->get_phaselist_vector(); else marshall_error(); return get_dummyvec();}

/**
  * Returns the frequency list vector (for loop insertion)
  */
  operator const SeqVector& () const {return get_freqlist_vector();}

/**
  * Sets the frequency encoding scheme
  */
  virtual SeqFreqChanInterface& set_encoding_scheme(encodingScheme scheme) {if(marshall) marshall->get_mutable_freqlist_vector().set_encoding_scheme(scheme); else marshall_error(); return *this;}

/**
  * Sets the reordering scheme for the frequency list
  */
  virtual SeqFreqChanInterface& set_reorder_scheme(reorderScheme scheme, unsigned int nsegments=1) {if(marshall) marshall->get_mutable_freqlist_vector().set_reorder_scheme(scheme,nsegments); else marshall_error(); return *this;}

/**
  * Returns the reordering vector of the frequency list (for loop insertion)
  */
  virtual const SeqVector& get_reorder_vector() const {if(marshall) return marshall->get_freqlist_vector().get_reorder_vector(); else marshall_error(); return get_dummyvec();}

/**
  * Sets the encoding scheme of the phase list
  */
  virtual SeqFreqChanInterface& set_phaselist_encoding_scheme(encodingScheme scheme) {if(marshall) marshall->get_mutable_phaselist_vector().set_encoding_scheme(scheme); else marshall_error(); return *this;}

/**
  * Sets the reordering scheme for the phase list
  */
  virtual SeqFreqChanInterface& set_phaselist_reorder_scheme(reorderScheme scheme, unsigned int nsegments=1) {if(marshall) marshall->get_mutable_phaselist_vector().set_reorder_scheme(scheme,nsegments); else marshall_error(); return *this;}

/**
  * Returns the reordering vector of the phase list (for loop insertion)
  */
  virtual const SeqVector& get_phaselist_reorder_vector() const {if(marshall) return marshall->get_phaselist_vector().get_reorder_vector(); else marshall_error(); return get_dummyvec();}


/**
  * Sets the frequency list to a single value 'freqoffset'
  */
  SeqFreqChanInterface& set_freqoffset(double freqoffset);

/**
  * Sets the phase list to a single value 'phaseval'
  */
  SeqFreqChanInterface& set_phase(double phaseval);

/**
  * Sets a phase list for RF spoiling of length 'size', multiplicator 'incr' and uniform 'offset'.
  * Defaults are recommended by the 'Handbook of MRI'
  */
  SeqFreqChanInterface& set_phasespoiling(unsigned int size=80, double incr=117.0, double offset=0.0);

 protected:
  SeqFreqChanInterface() {}
  virtual ~SeqFreqChanInterface() {}

  void set_marshall(SeqFreqChanInterface* mymarshall) {marshall=mymarshall;} // to be used in constructor code

 private:
  friend class SeqAcqEPI; // to allow SeqAcqEPI full access to its driver

  SeqFreqChanInterface* marshall; // for marshalling member functions to a sub-object


  virtual SeqVector& get_mutable_freqlist_vector() {if(marshall) return marshall->get_mutable_freqlist_vector(); else marshall_error(); return get_dummyvec();}
  virtual SeqVector& get_mutable_phaselist_vector() {if(marshall) return marshall->get_mutable_phaselist_vector(); else marshall_error(); return get_dummyvec();}

};


///////////////////////////////////////////////////////////////////

/**
  * @ingroup odinseq_internals
  * The base class for platform specific drivers of transmit/receive channels
  */
class SeqFreqChanDriver : public SeqDriverBase {

 public:
  SeqFreqChanDriver() {}
  virtual ~SeqFreqChanDriver() {}

  virtual bool prep_driver(const STD_string& nucleus, const dvector& freqlist) = 0;
  virtual void prep_iteration(double current_frequency, double current_phase, double freqchan_duration) const = 0;
  virtual int get_channel() const = 0;

  virtual STD_string get_iteratorcommand(objCategory cat,int freqlistindex) const = 0;
  virtual svector get_freqvec_commands(const STD_string& iterator, const STD_string& instr) const = 0;

  virtual STD_string get_pre_program(programContext& context, objCategory cat, const STD_string& instr_label, double default_frequency, double default_phase) const = 0;


  virtual SeqFreqChanDriver* clone_driver() const = 0;

  virtual void pre_event (eventContext& context,double starttime) const = 0;
  virtual void post_event(eventContext& context,double starttime) const = 0;
};

///////////////////////////////////////////////////////////////////

/**
  * @ingroup odinseq_internals
  * This is the base class for all sequence objects that occupy a
  * transmitter/receiver channel
  */
class SeqFreqChan : public SeqVector, public virtual SeqFreqChanInterface {

 public:
/**
  * Constructs an empty frequency channel labeled 'object_label'
  */
  SeqFreqChan(const STD_string& object_label="unnamedSeqFreqChan");


/**
  * Constructs a frequency channel labeled 'object_label' with the following properties:
  * - nucleus:     The nucleus for which the frequency channel should be reserved.
  *                 The default is the protons resonance frequency.
  * - freqlist:    This is an array of frequencies at which the transmit/receive signal will be
  *                modulated/demodulated.
  * - phaselist:   This is an array of phases at which the transmit/receive signal will be
  *                modulated/demodulated.
  */
  SeqFreqChan(const STD_string& object_label,const STD_string& nucleus,
                  const dvector& freqlist=0,const dvector& phaselist=0);


/**
  * Constructs a copy of 'sfc'
  */
  SeqFreqChan(const SeqFreqChan& sfc);


/**
  * Destructor
  */
  virtual ~SeqFreqChan() {}


/**
  * Assignment operator that makes this frequency channel become a copy of 'sfc'
  */
  SeqFreqChan& operator = (const SeqFreqChan& sfc);


/**
  * Return the assigned number of this frequency channel
  */
  int get_channel() const {return freqdriver->get_channel();}


/**
  * Returns the freqlist of the object
  */
  dvector get_freqlist() const {return frequency_list;}


/**
  * Returns the current phase (in rad !)
  */
  double get_phase() const {return phaselistvec.get_phase();}


/**
  * Returns the current frequency
  */
  virtual double get_frequency() const;


/**
  * Return the index for the phase list
  */
  unsigned int get_phaselistindex() const {return phaselistvec.get_phaselistindex();}


/**
  * Return the index for the frequency list
  */
  virtual unsigned int get_freqlistindex() const {return 1;}


/**
  * Returns the command to iterate through the frequency list
  */
  STD_string get_iteratorcommand(objCategory cat) const;



  // overloaded virtual functions of SeqFreqChanInterface
  SeqFreqChanInterface& set_nucleus(const STD_string& nucleus);
  SeqFreqChanInterface& set_freqlist (const dvector& freqlist) {frequency_list=freqlist; return *this;}
  SeqFreqChanInterface& set_phaselist(const dvector& phaselist) {phaselistvec.set_phaselist(phaselist); return *this;}
  const SeqVector& get_freqlist_vector() const {return *this;}
  const SeqVector& get_phaselist_vector() const {return phaselistvec;}


  // overloaded virtual functions of SeqVector
  unsigned int get_vectorsize() const {return frequency_list.size();}
  bool prep_iteration() const;
  bool is_qualvector() const {return false;}
  svector get_vector_commands(const STD_string& iterator) const {return freqdriver->get_freqvec_commands(iterator,get_driver_instr_label());}

  // overloaded functions for SeqTreeObj
  SeqValList get_freqvallist(freqlistAction action) const;


 protected:

  STD_string get_pre_program(programContext& context, objCategory cat, const STD_string& instr_label) const;

  void pre_event (eventContext& context,double starttime) const {freqdriver->pre_event(context,starttime);}
  void post_event(eventContext& context,double starttime) const {freqdriver->post_event(context,starttime);}

/**
  * Overload this function to return the duration of the object
  * while it effectively occupies the RF channel
  */
  virtual double get_freqchan_duration() const {return 0.0;}

  // overwriting virtual functions from SeqClass
  bool prep();

 private:
  friend class SeqPhaseListVector;

  // overloaded virtual functions of SeqFreqChanInterface
  SeqVector& get_mutable_freqlist_vector() {return *this;}
  SeqVector& get_mutable_phaselist_vector() {return phaselistvec;}


  double get_default_phase() const {return closest2zero(phaselistvec.get_phaselist());}
  double get_default_frequency() const {return closest2zero(frequency_list);}

  static double closest2zero(const dvector& v);

  // for EPIC
  virtual STD_string get_driver_instr_label() const {return "";}


  // the hardware driver
  mutable SeqDriverInterface<SeqFreqChanDriver> freqdriver;

  // the nucleus of this freq/phase object
  STD_string nucleusName;

  // the frequency list to iterate through
  dvector frequency_list;

  // the phaselist associated with this freq/phase object
  SeqPhaseListVector phaselistvec;
};

/////////////////////////////////////////////////////////////////////////

#endif
