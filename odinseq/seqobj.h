/***************************************************************************
                          seqobj.h  -  description
                             -------------------
    begin                : Wed Aug 8 2001
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/



#ifndef SEQOBJ_H
#define SEQOBJ_H

#include <tjutils/tjlist.h>
#include <tjutils/tjhandler.h>

#include <odinseq/seqtree.h>

/**
  * @addtogroup odinseq_internals
  * @{
  */

/**
  * Base class for elements in a list of sequence objects.
  */
class SeqObjBase : public ListItem<SeqObjBase>, public virtual  SeqTreeObj, public Handled<const SeqObjBase*> {

 protected:
  SeqObjBase(const STD_string& object_label="unnamedSeqObjBase");
  SeqObjBase(const SeqObjBase& soa);
  SeqObjBase& operator = (const SeqObjBase& soa);
};

/** @}
  */



#endif
