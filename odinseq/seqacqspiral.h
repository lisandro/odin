/***************************************************************************
                          seqacqspiral.h  -  description
                             -------------------
    begin                : Tue Nov 11 2003
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQACQSPIRAL_H
#define SEQACQSPIRAL_H

#include <odinseq/seqlist.h>
#include <odinseq/seqparallel.h>
#include <odinseq/seqacq.h>
#include <odinseq/seqdelay.h>
#include <odinseq/seqgradspiral.h>
#include <odinseq/seqrotmatrixvector.h>
#include <odinseq/seqgradtrapez.h>

/**
  * @addtogroup odinseq
  * @{
  */

/**
  * \brief  Acquisition + spiral gradient
  *
  * This class contains a gradient with a spiral k-space trajectory together with an acquisition window
  */
class SeqAcqSpiral : public virtual SeqAcqInterface, public SeqObjList  {

  // virtual functions of SeqFreqChanInterface are marhshalled to acq

 public:


/**
  * Constructs an acquisition window with a simultaneous spiral gradient labeled 'object_label' and the
  * following properties:
  * - sweepwidth:  The sampling frequency
  * - fov:         The Field of View in radial direction
  * - sizeRadial:  The number of image points in radial direction
  * - numofSegments : The number of spiral interleaves
  * - traj:        The spiral trajectory (will be modified, if neccessary)
  * - inout:       Spiral-in spiral-out trajectory
  * - optimize:    Optimize trajectory for minimum readout length if trajectory has a free tunable parameter
  * - nucleus:     See class 'SeqAcq' for explanation
  * - phaselist:   See class 'SeqAcq' for explanation
  */
  SeqAcqSpiral(const STD_string& object_label,double sweepwidth, float fov,
             unsigned int sizeRadial, unsigned int numofSegments, LDRtrajectory&  traj,
             bool inout=false, bool optimize=false, const STD_string& nucleus="",const dvector& phaselist=0);


/**
  * Constructs an acquisition window with spiral gradient which is a copy of 'sas'
  */
  SeqAcqSpiral(const SeqAcqSpiral& sas);



/**
  * Constructs an empty acquisition window with spiral gradient with the given label.
  */
  SeqAcqSpiral(const STD_string& object_label="unnamedSeqAcqSpiral");

/**
  * This assignment operator will make this object become an exact copy of 'sar'.
  */
  SeqAcqSpiral& operator = (const SeqAcqSpiral& sas);


/**
  * Returns the vector of the spiral interleaves (for loop insertion)
  */
  const SeqVector& get_segment_vector() const {return rotvec;}


/**
  * Returns the k-space trajectory for the interleave 'iseg'
  */
  fvector get_ktraj(unsigned int iseg, direction channel) const;

/**
  * Returns the Jacobian determinante, i.e. the correction function
  * for non-uniform weighting of k-space
  */
  fvector get_denscomp() const;


  // marshalling virtual functions of SeqAcqInterface to acq, except:
  double get_acquisition_center() const {return preacq.get_duration()+acq.get_acquisition_center();}
  double get_acquisition_start() const {return preacq.get_duration()+acq.get_acquisition_start();}
  SeqAcqInterface& set_sweepwidth(double sw, float os_factor); // produce warning


 private:

  // overwriting virtual functions from SeqClass
  bool prep();

  void common_init();

  void build_seq();

  SeqParallel par;

  SeqGradSpiral spirgrad_in;
  SeqGradSpiral spirgrad_out;

  SeqDelay preacq;
  SeqAcq acq;

  SeqGradTrapezParallel gbalance;

  SeqRotMatrixVector rotvec;

  bool inout_traj;
};

/** @}
  */

#endif
