/***************************************************************************
                          seqgradchanlist.h  -  description
                             -------------------
    begin                : Thu Apr 22 2004
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQGRADCHANLIST_H
#define SEQGRADCHANLIST_H


#include <odinseq/seqgradchan.h>

/**
  * @addtogroup odinseq_internals
  * @{
  */

/**
  * This class represents a list of gradient channel objects (i.e. those derived from SeqGradChan)
  * It is suitable to hold a gradient train for ONE gradient channel
  */
class SeqGradChanList : public List<SeqGradChan,SeqGradChan*, SeqGradChan&>, public Handled<SeqGradChanList*>, public virtual SeqGradInterface, public virtual SeqTreeObj {

 public:

/**
  * Construct an empty gradient channel list with the given label
  */
  SeqGradChanList(const STD_string& object_label = "unnamedSeqGradChanList" );

/**
  * Constructs a copy of 'sgcl'
  */
  SeqGradChanList(const SeqGradChanList& sgcl);

/**
  * Destructor
  */
  ~SeqGradChanList();


/**
  * Assignment operator that makes this gradient sequence object become a copy of 'sgcl'
  */
  SeqGradChanList& operator = (const SeqGradChanList& sgcl);


  // overloading virtual function from SeqTreeObj
  double get_duration() const;
  STD_string get_properties() const;
  unsigned int event(eventContext& context) const;
  void query(queryContext& context) const;

  // overloading virtual function from SeqGradInterface
  SeqGradInterface& set_strength(float gradstrength);
  SeqGradInterface& invert_strength();
  float get_strength() const;
  fvector get_gradintegral() const;
  double get_gradduration() const;
  SeqGradInterface& set_gradrotmatrix(const RotMatrix& matrix);


/**
  * Returns the occupied channel
  */
  direction get_channel() const;

/**
  * Appends the elements of 'sgcl' to this
  */
  SeqGradChanList& operator += (SeqGradChanList& sgcl);

/**
  * Appends  'sgc' to this
  */
  SeqGradChanList& operator += (SeqGradChan& sgc);


  fvector get_switchpoints() const;
  SeqGradChanList& get_chanlist4gp(const fvector& switchpoints);

 private:

  friend class SeqGradChanParallel;

  // overloading virtual function from SeqClass
  void clear_container();



  SeqGradChan* get_chan(double& chanstart, double midtime);

};


/** @}
  */

#endif
