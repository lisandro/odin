#include "seqacqdeph.h"
#include "seqacq.h"

void SeqAcqDeph::common_init() {
  dummyvec=SeqVector("dummyvec",1); // make one iteration the default, for dephasing of non-segmented EPI
}

SeqAcqDeph::SeqAcqDeph(const STD_string& object_label,const SeqAcqInterface& acq, dephaseMode mode)
 : SeqGradChanParallel(object_label) {
  Log<Seq> odinlog(this,"SeqAcqDeph(...)");
  common_init();

  SeqGradChanParallel::clear();
  const SeqVector* vec=acq.get_dephgrad(*this, mode==rephase);
  segvec.clear_handledobj();
  if(vec) segvec.set_handled(vec);
  if(mode==spinEcho) {
    SeqGradChanParallel::invert_strength();
  }
  ODINLOG(odinlog,normalDebug) << "integral(" << acq.get_label() << ")=" << SeqGradChanParallel::get_gradintegral().printbody() << STD_endl;
}

SeqAcqDeph::SeqAcqDeph(const SeqAcqDeph& sad) {
  common_init();
  SeqAcqDeph::operator = (sad);
}


SeqAcqDeph::SeqAcqDeph(const STD_string& object_label)
 : SeqGradChanParallel(object_label) {
  common_init();
}


SeqAcqDeph& SeqAcqDeph::operator = (const SeqAcqDeph& sad) {
  SeqGradInterface::operator = (sad);
  SeqGradChanParallel::operator = (sad);
  segvec=sad.segvec;
  return *this;
}


const SeqVector& SeqAcqDeph::get_epi_segment_vector() const {
  Log<Seq> odinlog(this,"get_epi_segment_vector");
  ODINLOG(odinlog,normalDebug) << "segvec.get_handled()=" << (void*)segvec.get_handled() << STD_endl;
  if(segvec.get_handled()) return *(segvec.get_handled());
  return dummyvec;
}


const SeqVector& SeqAcqDeph::get_epi_reduction_vector() const {
  if(segvec.get_handled()) return segvec.get_handled()->get_reorder_vector();
  return dummyvec;
}
