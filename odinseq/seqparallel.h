/***************************************************************************
                          seqparallel.h  -  description
                             -------------------
    begin                : Fri Apr 16 2004
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQPARALLEL_H
#define SEQPARALLEL_H

#include <tjutils/tjhandler.h>


#include <odinseq/seqgradobj.h>
#include <odinseq/seqobj.h>
#include <odinseq/seqdriver.h>


/**
  * @addtogroup odinseq_internals
  * @{
  */

/**
  * The base class for platform specific drivers of parallel RF/gradient handling
  */
class SeqParallelDriver : public SeqDriverBase {

 public:
  SeqParallelDriver() {}
  virtual ~SeqParallelDriver() {}

  virtual STD_string get_program(programContext& context, const SeqObjBase* soa, const SeqGradObjInterface* sgoa) const = 0;

  virtual double get_duration (const SeqObjBase* soa, const SeqGradObjInterface* sgoa) const  = 0;
  virtual double get_predelay (const SeqObjBase* soa, const SeqGradObjInterface* sgoa) const  = 0;

  virtual SeqParallelDriver* clone_driver() const = 0;
};

/////////////////////////////////////////////////////////////////////////////////////////


/**
  * This container class is used to play out a sequence object (RF pulse, acquisition)
  * and a gradient object in parallel
  */
class SeqParallel : public SeqObjBase, public virtual SeqGradInterface {

 public:

/**
  * Construct an empty gradient channel list with the given label
  */
  SeqParallel(const STD_string& object_label="unnamedSeqParallel");

/**
  * Constructs a copy of 'sgp'
  */
  SeqParallel(const SeqParallel& sgp);


/**
  * Assignment operator
  */
  SeqParallel& operator = (const SeqParallel& sgp);



/**
  * Makes 'sgc' become the gradient part
  */
  SeqParallel& operator /= (SeqGradChan& sgc);


/**
  * Makes 'sgcl' become the gradient part
  */
  SeqParallel& operator /= (SeqGradChanList& sgcl);

/**
  * Makes 'sgcp' become the gradient part
  */
  SeqParallel& operator /= (SeqGradChanParallel& sgcp);

/**
  * Makes 'sgoa' become the gradient part
  */
  SeqParallel& operator /= (SeqGradObjInterface& sgoa);



/**
  * Makes 'soa' become the RF part
  */
  SeqParallel& operator /= (const SeqObjBase& soa);





  // overloading virtual function from SeqTreeObj
  STD_string get_program(programContext& context) const {return pardriver->get_program(context,get_pulsptr(),get_const_gradptr());}
  double get_duration() const;
  STD_string get_properties() const;
  unsigned int event(eventContext& context) const;
  SeqValList get_freqvallist(freqlistAction action) const;
  SeqValList get_delayvallist() const;
  void query(queryContext& context) const;
  RecoValList get_recovallist(unsigned int reptimes, LDRkSpaceCoords& coords) const;
  double get_rf_energy() const;


  // overloading virtual function from SeqGradInterface
  SeqGradInterface& set_strength(float gradstrength);
  SeqGradInterface& invert_strength();
  float get_strength() const;
  fvector get_gradintegral() const;
  double get_gradduration() const;
  SeqGradInterface& set_gradrotmatrix(const RotMatrix& matrix);


/**
  * Returns the duration for the gradient commands in the pulse programm
  */
  double get_pulprogduration() const;



/**
  * Clears the RF anf gradient part
  */
  void clear();

 protected:
  SeqParallel& set_pulsptr(const SeqObjBase* pptr);
  const SeqObjBase* get_pulsptr() const;

  SeqParallel& set_gradptr(SeqGradObjInterface* gptr);
  SeqParallel& set_gradptr(const SeqGradObjInterface* gptr);
  SeqParallel& clear_gradptr();
  SeqGradObjInterface* get_gradptr() const;
  const SeqGradObjInterface* get_const_gradptr() const;


 private:
  friend class SeqOperator;
  friend class SeqObjList;
  friend class SeqObjVector;
  friend class SeqGradObjInterface;
  friend class SeqGradChanList;
  friend class SeqEpiDriverParavision;

  // overloading virtual function from SeqClass
  void clear_container() {clear();}


  mutable SeqDriverInterface<SeqParallelDriver> pardriver;

  Handler<const SeqObjBase* >          pulsptr;
  Handler<SeqGradObjInterface* >       gradptr;
  Handler<const SeqGradObjInterface* > const_gradptr;

};




/** @}
  */


#endif
