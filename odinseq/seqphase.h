/***************************************************************************
                          seqphase.h  -  description
                             -------------------
    begin                : Sun Apr 4 2004
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/



#ifndef SEQPHASE_H
#define SEQPHASE_H


#include <odinseq/seqclass.h>
#include <odinseq/seqvec.h>
#include <odinseq/seqdriver.h>

///////////////////////////////////////////////////////////////////

/**
  * @ingroup odinseq_internals
  * The base class for platform specific drivers of phase lists
  */
class SeqPhaseDriver : public SeqDriverBase {

 public:
  SeqPhaseDriver() {}
  virtual ~SeqPhaseDriver() {}

  virtual void prep_driver(const dvector& phaselist) = 0;

  virtual unsigned int get_phaselistindex(const dvector& phaselist) const = 0;

  virtual STD_string get_loopcommand(const dvector& phaselist) const = 0;

  virtual svector get_phasevec_commands(const STD_string& iterator, const STD_string& instr) const = 0;

  virtual SeqPhaseDriver* clone_driver() const = 0;
};


///////////////////////////////////////////////////////////////////

class SeqFreqChan; // forward declaration

/**
  * @ingroup odinseq_internals
  * A vector class for managing phase lists
  */
class SeqPhaseListVector : public SeqVector, public virtual SeqClass {

 private: //make everything private so that only SeqFreqChan can use it

  SeqPhaseListVector(const STD_string& object_label = "unnamedSeqPhaseListVector", const dvector& phase_list=0 );

  SeqPhaseListVector(const SeqPhaseListVector& spl);

  SeqPhaseListVector& operator = (const SeqPhaseListVector& spl);

/**
  * Specifies the phaselist
  */
  SeqPhaseListVector& set_phaselist(const dvector& pl);

/**
  * Returns the phaselist
  */
  dvector get_phaselist() const {return phaselist;}

/**
  * Return the index for the phaselist
  */
  unsigned int get_phaselistindex() const;

/**
  * Returns the current phase in deg
  */
  double get_phase() const;


  // overloaded functions of SeqVector
  unsigned int get_vectorsize() const {return phaselist.size();}
  STD_string get_loopcommand() const;
  svector get_vector_commands(const STD_string& iterator) const;
  bool prep_iteration() const;
  bool is_qualvector() const {return false;}

 private:
  friend class SeqFreqChan;



  // overwriting virtual functions from SeqClass
  bool prep();

  // the hardware driver
  mutable SeqDriverInterface<SeqPhaseDriver> phasedriver;

  dvector phaselist; // in deg

  // the frequency object in which this object is embedded
  SeqFreqChan* user;

};



#endif
