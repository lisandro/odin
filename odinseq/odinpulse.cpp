#include "odinpulse.h"
#include "seqsim.h"
#include "seqdriver.h"
#include "seqplatform.h"

///////////////////////////////////////////////////////////////////////


const kspace_coord& LDRtrajectory::calculate(float s) const {
  LDRfunctionPlugIn::coord_retval=kspace_coord();
  if(allocated_function) return allocated_function->calculate_traj(s);
  else return LDRfunctionPlugIn::coord_retval;
}




const traj_info& LDRtrajectory::get_traj_info() const {
  LDRfunctionPlugIn::traj_info_retval=traj_info();
  if(allocated_function) return allocated_function->get_traj_properties();
  else return LDRfunctionPlugIn::traj_info_retval;
}



///////////////////////////////////////////////////////////////////////


STD_complex LDRshape::calculate(const kspace_coord& coord ) const {
  if(allocated_function) return allocated_function->calculate_shape(coord);
  return STD_complex(0.0);
}

STD_complex LDRshape::calculate(float s, float Tp) const {
  if(allocated_function) return allocated_function->calculate_shape(s,Tp);
  return STD_complex(0.0);
}

const shape_info& LDRshape::get_shape_info() const {
  LDRfunctionPlugIn::shape_info_retval=shape_info();
  if(allocated_function) return allocated_function->get_shape_properties();
  else return LDRfunctionPlugIn::shape_info_retval;
}


///////////////////////////////////////////////////////////////////////


struct OdinPulseData {

  bool intactive;

  LDRenum dim_mode;
  LDRenum nucleus;

  LDRshape      shape;
  LDRtrajectory trajectory;
  LDRfilter     filter;

  LDRint npts;
  LDRdouble Tp;

  LDRcomplexArr B1;
  LDRfloatArr Gr;
  LDRfloatArr Gp;
  LDRfloatArr Gs;

  LDRdouble B10;
  LDRdouble G0;

  LDRbool consider_system_cond_flag;
  LDRbool consider_Nyquist_cond_flag;
  LDRbool take_min_smoothing_kernel;
  LDRdouble smoothing_kernel_size;
  LDRtriple spatial_offset;
  LDRdouble field_of_excitation;

  LDRenum pulse_type;

  LDRformula composite_pulse;
  LDRint    npts_1pulse;
  LDRdouble Tp_1pulse;

  LDRdouble pulse_gain;
  LDRdouble pulse_power;

  LDRdouble flipangle;
  float flip_corr;

  funcMode old_mode;
  bool ready;
};


///////////////////////////////////////////////////////////////////////

// Calculate maximum possible gradient strengt which is either given by the slew rate or max gradient strength
float OdinPulse::gradient_system_max (const fvector& Gvec, float Gmax, float maxslew, float Tp) {
  Log<Seq> odinlog("","gradient_system_max");
  float dGmax = 0.0;
  int npuls=Gvec.size();
  for (int i = 1; i < npuls; i++) dGmax=STD_max(dGmax, float(fabs(Gvec[i - 1] - Gvec[i])));
  if(dGmax>0.0 && npuls>0 ) {
    float Gmax_slew = maxslew * Tp / (float(npuls) * dGmax); // Gradient strength which would have to be used in order to stay within slew-rate limits
    return STD_min(Gmax_slew, Gmax);
  } else {
    return Gmax;
  }
}


float OdinPulse::max_kspace_step (const fvector& Gz,float gamma,float Tp,float G0) {
  float kz = 0.0, kzold = 0.0, maxstep = 0.0;
  int npuls=Gz.size();
  for (int i = npuls - 1; i >= 0; i--) {
    kz -= gamma * Tp * G0 / float(npuls) * Gz[i];
    maxstep = STD_max(maxstep, float(fabs(kz - kzold)));
    kzold = kz;
  }
  return maxstep;
}


float OdinPulse::max_kspace_step2 (const fvector& Gx, const fvector& Gy, float gamma,float Tp,float G0) {
  float kx = 0.0, ky = 0.0, kxold = 0.0, kyold = 0.0, maxstep = 0.0;
  int npuls=Gx.size();
  for (int i = npuls - 1; i >= 0; i--) {
    kx -= gamma * Tp * G0 / float(npuls) * Gx[i];
    ky -= gamma * Tp * G0 / float(npuls) * Gy[i];
    maxstep = STD_max(maxstep, float(norm(kx - kxold, ky - kyold)));
    kxold = kx;
    kyold = ky;
  }
  return maxstep;
}



//////////////////////////////////////////////////////////////////////////////////////////


OdinPulse::OdinPulse(const STD_string& pulse_label,bool interactive) : LDRblock(pulse_label), data(new OdinPulseData) {
  set_label(pulse_label); // because SeqClass is a virtual base class
  Log<Seq> odinlog(this,"OdinPulse(...)");

  
  data->shape.set_label("shape");
  data->trajectory.set_label("trajectory");
  data->filter.set_label("filter");

  data->ready=false;
  data->intactive=interactive;

  data->flip_corr=1.0;

  data->dim_mode.add_item("0D");
  data->dim_mode.add_item("1D");
  data->dim_mode.add_item("2D");
  data->dim_mode.set_actual(oneDeeMode);
  data->old_mode=funcMode(int(data->dim_mode));
  ODINLOG(odinlog,normalDebug) << "old_mode=" << int(data->old_mode) << STD_endl;


  data->nucleus=systemInfo->get_nuc_enum(); data->nucleus.set_actual(0);

  data->shape.set_function_mode(funcMode(int(data->dim_mode)));
  data->trajectory.set_function_mode(funcMode(int(data->dim_mode)));

  data->npts_1pulse=256;
  data->npts_1pulse.set_minmaxval(1,systemInfo->get_max_rf_samples());
  data->npts=data->npts_1pulse;
  resize_noupdate(data->npts);

  data->Tp_1pulse=2.0;
  data->Tp_1pulse.set_minmaxval(0.001,30.0);
  data->Tp_1pulse.set_unit(ODIN_TIME_UNIT);
  data->Tp=data->Tp_1pulse;

  data->take_min_smoothing_kernel=true;
  data->smoothing_kernel_size=0.001;// smoothing_kernel_size.set_minmaxval(0.001,20.0);
  data->smoothing_kernel_size.set_unit(ODIN_SPAT_UNIT);

  data->field_of_excitation=200.0;
  data->field_of_excitation.set_unit(ODIN_SPAT_UNIT);

  data->flipangle=90.0; data->flipangle.set_minmaxval(0.0,360.0);
  data->flipangle.set_unit(ODIN_ANGLE_UNIT);

  data->consider_system_cond_flag=true;
  data->consider_Nyquist_cond_flag=true;

  data->spatial_offset.set_unit(ODIN_SPAT_UNIT);

  for(int i=0; i<numof_pulseTypes; i++) data->pulse_type.add_item(pulseTypeLabel[i],i);
  data->pulse_type.set_actual(0);


  data->composite_pulse.set_syntax("A composite pulse can be specified by a string of the form a1(x2) a2(x2) ...  where a1,a2,... are the flipangles in degree and x1,x2,... are the axes, .e.g. X,-X,Y or -Y");

  data->pulse_gain=0.0;
  data->pulse_gain.set_parmode(noedit);
  data->pulse_gain.set_unit("dB");

  data->pulse_power=0.0;
  data->pulse_power.set_parmode(noedit);
  data->pulse_power.set_unit(STD_string(ODIN_FIELD_UNIT)+"^2*"+ODIN_TIME_UNIT);

  data->B10=0.0;
  data->B10.set_parmode(noedit);
  data->B10.set_unit(ODIN_FIELD_UNIT);

  data->G0=0.0;
  data->G0.set_parmode(noedit);
  data->G0.set_unit(ODIN_GRAD_UNIT);


  data->B1.set_filemode(exclude);

  GuiProps gp;
  gp.scale[xPlotScale]=ArrayScale("time",ODIN_TIME_UNIT,0.0,data->Tp);
  data->B1.set_gui_props(gp);

  data->Gr.set_filemode(exclude);
  data->Gp.set_filemode(exclude);
  data->Gs.set_filemode(exclude);


  data->shape.set_function(0);
  data->trajectory.set_function(0);
  data->filter.set_function(0);

  unsigned int capacity=systemInfo->get_max_rf_samples();
  data->B1.reserve(capacity);
  data->Gr.reserve(capacity);
  data->Gp.reserve(capacity);
  data->Gs.reserve(capacity);


  append_all_members();

  data->ready=true;
  OdinPulse::update();
}

OdinPulse::OdinPulse(const OdinPulse& pulse) : data(new OdinPulseData) {
  OdinPulse::operator = (pulse);
}


OdinPulse::~OdinPulse() {
  Log<Seq> odinlog(this,"~OdinPulse()");
  delete data;
}


int OdinPulse::append_all_members() {
  clear();
  append_member(data->dim_mode,"Mode");
  append_member(data->nucleus,"Nucleus");
  append_member(data->shape,"Shape");
  append_member(data->trajectory,"Trajectory");
  append_member(data->filter,"Filter");
  append_member(data->npts_1pulse,"NumberOfPoints");
  append_member(data->Tp_1pulse,"PulseDuration");
  if(int(data->dim_mode)>zeroDeeMode) append_member(data->take_min_smoothing_kernel,"TakeMinSmoothingKernel");
  if(int(data->dim_mode)>zeroDeeMode) append_member(data->smoothing_kernel_size,"SmoothingKernelSize");
  append_member(data->flipangle,"FlipAngle");
  if(int(data->dim_mode)>zeroDeeMode) append_member(data->consider_system_cond_flag,"ConsiderSystem");
  if(int(data->dim_mode)>zeroDeeMode) append_member(data->consider_Nyquist_cond_flag,"ConsiderNyquist");
  if(int(data->dim_mode)>zeroDeeMode) append_member(data->spatial_offset,"SpatialOffset");
  if(int(data->dim_mode)>zeroDeeMode) append_member(data->field_of_excitation,"FieldOfExcitation");
  if(int(data->dim_mode)<twoDeeMode)  append_member(data->pulse_type,"PulseType");
  append_member(data->composite_pulse,"CompositePulse");


  append_member(data->B1,"B1");
  if(int(data->dim_mode)==twoDeeMode) append_member(data->Gr,"x_Gradient");
  if(int(data->dim_mode)==twoDeeMode) append_member(data->Gp,"y_Gradient");
  if(int(data->dim_mode)==oneDeeMode) append_member(data->Gs,"z_Gradient");


  append_member(data->pulse_gain,"PulseGain");
  append_member(data->pulse_power,"PulsePower");
  append_member(data->B10,"B1_Max");
  append_member(data->G0,"GradientMax");
  return 0;
}


OdinPulse& OdinPulse::operator = (const OdinPulse& pulse) {
  Log<Seq> odinlog(this,"operator = (...)");
  SeqClass::operator = (pulse);
  LDRblock::operator = (pulse);
  ODINLOG(odinlog,normalDebug) << "operator of base classes called" << STD_endl;
  (*data)=(*(pulse.data));
  OdinPulse::append_all_members();
  update();
  return *this;
}

OdinPulse& OdinPulse::set_dim_mode(funcMode dmode) {
  Log<Seq> odinlog(this,"set_dim_mode");
  data->old_mode=funcMode(int(data->dim_mode));
  ODINLOG(odinlog,normalDebug) << "old_mode/dmode=" << int(data->old_mode) << "/" << int(dmode) << STD_endl;
  data->dim_mode.set_actual(dmode);
  data->shape.set_function_mode(funcMode(int(data->dim_mode)));
  data->trajectory.set_function_mode(funcMode(int(data->dim_mode)));
  update();
  return *this;
}


funcMode OdinPulse::get_dim_mode() const {
  return funcMode(int(data->dim_mode));
}

unsigned int OdinPulse::get_size() const {return data->npts;}

OdinPulse& OdinPulse::resize(unsigned int newsize) {
  data->npts_1pulse=newsize;
  resize_noupdate(newsize);
  return update();
}

OdinPulse& OdinPulse::resize_noupdate(unsigned int newsize) {
  data->B1.resize(newsize);
  data->Gr.resize(newsize);
  data->Gp.resize(newsize);
  data->Gs.resize(newsize);
  return *this;
}


OdinPulse& OdinPulse::set_Tp(double duration) {
  data->Tp_1pulse=duration;
  data->Tp=duration;
  return update();
}


double OdinPulse::get_Tp() const {
  return data->Tp;
}

double OdinPulse::get_Tp_1pulse() const {
  return data->Tp_1pulse;
}


OdinPulse& OdinPulse::set_consider_system_cond(bool flag) {
  data->consider_system_cond_flag=flag;
  update();
  return *this;
}

bool OdinPulse::get_consider_system_cond() const {return data->consider_system_cond_flag;}

OdinPulse& OdinPulse::set_consider_Nyquist_cond(bool flag) {
  data->consider_Nyquist_cond_flag=flag;
  update();
  return *this;
}

bool OdinPulse::get_consider_Nyquist_cond() const { return data->consider_Nyquist_cond_flag;}


OdinPulse& OdinPulse::set_spat_resolution(double sigma) {
  data->smoothing_kernel_size=sigma;
  data->take_min_smoothing_kernel=false;
  update();
  return *this;
}

double OdinPulse::get_spat_resolution() const {return data->smoothing_kernel_size;}

OdinPulse& OdinPulse::use_min_spat_resolution(bool flag) {
  data->take_min_smoothing_kernel=flag;
  update();
  return *this;
}

OdinPulse& OdinPulse::set_field_of_excitation(double fox) {
  data->field_of_excitation=fox;
  update();
  return *this;
}


double OdinPulse::get_field_of_excitation() const {return data->field_of_excitation;}


OdinPulse& OdinPulse::set_spatial_offset(direction direction, double offset) {
  data->spatial_offset[direction]=offset;
  update();
  return *this;
}

double OdinPulse::get_spatial_offset(direction direction) const {return data->spatial_offset[direction];}

OdinPulse& OdinPulse::set_nucleus(const STD_string& nucleusname) {
  data->nucleus.set_actual(nucleusname);
  update();
  return *this;
}

STD_string OdinPulse::get_nucleus() const {
  return data->nucleus;
}


OdinPulse& OdinPulse::set_pulse_type(pulseType type) {
  data->pulse_type.set_actual(type);
  update();
  return *this;
}


pulseType OdinPulse::get_pulse_type() const {return pulseType(int(data->pulse_type));}


OdinPulse& OdinPulse::set_composite_pulse(const STD_string& cpstring) {
  Log<Seq> odinlog(this,"set_composite_pulse");
  data->composite_pulse=cpstring;
  ODINLOG(odinlog,normalDebug) << "composite_pulse=>" << STD_string(data->composite_pulse) << "<" << STD_endl;
  update();
  return *this;
}


farray OdinPulse::get_composite_pulse_parameters() const {
  Log<Seq> odinlog(this,"get_composite_pulse_parameters");
  if(!is_composite_pulse()) return farray();
  ODINLOG(odinlog,normalDebug) << "composite_pulse=>" << STD_string(data->composite_pulse) << "<" << STD_endl;

  svector subpulses=tokens(data->composite_pulse);
  unsigned int n_pulses=subpulses.size();
  ODINLOG(odinlog,normalDebug) << "n_pulses=" << n_pulses << STD_endl;
  farray result(n_pulses,2);

  for(unsigned int i=0;i<n_pulses;i++) {

    float phase=0.0;
    STD_string dirstring=extract(subpulses[i],"(",")");
    dirstring=toupperstr(dirstring);
    ODINLOG(odinlog,normalDebug) << "dirstring=" << dirstring << STD_endl;

    if(dirstring=="X") phase=0.0;
    if(dirstring=="-X") phase=180.0;
    if(dirstring=="Y") phase=90.0;
    if(dirstring=="-Y") phase=270.0;
    result(i,1)=phase;

    ODINLOG(odinlog,normalDebug) << "extracting flip angle from >" << subpulses[i] << "<" << STD_endl;
    result(i,0)=atof(rmblock(subpulses[i],"(",")",true,true).c_str());
  }

  return result;
}


bool OdinPulse::is_composite_pulse() const {
  Log<Seq> odinlog(this,"is_composite_pulse");
  bool result=true;
  if(data->composite_pulse=="") result=false;
  ODINLOG(odinlog,normalDebug) << "composite_pulse/result=" << STD_string(data->composite_pulse) << "/" << result << STD_endl;
  return result;
}




OdinPulse& OdinPulse::set_flipangle(double angle) {
  data->flipangle=angle;
  update_B10andPower();
  return *this;
}


double OdinPulse::get_flipangle() const {return data->flipangle;}



int OdinPulse::load(const STD_string& filename, const LDRserBase& serializer) {
  Log<Seq> odinlog(this,"load");


  // determine the dim mode first and set the functions accordingly
  data->dim_mode.load(filename,serializer);
  ODINLOG(odinlog,normalDebug) << "dim_mode/old_mode=" << int(data->dim_mode) << "/" << int(data->old_mode) << STD_endl;

  // switch before loading so that plugin parameters can be assigned properly
  data->shape.set_function_mode(funcMode(int(data->dim_mode)));
  data->trajectory.set_function_mode(funcMode(int(data->dim_mode)));

  // then load the whole set of parameters and update the pulse
  int result=LDRblock::load(filename,serializer);

  update();

  return result;
}



float OdinPulse::ensure_unit_range(float x) {
  x=STD_min(float(1.0),x);
  x=STD_max(float(-1.0),x);
  return x;
}


OdinPulse& OdinPulse::generate() {
  Log<Seq> odinlog(this,"generate");
  if(!data->ready) {
    ODINLOG(odinlog,normalDebug) << "not ready" << STD_endl;
    return *this;
  }

  int i;
  float knew;
  float dkmax=0.0;

  if(data->take_min_smoothing_kernel) data->smoothing_kernel_size=0.001; // Reset so that it will be increased to min in a later step
  data->smoothing_kernel_size=STD_max(0.001, double(data->smoothing_kernel_size));

  data->shape.init_shape();
  data->trajectory.init_trajectory(this);

  // For loaded pulses
  int fixedsize=data->shape.get_shape_info().fixed_size;
  if(fixedsize>=0) data->npts_1pulse=fixedsize;

  // Calculate pulse using fixed rastertime, if necessary
  double rastime=systemInfo->get_rastertime(pulsObj);
  if(rastime) {
    data->npts_1pulse=int(secureDivision(data->Tp_1pulse,rastime)+0.5);
    data->Tp_1pulse=double(data->npts_1pulse)*rastime;
  }

  // reset to the size of one pulse
  data->npts=data->npts_1pulse;
  data->Tp=data->Tp_1pulse;

  resize_noupdate(data->npts_1pulse);
  ODINLOG(odinlog,normalDebug) << "resize done" << STD_endl;


  float gamma=systemInfo->get_gamma(data->nucleus);

  funcMode mode=funcMode(int(data->dim_mode));

  // Zero gradients before calculating the selectively
  data->Gr=LDRfloat(0.0);
  data->Gp=LDRfloat(0.0);
  data->Gs=LDRfloat(0.0);
  data->G0=0.0;

  float g0=0.0; // The scaling factor for the time-strength-independent gradient shape

  bool spatsel_mode=(mode>=oneDeeMode) && (mode<=twoDeeMode);

  if(spatsel_mode) { // generate gradients if in spatial-selective mode

    if(mode==oneDeeMode) {

      // calculate gradient:
      for(i=0; i<data->npts_1pulse; i++) {
        const kspace_coord& tds=data->trajectory.calculate (float(i)/float(data->npts_1pulse-1));
        data->Gs[i] = tds.Gz;
      }

      // normalize_gradient:
      g0 = data->Gs.normalize();

    } //  end oneDeeMode

    if(mode==twoDeeMode) {

      // calculate gradient:
      for(i=0; i<data->npts_1pulse; i++) {
        const kspace_coord& tds=data->trajectory.calculate (float(i)/float(data->npts_1pulse-1));
        data->Gr[i] = tds.Gx;
        data->Gp[i] = tds.Gy;
      }

      // normalize_gradient:
      g0 = STD_max (data->Gr.maxabs(), data->Gp.maxabs());
      if (g0) {
        data->Gr/=g0;
        data->Gp/=g0;
      }


    }
  }


  // The following calculations are based on these notations:
  //
  // k(t) = kr * f(t/Tp) = gamma * integral{ G(t') dt'} = gamma * G0 * integral{ G(t')/G0 dt'} = gamma * G0 * Tp * integral{ g(s)/g0 ds} = gamma * G0/g0 * Tp * f(s)
  //
  // => kr = gamma * Tp * G0/g0 
  //
  // with
  // k(t): Physical k-space trajectory
  // kr:   Maximum k-space radius (distance from center)
  // f(s): Trajectory as provided by plugin
  // G(t): Physical gradient shape
  // G0:   Scaling factor for physical gradient strength
  // g(s): Gradient shape as provided by plugin, f(s) = integral{ g(s) ds}
  // g0:   Scaling factor for gradient shape as provided by plugin
  //
  // Please note: the vectors G(t)/G0 and g(s)/g0 have the same normalized values.


  double nyquist_factor = sqrt (-2.0 * log (_BOUNDARY_VALUE_)) * 2.0; // TODO: replace this by PII
//  double nyquist_factor = PII;

  // set_gradient_max_scale:
  if(spatsel_mode) {
    // set G0 according to smoothing scale:
    float kr = secureDivision(nyquist_factor , data->smoothing_kernel_size);
    data->G0 = secureDivision(kr * g0 , gamma * data->Tp_1pulse);
  }
  ODINLOG(odinlog,normalDebug) << "set_gradient_max_scale done" << STD_endl;


  // calculate the maximum gradient strength of the pulse according to the system constrains
  float Gmax_system=systemInfo->get_max_grad();
  float maxslew_system=systemInfo->get_max_slew_rate();
  ODINLOG(odinlog,normalDebug) << "systemInfo stuff done" << STD_endl;
  float Gsystem=0.0;
  if(mode==oneDeeMode) {
    ODINLOG(odinlog,normalDebug) << "calling gradient_system_max in 1D mode" << STD_endl;
    Gsystem = gradient_system_max (data->Gs, Gmax_system, maxslew_system, data->Tp_1pulse);
  }
  if(mode==twoDeeMode) {
    ODINLOG(odinlog,normalDebug) << "calling gradient_system_max in 2D mode" << STD_endl;
    Gsystem = STD_min(gradient_system_max (data->Gr, Gmax_system, maxslew_system, data->Tp_1pulse), gradient_system_max (data->Gp, Gmax_system, maxslew_system, data->Tp_1pulse));
  }
  ODINLOG(odinlog,normalDebug) << "gradient scaling done" << STD_endl;


  // check to above calculated max gradient strength against the actual and correct if neccessary
  if (data->G0 >= Gsystem) {
    if (data->consider_system_cond_flag) {
      data->G0 = Gsystem;
      if(spatsel_mode) {
        knew = secureDivision(gamma * data->G0 * data->Tp_1pulse , g0);
        data->smoothing_kernel_size = STD_max( double(data->smoothing_kernel_size), nyquist_factor / knew);
      }
    } else ODINLOG(odinlog,warningLog) << "system conditions violated !" << STD_endl;
  }
  ODINLOG(odinlog,normalDebug) << "system constraints done" << STD_endl;


  // precalculations for Nyquist check
  float max_spatial_extension=0.5*data->field_of_excitation;  // Assume single point as the excitation shape, so that, in contrast to a shape that fills the whole FOX, aliases would appear at (+/- FOX).  Therefore, we can effectively use half the FOX without aliasing.
  max_spatial_extension*=sqrt(float(int(mode))); // extend to diagonal direction square-shaped alias-free FOX
  max_spatial_extension+=data->shape.get_shape_info().spatial_extent; // If shape is not single point, add the total extent of the shape. Otherwise it aliases into the FOX.
  if(mode==oneDeeMode) {
    dkmax=max_kspace_step(data->Gs, gamma, data->Tp_1pulse, data->G0);
    max_spatial_extension += fabs(data->spatial_offset[zAxis]);
  }
  if(mode==twoDeeMode) {
    dkmax=max_kspace_step2(data->Gr, data->Gp, gamma, data->Tp_1pulse,data->G0);  // max k-space step in tangential direction
    max_spatial_extension += norm (data->spatial_offset[xAxis], data->spatial_offset[yAxis]);  // add spatial offset
  }

  float kr0 = secureDivision(gamma * data->G0 * data->Tp_1pulse , g0); // k-space radius
  float dkmax_trajectory=2.0*kr0*data->trajectory.get_traj_info().max_kspace_step; // max k-space step in radial direction
  ODINLOG(odinlog,normalDebug) << "dkmax_trajectory/dkmax=" << dkmax_trajectory << "/" <<  dkmax << STD_endl;
  dkmax=STD_max(dkmax, dkmax_trajectory); // max of both

  // consider Nyquist condition and correct if neccessary
  if(spatsel_mode) {
    float phase_product=dkmax * max_spatial_extension;
    if (phase_product > 2.0*PII) { // Nyquist condition
      if (data->consider_Nyquist_cond_flag) {  // rescale k-space to fulfill Nyquist condition
        data->G0 = secureDivision( 2.0*PII * data->G0 , phase_product );
        knew = secureDivision( gamma * data->G0 * data->Tp_1pulse , g0 );
        data->smoothing_kernel_size = STD_max( double(data->smoothing_kernel_size), nyquist_factor / knew);
      } else ODINLOG(odinlog,warningLog) << "Nyquist condition violated !" << STD_endl;
    }
  }
  ODINLOG(odinlog,normalDebug) << "Nyquist constraints done" << STD_endl;



//  float kernel0D=sqrt(-32*log(_BOUNDARY_VALUE_));

  //generate RF waveform:
  kr0 = secureDivision(gamma * data->G0 * data->Tp_1pulse , g0);
  ODINLOG(odinlog,normalDebug) << "dim_mode/g0/G0/kr0 = " << data->dim_mode.printvalstring() << "/" << g0 << "/" << double(data->G0) << "/" << kr0 << STD_endl;
  ODINLOG(odinlog,normalDebug) << "adiabatic = " << is_adiabatic() << STD_endl;

  kspace_coord tds;
  for (i=0; i<data->npts_1pulse; i++) {
    float s=float(i)/float(data->npts_1pulse-1);
    tds=data->trajectory.calculate(s);
    tds.index=i;
    tds.kx=tds.kx*kr0;
    tds.ky=tds.ky*kr0;
    tds.kz=tds.kz*kr0;

    STD_complex Wk;

    if (mode==zeroDeeMode) Wk=data->shape.calculate(tds.traj_s,data->Tp_1pulse);
    else Wk=data->shape.calculate(tds);


    if ( !is_adiabatic() ) {

      if(mode==zeroDeeMode) {
        Wk*=data->filter.calculate(fabs(tds.traj_s-0.5)*2.0);
      } else {
        float kr = secureDivision( nyquist_factor , data->smoothing_kernel_size);
        float krad = sqrt(tds.kx*tds.kx+tds.ky*tds.ky+tds.kz*tds.kz);
        Wk*=data->filter.calculate(secureDivision( krad , kr ) );
      }
    }

    if(mode==oneDeeMode) {
      Wk *= tds.denscomp;
      if (data->spatial_offset[zAxis] != 0.0) {
        float argument = float(data->spatial_offset[zAxis]) * tds.kz;
        Wk*=exp(STD_complex(0.0,-argument));
      }
    }
    if(mode==twoDeeMode) {
      Wk *= tds.denscomp;
      if ((data->spatial_offset[xAxis] != 0.0) || (data->spatial_offset[yAxis] != 0.0)) {
        float argument = float(data->spatial_offset[xAxis]) * tds.kx + float(data->spatial_offset[yAxis]) * tds.ky;
        Wk*=exp(STD_complex(0.0,-argument));
      }
    }
    data->B1[i]=Wk;
  }
  ODINLOG(odinlog,normalDebug) << "generate RF done" << STD_endl;

  // final scaling
  float Bmax = amplitude(data->B1).maxvalue();
  ODINLOG(odinlog,normalDebug) << "Bmax=" << Bmax << STD_endl;
  if (Bmax <= 0.0) {
    ODINLOG(odinlog,warningLog) << "RF-amplitude array=0.0 !" << STD_endl;
  }
  for (i = 0; i<data->npts_1pulse; i++) data->B1[i]*= secureInv(Bmax);
  ODINLOG(odinlog,normalDebug) << "final scaling done" << STD_endl;

  ODINLOG(odinlog,normalDebug) << "all done" << STD_endl;
  return *this;
}

unsigned int OdinPulse::get_numof_composite_pulse() const {
  return get_composite_pulse_parameters().size(0);
}

bool OdinPulse::is_adiabatic() const {return data->shape.get_shape_info().adiabatic;}

float OdinPulse::get_rel_center() const {return data->trajectory.get_traj_info().rel_center;}

double OdinPulse::get_pulse_gain() const {return data->pulse_gain;}

float OdinPulse::get_flipangle_corr_factor() const {return data->flip_corr;}



OdinPulse& OdinPulse::make_composite_pulse() {
  Log<Seq> odinlog(this,"make_composite_pulse");

  // reset to the size of one pulse
  data->npts=data->npts_1pulse;
  data->Tp=data->Tp_1pulse;

  if(!is_composite_pulse()) {
    ODINLOG(odinlog,normalDebug) << "no composite pulse" << STD_endl;
    return *this;
  }
  ODINLOG(odinlog,normalDebug) << "is composite pulse" << STD_endl;

  OdinPulseData datacopy(*data);
  farray pars=get_composite_pulse_parameters();
  unsigned int npulses=pars.size(0);
  ODINLOG(odinlog,normalDebug) << "sdcopy & pars done" << STD_endl;

  unsigned int oldsize=datacopy.npts_1pulse;
  unsigned int newsize=npulses*oldsize;
  ODINLOG(odinlog,normalDebug) << "oldsize/newsize=" << oldsize << "/" << newsize << STD_endl;

  resize_noupdate(newsize);
  data->npts=newsize;
  data->Tp=double(data->Tp_1pulse)*double(npulses);
  ODINLOG(odinlog,normalDebug) << "Tp_1pulse/npulses/Tp=" << data->Tp_1pulse << "/" << npulses << "/" << data->Tp << STD_endl;


  unsigned int ipulse;

  float maxflip=0.0;
  for(ipulse=0; ipulse<npulses; ipulse++) {
    float flipang=pars(ipulse,0);
    if(flipang>maxflip) maxflip=flipang;
  }

  unsigned int index=0;
  for(ipulse=0; ipulse<npulses; ipulse++) {
    float flipscale=secureDivision(pars(ipulse,0),maxflip);
    STD_complex phasefactor=exp(STD_complex(0.0,pars(ipulse,1)/180.0*PII));
    for(unsigned int i=0;i<oldsize; i++) {
      data->B1[index]=flipscale*phasefactor*datacopy.B1[i];
      data->Gr[index]=datacopy.Gr[i];
      data->Gp[index]=datacopy.Gp[i];
      data->Gs[index]=datacopy.Gs[i];
      index++;
    }
  }
  ODINLOG(odinlog,normalDebug) << "assignment done" << STD_endl;

  data->flipangle=maxflip;
  update_B10andPower();
  ODINLOG(odinlog,normalDebug) << "B10/power done" << STD_endl;

  float gamma=systemInfo->get_gamma(data->nucleus);
  float flip_formfactor= 180.0/PII * gamma * data->B10 * abs(data->B1.sum()) * secureDivision(data->Tp,double(data->npts));
  data->flip_corr=secureDivision(flip_formfactor,data->flipangle);
  ODINLOG(odinlog,normalDebug) << "flip_formfactor/flip_corr=" << flip_formfactor << "/" << data->flip_corr << STD_endl;

  return *this;
}


OdinPulse& OdinPulse::set_pulse_gain() {
  Log<Seq> odinlog(this,"set_pulse_gain");
  if(!data->ready) return *this;

  unsigned int i;

  SeqSimMagsi mag; //("set_pulse_gain::mag");
//  mag.resize(1,1,1,1);

  float gamma=systemInfo->get_gamma(data->nucleus);
  ODINLOG(odinlog,normalDebug) << "gamma=" << gamma << STD_endl;

  // Field-Strength of 90�,Tp BP-Pulse as starting value
  data->B10 = secureDivision( 0.5 * PII , (gamma * data->Tp_1pulse) );
  ODINLOG(odinlog,normalDebug) << "B10=" << data->B10 << STD_endl;


  Sample sample;

  sample.set_spatial_offset(xAxis,0.0);
  sample.set_spatial_offset(yAxis,0.0);
  sample.set_spatial_offset(zAxis,0.0);

  if(int(data->dim_mode) == oneDeeMode) {
    sample.set_spatial_offset(zAxis,data->spatial_offset[zAxis]+ data->shape.get_shape_info().ref_z_pos);
  }

  if(int(data->dim_mode) == twoDeeMode) {
    float x_offset=data->spatial_offset[xAxis] + data->shape.get_shape_info().ref_x_pos;
    float y_offset=data->spatial_offset[yAxis] + data->shape.get_shape_info().ref_y_pos;

    ODINLOG(odinlog,normalDebug) << "x_offset=" << x_offset << STD_endl;
    ODINLOG(odinlog,normalDebug) << "y_offset=" << y_offset << STD_endl;

    sample.set_spatial_offset(xAxis,x_offset);
    sample.set_spatial_offset(yAxis,y_offset);
  }

  // deal with adiabatic mode first
  if(is_adiabatic()) {
    i=0;
    float Mzdes=-1.0+_SETREFGAIN_ADIABATIC_INTERVAL_; // inversion is default
    if(get_pulse_type()==saturation)  Mzdes= 0.0+_SETREFGAIN_ADIABATIC_INTERVAL_;
    while( /* i<SETREFGAIN_ITERATIONS &&*/ (mag.get_Mz())[0] > Mzdes) {
      simulate_pulse(mag,sample);
      data->B10 = _SETREFGAIN_ADIABATIC_INCR_FACTOR_*data->B10;
      i++;
    }
  }

  // adjust 90deg pulse
  if(!is_adiabatic()) {
    for (i = 0; i < _SETREFGAIN_ITERATIONS_; i++) {
      simulate_pulse(mag,sample);
      ODINLOG(odinlog,normalDebug) << "B10(pre) [" << i << "]=" << data->B10 << STD_endl;
      ODINLOG(odinlog,normalDebug) << "Mz=" << (mag.get_Mz())[0] << STD_endl;
      data->B10 = secureDivision(0.5 * PII * data->B10 , acos ((mag.get_Mz())[0]));
      ODINLOG(odinlog,normalDebug) << "B10(post)[" << i << "]=" << data->B10 << STD_endl;
    }
  }

  float formfactor=secureDivision(abs(data->B1.sum()),double(data->npts_1pulse));
  ODINLOG(odinlog,normalDebug) << "formfactor=" << formfactor << STD_endl;
  float B10_formfactor=secureDivision( 0.5 * PII , (formfactor * gamma  * data->Tp_1pulse));
  ODINLOG(odinlog,normalDebug) << "B10_formfactor=" << B10_formfactor << STD_endl;
  data->flip_corr=secureDivision(data->B10,B10_formfactor);
  ODINLOG(odinlog,normalDebug) << "flip_corr=" << data->flip_corr << STD_endl;


  // calculate pulse gain (formfactor) of the pulse
  data->pulse_gain = 20.0 * log10 (secureDivision( 0.5 * PII , (gamma * data->B10 * data->Tp_1pulse)));
  ODINLOG(odinlog,normalDebug) << "pulse_gain=" << float(data->pulse_gain) << STD_endl;

  update_B10andPower();
  ODINLOG(odinlog,normalDebug) << "pulse_power=" << float(data->pulse_power) << STD_endl;

  return *this;
}

void OdinPulse::update_B10andPower() {
  Log<Seq> odinlog(this,"update_B10andPower");
  if(!is_adiabatic()) {
    ODINLOG(odinlog,normalDebug) << "flipangle=" << data->flipangle << STD_endl;
    ODINLOG(odinlog,normalDebug) << "Tp_1pulse=" << double(data->Tp_1pulse) << STD_endl;
    ODINLOG(odinlog,normalDebug) << "gamma=" << double(systemInfo->get_gamma(data->nucleus)) << STD_endl;
    ODINLOG(odinlog,normalDebug) << "pulse_gain=" << data->pulse_gain << STD_endl;

    data->B10 = data->flipangle / 90.0 / data->Tp_1pulse * 0.5 * PII /
      (systemInfo->get_gamma(data->nucleus) * pow (10.0, data->pulse_gain / 20.0));

    ODINLOG(odinlog,normalDebug) << "B10=" << data->B10 << STD_endl;
  }

  data->pulse_power = get_power_depos();
}


OdinPulse& OdinPulse::update() {
  Log<Seq> odinlog(this,"update");
  ODINLOG(odinlog,normalDebug) << "dim_mode/old_mode=" << int(data->dim_mode) << "/" << int(data->old_mode) << STD_endl;
  if(int(data->dim_mode)!=int(data->old_mode)) {
    data->shape.set_function_mode(funcMode(int(data->dim_mode)));
    data->trajectory.set_function_mode(funcMode(int(data->dim_mode)));
    data->old_mode=funcMode(int(data->dim_mode));
    ODINLOG(odinlog,normalDebug) << "old_mode=" << int(data->old_mode) << STD_endl;
    append_all_members(); // append only members which are useful in the current dim_mode
  }
  if(data->intactive) recalc_pulse();

  // axis range may have changed
  GuiProps gp;
  gp.scale[xPlotScale]=ArrayScale("time",ODIN_TIME_UNIT,0.0,data->Tp);
  data->B1.set_gui_props(gp);
  data->Gr.set_gui_props(gp);
  data->Gp.set_gui_props(gp);
  data->Gs.set_gui_props(gp);

  return *this;
}

OdinPulse& OdinPulse::recalc_pulse() {
  generate();
  set_pulse_gain();
  make_composite_pulse();
  return *this;
}



float OdinPulse::get_power_depos() const {
  Log<Seq> odinlog(this,"get_power_depos");
  unsigned int n=data->B1.length(); // may differ from npts
  float result=0.0;
  float dt = secureDivision(data->Tp , n);
  float B1amp;

  for (unsigned int i=0; i<n; i++) {
    B1amp=  double(data->B10) * cabs(data->B1[i]);
    result+= B1amp*B1amp * dt;
  }
  return result; // To get values in reasonable range
}



OdinPulse& OdinPulse::set_shape(const STD_string& shapeval) {
  data->shape.set_funcpars(shapeval);
  update();
  return *this;
}

OdinPulse& OdinPulse::set_trajectory(const STD_string& trajval) {
  data->trajectory.set_funcpars(trajval);
  update();
  return *this;
}


OdinPulse& OdinPulse::set_filter(const STD_string& filterval) {
  data->filter.set_funcpars(filterval);
  update();
  return *this;
}

OdinPulse& OdinPulse::set_shape_parameter(const STD_string& parameter_label, const STD_string& value) {data->shape.set_parameter(parameter_label,value); return *this;}
OdinPulse& OdinPulse::set_trajectory_parameter(const STD_string& parameter_label, const STD_string& value) {data->trajectory.set_parameter(parameter_label,value); return *this;}
OdinPulse& OdinPulse::set_filter_parameter(const STD_string& parameter_label, const STD_string& value) {data->filter.set_parameter(parameter_label,value); return *this;}


STD_string OdinPulse::get_shape_parameter(const STD_string& parameter_label) const {return data->shape.get_parameter(parameter_label);}
STD_string OdinPulse::get_trajectory_parameter(const STD_string& parameter_label) const {return data->trajectory.get_parameter(parameter_label);}
STD_string OdinPulse::get_filter_parameter(const STD_string& parameter_label) const {return data->filter.get_parameter(parameter_label);}

STD_string OdinPulse::get_shape() const {return data->shape.get_function_name();}
STD_string OdinPulse::get_trajectory() const {return data->trajectory.get_function_name();}
STD_string OdinPulse::get_filter() const {return data->filter.get_function_name();}



const fvector& OdinPulse::get_Grad(direction channel) const {
  if(channel==readDirection) return data->Gr;
  if(channel==phaseDirection) return data->Gp;
  if(channel==sliceDirection) return data->Gs;
  return data->Gr;
}


double OdinPulse::get_G0() const {return data->G0;}

const cvector& OdinPulse::get_B1() const {return data->B1;}

double OdinPulse::get_B10() const {return data->B10;}


void OdinPulse::simulate_pulse(SeqSimAbstract& sim, const Sample& sample) const {
  Log<Seq> odinlog(this,"simulate_pulse");

//  sim.reset_magnetization(); // will be called in prepare_simulation

  unsigned int n=get_size();
  double dt=secureDivision(get_Tp(),n);
  float gamma=systemInfo->get_gamma(data->nucleus);
  ODINLOG(odinlog,normalDebug) << "n/dt/gamma=" << n << "/" << dt << "/" << gamma << STD_endl;

  SeqSimInterval simvals;
  simvals.dt=dt;
  simvals.freq=0.0;
  simvals.phase=0.0;
  simvals.rec=0.0;

  sim.prepare_simulation(sample);
  for(unsigned int i=0; i<n; i++) {
    simvals.B1=float(data->B10)*data->B1[i];
    simvals.Gx= data->G0*data->Gr[i];
    simvals.Gy= data->G0*data->Gp[i];
    simvals.Gz= data->G0*data->Gs[i];
    sim.simulate(simvals,gamma);
  }
  sim.finalize_simulation();
}


int OdinPulse::write_rf_waveform (const STD_string& filename) const {
  Log<Seq> odinlog(this,"write_rf_waveform");
  int result=SeqPlatformProxy()->write_rf_waveform(filename,data->B1);
  if(result<0) {
    ODINLOG(odinlog,errorLog) << " failed" << STD_endl;
  }
  return result;
}


int OdinPulse::load_rf_waveform (const STD_string& filename) {
  Log<Seq> odinlog(this,"load_rf_waveform");
  cvector B1load;
  B1load.reserve(systemInfo->get_max_rf_samples());
  int n=SeqPlatformProxy()->load_rf_waveform(filename,B1load);
  if(n>0) {
    resize(n);
    data->B1=B1load;
  }
  if(n>=0) n=0;
  if(n<0) {
    ODINLOG(odinlog,errorLog) << " failed" << STD_endl;
  }
  return n;
}
