/***************************************************************************
                          seqcounter.h  -  description
                             -------------------
    begin                : Fri Jan 19 2007
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQCOUNTER_H
#define SEQCOUNTER_H


#include <odinseq/seqvec.h>
#include <odinseq/seqdriver.h>
#include <odinseq/seqtree.h>

/**
  * @addtogroup odinseq_internals
  * @{
  */


class SeqCounter;  // forward declaration
class SeqObjList; // forward declaration

/**
  * The base class for platform specific drivers of counters objects to iterate over vectors
  */
class SeqCounterDriver : public SeqDriverBase {

 public:
  SeqCounterDriver() {}
  virtual ~SeqCounterDriver() {}

/**
  * Will be called once during prepping the sequence
  */
  virtual bool prep_driver() = 0;

/**
  * Function to update driver internals which is called before
  * the driver is queried for actions, seqlist can be zero
  */
  virtual void update_driver(const SeqCounter* counter, const SeqObjList* seqlist, const List<SeqVector,const SeqVector*,const SeqVector&>* vectors) const = 0;

/**
  * Duration the loop itself takes before iteration is started
  */
  virtual double get_preduration () const = 0;

/**
  * Duration the loop itself takes after its kernel is iterated
  */
  virtual double get_postduration() const = 0;

/**
  * Duration the loop itself takes before each iteration
  */
  virtual double get_preduration_inloop() const = 0;

/**
  * Duration the loop itself takes after each iteration
  */
  virtual double get_postduration_inloop() const = 0;


/**
  * Asks driver whether a program has to be created at all for the given context and loopkernel,
  * useful to avoid 'empty' loops in the program
  */
  virtual bool create_program(programContext& context, const STD_string& loopkernel) const = 0;

/**
  * Returns the head of the loop, i.e. code before the loop kernel
  */
  virtual STD_string get_program_head(programContext& context, const STD_string& loopkernel, unsigned int times) const  = 0;

/**
  * Returns the tail of the loop, i.e. code after the loop kernel
  */
  virtual STD_string get_program_tail(programContext& context, const STD_string& loopkernel, unsigned int times) const  = 0;

/**
  * Returns the head of the loop, i.e. code before the loop kernel if the loop is unrolled
  */
  virtual STD_string get_program_head_unrolled(programContext& context, unsigned int index) const  = 0;

/**
  * Returns commands to iterate vectors manually
  */
  virtual STD_string get_program_iterator(programContext& context) const  = 0;


/**
  * Will be called before vectors are prepped for the current iteration
  */
  virtual void pre_vecprepevent (eventContext& context) const = 0;

/**
  * Will be called after vectors are prepped for the current iteration
  * If repcounter>=0, the repetition display of the platform will be updated to 'repcounter'
  */
  virtual void post_vecprepevent(eventContext& context, int repcounter) const = 0;

/**
  * Will be called if any attribute of the counter was changed
  */
  virtual void outdate_cache() const = 0;

/**
  * Asks driver whether the loop should be unrolled i.e. whether each iteration will be
  * explicitely written to the program, seqlist can be zero
  */
  virtual bool unroll_program(const SeqCounter* counter, const SeqObjList* seqlist, const List<SeqVector,const SeqVector*,const SeqVector&>* vectors, programContext& context) const = 0;

/**
  * Return an exact copy of this driver
  */
  virtual SeqCounterDriver* clone_driver() const = 0;
};



///////////////////////////////////////////////////////////////////////////////////


/**
  * The base class for objects iterating through vectors by incremental counting
  */
class SeqCounter : public Handled<const SeqCounter*>, public virtual SeqTreeObj {

 public:
/**
  * Construct an empty counter object with the given label
  */
  SeqCounter(const STD_string& object_label = "unnamedSeqCounter");

/**
  * Constructs a copy of 'sl'
  */
  SeqCounter(const SeqCounter& sc);


  virtual ~SeqCounter() {}

/**
  * Assignment operator
  */
  SeqCounter& operator = (const SeqCounter& sc);

/**
  * The number of iterations of this counter object
  */
  virtual int get_times() const;


/**
  * Adds a vector to this counter
  */
  virtual void add_vector(const SeqVector& seqvector);



  // counter functions
  int get_counter() const {return counter;} // also used by SeqVector
  bool counter_is_active() const {return counter!=-1;}
  void init_counter(unsigned int start=0) const;
  void increment_counter() const {counter++;}
  void disable_counter() const {counter=-1;}

  // helper functions (also used by platform drivers)
  bool prep_veciterations() const;


 protected:

  // helpers to reduce template code
  mutable STD_list<const SeqVector*>::const_iterator veciter;
  STD_list<const SeqVector*>::const_iterator get_vecbegin() const {return vectors.get_const_begin();}
  STD_list<const SeqVector*>::const_iterator get_vecend() const {return vectors.get_const_end();}
  unsigned int n_vectors() const {return vectors.size();}

  List<SeqVector,const SeqVector*,const SeqVector&> vectors;


  void clear_vectorlist() {vectors.clear();}


  mutable SeqDriverInterface<SeqCounterDriver> counterdriver;


  // overwriting virtual functions from SeqClass
  bool prep();
  void clear_container();

 private:
  friend class SeqVector;


  // helper functions (also used by SeqVector)
  virtual bool unroll_program(programContext& context) const {return true;}

  void set_vechandler_for_all() const;

  mutable int counter;


};

/** @}
  */

#endif
