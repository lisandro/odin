/***************************************************************************
                          seqsimvec.h  -  description
                             -------------------
    begin                : Mon Aug 9 2004
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQSIMVEC_H
#define SEQSIMVEC_H

#include <odinseq/seqvec.h>

/**
  * @ingroup odinseq_internals
  * This vector class is used to group other vector objects together so that
  * they can be updated simultaneously at each iteration.
  */
class SeqSimultanVector : public SeqVector, public virtual SeqClass, public List<SeqVector, const SeqVector*, const SeqVector&> {

 public:

/**
  * Construct an empty vector of sequence objects with the given label
  */
  SeqSimultanVector(const STD_string& object_label="unnamedSeqSimultanVector") : SeqClass(), SeqVector(object_label)  {set_label(object_label);}

/**
  * Constructs a copy of 'ssv'
  */
  SeqSimultanVector(const SeqSimultanVector& ssv);

/**
  * Assignment operator that makes this vector object become a copy of 'ssv'
  */
  SeqSimultanVector& operator = (const SeqSimultanVector& ssv);

/**
  * Appends sv to the list of managed vectors
  */
  SeqSimultanVector& operator += (const SeqVector& sv);


/**
  * clears the list of managed vectors
  */
  SeqSimultanVector& clear() { List<SeqVector, const SeqVector*, const SeqVector&>::clear(); return *this;}


  // implemented virtual functions from SeqVector
  svector get_vector_commands(const STD_string& iterator) const;
  unsigned int get_vectorsize() const;
  unsigned int get_numof_iterations() const;
  STD_string get_loopcommand() const;
  nestingRelation get_nesting_relation() const;
  bool needs_unrolling_check() const;
  bool prep_iteration() const;
//  bool is_acq_vector() const;
  const SeqVector& set_vechandler(const SeqCounter* sc) const;
  bool is_qualvector() const;


 private:

  // implemented virtual functions from SeqClass
  void clear_container();

  
};



#endif
