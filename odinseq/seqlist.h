/***************************************************************************
                          seqlist.h  -  description
                             -------------------
    begin                : Wed Aug 14 2002
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/



#ifndef SEQLIST_H
#define SEQLIST_H


#include <tjutils/tjlist.h>

#include <odinseq/seqobj.h>
#include <odinseq/seqdriver.h>
#include <odinseq/seqrotmatrixvector.h>


#define _MIN_PPG_CMDSIZE_ 25
#define _MIN_GP_CMDSIZE_ 50




/**
  * @addtogroup odinseq_internals
  * @{
  */


/**
  * The base class for platform specific drivers of lists of sequence objects
  */
class SeqListDriver : public SeqDriverBase {

 public:
  SeqListDriver() {}
  virtual ~SeqListDriver() {}

  virtual STD_string pre_program (programContext& context, const SeqRotMatrixVector* rotmats) const = 0;
  virtual STD_string post_program(programContext& context, const SeqRotMatrixVector* rotmats) const = 0;

  virtual STD_string get_itemprogram(const SeqTreeObj* item, programContext& context) const = 0;

  virtual void pre_event (eventContext& context, const RotMatrix* rotmatrix) const = 0;
  virtual void post_event(eventContext& context, const RotMatrix* rotmatrix) const = 0;

  virtual void pre_itemevent (const SeqTreeObj* item, eventContext& context) const = 0;
  virtual void post_itemevent(const SeqTreeObj* item, eventContext& context) const = 0;

  virtual bool prep_driver() = 0;

  virtual SeqListDriver* clone_driver() const = 0;
};

/** @}
  */


//////////////////////////////////////////////////////////////////////

class SeqObjLoop; // forward declaration
class SeqDecoupling; // forward declaration
class SeqGradObjInterface; // forward declaration
class SeqGradChan; // forward declaration
class SeqGradChanList; // forward declaration

/**
  * @ingroup odinseq
  *
  * \brief Container for sequence objects
  *
  * This is a container class for other sequence objects.
  */
class SeqObjList : public SeqObjBase, public List<SeqObjBase, const SeqObjBase*, const SeqObjBase& > {

 public:

/**
  * Construct an empty sequence container with the given label
  */
  SeqObjList(const STD_string& object_label="unnamedSeqObjList");

/**
  * Constructs a copy of 'so'
  */
  SeqObjList(const SeqObjList& so);

  ~SeqObjList();

/**
  * Assignment operator that makes this sequence container become a copy of 'so'
  */
  SeqObjList& operator = (const SeqObjList& so);

/**
  * Special assignment for loops
  */
  SeqObjList& operator = (const SeqObjLoop& sl);


/**
  * Special assignment for decoupling
  */
  SeqObjList& operator = (const SeqDecoupling& sd);


/**
  * Special treatment for single non-container sequence objects
  */
  SeqObjList& operator = (const SeqObjBase& soa);

/**
  * Special treatment for assignment from gradient objects
  */
  SeqObjList& operator = (SeqGradObjInterface& sgoa) {clear(); return (*this)+=sgoa;}

/**
  * Special treatment for assignment from gradient objects
  */
  SeqObjList& operator = (SeqGradChan& sgc) {clear(); return (*this)+=sgc;}

/**
  * Special treatment for assignment from gradient objects
  */
  SeqObjList& operator = (SeqGradChanList& sgcl) {clear(); return (*this)+=sgcl;}

/**
  * Special treatment for appending sequence objects
  */
  SeqObjList& operator += (const SeqObjBase& soa);

/**
  * Special treatment for appending gradient objects
  */
  SeqObjList& operator += (SeqGradObjInterface& sgoa);


/**
  * Special treatment for appending gradient objects
  */
  SeqObjList& operator += (SeqGradChan& sgc);

/**
  * Special treatment for appending gradient objects
  */
  SeqObjList& operator += (SeqGradChanList& sgcl);



/**
  * Specifies an extra vector of rotatition matrices to be used with this container.
  * The part held by this container will be executed with the current rotation matrix.
  * The sequence vector 'matrixVec' can then be attached to a loop to iterate over the different matrices.
  */
  SeqObjList& set_gradrotmatrixvector(const SeqRotMatrixVector& matrixVec) {gradrotmatrixvec.set_handled(&matrixVec); return *this;}



  // overloading virtual function from SeqTreeObj
  STD_string get_program(programContext& context) const;
  unsigned int event(eventContext& context) const;
  double get_duration() const;
  STD_string get_properties() const;
  void query(queryContext& context) const;
  RecoValList get_recovallist(unsigned int reptimes, LDRkSpaceCoords& coords) const;
  SeqValList get_freqvallist(freqlistAction action) const;
  SeqValList get_delayvallist() const;
  double get_rf_energy() const;

 protected:

  // overloading virtual function from SeqClass
  void clear_container() {clear();}
  bool prep();

 private:
  friend class SeqGradChan;
  friend class SeqOperator;

  // overloading virtual function from SeqObjBase
  bool contains_list() const {return true;}


  mutable SeqDriverInterface<SeqListDriver> listdriver;

  Handler<const SeqRotMatrixVector* > gradrotmatrixvec;
  static Handler<const SeqRotMatrixVector* > current_gradrotmatrixvec;

};

#endif
