/***************************************************************************
                          odinpulse.h  -  description
                             -------------------
    begin                : Tue Jun 4 2002
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef ODINPULSE_H
#define ODINPULSE_H

#include <odinpara/ldrfunction.h>
#include <odinpara/ldrfilter.h>

#include <odinseq/seqpuls.h>


class SeqSimAbstract;  // forward declaration
class Sample;  // forward declaration



#define _BOUNDARY_VALUE_                   0.005
#define _SETREFGAIN_ADIABATIC_INTERVAL_    0.01
#define _SETREFGAIN_ADIABATIC_INCR_FACTOR_ 1.1
#define _SETREFGAIN_ITERATIONS_            3

 // Parameter values of this optimization parameter must be in range [0,1]
#define _TRAJ_OPTIMIZE_PARLABEL_           "FreeParameter"

//////////////////////////////////////////////////

/**
  * @addtogroup odinseq_internals
  * @{
  */

/**
  *
  * Wrapper class for trajectory functions.
  */
class LDRtrajectory : public LDRfunction,  public StaticHandler<LDRtrajectory> {

 public:
  LDRtrajectory(const STD_string& ldrlabel="unnamedLDRtrajectory") : LDRfunction(trajFunc,ldrlabel) {}

  LDRtrajectory(const LDRtrajectory& jt) : LDRfunction(jt) {}

  LDRtrajectory& operator = (const LDRtrajectory& jt) {LDRfunction::operator = (jt); return *this;}

  void init_trajectory(OdinPulse* pls=0) {if(allocated_function) allocated_function->init_trajectory(pls);}

  const kspace_coord& calculate(float s) const;

  const traj_info& get_traj_info() const;

  static void init_static();
  static void destroy_static();
};


//////////////////////////////////////////////////

/**
  *
  * Wrapper class for pulse shape functions.
  */
class LDRshape : public LDRfunction,  public StaticHandler<LDRshape> {

 public:
  LDRshape(const STD_string& ldrlabel="unnamedLDRshape") : LDRfunction(shapeFunc,ldrlabel) {}

  LDRshape(const LDRshape& js) : LDRfunction(js) {}

  LDRshape& operator = (const LDRshape& js) {LDRfunction::operator = (js); return *this;}

  void init_shape() {if(allocated_function) allocated_function->init_shape();}

  STD_complex calculate(const kspace_coord& coord ) const;
  STD_complex calculate(float s, float Tp) const;

  const shape_info& get_shape_info() const;
  farray get_excitation_mask(float fov, unsigned int size) const;


  static void init_static();
  static void destroy_static();
};



//////////////////////////////////////////////////

struct OdinPulseData; // forward declaration

/** @}
  */

//////////////////////////////////////////////////

/**
  * @addtogroup odinseq
  * @{
  */
  
/**
  * \brief  Advandced RF pulses
  *
  *
  * This class represents an advanced RF pulse. The RF and gradient
  * shape will be calculated according to a set of given parameters.
  */
class OdinPulse : public LDRblock, public virtual SeqClass {

 public:

/**
  * Constructs an OdinPulse with the given label. If 'interactive' is set to 'true', the RF, gradient shape and
  * pulse_gain will be recalculted each time a parameter has been changed
  */
  OdinPulse(const STD_string& pulse_label="unnamedOdinPulse",bool interactive=false);

/**
  * Constructs an OdinPulse which is a copy of 'pulse'
  */
  OdinPulse(const OdinPulse& pulse);


  virtual ~OdinPulse();

/**
  * This assignment operator will make this OdinPulse become an exact copy of 'pulse'.
  */
  OdinPulse& operator = (const OdinPulse& pulse);

/**
  *
  * Sets the pulse mode (dimensionality) of the pulse
  */
  OdinPulse& set_dim_mode(funcMode dmode);

/**
  *
  * Returns the pulse mode (dimensionality) of the pulse
  */
  funcMode get_dim_mode() const;

/**
  *
  * Returns the number of data points
  */
  unsigned int get_size() const;


/**
  *
  * specifies number of data points
  */
  OdinPulse& resize(unsigned int newsize);

/**
  *
  * Sets the duration of the pulse
  */
  OdinPulse& set_Tp(double duration);

/**
  *
  * Returns the duration of the pulse
  */
  double get_Tp() const;

/**
  *
  * Specifies whether to consider maximum gradient strength and
  * slew-rate while calculating the trajectory.
  */
  OdinPulse& set_consider_system_cond(bool flag);

/**
  *
  * Returns whether to consider maximum gradient strength and
  * slew-rate while calculating the trajectory.
  */
  bool get_consider_system_cond() const;



/**
  *
  * Specifies whether to consider Nyquist-theorem while sampling along
  * the k-space trajectory.
  */
  OdinPulse& set_consider_Nyquist_cond(bool flag);

/**
  *
  * Returns whether to consider Nyquist-theorem while sampling along
  * the k-space trajectory.
  */
  bool get_consider_Nyquist_cond() const;



/**
  *
  * Sets the spatial resolution if the pulse is calculated according
  * to the small tip angle approximation, otherwise this has no effect.
  */
  OdinPulse& set_spat_resolution(double sigma);

/**
  *
  * Sets whether minimum spatial resolution is used.
  */
  OdinPulse& use_min_spat_resolution(bool flag);

/**
  *
  * Returns the spatial resolution
  */
  double get_spat_resolution() const;

/**
  *
  * Sets the field of excitation, i.e. the spatial area where no aliasing occurs
  */
  OdinPulse& set_field_of_excitation(double fox);

/**
  *
  * Returns the field of excitation
  */
  double get_field_of_excitation() const;

/**
  *
  * Sets the spatial offset in the given direction
  */
  OdinPulse& set_spatial_offset(direction direction, double offset);

/**
  *
  * Returns the spatial offset in the given direction
  */
  double get_spatial_offset(direction direction) const;

/**
  *
  * Sets the nucleus for the pulse
  */
  OdinPulse& set_nucleus(const STD_string& nucleusname);

/**
  *
  * Returns the nucleus for the pulse
  */
  STD_string get_nucleus() const;

/**
  *
  * Sets the type of the pulse, this is necessary for calculating the pulse gain of
  * adiabatic pulses and optimization of pulses.
  */
  OdinPulse& set_pulse_type(pulseType type);

/**
  *
  * Returns the type of the pulse
  */
  pulseType get_pulse_type() const;

/**
  *
  * Specifies the composite pulse to use.
  * A composite pulse can be specified by a string
  * of the form a1(x2) a2(x2) ...  where a1,a2,...
  * are the flipangles in degree and x1,x2,...
  * are the axes, .e.g. X,-X,Y or -Y.
  */
  OdinPulse& set_composite_pulse(const STD_string& cpstring);

/**
  *
  * Returns a 2dim array with the first index indicating the pulse in the composite pulse
  * train and the second index selects the flipangle(=0) or pulse phase(=1) of the sub-pulse.
  */
  farray get_composite_pulse_parameters() const;

/**
  *
  * Returns 'true' if the pulse is a composite pulse
  */
  bool is_composite_pulse() const;

/**
  *
  * Returns the number of composite pulses
  */
  unsigned int get_numof_composite_pulse() const;

/**
  *
  * Returns whether the pulse shape is adiabatic
  */
  bool is_adiabatic() const;


/**
  *
  * Returns the relative center of the pulse,
  * i.e. the point where the center of k-space is passed.
  * The value will be in the range in the range [0,1].
  */
  float get_rel_center() const;

/**
  *
  * Returns the pulse gain
  */
  double get_pulse_gain() const;

/**
  *
  * Returns the correction factor for the IDEA flipangle
  */
  float get_flipangle_corr_factor() const;

/**
  *
  * Sets the flip angle
  */
  OdinPulse& set_flipangle(double angle);

/**
  *
  * Returns the flip angle
  */
  double get_flipangle() const;

/**
  *
  * Generates the pulse
  */
  OdinPulse& generate();

/**
  *
  * Calculates the pulse gain in comparison to a 90deg 1ms block-shaped pulse
  */
  OdinPulse& set_pulse_gain();


/**
  *
  * Update all relations and recalculate pulse
  */
  virtual OdinPulse& update();


/**
  *
  * Calculates the power deposition of the pulse
  */
  float get_power_depos() const;


/**
  *
  * Specifies the pulse shape and its parameters by a string where the first
  * element is the function name followed by the parameters in braces separated by commas
  */
  OdinPulse& set_shape(const STD_string& shapeval);



/**
  *
  * Specifies the pulse trajectory and its parameters by a string where the first
  * element is the function name followed by the parameters in braces separated by commas
  */
  OdinPulse& set_trajectory(const STD_string& shapeval);


/**
  *
  * Specifies the pulse filter and its parameters by a string where the first
  * element is the function name followed by the parameters in braces separated by commas
  */
  OdinPulse& set_filter(const STD_string& shapeval);


/**
  *
  * Sets the value of the pulse shape parameter which has the label 'parameter_label' to 'value'
  */
  OdinPulse& set_shape_parameter(const STD_string& parameter_label, const STD_string& value);

/**
  *
  * Sets the value of the pulse trajectory parameter which has the label 'parameter_label' to 'value'
  */
  OdinPulse& set_trajectory_parameter(const STD_string& parameter_label, const STD_string& value);

/**
  *
  * Sets the value of the pulse filter parameter which has the label 'parameter_label' to 'value'
  */
  OdinPulse& set_filter_parameter(const STD_string& parameter_label, const STD_string& value);

/**
  *
  * Returns the value of the pulse shape parameter which has the given label
  */
  STD_string get_shape_parameter(const STD_string& parameter_label) const;

/**
  *
  * Returns the value of the pulse trajectory parameter which has the given label
  */
  STD_string get_trajectory_parameter(const STD_string& parameter_label) const;

/**
  *
  * Returns the value of the pulse filter parameter which has the given label
  */
  STD_string get_filter_parameter(const STD_string& parameter_label) const;


/**
  *
  * Returns the label of the current pulse shape
  */
  STD_string get_shape() const;

/**
  *
  * Returns the label of the current pulse trajectory
  */
  STD_string get_trajectory() const;

/**
  *
  * Returns the label of the current pulse filter
  */
  STD_string get_filter() const;

/**
  *  Returns the gradient waveform for the specified channel
  */
  const fvector& get_Grad(direction channel) const;

/**
  *
  *  Returns the scaling factor for the gradient channels
  */
  double get_G0() const;

/**
  *  Returns the complex RF waveform
  */
  const cvector& get_B1() const;

/**
  *
  *  Returns the scaling factor for the RF channels
  */
  double get_B10() const;

/**
  *  Simulates the effect of the pulse using simulator 'sim'
  *  using virtual sample 'sample'.
  */
  void simulate_pulse(SeqSimAbstract& sim, const Sample& sample) const;

/**
  *
  * This function writes the RF waveform to file in the native (binary)
  * format of the current platform
  */
  int write_rf_waveform (const STD_string& filename) const;

/**
  *
  * This function loads the RF waveform from file in  the native (binary)
  * format of the current platform
  */
  int load_rf_waveform (const STD_string& filename);


  // overloading virtual functions of LDRblock
  int load(const STD_string& filename, const LDRserBase& serializer=LDRserJDX());


 protected:
  virtual void update_B10andPower();

  OdinPulse& recalc_pulse();

  double get_Tp_1pulse() const;

 private:
  int append_all_members();

  static float ensure_unit_range(float x);

  OdinPulse& make_composite_pulse();


  static float gradient_system_max (const fvector& Gvec, float Gmax, float maxslew, float Tp);
  static float max_kspace_step(const fvector& Gz,float gamma,float Tp,float G0);
  static float max_kspace_step2(const fvector& Gx, const fvector& Gy, float gamma,float Tp,float G0);

  OdinPulse& resize_noupdate(unsigned int newsize);


  // this pointer holds all data of the pulse
  OdinPulseData* data;

};

/** @}
  */

#endif
