/***************************************************************************
                          seqplot.h  -  description
                             -------------------
    begin                : Sat Sep 25 2004
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQPLOT_H
#define SEQPLOT_H

#include <odinpara/odinpara.h> // for units

class RotMatrix; // forward declaration

/**
  * @addtogroup odinseq_internals
  * @{
  */

enum plotChannel {B1re_plotchan=0, B1im_plotchan, rec_plotchan, signal_plotchan, freq_plotchan, phase_plotchan, Gread_plotchan, Gphase_plotchan, Gslice_plotchan, numof_plotchan};

enum markType                  {no_marker=0, exttrigger_marker, halttrigger_marker, snapshot_marker, reset_marker, acquisition_marker, endacq_marker, excitation_marker, refocusing_marker, storeMagn_marker, recallMagn_marker, inversion_marker, saturation_marker, numof_markers };
static const char* markLabel[]={"none",     "exttrigger",       "halttrigger",     "snapshot",      "reset",      "acquisition",      "endacq",      "excitation",      "refocusing",      "storeMagn",      "recallMagn",      "inversion",      "saturation" };
AVOID_CC_WARNING(markLabel)

enum timecourseMode                   {tcmode_curves=0, tcmode_plain,   tcmode_slew_rate,                  tcmode_kspace,         tcmode_M1,                                tcmode_M2,                                  tcmode_b_trace,                         tcmode_backgr_kspace,  tcmode_backgr_crossterm,                tcmode_eddy_currents, numof_tcmodes};
static const char* timecourseLabel[]= {"Curves",        "Plain",        "Slew Rate",                       "k-Space",             "1st Grad. Moment",                       "2nd Grad. Moment",                         "b-Value Trace",                        "Backgr. k-Space",     "Backgr. Crossterm",                    "Eddy Currents"};
static const char* timecoursePrefix[]={"G",              "G",           "dG",                              "k",                   "M1",                                     "M2",                                       "b",                                    "k",                   "c",                                    "G"};
static const char* timecourseUnit[]=  {ODIN_GRAD_UNIT,  ODIN_GRAD_UNIT, ODIN_GRAD_UNIT "/" ODIN_TIME_UNIT, "rad/" ODIN_SPAT_UNIT, "rad*" ODIN_TIME_UNIT "/" ODIN_SPAT_UNIT, "rad*" ODIN_TIME_UNIT "^2/" ODIN_SPAT_UNIT, ODIN_TIME_UNIT "/" ODIN_SPAT_UNIT "^2", "rad/" ODIN_SPAT_UNIT, ODIN_TIME_UNIT "/" ODIN_SPAT_UNIT "^2", ODIN_GRAD_UNIT};
AVOID_CC_WARNING(timecourseLabel)
AVOID_CC_WARNING(timecoursePrefix)
AVOID_CC_WARNING(timecourseUnit)



////////////////////////////////////////////////////////////////////

/**
  *  Special data structure suitable to display a single curve in QwtPlot
  */
struct Curve4Qwt {
  Curve4Qwt() : size(0), x(0), y(0), spikes(false), has_freq_phase(false), gradmatrix(0) {}
  bool operator == (const Curve4Qwt&) const {return false;} // dummy operator
  bool operator <  (const Curve4Qwt&) const {return false;} // dummy operator

  double get_bounds(bool endpoint) const {if(!size) return 0.0; if(endpoint) return x[size-1]; else return x[0];}

  const char* label;
  plotChannel channel;

  int size;
  double* x;
  double* y;
  bool spikes; // values will not be interpolated between points, draw sticks instead

  bool has_freq_phase;
  double freq;
  double phase;

  const RotMatrix* gradmatrix;

};

////////////////////////////////////////////////////////////////////


/**
  *  Special data structure suitable to display a simple marker in QwtPlot
  */
struct Marker4Qwt {
  bool operator == (const Marker4Qwt&) const {return false;} // dummy operator
  bool operator <  (const Marker4Qwt&) const {return false;} // dummy operator
  double get_bounds(bool) const {return x;}
  const char* label;
  double x;
  markType type;
};

////////////////////////////////////////////////////////////////////

/**
  *  Special data structure suitable to display a marker with stored values in QwtPlot
  */
struct TimecourseMarker4Qwt {
  bool operator == (const TimecourseMarker4Qwt&) const {return false;} // dummy operator
  bool operator <  (const TimecourseMarker4Qwt&) const {return false;} // dummy operator
  double get_bounds(bool) const {return x;}
  double x;
  double y[numof_plotchan];
  markType type;
};

////////////////////////////////////////////////////////////////////

/**
  *  Special data structure suitable to hold digitized timecourses
  */
struct SeqTimecourseData {
  SeqTimecourseData() : size(0), x(0), n_rec_points(0) {for(int i=0; i<numof_plotchan; i++) y[i]=0;}

  unsigned int size;
  double* x;
  double* y[numof_plotchan];
  unsigned int n_rec_points;
};

/////////////////////////////////////////////////////////////////////

class SeqSimFeedbackAbstract {

 public:

  virtual void plot_vector(double timepoint, float M[3], float* dM) = 0;

};

////////////////////////////////////////////////////////////////////

class ProgressMeter; // forward declaration

/**
  *  Interface class for SeqPlotData
  */
class SeqPlotDataAbstract {

 public:
  virtual ~SeqPlotDataAbstract() {}

  virtual void reset() = 0;

  virtual void get_curves(STD_list<Curve4Qwt>::const_iterator& result_begin, STD_list<Curve4Qwt>::const_iterator& result_end, double starttime, double endtime, double max_highres_interval  ) const = 0;

  virtual void get_signal_curves(STD_list<Curve4Qwt>::const_iterator& result_begin, STD_list<Curve4Qwt>::const_iterator& result_end, double starttime, double endtime  ) const = 0;

  virtual void get_markers(STD_list<Marker4Qwt>::const_iterator& result_begin, STD_list<Marker4Qwt>::const_iterator& result_end, double starttime, double endtime  ) const = 0;

  virtual bool timecourse_created(timecourseMode type) const = 0;

  virtual bool create_timecourses(timecourseMode type, const STD_string& nucleus, ProgressMeter* progmeter) const = 0;

  virtual const SeqTimecourseData* get_timecourse(timecourseMode type) const = 0;

  virtual const SeqTimecourseData* get_subtimecourse(timecourseMode type, double starttime, double endtime) const = 0;

  virtual void get_timecourse_markers(timecourseMode type, STD_list<TimecourseMarker4Qwt>::const_iterator& result_begin, STD_list<TimecourseMarker4Qwt>::const_iterator& result_end, double starttime, double endtime ) const = 0;

  virtual double get_total_duration() const = 0;

  virtual unsigned int n_frames() const = 0;

  virtual LDRblock& get_opts(bool timecourse_opts, bool simulation_opts) const = 0;

  virtual void set_coilsdir(const STD_string& coilsdir) const = 0;

  virtual void add_signal_curve(const Curve4Qwt& signal_curve) const = 0;

  virtual bool monitor_simulation() const = 0;

  virtual bool simulate(const STD_string& fidfile, const STD_string& samplefile, ProgressMeter* progmeter, SeqSimFeedbackAbstract* feedback) const = 0;

  virtual bool has_curves_on_channel(plotChannel chan) const = 0;

};



/** @}
  */



#endif
