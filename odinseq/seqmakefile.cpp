#include "seqmakefile.h"
#include "seqplatform.h"
#include "seqdriver.h"

#ifndef NO_CMDLINE

STD_string SeqMakefile::get_exe_postfix() {
  STD_string result;
#ifdef USING_WIN32
  result=".exe";
#endif
  return result;
}


STD_string SeqMakefile::get_so_postfix() {
  STD_string result=".so";
#ifdef MACOS
  result=".dylib";
#endif
#ifdef USING_WIN32
  result=".dll";
#endif
  return result;
}

STD_string SeqMakefile::get_obj_postfix() {
  STD_string result=".o";
#ifdef USING_WIN32
#ifndef USING_GCC
  result=".obj";
#endif
#endif
  return result;
}



SeqMakefile::SeqMakefile(const STD_string& methlabel, const STD_string& odin_install_prefix,
            const STD_string& compiler, const STD_string& compiler_flags, const STD_string& linker,
            const STD_string& extra_includes, const STD_string& extra_libs)
 : inst_prefix(odin_install_prefix), cxx(compiler), cxxflags(compiler_flags), ld(linker), add_includes(extra_includes), add_libs(extra_libs) {
   set_label(methlabel);
//   SeqPlatformCreator(); // create platform instances/drivers
}


STD_string SeqMakefile::get_Makefile(const STD_string& methroot) const {
  STD_string result;
  unsigned int i;


  result+="all: "+get_methlabel()+get_exe_postfix()+" ";
  /*if(pf!=paravision)*/ result+=get_methlabel()+get_so_postfix();
  result+="\n\n";

  // code for shared object
  svector cmd_chain(get_method_compile_chain(false,true));
  if(cmd_chain.size()<3) return result;

  result+=get_methlabel()+get_obj_postfix()+": "+get_methlabel()+".cpp\n";
  result+="	"+cmd_chain[0]+"\n"; // stuff for unique id
  result+="	"+cmd_chain[1]+"\n"; // compilation of object
  result+="\n\n";

  result+=get_methlabel()+get_so_postfix()+": "+get_methlabel()+get_obj_postfix()+"\n";
  for(i=2; i<cmd_chain.size(); i++) result+="	"+cmd_chain[i]+"\n";
  result+="\n\n";

  // code for executable
  cmd_chain=get_method_compile_chain(true,false);
  if(cmd_chain.size()<3) return result;

  result+=get_methlabel()+get_exe_postfix()+": "+get_methlabel()+get_obj_postfix()+"\n";
  for(i=2; i<cmd_chain.size(); i++) result+="	"+cmd_chain[i]+"\n";
  result+="\n\n";

  result+=STD_string("clean:\n")+"	"+get_method_clean()+"\n\n";

  result+=STD_string("install: ")+get_methlabel()+get_exe_postfix()+"\n	"+get_method_install(methroot)+"\n\n";

  return result;
}


bool valid_c_char(char c, bool firstchar) {
  bool result=false;
  if(c=='_') result=true;
  if(c>='a' && c<='z') result=true;
  if(c>='A' && c<='Z') result=true;
  if(!firstchar && c>='0' && c<='9') result=true;
  return result;
}

STD_string valid_c_label(const STD_string& label) {
  STD_string clabel(label);
  if(!clabel.length()) {
    clabel="Label";
  } else {
    if(!valid_c_char(clabel[0],true)) clabel="_"+clabel;
    for(unsigned int i=0; i<clabel.length(); i++) {
      if(!valid_c_char(clabel[i],false)) clabel[i]='_';
    }
  }
  return clabel;
}


STD_string SeqMakefile::get_methdefines(const STD_string& main, const STD_string& classlabel) const {
  return " -DMETHOD_LABEL="+get_methlabel()+" -DODINMAIN="+main+" -DMETHOD_CLASS="+classlabel+" ";
}



svector SeqMakefile::get_method_compile_chain(bool executable, bool shared_object) const {

  STD_string includes;
  if(inst_prefix!="") includes=STD_string(" -I\"")+inst_prefix+SEPARATOR_STR+"include\" ";
  includes+=" "+add_includes+" ";
  STD_string libdir;
  if(STD_string(LIBDIR_CONFIGURE)!="") libdir=STD_string(" -L\"")+LIBDIR_CONFIGURE+"\" "; // For Debian package
  else if(inst_prefix!="") libdir=STD_string(" -L\"")+inst_prefix+SEPARATOR_STR+"lib\" ";
  libdir+=" "+add_libs+" ";


  // UNIX defaults
  STD_string compiler(CXX);
  STD_string linker(CXX);

  if(cxx!="") compiler=cxx;
  if(ld!="") linker=ld;

#ifdef USING_WIN32
  // Windows compile chain

  STD_string odin_libs;
#ifdef USING_GCC // Assume MinGW
  odin_libs=" -lodinseq -lodinpara -ltjutils ";
#else // Assume Visual C++ 6
  odin_libs+=" \""+inst_prefix+SEPARATOR_STR+"lib"+SEPARATOR_STR+"libtjutils.lib\" ";
  odin_libs+=" \""+inst_prefix+SEPARATOR_STR+"lib"+SEPARATOR_STR+"libodinpara.lib\" ";
  odin_libs+=" \""+inst_prefix+SEPARATOR_STR+"lib"+SEPARATOR_STR+"libodinseq.lib\" ";
#endif

  STD_string defines=get_methdefines("__declspec(dllexport)main",valid_c_label(get_methlabel()));

  svector result; result.resize(2);
  result[0]="\""+compiler+"\" -c "+includes+get_methlabel()+".cpp "+cxxflags+defines;
#ifdef USING_GCC // Assume MinGW, BASELIBS must be specified explicitely since libtool is not used
  result[1]="\""+linker+"\" -shared -o _"+get_methlabel()+get_so_postfix()+" "+get_methlabel()+get_obj_postfix()+" "+odin_libs+" "+libdir+" "+BASELIBS+" "+LDFLAGS;
#else // Assume Visual C++ 6
  result[1]="\""+linker+"\" /DLL /OUT:_"+get_methlabel()+get_so_postfix()+" "+get_methlabel()+get_obj_postfix()+" "+odin_libs+" "+libdir+" "+LDFLAGS;
#endif

#else
  // UNIX compile chain

  STD_string odin_libs(" -lodinseq -lodinpara -ltjutils ");

  if(executable) odin_libs+=STD_string(BASELIBS)+" "; // for creating static binary

  unsigned int chainsize=2;
  if(executable) chainsize+=1;
  if(shared_object) chainsize+=2;

  svector result; result.resize(chainsize);


  STD_string unique_id_file="unique_id_"+get_methlabel();

  STD_string defines=get_methdefines("main","SeqMethod`cat "+unique_id_file+"`"); // need this class label for seqtree dump of seqtest

  int ichain=0;

  // make sure that we have a unique file name for the shared object (for re-linking the same method)
  result[ichain]="echo \"_`date +%y%m%d%H%M%S`"+get_methlabel()+"\" > "+unique_id_file;
  ichain++;

  result[ichain]=compiler+" -c "+get_methlabel()+".cpp "+includes+cxxflags+" "+PIC_FLAGS+" "+defines;
  ichain++;

  if(executable) {
    STD_string exeflags;
#ifdef MACOS
    exeflags="-Wl,-bind_at_load "; // avoids lots of warnings
#endif
    result[ichain]=linker+" "+exeflags+cxxflags+" -o "+get_methlabel()+" "+get_methlabel()+get_obj_postfix()+" -lc "+libdir+LDFLAGS+" "+odin_libs;
    ichain++;
  }

  if(shared_object) {
    result[ichain]  ="rm -f *"+get_so_postfix()+" so_locations";
    ichain++;

    STD_string soflags;
#ifdef MACOS
    soflags=" -dynamiclib ";  // the MacOS way of shared objects
#else
    soflags=" -shared -Wl,-soname,`cat "+unique_id_file+"`"+get_so_postfix()+" ";
#endif
    soflags+=" -o `cat "+unique_id_file+"`"+get_so_postfix()+" ";
    result[ichain]=linker+soflags+get_methlabel()+get_obj_postfix()+" -lc "+libdir+LDFLAGS+" "+odin_libs;
    ichain++;
  }

#endif

  return result;
}  


STD_string SeqMakefile::get_method_clean() const {
  return  "rm -f unique_id_"+get_methlabel()+" *"+get_methlabel()+"*"+get_so_postfix()+" *"+get_methlabel()+"*"+get_obj_postfix()+" "+get_methlabel()+" odin_parx* "+get_methlabel()+"_sequencePars* odinpls* "+get_methlabel()+".ppg "+get_methlabel()+".r odin_versionInfo "+get_methlabel()+"_description "+get_methlabel()+"_messages";
}

STD_string SeqMakefile::get_method_install(const STD_string& methdir) const {
  return  "./"+get_methlabel()+" write_code -s "+methdir;
}


svector SeqMakefile::get_odin4idea_method_compile_chain(const STD_string& in_dir, const STD_string& odindir,
                                                        const STD_string& vxworks_path, const STD_string& vxworks_cxx, const STD_string& vxworks_flags,
                                                        const STD_string& win_cxx, const STD_string& hostd_flags, const STD_string& host_flags) const {

  STD_string defines=get_methdefines(get_methlabel()+"_main",valid_c_label(get_methlabel()));

/*  This will be done in cxxwrapper
#ifdef USING_WIN32
  // add path for ppc-compiler binaries
  LDRfileName ppc_path(vxworks_path); // normalize file name
  STD_string oldpath=getenv_nonnull("PATH");

  // remove blanks which make ccppc fail
  while(oldpath.find("; ")!=STD_string::npos) oldpath=replaceStr(oldpath,"; ",";");
  while(oldpath.find(" ;")!=STD_string::npos) oldpath=replaceStr(oldpath," ;",";");

  if(oldpath.find(ppc_path) == STD_string::npos) {
    STD_string path="PATH="+oldpath+";"+ppc_path;
    _putenv(path.c_str());
  }
#endif
*/

  LDRfileName methdir(in_dir); // normalize filename

  STD_string method_bname=methdir+SEPARATOR_STR+get_methlabel();
  STD_string method_bname_slash=replaceStr(method_bname,"\\","/");  // use forward slashes for vxworks


  STD_string includes;
  svector result; result.resize(3);

  includes=STD_string(" -I")+odindir+SEPARATOR_STR+"vxworks"+SEPARATOR_STR+"include";
  result[0]=LDRfileName(vxworks_cxx)+" "+method_bname_slash+".cpp -c "+includes+" "+defines+" "+vxworks_flags+" -o "+method_bname_slash+".o"; // srccode must be 1st for ccpentium

  includes=STD_string(" -I")+odindir+SEPARATOR_STR+"host"+SEPARATOR_STR+"include";
  result[1]=LDRfileName(win_cxx)+" "+method_bname+".cpp -c "+includes+" "+defines+" "+host_flags+" -Fo"+method_bname+".obj";

  includes=STD_string(" -I")+odindir+SEPARATOR_STR+"hostd"+SEPARATOR_STR+"include";
  result[2]=LDRfileName(win_cxx)+" "+method_bname+".cpp -c "+includes+" "+defines+" "+hostd_flags+" -Fo"+method_bname+"d.obj";

  return result;
}
#endif




