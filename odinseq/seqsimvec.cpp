#include "seqsimvec.h"


SeqSimultanVector::SeqSimultanVector(const SeqSimultanVector& ssv) {
   SeqSimultanVector::operator = (ssv);
}

SeqSimultanVector& SeqSimultanVector::operator = (const SeqSimultanVector& ssv) {
  SeqVector::operator = (ssv);
  SeqClass::operator = (ssv);
  List<SeqVector, const SeqVector*, const SeqVector&>::operator = (ssv);
  return *this;
}

SeqSimultanVector& SeqSimultanVector::operator += (const SeqVector& sv) {
  Log<Seq> odinlog(this,"+=");
  if(&sv==this) ODINLOG(odinlog,errorLog) << "refusing to manage myself" << STD_endl;
  else {
    append(sv);
    sv.simhandler.set_handled(this);
  }
  return *this;
}



svector SeqSimultanVector::get_vector_commands(const STD_string& iterator) const {
  Log<Seq> odinlog(this,"get_vector_commands");
  STD_list<STD_string> labels;
  STD_list<const SeqVector*>::const_iterator veciter; // using local iterator because member functions may be nested
  for(veciter=get_const_begin();veciter!=get_const_end();++veciter) {
    svector veclabels=(*veciter)->get_vector_commands(iterator);
    for(unsigned int i=0; i<veclabels.size(); i++) labels.push_back(veclabels[i]);
  }
  svector result; result.resize(labels.size());
  unsigned int index=0;
  for(STD_list<STD_string>::const_iterator it=labels.begin(); it!=labels.end(); ++it) {
    result[index]=*it;
    index++;
  }
  return result;
}

unsigned int SeqSimultanVector::get_vectorsize() const {
  Log<Seq> odinlog(this,"get_vectorsize");
  unsigned int result=0;
  STD_list<const SeqVector*>::const_iterator veciter; // using local iterator because member functions may be nested
  if(size()) {
    result=(*get_const_begin())->get_vectorsize();
    for(veciter=get_const_begin();veciter!=get_const_end();++veciter) {
      if((*veciter)->get_vectorsize()!=result) {
        ODINLOG(odinlog,errorLog) << "vector size mismatch" << STD_endl;
      }
    }
  }
  return result;
}


unsigned int SeqSimultanVector::get_numof_iterations() const {
  Log<Seq> odinlog(this,"get_numof_iterations");
  unsigned int result=0;
  STD_list<const SeqVector*>::const_iterator veciter; // using local iterator because member functions may be nested
  if(size()) {
    result=(*get_const_begin())->get_numof_iterations();
    for(veciter=get_const_begin();veciter!=get_const_end();++veciter) {
      if((*veciter)->get_numof_iterations()!=result) {
        ODINLOG(odinlog,errorLog) << "numof_iterations mismatch" << STD_endl;
      }
    }
  }
  return result;
}



STD_string SeqSimultanVector::get_loopcommand() const {
  Log<Seq> odinlog(this,"get_loopcommand");
  STD_string result;
  STD_list<const SeqVector*>::const_iterator veciter; // using local iterator because member functions may be nested
  if(size()) {
    result=(*get_const_begin())->get_loopcommand();
    for(veciter=get_const_begin();veciter!=get_const_end();++veciter) {
      if((*veciter)->get_loopcommand()!=result) {
        ODINLOG(odinlog,errorLog) << "loopcommand mismatch" << STD_endl;
      }
    }
  }
  return result;
}

nestingRelation SeqSimultanVector::get_nesting_relation() const {
  Log<Seq> odinlog(this,"get_nesting_relation");
  nestingRelation result=noRelation;
  STD_list<const SeqVector*>::const_iterator veciter; // using local iterator because member functions may be nested
  if(size()) {
    result=(*get_const_begin())->get_nesting_relation();
    for(veciter=get_const_begin();veciter!=get_const_end();++veciter) {
      if((*veciter)->get_nesting_relation()!=result) {
        ODINLOG(odinlog,errorLog) << "nesting_relation mismatch" << STD_endl;
      }
    }
  }
  return result;
}


bool SeqSimultanVector::needs_unrolling_check() const {
  Log<Seq> odinlog(this,"needs_unrolling_check");
  bool result=false;
  STD_list<const SeqVector*>::const_iterator veciter; // using local iterator because member functions may be nested
  for(veciter=get_const_begin();veciter!=get_const_end();++veciter) {
    if((*veciter)->needs_unrolling_check()) result=true;
  }
  return result;
}


bool SeqSimultanVector::prep_iteration() const {
  Log<Seq> odinlog(this,"prep_iteration");
  bool result=true;
  ODINLOG(odinlog,normalDebug) << "managing " << size() << " vectors" << STD_endl;
  STD_list<const SeqVector*>::const_iterator veciter; // using local iterator because member functions may be nested
  for(veciter=get_const_begin(); veciter!=get_const_end(); ++veciter) {
    ODINLOG(odinlog,normalDebug) << "prep_iteration of " << (*veciter)->get_label() << STD_endl;
    if(!(*veciter)->prep_iteration()) {
      ODINLOG(odinlog,errorLog) << (*veciter)->get_label() << ".prep_iteration() failed" << STD_endl;
      return false;
    }
  }
  return result;
}


/*
bool SeqSimultanVector::is_acq_vector() const {
  Log<Seq> odinlog(this,"SeqSimultanVector::is_acq_vector");
  return SeqVector::is_acq_vector();
}
*/

const SeqVector& SeqSimultanVector::set_vechandler(const SeqCounter* sc) const {
  Log<Seq> odinlog(this,"set_vechandler");
  SeqVector::set_vechandler(sc);
  STD_list<const SeqVector*>::const_iterator veciter; // using local iterator because member functions may be nested
  for(veciter=get_const_begin();veciter!=get_const_end();++veciter) {
    (*veciter)->set_vechandler(sc);
  }
  return *this;
}

bool SeqSimultanVector::is_qualvector() const {
  Log<Seq> odinlog(this,"is_qualvector");
  STD_list<const SeqVector*>::const_iterator veciter; // using local iterator because member functions may be nested
  for(veciter=get_const_begin();veciter!=get_const_end();++veciter) {
    if((*veciter)->is_qualvector()) return true;
  }
  return false;
}

void SeqSimultanVector::clear_container() {
  List<SeqVector, const SeqVector*, const SeqVector&>::clear();
}

