/***************************************************************************
                          seqacqread.h  -  description
                             -------------------
    begin                : Tue Aug 13 2002
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQACQREAD_H
#define SEQACQREAD_H

#include <odinseq/seqacq.h>
#include <odinseq/seqdelay.h>
#include <odinseq/seqgradconst.h>
#include <odinseq/seqgradtrapez.h>
#include <odinseq/seqparallel.h>

/**
  * @addtogroup odinseq
  * @{
  */

/**
  * \brief  Acquisition + readout gradient
  *
  * This class represents an acquisition window with a read gradient.
  */
class SeqAcqRead : public virtual SeqAcqInterface, public SeqParallel  {

  // virtual functions of SeqFreqChanInterface are marhshalled to acq

 public:

/**
  * Constructs an acquisition window with a simultaneous read gradient labeled 'object_label' and the
  * following properties:
  * - sweepwidth:  The sampling frequency
  * - read_size:   The desired matrix size in read direction
  * - fov:         The Field of View
  * - gradchannel: The channel where the read gradient will be placed
  * - os_factor:   See class 'SeqAcq' for explanation
  * - partial_fourier: The amount of partial Fourier undersampling (0=no undersampling, 1=half fourier)
  * - partial_fourier_at_end: If set to 'true', partial Fourier will omit data at the end of the readout
  * - nucleus:     See class 'SeqAcq' for explanation
  * - phaselist:   See class 'SeqAcq' for explanation
  * - freqlist:    See class 'SeqAcq' for explanation
  * - timestep:    The time resolution for the digitised ramps, the default is to
  *                take minimum time step the scanner is capable of.
  * - rampmode:    This parameter determines the shape of the ramp waveform
  */
  SeqAcqRead(const STD_string& object_label,double sweepwidth,unsigned int read_size,
              float fov, direction gradchannel, float os_factor=1.0,
              float partial_fourier=0.0, bool partial_fourier_at_end=false,
              const STD_string& nucleus="", const dvector& phaselist=0, const dvector& freqlist=0,
              float timestep = 0.0, rampType rampmode = linear);

/**
  * Constructs an acquisition window with read gradient which is a copy of 'sar'
  */
  SeqAcqRead(const SeqAcqRead& sar);

/**
  * Constructs an empty acquisition window with read gradient with the given label.
  */
  SeqAcqRead(const STD_string& object_label="unnamedSeqAcqRead");

/**
  * This assignment operator will make this object become an exact copy of 'sar'.
  */
  SeqAcqRead& operator = (const SeqAcqRead& sar);

/**
  * Returns reference to read gradient.
  */
  const SeqGradTrapez& get_readgrad() const {return read;}


  // marshalling virtual functions of SeqAcqInterface to acq, except:
  double get_acquisition_center() const;
  double get_acquisition_start() const;
  SeqAcqInterface& set_sweepwidth(double sw, float os_factor); // produce warning



 private:
  friend class SeqGradEcho;

  // overloaded for SeqAcqDeph
  const SeqVector* get_dephgrad(SeqGradChanParallel& dephobj, bool rephase) const;

  void build_seq();

  void common_init();

  float corrected_partfour; // helper during construction

  SeqAcq acq;
  SeqGradTrapez read;
  SeqDelay middelay;
  SeqGradDelay midgrad;
  SeqDelay tozero;

  SeqGradTrapez readdephgrad;
  SeqGradTrapez readrephgrad;

};

/** @}
  */



#endif
