/***************************************************************************
                          seqcmdline.h  -  description
                             -------------------
    begin                : Tue Jan 18 2005
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQCMDLINE_H
#define SEQCMDLINE_H

#include <odinseq/seqclass.h>

#define USAGE_INDENTION_FACTOR 2


/////////////////////////////////////////////////////////


#ifdef NO_CMDLINE
struct SeqCmdlineActionList {}; // dummy class
#else
struct SeqCmdlineAction {
  SeqCmdlineAction(const STD_string& act, const STD_string& descr) : action(act), description(descr) {}
  void add_req_arg(const STD_string& opt, const STD_string& descr) {req_args[opt]=descr;}
  void add_opt_arg(const STD_string& opt, const STD_string& descr) {opt_args[opt]=descr;}
  STD_string action;
  STD_string description;
  STD_map<STD_string,STD_string> req_args;
  STD_map<STD_string,STD_string> opt_args;
};

typedef STD_list<SeqCmdlineAction> SeqCmdlineActionList;
#endif


/////////////////////////////////////////////////////////


/**
  * @ingroup odinseq_internals
  *  Helper class to deal with command line arguments
  */
class SeqCmdLine : public SeqClass {

 public:
  static int process(int argc, char *argv[]);

  static STD_string format_actions(const SeqCmdlineActionList& actions);

 private:
  static STD_string usage(const STD_string& meth, const STD_string& description);

};


#endif
