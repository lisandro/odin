/***************************************************************************
                          seqtemplate.h  -  description
                             -------------------
    begin                : Mon Tue 23 2005
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQTEMPLATE_H
#define SEQTEMPLATE_H

#include <odinseq/seqlist.h>

/**
  * @addtogroup odinseq
  * @{
  */


struct SeqFieldMapPars; // forward declaration
struct SeqFieldMapObjects; // forward declaration

/**
  * \brief Multi-echo module for field-map pre-scan
  *
  * In order to acquire a field-map together with the actual scan, this
  * sequence module employs an EPI-like readout where each shot acquires
  * an even number of alternating echoes (each with a different TE)
  * separately for each line in k-space.
  *
  */
class SeqFieldMap : public SeqObjList {

  // Disable copying
  SeqFieldMap(const SeqFieldMap& sfm) {}
  SeqFieldMap& operator = (const SeqFieldMap& sfm) {return *this;}


  void alloc_data(const STD_string& objlabel);

  SeqFieldMapPars* pars; 
  SeqFieldMapObjects* objs;


 public:


/**
  * Constructs a field-map scan
  */
  SeqFieldMap() : pars(0), objs(0) {}


  ~SeqFieldMap();



/**
  * Initialize field-map scan with default values, sequence objects are prefixed by 'objlabel'.
  * This function is usually called in the sequence initialization step.
  */
  void init(const STD_string& objlabel);


/**
  * Builds the field-map scan using bandwidth 'sweepwidth' with oversampling factor 'os_factor', an extra preparation 'prep' prior to each excitation and minimum relaxation delay 'min_relaxdelay'.
  * This function is usually called in the sequence building step.
  */
  void build_seq(double sweepwidth, float os_factor=1.0, const SeqObjList& prep=SeqObjList(), double min_relaxdelay=0.0);


/**
  * Returns block of local parameters
  */
  LDRblock& get_parblock();

};


/** @}
  */



#endif
