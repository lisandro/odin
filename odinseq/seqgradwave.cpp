#include "seqgradwave.h"

SeqGradWave::SeqGradWave(const STD_string& object_label,direction gradchannel, double gradduration,
	         float maxgradstrength,const fvector& waveform)
             : SeqGradChan(object_label,gradchannel,maxgradstrength,gradduration) {
  set_wave(waveform);
}

SeqGradWave::SeqGradWave(const SeqGradWave& sgw) {
  SeqGradWave::operator = (sgw);
}

SeqGradWave::SeqGradWave(const STD_string& object_label)
             : SeqGradChan(object_label)  {
}


SeqGradWave& SeqGradWave::set_wave(const fvector& waveform) {
  Log<Seq> odinlog(this,"set_wave");
  wave=waveform;
  ODINLOG(odinlog,normalDebug) << "wave(" << wave.length() << ")" << STD_endl;
  return *this;
}


float SeqGradWave::get_integral(double tmin, double tmax) const {
  Log<Seq> odinlog(this,"get_integral");
  double dur=get_gradduration();
  ODINLOG(odinlog,normalDebug) << "dur=" << dur << STD_endl;

  if(tmin<0.0) tmin=0.0; if(tmin>dur) tmin=dur;
  if(tmax<0.0) tmax=0.0; if(tmax>dur) tmax=dur;
  ODINLOG(odinlog,normalDebug) << "dur/tmin/tmax=" << dur << "/" << tmin << "/" << tmax << STD_endl;

  double len=double(wave.length());
  unsigned int startindex=(unsigned int)(secureDivision(tmin,dur)*len+0.5);
  unsigned int endindex  =(unsigned int)(secureDivision(tmax,dur)*len+0.5);
  ODINLOG(odinlog,normalDebug) << "len/startindex/endindex=" << len << "/" << startindex << "/" << endindex << STD_endl;

  float integral=wave.range(startindex,endindex).sum();
  ODINLOG(odinlog,normalDebug) << "sum=" << integral << STD_endl;
  integral= secureDivision(integral * get_strength() * dur,len);
  ODINLOG(odinlog,normalDebug) << "integral=" << integral << STD_endl;
  return integral;
}



unsigned int SeqGradWave::get_npts() const {
  return wave.length();
}

SeqGradWave& SeqGradWave::operator = (const SeqGradWave& sgw) {
  Log<Seq> odinlog(this,"operator=");
  SeqGradChan::operator = (sgw);
  wave=sgw.wave;
  ODINLOG(odinlog,normalDebug) << "wave(" << wave.length() << ")" << STD_endl;
  return *this;
}

STD_string SeqGradWave::get_grdpart(float matrixfactor) const {
  return graddriver->get_wave_program(get_strength(),matrixfactor);
}

bool SeqGradWave::prep() {
  Log<Seq> odinlog(this,"prep");
  if(!SeqGradChan::prep()) return false;
  check_wave();
  return graddriver->prep_wave(get_strength(),get_grdfactors_norot(),get_gradduration(),wave);
}

SeqGradChan& SeqGradWave::get_subchan(double starttime, double endtime) const {
  Log<Seq> odinlog(this,"get_subchan");


  double startindex_round=starttime/get_gradduration()*double(wave.length())*1000.0+0.5;
  double endindex_round=    endtime/get_gradduration()*double(wave.length())*1000.0+0.5;

  unsigned int startindex=(unsigned int)(startindex_round)/1000;
  unsigned int endindex  =(unsigned int)(endindex_round)/1000;

  ODINLOG(odinlog,normalDebug) << "startindex/endindex=" <<  startindex << "/" << endindex << STD_endl;

  LDRfloatArr subwave(wave.range(startindex,endindex));
  if(!subwave.length()) {
    subwave.resize(1);
    if(wave.length()>startindex) subwave[0]=wave[startindex];
  }

  ODINLOG(odinlog,normalDebug) << "starttime/endtime/length=" <<  starttime << "/" << endtime << "/" << subwave.length()  << STD_endl;

  SeqGradWave* sgw= new SeqGradWave(STD_string(get_label())+"_("+ftos(starttime)+"-"+ftos(endtime)+")",
                                        get_channel(),endtime-starttime,get_strength(),subwave);

  sgw->prep(); // we have to call prep() manually here because prep_all() has already been executed

  sgw->set_temporary();
  return *sgw;
}


int SeqGradWave::get_wavesize() const {
  Log<Seq> odinlog(this,"get_wavesize");
  ODINLOG(odinlog,normalDebug) << "wavesize=" << wave.length() << STD_endl;
  return wave.length();
}

void SeqGradWave::resize(unsigned int newsize) {
  Log<Seq> odinlog(this,"resize");
  ODINLOG(odinlog,normalDebug) << "to " << newsize << STD_endl;
  wave.interpolate(newsize);
  check_wave();
  graddriver->update_wave(wave);
}

void SeqGradWave::check_wave() {
  Log<Seq> odinlog(this,"check_wave");
  float max=0.0;
  for(unsigned int i=0; i<wave.length(); i++) {
    if(wave[i]> 1.0) {if(fabs(wave[i])>max) max=fabs(wave[i]); wave[i]= 1.0;}
    if(wave[i]<-1.0) {if(fabs(wave[i])>max) max=fabs(wave[i]); wave[i]=-1.0;}
  }
  if(max>0.0) {
    ODINLOG(odinlog,warningLog) << "Corrected SeqGradWave value of " << max << " to stay within [-1,1] limits" << STD_endl;
  }
}


