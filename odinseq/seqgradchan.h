/***************************************************************************
                          seqgradchan.h  -  description
                             -------------------
    begin                : Mon Aug 19 2002
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQGRADCHAN_H
#define SEQGRADCHAN_H


#include <odinseq/seqgrad.h>
#include <odinseq/seqdur.h>
#include <odinseq/seqdriver.h>
#include <odinseq/seqvec.h>


#define _GRADROTMATRIX_LIMIT_  1.0e-5


/**
  * @addtogroup odinseq_internals
  * @{
  */


/**
  * The base class for platform specific drivers of gradient channel objects
  */
class SeqGradChanDriver : public SeqDriverBase {

 public:
  SeqGradChanDriver() {}
  virtual ~SeqGradChanDriver() {}

  virtual STD_string get_const_program   (float strength, float matrixfactor) const = 0;
  virtual STD_string get_onepoint_program(float strength, float matrixfactor) const = 0;
  virtual STD_string get_delay_program   (float strength, float matrixfactor) const = 0;
  virtual STD_string get_wave_program    (float strength, float matrixfactor) const = 0;
  virtual STD_string get_trapez_program  (float strength, float matrixfactor) const = 0;
  virtual STD_string get_vector_program  (float strength, float matrixfactor, int reordercount) const = 0;

  virtual svector get_vector_commands(const STD_string& iterator) const = 0;
  virtual svector get_reord_commands() const = 0;

  virtual bool prep_const(float strength, const fvector& strengthfactor, double gradduration) = 0;

  virtual bool prep_onepoint(float strength, const fvector& strengthfactor, double gradduration) = 0;

  virtual bool prep_wave(float strength, const fvector& strengthfactor, double gradduration, const fvector& wave) = 0;
  virtual void update_wave(const fvector& wave) = 0;

  virtual bool prep_vector(float strength, const fvector& strengthfactor, double gradduration,
                           const fvector& gradvec, const iarray& index_matrix, nestingRelation nr) = 0;
  virtual bool prep_vector_iteration(unsigned int count) = 0;

  virtual bool prep_trapez(float strength, const fvector& strengthfactor, double ruptime, const fvector& rupshape, double consttime, double rdowntime, const fvector& rdownshape) = 0;


  virtual void event(eventContext& context, double starttime) const = 0;

  virtual float check_strength(float strength) const = 0;

  virtual SeqGradChanDriver* clone_driver() const = 0;
};


///////////////////////////////////////////////////////////////////////////////


/**
  * This is the base class for all objects that represent a single gradient object
  */
class SeqGradChan : public virtual SeqGradInterface, public ListItem<SeqGradChan>, public SeqDur {

 public:
/**
  * Constructs a gradient channel object labeled 'object_label' with the following properties:
  * - gradchannel:   The channel this object should be played out
  * - gradstrength:  The gradient strength for this object
  * - gradduration:  The duration of this gradient object
  */
  SeqGradChan(const STD_string& object_label,direction gradchannel,
                float gradstrength, double gradduration);

/**
  * Construct an empty gradient channel object with the given label
  */
  SeqGradChan(const STD_string& object_label = "unnamedSeqGradChan" );

/**
  * Constructs a copy of 'sgc'
  */
  SeqGradChan(const SeqGradChan& sgc);

/**
  * Destructor
  */
  virtual ~SeqGradChan() {}

/**
  * Assignment operator that makes this gradient channel object become a copy of 'sgc'
  */
  SeqGradChan& operator = (const SeqGradChan& sgc);


/**
  * Returns the occupied channel
  */
  virtual direction get_channel() const;

/**
  * Overload this function to return the size of the used waveform, if any
  */
  virtual int get_wavesize() const {return 0;}

/**
  * Overload this function to resize the current waveform, if any
  */
  virtual void resize(unsigned int newsize) {}




  // overwriting virtual functions from SeqGradInterface
  SeqGradInterface& set_strength(float gradstrength);
  SeqGradInterface& invert_strength();
  float get_strength() const;
  double get_gradduration() const {return get_duration();}
  SeqGradInterface& set_gradrotmatrix(const RotMatrix& matrix);
  fvector get_gradintegral() const;

  // overwriting virtual functions from SeqTreeObj
  STD_string get_properties() const;
  unsigned int event(eventContext& context) const;


  STD_string get_grdpart_rot(direction chan) const;

 protected:
 
/**
  * returns element [channel][chan]  val. from the current rot. Matrix
  */
  float get_grdfactor(direction chan) const;

/**
  * returns element [channel][chan] val. from the current rot. Matrix without considering
  * the rotation matrix of the current event block (for IDEA)
  */
  fvector get_grdfactors_norot() const;


  mutable SeqDriverInterface<SeqGradChanDriver> graddriver;

 private:
  friend class SeqGradChanList;
  friend class SeqGradChanParallel;

/**
  * Overload this function to return a cut-out of this object in the interval [starttime,endtime]
  */
  virtual SeqGradChan& get_subchan(double starttime, double endtime) const = 0;


/**
  * Overload this function to return this objects part in the (gradient) program
  */
  virtual STD_string get_grdpart(float matrixfactor) const = 0;

/**
  * Overload this function to returns this objects gradient integral
  */
  virtual float get_integral() const = 0;


  RotMatrix get_total_rotmat() const;


  float strength;
  direction channel; // Do not change this once it it is initialised
  RotMatrix gradrotmatrix;
};


/** @}
  */


#endif
