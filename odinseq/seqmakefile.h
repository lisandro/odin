/***************************************************************************
                          seqmakefile.h  -  description
                             -------------------
    begin                : Wed Apr 14 2004
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQMAKEFILE_H
#define SEQMAKEFILE_H


#include <odinseq/seqclass.h>

#ifndef NO_CMDLINE

/**
  * @ingroup odinseq_internals
  *  Helper class to generate Makefiles for different platforms
  */
class SeqMakefile : public SeqClass {

public:
  SeqMakefile(const STD_string& methlabel, const STD_string& odin_install_prefix="",
              const STD_string& compiler="", const STD_string& compiler_flags=CXXFLAGS, const STD_string& linker="",
              const STD_string& extra_includes="", const STD_string& extra_libs="");

  STD_string get_Makefile(const STD_string& methroot) const;

  svector get_method_compile_chain(bool executable, bool shared_object) const;

  STD_string get_method_clean() const;

  STD_string get_method_install(const STD_string& methdir) const;

  svector get_odin4idea_method_compile_chain(const STD_string& in_dir, const STD_string& odindir,
                                             const STD_string& vxworks_path, const STD_string& vxworks_cxx, const STD_string& vxworks_flags,
                                             const STD_string& win_cxx, const STD_string& hostd_flags, const STD_string& host_flags) const;

  static STD_string get_exe_postfix();
  static STD_string get_so_postfix();
  static STD_string get_obj_postfix();

private:
  STD_string get_methlabel() const {return get_label();}
  STD_string get_methdefines(const STD_string& main, const STD_string& classlabel) const;

  LDRfileName inst_prefix;
  STD_string cxx;
  STD_string cxxflags;
  STD_string ld;
  STD_string add_includes;
  STD_string add_libs;
};

#endif



#endif
