/***************************************************************************
                          seqacqdeph.h  -  description
                             -------------------
    begin                : Tue Aug 13 2002
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQACQDEPH_H
#define SEQACQDEPH_H

#include <odinseq/seqgradchanparallel.h>
#include <odinseq/seqgradtrapez.h>


class SeqAcqInterface; // forward declaration


/**
  * @addtogroup odinseq
  * @{
  */


/**
  * This enum is used to specify the type of the pre-dephasing or post-rephasing gradient:
  * - FID: It will have opposite polarity
  * - spinEcho: It will have the same polarity
  * - rephase: Gradient pulse to rephase acquisition gradients
  */
enum dephaseMode {FID,spinEcho,rephase};



//////////////////////////////////////////////////////////////////////////

/**
  * \brief  Pre-read dephasing
  *
  * This class provides a pre-dephasing or post-rephasing gradient pulse for acquisition objects
  */
class SeqAcqDeph : public SeqGradChanParallel {

 public:

/**
  * Constructs a read dephase gradient pulse labeled 'object_label' with the following properties:
  * - acq:       The acquisition object for which the dephasing should be performed
  * - polarity:  Specifies whether the gradient will have balanced (FID) or
  *              inverted polarity (spinEcho).
  */
  SeqAcqDeph(const STD_string& object_label,const SeqAcqInterface& acq, dephaseMode mode = FID);

/**
  * Constructs a copy of 'sad'
  */
  SeqAcqDeph(const SeqAcqDeph& sad);

/**
  * Construct an empty read dephase gradient pulse with the given label
  */
   SeqAcqDeph(const STD_string& object_label = "unnamedSeqAcqDeph");

/**
  * Assignment operator that makes this object become a copy of 'sad'
  */
  SeqAcqDeph& operator = (const SeqAcqDeph& sad);

/**
  * returns vector to iterate through different segments in multi-shot EPI
  */
  const SeqVector& get_epi_segment_vector() const;

/**
  * returns vector to iterate through different GRAPPA interleaves in parallel-imaging EPI
  */
  const SeqVector& get_epi_reduction_vector() const;


 private:
  void common_init();

  SeqVector dummyvec;
  Handler<const SeqVector*> segvec;


};


/** @}
  */

#endif
