/***************************************************************************
                          seqoperator.h  -  description
                             -------------------
    begin                : Mon Apr 19 2004
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQOPERATOR_H
#define SEQOPERATOR_H

#include <odinseq/seqclass.h>


// forward declarations
class SeqObjBase;
class SeqGradObjInterface;
class SeqObjList;
class SeqObjLoop;
class SeqDecoupling;
class SeqParallel;
class SeqGradChan;
class SeqGradChanList;
class SeqGradChanParallel;

/**
  * @ingroup odinseq_internals
  * A class for connecting sequence objects via operators.
  */
class SeqOperator : public SeqClass {

 public:
  static SeqObjList& concat(const SeqObjBase& s1, const SeqObjBase& s2);
  static SeqObjList& concat(const SeqObjBase& s1, const SeqObjList& s2, bool swap);
  static SeqObjList& concat(const SeqObjBase& s1, const SeqObjLoop& s2, bool swap);
  static SeqObjList& concat(const SeqObjBase& s1, const SeqDecoupling& s2, bool swap);
  static SeqObjList& concat(const SeqObjBase& s1, const SeqParallel& s2, bool swap);
  static SeqObjList& concat(const SeqObjBase& s1, SeqGradChan& s2, bool swap);
  static SeqObjList& concat(const SeqObjBase& s1, SeqGradChanList& s2, bool swap);
  static SeqObjList& concat(const SeqObjBase& s1, SeqGradChanParallel& s2, bool swap);

  static SeqObjList& concat(const SeqObjList& s1, const SeqObjList& s2);
  static SeqObjList& concat(const SeqObjList& s1, const SeqObjLoop& s2, bool swap);
  static SeqObjList& concat(const SeqObjList& s1, const SeqDecoupling& s2, bool swap);
  static SeqObjList& concat(const SeqObjList& s1, const SeqParallel& s2, bool swap);
  static SeqObjList& concat(const SeqObjList& s1, SeqGradChan& s2, bool swap);
  static SeqObjList& concat(const SeqObjList& s1, SeqGradChanList& s2, bool swap);
  static SeqObjList& concat(const SeqObjList& s1, SeqGradChanParallel& s2, bool swap);

  static SeqObjList& concat(const SeqObjLoop& s1, const SeqObjLoop& s2);
  static SeqObjList& concat(const SeqObjLoop& s1, const SeqDecoupling& s2, bool swap);
  static SeqObjList& concat(const SeqObjLoop& s1, const SeqParallel& s2, bool swap);
  static SeqObjList& concat(const SeqObjLoop& s1, SeqGradChan& s2, bool swap);
  static SeqObjList& concat(const SeqObjLoop& s1, SeqGradChanList& s2, bool swap);
  static SeqObjList& concat(const SeqObjLoop& s1, SeqGradChanParallel& s2, bool swap);

  static SeqObjList& concat(const SeqDecoupling& s1, const SeqDecoupling& s2);
  static SeqObjList& concat(const SeqDecoupling& s1, const SeqParallel& s2, bool swap);
  static SeqObjList& concat(const SeqDecoupling& s1, SeqGradChan& s2, bool swap);
  static SeqObjList& concat(const SeqDecoupling& s1, SeqGradChanList& s2, bool swap);
  static SeqObjList& concat(const SeqDecoupling& s1, SeqGradChanParallel& s2, bool swap);

  static SeqObjList& concat(const SeqParallel& s1, const SeqParallel& s2);
  static SeqObjList& concat(const SeqParallel& s1, SeqGradChan& s2, bool swap);
  static SeqObjList& concat(const SeqParallel& s1, SeqGradChanList& s2, bool swap);
  static SeqObjList& concat(const SeqParallel& s1, SeqGradChanParallel& s2, bool swap);

  static SeqGradChanList& concat(SeqGradChan& s1, SeqGradChan& s2);
  static SeqGradChanList& concat(SeqGradChan& s1, SeqGradChanList& s2, bool swap);
  static SeqGradChanParallel& concat(SeqGradChan& s1, SeqGradChanParallel& s2, bool swap);

  static SeqGradChanList& concat(SeqGradChanList& s1, SeqGradChanList& s2);
  static SeqGradChanParallel& concat(SeqGradChanList& s1, SeqGradChanParallel& s2, bool swap);

  static SeqGradChanParallel& concat(SeqGradChanParallel& s1, SeqGradChanParallel& s2);



  static SeqParallel& simultan(const SeqObjBase& s1, SeqGradObjInterface& s2);
  static SeqParallel& simultan(const SeqObjBase& s1, SeqGradChan& s2);
  static SeqParallel& simultan(const SeqObjBase& s1, SeqGradChanList& s2);


  static SeqGradChanParallel& simultan(SeqGradChan& s1, SeqGradChan& s2);
  static SeqGradChanParallel& simultan(SeqGradChan& s1, SeqGradChanList& s2);
  static SeqGradChanParallel& simultan(SeqGradChan& s1, SeqGradChanParallel& s2);

  static SeqGradChanParallel& simultan(SeqGradChanList& s1, SeqGradChanList& s2);
  static SeqGradChanParallel& simultan(SeqGradChanList& s1, SeqGradChanParallel& s2);

  static SeqGradChanParallel& simultan(SeqGradChanParallel& s1, SeqGradChanParallel& s2);

 private:
  static void append_list2list(SeqObjList& dst, const SeqObjList& src);

  static SeqObjList& create_SeqObjList_label(const STD_string& label1, const STD_string& label2, bool swap);
  static SeqObjList& create_SeqObjList_obj(const SeqObjBase& s1, const SeqObjBase& s2, bool swap);
  static SeqObjList& create_SeqObjList_list(const SeqObjList& s1, const SeqObjBase& s2, bool swap);

  static SeqGradChanList& create_SeqGradChanList(const STD_string& label1, const STD_string& label2, bool swap);
  static SeqGradChanList& create_SeqGradChanList(SeqGradChan& sgc);
  static SeqGradChanParallel& create_SeqGradChanParallel_concat(const STD_string& label1, const STD_string& label2, bool swap);

  static SeqParallel& create_SeqParallel(const STD_string& label1, const STD_string& label2);
  static SeqGradChanParallel& create_SeqGradChanParallel_simultan(const STD_string& label1, const STD_string& label2);

};

inline SeqObjList& operator + (const SeqObjBase& s1, const SeqObjBase& s2){return SeqOperator::concat(s1,s2);}
inline SeqObjList& operator + (const SeqObjBase& s1, const SeqObjList& s2)    {return SeqOperator::concat(s1,s2,false);}
inline SeqObjList& operator + (const SeqObjBase& s1, const SeqObjLoop& s2)    {return SeqOperator::concat(s1,s2,false);}
inline SeqObjList& operator + (const SeqObjBase& s1, const SeqDecoupling& s2) {return SeqOperator::concat(s1,s2,false);}
inline SeqObjList& operator + (const SeqObjBase& s1, const SeqParallel& s2)   {return SeqOperator::concat(s1,s2,false);}
inline SeqObjList& operator + (const SeqObjBase& s1, SeqGradChan& s2)         {return SeqOperator::concat(s1,s2,false);}
inline SeqObjList& operator + (const SeqObjBase& s1, SeqGradChanList& s2)     {return SeqOperator::concat(s1,s2,false);}
inline SeqObjList& operator + (const SeqObjBase& s1, SeqGradChanParallel& s2) {return SeqOperator::concat(s1,s2,false);}

inline SeqObjList& operator + (const SeqObjList& s1, const SeqObjBase& s2){return SeqOperator::concat(s2,s1,true);}
inline SeqObjList& operator + (const SeqObjList& s1, const SeqObjList& s2)    {return SeqOperator::concat(s1,s2);}
inline SeqObjList& operator + (const SeqObjList& s1, const SeqObjLoop& s2)    {return SeqOperator::concat(s1,s2,false);}
inline SeqObjList& operator + (const SeqObjList& s1, const SeqDecoupling& s2) {return SeqOperator::concat(s1,s2,false);}
inline SeqObjList& operator + (const SeqObjList& s1, const SeqParallel& s2)   {return SeqOperator::concat(s1,s2,false);}
inline SeqObjList& operator + (const SeqObjList& s1, SeqGradChan& s2)         {return SeqOperator::concat(s1,s2,false);}
inline SeqObjList& operator + (const SeqObjList& s1, SeqGradChanList& s2)     {return SeqOperator::concat(s1,s2,false);}
inline SeqObjList& operator + (const SeqObjList& s1, SeqGradChanParallel& s2) {return SeqOperator::concat(s1,s2,false);}

inline SeqObjList& operator + (const SeqObjLoop& s1, const SeqObjBase& s2){return SeqOperator::concat(s2,s1,true);}
inline SeqObjList& operator + (const SeqObjLoop& s1, const SeqObjList& s2)    {return SeqOperator::concat(s2,s1,true);}
inline SeqObjList& operator + (const SeqObjLoop& s1, const SeqObjLoop& s2)    {return SeqOperator::concat(s1,s2);}
inline SeqObjList& operator + (const SeqObjLoop& s1, const SeqDecoupling& s2) {return SeqOperator::concat(s1,s2,false);}
inline SeqObjList& operator + (const SeqObjLoop& s1, const SeqParallel& s2)   {return SeqOperator::concat(s1,s2,false);}
inline SeqObjList& operator + (const SeqObjLoop& s1, SeqGradChan& s2)         {return SeqOperator::concat(s1,s2,false);}
inline SeqObjList& operator + (const SeqObjLoop& s1, SeqGradChanList& s2)     {return SeqOperator::concat(s1,s2,false);}
inline SeqObjList& operator + (const SeqObjLoop& s1, SeqGradChanParallel& s2) {return SeqOperator::concat(s1,s2,false);}

inline SeqObjList& operator + (const SeqDecoupling& s1, const SeqObjBase& s2){return SeqOperator::concat(s2,s1,true);}
inline SeqObjList& operator + (const SeqDecoupling& s1, const SeqObjList& s2)    {return SeqOperator::concat(s2,s1,true);}
inline SeqObjList& operator + (const SeqDecoupling& s1, const SeqObjLoop& s2)    {return SeqOperator::concat(s2,s1,true);}
inline SeqObjList& operator + (const SeqDecoupling& s1, const SeqDecoupling& s2) {return SeqOperator::concat(s1,s2);}
inline SeqObjList& operator + (const SeqDecoupling& s1, const SeqParallel& s2)   {return SeqOperator::concat(s1,s2,false);}
inline SeqObjList& operator + (const SeqDecoupling& s1, SeqGradChan& s2)         {return SeqOperator::concat(s1,s2,false);}
inline SeqObjList& operator + (const SeqDecoupling& s1, SeqGradChanList& s2)     {return SeqOperator::concat(s1,s2,false);}
inline SeqObjList& operator + (const SeqDecoupling& s1, SeqGradChanParallel& s2) {return SeqOperator::concat(s1,s2,false);}

inline SeqObjList& operator + (const SeqParallel& s1, const SeqObjBase& s2){return SeqOperator::concat(s2,s1,true);}
inline SeqObjList& operator + (const SeqParallel& s1, const SeqObjList& s2)    {return SeqOperator::concat(s2,s1,true);}
inline SeqObjList& operator + (const SeqParallel& s1, const SeqObjLoop& s2)    {return SeqOperator::concat(s2,s1,true);}
inline SeqObjList& operator + (const SeqParallel& s1, const SeqDecoupling& s2) {return SeqOperator::concat(s2,s1,true);}
inline SeqObjList& operator + (const SeqParallel& s1, const SeqParallel& s2)   {return SeqOperator::concat(s1,s2);}
inline SeqObjList& operator + (const SeqParallel& s1, SeqGradChan& s2)         {return SeqOperator::concat(s1,s2,false);}
inline SeqObjList& operator + (const SeqParallel& s1, SeqGradChanList& s2)     {return SeqOperator::concat(s1,s2,false);}
inline SeqObjList& operator + (const SeqParallel& s1, SeqGradChanParallel& s2) {return SeqOperator::concat(s1,s2,false);}

inline SeqObjList&          operator + (SeqGradChan& s1, const SeqObjBase& s2){return SeqOperator::concat(s2,s1,true);}
inline SeqObjList&          operator + (SeqGradChan& s1, const SeqObjList& s2)    {return SeqOperator::concat(s2,s1,true);}
inline SeqObjList&          operator + (SeqGradChan& s1, const SeqObjLoop& s2)    {return SeqOperator::concat(s2,s1,true);}
inline SeqObjList&          operator + (SeqGradChan& s1, const SeqDecoupling& s2) {return SeqOperator::concat(s2,s1,true);}
inline SeqObjList&          operator + (SeqGradChan& s1, const SeqParallel& s2)   {return SeqOperator::concat(s2,s1,true);}
inline SeqGradChanList&     operator + (SeqGradChan& s1, SeqGradChan& s2)         {return SeqOperator::concat(s1,s2);}
inline SeqGradChanList&     operator + (SeqGradChan& s1, SeqGradChanList& s2)     {return SeqOperator::concat(s1,s2,false);}
inline SeqGradChanParallel& operator + (SeqGradChan& s1, SeqGradChanParallel& s2) {return SeqOperator::concat(s1,s2,false);}

inline SeqObjList&          operator + (SeqGradChanList& s1, const SeqObjBase& s2){return SeqOperator::concat(s2,s1,true);}
inline SeqObjList&          operator + (SeqGradChanList& s1, const SeqObjList& s2)    {return SeqOperator::concat(s2,s1,true);}
inline SeqObjList&          operator + (SeqGradChanList& s1, const SeqObjLoop& s2)    {return SeqOperator::concat(s2,s1,true);}
inline SeqObjList&          operator + (SeqGradChanList& s1, const SeqDecoupling& s2) {return SeqOperator::concat(s2,s1,true);}
inline SeqObjList&          operator + (SeqGradChanList& s1, const SeqParallel& s2)   {return SeqOperator::concat(s2,s1,true);}
inline SeqGradChanList&     operator + (SeqGradChanList& s1, SeqGradChan& s2)         {return SeqOperator::concat(s2,s1,true);}
inline SeqGradChanList&     operator + (SeqGradChanList& s1, SeqGradChanList& s2)     {return SeqOperator::concat(s1,s2);}
inline SeqGradChanParallel& operator + (SeqGradChanList& s1, SeqGradChanParallel& s2) {return SeqOperator::concat(s1,s2,false);}

inline SeqObjList&          operator + (SeqGradChanParallel& s1, const SeqObjBase& s2){return SeqOperator::concat(s2,s1,true);}
inline SeqObjList&          operator + (SeqGradChanParallel& s1, const SeqObjList& s2)    {return SeqOperator::concat(s2,s1,true);}
inline SeqObjList&          operator + (SeqGradChanParallel& s1, const SeqObjLoop& s2)    {return SeqOperator::concat(s2,s1,true);}
inline SeqObjList&          operator + (SeqGradChanParallel& s1, const SeqDecoupling& s2) {return SeqOperator::concat(s2,s1,true);}
inline SeqObjList&          operator + (SeqGradChanParallel& s1, const SeqParallel& s2)   {return SeqOperator::concat(s2,s1,true);}
inline SeqGradChanParallel& operator + (SeqGradChanParallel& s1, SeqGradChan& s2)         {return SeqOperator::concat(s2,s1,true);}
inline SeqGradChanParallel& operator + (SeqGradChanParallel& s1, SeqGradChanList& s2)     {return SeqOperator::concat(s2,s1,true);}
inline SeqGradChanParallel& operator + (SeqGradChanParallel& s1, SeqGradChanParallel& s2) {return SeqOperator::concat(s1,s2);}



inline SeqParallel& operator / (const SeqObjBase& s1, SeqGradObjInterface& s2) {return SeqOperator::simultan(s1,s2);}
inline SeqParallel& operator / (SeqGradObjInterface& s1, const SeqObjBase& s2) {return SeqOperator::simultan(s2,s1);}

inline SeqParallel& operator / (const SeqObjBase& s1, SeqGradChan& s2) {return SeqOperator::simultan(s1,s2);}
inline SeqParallel& operator / (SeqGradChan& s1, const SeqObjBase& s2) {return SeqOperator::simultan(s2,s1);}

inline SeqParallel& operator / (const SeqObjBase& s1, SeqGradChanList& s2) {return SeqOperator::simultan(s1,s2);}
inline SeqParallel& operator / (SeqGradChanList& s1, const SeqObjBase& s2) {return SeqOperator::simultan(s2,s1);}


inline SeqGradChanParallel& operator / (SeqGradChan& s1, SeqGradChan& s2) {return SeqOperator::simultan(s1,s2);}

inline SeqGradChanParallel& operator / (SeqGradChan& s1, SeqGradChanList& s2) {return SeqOperator::simultan(s1,s2);}
inline SeqGradChanParallel& operator / (SeqGradChanList& s1, SeqGradChan& s2) {return SeqOperator::simultan(s2,s1);}

inline SeqGradChanParallel& operator / (SeqGradChan& s1, SeqGradChanParallel& s2) {return SeqOperator::simultan(s1,s2);}
inline SeqGradChanParallel& operator / (SeqGradChanParallel& s1, SeqGradChan& s2) {return SeqOperator::simultan(s2,s1);}

inline SeqGradChanParallel& operator / (SeqGradChanList& s1, SeqGradChanList& s2) {return SeqOperator::simultan(s1,s2);}

inline SeqGradChanParallel& operator / (SeqGradChanList& s1, SeqGradChanParallel& s2) {return SeqOperator::simultan(s1,s2);}
inline SeqGradChanParallel& operator / (SeqGradChanParallel& s1, SeqGradChanList& s2) {return SeqOperator::simultan(s2,s1);}

inline SeqGradChanParallel& operator / (SeqGradChanParallel& s1, SeqGradChanParallel& s2) {return SeqOperator::simultan(s1,s2);}


#endif
