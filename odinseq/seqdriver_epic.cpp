#ifndef TJUTILS_CONFIG_H
#define TJUTILS_CONFIG_H
#include <tjutils/config.h>
#endif

#ifdef EPIC_PLUGIN
#include "../platforms/EPIC/odinseq_epic/seqepic.cpp"
#include "../platforms/EPIC/odinseq_epic/seqacq_epic.cpp"
#include "../platforms/EPIC/odinseq_epic/seqacqepi_epic.cpp"
#include "../platforms/EPIC/odinseq_epic/seqcounter_epic.cpp"
#include "../platforms/EPIC/odinseq_epic/seqdec_epic.cpp"
#include "../platforms/EPIC/odinseq_epic/seqdelay_epic.cpp"
#include "../platforms/EPIC/odinseq_epic/seqfreq_epic.cpp"
#include "../platforms/EPIC/odinseq_epic/seqgradchan_epic.cpp"
#include "../platforms/EPIC/odinseq_epic/seqgradtrapez_epic.cpp"
#include "../platforms/EPIC/odinseq_epic/seqlist_epic.cpp"
#include "../platforms/EPIC/odinseq_epic/seqparallel_epic.cpp"
#include "../platforms/EPIC/odinseq_epic/seqphase_epic.cpp"
#include "../platforms/EPIC/odinseq_epic/seqpuls_epic.cpp"
#include "../platforms/EPIC/odinseq_epic/seqtrigg_epic.cpp"
#endif
