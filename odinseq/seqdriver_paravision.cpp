#ifndef TJUTILS_CONFIG_H
#define TJUTILS_CONFIG_H
#include <tjutils/config.h>
#endif

// include Qt stuff first to avoid ambiguities with 'debug'
/*
#ifdef GUISUPPORT
#include <qlistview.h>
#endif
*/


#ifdef PARAVISION_PLUGIN
#include "../platforms/Paravision/odinseq_paravision/seqparavision.cpp"
#include "../platforms/Paravision/odinseq_paravision/seqacq_paravision.cpp"
#include "../platforms/Paravision/odinseq_paravision/seqacqepi_paravision.cpp"
#include "../platforms/Paravision/odinseq_paravision/seqcounter_paravision.cpp"
#include "../platforms/Paravision/odinseq_paravision/seqdec_paravision.cpp"
#include "../platforms/Paravision/odinseq_paravision/seqdelay_paravision.cpp"
#include "../platforms/Paravision/odinseq_paravision/seqfreq_paravision.cpp"
#include "../platforms/Paravision/odinseq_paravision/seqgradchan_paravision.cpp"
#include "../platforms/Paravision/odinseq_paravision/seqgradtrapez_paravision.cpp"
#include "../platforms/Paravision/odinseq_paravision/seqlist_paravision.cpp"
#include "../platforms/Paravision/odinseq_paravision/seqparallel_paravision.cpp"
#include "../platforms/Paravision/odinseq_paravision/seqphase_paravision.cpp"
#include "../platforms/Paravision/odinseq_paravision/seqpilot_paravision.cpp"
#include "../platforms/Paravision/odinseq_paravision/seqpuls_paravision.cpp"
#include "../platforms/Paravision/odinseq_paravision/seqtrigg_paravision.cpp"
#endif

