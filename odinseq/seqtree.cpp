#ifndef TJUTILS_CONFIG_H
#define TJUTILS_CONFIG_H
#include <tjutils/config.h>
#endif

#ifdef HAVE_TYPEINFO
#include <typeinfo>
#endif

#include "seqtree.h"

#include <tjutils/tjfeedback.h>


void eventContext::increase_progmeter() {
  if(event_progmeter) {
    abort = (abort || event_progmeter->increase_counter());
  }
}

///////////////////////////////////////////////////////////////////////////


SeqTreeObj::SeqTreeObj() : SeqClass() {
  Log<Seq> odinlog("SeqTreeObj","SeqTreeObj()");
  set_label("unnamedSeqTreeObj"); // this is neccessary because SeqClass is a virtual base class
}

void SeqTreeObj::display_event(eventContext& context) const {
  if(!context.event_display) return;
  svector columntext; columntext.resize(2);
  columntext[0]=ftos(context.elapsed);
  columntext[1]=get_label();
  context.event_display->display_node(this,0,looplevel,columntext);
}


void SeqTreeObj::query(queryContext& context) const {

  if(context.action==count_acqs) context.numof_acqs=0;

  if(context.action==checkoccur) context.checkoccur_result=context.checkoccur_result || (context.checkoccur_sto==this);

  if(context.action==display_tree) {
    svector columntext; columntext.resize(4);
    const char* type="unknown";
#ifdef HAVE_TYPEINFO
    type=typeid(*this).name();
    while( (*type) >='0' && (*type) <='9' ) type++;
#endif

    STD_string typestr(type);
    // replace SeqMethod_... by SeqMethod for seqtree dump of seqtest
    if(typestr.find("SeqMethod_")==0) typestr="SeqMethod";

    columntext[0]=get_label();
    columntext[1]=typestr;
    columntext[2]=ftos(get_duration());
    columntext[3]=get_properties();
    context.tree_display->display_node(this,context.parentnode,context.treelevel,columntext);
  }

}



bool SeqTreeObj::contains(const SeqTreeObj* sto) const {
  queryContext qc;
  qc.action=checkoccur; qc.checkoccur_sto=sto; 
  this->query(qc);
  return qc.checkoccur_result;
}


void SeqTreeObj::tree(SeqTreeCallbackAbstract* display) const {
  queryContext qc;
  qc.action=display_tree;
  qc.tree_display=display;
  qc.parentnode=this;
  this->query(qc);
}



int SeqTreeObj::looplevel=0;

