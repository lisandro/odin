#include "seqpulsndim.h"

#include "seqgradwave.h"
#include "seqgradconst.h"
#include "seqdelay.h"
#include "seqgradchanparallel.h"

#include <tjutils/tjhandler_code.h>


struct SeqPulsNdimObjects {

  SeqPulsNdimObjects() {}

  SeqPulsNdimObjects(const STD_string& object_label, double gradshift_delay) :
    gx(object_label+"_Gx",readDirection,0.0,0.0,fvector()),
    gy(object_label+"_Gy",phaseDirection,0.0,0.0,fvector()),
    gz(object_label+"_Gz",sliceDirection,0.0,0.0,fvector()),
    sgh(object_label+"_handler"),
    rftrain(object_label+"_rftrain"),
    sp(object_label+"_rf"),
    dgs(object_label+"_shift_delay",gradshift_delay-sp.get_pulsstart()) {}


  SeqGradWave gx;
  SeqGradWave gy;
  SeqGradWave gz;

  SeqGradDelay gx_delay;
  SeqGradDelay gy_delay;
  SeqGradDelay gz_delay;

  SeqGradChanParallel sgh;
  SeqObjList rftrain;

  SeqPuls sp;
  SeqDelay dgs;
};

/////////////////////////////////////////////////////////////////

SeqPulsNdim::SeqPulsNdim(const STD_string& object_label )
 : SeqParallel(object_label),
   objs(new SeqPulsNdimObjects(object_label,systemInfo->get_grad_shift_delay())) {
  SeqPulsInterface::set_marshall(&(objs->sp));
  SeqFreqChanInterface::set_marshall(&(objs->sp));
  dims=0;
  gradshift=0.0;
  build_seq();
}

SeqPulsNdim::SeqPulsNdim(const SeqPulsNdim& spnd) : objs(new SeqPulsNdimObjects) {
  SeqPulsInterface::set_marshall(&(objs->sp));
  SeqFreqChanInterface::set_marshall(&(objs->sp));
  SeqPulsNdim::operator = (spnd);
}


SeqPulsNdim::~SeqPulsNdim() {
  Log<Seq> odinlog(this,"~SeqPulsNdim()");
  delete objs;
}

SeqPulsNdim& SeqPulsNdim::set_rfwave(const cvector& waveform) {
  objs->sp.set_wave(waveform);
  return *this;
}

SeqPulsNdim& SeqPulsNdim::set_gradwave(direction dir, const fvector& waveform) {
  if(dir==readDirection) objs->gx.set_wave(waveform);
  if(dir==phaseDirection) objs->gy.set_wave(waveform);
  if(dir==sliceDirection) objs->gz.set_wave(waveform);
  return *this;
}


SeqPulsNdim& SeqPulsNdim::set_B1max(float b1max) {
  objs->sp.set_B1max(b1max);
  return *this;
}

cvector SeqPulsNdim::get_rfwave() const {return objs->sp.get_wave();}

fvector SeqPulsNdim::get_gradwave(direction dir) const {
  if(dir==readDirection) return objs->gx.get_wave();
  if(dir==phaseDirection) return objs->gy.get_wave();
  if(dir==sliceDirection) return objs->gz.get_wave();
  return fvector();
}


int SeqPulsNdim::get_dims() const {return dims;}


SeqPulsInterface& SeqPulsNdim::set_pulsduration(float pulsduration) {
  Log<Seq> odinlog(this,"SeqPulsNdim::set_pulsduration");
  ODINLOG(odinlog,normalDebug) << "=" << pulsduration << STD_endl;
  objs->sp.set_pulsduration(pulsduration);
  objs->gx.set_duration(pulsduration);
  objs->gy.set_duration(pulsduration);
  objs->gz.set_duration(pulsduration);
  return *this;
}



float SeqPulsNdim::get_magnetic_center() const {
  Log<Seq> odinlog(this,"get_magnetic_center");
  ODINLOG(odinlog,normalDebug) << "dgs.get_duration()=" << objs->dgs.get_duration() << STD_endl;
  ODINLOG(odinlog,normalDebug) << "sp.get_magnetic_center()=" << objs->sp.get_magnetic_center() << STD_endl;

  if(get_dims()) return objs->sgh.get_pulprogduration()+objs->dgs.get_duration()+objs->sp.get_magnetic_center();
  else return objs->sp.get_magnetic_center();
}


SeqVector& SeqPulsNdim::set_indexvec(const ivector& iv) {objs->sp.set_indexvec(iv); return objs->sp;}


SeqPulsNdim& SeqPulsNdim::operator += (SeqGradChanList& sgcl) {objs->sgh+=sgcl; return *this;}


SeqPulsNdim& SeqPulsNdim::set_system_flipangle(float angle) {objs->sp.set_system_flipangle(angle); return *this;}


SeqPulsNdim& SeqPulsNdim::set_grad_shift_offset(float grad_shift_offset) {
  gradshift=grad_shift_offset;
  return *this;
}



SeqPulsNdim& SeqPulsNdim::operator = (const SeqPulsNdim& spnd) {
  Log<Seq> odinlog(this,"operator = (...)");
  SeqParallel::operator = (spnd);
  dims=spnd.get_dims();
  gradshift=spnd.gradshift;

  objs->gx=spnd.objs->gx;
  objs->gy=spnd.objs->gy;
  objs->gz=spnd.objs->gz;

  objs->gx_delay=spnd.objs->gx_delay;
  objs->gy_delay=spnd.objs->gy_delay;
  objs->gz_delay=spnd.objs->gz_delay;

  objs->sgh=spnd.objs->sgh;
  objs->rftrain=spnd.objs->rftrain;
  objs->dgs=spnd.objs->dgs;
  objs->sp=spnd.objs->sp;

  build_seq();
  return *this;
}

SeqPulsNdim& SeqPulsNdim::build_seq() {
  Log<Seq> odinlog(this,"build_seq");
  int dimensionality=get_dims();
  ODINLOG(odinlog,normalDebug) << "dimensionality=" << dimensionality << STD_endl;
  objs->sgh.clear();
  objs->rftrain.clear();
  clear_gradptr();

  double shift=systemInfo->get_grad_shift_delay()+gradshift-objs->sp.get_pulsstart();
  bool have_dgs=false;
  bool have_grad_delays=false;
  if(shift>0.0) {
    have_dgs=true;
    objs->dgs.set_duration(shift);
  }
  if(shift<0.0) {
    have_grad_delays=true;
    objs->gx_delay=SeqGradDelay("gx_delay",readDirection,-shift);
    objs->gy_delay=SeqGradDelay("gy_delay",phaseDirection,-shift);
    objs->gz_delay=SeqGradDelay("gz_delay",sliceDirection,-shift);
  }


  if(dimensionality==3) {
    if(have_grad_delays) objs->sgh+=((objs->gx_delay+objs->gx)/(objs->gy_delay+objs->gy)/(objs->gz_delay+objs->gz));
    else objs->sgh+=(objs->gx/objs->gy/objs->gz);
  }

  if(dimensionality==2) {
    if(have_grad_delays) objs->sgh+=((objs->gx_delay+objs->gx)/(objs->gy_delay+objs->gy)/(objs->gz_delay));
    else objs->sgh+=(objs->gx/objs->gy);
  }

  if(dimensionality==1) {
    if(have_grad_delays) objs->sgh+=((objs->gx_delay)/(objs->gy_delay)/(objs->gz_delay+objs->gz));
    else objs->sgh+=(objs->gz);
    double dummydur=objs->gz.get_gradduration();
    ODINLOG(odinlog,normalDebug) << "dummydur=" << dummydur << STD_endl;
  }

  if(dimensionality) {
    if(have_dgs) objs->rftrain.append(objs->dgs);
    set_gradptr((SeqGradObjInterface*)&(objs->sgh));
  }
  objs->rftrain.append(objs->sp);

  set_pulsptr(&(objs->rftrain));

  return *this;
}


template class Handled<SeqPulsNdim*>;
template class Handler<SeqPulsNdim*>;
