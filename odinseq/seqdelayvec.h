/***************************************************************************
                          seqdelayvec.h  -  description
                             -------------------
    begin                : Mon Oct 7 2002
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQDELAYVEC_H
#define SEQDELAYVEC_H

#include <odinseq/seqobj.h>
#include <odinseq/seqvec.h>
#include <odinseq/seqdriver.h>

/**
  * @ingroup odinseq_internals
  * The base class for platform specific drivers of delay vectors
  */
class SeqDelayVecDriver : public SeqDriverBase {

 public:
  SeqDelayVecDriver() {}
  virtual ~SeqDelayVecDriver() {}

  virtual bool prep_driver() = 0;

  virtual STD_string get_program(programContext& context, double duration) const = 0;

  virtual bool unroll_program() const = 0;

  virtual SeqDelayVecDriver* clone_driver() const = 0;
};

///////////////////////////////////////////////////////////////


/**
  * @addtogroup odinseq
  * @{
  */


/**
  * \brief  Vector of variable timing delays
  *
  * This class represents a vector of delay (delay list) that can be attached to a loop.
  */
class SeqDelayVector : public SeqObjBase, public SeqVector {

 public:

/**
  * Constructs a delay vector labeled 'object_label' with the following properties:
  * - delaylist: The list of delays that will be played out subsequently when looping
  *   over this object
  */
  SeqDelayVector(const STD_string& object_label,const dvector& delaylist);


/**
  * Constructs an empty delay vector labeled 'object_label'
  */
  SeqDelayVector(const STD_string& object_label = "unnamedSeqDelayVector");


/**
  * Constructs a delay which is a copy of 'sdv'
  */
  SeqDelayVector(const SeqDelayVector& sdv);


/**
  * This assignment operator will make this object become an exact copy of 'sdv'.
  */
  SeqDelayVector& operator = (const SeqDelayVector& sdv);


/**
  * Specifies the list of delays that will be played out subsequently when looping
  *   over this object
  */
  SeqDelayVector& set_delayvector(const dvector& delaylist);

/**
  * Returns the list of delays that will be played out subsequently when looping
  * over this object
  */
  dvector get_delayvector() const;


  // overloading virtual function from SeqTreeObj
  double get_duration() const;
  STD_string get_program(programContext& context) const;
  SeqValList get_delayvallist() const;

 private:

  // overwriting virtual functions from SeqClass
  bool prep();

  // overloading virtual function from SeqVector
  unsigned int get_vectorsize() const;
  bool needs_unrolling_check() const {return delayvecdriver->unroll_program();}

  mutable SeqDriverInterface<SeqDelayVecDriver> delayvecdriver;

  dvector delayvec;

};


/** @}
  */


#endif
