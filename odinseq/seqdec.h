/***************************************************************************
                          seqdec.h  -  description
                             -------------------
    begin                : Fri Sep 7 2001
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQDEC_H
#define SEQDEC_H

#include <tjutils/tjembed.h>

#include <odinseq/seqlist.h>
#include <odinseq/seqfreq.h>
#include <odinseq/seqsimvec.h>
#include <odinseq/seqdriver.h>




/**
  * @ingroup odinseq_internals
  * The base class for platform specific drivers of decoupling periods
  */
class SeqDecouplingDriver : public SeqDriverBase {

 public:
  SeqDecouplingDriver() {}
  virtual ~SeqDecouplingDriver() {}

  virtual bool prep_driver(double decdur, int channel, float decpower, const STD_string& program, double pulsedur) = 0;
  virtual void event(eventContext& context, double start) const = 0;

  virtual double get_preduration() const = 0;
  virtual double get_postduration() const = 0;

  virtual STD_string get_preprogram(programContext& context, const STD_string& iteratorcommand) const = 0;
  virtual STD_string get_postprogram(programContext& context) const = 0;

  virtual SeqDecouplingDriver* clone_driver() const = 0;
};


////////////////////////////////////////////////////////////////////////////




/**
  * @addtogroup odinseq
  * @{
  */

/**
  * \brief  Decoupling period
  *
  * This class represents a decoupling container which can hold a part
  * of the sequence where decoupling is switched on.
  */
class SeqDecoupling : public SeqObjList, public Embed<SeqDecoupling,SeqObjBase>, public SeqFreqChan {

 public:

/**
  * Constructs a container for a sequence part at which decoupling will be switched on
  * with the following properties:
  * - nucleus:     The nucleus for which the decoupling should be performed.
  * - decpower:    The maximum power for the decoupling program.
  * - freqlist:    Frequency list for the decoupler.
  * - decprog:     The file name of the decoupling program which will be used. If none
  *                is specified, continuous wave decouling will be used.
  * - decpulsduration: If a decoupling program is given, this parameter determines
  *                    the pulse duration of each single pulse in a composite pulse
  *                    decoupling scheme.
  */
  SeqDecoupling(const STD_string& object_label,const STD_string& nucleus,float decpower,const dvector& freqlist=0,
                          const STD_string decprog="",float decpulsduration=0.0);

/**
  * Constructs a decoupling container which is a copy of 'sd'
  */
  SeqDecoupling(const SeqDecoupling& sd);

/**
  * Constructs an empty decoupling container with the given label.
  */
  SeqDecoupling(const STD_string& object_label="unnamedSeqDecoupling");



/**
  * This function specifies that during the sequence part 'so' decoupling
  * will be switched on.
  */
  SeqDecoupling& operator () (const SeqObjBase& so);



/**
  * Returns the decoupling power
  */
  float get_decpower() const {return power;}

/**
  * Sets the decoupling power
  */
  void set_decpower(float p) {power=p;}

/**
  * Returns the decoupling program
  */
  STD_string get_program() const;

/**
  * Sets the decoupling program
  */
  void set_program(const STD_string& p);

/**
  * Returns the decoupling pulse duration for Composite Pulse Decoupling
  */
  double get_pulsduration() const;

/**
  * Sets the decoupling pulse duration for Composite Pulse Decoupling
  */
  void set_pulsduration(float d);


/**
  * This assignment operator will make this object become an exact copy of 'sd'.
  */
  SeqDecoupling& operator = (const SeqDecoupling& sd);


  // implementing virtual functions of SeqTreeObj
  double get_duration() const;
  unsigned int event(eventContext& context) const;


  // overwriting virtual function from SeqFreqChan
  STD_string get_program(programContext& context) const;
  double get_freqchan_duration() const {return get_duration();}
  unsigned int get_freqlistindex() const {return 2;}
  const SeqVector& get_freqlist_vector() const;


  SeqValList get_freqvallist(freqlistAction action) const;

  void clear_container();

 private:
  friend class Embed<SeqDecoupling,SeqObjBase>;

  int set_body(const SeqObjBase& so);

  // overwriting virtual functions from SeqClass
  bool prep();

  float power;
  STD_string decprogram;
  double pulsduration;


  mutable SeqDriverInterface<SeqDecouplingDriver> decdriver;


  SeqSimultanVector instvec;

};

/** @}
  */


#endif
