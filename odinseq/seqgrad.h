/***************************************************************************
                          seqgrad.h  -  description
                             -------------------
    begin                : Wed Aug 8 2001
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQGRAD_H
#define SEQGRAD_H

#include <odinseq/seqclass.h>


class RotMatrix; // forward declaration

/**
  * @addtogroup odinseq_internals
  * @{
  */

/**
  * This is the abstract base class for all objects that represent
  * gradient fields
  */
class SeqGradInterface : public virtual SeqClass {

 public:

/**
  * Changes the strength of the gradient object
  */
  virtual SeqGradInterface& set_strength(float gradstrength) = 0;

/**
  * Changes the polarity of the gradient, i.e. inverts the sign of the gradient strength
  */
  virtual SeqGradInterface& invert_strength() = 0;

/**
  * Returns the strength of the gradient object
  */
  virtual float get_strength() const = 0;

/**
  * Returns the integral vector of the gradient course
  */
  virtual fvector get_gradintegral() const = 0;

/**
  * Returns the norm of the integral vector of the gradient course
  */
  float get_gradintegral_norm() const;


/**
  * Returns the duration of the gradient object. This function returns
  * only the effective duration during whichthe  gradient is active,
  * any delays before and after the gradient are omitted.
  */
  virtual double get_gradduration() const = 0;

/**
  * This function can be used to specify a rotation of the gradient object
  * in the spatial domain, the rotation will be applied to this object only
  * rather than to the whole sequence.
  */
  virtual SeqGradInterface& set_gradrotmatrix(const RotMatrix& matrix) = 0;


 protected:

  SeqGradInterface() {}
  virtual ~SeqGradInterface() {}

};


/** @}
  */


#endif
