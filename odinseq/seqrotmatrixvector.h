/***************************************************************************
                          seqrotmatrixvector.h  -  description
                             -------------------
    begin                : Wed Jul 30 2003
    copyright            : (C) 2000-2015 by Thies H. Jochimsen and Michael v. Mengershausen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQROTMATRIXVECTOR_H
#define SEQROTMATRIXVECTOR_H

#include <odinseq/seqvec.h>

class SeqGradChanList;  // forward declaration

/**
  * @ingroup odinseq
  *
  * \brief Vector of SeqRotMatrix's
  *
  * This class keeps an arry of rotation matrices.
  * It can be used consecutively by a gradient object within a loop
  * via the function SeqObjList::set_gradrotmatrixvector().
  */
class SeqRotMatrixVector : public SeqVector, public Handled<const SeqRotMatrixVector* >  {

 public: 
/**
  * default constructor
  */
 SeqRotMatrixVector(const STD_string& object_label = "unnamedSeqRotMatrixVector");

/**
  * constructs a rot-matrix vector from an existing rot-matrix vector
  */
 SeqRotMatrixVector(const SeqRotMatrixVector& srmv);


/**
  * Destructor
  */
 ~SeqRotMatrixVector();

/**
  * assignment operator
  */
 SeqRotMatrixVector& operator = (const SeqRotMatrixVector& srmv);

/**
  * returns the index'th matrix
  */
 const RotMatrix& operator [] (unsigned long index) const;

/**
  * returns the currently active matrix when used inside a loop
  */
 const RotMatrix& get_current_matrix () const;

/**
  * Makes this a vector of 'nsegments' matrices that
  * rotate the first and second dimension (read, phase) with
  * a constant angle increment of 2*PI/nsegments.
  */
  SeqRotMatrixVector& create_inplane_rotation(unsigned int nsegments);

/**
  * clears the list of rotation matrices
  */
 SeqRotMatrixVector& clear() {
   rotMatrixList.clear();
   return *this;
 };


/**
  * adds a new rotation matrix at the end of the vector
  */
  SeqRotMatrixVector& append(const RotMatrix& srm) {
//   srm.check_and_correct();
   rotMatrixList.push_back(srm);
   return *this;
 };


/**
  * returns values with max. absolute values for each element of the matrix vector
  */
  RotMatrix get_maxMatrix () const;



  // implemented virtual from SeqVector
  unsigned int get_vectorsize() const {return rotMatrixList.size();};
  bool needs_unrolling_check() const {return get_vectorsize()>1;}
  bool is_qualvector() const {return false;}
  bool prep_iteration() const;
  svector get_vector_commands(const STD_string& iterator) const {iterator_cache=iterator; return svector();}

  // return stored iterator, quick hack for EPIC rotmats
  const STD_string& get_iterator() const {return iterator_cache;}

 private:
  friend class SeqGradChan;

  mutable STD_string iterator_cache;

  STD_list<RotMatrix> rotMatrixList;
  RotMatrix dummyrotmat;

};

#endif
