/***************************************************************************
                          seqclass.h  -  description
                             -------------------
    begin                : Sun Jan 20 2002
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQCLASS_H
#define SEQCLASS_H


#include <tjutils/tjtypes.h>
#include <tjutils/tjstring.h>
#include <tjutils/tjstatic.h>
#include <tjutils/tjlog.h>
#include <tjutils/tjlabel.h>
#include <tjutils/tjindex.h>
#include <tjutils/tjhandler.h>
#include <tjutils/tjvector.h>

#include <odinpara/reco.h>
#include <odinpara/geometry.h>
#include <odinpara/seqpars.h>
#include <odinpara/study.h>
#include <odinpara/system.h>


/**
  * @addtogroup odinseq_internals
  * @{
  */


///////////////////////////////////////////////////////////////////////////

// For debugging the Seq component
class Seq {
 public:
  static const char* get_compName();
};

///////////////////////////////////////////////////////////////////////////

class SeqVector; //forward declaration

/**
  * This is the base class for all Seq* classes. It provides
  * functions that are common to all classes of the sequence library,
  * e.g. functions for error/debug message handling and acces to
  * proxies, i.e. geometryInfo, studyInfo and systemInfo.
  */
class SeqClass : public virtual Labeled, public StaticHandler<SeqClass> {

 public:


/**
  * Mark object as temporary so that it will be deleted automatically
  */
  SeqClass& set_temporary();


  System& get_systemInfo() {return *systemInfo;}

  // functions to initialize/delete static members by the StaticHandler template class
  static void init_static();
  static void destroy_static();


 protected:

  // prevent direct construction/assignment/destruction of this class
  SeqClass();
  virtual ~SeqClass();
  SeqClass& operator = (const SeqClass& sc);

/**
  * Writes a marshalling error, i.e. mashalling without appropriate sub-object, to the log
  */
  void marshall_error() const;

/**
  * Delete all temporary objects
  */
  static void clear_temporary();

/**
  * Overload this function in case the class is a container object, i.e.
  * it contains other sequence objects. The function should clear all
  * references to other sequence objects, i.e. to thos it contains.
  */
  virtual void clear_container() {}


/**
  * Clear the contents of all container objects
  */
  static void clear_containers();

/**
  * Overload this function to prepare objects before the measurement. This function
  * will then be called automatically after editing the sequence parameters and
  * before the measurement is started. This function should not alter the physical
  * properties of the sequence object, e.g. its duration.
  */
  virtual bool prep() {prepped=true; return true;}


  // call prep() of all objects
  static bool prep_all();


  // points to systemInfo of the currently active platform
  SystemInterface& systemInfo;

  // points to the geometry parameters
  static SingletonHandler<Geometry,false>  geometryInfo;

  // points to the geometry parameters
  static SingletonHandler<Study,false>  studyInfo;

  // points to the reco parameters
  static SingletonHandler<RecoPars,false> recoInfo;


  static SeqVector& get_dummyvec();


 private:
  friend class SeqOperator;
  friend class SeqMethod;
  friend class SeqMethodProxy;
  friend class SystemInterface;
  friend class SeqPlatformProxy;


  // remove all references to other sequence objects
  static void clear_objlists();

  bool prepped;

  // List to hold references to SeqClass objects
  struct SeqClassList : public STD_list<SeqClass *>, public Labeled {};

  static SingletonHandler<SeqClassList,false> allseqobjs;
  static SingletonHandler<SeqClassList,false> tmpseqobjs;

  static SingletonHandler<SeqClassList,false> seqobjs2prep;
  static SingletonHandler<SeqClassList,false> seqobjs2clear;

  static SystemInterface* systemInfo_ptr;

  static SeqVector* dummyvec;
};



/** @}
  */
#endif
