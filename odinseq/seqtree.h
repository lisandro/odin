/***************************************************************************
                          seqtree.h  -  description
                             -------------------
    begin                : Fri Oct 17 2003
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/



#ifndef SEQTREE_H
#define SEQTREE_H

#include <tjutils/tjvallist.h>

#include <odinseq/seqclass.h>
#include <odinseq/seqoperator.h>



/**
  * @addtogroup odinseq_internals
  * @{
  */

///////////////////////////////////////////////////////////////////////////

/**
  * Callback to display tree-like structures
  */
class SeqTreeCallbackAbstract {

 public:
  virtual void display_node(const SeqClass* thisnode, const SeqClass* parentnode, int treelevel, const svector& columntext) = 0;

};

///////////////////////////////////////////////////////////////////////////

class ProgressMeter; // forward declaration

/**
  * This enum is used to specify the behaviour for an event:
  * - seqRun: trigger the hardware events
  * - printEvent: Print all events to the console
  * - countEvents: Returns the number of events without actually executing them
  */
enum eventAction {seqRun=0, printEvent, countEvents};

/**
  * Structure that holds the context in which the event of a sequence object is requested
  */
struct eventContext {
  eventContext() : action(seqRun), abort(false), noflush(false), seqcheck(false),
                   elapsed(0.0),
                   event_display(0), event_progmeter(0) {}

  // the action to be performed
  eventAction action;

  // signal to abort execution of the sequence
  bool abort;

  // do not flush eventblock
  bool noflush;

  // just a test run (e.g. fSEQCheck on Idea)
  bool seqcheck;

  // counter for time elapsed since start of the sequence
  double elapsed;

  // callback pointers to display events in GUI/console
  SeqTreeCallbackAbstract* event_display;

  // pointer for progess meter
  ProgressMeter* event_progmeter;

  // helper function to update/increase progmeter and store status in 'abort'
  void increase_progmeter();

};

///////////////////////////////////////////////////////////////////////////

/**
  * This enum is used to specify the behaviour for a call to get_freqvallist:
  * - calcDeps: Calculates dependencies between consecutive pulse frequency events
  * - calcList: Creates the gloabal pulse frequency list
  * - calcAcqList: Creates the gloabal acquisition frequency list
  * - calcDecList: Creates the gloabal decoupling frequency list
  */
enum freqlistAction {calcDeps=0,calcList,calcAcqList,calcDecList};

///////////////////////////////////////////////////////////////////////////

/**
  * This enum is used to specify the behaviour for program printing:
  * - brukerPpg: create Bruker pulse program
  * - brukerGpg: create Bruker gradient program
  * - brukerParx: create Bruker parx interface file (to set ACQP parameters)
  * - epicCode:  Sequence source code for the EPIC framework
  * - epicPg:    Code for the pulsegen section (EPIC)
  */
enum programMode {brukerPpg=0, brukerGpg, brukerParx, epicCode, epicPg, epicScan, epicPreScan, epicPreScanDummy, numOfProgModes};


/**
  * Structure that holds the context the sequence program is (recursively) generated with, i.e. type of program or formatting
  */
struct programContext {
  programContext() : mode(programMode(0)), formatted(true), nestlevel(0), neststatus(false)  {}

  // the type of program to generate
  programMode mode;

  // whether code is nicely formatted (indention, comments), the main
  // use of this flag is to decide whether to blocks of code are the
  // same (despite formatting) and whether they can be wrapped in a loop
  bool formatted;

  // counter for indention if generating pulse programs
  int nestlevel;

  // whether indention is currently enabled
  bool neststatus;
};


///////////////////////////////////////////////////////////////////////////

class SeqTreeObj; //forward declaration

/**
  * This enum is used to specify the behaviour for a call to query:
  * - count_acqs: Count the number of acquisition objects
  * - checkoccur: Check the occurence of a particular sequence object in this branch of the tree
  * - check_acq_iter: Check for an iterator in subtree which is relevant for reco indicies
  * - tag_toplevel_reploop: Identify and tag outermost repetition loop
  * - display_tree: Display sequence tree
  */
enum queryAction {count_acqs, checkoccur, check_acq_iter, tag_toplevel_reploop, display_tree};

/**
  * Structure that holds the context the sequence tree is (recursively) queried with
  */
struct queryContext {
  queryContext() : action(count_acqs), numof_acqs(0),
  checkoccur_sto(0), checkoccur_result(false),
  check_acq_iter_result(false),
  tree_display(0), parentnode(0), treelevel(0),
  repetitions_prot(0) {}

  // the action requested
  queryAction action;

  // stuff for action 'count_acqs'
  unsigned int numof_acqs;

  // stuff for action 'checkoccur'
  const SeqTreeObj* checkoccur_sto;
  bool checkoccur_result;

  bool check_acq_iter_result;
  
  // stuff for action 'display_tree'
  SeqTreeCallbackAbstract* tree_display;
  const SeqTreeObj* parentnode;
  int treelevel;

  // stuff for action 'tag_toplevel_reploop'
  int repetitions_prot; // the number of reps in the protocol
};


///////////////////////////////////////////////////////////////////////////

/**
  * Instantiation of value list (SeqValList) with double.
  * It is used in sequence trees to retrieve frequency lists, delay lists, etc.
  */
struct SeqValList : public ValList<double> {
  SeqValList(const STD_string& object_label="unnamedSeqValList", unsigned int repetitions=1) : ValList<double>(object_label,repetitions) {}
};

///////////////////////////////////////////////////////////////////////////



/**
  * This is the base class for all objects that can be part of the sequence tree
  */
class SeqTreeObj : public virtual SeqClass {

 public:

/**
  * Returns the  duration of the sequence object.
  */
  virtual double get_duration() const = 0;

/**
  * Returns the part in the pulse/gradient program of this sequence object.
  * 'context' holds data about how the program should be generated, e.g. type
  * of program and formatting.
  */
  virtual STD_string get_program(programContext& context) const {return "";}

/**
  * Returns a string describing the object
  */
  virtual STD_string get_properties() const {return "";}


/**
  * Queries the sequence tree for events, returns the number of events executed
  */
  virtual unsigned int event(eventContext& context) const { // make inline to speed up event execution
    if(context.action==printEvent) display_event(context);
    context.elapsed+=get_duration();
    return 0; // no real hardware events played out
  }


/**
  * Appends the k-space coordinates in this branch of the sequence tree to 'coords'
  * and returns their ordering
  */
  virtual RecoValList get_recovallist(unsigned int reptimes, LDRkSpaceCoords& coords) const {return RecoValList();}

/**
  * Returns true if 'sto' can be found in the objects branch of the sequence tree
  */
  bool contains(const SeqTreeObj* sto) const;

/**
  * Generates a graphical representation of the sequence tree using  'display'
  */
  void tree(SeqTreeCallbackAbstract* display) const;

/**
  * Returns the list of frequency values in the objects branch of the sequence tree
  */
  virtual SeqValList get_freqvallist(freqlistAction action) const {return SeqValList();}

/**
  * Returns the list of delay values in the objects branch of the sequence tree
  */
  virtual SeqValList get_delayvallist() const {return SeqValList();}

/**
  * Returns the RF power deposition in the objects branch of the sequence tree.
  * The physical unit is unspecified, it depends on the current platform.
  */
  virtual double get_rf_energy() const {return 0.0;}


 protected:
  friend class SeqObjList;
  friend class SeqObjVector;
  friend class SeqParallel;



  SeqTreeObj();

  virtual ~SeqTreeObj() {}

  SeqTreeObj& operator = (const SeqTreeObj& sto) {
    SeqClass::operator =(sto);
    return *this;
  }

/**
  * Query the sequence tree recursively
  */
  virtual void query(queryContext& context) const;


  static int looplevel;

 private:
  void display_event(eventContext& context) const;

};



/** @}
  */

#endif
