/***************************************************************************
                          seqpulsar.h  -  description
                             -------------------
    begin                : Wed Aug 8 2001
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQPULSAR_H
#define SEQPULSAR_H

#include <tjutils/tjhandler.h>

#include <odinseq/seqpulsndim.h>
#include <odinseq/odinpulse.h>
#include <odinseq/seqgradtrapez.h>



/**
  * @addtogroup odinseq
  * @{
  */

/**
  * \brief  Pulsar pulses, combines OdinPulse and SeqPulsNdim
  *
  * This class is an interface to the Pulsar program;
  * It is possible to access most of the parameters of the underlying
  * data structure 'OdinPulse' via get/set functions
  */
class SeqPulsar : public SeqPulsNdim, public OdinPulse, public StaticHandler<SeqPulsar> {

 public:

/**
  * Constructs an empty Pulsar pulse with the given label.
  * If 'rephased' is true, rephasing gradient lobes will be added to null the
  * zero'th order gradient moment, i.e. the integral.
  * If 'interactive' is true, the pulse will be updated each time a parameter
  * was changed, otherwise update() has to be called manually.
  */
  SeqPulsar(const STD_string& object_label = "unnamedSeqPulsar", bool rephased=false, bool interactive=true);

/**
  * Constructs a Pulsar pulse which is a copy of 'sp'
  */
  SeqPulsar(const SeqPulsar& sp);

/**
  * Destructor
  */
  ~SeqPulsar();

/**
  * Specifies whether an additional gradient rephasing lobe will be added to rephase
  * the magnetization in case of an excitation pulse.
  * If strength is larger than zero, this maximum strength will be used
  * for the rephaser gradient.
  */
  SeqPulsar& set_rephased(bool rephased, float strength=0.0);

/**
  * Returns the rephasers integral on all three channels
  */
  fvector get_reph_gradintegral() const;

/**
  * This assignment operator will make this object become an exact copy of 'sp'.
  */
  SeqPulsar& operator = (const SeqPulsar& sp);

/**
  * Recalculates the pulse manually, this is only required if the pulse is not interactive
  */
  SeqPulsar& refresh();

/**
  * If this flag is set to true, the pulse will be recalculated each time a parameter is changed,
  * otherwise it must be done manually via the refresh() function
  */
  SeqPulsar& set_interactive(bool flag);


  // overloading virtual function from SeqGradInterface
  float get_strength() const {return OdinPulse::get_G0();} // always return strength of pulse, and not that of rephasers


  // reimplemented virtual functions from SeqPulsNdim
  int get_dims() const;

  // reimplemented virtual functions from SeqFreqChanInterface
  SeqFreqChanInterface& set_nucleus(const STD_string& nucleus);

  // reimplemented virtual functions from SeqPulsInterface
  float get_flipangle() const {return OdinPulse::get_flipangle(); }
  SeqPulsInterface& set_flipangle(float flipangle);
  SeqPulsInterface& set_pulsduration(float pulsduration);
  SeqPulsInterface& set_power(float pulspower);
  SeqPulsInterface& set_pulse_type(pulseType type);
  pulseType get_pulse_type() const { return SeqPulsNdim::get_pulse_type();}


  // overloading virtual function from SeqTreeObj
  STD_string get_properties() const;


  // initialize/destroy static members via StaticHandler
  static void init_static();
  static void destroy_static();

  // Dummy operators for vector of pulsars with STL replacement
  bool operator == (const SeqPulsar& sp) const {return STD_string(get_label())==STD_string(sp.get_label());}
  bool operator <  (const SeqPulsar& sp) const {return STD_string(get_label())<STD_string(sp.get_label());}
  bool operator != (const SeqPulsar& sp) const {return !(*this==sp);}


 private:

  friend class SeqPulsarReph;
  friend class SeqMethod;
  friend class SeqMethodProxy;

  void common_init();

  // virtual functions of OdinPulse
  OdinPulse& update();
  void update_B10andPower();

  void create_rephgrads(bool recreate) const; // const to be used in SeqPulsarReph constructor

  bool rephased_pulse;
  float rephaser_strength;
  bool attenuation_set;
  bool always_refresh;

  float rephase_integral[n_directions]; // cache for get_reph_gradintegral()
  mutable SeqGradTrapez* reph_grad[n_directions];

  static void register_pulse(SeqPulsar* pls);
  static void unregister_pulse(SeqPulsar* pls);

  // struct to store references to all pulses which are currently active
  struct PulsarList : public STD_list<const SeqPulsar*>, public Labeled {};

  static SingletonHandler<PulsarList,false> active_pulsar_pulses;

};


////////////////////////////////////////////////////////////////////////////////

/**
  *
  * \brief Post-pulse rephaser
  *
  * This class represents a rephasing gradient pulse for SeqPulsar, i.e. it can serve as a refocusing lobe for a SeqPulsar
  * pulse
  */
class SeqPulsarReph : public SeqGradChanParallel {

 public:
/**
  * Constructs a rephasing gradient pulse labeled 'object_label' with the following properties:
  * - puls:   The puls that should be rephased
  */
  SeqPulsarReph(const STD_string& object_label,const SeqPulsar& puls);

/**
  * Constructs a copy of 'spr'
  */
  SeqPulsarReph(const SeqPulsarReph& spr);

/**
  * Construct an empty rephasing gradient pulse with the given label
  */
  SeqPulsarReph(const STD_string& object_label = "unnamedSeqPulsarReph");

/**
  * Destructor
  */
  ~SeqPulsarReph();

/**
  * Assignment operator that makes this object become a copy of 'spr'
  */
  SeqPulsarReph& operator = (const SeqPulsarReph& spr);


/**
  * Returns the duration of the ramp that switches the gradient pulse on
  */
  float get_onramp_duration() const;

/**
  * Returns the duration of the rephasing part of the gradient pulse
  */
  float get_constgrad_duration() const;

/**
  * Returns the duration of the ramp that switches the gradient pulse on
  */
  float get_offramp_duration() const;


 private:
  void build_seq();

  unsigned int dim;

  SeqGradTrapez gxpulse;
  SeqGradTrapez gypulse;
  SeqGradTrapez gzpulse;

};



////////////////////////////////////////////////////////////////////////////////

/**
  *
  * \brief Sinc pulse
  *
  * A sinc shaped pulse
  *
  */
class SeqPulsarSinc : public SeqPulsar {

 public:
/**
  * Constructs a slice selective sinc pulse labeled 'object_label' with the following properties:
  * - slicethickness: The thickness of the slice that will be excited
  * - rephased:      If true, an additional gradient lobe will be added to rephase the magnetisation
  * - duration:       The pulse duration
  * - flipangle:      The flip angle
  * - resolution:     The spatial resolution, i.e. the scale on which the excitation profile is smoothed
  * - npoints:        The number of digitised (complex) points for the pulse
  */
  SeqPulsarSinc(const STD_string& object_label="unnamedSeqPulsarSinc", float slicethickness=5.0, bool rephased=true, float duration=2.0, float flipangle=90.0, float resolution=1.5, unsigned int npoints=256);

/**
  * Constructs a copy of 'sps'
  */
  SeqPulsarSinc(const SeqPulsarSinc& sps);

/**
  * Assignment operator that makes this object become a copy of 'sps'
  */
  SeqPulsarSinc& operator = (const SeqPulsarSinc& sps);

};


////////////////////////////////////////////////////////////////////////////////

/**
  *
  * \brief Gauss pulse
  *
  * A Gaussian shaped pulse
  *
  */
class SeqPulsarGauss : public SeqPulsar {

 public:
/**
  * Constructs a slice selective Gauss pulse labeled 'object_label' with the following properties:
  * - slicethickness: The thickness of the slice (FWHM) that will be excited
  * - rephased:      If true, an additional gradient lobe will be added to rephase the magnetisation
  * - duration:       The pulse duration
  * - flipangle:      The flip angle
  * - npoints:        The number of digitised (complex) points for the pulse
  */
  SeqPulsarGauss(const STD_string& object_label="unnamedSeqPulsarGauss", float slicethickness=5.0, bool rephased=true, float duration=1.0, float flipangle=90.0, unsigned int npoints=128);

/**
  * Constructs a copy of 'spg'
  */
  SeqPulsarGauss(const SeqPulsarGauss& spg);

/**
  * Assignment operator that makes this object become a copy of 'spg'
  */
  SeqPulsarGauss& operator = (const SeqPulsarGauss& spg);

};


////////////////////////////////////////////////////////////////////////////////

/**
  *
  * \brief Const pulse
  *
  * A block shaped shaped pulse
  *
  */
class SeqPulsarBP : public SeqPulsar {

 public:
/**
  * Constructs a block shaped pulse labeled 'object_label' with the following properties:
  * - duration:       The pulse duration
  * - flipangle:      The flip angle
  */
  SeqPulsarBP(const STD_string& object_label="unnamedSeqPulsarBP", float duration=1.0, float flipangle=90.0, const STD_string& nucleus="");

/**
  * Constructs a copy of 'spb'
  */
  SeqPulsarBP(const SeqPulsarBP& spb);

/**
  * Assignment operator that makes this object become a copy of 'spb'
  */
  SeqPulsarBP& operator = (const SeqPulsarBP& spb);

};




/** @}
  */

#endif
