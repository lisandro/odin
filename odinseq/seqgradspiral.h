/***************************************************************************
                          seqgradspiral.h  -  description
                             -------------------
    begin                : Tue Aug 13 2002
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQGRADSPIRAL_H
#define SEQGRADSPIRAL_H

#include <tjutils/tjnumeric.h>

#include <odinseq/odinpulse.h>
#include <odinseq/seqgradchan.h>
#include <odinseq/seqgradwave.h>
#include <odinseq/seqgradconst.h>
#include <odinseq/seqgradchanparallel.h>


/**
  * @addtogroup odinseq_internals
  * @{
  */

/**
  * Spiral Gradients
  */
class SeqGradSpiral: public SeqGradChanParallel, public MinimizationFunction {

 public:
/**
  * Constructs a spiral gradient labeled 'object_label' and the
  * following properties:
  * - traj:        The spiral trajectory (will be modified, if neccessary)
  * - dt:          The dwell time
  * - resolution:  Spatial resolution
  * - sizeRadial:  The number of image points in radial direction
  * - numofSegments : The number of spiral interleaves
  * - inwards:     Reverse trajectory so that the spiral ends in the center of k-space, in addition, rotates trajectory by 180 deg
  * - optimize:    Optimize trajectory for minimum readout length if trajectory has a free tunable parameter
  * - nucleus:     The imaging nucleus
  */
  SeqGradSpiral(const STD_string& object_label, LDRtrajectory& traj, double dt, float resolution, unsigned int sizeRadial, unsigned int numofSegments, bool inwards=false, bool optimize=false, const STD_string& nucleus="");

/**
  * Constructs an empty spiral gradient with the given label.
  */
  SeqGradSpiral(const STD_string& object_label = "unnamedSeqGradSpiral" );

/**
  * Constructs a spiral gradient which is a copy of 'sgs'
  */
  SeqGradSpiral(const SeqGradSpiral& sgs);

/**
  * This assignment operator will make this object become an exact copy of 'sgs'.
  */
  SeqGradSpiral& operator = (const SeqGradSpiral& sgs);


/**
  * Returns the size of the spiral without ramps
  */
  unsigned int spiral_size() const {return denscomp.size();}

/**
  * Returns the duration of the ramp that switches the gradients off
  */
  double get_ramp_duration() const {return (get_gradduration()-spiral_dur);}


/**
  * Sets the delay of the gradient delays preceding the spiral gradients
  */
  SeqGradSpiral& set_predelay_duration(double dur);

/**
  * Returns the Jacobian determinante, i.e. the correction function
  * for non-uniform weighting of k-space
  */
  const fvector& get_denscomp() const {return denscomp;}

/**
  * Returns the k-space trajectory for the interleave 'iseg' and the given channel
  */
  fvector get_ktraj(direction channel) const;


 private:
  void common_init() {traj_cache=0;}
  void build_seq();

  float readout_npts() const;

  // implement virtual functions of MinimizationFunction to minimize readout duration
  float evaluate(const fvector& spirpar) const;
  unsigned int numof_fitpars() const {return 1;}
  
  SeqGradWave gx;
  SeqGradWave gy;

  SeqGradDelay gxdelay;
  SeqGradDelay gydelay;

  fvector kx;
  fvector ky;

  fvector denscomp;

  double spiral_dur;
  double predelay;

  // cache values for readout_npts/evaluate
  LDRtrajectory* traj_cache;
  double dt_cache;
  float resolution_cache;
  unsigned int sizeRadial_cache;
  float gamma_cache;

};

//////////////////////////////////////////////////////////////////////////////////////////



/** @}
  */

#endif
