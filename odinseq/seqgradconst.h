/***************************************************************************
                          seqgradconst.h  -  description
                             -------------------
    begin                : Tue Aug 13 2002
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQGRADCONST_H
#define SEQGRADCONST_H

#include <odinseq/seqgradchan.h>

/**
  * @addtogroup odinseq
  * @{
  */

/**
  * \brief  Constant gradient
  *
  * This class represents a gradient object with a constant gradient field
  */
class SeqGradConst : public SeqGradChan {

 public:

/**
  * Constructs a constant gradient field labeled 'object_label' with the following properties:
  * - gradchannel:   The channel this object should be played out
  * - gradstrength:  The gradient strength for this object
  * - gradduration:  The duration of this gradient object
  */
  SeqGradConst(const STD_string& object_label,direction gradchannel,
                            float gradstrength, double gradduration);

/**
  * Constructs a copy of 'sgc'
  */
  SeqGradConst(const SeqGradConst& sgc);

/**
  * Construct an empty gradient object with the given label
  */
  SeqGradConst(const STD_string& object_label = "unnamedSeqGradConst" );

/**
  * Assignment operator that makes this gradient channel object become a copy of 'sgc'
  */
  SeqGradConst& operator = (const SeqGradConst& sgc);

 private:
  friend class SeqGradVector;

  // overwriting virtual functions from SeqClass
  bool prep();


  // overwriting virtual functions from SeqGradChan
  SeqGradChan& get_subchan(double starttime, double endtime) const;
  STD_string get_grdpart(float matrixfactor) const;
  float get_integral() const {return get_strength() * get_duration();}


};

//////////////////////////////////////////////////////////////////////////////////


/**
  * \brief  Gradient delay
  *
  * This class represents a gradient delay which can be inserted into a list of gradient objects
  */
class SeqGradDelay : public SeqGradChan {

 public:

/**
  * Constructs a constant gradient field labeled 'object_label' with the following properties:
  * - gradchannel:   The channel this object should be played out
  * - gradduration:  The duration of this gradient object
  */
  SeqGradDelay(const STD_string& object_label, direction gradchannel, double gradduration);

/**
  * Constructs a copy of 'sgd'
  */
  SeqGradDelay(const SeqGradDelay& sgd);

/**
  * Construct an empty gradient object with the given label
  */
  SeqGradDelay(const STD_string& object_label = "unnamedSeqGradDelay" );

/**
  * Assignment operator that makes this gradient channel object become a copy of 'sgd'
  */
  SeqGradDelay& operator = (const SeqGradDelay& sgd);


  // overwriting virtual functions from SeqGradInterface
  float get_strength() const {return 0.0;} // always return zero strength


 private:
  // overwriting virtual functions from SeqGradChan
  SeqGradChan& get_subchan(double starttime, double endtime) const;
  STD_string get_grdpart(float matrixfactor) const;
  float get_integral() const {return 0.0;}

};


/** @}
  */

#endif
