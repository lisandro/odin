/***************************************************************************
                          seqdriver.h  -  description
                             -------------------
    begin                : Sat Apr 3 2004
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQDRIVER_H
#define SEQDRIVER_H


#include <odinseq/seqclass.h>
#include <odinseq/seqplatform.h>

class SeqDriverBase : public virtual SeqClass {

 public:
  SeqDriverBase() {}
  virtual ~SeqDriverBase() {}
  virtual odinPlatform get_driverplatform() const = 0;
};


/////////////////////////////////////////////////////////////////


/**
  * @ingroup odinseq_internals
  *  The interface class for all driver (platform specific) classes
  */
template<class D>
class SeqDriverInterface : public SeqClass {
 public:
  typedef D* (*create_driver_callback)();

  SeqDriverInterface(const STD_string& driverlabel="unnamedSeqDriverInterface")
   : current_driver(0) {
    set_label(driverlabel);
  }

  virtual ~SeqDriverInterface(){
    if(current_driver) delete current_driver;
  }

  SeqDriverInterface& operator = (const SeqDriverInterface& di) {
    SeqClass::operator = (di);
    if(current_driver) delete current_driver;
    current_driver=0;
    if(di.current_driver) current_driver=di.current_driver->clone_driver();
    return *this;
  }
  
  D& operator *  () {return *get_driver();}
  D* operator -> () {return get_driver();}

  
 private:

  // overwriting virtual functions from SeqClass
  bool prep() {return bool(get_driver());} // make sure correct driver is loaded

  D* get_driver() {
    odinPlatform current_pf=SeqPlatformProxy::get_current_platform();
    if(!current_driver) allocate_driver();
    else {
      odinPlatform driver_platform=current_driver->get_driverplatform();
      if(driver_platform!=current_pf) {
        delete current_driver;
        allocate_driver();
      }
    }
    if(!current_driver) {
      STD_cerr << "ERROR: " << get_label() << ": Driver missing for platform " << SeqPlatformProxy::get_platform_str(current_pf) << STD_endl;
    }
    if(current_driver->get_driverplatform()!=current_pf) {
      STD_string drvplat(SeqPlatformProxy::get_possible_platforms()[current_driver->get_driverplatform()]);
      STD_cerr << "ERROR: " << get_label() << ": Driver has wrong platform signature " << drvplat << ", but expected " << SeqPlatformProxy::get_platform_str(current_pf) << STD_endl;
    }
    return current_driver;
  }

  void allocate_driver() {
    current_driver=pfinterface->create_driver(current_driver);
    if(current_driver) current_driver->set_label(get_label());
  }

  
  SeqPlatformProxy pfinterface;
  D* current_driver;
};

#endif
