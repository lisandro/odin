#include "seqcmdline.h"
#include "seqmeth.h"
#include "seqplatform.h"

#include <tjutils/tjprofiler.h>

#ifndef NO_CMDLINE
class SeqTreeCallbackConsole : public SeqTreeCallbackAbstract {

 public:
  SeqTreeCallbackConsole() {}

 private:

  // implementing virtual function of SeqTreeCallbackAbstract
  void display_node(const SeqClass* thisnode, const SeqClass* parentnode, int treelevel, const svector& columntext) {
    STD_string space = "";
    for (int i = 0; i<(treelevel-1); i++) space+="|  ";
    if(treelevel>0) space+="|- ";
    STD_cout << space;
    for(unsigned int icol=0; icol<columntext.size(); icol++) STD_cout << columntext[icol] << " \t";
    STD_cout << STD_endl;
  }

};
#endif


//////////////////////////////////////////////////////////////////


int SeqCmdLine::process(int argc, char *argv[]) {
#ifndef NO_CMDLINE
  Log<Seq> odinlog("SeqCmdLine","process");

  SeqMethodProxy method;
  SeqPlatformProxy platform;
  char value[ODIN_MAXCHAR];
  bool valid_command=false;

// try to find a suitable platform to switch to
#ifdef STANDALONE_PLUGIN
  platform.set_current_platform(standalone); // default
#else
#ifdef PARAVISION_PLUGIN
  platform.set_current_platform(paravision);
#else
#ifdef IDEA_PLUGIN
  platform.set_current_platform(numaris_4);
#else
#ifdef EPIC_PLUGIN
  platform.set_current_platform(epic);
#else
#error No valid platform plugin available
#endif
#endif
#endif
#endif

  LDRfileName methlabel(argv[0]);
  
  // do we have at least one action argument ?
  if (argc<2) {
    STD_cout << usage(methlabel.get_basename(),method->get_description()) << STD_endl;
    return 0;
  }

  // switch on error tracing
//  set_tracing(Error);


  // setting up stuff for different test cases of seqtest
  // do this before anything else
  if(getCommandlineOption(argc,argv,"-testcase",value,ODIN_MAXCHAR)) {
    int testcase=atoi(value);
    ODINLOG(odinlog,normalDebug) << "testcase=" << testcase << STD_endl;
    method->set_current_testcase(testcase);
    if(testcase) {
      STD_string newlabel(method->get_label());
      if(newlabel.length()) newlabel[newlabel.length()-1]=char('0'+testcase);
      method->set_label(newlabel);
    }
  }


  if(!method->init()) {
    ODINLOG(odinlog,errorLog) << "method->init() failed" << STD_endl;
    return -1;
  }

  STD_string action(argv[1]);

  // switch to platform which offers the requested action
  int pfact=platform.get_platform_for_action(action);
  ODINLOG(odinlog,normalDebug) << "action/pfact=" << action << "/" << pfact << STD_endl;
  if(pfact>=0) platform.set_current_platform(odinPlatform(pfact));
  else platform.set_current_platform(standalone); // used as default for non-platform-specific actions

  STD_string scandir=".";
  if(getCommandlineOption(argc,argv,"-scandir",value,ODIN_MAXCHAR)) {
   scandir=value;
   ODINLOG(odinlog,normalDebug) << "scandir=" << scandir << STD_endl;
  }
  SystemInterface()->set_scandir(scandir);



  // analyze command line options and perform the requested action:

  if(action=="description") {
    STD_cout << methlabel.get_basename()<< STD_endl << justificate(method->get_description()) << STD_endl;
    valid_command=true;
  }

  if(action=="ntests") {
    STD_cout << method->numof_testcases() << STD_endl;
    exit(0);
  }


  if(action=="events") {
    if(getCommandlineOption(argc,argv,"-p",value,ODIN_MAXCHAR)) method->load_protocol(value);
    if(method->prepare()) {
      eventContext context;
      context.action=printEvent;
      SeqTreeCallbackConsole display;
      context.event_display=&display;
      STD_cout << "---------- Events: -------------------" << STD_endl;
      method->event(context);
      STD_cout << STD_endl;
    } else {
      ODINLOG(odinlog,errorLog) << "method->prepare() failed" << STD_endl;
      return -1;
    }

    valid_command=true;
  }

  if(action=="tree") {
    if(getCommandlineOption(argc,argv,"-p",value,ODIN_MAXCHAR)) method->load_protocol(value);

    if(method->build()) {
      SeqTreeCallbackConsole display;
      method->tree(&display);
    } else {
      ODINLOG(odinlog,errorLog) << "method->build() failed" << STD_endl;
      return -1;
    }
    valid_command=true;
  }

  if(!valid_command) {
    ODINLOG(odinlog,normalDebug) << "handind over control to platform" << STD_endl;
    int result=platform->process(argc,argv);
    ODINLOG(odinlog,normalDebug) << "result=" << result << STD_endl;
    if(result<0) return -1;
    if(result>0) valid_command=true;
  }

  ODINLOG(odinlog,normalDebug) << "valid_command=" << valid_command << STD_endl;
  if(!valid_command) {
    LDRfileName methlabel(argv[0]);
    STD_cout << usage(methlabel.get_basename(),method->get_description()) << STD_endl;
    return -1;
  }

  // clean up
  SeqMethodProxy::delete_methods();

#endif
  return 0; // return 0 to shell on success
}


STD_string SeqCmdLine::usage(const STD_string& meth, const STD_string& description) {
  STD_string result;
#ifndef NO_CMDLINE
  STD_string separator=n_times(" ",USAGE_INDENTION_FACTOR);

  result+="\nODIN method: "+meth+"\n\n";
  result+="DESCRIPTION:\n"+justificate(description,USAGE_INDENTION_FACTOR)+"\n\n";
  result+="USAGE:"+separator+meth+" action [options]"+"\n";
  result+=separator+"where 'action' can be one of the following:\n\n";

  SeqCmdlineActionList global_actions;

  SeqCmdlineAction descr("description","Prints a description of sequence.");
  global_actions.push_back(descr);

  SeqCmdlineAction ntests("ntests","Prints number of test cases. Exits immediately thereafter.");
  global_actions.push_back(ntests);

  SeqCmdlineAction events("events","Prints all events in the sequence. ");
  events.add_opt_arg("p","The file with the measurement protocol");
  global_actions.push_back(events);

  SeqCmdlineAction tree("tree","Prints the tree of sequence objects. ");
  tree.add_opt_arg("p","The file with the measurement protocol");
  global_actions.push_back(tree);
  
  result+="GLOBAL ACTIONS:\n\n";
  result+=format_actions(global_actions);

  result+=SeqPlatformProxy::get_platforms_usage();

  result+="ADDITIONAL OPTIONS:\n\n";
#ifdef ODIN_DEBUG
  result+=justificate(LogBase::get_usage(),USAGE_INDENTION_FACTOR)+"\n";
#endif
  result+=justificate("-scandir <Scan-Directory>:  Sets the directory where experimental data is stored.",USAGE_INDENTION_FACTOR)+"\n";

  result+=justificate("-testcase <TestCase-Index>:  Uses the default sequence parameters from the given test case.",USAGE_INDENTION_FACTOR)+"\n";


  result+="\n";
#endif
  return result;
}

STD_string SeqCmdLine::format_actions(const SeqCmdlineActionList& actions) {
  STD_string result;
#ifndef NO_CMDLINE
  STD_string separator=n_times(" ",USAGE_INDENTION_FACTOR);

  STD_string argline;
  STD_map<STD_string,STD_string>::const_iterator argit;
  for(STD_list<SeqCmdlineAction>::const_iterator it=actions.begin(); it!=actions.end(); ++it) {
    result+=separator+it->action+"\n";
    result+=justificate(it->description,USAGE_INDENTION_FACTOR);
    if(it->req_args.size()) result+=separator+"Required arguments:\n";
    for(argit=it->req_args.begin(); argit!=it->req_args.end(); ++argit) {
      argline=separator+"-"+argit->first+" <"+argit->second+">";
      result+=separator+justificate(argline,USAGE_INDENTION_FACTOR,true);
    }
    if(it->opt_args.size()) result+=separator+"Optional arguments:\n";
    for(argit=it->opt_args.begin(); argit!=it->opt_args.end(); ++argit) {
      argline=separator+"-"+argit->first+" <"+argit->second+">\n";
      result+=separator+justificate(argline,USAGE_INDENTION_FACTOR,true);
    }
    result+="\n";
  }
#endif
  return result;
}




