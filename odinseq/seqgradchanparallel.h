/***************************************************************************
                          seqgradchanparallel.h  -  description
                             -------------------
    begin                : Thu Apr 22 2004
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQGRADCHANPARALLEL_H
#define SEQGRADCHANPARALLEL_H


#include <odinseq/seqgradchanlist.h>
#include <odinseq/seqgradobj.h>
#include <odinseq/seqdriver.h>


////////////////////////////////////////////////////////////////////


/**
  * @ingroup odinseq_internals
  * The base class for platform specific drivers of lists of simultaneous gradient objects
  */
class SeqGradChanParallelDriver : public SeqDriverBase {

 public:
  SeqGradChanParallelDriver() {}
  virtual ~SeqGradChanParallelDriver() {}

  virtual bool prep_driver(SeqGradChanList* chanlists[3]) const = 0;

  virtual STD_string get_program(programContext& context) const = 0;

  virtual SeqGradChanParallelDriver* clone_driver() const = 0;
};


////////////////////////////////////////////////////////////////////



/**
  * @addtogroup odinseq_internals
  * @{
  */

/**
  * This class is a container for three SeqGradChanList's
  */
class SeqGradChanParallel : public SeqGradObjInterface {

 public:

/**
  * Construct an empty gradient channel list with the given label
  */
  SeqGradChanParallel(const STD_string& object_label = "unnamedSeqGradChanParallel" );

/**
  * Constructs a copy of 'sgcp'
  */
  SeqGradChanParallel(const SeqGradChanParallel& sgcp);

/**
  * Destructor
  */
  ~SeqGradChanParallel();

/**
  * Assignment operator that makes this gradient sequence object become a copy of 'sgcp'
  */
  SeqGradChanParallel& operator = (const SeqGradChanParallel& sgcp);

/**
  * Uses 'sgc' for the corresponding channel
  */
  SeqGradChanParallel& operator /= (SeqGradChan& sgc);

/**
  * Uses 'sgcl' for the corresponding channel
  */
  SeqGradChanParallel& operator /= (SeqGradChanList& sgcl);


  // overloading virtual function from SeqTreeObj
  STD_string get_program(programContext& context) const;
  STD_string get_properties() const;
  unsigned int event(eventContext& context) const;
  void query(queryContext& context) const;

  // overloading virtual function from SeqGradInterface
  SeqGradInterface& set_strength(float gradstrength);
  SeqGradInterface& invert_strength();
  float get_strength() const;
  fvector get_gradintegral() const;
  double get_gradduration() const;
  SeqGradInterface& set_gradrotmatrix(const RotMatrix& matrix);



/**
  * Appends 'sgc' to this
  */
  SeqGradChanParallel& operator += (SeqGradChan& sgc);


/**
  * Appends 'sgcl' to this
  */
  SeqGradChanParallel& operator += (SeqGradChanList& sgcl);

/**
  * Appends 'sgcp' to this
  */
  SeqGradChanParallel& operator += (SeqGradChanParallel& sgcp);


/**
  * Clear all sublists
  */
  void clear();


 private:
  friend class SeqOperator;
  friend class SeqGradChan;
  friend class SeqGradChanList;
  friend class SeqGradEPI;
  friend class SeqEpiKernel;

  // overwriting virtual functions from SeqClass
  bool prep();
  void clear_container();

  // overwriting virtual functions from SeqGradObjInterface
  bool need_gp_terminator() const {return true;}

  void padd_channel_with_delay(direction chanNo, double maxdur);

  SeqGradChanList* get_gradchan(direction channel) const;
  SeqGradChanParallel& set_gradchan(direction channel, SeqGradChanList* sgcl);

  mutable SeqDriverInterface<SeqGradChanParallelDriver> paralleldriver;

  Handler<SeqGradChanList* >  gradchan[3];
};


/** @}
  */

#endif
