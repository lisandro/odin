#include "seqtemplate.h"


#include <odinseq/seqpulsar.h>
#include <odinseq/seqacqepi.h>
#include <odinseq/seqacqdeph.h>
#include <odinseq/seqgradpulse.h>
#include <odinseq/seqgradphase.h>
#include <odinseq/seqloop.h>
#include <odinseq/seqdelay.h>



struct SeqFieldMapPars {
  LDRblock parblock;
  LDRint NumOfEchoes;
  LDRfloat Resolution;
  LDRdouble T1Ernst;
  LDRint DummyCycles;
  LDRint ReadSize;
  LDRint PhaseSize;
  LDRint SliceSize;
  LDRdouble FlashFlipAngle;
  LDRdouble ExtraDelay;
};

struct SeqFieldMapObjects {
  SeqFieldMapObjects(const STD_string& objlabel) :
    exc(objlabel+"_exc"),
    epi(objlabel+"_epi"),
    deph(objlabel+"_deph"),
    pe3d(objlabel+"_pe3d"),
    crusher(objlabel+"_crusher"),
    extradelay(objlabel+"_extradelay"),
    pepart(objlabel+"_pepart"),
    peloop(objlabel+"_peloop"),
    peloop3d(objlabel+"_peloop3d"),
    sliceloop(objlabel+"_sliceloop"),
    acqdummy(objlabel+"_acqdummy"),
    dummyloop(objlabel+"_dummyloop") {}
  SeqPulsar exc;
  SeqAcqEPI epi;
  SeqAcqDeph deph;
  SeqGradPhaseEnc pe3d;
  SeqGradConstPulse crusher;
  SeqDelay   extradelay;
  SeqObjList pepart;
  SeqObjLoop peloop;
  SeqObjLoop peloop3d;
  SeqObjLoop sliceloop;
  SeqDelay   acqdummy;
  SeqObjLoop dummyloop;
};


void SeqFieldMap::alloc_data(const STD_string& objlabel) {
  if(!pars) pars=new SeqFieldMapPars;
  if(!objs) objs=new SeqFieldMapObjects(objlabel);
}


SeqFieldMap::~SeqFieldMap() {
  if(pars) delete pars;
  if(objs) delete objs;
}


void SeqFieldMap::init(const STD_string& objlabel) {
  alloc_data(objlabel);

  set_label(objlabel);

  pars->parblock.set_embedded(false).set_label(objlabel+"_parblock");

  pars->parblock.clear();

  pars->NumOfEchoes.set_description("Number of ecoes for fieldmap calculation").set_label("NumOfEchoes");
  pars->NumOfEchoes=8;
  pars->parblock.append(pars->NumOfEchoes);

  pars->Resolution.set_description("Spatial in-plane resolution").set_unit(ODIN_SPAT_UNIT).set_label("Resolution");
  pars->Resolution=3.0;
  pars->parblock.append(pars->Resolution);

  pars->T1Ernst.set_description("For optimum SNR, the flip angle will be set to the Ernst angle using this T1").set_unit(ODIN_TIME_UNIT).set_label("T1Ernst");
  pars->T1Ernst=1300.0;
  pars->parblock.append(pars->T1Ernst);

  pars->DummyCycles.set_description("Number of dummy repetions").set_label("DummyCycles");
  pars->DummyCycles=3;
  pars->parblock.append(pars->DummyCycles);

  pars->ExtraDelay.set_description("Extra TR delay").set_unit(ODIN_TIME_UNIT).set_label("ExtraDelay");
  pars->parblock.append(pars->ExtraDelay);


  pars->FlashFlipAngle.set_description("Flip-angle of excitation pulse").set_parmode(noedit).set_label("FlashFlipAngle");
  pars->parblock.append(pars->FlashFlipAngle);

  pars->ReadSize.set_description("Size in read direction").set_parmode(noedit).set_label("ReadSize");
  pars->parblock.append(pars->ReadSize);

  pars->PhaseSize.set_description("Size in phase direction").set_parmode(noedit).set_label("PhaseSize");
  pars->parblock.append(pars->PhaseSize);

  pars->SliceSize.set_description("Size in slice direction").set_parmode(noedit).set_label("SliceSize");
  pars->parblock.append(pars->SliceSize);

}



void SeqFieldMap::build_seq(double sweepwidth, float os_factor, const SeqObjList& prep, double min_relaxdelay) {
  alloc_data(get_label());

  STD_string objlabel(get_label());

  if(geometryInfo->get_Mode()==voxel_3d) {

    // Same settings as in swi sequence
    float spatres=3.0;
    float slicethick=geometryInfo->get_FOV(sliceDirection)-2.0*spatres;
    if(slicethick<spatres) slicethick=spatres;
    objs->exc=SeqPulsarSinc(objlabel+"_exc", slicethick, true, 4.0, 90.0, spatres, 512); // flip angle will be adjusted later
    objs->exc.set_filter("Gauss");
    objs->exc.set_freqoffset(systemInfo->get_gamma() * objs->exc.get_strength() / (2.0*PII) *  geometryInfo->get_offset(sliceDirection) );
  } else {

    objs->exc=SeqPulsarSinc(objlabel+"_exc",geometryInfo->get_sliceThickness());
    objs->exc.set_freqlist( systemInfo->get_gamma() * objs->exc.get_strength() / (2.0*PII) *  geometryInfo->get_sliceOffsetVector() );
  }
  objs->exc.set_pulse_type(excitation);


  pars->ReadSize= int(secureDivision(geometryInfo->get_FOV(readDirection) ,pars->Resolution)+0.5);
  pars->PhaseSize=int(secureDivision(geometryInfo->get_FOV(phaseDirection),pars->Resolution)+0.5);
  if(geometryInfo->get_Mode()==voxel_3d) {
    pars->SliceSize=int(secureDivision(geometryInfo->get_FOV(sliceDirection),pars->Resolution)+0.5);
  } else {
    pars->SliceSize=1;
  }

  if(pars->NumOfEchoes%2) pars->NumOfEchoes++;

  objs->epi=SeqAcqEPI(objlabel+"_epi",sweepwidth,
                pars->ReadSize,geometryInfo->get_FOV(readDirection),
                pars->PhaseSize,geometryInfo->get_FOV(phaseDirection),
                pars->PhaseSize,1,os_factor,"",0,0,linear,false,1.0,0.0,pars->NumOfEchoes/2);

  objs->epi.set_template_type(fieldmap_template);

  objs->deph=SeqAcqDeph(objlabel+"_deph",objs->epi,FID); // Does the phase encoding in one dimension

  objs->pepart=SeqObjList(objlabel+"_pepart");
  if(geometryInfo->get_Mode()==voxel_3d) {
    objs->pe3d=SeqGradPhaseEnc(objlabel+"_pe3d",pars->SliceSize,geometryInfo->get_FOV(sliceDirection),sliceDirection,0.25*systemInfo->get_max_grad());
    objs->pepart=objs->deph/objs->pe3d;
  } else {
    objs->pepart=objs->deph;
  }

  double crusher_strength=0.4*systemInfo->get_max_grad();
  double crusher_integral=2.0*fabs(objs->deph.get_gradintegral().sum());
  double crusher_dur=secureDivision(crusher_integral,crusher_strength);
  objs->crusher=SeqGradConstPulse(objlabel+"_crusher",readDirection,crusher_strength,crusher_dur);

  pars->ExtraDelay=STD_max(double(pars->ExtraDelay),min_relaxdelay);

  objs->extradelay=SeqDelay(objlabel+"_extradelay",pars->ExtraDelay);

  double readoutdur = objs->pepart.get_duration() + objs->epi.get_duration();
  double kerneldur = prep.get_duration()+objs->exc.get_duration()+readoutdur+objs->crusher.get_duration()+objs->extradelay.get_duration();

  float TR=double(geometryInfo->get_nSlices())*kerneldur;
  pars->FlashFlipAngle=180.0/PII * acos( exp ( -secureDivision ( TR, pars->T1Ernst) ) );
  objs->exc.set_flipangle( pars->FlashFlipAngle );


  objs->acqdummy=SeqDelay(objlabel+"_acqdummy",readoutdur);

  objs->peloop=SeqObjLoop(objlabel+"_peloop");
  objs->peloop3d=SeqObjLoop(objlabel+"_peloop3d");
  objs->sliceloop=SeqObjLoop(objlabel+"_sliceloop");
  objs->dummyloop=SeqObjLoop(objlabel+"_dummyloop");


  SeqObjList::clear();

  if(pars->DummyCycles>0) {
    (*this)+= (objs->dummyloop) (
                  (objs->sliceloop) (
                    prep + objs->exc + objs->acqdummy + objs->crusher + objs->extradelay
                  )[objs->exc]
              )[pars->DummyCycles];
  }

  if(geometryInfo->get_Mode()==voxel_3d) {

    (*this)+= (objs->peloop3d) (
                (objs->peloop) (
                  prep + objs->exc + objs->pepart + objs->epi + objs->crusher + objs->extradelay
                )[objs->deph.get_epi_segment_vector()]
              )[objs->pe3d];

    objs->epi.set_reco_vector(line3d,objs->pe3d);

  } else {

    (*this)+= (objs->peloop) (
                (objs->sliceloop) (
                  prep + objs->exc + objs->pepart + objs->epi + objs->crusher + objs->extradelay
                )[objs->exc]
              )[objs->deph.get_epi_segment_vector()];

    objs->epi.set_reco_vector(slice,objs->exc);
  }

//  objs->epi.set_te_offset(objs->exc.get_duration()-objs->exc.get_magnetic_center()+objs->pepart.get_duration());

}


LDRblock& SeqFieldMap::get_parblock() {
  alloc_data(get_label());
  return pars->parblock;
}
