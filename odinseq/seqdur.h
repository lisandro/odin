/***************************************************************************
                          seqdur.h  -  description
                             -------------------
    begin                : Wed Aug 14 2002
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/



#ifndef SEQDUR_H
#define SEQDUR_H

#include <odinseq/seqtree.h>

/**
  * @addtogroup odinseq_internals
  * @{
  */


/**
  * This is the base class for all objects which have a certain
  * duration. This is one of the base classes for all 'sequence atoms'
  * i.e. those classes which cannot be devided further (e.g. SeqPuls, SeqAcq, SeqDelay, ...).
  * In contrast, the duration of composite objects (e.g. SeqObjList)
  * is calculated from their elements.
  */
class SeqDur : public virtual SeqTreeObj {


 public:

/**
  * Construct a duration object with the given 'object_label' and the specified 'duration'
  */
  SeqDur(const STD_string& object_label, float duration);


/**
  * Construct an empty duration object with the given label
  */
  SeqDur(const STD_string& object_label="unnamedSeqDur");


/**
  * Construct a duration object which is a copy of 'sd'
  */
  SeqDur(const SeqDur& sd);


/**
  * Sets the duration
  */
  SeqDur& set_duration(float duration);


  // implementing virtual functions of SeqTreeObj
  double get_duration() const;


/**
  * Assignment operator that makes this duration object become a copy of 'sd'
  */
  SeqDur& operator = (const SeqDur& sd);


 private:
  double dur;

};


/** @}
  */

#endif
