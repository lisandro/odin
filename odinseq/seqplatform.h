/***************************************************************************
                          seqplatform.h  -  description
                             -------------------
    begin                : Sat Apr 3 2004
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQPLATFORM_H
#define SEQPLATFORM_H

#include <tjutils/tjhandler.h>

#include <odinseq/seqtree.h>
#include <odinseq/seqplot.h>
#include <odinseq/seqcmdline.h>

// Global Bruker stuff
#define REFGAIN_FILE "odin_refgain"
#define MAINNUC_FILE "odin_mainnuc"



// forward declarations for hardware drivers
class SeqAcqDriver;
class SeqEpiDriver;
class SeqDecouplingDriver;
class SeqDelayDriver;
class SeqDelayVecDriver;
class SeqFreqChanDriver;
class SeqGradChanDriver;
class SeqGradChanParallelDriver;
class SeqGradTrapezDriver;
class SeqListDriver;
class SeqCounterDriver;
class SeqParallelDriver;
class SeqPhaseDriver;
class SeqPulsDriver;
class SeqTriggerDriver;


class eventContext; // forward declaration

class ProgressMeter; // forward declaration

/////////////////////////////////////////////////////////

/**
  * @ingroup odinseq_internals
  * Enum to specify data processing on the platform:
  * - odinReco: Store raw data on disk and call odinreco
  * - rawData:  Store raw data on disk
  * - recoInfoOnly: Story only recoInfo file. Raw data has to be copied manually using Twix
  */
enum recoMode {odinReco=0, rawData, recoInfoOnly};

/////////////////////////////////////////////////////////

/**
  * @ingroup odinseq_internals
  *  This class is used only for resolving ambiguities in constructors
  *  of other classes during platform registration
  */
class PlatformRegistration {};


/////////////////////////////////////////////////////////

/**
  * @ingroup odinseq_internals
  *  Virtual base class for all classes that represent an ODIN platform,
  * i.e. that deal with platform specific peculiarities
  */
class SeqPlatform : public virtual SeqClass {

 public:
  enum eventLogging {noLogging=0,loggEvent};

/**
  * Initialize platform, is called whenever a method is initialized.
  */
  virtual void init() = 0;

/**
  * This function is overloaded to clean up the platform interface before a new sequence is prepared.
  */
  virtual void reset_before_prep() = 0;

/**
  * This function is overloaded to prepare all the global stuff which is needed to start an experiment.
  * 'nacqs_total' must contain the total number of acqusition events in the the sequence.
  */
  virtual void prepare_measurement(unsigned int nacqs_total) = 0;


  // functions for driver factory
  virtual SeqAcqDriver*        create_driver(SeqAcqDriver*) const = 0;
  virtual SeqEpiDriver*        create_driver(SeqEpiDriver*) const = 0;
  virtual SeqDecouplingDriver* create_driver(SeqDecouplingDriver*) const = 0;
  virtual SeqDelayDriver*      create_driver(SeqDelayDriver*) const = 0;
  virtual SeqDelayVecDriver*   create_driver(SeqDelayVecDriver*) const = 0;
  virtual SeqFreqChanDriver*   create_driver(SeqFreqChanDriver*) const = 0;
  virtual SeqGradChanDriver*   create_driver(SeqGradChanDriver*) const = 0;
  virtual SeqGradChanParallelDriver* create_driver(SeqGradChanParallelDriver*) const = 0;
  virtual SeqGradTrapezDriver* create_driver(SeqGradTrapezDriver*) const = 0;
  virtual SeqListDriver*       create_driver(SeqListDriver*) const = 0;
  virtual SeqCounterDriver*    create_driver(SeqCounterDriver*) const = 0;
  virtual SeqParallelDriver*   create_driver(SeqParallelDriver*) const = 0;
  virtual SeqPhaseDriver*      create_driver(SeqPhaseDriver*) const = 0;
  virtual SeqPulsDriver*       create_driver(SeqPulsDriver*) const = 0;
  virtual SeqTriggerDriver*    create_driver(SeqTriggerDriver*) const = 0;

  virtual int process(int argc, char *argv[]) = 0;
  virtual SeqCmdlineActionList get_actions_usage() const = 0;

  virtual void pre_event (eventContext& context) const = 0;
  virtual void post_event(eventContext& context) const = 0;

  virtual fvector get_acq_channel_scale_factors() const = 0;

  virtual STD_string get_program(programContext& context) const = 0;

  virtual STD_string get_rawfile() const = 0;
  virtual STD_string get_rawdatatype() const = 0;
  virtual unsigned int get_rawheader_size() const = 0;
  virtual STD_string get_image_proc() const = 0;
  virtual bool create_recoInfo() const = 0;

  virtual int write_rf_waveform (const STD_string& filename, const cvector& waveform) const = 0;
  virtual int load_rf_waveform (const STD_string& filename, cvector& result) const = 0;

  virtual int get_max_methodname_length() const = 0;

  virtual void set_eventlogging(eventLogging loggflag) = 0;

  // Paravision specific
  virtual bool pv_pilot(ProgressMeter* progmeter) {return true;}
  virtual STD_string pv_pilot_scan() const {return "";}
  virtual bool pv_gop(bool autorg, ProgressMeter* progmeter) {return true;}
  virtual bool pv_stop() {return true;}

  // IDEA specific
  virtual void set_idea_pars(void* pMrProt,void* pSeqLim,void* pSeqExpo, recoMode reco_mode) {}

  // StandAlone specific
  virtual SeqPlotDataAbstract* get_plot_data() {return 0;}
  virtual bool create_plot_events(ProgressMeter* progmeter) {return true;}

  virtual ~SeqPlatform() {}

 protected:
  SeqPlatform() {}
};


/////////////////////////////////////////////////////////


/**
  * @ingroup odinseq_internals
  *  Singleton to hold platform instances (driver factories)
  */
class SeqPlatformInstances : public SeqClass {

 public:
  SeqPlatformInstances();
  ~SeqPlatformInstances();

  bool set_current(odinPlatform pf);
  SeqPlatform* get_current() {return instance[get_current_platform_id()];}
  odinPlatform get_current_platform_id() {return SystemInterface::get_current_pf();}

 private:
  friend class SeqPlatformProxy;

  SeqPlatform* instance[numof_platforms];
  static odinPlatform pf_during_platform_construction;
};


////////////////////////////////////////////////////////////////////


/**
  * @ingroup odinseq_internals
  *  The proxy class for platform specific classes
  */
class SeqPlatformProxy : public virtual SeqClass, public StaticHandler<SeqPlatformProxy> {
 public:

  SeqPlatformProxy()  {set_label("SeqPlatformProxy");}

  static void set_current_platform(odinPlatform pF);
  static odinPlatform get_current_platform();

  static int load_systemInfo(const STD_string& filename);

  static STD_string get_platform_str(odinPlatform pF);
  static svector get_possible_platforms();

  static STD_string get_platforms_usage();

  static int get_platform_for_action(const STD_string& action);


  SeqPlatform& operator *  () {return *get_platform_ptr();}
  SeqPlatform* operator -> () {return get_platform_ptr();}

  // functions to initialize/delete static members by the StaticHandler template class
  static void init_static();
  static void destroy_static();

 private:
  friend class SeqPlatform;

  // manually allocate of platform instances
  static void create_platform_instances();

  static SeqPlatform* get_platform_ptr();

  // manage platform instances (driver factories)
  static SingletonHandler<SeqPlatformInstances,false> platforms;
};




#endif
