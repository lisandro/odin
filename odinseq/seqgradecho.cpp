#include "seqgradecho.h"

void SeqGradEcho::common_init(const STD_string& objlabel) {
  SeqAcqInterface::set_marshall(&acqread);
  SeqFreqChanInterface::set_marshall(&acqread);
  postexcpart.set_label(objlabel+"_postexcpart");
  postacqpart.set_label(objlabel+"_postacqpart");
  phasesim.set_label(objlabel+"_phasesim");
  phasesim3d.set_label(objlabel+"_phasesim3d");
  phasereordsim.set_label(objlabel+"_phasereordsim");
  midpart.set_label(objlabel+"_midpart");
  mode=slicepack;
  balanced_grads=false;
}


SeqGradEcho::SeqGradEcho(const STD_string& object_label, SeqPulsar& exc,
              double sweepwidth, unsigned int readnpts, float FOVread,
              unsigned int phasenpts, float FOVphase, encodingScheme scheme,
              reorderScheme reorder, unsigned int nsegments, unsigned int reduction, unsigned int acl_bands,
              bool balanced, float partial_fourier_phase, float partial_fourier_read, bool partial_fourier_read_at_end, float os_factor, const STD_string& nucleus) :
 SeqObjList(object_label),
 pls_reph(object_label+"_exc_reph",exc),
 acqread(object_label+"_acqread", sweepwidth, readnpts, FOVread, readDirection, os_factor, partial_fourier_read, partial_fourier_read_at_end, nucleus) {

  Log<Seq> odinlog(this,"SeqGradEcho");

  common_init(object_label);

  mode=slicepack;
  balanced_grads=balanced;

  pulsptr.set_handled(&exc);

  // use duration of on-ramp and constant part of pls_reph as duration of constant part of the other trapezoids
  double constdur=pls_reph.get_constgrad_duration()+pls_reph.get_onramp_duration();

  // use SeqGradPhaseEnc to initialize phase encoding
  SeqGradPhaseEnc pedummy(object_label+"_phase", phasenpts, FOVphase, constdur, phaseDirection,
                                 scheme, reorder, nsegments, reduction, acl_bands, partial_fourier_phase, nucleus);
  phase=pedummy.vectorgrad;

  if(balanced_grads) {
    phase_rew=phase;
    phase_rew.set_label("phase_rew");
    phase_rew.invert_strength();
  }

  // the same for read-dephase
  float readdeph_strengh=secureDivision(acqread.readdephgrad.get_integral(),constdur);
  readdeph=SeqGradConst(object_label+"_readdeph",acqread.read.get_channel(),readdeph_strengh,constdur);

  build_seq();
}



SeqGradEcho::SeqGradEcho(const STD_string& object_label,
              unsigned int readnpts, float FOVread, unsigned int phasenpts, float FOVphase, unsigned int slicenpts, float FOVslice,
              SeqPulsar& exc, double sweepwidth,
              unsigned int reduction, unsigned int acl_bands,
              bool balanced, float partial_fourier_phase, float partial_fourier_phase3d,
              float partial_fourier_read, bool partial_fourier_read_at_end, float os_factor, const STD_string& nucleus) :
 SeqObjList(object_label),
 pls_reph(object_label+"_exc_reph",exc),
 acqread(object_label+"_acqread", sweepwidth, readnpts, FOVread, readDirection, os_factor, partial_fourier_read, partial_fourier_read_at_end, nucleus) {

  Log<Seq> odinlog(this,"SeqGradEcho");

  common_init(object_label);

  mode=voxel_3d;
  balanced_grads=balanced;

  pulsptr.set_handled(&exc);

  // use duration of on-ramp and constant part of pls_reph as duration of constant part of the other trapezoids
  double constdur=pls_reph.get_constgrad_duration()+pls_reph.get_onramp_duration();

  // use SeqGradPhaseEnc to initialize phase encoding
  SeqGradPhaseEnc pedummy(object_label+"_phase", phasenpts, FOVphase, constdur, phaseDirection,
                                 linearEncoding, noReorder, 1, reduction, acl_bands, partial_fourier_phase, nucleus);
  phase=pedummy.vectorgrad;


  // use SeqGradPhaseEnc to initialize phase encoding
  SeqGradPhaseEnc pedummy3d(object_label+"_phase3d", slicenpts, FOVslice, constdur, sliceDirection,
                                 linearEncoding, noReorder, 1, reduction, acl_bands, partial_fourier_phase3d, nucleus);
//  phase3d=pedummy3d.vectorgrad;
  ODINLOG(odinlog,normalDebug) << "pedummy3d.get_trims()=" << pedummy3d.get_trims() << STD_endl;

  // Merge slice rephaser and 2nd phase encoding
  float integral_phase3d=pedummy3d.vectorgrad.get_strength()*pedummy3d.vectorgrad.get_gradduration();
  float integral_reph=pls_reph.get_gradintegral()[sliceDirection];
  fvector integrals_slice = pedummy3d.vectorgrad.get_trims()*integral_phase3d + integral_reph;
  ODINLOG(odinlog,normalDebug) << "integrals_slice=" << integrals_slice << STD_endl;
  float maxabsintegral_slice=integrals_slice.maxabs();
  float strength_slice=secureDivision(maxabsintegral_slice, constdur);
  fvector trims_slice(integrals_slice/maxabsintegral_slice);
  phase3d=SeqGradVector(object_label+"_phase3d", sliceDirection, strength_slice, trims_slice, constdur);

  // Copy indevec manually after merge
  ivector index3dvec=((const SeqVector&)pedummy3d).get_indexvec();
  ODINLOG(odinlog,normalDebug) << "index3dvec=" << index3dvec << STD_endl;
  phase3d.set_indexvec(index3dvec);


  if(balanced_grads) {
    phase_rew=phase;
    phase_rew.set_label("phase_rew");
    phase_rew.invert_strength();
    phase3d_rew=phase3d;
    phase3d_rew.set_label("phase3d_rew");
    phase3d_rew.invert_strength();
  }

  // the same for read-dephase
  float readdeph_strengh=secureDivision(acqread.readdephgrad.get_integral(),constdur);
  readdeph=SeqGradConst(object_label+"_readdeph",acqread.read.get_channel(),readdeph_strengh,constdur);

  build_seq();
}





SeqGradEcho::SeqGradEcho(const STD_string& object_label) :
            SeqObjList(object_label) {
  common_init(object_label);
}


SeqGradEcho::SeqGradEcho(const SeqGradEcho& sge) {
  SeqGradEcho::operator = (sge);
  common_init(sge.get_label());
}


SeqGradEcho& SeqGradEcho::operator = (const SeqGradEcho& sge) {
  SeqObjList::operator = (sge);

  pulsptr=sge.pulsptr;
  pls_reph=sge.pls_reph;
  phase=sge.phase;
  phase3d=sge.phase3d;
  phase_rew=sge.phase_rew;
  phase3d_rew=sge.phase3d_rew;
  acqread=sge.acqread;
  readdeph=sge.readdeph;
  midpart=sge.midpart;

  mode=sge.mode;
  balanced_grads=sge.balanced_grads;

  build_seq();
  return *this;
}


SeqGradEcho& SeqGradEcho::set_pe_reorder_scheme(reorderScheme scheme,unsigned int nsegments) {
  phase.set_reorder_scheme(scheme,nsegments);
  if(balanced_grads) phase_rew.set_reorder_scheme(scheme,nsegments);
  return *this;
}


SeqVector& SeqGradEcho::get_pe_vector() {
  if(balanced_grads) return phasesim;
  return phase;
}


SeqVector& SeqGradEcho::get_pe3d_vector() {
  if(balanced_grads) return phasesim3d;
  return phase3d;
}


const SeqVector& SeqGradEcho::get_pe_reorder_vector() const {
  if(balanced_grads) return phasereordsim;
  return phase.get_reorder_vector();
}


double SeqGradEcho::get_echo_time() const {
  Log<Seq> odinlog(this,"get_echo_time");
  double result=0.0;
  if(pulsptr.get_handled()) {
    double magcenter=(pulsptr.get_handled()->get_duration()) - (pulsptr.get_handled()->get_magnetic_center());
    ODINLOG(odinlog,normalDebug) << "magcenter=" << magcenter << STD_endl;
    result+=magcenter;
  }
  double middur=midpart.get_duration();
  double postexcdur=postexcpart.get_duration();
  double acqcenter=acqread.get_acquisition_center();
  ODINLOG(odinlog,normalDebug) << "middur/postexcdur/acqcenter=" << middur << "/" << postexcdur << "/" << acqcenter << STD_endl;
  result += middur + postexcdur + acqcenter;
  return result;
}


SeqGradEcho& SeqGradEcho::set_midpart(const SeqObjBase& soa) {
  midpart=soa;
  build_seq(); // Has to be re-built so that objects of midpart are copied to this
  return *this;
}


SeqAcqInterface& SeqGradEcho::set_template_type(templateType type) {
  acqread.set_template_type(type);
  if(type==phasecorr_template) phase.set_strength(0.0);
  return *this;
}


double SeqGradEcho::get_preacq() const {
  double result=0.0;
  if(pulsptr.get_handled()) result+=pulsptr.get_handled()->get_duration();
  result+=midpart.get_duration();
  result+=postexcpart.get_duration();
  return result;
}


SeqGradInterface& SeqGradEcho::set_gradrotmatrix(const RotMatrix& matrix) {
  if(pulsptr.get_handled()) pulsptr.get_handled()->set_gradrotmatrix(matrix);
  postexcpart.set_gradrotmatrix(matrix);
  acqread.set_gradrotmatrix(matrix);
  postacqpart.set_gradrotmatrix(matrix);
  return *this;
}

SeqGradInterface& SeqGradEcho::invert_strength() {
  if(pulsptr.get_handled()) pulsptr.get_handled()->invert_strength();
  postexcpart.invert_strength();
  acqread.invert_strength();
  postacqpart.invert_strength();
  return *this;
}


fvector SeqGradEcho::get_gradintegral() const {
  fvector result(3);
  result=0.0;
  if(pulsptr.get_handled()) result+=pulsptr.get_handled()->get_gradintegral();
  result+=postexcpart.get_gradintegral();
  result+=acqread.get_gradintegral();
  result+=postacqpart.get_gradintegral();
  return result;
}


void SeqGradEcho::build_seq() {
  Log<Seq> odinlog(this,"build_seq");
  clear();
  postexcpart.clear();
  postacqpart.clear();
  phasesim.clear();
  phasesim3d.clear();
  phasereordsim.clear();

  if(balanced_grads) {
    phasesim+=phase;
    phasesim+=phase_rew;
    phasereordsim+=phase.get_reorder_vector();
    phasereordsim+=phase_rew.get_reorder_vector();
    if(mode==voxel_3d) {
      phasesim3d+=phase3d;
      phasesim3d+=phase3d_rew;
    }
  }


  if(mode==voxel_3d) {
    postexcpart /=  phase3d / phase / readdeph ;
    if(balanced_grads) postacqpart /=  phase3d_rew / phase_rew / readdeph ;
  } else {
    postexcpart /=  pls_reph / phase / readdeph ;
    if(balanced_grads) postacqpart /=  pls_reph / phase_rew / readdeph ;
  }

  if(pulsptr.get_handled()) {
    (*this)+=(*pulsptr.get_handled()) + midpart + postexcpart + acqread;
    if(balanced_grads) (*this) += postacqpart;
  } else {
    ODINLOG(odinlog,warningLog) << "No pulse specified for gradient echo module" << STD_endl;
  }

  // reco stuff
  acqread.set_reco_vector(line,phase);
  if(mode==voxel_3d) acqread.set_reco_vector(line3d,phase3d);
  if(pulsptr.get_handled()) acqread.set_reco_vector(slice,*pulsptr.get_handled());

}


