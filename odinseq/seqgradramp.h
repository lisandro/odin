/***************************************************************************
                          seqgradramp.h  -  description
                             -------------------
    begin                : Tue Aug 13 2002
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQGRADRAMP_H
#define SEQGRADRAMP_H

#include <odinseq/seqgradwave.h>




/**
  * @addtogroup odinseq
  * @{
  */


/**
  * This enum is used to specify the shape of gradient ramp:
  * - linear: A ramp that linearly increases/decreases the gradient field.
  *           The first ramp point has the initial gradient strength, the last point
  *           the final gradient strength. With this selection, the integral over the
  *           ramp is the same as that over a continuous triangular function, i.e
  *           Integral = 1/2 * strength_difference * duration.
  * - sinusoidal: The sinus between -pi/2 and pi/2 is used for the gradient shape
  * - half_sinusoidal: The sinus between 0 and pi/2 is used for the gradient shape
  */
enum rampType {linear, sinusoidal, half_sinusoidal};


///////////////////////////////////////////////////////////

/**
  * \brief Gradient Ramp
  *
  * This class represents a digitized gradient ramp.
  */
class SeqGradRamp : public SeqGradWave {

 public:

/**
  * Constructs a gradient ramp labeled 'object_label' with the following properties:
  * - gradchannel:   The channel this object should be played out
  * - initgradstrength: The initial gradient strength, i.e. the strength at the beginning of the ramp
  * - finalgradstrength: The final gradient strength, i.e. the strength at the end of the ramp
  * - timestep:      The time between adjecent points of the ramp
  * - type:          The shape for the ramp
  * - steepnessfactor: This parameter in the range of ]0,1] determines the relative rising
  *                  speed of the gradient strength, i.e. with 1 the gradients are switched
  *                  as fast as possible
  * - reverse:       Reverse ramp calculation (only relevant for asymmetric ramps)
  */
  SeqGradRamp(const STD_string& object_label,direction gradchannel,
           float initgradstrength, float finalgradstrength, double timestep,rampType type = linear,
           float steepnessfactor=1.0, bool reverse = false);

/**
  * Constructs a gradient ramp labeled 'object_label' with the following properties:
  * - gradchannel:   The channel this object should be played out
  * - gradduration:  The duration of this gradient object
  * - initgradstrength: The initial gradient strength, i.e. the strength at the beginning of the ramp
  * - finalgradstrength: The final gradient strength, i.e. the strength at the end of the ramp
  * - timestep:      The time between adjecent points of the ramp
  * - type:          The shape for the ramp
  * - reverse:       Reverse ramp calculation (only relevant for asymmetric ramps)
  */
  SeqGradRamp(const STD_string& object_label,direction gradchannel,double gradduration,
           float initgradstrength, float finalgradstrength, double timestep,
           rampType type = linear, bool reverse = false);


/**
  * Constructs a copy of 'sgr'
  */
  SeqGradRamp(const SeqGradRamp& sgr);

/**
  * Construct an empty gradient object with the given label
  */
  SeqGradRamp(const STD_string& object_label = "unnamedSeqGradRamp");

/**
  * Sets the ramp according to the following parameters:
  * - initgradstrength: The initial gradient strength, i.e. the strength at the beginning of the ramp
  * - finalgradstrength: The final gradient strength, i.e. the strength at the end of the ramp
  * - timestep:      The time between adjecent points of the ramp
  * - type:          The shape for the ramp
  * - steepnessfactor: This parameter in the range of ]0,1] determines the relative rising
  *                  speed of the gradient strength, i.e. with 1 the gradients are switched
  *                  as fast as possible
  * - reverse:       Reverse ramp calculation (only relevant for asymmetric ramps)
  */
  SeqGradRamp& set_ramp(float initgradstrength, float finalgradstrength,
     double timestep,rampType type = linear,float steepnessfactor=1.0, bool reverse = false);


/**
  * Sets the ramp according to the following parameters:
  * - gradduration:  The duration of this gradient object
  * - initgradstrength: The initial gradient strength, i.e. the strength at the beginning of the ramp
  * - finalgradstrength: The final gradient strength, i.e. the strength at the end of the ramp
  * - timestep:      The time between adjecent points of the ramp
  * - type:          The shape for the ramp
  * - reverse:       Reverse ramp calculation (only relevant for asymmetric ramps)
  */
  SeqGradRamp& set_ramp(double gradduration,float initgradstrength,
     float finalgradstrength, double timestep,rampType type = linear, bool reverse = false);

/**
  * Returns the shape for the ramp
  */
  rampType get_ramptype() const {return ramptype;}


/**
  * Assignment operator that makes this gradient channel object become a copy of 'sgr'
  */
  SeqGradRamp& operator = (const SeqGradRamp& sgr);


  // helper functions
  static fvector makeGradRamp(rampType type, float beginVal, float endVal, unsigned int n_vals, bool reverseramp);
  static unsigned int npts4ramp(rampType type, float beginVal, float endVal, float maxIncrement);
  static unsigned int npts4ramp(double totaldur, double timestep);


  // overloading functions from SeqGradInterface
  SeqGradInterface& set_strength(float gradstrength);

 private:
  void generate_ramp();

  float initstrength;
  float finalstrength;
  double dt;
  float steepness;
  bool steepcontrol;
  rampType ramptype;
  bool  reverseramp;

};

/** @}
  */

#endif
