/***************************************************************************
                          seqpulsndim.h  -  description
                             -------------------
    begin                : Wed Aug 8 2001
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQPULSNDIM_H
#define SEQPULSNDIM_H

#include <odinseq/seqpuls.h>
#include <odinseq/seqparallel.h>


struct SeqPulsNdimObjects; // forward declaration


/**
  * @addtogroup odinseq
  * @{
  */

/**
  *
  * \brief RF Pulse + gradients
  *
  * This class represents a shaped n-dimesional RF-pulse, i.e. it
  * consists of a complex waveform and in addition three gradient
  * waveforms in the three spatial directions that are played out simultaneously
  */
class SeqPulsNdim : public SeqParallel, public virtual SeqPulsInterface, public Handled<SeqPulsNdim*> {

  // virtual functions of SeqPulsInterface are marshalled to sp
  // virtual functions of SeqFreqChanInterface are marshalled to sp

 public:

/**
  * Default constructor
  */
  SeqPulsNdim(const STD_string& object_label = "unnamedSeqPulsNdim" );

/**
  * Copy constructor
  */
  SeqPulsNdim(const SeqPulsNdim& spnd);


/**
  * Destructor
  */
  ~SeqPulsNdim();

/**
  * Sets the RF waveform of the pulse
  */
  SeqPulsNdim& set_rfwave(const cvector& waveform);

/**
  * Sets the waveform for the gradient pulse in direction 'dir'
  */
  SeqPulsNdim& set_gradwave(direction dir, const fvector& waveform);

/**
  * Sets the B1 scaling for simulation
  */
  SeqPulsNdim& set_B1max(float b1max);

/**
  * Returns the complex RF waveform
  */
  cvector get_rfwave() const;

/**
  * Returns the waveform of the gradient pulse in direction 'dir'
  */
  fvector get_gradwave(direction dir) const;

/**
  * Specifies an extra shift between RF and the gradient channels
  */
  SeqPulsNdim& set_grad_shift_offset(float grad_shift_offset);

/**
  * Dimensionality of the pulse
  */
  virtual int get_dims() const;

  // virtual functions of SeqPulsInterface are (mostly) marshalled to sp, except:
  SeqPulsInterface& set_pulsduration(float pulsduration);
  float get_magnetic_center() const;


  // forwarding virtual functions of SeqVector to sp
  SeqVector& set_indexvec(const ivector& iv);


/**
  * This assignment operator will make this object become an exact copy of 'spnd'.
  */
  SeqPulsNdim& operator = (const SeqPulsNdim& spnd);

/**
  * Appends the gradient list sgcl to the gradient part of the pulse
  */
  SeqPulsNdim& operator += (SeqGradChanList& sgcl);


 protected:
  SeqPulsNdim& set_system_flipangle(float angle);

  SeqPulsNdim& build_seq();

 private:
  int dims;
  double gradshift;

  SeqPulsNdimObjects* objs;

};


/** @}
  */

#endif
