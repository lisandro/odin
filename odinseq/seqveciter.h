/***************************************************************************
                          seqveciter.h  -  description
                             -------------------
    begin                : Fri Jan 19 2007
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQVECITER_H
#define SEQVECITER_H


#include <odinseq/seqcounter.h>
#include <odinseq/seqobj.h>

/**
  * @addtogroup odinseq
  * @{
  */


/**
  * \brief Iterator for vectors
  *
  * Iterator to step manually through vectors.
  * Each occurence of this iterator in the sequence tree will
  * increment actual value of the vector by one. If the end of
  * the vector is reached, iteration starts again at the beginning,
  * i.e. the iteration is cyclical.
  * Use add_vector() to append vectors which should be controlled
  * by this iterator.
  */
class SeqVecIter : public SeqCounter, public SeqObjBase {

 public:

/**
  * Construct an empty iterator with the given label.
  * Iteration will start at index 'start'.
  */
  SeqVecIter(const STD_string& object_label = "unnamedSeqVecIter", unsigned int start=0);

/**
  * Copy constructor
  */
  SeqVecIter(const SeqVecIter& svi);

/**
  * Assignment operator
  */
  SeqVecIter& operator = (const SeqVecIter& svi);


  // overloading virtual function from SeqTreeObj
  STD_string get_program(programContext& context) const;
  double get_duration() const;
  unsigned int event(eventContext& context) const;
  STD_string get_properties() const;
  void query(queryContext& context) const;
  RecoValList get_recovallist(unsigned int reptimes, LDRkSpaceCoords& coords) const;


 private:

  bool is_acq_iterator() const;
   
  // overwriting virtual functions from SeqClass
  bool prep();

  unsigned int startindex;

};

/** @}
  */

#endif
