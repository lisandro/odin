#include "seqpulsar.h"
#include "seqsim.h"
#include "seqgradramp.h"

#include <tjutils/tjhandler_code.h>

void SeqPulsar::common_init() {
  register_pulse(this);
  for(int i=0;i<n_directions; i++) {
    reph_grad[i]=0;
    rephase_integral[i]=0.0;
  }
}

SeqPulsar::SeqPulsar(const STD_string& object_label, bool rephased, bool interactive)
 : SeqPulsNdim(object_label), OdinPulse(object_label) {
  Log<Seq> odinlog(this,"SeqPulsar(object_label)");
  common_init();
  always_refresh=interactive;
  attenuation_set=false;
  rephased_pulse=rephased;
  rephaser_strength=0.0;
  if(rephased_pulse) SeqPulsar::set_pulse_type(excitation); // do NOT call virtual function in constructor
  else  SeqPulsar::set_pulse_type(refocusing); // do NOT call virtual function in constructor
}

SeqPulsar::SeqPulsar(const SeqPulsar& sp) {
  common_init();
  SeqPulsar::operator = (sp);
}

SeqPulsar::~SeqPulsar() {
  Log<Seq> odinlog(this,"~SeqPulsar");
  unregister_pulse(this);
  for(int i=0;i<n_directions; i++) if(reph_grad[i]) {
    ODINLOG(odinlog,normalDebug) << "deleting reph_grad[" << i << "] ..." << STD_endl;
    delete reph_grad[i];
  }
}

SeqPulsInterface& SeqPulsar::set_flipangle(float flipangle) {
  Log<Seq> odinlog(this,"set_flipangle");
  OdinPulse::set_flipangle(flipangle);
//  refresh();
  return *this;
}


SeqPulsInterface& SeqPulsar::set_pulsduration(float pulsduration) {
  Log<Seq> odinlog(this,"set_pulsduration");
  OdinPulse::set_Tp(pulsduration);
//  refresh();
  return *this;
}

SeqPulsInterface& SeqPulsar::set_power(float pulspower) {
  SeqPulsNdim::set_power(pulspower);
  attenuation_set=true;
  return *this;
}

SeqPulsInterface& SeqPulsar::set_pulse_type(pulseType type) {
  SeqPulsNdim::set_pulse_type(type);
  OdinPulse::set_pulse_type(type);
  return *this;
}


SeqPulsar& SeqPulsar::set_rephased(bool rephased, float strength) {
  Log<Seq> odinlog(this,"set_refocused");
  rephased_pulse=rephased;
  rephaser_strength=strength;
  SeqPulsar::update();
  return *this;
}


fvector SeqPulsar::get_reph_gradintegral() const {
  fvector result(n_directions);
  result=0.0;
  for(int i=0;i<n_directions; i++) {
//    if(reph_grad[i]) result+=reph_grad[i]->get_gradintegral();
    result[i]=-rephase_integral[i];
  }
  return result;
}

SeqPulsar& SeqPulsar::operator = (const SeqPulsar& sp) {
  Log<Seq> odinlog(this,"operator=");
  always_refresh=sp.always_refresh; // assign before OdinPulse::operator = to avoid call of SeqPulsar::update()
  OdinPulse::operator = (sp);
  SeqPulsNdim::operator = (sp);
  attenuation_set=sp.attenuation_set;
  rephased_pulse=sp.rephased_pulse;
  rephaser_strength=sp.rephaser_strength;
  SeqPulsar::update(); // do NOT call virtual function in constructor
  return *this;
}


OdinPulse& SeqPulsar::update() {
  Log<Seq> odinlog(this,"update");
  if(always_refresh) refresh();
  return *this;
}

SeqPulsar& SeqPulsar::set_interactive(bool flag) {
  always_refresh=flag;
  return *this;
}

STD_string SeqPulsar::get_properties() const {
  return "Shape="+get_shape()+", Trajectory="+get_trajectory()+", Filter="+get_filter();
}


SeqPulsar& SeqPulsar::refresh() {
  Log<Seq> odinlog(this,"refresh");

  int i,j,index;

  SeqPulsNdim::set_nucleus(OdinPulse::get_nucleus());

  OdinPulse::recalc_pulse();

  int npts=OdinPulse::get_size();

  float Tp=OdinPulse::get_Tp();
  float dt=secureDivision(Tp,float(npts));

  float G0=OdinPulse::get_G0();

  ODINLOG(odinlog,normalDebug) << "npts/Tp/dt/G0=" << npts << "/" << Tp << "/" << dt << "/" << G0 << STD_endl;


  fvector pulsegrad[n_directions];
  for(i=0;i<n_directions; i++) pulsegrad[i]=OdinPulse::get_Grad(direction(i));

  // find maximum initial gradient strength
  float GinitMax=0.0;
  direction GinitMaxChannel=readDirection;
  for(i=0;i<n_directions; i++) {
    float Ginit=float(pulsegrad[i][0])*G0;
    if(fabs(Ginit)>GinitMax) {
      GinitMax=fabs(Ginit);
      GinitMaxChannel=direction(i);
    }
  }
  ODINLOG(odinlog,normalDebug) << "GinitMax/GinitMaxChannel=" << GinitMax << "/" << GinitMaxChannel << STD_endl;

  // find maximum final gradient strength
  float GfinalMax=0.0;
  direction GfinalMaxChannel=readDirection;
  for(i=0;i<n_directions; i++) {
    float Gfinal=float(pulsegrad[i][npts-1])*G0;
    if(fabs(Gfinal)>GfinalMax) {
      GfinalMax=fabs(Gfinal);
      GfinalMaxChannel=direction(i);
    }
  }
  ODINLOG(odinlog,normalDebug) << "GfinalMax=" << GfinalMax << STD_endl;
  ODINLOG(odinlog,normalDebug) << "GfinalMaxChannel=" << GfinalMaxChannel << STD_endl;

  // use gradient ramp objects to generate the pulse ramps
  // these are pointers because of the delete[]-does-not-call-destructor-of-base-class bug in GCC
  SeqGradRamp* onramp[n_directions];
  SeqGradRamp* offramp[n_directions];
  for(i=0;i<n_directions; i++) {
    onramp[i]=0;
    offramp[i]=0;
  }

  // generate steepest ramps first
  float onrampdur=0.0;
  float offrampdur=0.0;
  for(i=0;i<n_directions; i++) {

    float initStrength=pulsegrad[i][0];
    float finalStrength=pulsegrad[i][npts-1];

    if(i==GinitMaxChannel && initStrength!=0.0) {
      onramp[i] =new SeqGradRamp("onramp", direction(i),0.0,initStrength*G0,dt);
      onrampdur=(onramp[i])->get_duration();
      ODINLOG(odinlog,normalDebug) << "maxinit/dur[" << i << "]=" << initStrength*G0 << "/" << onrampdur << STD_endl;
    }

    if(i==GfinalMaxChannel && finalStrength!=0.0) {
      offramp[i]=new SeqGradRamp("offramp",direction(i),finalStrength*G0,0.0,dt);
      offrampdur=(offramp[i])->get_duration();
      ODINLOG(odinlog,normalDebug) << "maxfinal/dur[" << i << "]=" << finalStrength*G0 << "/" << offrampdur << STD_endl;
    }


  }

  // generate the other ramps
  for(i=0;i<n_directions; i++) {
    float initStrength=pulsegrad[i][0];
    float finalStrength=pulsegrad[i][npts-1];
    if(i!=GinitMaxChannel && initStrength!=0.0)  onramp[i] =new SeqGradRamp("onramp",  direction(i),onrampdur,0.0,initStrength*G0,dt);
    if(i!=GfinalMaxChannel && finalStrength!=0.0) offramp[i]=new SeqGradRamp("offramp", direction(i),offrampdur,finalStrength*G0,0.0,dt);
  }


  int rampOn_npts=0;
  if(onramp[GinitMaxChannel]) rampOn_npts=onramp[GinitMaxChannel]->get_npts();
  int rampOff_npts=0;
  if(offramp[GfinalMaxChannel]) rampOff_npts=offramp[GfinalMaxChannel]->get_npts();
  int total_pts=rampOn_npts+npts+rampOff_npts;

  ODINLOG(odinlog,normalDebug) << "dt=" << dt << STD_endl;
  ODINLOG(odinlog,normalDebug) << "rampOn_npts=" << rampOn_npts << STD_endl;
  ODINLOG(odinlog,normalDebug) << "rampOff_npts=" << rampOff_npts << STD_endl;


  // the arrays that will be assigned to the SeqPulsNdim base class
  cvector wv(total_pts);
  fvector Gpulse[n_directions];
  for(i=0;i<n_directions; i++) Gpulse[i].resize(total_pts);


// The gradient waveform will look something like this
// with the marked intervals treated below according to the numbers:
//
//     ___2___
//   1/       \3
//.../.........\....



///////////////////////////////////
//  Gradient part No 1

  for(j=0;j<rampOn_npts;j++) {
    wv[j]=0.0;
    for(i=0;i<n_directions; i++) {
      if(onramp[i]) Gpulse[i][j]=(*(onramp[i]))[j]*onramp[i]->get_strength()/G0;
      else Gpulse[i][j]=0.0;
    }
  }

///////////////////////////////////
//  Gradient part No 2

  cvector b1pulse=OdinPulse::get_B1();

  for(j=0;j<npts;j++) {
    index=rampOn_npts+j;
    wv[index]=b1pulse[j];
    for(i=0;i<n_directions; i++) Gpulse[i][index]=pulsegrad[i][j];
  }

///////////////////////////////////
//  Gradient part No 3

  for(j=0;j<rampOff_npts;j++) {
    index=rampOn_npts+npts+j;
    wv[index]=0.0;
    for(i=0;i<n_directions; i++) {
      if(offramp[i]) Gpulse[i][index]=(*(offramp[i]))[j]*offramp[i]->get_strength()/G0;
      else Gpulse[i][index]=0.0;
    }
  }


///////////////////////////////////
//  Deal with rephasing gradient lobe

  bool add_refoc_lobe=false;
  if(rephased_pulse  && get_rel_center()!=1.0) add_refoc_lobe=true;
  ODINLOG(odinlog,normalDebug) << "add_refoc_lobe=" << add_refoc_lobe << STD_endl;

  // calculate the gradient integral that will be rephased
  // calculate it, even if add_refoc_lobe==false so that it can be used later by get_reph_gradintegral()
  for(i=0;i<n_directions; i++) rephase_integral[i]=0.0;
  unsigned int reph_startindex=rampOn_npts+( unsigned int)(get_rel_center()*float(npts));
  for(j=reph_startindex; j<total_pts; j++) {
    for(i=0;i<n_directions; i++) rephase_integral[i]+=Gpulse[i][j]*G0 * dt;
  }

  if(add_refoc_lobe) {
    create_rephgrads(true);
  }


///////////////////////////////////
// copying generated pulse into the base class SeqPulsNdim

  update_B10andPower();

  float rel_magnetic_center=( (float)rampOn_npts + get_rel_center() * float(npts) ) / float(total_pts);
  set_rel_magnetic_center(rel_magnetic_center);

  SeqPulsNdim::set_rfwave(wv);
  SeqPulsNdim::set_pulsduration(total_pts*dt);
  ODINLOG(odinlog,normalDebug) << "total_pts=" << total_pts << STD_endl;
  ODINLOG(odinlog,normalDebug) << "dt=" << dt << STD_endl;
  ODINLOG(odinlog,normalDebug) << "pulsduration=" << total_pts*dt << STD_endl;

  for(i=0;i<n_directions; i++) SeqPulsNdim::set_gradwave(direction(i),Gpulse[i]);
  SeqPulsNdim::build_seq();
  SeqPulsNdim::set_strength(OdinPulse::get_G0());

  if(add_refoc_lobe) {
    for(i=0;i<n_directions; i++) {
      if(rephase_integral[i]) {
        ODINLOG(odinlog,normalDebug) << "appending reph_grad[" << i << "]" << STD_endl;
        (*this)+=*(reph_grad[i]);   // append it to the gradient part of the pulse
      }
    }
  }

  for(i=0;i<n_directions; i++) {
    if(onramp[i]) delete onramp[i];
    if(offramp[i]) delete offramp[i];
  }

  return *this;
}


void SeqPulsar::create_rephgrads(bool recreate) const {
  Log<Seq> odinlog(this,"create_rephgrads");
  ODINLOG(odinlog,normalDebug) << "recreate=" << recreate << STD_endl;

  // Estimate reasonable duration for rephasing gradients
  float rephdur=(1.0-get_rel_center())*OdinPulse::get_Tp();
  ODINLOG(odinlog,normalDebug) << "rephdur=" << rephdur << STD_endl;

  // generate rephasing pulses
  for(int i=0;i<n_directions; i++) {
    ODINLOG(odinlog,normalDebug) << "rephase_integral/reph_grad[" << i << "]=" << rephase_integral[i] << "/" << reph_grad[i] << STD_endl;
    if(recreate && reph_grad[i]) {
      delete reph_grad[i]; reph_grad[i]=0;
    }
    if(rephase_integral[i] && !reph_grad[i]) {
      if(rephaser_strength>0.0) {
        reph_grad[i]=new SeqGradTrapez(get_label()+"_reph_grad", -rephase_integral[i], rephaser_strength, direction(i));
      } else {
        reph_grad[i]=new SeqGradTrapez(get_label()+"_reph_grad", -rephase_integral[i], direction(i), rephdur);
      }
    }
  }
}


void SeqPulsar::update_B10andPower() {
  Log<Seq> odinlog(this,"SeqPulsar::update_B10andPower");
  OdinPulse::update_B10andPower();

  float pow,argument;

  if(is_adiabatic()) {
    argument=secureInv(get_Tp_1pulse());
  } else {
    argument=secureInv(get_Tp_1pulse())*get_flipangle()/90.0;
  }
  if (argument==0.0) pow=_MAX_PULSE_ATTENUATION_;
  else pow=-20.*log10(argument)+get_pulse_gain()+systemInfo->get_reference_gain();

  if(!attenuation_set) SeqPulsNdim::set_power(pow);

  float sysflip=get_flipangle()*get_flipangle_corr_factor();
  float b10=OdinPulse::get_B10();
  ODINLOG(odinlog,normalDebug) << "pow/sysflip/b10=" << pow << "/" << sysflip << "/" << b10 << STD_endl;

  SeqPulsNdim::set_system_flipangle(sysflip);
  SeqPulsNdim::set_B1max(b10);
}



SeqFreqChanInterface& SeqPulsar::set_nucleus(const STD_string& nucleus) {
  OdinPulse::set_nucleus(nucleus);
  SeqPulsNdim::set_nucleus(nucleus);
  SeqPulsar::update();
  return *this;
}

int SeqPulsar::get_dims() const {
  int result=0;
  funcMode dimmode=get_dim_mode();
  if(dimmode == zeroDeeMode) result=0;
  if(dimmode == oneDeeMode) result=1;
  if(dimmode == twoDeeMode) result=2;
  return result;
}


void SeqPulsar::register_pulse(SeqPulsar* pls) {
  Log<Seq> odinlog("SeqPulsar","register_pulse");
  ODINLOG(odinlog,normalDebug) << " " << pls->get_label() << STD_endl;
  active_pulsar_pulses->push_back(pls);
}

void SeqPulsar::unregister_pulse(SeqPulsar* pls) {
  Log<Seq> odinlog("SeqPulsar","unregister_pulse");
  ODINLOG(odinlog,normalDebug) << " " << pls->get_label() << STD_endl;
  active_pulsar_pulses->remove(pls);
}

void SeqPulsar::init_static() {
  active_pulsar_pulses.init("active_pulsar_pulses");
}

void SeqPulsar::destroy_static() {
  active_pulsar_pulses.destroy();
}


template class SingletonHandler<SeqPulsar::PulsarList,false>;
SingletonHandler<SeqPulsar::PulsarList,false> SeqPulsar::active_pulsar_pulses;

EMPTY_TEMPL_LIST bool StaticHandler<SeqPulsar>::staticdone=false;


/////////////////////////////////////////////////////////////////////////////



SeqPulsarReph::SeqPulsarReph(const STD_string& object_label,const SeqPulsar& puls)
        : SeqGradChanParallel(object_label) {

  dim=puls.get_dims();

  puls.create_rephgrads(false); // create if necessary
  if(puls.reph_grad[readDirection]) gxpulse=*(puls.reph_grad[readDirection]);
  if(puls.reph_grad[phaseDirection]) gypulse=*(puls.reph_grad[phaseDirection]);
  if(puls.reph_grad[sliceDirection]) gzpulse=*(puls.reph_grad[sliceDirection]);

  build_seq();
}

SeqPulsarReph::SeqPulsarReph(const SeqPulsarReph& spr): SeqGradChanParallel(spr) {
  dim=0;
  SeqPulsarReph::operator = (spr);
}

SeqPulsarReph::SeqPulsarReph(const STD_string& object_label) : SeqGradChanParallel(object_label) {
  dim=0;
}

SeqPulsarReph& SeqPulsarReph::operator = (const SeqPulsarReph& spr) {
  SeqGradChanParallel::operator = (spr);
  dim=spr.dim;
  gxpulse=spr.gxpulse;
  gypulse=spr.gypulse;
  gzpulse=spr.gzpulse;
  build_seq();
  return *this;
}

SeqPulsarReph::~SeqPulsarReph() {
}

float SeqPulsarReph::get_onramp_duration() const {return gzpulse.get_onramp_duration();}
float SeqPulsarReph::get_constgrad_duration() const {return gzpulse.get_constgrad_duration();}
float SeqPulsarReph::get_offramp_duration() const {return gzpulse.get_offramp_duration();}


void SeqPulsarReph::build_seq() {
  clear();

  if(dim==3) (*this) += gxpulse / gypulse / gzpulse;
  if(dim==2) (*this) += gxpulse / gypulse;
  if(dim==1) (*this) += gzpulse;
}




/////////////////////////////////////////////////////////////////////////////

SeqPulsarSinc::SeqPulsarSinc(const STD_string& object_label, float slicethicknes, bool rephased, float duration, float flipangle, float resolution, unsigned int npoints)
  : SeqPulsar(object_label,rephased,false) {
  set_dim_mode(oneDeeMode);
  set_Tp(duration);
  resize(npoints);
  SeqPulsar::set_flipangle(flipangle); // do NOT call virtual function in constructor
  set_shape("Sinc("+ftos(slicethicknes)+")");
  set_trajectory("Const(0.0,1.0)");
  set_filter("Triangle");
  set_spat_resolution(resolution);
  SeqPulsar::set_encoding_scheme(maxDistEncoding); // do NOT call virtual function in constructor
  refresh();
  set_interactive(true);
}

SeqPulsarSinc::SeqPulsarSinc(const SeqPulsarSinc& sps) {
  SeqPulsarSinc::operator = (sps);
}

SeqPulsarSinc& SeqPulsarSinc::operator = (const SeqPulsarSinc& sps) {
  SeqPulsar::operator = (sps);
  refresh();
  return *this;
}


/////////////////////////////////////////////////////////////////////////////

SeqPulsarGauss::SeqPulsarGauss(const STD_string& object_label, float slicethicknes, bool rephased, float duration, float flipangle, unsigned int npoints)
  : SeqPulsar(object_label, rephased, false) {
  set_dim_mode(oneDeeMode);
  set_Tp(duration);
  resize(npoints);
  SeqPulsar::set_flipangle(flipangle); // do NOT call virtual function in constructor
  set_shape("Const");
  set_trajectory("Const(0.0,1.0)");
  set_filter("Gauss");
  set_spat_resolution(0.5*slicethicknes);
  SeqPulsar::set_encoding_scheme(maxDistEncoding); // do NOT call virtual function in constructor
  refresh();
  set_interactive(true);
}

SeqPulsarGauss::SeqPulsarGauss(const SeqPulsarGauss& spg) {
  SeqPulsarGauss::operator = (spg);
}

SeqPulsarGauss& SeqPulsarGauss::operator = (const SeqPulsarGauss& spg) {
  SeqPulsar::operator = (spg);
  refresh();
  return *this;
}

/////////////////////////////////////////////////////////////////////////////

SeqPulsarBP::SeqPulsarBP(const STD_string& object_label, float duration, float flipangle, const STD_string& nucleus)
  : SeqPulsar(object_label, false, false) {
  set_dim_mode(zeroDeeMode);
  SeqPulsar::set_nucleus(nucleus); // do NOT call virtual function in constructor
  set_Tp(duration);
  unsigned int npoints=32;
  resize(npoints);
  SeqPulsar::set_flipangle(flipangle); // do NOT call virtual function in constructor
  set_shape("Const");
  set_trajectory("Const(0.0,1.0)");
  set_filter("NoFilter");
  refresh();
  set_interactive(true);
}

SeqPulsarBP::SeqPulsarBP(const SeqPulsarBP& spb) {
  SeqPulsarBP::operator = (spb);
}

SeqPulsarBP& SeqPulsarBP::operator = (const SeqPulsarBP& spb) {
  SeqPulsar::operator = (spb);
  refresh();
  return *this;
}

