/***************************************************************************
                          seqsat.h  -  description
                             -------------------
    begin                : Tue Oct 19 2004
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQSAT_H
#define SEQSAT_H

#include <odinseq/seqpulsar.h>
#include <odinseq/seqgradpulse.h>


/**
  * @addtogroup odinseq
  * @{
  */


/**
  *
  * Enum to specifiy saturation component:
  * - fat: lipid protons will be saturated
  * - water: water protons will be saturated
  */
enum satNucleus {fat=0,water};


////////////////////////////////////////////////////////////////////////////

/**
  *
  * \brief Saturation pulse
  *
  * A 90deg Gaussian shaped saturation pulse
  *
  */
class SeqPulsarSat : public SeqPulsar {

 public:


/**
  * Constructs saturation pulse labeled 'object_label' with the following properties:
  * - nuc: the nucleus to saturate
  * - bandwidth: The bandwidth in kHz
  */
  SeqPulsarSat(const STD_string& object_label="unnamedSeqPulsarSat", satNucleus nuc=fat,float bandwidth=0.3);

/**
  * Constructs a saturation pulse labeled 'object_label' with the following properties:
  * - bandwidth: The bandwidth in kHz
  * - freqoffset: Frequency offset relative to the base frequency
  * - flipangle: The flipangle
  * - rel_filterwidth: Pulse width
  */
  SeqPulsarSat(const STD_string& object_label,float bandwidth,
                           double freqoffset=0.0, float flipangle=90.0, float rel_filterwidth=0.3);


/**
  * Constructs a copy of 'spg'
  */
  SeqPulsarSat(const SeqPulsarSat& spg);

/**
  * Assignment operator that makes this object become a copy of 'spg'
  */
  SeqPulsarSat& operator = (const SeqPulsarSat& spg);

};


////////////////////////////////////////////////////////////////////////////


/**
  *
  * \brief Saturation pulse + spoilers
  *
  * A saturation pulse + spoiler gradient pulse
  *
  */
class SeqSat : public SeqObjList, public virtual SeqPulsInterface, public virtual SeqGradInterface {

  // virtual functions of SeqPulsInterface are marshalled to puls
  // virtual functions of SeqFreqChanInterface are marshalled to puls

 public:
/**
  * Constructs saturation pulse labeled 'object_label' with the following properties:
  * - nuc: the nucleus to saturate
  * - bandwidth: The bandwidth in kHz
  * - npulses: The number of consecutive pulses
  */
  SeqSat(const STD_string& object_label="unnamedSeqSat",satNucleus nuc=fat,float bandwidth=0.3, unsigned int npulses=1);


/**
  * Constructs a copy of 'spg'
  */
  SeqSat(const SeqSat& spg);

/**
  * Assignment operator that makes this object become a copy of 'spg'
  */
  SeqSat& operator = (const SeqSat& spg);


  // overloading virtual function from SeqGradInterface
  SeqGradInterface& set_strength(float gradstrength);
  SeqGradInterface& invert_strength();
  float get_strength() const;
  fvector get_gradintegral() const;
  double get_gradduration() const;
  SeqGradInterface& set_gradrotmatrix(const RotMatrix& matrix);

 private:
  void build_seq();

  SeqPulsarSat puls;
  SeqGradConstPulse spoiler_read_pos;
  SeqGradConstPulse spoiler_slice_neg;
  SeqGradConstPulse spoiler_read_neg;
  SeqGradConstPulse spoiler_slice_pos;
  SeqGradConstPulse spoiler_phase_pos;

  unsigned int npulses_cache;

};

/** @}
  */

#endif
