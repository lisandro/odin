/***************************************************************************
                          seqpuls.h  -  description
                             -------------------
    begin                : Wed Aug 8 2001
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQPULS_H
#define SEQPULS_H

#include <odinseq/seqdur.h>
#include <odinseq/seqfreq.h>
#include <odinseq/seqobj.h>
#include <odinseq/seqdriver.h>



#define _MAX_PULSE_ATTENUATION_ 120.0


/**
  * @ingroup odinseq
  * Enum to specify the type of a pulse.It is essential for the calculation of k-space trajectories
  * and other stuff to specify this correctly for each object derived from 'SeqPulsInterface'
  * - excitation:  An excitation pulse.
  * - refocusing:  A refocusing pulse.
  * - storeMagn:   Stores the magnetization in longitudinal direction (stimulated echo)
  * - recallMagn:  Recalls the magnetization from the longitudinal direction (stimulated echo)
  * - inversion:   An inversion pulse
  * - saturation:  A saturation pulse
  */
enum pulseType                      {excitation=0,  refocusing,  storeMagn,  recallMagn,  inversion,  saturation, numof_pulseTypes};
static const char* pulseTypeLabel[]={"excitation", "refocusing","storeMagn","recallMagn","inversion","saturation"};
AVOID_CC_WARNING(pulseTypeLabel)



///////////////////////////////////////////////////////////////////////////////////////



/**
  * @ingroup odinseq_internals
  * This is the abstract base class for all objects that have the
  * property of a generating an RF field
  */
class SeqPulsInterface : public virtual SeqTreeObj, public virtual SeqFreqChanInterface {

 public:

/**
  * Sets the duration of the pulse. In case of a more complex pulse
  * that needs some extra commands before and after the actual pulse
  * will be played out, this function still sets the effective
  * duration, i.e. when the RF-field is non-zero
  */
  virtual SeqPulsInterface& set_pulsduration(float pulsduration) {if(marshall) marshall->set_pulsduration(pulsduration); else marshall_error(); return *this;}

/**
  * Returns the duration of the pulse. In case of a more complex pulse
  * that needs some extra commands before and after the actual pulse
  * will be played out, this function still returns the effective
  * duration, i.e. when the RF-field is applied
  */
  virtual double get_pulsduration() const {if(marshall) return marshall->get_pulsduration(); else marshall_error(); return 0.0;}


/**
  * Returns the flip angle of the pulse
  */
  virtual float get_flipangle() const {if(marshall) return marshall->get_flipangle(); else marshall_error(); return 0.0;}


/**
  * Sets the flip angle of the pulse
  */
  virtual SeqPulsInterface& set_flipangle(float flipangle) {if(marshall) marshall->set_flipangle(flipangle); else marshall_error(); return *this;}


/**
  * Sets a list of flip angles which can be iterated through by a loop
  * and the get_flipangle_vector() function
  */
  SeqPulsInterface& set_flipangles(const fvector& flipangles);


/**
  * Returns the list of actual flip angles which will be iterated
  * through by a loop in combination with the get_flipangle_vector() function
  */
  fvector get_flipangles() const {return get_flipangle()*get_flipscales();}


/**
  * Sets the power deposition (or pulse attenuation on Bruker) of the pulse
  */
  virtual SeqPulsInterface& set_power(float pulspower) {if(marshall) marshall->set_power(pulspower); else marshall_error(); return *this;}


/**
  * Returns the power deposition (or pulse attenuation on Bruker) of the pulse
  */
  virtual float get_power() const {if(marshall) return marshall->get_power(); else marshall_error(); return 0.0;}


/**
  * Sets the relative center of the pulse
  */
  virtual SeqPulsInterface& set_rel_magnetic_center(float center) {if(marshall) marshall->set_rel_magnetic_center(center); else marshall_error(); return *this;}

/**
  * Returns the relative center of the pulse
  */
  virtual float get_rel_magnetic_center() const {if(marshall) return marshall->get_rel_magnetic_center(); else marshall_error(); return 0.0;}

/**
  * Returns the absolute center of the pulse
  */
  virtual float get_magnetic_center() const {if(marshall) return marshall->get_magnetic_center(); else marshall_error(); return 0.0;}

/**
  * Sets the type of the pulse
  */
  virtual SeqPulsInterface& set_pulse_type(pulseType type) {if(marshall) marshall->set_pulse_type(type); else marshall_error(); return *this;}

/**
  * Returns the type of the pulse
  */
  virtual pulseType get_pulse_type() const {if(marshall) return marshall->get_pulse_type(); else marshall_error(); return pulseType(0);}


/**
  * Sets the reordering scheme of the flipangle vector
  */
  virtual SeqPulsInterface& set_flipangle_reorder_scheme(reorderScheme scheme,unsigned int nsegments) {if(marshall) marshall->set_flipangle_reorder_scheme(scheme,nsegments); else marshall_error(); return *this;}


/**
  * Returns a sequence vector to iterate over flipangles
  */
  virtual const SeqVector& get_flipangle_vector() const {if(marshall) return marshall->get_flipangle_vector(); else marshall_error(); return get_dummyvec();}

/**
  * Returns the reorder vector for flipangles
  */
  virtual const SeqVector& get_flipangle_reorder_vector() const {if(marshall) return marshall->get_flipangle_vector(); else marshall_error(); return get_dummyvec();}

 protected:

  SeqPulsInterface() : marshall(0) {}
  virtual ~SeqPulsInterface() {}

  virtual void set_flipscales(const fvector& flipscales) {if(marshall) marshall->set_flipscales(flipscales); else marshall_error();}
  virtual fvector get_flipscales() const  {if(marshall) return marshall->get_flipscales(); else marshall_error(); return fvector();}

  void set_marshall(SeqPulsInterface* mymarshall) {marshall=mymarshall;} // to be used in constructor code

 private:
  SeqPulsInterface* marshall; // for marshalling member functions to a sub-object
};


///////////////////////////////////////////////////////////////////////////////////////


class SeqPuls; // forward declaration

/**
  * @ingroup odinseq_internals
  * A vector class to iterate through a list of flip angles, only to be used by SeqPuls
  */
class SeqFlipAngVector : public SeqVector {

 public:

  SeqFlipAngVector(const STD_string& object_label, SeqPuls* flipangvec_user ) : SeqVector(object_label), user(flipangvec_user) {}


  // overloading virtual functions from SeqVector
  unsigned int get_vectorsize() const {return flipanglescale.size();}
  bool needs_unrolling_check() const {return true;}
  bool prep_iteration() const;
  svector get_vector_commands(const STD_string& iterator) const;

  fvector flipanglescale;

 private:
  // disable copying
  SeqFlipAngVector(const SeqFlipAngVector&) {}
  SeqFlipAngVector& operator = (const SeqFlipAngVector&) {return *this;}

  SeqPuls* user;
};



///////////////////////////////////////////////////////////////////////////////////////

/**
  * @ingroup odinseq_internals
  * The base class for platform specific drivers of RF pulses
  */
class SeqPulsDriver : public SeqDriverBase {

 public:
  SeqPulsDriver() {}
  virtual ~SeqPulsDriver() {}

  virtual bool prep_driver(const cvector& wave, double pulsduration, double pulscenter, float b1max, float power, float flipangle, const fvector& flipscales, pulseType plstype) = 0;
  virtual void event(eventContext& context, double start) const = 0;
  virtual double get_rf_energy() const = 0;

  virtual bool prep_flipangle_iteration(unsigned int count) = 0;

  virtual double get_predelay() const = 0;
  virtual double get_postdelay() const = 0;

  virtual STD_string get_program(programContext& context, unsigned  int phaselistindex, int channel, const STD_string& iteratorcommand) const = 0;

  virtual STD_string get_instr_label() const = 0;

  virtual svector get_flipvector_commands(const STD_string& iterator) const =0;
//  virtual svector get_phase_commands(const STD_string& iterator) const =0;

  virtual void new_freq(double newfreq) const = 0;
  virtual bool has_new_freq() const = 0;


  virtual SeqPulsDriver* clone_driver() const = 0;
};

///////////////////////////////////////////////////////////////////////////////////////


/**
  * @addtogroup odinseq
  * @{
  */


/**
  *
  * \brief RF pulse
  *
  * This class represents a shaped RF-pulse with a complex waveform
  */
class SeqPuls : public virtual SeqPulsInterface, public SeqObjBase, public SeqFreqChan, public SeqDur {

 public:

/**
  * Constructs a pulse labeled 'object_label' with the following properties:
  * - wave:         The complex waveform that will be played out
  * - pulsduration: The duration of the pulse
  * - pulspower:    The power of the pulse in dB
  * - nucleus:      This parameter determines the frequency at which the pulse will
  *                 be played out to be the resonance frequency of the specified nucleus
  * - phaselist:    This is an array of phases at which the acquired pulse will be
  *                 modulated. Use the get_phaselist_vector() function to attach it to a loop.
  * - freqlist:    This is an array of frequency offsets at which the pulse waveform
  *                will be modulated.
  * - rel_magnetic_center:  This parameter specifies the point in time that will be used
  *                 as the center of the pulse for timing calculations, e.g. for a spin-echo
  */
SeqPuls(const STD_string& object_label,const cvector& waveform,float pulsduration,
            float pulspower,
            const STD_string& nucleus="",const dvector& phaselist=0,
            const dvector& freqlist=0,float rel_magnetic_center=0.5);

/**
  * Constructs an empty pulse with the given label.
  */
  SeqPuls(const STD_string& object_label = "unnamedSeqPuls");

/**
  * Constructs a pulse which is a copy of 'sp'
  */
  SeqPuls(const SeqPuls& sp);


/**
  * This assignment operator will make this pulse become an exact copy of 'sp'.
  */
  SeqPuls& operator = (const SeqPuls& sp);


/**
  * Sets the complex waveform of the pulse
  */
  SeqPuls& set_wave(const cvector& waveform);

/**
  * Returns the complex RF waveform
  */
  cvector get_wave() const {return wave;}

/**
  * Returns the time offset within this object where the RF starts
  */
  double get_pulsstart() const {return pulsdriver->get_predelay();}


  // overloading virtual functions from SeqPulsInterface
  SeqPulsInterface& set_pulsduration(float pulsduration);
  double get_pulsduration() const;
  float get_flipangle() const {return system_flipangle;}
  SeqPulsInterface& set_flipangle(float flipangle) {return set_system_flipangle(flipangle);}
  float get_power() const {return power;}
  SeqPulsInterface& set_power(float pulspower) {power=pulspower; return *this;}
  SeqPulsInterface& set_rel_magnetic_center(float center) {relmagcent=center; return *this;}
  float get_rel_magnetic_center() const {return relmagcent;}
  float get_magnetic_center() const;
  SeqPulsInterface& set_pulse_type(pulseType type);
  pulseType get_pulse_type() const;
  const SeqVector& get_flipangle_vector() const {return flipvec;}
  void set_flipscales(const fvector& flipscales) {flipvec.flipanglescale=flipscales;}
  fvector get_flipscales() const {return flipvec.flipanglescale;}
  SeqPulsInterface& set_flipangle_reorder_scheme(reorderScheme scheme,unsigned int nsegments) {flipvec.set_reorder_scheme(scheme,nsegments); return *this;}
  const SeqVector& get_flipangle_reorder_vector() const {return flipvec.get_reorder_vector();}


  // overloading virtual function from SeqTreeObj
  double get_duration() const;
  STD_string get_program(programContext& context) const;
  double get_rf_energy() const;
  STD_string get_properties() const;
  unsigned int event(eventContext& context) const;
  SeqValList get_freqvallist(freqlistAction action) const;


  // some helper functions to use SeqPulse together with OdinPulse/Pulsar
  SeqPuls& set_system_flipangle(float angle);
  SeqPuls& set_B1max(float b1max);
  float get_B1max() const {return B1max_mT;}


 private:
  friend class SeqFlipAngVector;

  // overloading virtual functions from SeqFreqChan
  double get_freqchan_duration() const {return get_pulsduration();}
  STD_string get_driver_instr_label() const {return pulsdriver->get_instr_label();}

  // overwriting virtual functions from SeqClass
  bool prep();

  mutable SeqDriverInterface<SeqPulsDriver> pulsdriver;

  cvector wave;
  float power;
  float system_flipangle;
  float B1max_mT;
  float relmagcent;
  pulseType plstype;

  SeqFlipAngVector flipvec;

};



/** @}
  */

#endif
