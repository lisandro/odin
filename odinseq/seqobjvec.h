/***************************************************************************
                          seqobjvec.h  -  description
                             -------------------
    begin                : Mon Aug 9 2004
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQOBJVEC_H
#define SEQOBJVEC_H

#include <odinseq/seqvec.h>
#include <odinseq/seqobj.h>

class SeqGradObjInterface; // forward declaration

/**
  * @ingroup odinseq
  *
  * \brief Vector to loop over sequence objects
  *
  * This vector class is used to iterate over a list of other sequence objects.
  * To make use of it, append other sequence objects to this via the += operator and use it together with a loop:
  *   \verbatim
      SeqObjVector objvec;

      SeqPuls      alpha;          // excitation pulse
      SeqDelay     delay;          // delay

      objvec+=alpha;               // put alpha into the vector
      objvec+=delay;               // put delay into the vector

      SeqObjLoop loop;

      loop ( objvec ) [objvec];    // plays out alpha first and then delay
      \endverbatim
  *
  */
class SeqObjVector : public SeqVector, public SeqObjBase, public List<SeqObjBase, const SeqObjBase*, const SeqObjBase&> {

 public:
/**
  * Construct an empty vector of sequence objects with the given label
  */
  SeqObjVector(const STD_string& object_label="unnamedSeqObjVector");


/**
  * Constructs a copy of 'sov'
  */
  SeqObjVector(const SeqObjVector& sov);

/**
  * Assignment operator that makes this sequence container become a copy of 'sov'
  */
  SeqObjVector& operator = (const SeqObjVector& sov);

/**
  * Appends soa to the list of elements in this vector of sequence objects
  */
  SeqObjVector& operator += (const SeqObjBase& soa);

/**
  * Appends sgoa to the list of elements in this vector of sequence objects
  */
  SeqObjVector& operator += (SeqGradObjInterface& sgoa);

/**
  * Appends sgcl to the list of elements in this vector of sequence objects
  */
  SeqObjVector& operator += (SeqGradChanList& sgcl);


  // overloading virtual function from SeqTreeObj
  STD_string get_program(programContext& context) const;
  double get_duration() const;
  unsigned int event(eventContext& context) const;
  void query(queryContext& context) const;
  RecoValList get_recovallist(unsigned int reptimes, LDRkSpaceCoords& coords) const;
  SeqValList get_freqvallist(freqlistAction action) const;
  SeqValList get_delayvallist() const;
  double get_rf_energy() const;


  // implemented virtual functions from SeqClass
  void clear_container();

  // implemented virtual functions from SeqVector
  unsigned int get_vectorsize() const {return List<SeqObjBase, const SeqObjBase*, const SeqObjBase&>::size();}
  bool needs_unrolling_check() const {return true;}
  bool is_obj_vector() const {return true;}
  bool is_acq_vector() const {return true;} // this vector may contain vectors which are relevant for acquisition
  bool is_qualvector() const {return true;} // timing may change

 private:
  constiter get_current() const;
};

#endif
