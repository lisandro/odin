/***************************************************************************
                          seqgradvec.h  -  description
                             -------------------
    begin                : Tue Aug 13 2002
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQGRADVEC_H
#define SEQGRADVEC_H

#include <odinseq/seqgradchan.h>
#include <odinseq/seqvec.h>


/**
  * @addtogroup odinseq
  * @{
  */

/**
  * \brief Vector of constant gradients
  *
  * This class represents a gradient object with a constant gradient
  * shape with the the strength taken from a vector. This object
  * can be attached to a SeqLoop to iterate over the values in the
  * vector.
  */
class SeqGradVector : public SeqGradChan, public SeqVector {

 public:
/**
  * Constructs a gradient vector labeled 'object_label' with the following properties:
  * - gradchannel:   The channel this object should be played out
  * - maxgradstrength:  The maximum gradient strength for this object
  * - trimarray:     The vector of gradient strength values
  * - gradduration:  The duration of this gradient object
  */
  SeqGradVector(const STD_string& object_label,direction gradchannel,
    float maxgradstrength,const fvector& trimarray, double gradduration);

/**
  * Constructs a copy of 'sgv'
  */
  SeqGradVector(const SeqGradVector& sgv);

/**
  * Construct an empty gradient object with the given label
  */
  SeqGradVector(const STD_string& object_label = "unnamedSeqGradVector");

/**
  * Specifies the trim values that will be used
  */
  SeqGradVector& set_trims(const fvector& trims);

/**
  * Returns the trim values that will be used
  */
  fvector get_trims() const {return trimvals;}


/**
  * Assignment operator that makes this gradient channel object become a copy of 'sgv'
  */
  SeqGradVector& operator = (const SeqGradVector& sgv);


 private:
  friend class SeqGradSubVector;
  friend class SeqGradVectorPulse;

  // overwriting virtual functions from SeqClass
  bool prep();

  // overwriting virtual functions from SeqGradChan
  SeqGradChan& get_subchan(double starttime, double endtime) const;
  STD_string get_grdpart(float matrixfactor) const;
  float get_integral() const {return get_current_strength()*get_gradduration();}


  // overwriting virtual functions from SeqVector
  svector get_vector_commands(const STD_string& iterator) const;
  unsigned int get_vectorsize() const;
  bool prep_iteration() const;
  bool is_qualvector() const {return false;}
  svector get_reord_vector_commands(const STD_string& iterator) const;


  float get_current_strength() const;

  const SeqGradVector* parent;

  fvector trimvals;
};




/** @}
  */

/////////////////////////////////////////////////////////////////






#endif
