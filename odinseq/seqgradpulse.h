/***************************************************************************
                          seqgradpulse.h  -  description
                             -------------------
    begin                : Tue Aug 13 2002
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQGRADPULSE_H
#define SEQGRADPULSE_H

#include <odinseq/seqgradconst.h>
#include <odinseq/seqgradvec.h>
#include <odinseq/seqgradchanlist.h>
#include <odinseq/seqgradchanparallel.h>



/**
  * @addtogroup odinseq
  * @{
  */


/////////////////////////////////////////////////////////////////////////////////

/**
  * \brief Constant gradient pulse
  *
  * This class represents a gradient pulse with a constant gradient field
  */
class SeqGradConstPulse : public SeqGradChanList {

 public:
/**
  * Constructs a constant gradient pulse labeled 'object_label' with the following properties:
  * - gradchannel:   The channel this object should be played out
  * - gradstrength:  The gradient strength for this object
  * - gradduration:  The duration of this gradient object
  */
  SeqGradConstPulse(const STD_string& object_label,direction gradchannel, float gradstrength, float gradduration);


/**
  * Constructs a copy of 'sgcp'
  */
  SeqGradConstPulse(const SeqGradConstPulse& sgcp);

/**
  * Construct an empty constant gradient pulse with the given label
  */
  SeqGradConstPulse(const STD_string& object_label = "unnamedSeqGradConstPulse");

/**
  * Assignment operator that makes this constant gradient pulse object become a copy of 'sgcp'
  */
  SeqGradConstPulse& operator = (const SeqGradConstPulse& sgcp);


/**
  * setting duration of the constant part
  */
  SeqGradConstPulse& set_constduration(float duration) {constgrad.set_duration(duration);return *this;};


/**
  * getting duration of the constant part
  */
  double get_constduration() const {return constgrad.get_gradduration();}


  // overloading virtual function from SeqGradInterface
  SeqGradInterface& set_strength(float gradstrength);


 private:
  SeqGradConst constgrad;
  SeqGradDelay offgrad;

};



/////////////////////////////////////////////////////////////////////////////////

/**
  * \brief Vector of gradient pulses
  *
  * This class represents a gradient pulse with a constant gradient
  * shape with the the strength taken from a vector. This object
  * can be attached to a SeqLoop to iterate over the values in the
  * vector.
  */
class SeqGradVectorPulse : public SeqGradChanList {

 public:
/**
  * Constructs a gradient vector pulse labeled 'object_label' with the following properties:
  * - gradchannel:   The channel this object should be played out
  * - maxgradstrength:  The maximum gradient strength for this object
  * - trimarray:     The vector of gradient strength values
  * - gradduration:  The duration of this gradient object
  */
  SeqGradVectorPulse(const STD_string& object_label,direction gradchannel, float maxgradstrength,
                         const fvector& trimarray, float gradduration);

/**
  * Constructs a copy of 'sgvp'
  */
  SeqGradVectorPulse(const SeqGradVectorPulse& sgvp);

/**
  * Construct an empty gradient pulse with the given label
  */
  SeqGradVectorPulse(const STD_string& object_label = "unnamedSeqGradVectorPulse");

/**
  * Assignment operator that makes this constant gradient pulse object become a copy of 'sgvp'
  */
  SeqGradVectorPulse& operator = (const SeqGradVectorPulse& sgvp);


/**
  * Specifies the trim values that will be used
  */
  SeqGradVectorPulse& set_trims(const fvector& trims) { vectorgrad.set_trims(trims); return *this;}

/**
  * Returns the trim values that will be used
  */
  fvector get_trims() const {return vectorgrad.get_trims();}

/**
  * Sets the reordering scheme and the number of segments
  */
  SeqGradVectorPulse& set_reorder_scheme(reorderScheme scheme,unsigned int nsegments=1) {vectorgrad.set_reorder_scheme(scheme,nsegments);  return *this;}


/**
  * Sets the phase encoding scheme and the number of phase encoding steps
  */
  SeqGradVectorPulse& set_encoding_scheme(encodingScheme scheme) {vectorgrad.set_encoding_scheme(scheme);  return *this;}

/**
  * Returns the reordering vector (for loop insertion)
  */
  const SeqVector& get_reorder_vector() const {return vectorgrad.get_reorder_vector();}

/**
  * setting duration of the gradient vector part
  */
  SeqGradVectorPulse& set_constduration(float duration){vectorgrad.set_duration(duration);return *this;};

/**
  * getting duration of the gradient vector part
  */
  double get_constduration() const {return vectorgrad.get_gradduration();}

/**
  * conversion operator for loop insertion
  */
  operator const SeqVector& () const {return vectorgrad;}


  // overloading virtual function from SeqGradInterface
  SeqGradInterface& set_strength(float gradstrength);

 private:
  friend class SeqGradPhaseEnc;
  friend class SeqGradEcho;

  SeqGradVector vectorgrad;
  SeqGradDelay offgrad;
};




/** @}
  */

#endif
