#include "seqmeth.h"
#include "seqacq.h"
#include "seqdelayvec.h"
#include "seqfreq.h"
#include "seqpulsar.h"
#include "seqcmdline.h"

// for gradient intro
#include "seqgradpulse.h"
#include "seqdelay.h"

#include <tjutils/tjprofiler.h>

#include <tjutils/tjhandler_code.h>


//////////////////////////////////////////////////////////////////////

#if defined(HAVE_DL) && defined(HAVE_SIGNAL_H) && defined(HAVE_SETJMP_H)
#define CATCH_SEGFAULT_ON_UNIX
#endif


#ifdef CATCH_SEGFAULT_ON_UNIX
#include <dlfcn.h>
#include <signal.h>
#include <setjmp.h>

// forward declaration function which will be registered as signal handler
void segfaultHandler( int );

// Class to catch segmentation faults in functions of the method plug-in.
// Otherwise the whole ODIN UI would crash...
class CatchSegFaultContext : public StaticHandler<CatchSegFaultContext> {

 public:
  CatchSegFaultContext(const char* contextlabel) {
    Log<Seq> odinlog(contextlabel,"CatchSegFaultContext");
    (*lastmsg)="";
    (*label)=contextlabel;

    sa.sa_handler=segfaultHandler;
//    sa.sa_sigaction=0;
    sa.sa_flags=0;
    sigprocmask(SIG_SETMASK,&sa.sa_mask,0);
/*
#ifndef sgi
    sa.sa_restorer=0;
#endif
*/
    if(sigaction(SIGSEGV,&sa,0)) {
      ODINLOG(odinlog,errorLog) << "unable to register segfaultHandler for " << (*label) << STD_endl;
    }
  }

  ~CatchSegFaultContext() {
    Log<Seq> odinlog(label->c_str(),"~CatchSegFaultContext");
    sa.sa_handler=SIG_DFL;
    sigaction(SIGSEGV,&sa,0);
    segfault_occured=false;
  }

  bool segfault() volatile {
    Log<Seq> odinlog(label->c_str(),"segfault");
    bool result=segfault_occured;
    segfault_occured=false;
    return result;
  }

  static void catch_segfault() {
    Log<Seq> odinlog("","catch_segfault");
    if(lastmsg) {
      (*lastmsg)="Segmentation fault in " + (*label);
      ODINLOG(odinlog,errorLog) << (*lastmsg) << STD_endl;
    }
    segfault_occured=true;
    longjmp(CatchSegFaultContext::segfault_cont_pos, 0);
  }

  static void report_exception(const char* funcname) {
    Log<Seq> odinlog("","report_exception");
    if(lastmsg) {
      (*lastmsg)=STD_string("Exception in ") + funcname;
      ODINLOG(odinlog,errorLog) << (*lastmsg) << STD_endl;
    }
  }

  static const char* get_lastmsg() {
    if(lastmsg) return lastmsg->c_str();
    return 0;
  }

  // functions to initialize/delete static members by the StaticHandler template class
  static void init_static() {
    label=new STD_string;
    lastmsg=new STD_string;
  }


  static void destroy_static() {
    if(label) delete label; label=0;
    if(lastmsg) delete lastmsg; lastmsg=0;
  }

 private:
  friend class SeqMethod;
  friend class SeqMethodProxy;

  static STD_string* label;
  struct sigaction sa;
  static bool segfault_occured;
  static jmp_buf segfault_cont_pos;
  static STD_string* lastmsg;

};

STD_string*    CatchSegFaultContext::label=0;
bool           CatchSegFaultContext::segfault_occured = false;
jmp_buf        CatchSegFaultContext::segfault_cont_pos;
STD_string*    CatchSegFaultContext::lastmsg=0;
EMPTY_TEMPL_LIST bool StaticHandler<CatchSegFaultContext>::staticdone=false;


// function which will be registered as signal handler
void segfaultHandler( int ) {CatchSegFaultContext::catch_segfault();}

// macro to call functions whose segfaults will be catched
#define CATCHSEGFAULT(func,func_name)\
  try {\
    volatile CatchSegFaultContext csfc(func_name);\
    setjmp(CatchSegFaultContext::segfault_cont_pos);\
    if(!csfc.segfault()) func;\
    else return false;\
  } catch(...) {\
    CatchSegFaultContext::report_exception(func_name);\
    return false;\
  }

#else

// simply call the function if we do not have CatchSegFaultContext
#define CATCHSEGFAULT(func,func_name) func;

#endif

//////////////////////////////////////////////////////////////////////


#ifdef USING_WIN32
#ifndef ODIN4IDEA
#define METHOD_IN_DLL
#endif
#endif


#ifdef METHOD_IN_DLL

// function signature
typedef void (*set_global_stuff_fptr) (SingletonMap* sol_map);


#if __cplusplus
extern "C" {
#endif

// the DLL exported function which will be called after loading the DLL
__declspec(dllexport) void set_global_stuff(SingletonMap* sol_map) {

  // NOTE:  Debug uses SingletonHandler -> do not use Debug in here
  SingletonBase::set_singleton_map_external(sol_map);
}

#if __cplusplus
} // extern "C"
#endif

#endif

//////////////////////////////////////////////////////////////////////

/**
  * @ingroup odinseq_internals
  * Fallback method
  */
class SeqEmpty : public SeqMethod {

 public:

  SeqEmpty() : SeqMethod("SeqEmpty") {}

  void method_pars_init() {}
  void method_seq_init() {}
  void method_rels() {}
  void method_pars_set() {}

};


//////////////////////////////////////////////////////////////////////

void SeqMethodProxy::set_current_method(unsigned int index) {
  if(!registered_methods) return;
  unsigned int i=0;
  for(STD_list<SeqMethod*>::iterator it=registered_methods->begin(); it!=registered_methods->end(); ++it) {
    (*it)->clear();
    if(i==index) current_method->ptr=(*it);
    i++;
  }
}

SeqMethod* SeqMethodProxy::get_current_method() {
  if(get_numof_methods()) return current_method->ptr;
  else return  empty_method;
}

unsigned int SeqMethodProxy::get_numof_methods() {
  if(!registered_methods) return 0;
  return registered_methods->size();
}

unsigned int SeqMethodProxy::delete_methods() {
  Log<Seq> odinlog("SeqMethodProxy","delete_methods");
  unsigned int result=get_numof_methods();
  ODINLOG(odinlog,normalDebug) << "deleting " << result << " method(s) ..." << STD_endl;
  if(result) {
    for(STD_list<SeqMethod*>::iterator it=registered_methods->begin(); it!=registered_methods->end(); ++it) {

      // tmp copy of handle
      void* handle=(*it)->dl_handle;

      ODINLOG(odinlog,normalDebug) << "clearing " << (*it)->get_label() << STD_endl;
      (*it)->clear();

      ODINLOG(odinlog,normalDebug) << "deleting " << (*it)->get_label() << STD_endl;
      CATCHSEGFAULT(delete (*it),("~"+STD_string((*it)->get_label())).c_str())

      if(handle!=NULL) {
        if(dlclose(handle)) {
          ODINLOG(odinlog,errorLog) << "dlclose: " << dlerror() << STD_endl;
        }
      }

    }
  }
  registered_methods->erase(registered_methods->begin(),registered_methods->end());

  // clear residual pointers to pulses which do not longer exist
  if(SeqPulsar::active_pulsar_pulses) SeqPulsar::active_pulsar_pulses->clear();

  // clear references which might be left over
  SeqClass::clear_objlists();

  return result;
}

void SeqMethodProxy::register_method(SeqMethod* meth) {
  Log<Seq> odinlog("SeqMethodProxy","register_method");
  if(!get_numof_methods()) current_method->ptr=meth;

  registered_methods->push_back(meth);
  registered_methods->sort();
  registered_methods->unique();
  ODINLOG(odinlog,normalDebug) << "registered " << meth->get_label() << STD_endl;
}

const char* SeqMethodProxy::get_method_label() {return current_method->ptr->get_label().c_str();}

const char* SeqMethodProxy::get_status_string() {
  Log<Seq> odinlog("SeqMethodProxy","get_status_string");
  const char* result=0;
#ifdef CATCH_SEGFAULT_ON_UNIX
  ODINLOG(odinlog,normalDebug) << "obtaining from CatchSegFaultContext" << STD_endl;
  result=CatchSegFaultContext::get_lastmsg();
#endif
  if(!result || (result && STD_string(result)=="")) {
    ODINLOG(odinlog,normalDebug) << "obtaining from current method" << STD_endl;
    result= get_current_method()->get_current_state_label().c_str();
  }
  if(result) ODINLOG(odinlog,normalDebug) << "result=" << result << STD_endl;
  else ODINLOG(odinlog,normalDebug) << "result=0" << STD_endl;
  return result;
}

SeqMethod& SeqMethodProxy::operator [] (unsigned int index) {
  if(!registered_methods) return *empty_method;
  unsigned int i=0;
  for(STD_list<SeqMethod*>::iterator it=registered_methods->begin(); it!=registered_methods->end(); ++it) {
    if(i==index) return *(*it);
    i++;
  }
  return *empty_method;
}


bool SeqMethodProxy::load_method_so(const STD_string& so_filename) {
  Log<Seq> odinlog("SeqMethodProxy","load_method_so");

  int flag=0;
#ifdef HAVE_DL
  flag=RTLD_LAZY;
#endif
  void *handle = dlopen (so_filename.c_str(), flag);
  if(handle==NULL) {
    ODINLOG(odinlog,errorLog) <<  dlerror() << STD_endl;
    return false;
  }

  // remove methods which are still linked
  delete_methods();

#ifdef METHOD_IN_DLL
  // set global map of singleton objects in DLL before calling odinmain
  Profiler profdummy("dummy");
  ODINLOG(odinlog,normalDebug) << "profdummy done" << STD_endl;
  SeqPulsar plsdummy; // create list of registered Pulsar pulses
  ODINLOG(odinlog,normalDebug) << "plsdummy done" << STD_endl;
  SeqAcq acqdummy;    // create acq singletons
  ODINLOG(odinlog,normalDebug) << "acqdummy done" << STD_endl;
//  LogBase::set_log_output_function(dll_tracefunction); // Route debug output through own cerr to get output on shell
  set_global_stuff_fptr set_global_stuff_f=(void (*)(SingletonMap*))dlsym(handle, "set_global_stuff");
  ODINLOG(odinlog,normalDebug) << "set_global_stuff_f=" << (void*)set_global_stuff_f << STD_endl;
  set_global_stuff_f(SingletonBase::get_singleton_map());
  ODINLOG(odinlog,normalDebug) << "set_global_stuff_f called" << STD_endl;
#endif

  // call main of the linked method to initialize it
  int (*odinmain)(int, char**)=(int (*)(int, char**))dlsym(handle, "main");
  ODINLOG(odinlog,normalDebug) << "handle/odinmain=" << (void*)handle << "/" << (void*)odinmain << STD_endl;
  CATCHSEGFAULT(odinmain(0,0),(so_filename+"::odinmain").c_str())

  // store handle
  current_method->ptr->dl_handle=handle;

  return true;
}


void SeqMethodProxy::init_static() {
  Log<Seq> odinlog("SeqMethodProxy","init_static");

  registered_methods.init("registered_methods");
  ODINLOG(odinlog,normalDebug) << "registered_methods allocated" << STD_endl;

  empty_method=new SeqEmpty;
  ODINLOG(odinlog,normalDebug) << "empty_method allocated" << STD_endl;
  
  current_method.init("current_method");
  current_method->ptr=empty_method;

}

void SeqMethodProxy::destroy_static() {
  Log<Seq> odinlog("SeqMethodProxy","destroy_static");

  current_method.destroy();
  registered_methods.destroy();
//  totalDuration.destroy();

  delete empty_method;
}


//////////////////////////////////////////////////////////////////////

template class SingletonHandler<SeqMethodProxy::MethodList,false>;
SingletonHandler<SeqMethodProxy::MethodList,false> SeqMethodProxy::registered_methods;

template class SingletonHandler<SeqMethodProxy::MethodPtr,false>;
SingletonHandler<SeqMethodProxy::MethodPtr,false> SeqMethodProxy::current_method;

SeqMethod* SeqMethodProxy::empty_method=0;

EMPTY_TEMPL_LIST bool StaticHandler<SeqMethodProxy>::staticdone=false;


//////////////////////////////////////////////////////////////////////

SeqMethod::SeqMethod(const STD_string& method_label) : SeqObjList(method_label), StateMachine<SeqMethod>(&empty),
    commonPars(0),methodPars(0), dl_handle(0), protcache(0),
    empty      (this,"Empty",           0,      &SeqMethod::reset),
    initialised(this,"Initialised",&empty,      &SeqMethod::empty2initialised),
    built      (this,"Built",      &initialised,&SeqMethod::initialised2built),
    prepared   (this,"Prepared",   &built,      &SeqMethod::built2prepared)  {
  Log<Seq> odinlog(this,"SeqMethod()");
  set_current_testcase(0);
}


SeqMethod::~SeqMethod() {
  Log<Seq> odinlog(this,"~SeqMethod()");
  clear();
  if(methodPars) delete methodPars;
  if(commonPars) delete commonPars;
  if(protcache) delete protcache;
}


SeqMethod& SeqMethod::append_parameter(LDRbase& ldr,const STD_string& label,parameterMode parmode) {
  ldr.set_label(label);

  LDRblock* blockdum=0; // avoid compiler warning
  blockdum=ldr.cast(blockdum);
  if(!blockdum) ldr.set_parmode(parmode); // Do not change parmode of a whole block

  if(methodPars) methodPars->append(ldr);
  return *this;
}


SeqMethod& SeqMethod::set_sequence(const SeqObjBase& s) {
  SeqObjList::clear();

  if(commonPars->get_GradientIntro()) {

    SeqDelay* tokdelay=new SeqDelay("tokdelay",500.0);
    tokdelay->set_temporary();

    float maxgrad=systemInfo->get_max_grad();
    SeqGradConstPulse* tok1=new SeqGradConstPulse("tok1",readDirection,0.2*maxgrad,1.0);
    tok1->set_temporary();
    SeqGradConstPulse* tok2=new SeqGradConstPulse("tok2",readDirection,0.4*maxgrad,1.0);
    tok2->set_temporary();
    SeqGradConstPulse* tok3=new SeqGradConstPulse("tok3",readDirection,0.6*maxgrad,1.0);
    tok3->set_temporary();

    (*this)+=(*tokdelay);
    (*this)+=(*tok1);
    (*this)+=(*tokdelay);
    (*this)+=(*tok2);
    (*this)+=(*tokdelay);
    (*this)+=(*tok3);
    (*this)+=(*tokdelay);
  }

  (*this)+=s;
  return *this;
}



int SeqMethod::process(int argc, char *argv[]) {
  // Debug already initialized by ODINMETHOD_ENTRY_POINT
  register_method(this);
  if(!argc) return 0;  // process function is only used to register the method
  return SeqCmdLine::process(argc,argv);
}


void SeqMethod::create_protcache() const {
  Log<Seq> odinlog(this,"create_protcache");
  if(!protcache) protcache=new Protocol;
  (*protcache)=Protocol(PROTOCOL_BLOCK_LABEL); // start with a fresh protocol in order to avoid adding method pars multiple times, use same label as in recoInfo

  protcache->system=(*systemInfo);
  ODINLOG(odinlog,normalDebug) << "filemode(protcache->system)=" << protcache->system.grad_reson_center.get_filemode() << STD_endl;
  ODINLOG(odinlog,normalDebug) << "filemode(*systemInfo)=" << systemInfo->grad_reson_center.get_filemode() << STD_endl;

  geometryInfo.copy(protcache->geometry); // use custom copy function of SingletonHandler
  studyInfo.copy(protcache->study); // use custom copy function of SingletonHandler
  if(commonPars) protcache->seqpars=(*commonPars);
  if(methodPars) protcache->methpars.create_copy(*methodPars);
  protcache->append_all_members(); // re-merge all parameters
}

int SeqMethod::load_protocol(const STD_string& filename) {
  Log<Seq> odinlog(this,"load_protocol");
  int npars=0;
  int result=0;
  int retval;
  retval=geometryInfo->load(filename);
  if(retval<0) result=retval; else npars+=retval;

  retval=studyInfo->load(filename);
  if(retval<0) result=retval; else npars+=retval;

  retval=SeqPlatformProxy::load_systemInfo(filename);
  if(retval<0) result=retval; else npars+=retval;

  retval=SeqMethodProxy()->load_sequencePars(filename);
  if(retval<0) result=retval; else npars+=retval;

  if(result>=0) result=npars;

  return result;
}


SeqMethod& SeqMethod::init_systemInfo(double basicfreq,double maxgrad,double slewrate) {
  systemInfo->set_B0_from_freq(basicfreq);
  systemInfo->max_grad=maxgrad;
  systemInfo->max_slew_rate=slewrate;
  return *this;
}


int SeqMethod::write_recoInfo(const STD_string& filename) const {
  create_protcache();
  recoInfo->prot.clear(); // Clear and merge instead of assignment to work around bug in assignment of LDR on MinGW
  recoInfo->prot.merge(*protcache);
  return recoInfo->write(filename);
}


int SeqMethod::write_meas_contex(const STD_string& prefix) const {
  Log<Seq> odinlog(this,"write_meas_contex");
  Profiler prof("write_meas_contex");
  int result=0;
#ifndef NO_FILEHANDLING
//  result+=write_protocol(prefix+"protocol");
//  result+=write_systemInfo(prefix+"systemInfo");
//  result+=write_geometry(prefix+"geometryInfo");
//  result+=write_sequencePars(prefix+"sequencePars");
  result+=write_recoInfo(prefix+"recoInfo");
#endif
  return result;
}

double SeqMethod::get_totalDuration() const {
  if(commonPars) return commonPars->get_ExpDuration();
  return 0.0;
}


int SeqMethod::load_sequencePars(const STD_string& filename) {
  Log<Seq> odinlog(this,"load_sequencePars");
  if(commonPars) {
    commonPars->load(filename);
    commonPars->set_Sequence(get_label()); // restore sequence label
  }
  ODINLOG(odinlog,normalDebug) << "commonPars->load(" << filename << ") done" << STD_endl;
  if(methodPars) methodPars->load(filename);
  ODINLOG(odinlog,normalDebug) << "methodPars->load(" << filename << ") done" << STD_endl;
  set_parblock_labels(); // Overwrite block labels of file
  return 0;
}

int SeqMethod::write_sequencePars(const STD_string& filename) const {
  LDRblock block( (STD_string(get_label())+"_sequencePars") );
  if(commonPars) block.merge(*commonPars);
  if(methodPars) block.merge(*methodPars);
  return block.write(filename);
}


bool SeqMethod::set_sequenceParameter(const STD_string& parameter_label, const STD_string& value) {
  Log<Seq> odinlog(this,"set_sequenceParameter");
  bool result=false;

  // first send it to common pars without prefix
  STD_string parlabel=parameter_label;
  if(commonPars) if(commonPars->LDRblock::parseval(parlabel,value)) result=true;

  // then send it to method pars with prefix
  STD_string prefixstr=STD_string(get_label())+"_";
  if(parameter_label.find(prefixstr)!=0) parlabel=prefixstr+parameter_label;
  if(methodPars) if(methodPars->LDRblock::parseval(parlabel,value)) result=true;

  return result;
}

SeqMethod& SeqMethod::set_commonPars(const SeqPars& pars) {
  (*commonPars)=pars;
  commonPars->set_Sequence(get_label()); // restore sequence label
  return *this;
}


int SeqMethod::load_systemInfo(const STD_string& filename) {
  return SeqPlatformProxy::load_systemInfo(filename);
}

unsigned int SeqMethod::event(eventContext& context) const {
  Log<Seq> odinlog(this,"event");

  unsigned int result=0;

  if(context.action==seqRun) {
    ODINLOG(odinlog,normalDebug) << "calling pre_event of " << platform->get_label() << STD_endl;
    platform->pre_event(context);
    if(context.abort) {ODINLOG(odinlog,errorLog) << "aborting" << STD_endl; return result;} // return immediately
  }

  ODINLOG(odinlog,normalDebug) << "------------------- action=" << context.action << "-------------------" << STD_endl;
  result=SeqObjList::event(context);
  if(context.abort) {ODINLOG(odinlog,errorLog) << "aborting" << STD_endl; return result;} // return immediately

  if(context.action==seqRun) {
    ODINLOG(odinlog,normalDebug) << "calling post_event of " << platform->get_label() << STD_endl;
    platform->post_event(context);
    if(context.abort) {ODINLOG(odinlog,errorLog) << "aborting" << STD_endl; return result;} // return immediately
  }

  return result;
}


void SeqMethod::parameter_relations(LDReditWidget* editwidget) {
#ifdef GUISUPPORT
  clear();
  build();
  editwidget->updateWidget();
#endif
}


STD_list<const SeqPulsar*>  SeqMethod::get_active_pulsar_pulses() const {
  Log<Seq> odinlog(this,"get_active_pulsar_pulses");
  SeqPulsar::PulsarList result;
  if(SeqPulsar::active_pulsar_pulses) SeqPulsar::active_pulsar_pulses.copy(result);
  return result;;
}


unsigned int SeqMethod::get_numof_acquisitions() const {
  Log<Seq> odinlog(this,"get_numof_acquisitions");
  queryContext qc; qc.action=count_acqs;
  SeqObjList::query(qc);
  return qc.numof_acqs;
}

bool SeqMethod::prep_acquisition() const {
  Log<Seq> odinlog(this,"prep_acquisition",significantDebug);
  Profiler prof("prep_acquisition");

  double totaldur=get_totalDuration();
  unsigned int nacqs_total=get_numof_acquisitions();
  ODINLOG(odinlog,infoLog) << "duration=" << totaldur << " min" << STD_endl;
  ODINLOG(odinlog,infoLog) << "numof_acquisitions=" << nacqs_total << STD_endl;


  if(platform->create_recoInfo()) { // do this only if really necessary, i.e. NOT on VxWorks

    (recoInfo->DataFormat)    =platform->get_rawdatatype();
    (recoInfo->RawFile)       =platform->get_rawfile();
    (recoInfo->RawHeaderSize) =platform->get_rawheader_size();
    (recoInfo->ImageProc)     =platform->get_image_proc();

    for(int idir=readDirection; idir<=sliceDirection; idir++) {
      (recoInfo->RelativeOffset)[idir]=secureDivision(geometryInfo->get_offset(direction(idir)),geometryInfo->get_FOV(direction(idir)));
    }

    recoInfo->ChannelScaling=platform->get_acq_channel_scale_factors();

    ODINLOG(odinlog,normalDebug) << "getting k-space ordering from sequence" << STD_endl;
    recoInfo->kSpaceCoords.clear();
    recoInfo->kSpaceOrdering=SeqObjList::get_recovallist(1,recoInfo->kSpaceCoords);

    unsigned int nacqs_recoInfo=recoInfo->get_NumOfAdcChunks();

    if(nacqs_recoInfo!=nacqs_total) {
      ODINLOG(odinlog,errorLog) << "Inconsistent number of acqs: " << nacqs_recoInfo << "!=" << nacqs_total << STD_endl;
      return false;
    }

  }

  // tag repetition loop
  queryContext qc;
  qc.action=tag_toplevel_reploop;
  qc.repetitions_prot=commonPars->get_NumOfRepetitions();
  SeqObjList::query(qc);


  // execute platform-specific initialization
  platform->prepare_measurement(nacqs_total);

  // set current date/time
  studyInfo->set_timestamp();

  return true;
}


bool SeqMethod::reset() {
  Log<Seq> odinlog(this,"reset", significantDebug);
//  SeqClass::clear_messages();
  clear_containers();
  clear_temporary();
  recoInfo->reset();
  return true;
}


bool SeqMethod::empty2initialised() {
  Log<Seq> odinlog(this,"empty2initialised", significantDebug);
  Profiler prof("empty2initialised");

  STD_string method_label(get_label());
  int maxmethname=platform->get_max_methodname_length();
  if(maxmethname>=0 && int(method_label.length())>maxmethname) {
    ODINLOG(odinlog,warningLog) << "Method identifier >" << method_label << "< too long (max=" << maxmethname << " chars), will be cut" << STD_endl;
    STD_string cut=get_label().substr(0,maxmethname);
    set_label(cut);
  }

  if(!commonPars) {
    commonPars=new SeqPars();
    commonPars->set_Sequence(get_label()); // init sequence label
    ODINLOG(odinlog,normalDebug) << "commonPars allocated" << STD_endl;
  }

  if(!methodPars) {
    methodPars=new LDRblock();

    CATCHSEGFAULT(method_pars_init(),"method_pars_init")

    methodPars->set_prefix(get_label());
    ODINLOG(odinlog,normalDebug) << "methodPars allocated" << STD_endl;
  }

  set_parblock_labels();

  platform->init();

  return true;
}


bool SeqMethod::initialised2built() {
  Log<Seq> odinlog(this,"initialised2built", significantDebug);
  Profiler prof("initialised2built");

  ODINLOG(odinlog,significantDebug) <<  "calling method_seq_init()" << STD_endl;
  CATCHSEGFAULT(method_seq_init(),"method_seq_init")

  return calc_timings();
}


void SeqMethod::set_parblock_labels() {
  commonPars->set_label("Common Sequence Parameters");
  methodPars->set_label(STD_string(get_label())+" Sequence Parameters");
}


bool SeqMethod::calc_timings() {
  Log<Seq> odinlog(this,"calc_timings", significantDebug);
  ODINLOG(odinlog,significantDebug) <<  "calling method_rels()" << STD_endl;
  CATCHSEGFAULT(method_rels(),"method_rels")

  double totalDuration_sec=SeqObjList::get_duration()/1000.0;
  ODINLOG(odinlog,normalDebug) <<  "totalDuration_sec=" << totalDuration_sec << STD_endl;
  if(commonPars) commonPars->set_ExpDuration(totalDuration_sec/60.0);
  return true;
}

bool SeqMethod::update_timings() {
  Log<Seq> odinlog(this,"update_timings", significantDebug);
  if(!build()) return false;
  if(!calc_timings()) return false;
  return true;
}


bool SeqMethod::built2prepared() {
  Log<Seq> odinlog(this,"built2prepared", significantDebug);

  ODINLOG(odinlog,significantDebug) <<  "calling method_pars_set()" << STD_endl;
  CATCHSEGFAULT(method_pars_set(),"method_pars_set")

  SeqTreeObj::looplevel=0;

  platform->reset_before_prep();
  return prep_all();
}
