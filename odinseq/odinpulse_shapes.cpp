#include "odinpulse.h"

#include "seqplatform.h"

#ifndef __DBL_EPSILON__
#define __DBL_EPSILON__ 1.e-38
#endif
////////////////////////////////////////////////////
// adiabatic shapes:

/**
  *
  * Sech Pulse
  */
class Sech : public LDRfunctionPlugIn
{

    LDRdouble trunc;
    LDRdouble bw;

public:

    Sech() : LDRfunctionPlugIn ( "Sech" )
    {
        set_description ( "Adiabatic hyperbolic secant pulse." );

        trunc=0.01;
        trunc.set_minmaxval ( 0.001,0.5 ).set_description ( "Relative amplitude at the edges of the pulse" );
        append_member ( trunc,"TruncationLevel" );

        bw=10.0;
        bw.set_minmaxval ( 0.001,100.0 ).set_description ( "Inversion width" ).set_unit ( ODIN_FREQ_UNIT );
        append_member ( bw,"BandWidth" );
    }

    STD_complex calculate_shape ( float s, float Tp ) const
    {

        double beta=2.0*acosh ( secureInv ( trunc ) );
        double my=secureDivision ( PII*Tp*bw,beta );

        double x=beta* ( s-0.5 );
        double sech= ( 1/cosh ( x ) );

        double amplitude=100*sech;
        double phase=my*log ( sech );

        return STD_complex ( amplitude*cos ( phase ),amplitude*sin ( phase ) );
    }

    const shape_info& get_shape_properties() const
    {
        shape_info_retval.adiabatic=true;
        return shape_info_retval;
    }

    // implementing virtual function of LDRfunctionPlugIn
    LDRfunctionPlugIn* clone() const {return new Sech;}
};

////////////////////////////////////////////////////

/**
  *
  * Wurst Pulse
  */
class Wurst : public LDRfunctionPlugIn
{

    LDRdouble ncycles;
    LDRdouble truncpar;

public:
    Wurst() : LDRfunctionPlugIn ( "Wurst" )
    {
        set_description ( "Adiabatic WURST pulse (Kupce and Freeman 1995, JMR A 117:246)" );

        ncycles=10.0;
        ncycles.set_minmaxval ( 1.0,50.0 ).set_description ( "Number of phase cycles" );
        append_member ( ncycles,"NumOfCycles" );

        truncpar=10.0;
        truncpar.set_minmaxval ( 1.0,50.0 ).set_description ( "Truncation Parameter" );
        append_member ( truncpar,"Truncation" );
    }

    STD_complex calculate_shape ( float s, float Tp ) const
    {
        float beta_t,phi,amplitude,ss;

        ss=s-0.5;

        phi=4.0* ( 2.0*PII*ncycles ) *ss*ss;
        beta_t=ss*PII;

        amplitude=1.0-pow ( double ( fabs ( sin ( beta_t ) ) ),double ( truncpar ) );

        return STD_complex ( amplitude*cos ( phi ),amplitude*sin ( phi ) );
    }


    const shape_info& get_shape_properties() const
    {
        shape_info_retval.adiabatic=true;
        return shape_info_retval;
    }

    // implementing virtual function of LDRfunctionPlugIn
    LDRfunctionPlugIn* clone() const {return new Wurst;}
};


////////////////////////////////////////////////////

/**
  *
  * Wurst Pulse
  */
class Fermi : public LDRfunctionPlugIn
{

    LDRdouble width;
    LDRdouble slope;

public:
    Fermi() : LDRfunctionPlugIn ( "Fermi" )
    {
        set_description ( "Fermi pulse for MT and B1 mapping with Bloch-Siegert shift" );


        width=0.75;
        width.set_minmaxval ( 0, 1 ).set_description ( "Distance of Fermi Function turning points (not FWHM)" );
        append_member ( width,"width" );

        slope=80;
        slope.set_minmaxval ( 0,150 ).set_description ( "Exponential factor in Fermi function: influences the slope of the ramps" );
        append_member ( slope,"slope" );


    }
  
    STD_complex calculate_shape ( float s, float Tp ) const
    {
        STD_complex result ( exp ( -width / 2 * slope ) / ( exp ( ( fabs ( s - 0.5 ) - width / 2 ) * slope ) + 1 ), 0 );
        if ( s < __DBL_EPSILON__ || s > 1 - __DBL_EPSILON__ )
            result = STD_complex ( 0,0 );
        return result;
    }


    // implementing virtual function of LDRfunctionPlugIn
    LDRfunctionPlugIn* clone() const {return new Fermi;}
};



////////////////////////////////////////////////////

/**
  *
  * Const Pulse
  */
class ConstPulse : public LDRfunctionPlugIn
{

public:
    ConstPulse() : LDRfunctionPlugIn ( "Const" )
    {
        set_description ( "Constant-amplitude pulse" );
    }

    STD_complex calculate_shape ( float,float ) const
    {
        return STD_complex ( 1.0 );
    }

    STD_complex calculate_shape ( const kspace_coord& ) const
    {
        return STD_complex ( 1.0 );
    }

    // implementing virtual function of LDRfunctionPlugIn
    LDRfunctionPlugIn* clone() const {return new ConstPulse;}
};


////////////////////////////////////////////////////

/**
  *
  * Sinc Pulse
  */
class Sinc : public LDRfunctionPlugIn
{

public:

    LDRdouble thick;

    Sinc() : LDRfunctionPlugIn ( "Sinc" )
    {
        set_description ( "Pulse with a box-car shaped excitation profile" );

        thick=5.0;
        thick.set_minmaxval ( 0.01,200.0 ).set_description ( "Slice thickness" ).set_unit ( ODIN_SPAT_UNIT );
        append_member ( thick,"SliceThickness" );
    }

    STD_complex calculate_shape ( const kspace_coord& tdep ) const
    {
        return STD_complex ( sinc ( 0.5*tdep.kz*thick ) );
    }

    const shape_info& get_shape_properties() const
    {
        shape_info_retval.spatial_extent=thick;
        return shape_info_retval;
    }

    // implementing virtual function of LDRfunctionPlugIn
    LDRfunctionPlugIn* clone() const {return new Sinc;}
};


////////////////////////////////////////////////////

/**
  *
  * Import ASCII
  */
class ImportASCII : public LDRfunctionPlugIn
{

public:

    LDRfileName fname;
    LDRcomplexArr shape;

    ImportASCII() : LDRfunctionPlugIn ( "ImportASCII" )
    {
        set_description ( "Import pulse from ASCII file which must have the format 'amplitude phase amplitude phase ...'. The phase is taken as rad." );

        fname.set_description ( "ASCII file name" );
        append_member ( fname,"FileName" );
    }

    void init_shape()
    {
        if ( fname=="" ) return;
        STD_string sF;
        ::load ( sF,fname );
        svector posstring=tokens ( sF );
        unsigned int numpoints=posstring.size() /2;
        shape.redim ( numpoints );
        for ( unsigned int i=0; i<numpoints; i++ )
        {
            float amp=atof ( posstring[2*i].c_str() );
            float pha=atof ( posstring[2*i+1].c_str() );

            shape[i]=STD_complex ( amp*cos(pha), amp*sin(pha) );

        }
    }

    STD_complex calculate_shape ( float s, float ) const
    {
        unsigned int index= ( unsigned int ) ( s*float ( shape.length()-1 ) );
        if ( index<shape.length() ) return shape[index];
        return STD_complex ( 0.0 );
    }

    STD_complex calculate_shape ( const kspace_coord& tdep ) const
    {
        if ( tdep.index<int ( shape.length() ) ) return shape[tdep.index];
        return STD_complex ( 0.0 );
    }

    const shape_info& get_shape_properties() const
    {
        shape_info_retval.fixed_size=shape.length();
        return shape_info_retval;
    }

    // implementing virtual function of LDRfunctionPlugIn
    LDRfunctionPlugIn* clone() const {return new ImportASCII;}
};


////////////////////////////////////////////////////


/**
  *
  * Import Bruker
  */
class ImportBruker : public LDRfunctionPlugIn
{

public:

    LDRfileName fname;
    LDRcomplexArr shape;

    ImportBruker() : LDRfunctionPlugIn ( "ImportBruker" )
    {
        set_description ( "Import pulse in Bruker format" );

        fname.set_description ( "Bruker pulse file name" );
        append_member ( fname,"FileName" );
    }

    void init_shape()
    {
        if ( fname=="" ) return;
        SeqPlatformProxy::set_current_platform ( paravision );
        OdinPulse sd;
        int errcode=sd.load_rf_waveform ( fname );
        if ( !errcode ) shape=sd.get_B1();
        SeqPlatformProxy::set_current_platform ( standalone );
    }

    STD_complex calculate_shape ( float s, float ) const
    {
        unsigned int index= ( unsigned int ) ( s*float ( shape.length()-1 ) );
        if ( index<shape.length() ) return shape[index];
        return STD_complex ( 0.0 );
    }

    STD_complex calculate_shape ( const kspace_coord& tdep ) const
    {
        if ( tdep.index<int ( shape.length() ) ) return shape[tdep.index];
        return STD_complex ( 0.0 );
    }

    const shape_info& get_shape_properties() const
    {
        shape_info_retval.fixed_size=shape.length();
        return shape_info_retval;
    }

    // implementing virtual function of LDRfunctionPlugIn
    LDRfunctionPlugIn* clone() const {return new ImportBruker;}
};


////////////////////////////////////////////////////
// 2D shapes:

/**
  *
  * Pulse with rectangular excitation profile
  */
class Rect : public LDRfunctionPlugIn
{

public:

    LDRdouble width;
    LDRdouble height;

    Rect() : LDRfunctionPlugIn ( "Rect" )
    {
        set_description ( "Pulse with rectangular excitation profile" );

        width=100.0;
        width.set_minmaxval ( 1.0,500.0 ).set_description ( "Width of the rectangle" ).set_unit ( ODIN_SPAT_UNIT );
        append_member ( width,"RectWidth" );

        height=100.0;
        height.set_minmaxval ( 1.0,500.0 ).set_description ( "Height of the rectangle" ).set_unit ( ODIN_SPAT_UNIT );

        append_member ( height,"RectHeight" );
    }

    STD_complex calculate_shape ( const kspace_coord& tdep ) const
    {
        STD_complex result;
        if ( tdep.kx!=0.0 ) result=STD_complex ( 2.0*sin ( 0.5*tdep.kx* ( width ) ) /tdep.kx );
        else result=STD_complex ( width );
        if ( tdep.ky!=0.0 ) result*=STD_complex ( 2.0*sin ( 0.5*tdep.ky* ( height ) ) /tdep.ky );
        else result*=STD_complex ( height );
        return result;
    }

    const shape_info& get_shape_properties() const
    {
        shape_info_retval.spatial_extent=norm ( width,height ); // diagonal of rectangle
        return shape_info_retval;
    }

    // implementing virtual function of LDRfunctionPlugIn
    LDRfunctionPlugIn* clone() const {return new Rect;}
};


////////////////////////////////////////////////////

/**
  *
  * Pulse with disk shaped excitation profile
  */
class Disk : public LDRfunctionPlugIn
{

public:

    LDRdouble diameter;

    Disk() : LDRfunctionPlugIn ( "Disk" )
    {
        set_description ( "Pulse with disk-shaped profile" );

        diameter=100.0;
        diameter.set_minmaxval ( 1.0,500.0 ).set_description ( "Diameter of the disk" ).set_unit ( ODIN_SPAT_UNIT );
        append_member ( diameter,"Diameter" );
    }

    STD_complex calculate_shape ( const kspace_coord& tdep ) const
    {
        float knorm=norm ( tdep.kx,tdep.ky );
        if ( knorm!=0.0 ) return STD_complex ( double ( diameter ) *0.5*j1 ( knorm*diameter*0.5 ) /knorm );
        return STD_complex ( 0.0 );
    }

    const shape_info& get_shape_properties() const
    {
        shape_info_retval.spatial_extent=diameter;
        return shape_info_retval;
    }

    // implementing virtual function of LDRfunctionPlugIn
    LDRfunctionPlugIn* clone() const {return new Disk;}
};



////////////////////////////////////////////////////

/**
  *
  * Pulse with disk shaped excitation profile
  */
class NPeaks : public LDRfunctionPlugIn
{

public:

    LDRfileName peakfile;
    LDRdouble fox;

    LDRdoubleArr peaks;

    NPeaks() : LDRfunctionPlugIn ( "NPeaks" )
    {
        set_description ( "Pulse with excitation profile consisting of multiple peaks" );

        peakfile.set_description ( "File name" );
        append_member ( peakfile,"PeakFile" );

        fox=200.0;
        fox.set_minmaxval ( 0.0,500.0 ).set_description ( "Maximum extent of subject" ).set_unit ( ODIN_SPAT_UNIT );
        append_member ( fox,"FieldOfExcitation" );
    }

    void init_shape()
    {
        if ( peakfile=="" ) return;
        STD_string sF;
        ::load ( sF,peakfile );
        svector posstring=tokens ( sF );
        unsigned int npeaks=posstring.size() /2;
        peaks.redim ( npeaks,2 );
        for ( unsigned int i=0; i<npeaks; i++ )
        {
            peaks ( i,0 ) =atof ( posstring[2*i].c_str() );
            peaks ( i,1 ) =atof ( posstring[2*i+1].c_str() );
        }
    }

    STD_complex calculate_shape ( const kspace_coord& tdep ) const
    {
        STD_complex result ( 0.0 );
        for ( unsigned int i=0; i<peaks.size ( 0 );i++ )
        {
            float kappa= -fox* ( tdep.kx*peaks ( i,0 ) + tdep.ky*peaks ( i,1 ) );
            result+=STD_complex ( cos ( kappa ),sin ( kappa ) );
        }
        return result;
    }

    const shape_info& get_shape_properties() const
    {
        if ( peaks.size ( 0 ) )
        {
            shape_info_retval.ref_x_pos=fox*peaks ( ( peaks.size ( 0 )-1 ) /2,0 );
            shape_info_retval.ref_y_pos=fox*peaks ( ( peaks.size ( 0 )-1 ) /2,1 );
        }
        shape_info_retval.spatial_extent=sqrt ( 2.0 ) *fox; // diagonal of FOX
        return shape_info_retval;
    }

    // implementing virtual function of LDRfunctionPlugIn
    LDRfunctionPlugIn* clone() const {return new NPeaks;}
};


/////////////////////////////////////////////////////////////////////////////////////////



void LDRshape::init_static()
{

    ( new ConstPulse )->register_function ( shapeFunc,zeroDeeMode ).register_function ( shapeFunc,oneDeeMode ).register_function ( shapeFunc,twoDeeMode );

    ( new ImportASCII )->register_function ( shapeFunc,zeroDeeMode ).register_function ( shapeFunc,oneDeeMode ).register_function ( shapeFunc,twoDeeMode );
    ( new ImportBruker )->register_function ( shapeFunc,zeroDeeMode ).register_function ( shapeFunc,oneDeeMode ).register_function ( shapeFunc,twoDeeMode );

    ( new Sinc )->register_function ( shapeFunc,oneDeeMode );
    ( new Sech )->register_function ( shapeFunc,zeroDeeMode );
    ( new Wurst )->register_function ( shapeFunc,zeroDeeMode );
    ( new Fermi )->register_function ( shapeFunc,zeroDeeMode );

    ( new Rect )->register_function ( shapeFunc,twoDeeMode );
    ( new Disk )->register_function ( shapeFunc,twoDeeMode );
    ( new NPeaks )->register_function ( shapeFunc,twoDeeMode );
}

void LDRshape::destroy_static()
{
    // pulgins will be deleted by LDRfunction
}

EMPTY_TEMPL_LIST bool StaticHandler<LDRshape>::staticdone=false;


