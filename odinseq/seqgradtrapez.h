/***************************************************************************
                          seqgradtrapez.h  -  description
                             -------------------
    begin                : Tue Apr 27 2004
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQGRADTRAPEZ_H
#define SEQGRADTRAPEZ_H

#include <odinseq/seqgradchanlist.h>
#include <odinseq/seqgradchanparallel.h>
#include <odinseq/seqgradramp.h>
#include <odinseq/seqdriver.h>


////////////////////////////////////////////////////////////////////


/**
  * @ingroup odinseq_internals
  * The base class for platform specific drivers of trapezoidal gradient pulses
  */
class SeqGradTrapezDriver : public SeqDriverBase {

 public:
  SeqGradTrapezDriver() {}
  virtual ~SeqGradTrapezDriver() {}

/**
  * Update the driver with the following properties:
  * - channel:    The channel (direction) of the trapez
  * - onrampdur:  Duration of the on-ramp
  * - constdur:   Duration of the flat top
  * - offrampdur: Duration of the off-ramp
  * - strength:   Max gradient strength
  * - timestep:   Dwell time (gradient raster) of the ramps
  * - type:       The shape for the ramps
  * - exclude_offramp_from_timing: Exclude the off-ramp from timing calculations
  */
  virtual bool update_driver(direction channel, double onrampdur, double constdur, double offrampdur, float strength, double timestep, rampType type, bool exclude_offramp_from_timing) = 0;

  virtual float get_onramp_integral (double tmin, double tmax) const = 0;
  virtual float get_offramp_integral(double tmin, double tmax) const = 0;

  virtual const fvector& get_onramp()  const = 0;
  virtual const fvector& get_offramp() const = 0;

  virtual SeqGradChanList get_driverchanlist() = 0;

  virtual bool check_ramptype(rampType type) const = 0;

  virtual SeqGradTrapezDriver* clone_driver() const = 0;
};

////////////////////////////////////////////////////////////////////

/**
  * @ingroup odinseq_internals
  * The Default driver for trapezoidal gradient pulses
  */
class SeqGradTrapezDefault : public SeqGradTrapezDriver, public SeqGradChan {

 public:

  SeqGradTrapezDefault();

  SeqGradTrapezDefault(const SeqGradTrapezDefault& sgtd);

  ~SeqGradTrapezDefault() {}


  // overloading virtual functions from SeqGradTrapezDriver
  bool update_driver(direction channel, double onrampdur, double constdur, double offrampdur, float strength, double timestep, rampType type, bool exclude_offramp_from_timing);
  float get_onramp_integral (double tmin, double tmax) const {return onramp_cache. get_integral(tmin,tmax);}
  float get_offramp_integral(double tmin, double tmax) const {return offramp_cache.get_integral(tmin,tmax);}
  const fvector& get_onramp()  const {return onramp_cache.get_wave();}
  const fvector&  get_offramp() const {return offramp_cache.get_wave();}
  SeqGradChanList get_driverchanlist();
  bool check_ramptype(rampType type) const {return true;}
  SeqGradTrapezDriver* clone_driver() const {return new SeqGradTrapezDefault(*this);}

  // overloading virtual functions from SeqDriverBase
  odinPlatform get_driverplatform() const {return standalone;}

 private:
  // overwriting virtual functions from SeqClass
  bool prep();

  // overloading virtual function from SeqTreeObj
  STD_string get_properties() const;

  // overloading virtual functions from SeqGradInterface
  SeqGradInterface& set_strength(float gradstrength);
  SeqGradInterface& invert_strength();
  float get_strength() const {return onramp_cache.get_strength();}
  double get_gradduration() const;
  SeqGradInterface& set_gradrotmatrix(const RotMatrix& matrix);


  // overloading virtual functions from SeqGradChan
  STD_string get_grdpart(float matrixfactor) const {return graddriver->get_trapez_program(get_strength(),matrixfactor);}
  SeqGradChan& get_subchan(double starttime, double endtime) const;
  direction get_channel() const {return onramp_cache.get_channel();}
  float get_integral() const;

  SeqGradRamp onramp_cache;
  SeqGradRamp offramp_cache;

  double const_dur;
  bool exclude_offramp;

};



////////////////////////////////////////////////////////////////////

/**
  * @addtogroup odinseq
  * @{
  */


/**
  * \brief Trapezoidal gradient
  *
  * This class represents a gradient pulse with a trapezoidal shape, i.e.
  * a piecewise constant gradient with ramps
  */
class SeqGradTrapez : public SeqGradChanList {

 public:

/**
  * Constructs a gradient pulse labeled 'object_label' with the following properties:
  * - gradchannel:   The channel this object should be played out
  * - gradstrength: The maximum gradient strength for this object
  * - constgradduration:  The duration of the constant part of the gradient pulse
  * - timestep:      The time resolution for the ramps
  * - type:          The shape for the ramps
  * - minrampduration: The minimum duration for the ramps
  * - steepness: This parameter in the range of ]0,1] determines the relative rising
  *                  speed of the gradient strength, i.e. with 1 the gradients are switched
  *                  as fast as possible
  */
  SeqGradTrapez(const STD_string& object_label,direction gradchannel,
               float gradstrength, double constgradduration, double timestep=0.01,
               rampType type = linear, double minrampduration=0.0, float steepness=1.0);

/**
  * Constructs a gradient pulse labeled 'object_label' with the following properties:
  * - gradintegral:  The overall integral for the gradient pulse
  * - gradchannel:   The channel this object should be played out
  * - constgradduration:  The duration of the constant part of the gradient pulse
  * - timestep:      The time resolution for the ramps
  * - rampType:      The shape for the ramps
  * - minrampduration: The minimum duration for the ramps
  * - steepness: This parameter in the range of ]0,1] determines the relative rising
  *                  speed of the gradient strength, i.e. with 1 the gradients are switched
  *                  as fast as possible
  */
  SeqGradTrapez(const STD_string& object_label,float gradintegral,
               direction gradchannel, double constgradduration,
               double timestep=0.01, rampType type = linear, double minrampduration=0.0, float steepness=1.0);


/**
  * Constructs a gradient pulse labeled 'object_label' with the following properties:
  * - gradintegral:  The overall integral for the gradient pulse
  * - gradstrength:  The strength of the constant part of the gradient pulse
  * - gradchannel:   The channel this object should be played out
  * - timestep:      The time resolution for the ramps
  * - rampType:      The shape for the ramps
  * - minrampduration: The minimum duration for the ramps
  * - steepness: This parameter in the range of ]0,1] determines the relative rising
  *                  speed of the gradient strength, i.e. with 1 the gradients are switched
  *                  as fast as possible
  */
  SeqGradTrapez(const STD_string& object_label,float gradintegral,
               float gradstrength, direction gradchannel,
               double timestep=0.01, rampType type = linear, double minrampduration=0.0, float steepness=1.0);


/**
  * Constructs a copy of 'sgt'
  */
  SeqGradTrapez(const SeqGradTrapez& sgt);

/**
  * Construct an empty gradient pulse with the given label
  */
  SeqGradTrapez(const STD_string& object_label = "unnamedSeqGradTrapez");

/**
  * Assignment operator that makes this gradient pulse become a copy of 'sgt'
  */
  SeqGradTrapez& operator = (const SeqGradTrapez& sgt);


/**
  * Sets the duration of the constant part of the gradient pulse
  */
  SeqGradTrapez& set_constgrad_duration(double duration);

/**
  * Returns the duration of the ramp that switches the gradient pulse on
  */
  double get_onramp_duration() const {return onrampdur;}


/**
  * Returns the duration of the constant part of the gradient pulse
  */
  double get_constgrad_duration() const {return constdur;}


/**
  * Returns the duration of the ramp that switches the gradient pulse off
  */
  double get_offramp_duration() const {return offrampdur;}


/**
  * Returns the integral of the ramp that switches the gradient pulse on from tmin to tmax
  */
  float get_onramp_integral(double tmin, double tmax) const {return trapezdriver->get_onramp_integral(tmin,tmax);}

/**
  * Returns the integral of the ramp that switches the gradient pulse on
  */
  float get_onramp_integral() const {return get_onramp_integral(0.0,get_onramp_duration());}

/**
  * Returns the integral of the constant part
  */
  float get_constgrad_integral() const {return constdur*trapezstrength;}

/**
  * Returns the integral of the ramp that switches the gradient pulse off from tmin to tmax
  */
  float get_offramp_integral(double tmin, double tmax) const {return trapezdriver->get_offramp_integral(tmin,tmax);}

/**
  * Returns the integral of the ramp that switches the gradient pulse off
  */
  float get_offramp_integral() const {return get_offramp_integral(0.0,get_offramp_duration());}

/**
  * Sets the overall integral of the gradient pulse
  */
  SeqGradTrapez& set_integral(float gradintegral);

/**
  * Returns the overall integral of the gradient pulse
  */
  float get_integral() const;

/**
  * Returns the number of digitised points of the ramp that switches the gradient pulse on
  */
  unsigned int get_onramp_npts() const;

/**
  * Returns the number of digitised points during the constant part
  */
  unsigned int get_const_npts() const;

/**
  * Returns the number of digitised points of the ramp that switches the gradient pulse off
  */
  unsigned int get_offramp_npts() const;

/**
  * Returns the total number of digitised points
  */
  unsigned int get_npts() const;


/**
  * Returns the waveform of the ramp that switches the gradient pulse on
  */
  fvector get_onramp() const {return trapezdriver->get_onramp();}

/**
  * Returns the waveform of the ramp that switches the gradient pulse on
  */
  fvector get_offramp() const {return trapezdriver->get_offramp();}

/**
  * Returns a waveform which is a digitised version of the whole trapezoid
  */
  fvector get_trapezshape() const;

/**
  * Returns the time resolution for the ramps
  */
  double get_timestep() const {return dt;}

/**
  * Returns the shape type of the ramps
  */
  rampType get_ramptype() const {return rampMode;}

/**
  * Spefifies whether to exclude the off-ramp from timing calculations
  */
  SeqGradTrapez& exclude_offramp_from_timing(bool flag);


  // overloading virtual function from SeqGradInterface
  float get_strength() const {return trapezstrength;}
  double get_gradduration() const {return get_onramp_duration()+get_constgrad_duration()+get_offramp_duration();} // override result of SeqGradChanList which may differ due to concatenating other SeqGradChans


 private:
  void common_init();

  void check_platform();
  void update_driver();
  void build_seq();

  static void get_ramps(const STD_string& label, float& rampintegral, double& rampondur, double& rampoffdur, float strength, double dwelltime, rampType ramptype, float steepness, double mindur);


  mutable SeqDriverInterface<SeqGradTrapezDriver> trapezdriver;

  rampType rampMode;
  double dt;
  float steepnessfactor;
  bool exclude_offramp_timing;

  direction trapezchannel;
  double onrampdur;
  double constdur;
  double offrampdur;
  float trapezstrength;
};

////////////////////////////////////////////////////////////////////


/**
  * \brief 3 simultaneous trapezoids
  *
  * A class to place 3 synchronous gradient pulses with
  * trapezoidal shape on all 3 axes simultaneously.
  */
class SeqGradTrapezParallel : public SeqGradChanParallel {

 public:

/**
  * Constructs a gradient pulse labeled 'object_label' with the following properties:
  * - gradintegral_read:  The desired gradient integral on the read channel
  * - gradintegral_phase: The desired gradient integral on the phase channel
  * - gradintegral_slice: The desired gradient integral on the slice channel
  * - maxgradstrength:  The maximum gradient strength
  * - timestep:      The time resolution for the ramps
  * - rampType:      The shape for the ramps
  * - minrampduration: The minimum duration for the ramps
  */
  SeqGradTrapezParallel(const STD_string& object_label,
               float gradintegral_read, float gradintegral_phase, float gradintegral_slice,
               float maxgradstrength, double timestep=0.01,
               rampType type = linear, double minrampduration=0.0);

/**
  * Constructs a copy of 'sgtp'
  */
  SeqGradTrapezParallel(const SeqGradTrapezParallel& sgtp);

/**
  * Construct an empty gradient pulse with the given label
  */
  SeqGradTrapezParallel(const STD_string& object_label = "unnamedSeqGradTrapezParallel");

/**
  * Assignment operator that makes this gradient pulse become a copy of 'sgtp'
  */
  SeqGradTrapezParallel& operator = (const SeqGradTrapezParallel& sgtp);


 private:
  void build_seq();

  SeqGradTrapez readgrad;
  SeqGradTrapez phasegrad;
  SeqGradTrapez slicegrad;


};

/** @}
  */


#endif
