/***************************************************************************
                          seqgradwave.h  -  description
                             -------------------
    begin                : Tue Aug 13 2002
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQGRADWAVE_H
#define SEQGRADWAVE_H

#include <odinseq/seqgradchan.h>


/**
  * @addtogroup odinseq
  * @{
  */

/**
  * \brief Gradient Waveform
  *
  * This class represents a gradient object with an arbitrarily shaped gradient field
  * The physical gradient strengths of one data point is the product of the value
  * of the waveform at that point and the gradient strength of this object
  */
class SeqGradWave : public SeqGradChan {

 public:

/**
  * Constructs a gradient waveform labeled 'object_label' with the following properties:
  * - gradchannel:   The channel this object should be played out
  * - gradduration:  The duration of this gradient object
  * - maxgradstrength:  The maximum gradient strength for this object
  * - waveform:      The waveform that will be played out
  */
  SeqGradWave(const STD_string& object_label,direction gradchannel, double gradduration,
           float maxgradstrength,const fvector& waveform);


/**
  * Constructs a copy of 'sgw'
  */
  SeqGradWave(const SeqGradWave& sgw);

/**
  * Construct an empty gradient object with the given label
  */
  SeqGradWave(const STD_string& object_label = "unnamedSeqGradWave");

/**
  * Specifies the waveform that will be used
  */
  SeqGradWave& set_wave(const fvector& waveform);

/**
  * Returns the waveform that will be used
  */
  const fvector& get_wave() const {return wave;}


/**
  * Returns the integral of the waveform time tmin to time tmax (relative to the starting point of the ramp)
  */
  float get_integral(double tmin, double tmax) const;


/**
  * Returns the number of digitised points for the waveform
  */
  unsigned int get_npts() const;

/**
  * Returns the number of digitised points for the waveform
  */
  unsigned int size() const {return get_npts();}

/**
  * Returns the sampling rate of the waveform
  */
  double get_timestep() const {return secureDivision(get_gradduration(),double(get_npts()));}

/**
  * Returns the i'th element of the waveform
  */
  float operator [] (unsigned int i) const {return wave[i];}


/**
  * Assignment operator that makes this gradient channel object become a copy of 'sgw'
  */
  SeqGradWave& operator = (const SeqGradWave& sgw);


 protected:
  fvector wave;

 private:

  // overwriting virtual functions from SeqClass
  bool prep();

  // overwriting virtual functions from SeqGradChan
  int get_wavesize() const;
  void resize(unsigned int newsize);
  SeqGradChan& get_subchan(double starttime, double endtime) const;
  STD_string get_grdpart(float matrixfactor) const;
  float get_integral() const {return get_integral(0.0,get_gradduration());}

  void check_wave();
 
};


/** @}
  */

#endif
