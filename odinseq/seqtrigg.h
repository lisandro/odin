/***************************************************************************
                          seqtrigg.h  -  description
                             -------------------
    begin                : Tue Aug 13 2002
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQTRIGG_H
#define SEQTRIGG_H

#include <odinseq/seqobj.h>
#include <odinseq/seqdriver.h>


//////////////////////////////////////////////////////////////////////

/**
  * @ingroup odinseq_internals
  * The base class for platform specific drivers of external triggers
  */
class SeqTriggerDriver : public SeqDriverBase {

 public:
  SeqTriggerDriver() {}
  virtual ~SeqTriggerDriver() {}

  virtual double get_postduration() const = 0;

  virtual bool prep_exttrigger(double duration) = 0;

  virtual bool prep_halttrigger() = 0;

  virtual bool prep_snaptrigger(const STD_string& snapshot_fname) = 0;

  virtual bool prep_resettrigger() = 0;

  virtual void event(eventContext& context, double start) const = 0;

  virtual STD_string get_program(programContext& context) const = 0;

  virtual SeqTriggerDriver* clone_driver() const = 0;
};




//////////////////////////////////////////////////////////////////////


/**
  * @addtogroup odinseq
  * @{
  */

/**
  *
  * \brief Output trigger
  *
  * Insert triggers for external devices into the sequence.
  */
class SeqTrigger : public SeqObjBase {

 public:

/**
  * Constructs a trigger labeled 'object_label' with the following properties:
  * - duration: The duration of the trigger pulse
  */
  SeqTrigger(const STD_string& object_label, double duration);

/**
  * Default Constructor
  */
  SeqTrigger(const STD_string& object_label = "unnamedSeqTrigger");

/**
  * Constructs a trigger which is a copy of 'st'
  */
  SeqTrigger(const SeqTrigger& st) {SeqTrigger::operator = (st);}

/**
  * This assignment operator will make this object become an exact copy of 'st'.
  */
  SeqTrigger& operator = (const SeqTrigger& st);

  // overloading virtual function from SeqTreeObj
  STD_string get_program(programContext& context) const;
  double get_duration() const;
  unsigned int event(eventContext& context) const;


 private:

  // overwriting virtual functions from SeqClass
  bool prep();

  mutable SeqDriverInterface<SeqTriggerDriver> triggdriver;

  double triggdur;

};

//////////////////////////////////////////////////////////////////////

/**
  *
  * \brief Input trigger
  *
  * Halt sequence until an input trigger is received (e.g. cardiac gating).
  */
class SeqHalt : public SeqObjBase {

 public:

/**
  * Constructs a trigger labeled 'object_label'
  */
  SeqHalt(const STD_string& object_label = "unnamedSeqHalt");

/**
  * Constructs a trigger which is a copy of 'sh'
  */
  SeqHalt(const SeqHalt& sh) {SeqHalt::operator = (sh);}

/**
  * This assignment operator will make this object become an exact copy of 'sh'.
  */
  SeqHalt& operator = (const SeqHalt& sh);

  // overloading virtual function from SeqTreeObj
  STD_string get_program(programContext& context) const;
  double get_duration() const;
  unsigned int event(eventContext& context) const;


 private:

  // overwriting virtual functions from SeqClass
  bool prep();

  mutable SeqDriverInterface<SeqTriggerDriver> triggdriver;
};

//////////////////////////////////////////////////////////////////////


/**
  *
  * \brief Magnetization snapshot
  *
  * This class is used to trigger a snapshot of the current magnetization
  * during simulation of the sequence.
  */
class SeqSnapshot : public SeqObjBase {

 public:

/**
  * Constructs a trigger labeled 'object_label' with the following properties:
  * - snapshot_fname: The file name where the magnetization will be stored consecutively in float format.
  */
  SeqSnapshot(const STD_string& object_label, const STD_string& snapshot_fname);

/**
  * Default Constructor
  */
  SeqSnapshot(const STD_string& object_label = "unnamedSeqSnapshot");

/**
  * Constructs a snapshot trigger which is a copy of 'ss'
  */
  SeqSnapshot(const SeqSnapshot& ss) {SeqSnapshot::operator = (ss);}

/**
  * This assignment operator will make this object become an exact copy of 'ss'.
  */
  SeqSnapshot& operator = (const SeqSnapshot& ss);

  // overloading virtual function from SeqTreeObj
  double get_duration() const {return 0.0;}
  unsigned int event(eventContext& context) const;


 private:

  // overwriting virtual functions from SeqClass
  bool prep();

  STD_string magn_fname;

  mutable SeqDriverInterface<SeqTriggerDriver> triggdriver;

};

//////////////////////////////////////////////////////////////////////


/**
  *
  * \brief Magnetization reset
  *
  * This class is used to reset the current magnetization to its initial equilibrium state
  * during simulation of the sequence.
  */
class SeqMagnReset : public SeqObjBase {

 public:

/**
  * Constructs a trigger labeled 'object_label'
  */
  SeqMagnReset(const STD_string& object_label = "unnamedSeqMagnReset");

/**
  * Constructs a snapshot trigger which is a copy of 'smr'
  */
  SeqMagnReset(const SeqMagnReset& smr) {SeqMagnReset::operator = (smr);}

/**
  * This assignment operator will make this object become an exact copy of 'smr'.
  */
  SeqMagnReset& operator = (const SeqMagnReset& smr);

  // overloading virtual function from SeqTreeObj
  double get_duration() const {return 0.0;}
  unsigned int event(eventContext& context) const;


 private:

  // overwriting virtual functions from SeqClass
  bool prep();

  mutable SeqDriverInterface<SeqTriggerDriver> triggdriver;

};

/** @}
  */


#endif
