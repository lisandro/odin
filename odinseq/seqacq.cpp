#include "seqacq.h"
#include "seqdelay.h"
#include "seqmeth.h"

#include <tjutils/tjfeedback.h>
#include <tjutils/tjhandler_code.h>



void SeqAcq::common_init() {
  sweep_width=0.0;
  npts=0;
  oversampl=1.0;
  rel_center=0.5;
  reflect_flag=false;
  readoutIndex=-1;
  trajIndex=-1;
  weightIndex=-1;

  dimvec=new Handler<const SeqVector*>*[n_recoIndexDims];
  for(int i=0; i<n_recoIndexDims; i++) {
    dimvec[i]=new Handler<const SeqVector*>;
    default_recoindex[i]=0;
  }
}


SeqAcq::~SeqAcq() {
  for(int i=0; i<n_recoIndexDims; i++) delete dimvec[i];
  delete[] dimvec;
}


SeqAcq::SeqAcq(const STD_string& object_label,unsigned int nAcqPoints,double sweepwidth, float os_factor,
                              const STD_string& nucleus,const dvector& phaselist,
                              const dvector& freqlist)
 : SeqObjBase(object_label), SeqFreqChan(object_label,nucleus,freqlist,phaselist),
   acqdriver(object_label) {
  common_init();
  SeqAcq::set_sweepwidth(sweepwidth, os_factor); // Adjust sweepwidth, do not use virtual functions in constructor
  set_npts(nAcqPoints);  // Catch warning
}



SeqAcq::SeqAcq(const SeqAcq& sa) : acqdriver(STD_string(sa.get_label())) {
  common_init();
  SeqAcq::operator = (sa);
}

SeqAcq::SeqAcq(const STD_string& object_label) : SeqObjBase(object_label), SeqFreqChan(object_label),
   acqdriver(object_label) {
  common_init();
}

SeqAcqInterface& SeqAcq::set_readout_shape(const fvector& shape, unsigned int dstsize) {
  if(oversampl>1.0) {
    unsigned int adcsize_os=(unsigned int)(oversampl*shape.size()+0.5);
    fvector newshape(shape);
    newshape.interpolate(adcsize_os);
    readoutIndex=recoInfo->append_readout_shape(newshape,dstsize);
  } else {
    readoutIndex=recoInfo->append_readout_shape(shape,dstsize);
  }
  return *this;
}


void SeqAcq::set_kspace_traj(const farray& kspaceTraj) {
  Log<Seq> odinlog(this,"set_kspace_traj");
  if(kspaceTraj.dim()!=3) {
    ODINLOG(odinlog,errorLog) << "Dimension of kspaceTraj != 3" << STD_endl;
    return;
  }
  if(kspaceTraj.size(2)!=3) {
    ODINLOG(odinlog,errorLog) << "Third dimension of kspaceTraj != 3" << STD_endl;
    return;
  }

  unsigned int trajsize=kspaceTraj.size(1);
  if(trajsize!=npts) {
    ODINLOG(odinlog,warningLog) << "size mismatch : " << trajsize << "!=" << npts << STD_endl;
  }

  trajIndex=recoInfo->append_kspace_traj(kspaceTraj);
}


void SeqAcq::set_weight_vec(const cvector& weightVec) {
  Log<Seq> odinlog(this,"set_weight_vec");
  unsigned int vecsize=weightVec.length();
  if(vecsize!=npts) {
    ODINLOG(odinlog,warningLog) << "size mismatch : " << vecsize << "!=" << npts << STD_endl;
  }
  weightIndex=recoInfo->append_adc_weight_vec(weightVec);
}


double SeqAcq::get_acquisition_center() const {
  Log<Seq> odinlog(this,"get_acquisition_center");
  double centerpts=rel_center*double(npts);
  ODINLOG(odinlog,normalDebug) << "centerpts=" << centerpts << STD_endl;
  double result=get_acquisition_start()+secureDivision(centerpts,sweep_width);
  ODINLOG(odinlog,normalDebug) << "result=" << result << STD_endl;
  return result;
}

SeqAcqInterface& SeqAcq::set_sweepwidth(double sw, float os_factor) {
  Log<Seq> odinlog(this,"set_sweepwidth");
  double possible_sw_os=acqdriver->adjust_sweepwidth(sw*os_factor);
  ODINLOG(odinlog,normalDebug) << "possible_sw_os/sw/os_factor=" << possible_sw_os << "/" << sw << "/" << os_factor << STD_endl;
  sweep_width=secureDivision(possible_sw_os,os_factor);
  oversampl=STD_max(float(1.0),os_factor);
  return *this;
}

SeqAcqInterface& SeqAcq::set_reco_vector(recoDim dim, const SeqVector& vec, const dvector& valvec) {
  Log<Seq> odinlog(this,"set_reco_vector");
  if(dim<n_recoIndexDims) {
    dimvec[dim]->set_handled(&vec);
    ODINLOG(odinlog,normalDebug) << "valvec(" << recoDimLabel[dim] << ")=" << valvec.printbody() << STD_endl;
    recoInfo->set_DimValues(dim,valvec);
  } else {
    ODINLOG(odinlog,warningLog) << "dim=" << dim << " out of range" << STD_endl;
  }
  return *this;
}


SeqAcqInterface& SeqAcq::set_default_reco_index(recoDim dim, unsigned int index) {
  Log<Seq> odinlog(this,"set_default_reco_index");
  if(dim<n_recoIndexDims) {
    default_recoindex[dim]=index;
  } else {
    ODINLOG(odinlog,warningLog) << "dim=" << dim << " out of range" << STD_endl;
  }
  return *this;
}


SeqAcq& SeqAcq::set_npts(unsigned int nAcqPoints) {
  Log<Seq> odinlog(this,"set_npts");
  ODINLOG(odinlog,normalDebug) << "nAcqPoints=" << nAcqPoints << STD_endl;
  npts=nAcqPoints;
  if(!npts) {
    ODINLOG(odinlog,warningLog) << "Zero sampling points" << STD_endl;
  }
  return *this;
}



SeqAcq& SeqAcq::operator = (const SeqAcq& sa) {
  SeqObjBase::operator = (sa);
  SeqFreqChan::operator = (sa);

  sweep_width=sa.sweep_width;
  npts=sa.npts;
  oversampl=sa.oversampl;
  rel_center=sa.rel_center;
  reflect_flag=sa.reflect_flag;
  readoutIndex=sa.readoutIndex;
  trajIndex=sa.trajIndex;
  weightIndex=sa.weightIndex;

  for(int i=0; i<n_recoIndexDims; i++) default_recoindex[i]=sa.default_recoindex[i];

  acqdriver=sa.acqdriver;

  return *this;
}


bool SeqAcq::prep() {
  Log<Seq> odinlog(this,"prep");

  if(!SeqFreqChan::prep()) return false;

  kcoord.oversampling=oversampl;
  kcoord.relcenter=rel_center;
  kcoord.adcSize=int(float(npts)*kcoord.oversampling+0.5);
  if(reflect_flag)   kcoord.flags=kcoord.flags|recoReflectBit;

  kcoord.readoutIndex=readoutIndex;
  kcoord.trajIndex=trajIndex;
  kcoord.weightIndex=weightIndex;

  kcoord.dtIndex=recoInfo->append_dwell_time(secureInv(oversampl*sweep_width));

  kcoord.channels=acqdriver->get_numof_channels();

  // For slicetime functor in odinreco:
  // store slice order in DimValues of recoInfo according to vector in slice dimension
  const SeqVector* vec=dimvec[slice]->get_handled();
  if(vec) {
    ivector ivec(vec->get_index_matrix());
    dvector ivals(ivec.size());
    for(unsigned int i=0; i<ivec.size(); i++) ivals[i]=ivec[i];
    recoInfo->set_DimValues(slice,ivals);
  }

  return acqdriver->prep_driver(kcoord, oversampl*sweep_width, (unsigned int)(oversampl*npts+0.5), get_acquisition_center(), get_channel());
}


STD_string SeqAcq::get_program(programContext& context) const {
  STD_string result=SeqFreqChan::get_pre_program(context,acqObj,acqdriver->get_instr_label());
  result+=acqdriver->get_program(context,get_phaselistindex());
  return result;
}

double SeqAcq::get_duration() const {
  double result=acqdriver->get_predelay();
  result+=get_freqchan_duration();
  result+=acqdriver->get_postdelay(oversampl*sweep_width);
  return result;
}


SeqValList SeqAcq::get_freqvallist(freqlistAction action) const {
  Log<Seq> odinlog(this,"get_freqvallist");
  SeqValList result(get_label());
  double newfreq=SeqFreqChan::get_frequency();

  if(action==calcAcqList) {
    result.set_value(newfreq);
    ODINLOG(odinlog,normalDebug) << "freq=" << newfreq << STD_endl;
  }

  return result;
}

const kSpaceCoord& SeqAcq::get_kcoord() const {
  Log<Seq> odinlog(this,"get_kcoord");

  // fill reco index with values of the attached vectors
  for(int i=0; i<n_recoIndexDims; i++) {
    const SeqVector* vec=dimvec[i]->get_handled();
    if(vec) kcoord.index[i]=vec->get_acq_index();
    else    kcoord.index[i]=default_recoindex[i];
  }
  return kcoord;
}



RecoValList SeqAcq::get_recovallist(unsigned int reptimes, LDRkSpaceCoords& coords) const {
  Log<Seq> odinlog(this,"get_recovallist");
  kSpaceCoord idx(get_kcoord());
  idx.reps=reptimes;
  coords.append_coord(idx);
  RecoValList result(get_label());
  result.set_value(idx.number);
  return result;
}


unsigned int SeqAcq::event(eventContext& context) const {
  Log<Seq> odinlog(this,"event");

  double startelapsed=context.elapsed;
  SeqTreeObj::event(context);

  if(context.action==seqRun) {
    SeqFreqChan::pre_event(context,startelapsed);
    ODINLOG(odinlog,normalDebug) << "startelapsed/get_acquisition_start()=" << startelapsed << "/" << get_acquisition_start() << STD_endl;
    acqdriver->event(context,startelapsed+get_acquisition_start());
    SeqFreqChan::post_event(context,startelapsed+get_acquisition_start()+get_freqchan_duration());
  }

  context.increase_progmeter();
  return 1;
}

STD_string SeqAcq::get_properties() const {
  return "SweepWidth="+ftos(sweep_width)+", Samples="+itos(npts)+", OverSampling="+ftos(oversampl);
}

