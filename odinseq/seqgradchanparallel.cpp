#include "seqgradchanparallel.h"
#include "seqgradconst.h"

SeqGradChanParallel::SeqGradChanParallel(const STD_string& object_label)
  : SeqGradObjInterface(object_label), paralleldriver(object_label) {
}

SeqGradChanParallel::SeqGradChanParallel(const SeqGradChanParallel& sgcp)
 : paralleldriver(sgcp.get_label()) {
  Log<Seq> odinlog(this,"SeqGradChanParallel");
  SeqGradChanParallel::operator = (sgcp);
}


SeqGradChanParallel::~SeqGradChanParallel() {
  Log<Seq> odinlog(this,"~SeqGradChanParallel");
  for(unsigned int cn=readDirection; cn <= sliceDirection; cn++) {
    direction chanNo=direction(cn);
    if(get_gradchan(chanNo)) get_gradchan(chanNo)->clear();
  }
}


SeqGradChanParallel& SeqGradChanParallel::operator = (const SeqGradChanParallel& sgcp) {
  Log<Seq> odinlog(this,"operator = (...)");
  SeqGradObjInterface::operator = (sgcp);
  paralleldriver=sgcp.paralleldriver;
  clear();

  // create deep copy
  for(unsigned int cn=readDirection; cn <= sliceDirection; cn++) {
    direction chanNo=direction(cn);
    SeqGradChanList* sgcl=sgcp.get_gradchan(chanNo);
    if(sgcl) {
      SeqGradChanList* my_sgcl=get_gradchan(chanNo);
      if(my_sgcl) (*my_sgcl)=(*sgcl);
      else {
        ODINLOG(odinlog,normalDebug) << "creating copy of " << sgcl->get_label() << STD_endl;
        SeqGradChanList* sgcl_copy=new SeqGradChanList(*sgcl);
        sgcl_copy->set_temporary();
        set_gradchan(chanNo,sgcl_copy);
      }
    }
  }
  return *this;
}


SeqGradChanParallel& SeqGradChanParallel::operator /= (SeqGradChan& sgc) {
  Log<Seq> odinlog(this,"operator /= (SeqGradChan&)");
  direction chanNo=sgc.get_channel();
  SeqGradChanList* sgcl=get_gradchan(chanNo);
  if(sgcl) {
    sgcl->clear();
  } else {
    sgcl=new SeqGradChanList("("+sgc.get_label()+")");
    sgcl->set_temporary();
    set_gradchan(chanNo,sgcl);
  }
  (*sgcl)+=sgc;
  return *this;
}


SeqGradChanParallel& SeqGradChanParallel::operator /= (SeqGradChanList& sgcl) {
  Log<Seq> odinlog(this,"operator /= (SeqGradChanList&)");
  set_gradchan(sgcl.get_channel(),&sgcl);
  return *this;
}


STD_string SeqGradChanParallel::get_program(programContext& context) const {
  return paralleldriver->get_program(context);
}


bool SeqGradChanParallel::prep() {
  Log<Seq> odinlog(this,"prep");

  if(!SeqGradObjInterface::prep()) return false;

  SeqGradChanList* chanlists[3];
  for(unsigned int i=0; i<3; i++) chanlists[i]=get_gradchan(direction(i)); 
  
  return paralleldriver->prep_driver(chanlists);
}


SeqGradInterface& SeqGradChanParallel::set_strength(float gradstrength) {
  Log<Seq> odinlog(this,"set_strength");
  for(unsigned int cn=readDirection; cn <= sliceDirection; cn++) {
    direction chanNo=direction(cn);
    if(get_gradchan(chanNo)) get_gradchan(chanNo)->set_strength(gradstrength);
  }
  return *this;
}

SeqGradInterface& SeqGradChanParallel::invert_strength() {
  Log<Seq> odinlog(this,"invert_strength");
  for(unsigned int cn=readDirection; cn <= sliceDirection; cn++) {
    direction chanNo=direction(cn);
    if(get_gradchan(chanNo)) get_gradchan(chanNo)->invert_strength();
  }
  return *this;
}


float SeqGradChanParallel::get_strength() const {
  Log<Seq> odinlog(this,"get_strength");
  float result=0.0;
  for(unsigned int cn=readDirection; cn <= sliceDirection; cn++) {
    direction chanNo=direction(cn);
    float tmp=0.0;
    if(get_gradchan(chanNo)) tmp=get_gradchan(chanNo)->get_strength();
    if(fabs(tmp)>fabs(result)) result=tmp;
  }
  return result;
}

double SeqGradChanParallel::get_gradduration() const {
  Log<Seq> odinlog(this,"get_gradduration");
  double result=0.0;
  for(unsigned int cn=readDirection; cn <= sliceDirection; cn++) {
    direction chanNo=direction(cn);
    double chandur=0.0;
    if(get_gradchan(chanNo)) chandur=fabs(get_gradchan(chanNo)->get_gradduration());
    if( chandur > result) result=chandur;
  }
  return result;
}

SeqGradInterface& SeqGradChanParallel::set_gradrotmatrix(const RotMatrix& matrix) {
  Log<Seq> odinlog(this,"set_gradrotmatrix");
  for(unsigned int cn=readDirection; cn <= sliceDirection; cn++) {
    direction chanNo=direction(cn);
    if(get_gradchan(chanNo)) get_gradchan(chanNo)->set_gradrotmatrix(matrix);
  }
  return *this;
}

fvector SeqGradChanParallel::get_gradintegral() const {
  Log<Seq> odinlog(this,"get_gradintegral");
  fvector result(3);
  result=0.0;
  for(unsigned int cn=readDirection; cn <= sliceDirection; cn++) {
    direction chanNo=direction(cn);
    if(get_gradchan(chanNo)) result=result+(get_gradchan(chanNo)->get_gradintegral());
  }
  ODINLOG(odinlog,normalDebug) << "result=" << result.printbody() << STD_endl;
  return result;
}


SeqGradChanParallel& SeqGradChanParallel::operator += (SeqGradChan& sgc) {
  Log<Seq> odinlog(this,"SeqGradChanParallel::operator += (SeqGradChan)");
  ODINLOG(odinlog,normalDebug) << "appending " << sgc.get_label() << STD_endl;
  direction chanNo=sgc.get_channel();
  padd_channel_with_delay(chanNo, get_gradduration());
  if(get_gradchan(chanNo)) {
    (*get_gradchan(chanNo))+=sgc;
  } else {
    SeqGradChanList* sgcl=new SeqGradChanList(STD_string("(")+get_label()+")");
    sgcl->set_temporary();
    (*sgcl)+=sgc;
    set_gradchan(chanNo,sgcl);
  }
  return *this;
}


SeqGradChanParallel& SeqGradChanParallel::operator += (SeqGradChanList& sgcl) {
  Log<Seq> odinlog(this,"SeqGradChanParallel::operator += (SeqGradChanList)");
  direction chanNo=sgcl.get_channel();
  padd_channel_with_delay(chanNo, get_gradduration());
  if(get_gradchan(chanNo)) {
    ODINLOG(odinlog,normalDebug) << "appending " << sgcl.get_label() << STD_endl;
    (*get_gradchan(chanNo))+=sgcl;
  } else {
    ODINLOG(odinlog,normalDebug) << "creating copy of " << sgcl.get_label() << STD_endl;
    SeqGradChanList* sgcl_copy=new SeqGradChanList(sgcl);
    sgcl_copy->set_temporary();
    set_gradchan(chanNo,sgcl_copy);
  }
  return *this;
}

SeqGradChanParallel& SeqGradChanParallel::operator += (SeqGradChanParallel& sgcp) {
  Log<Seq> odinlog(this,"operator += (SeqGradChanParallel)");
  ODINLOG(odinlog,normalDebug) << "appending " << sgcp.get_label() << STD_endl;
  double maxdur=get_gradduration();
  for(unsigned int cn=readDirection; cn <= sliceDirection; cn++) {
    direction chanNo=direction(cn);
    if(sgcp.get_gradchan(chanNo)) {
      padd_channel_with_delay(chanNo,maxdur);
      if(get_gradchan(chanNo)) (*get_gradchan(chanNo))+=(*sgcp.get_gradchan(chanNo));
      else {
        ODINLOG(odinlog,normalDebug) << "creating copy of " << sgcp.get_label() << STD_endl;
        SeqGradChanList* sgcl_copy=new SeqGradChanList(*(sgcp.get_gradchan(chanNo)));
        sgcl_copy->set_temporary();
        set_gradchan(chanNo,sgcl_copy);
      }
    }
  }
  return *this;
}



void SeqGradChanParallel::clear() {
  Log<Seq> odinlog(this,"clear");
  for(unsigned int cn=readDirection; cn <= sliceDirection; cn++) {
    gradchan[cn].clear_handledobj();
  }
}


unsigned int SeqGradChanParallel::event(eventContext& context) const {
  Log<Seq> odinlog(this,"event");
  unsigned int result=0;
  double startelapsed=context.elapsed;
  double maxelapsed=context.elapsed;
  for(unsigned int cn=readDirection; cn <= sliceDirection; cn++) {
    context.elapsed=startelapsed;
    direction chanNo=direction(cn);
    if(get_gradchan(chanNo)) {
      result+=get_gradchan(chanNo)->event(context);
      if(context.abort) {ODINLOG(odinlog,errorLog) << "aborting" << STD_endl; return result;} // return immediately
      if(context.elapsed>maxelapsed) maxelapsed=context.elapsed;
    }
  }
  context.elapsed=maxelapsed;
  return result;
}

void SeqGradChanParallel::clear_container() {clear();}



void SeqGradChanParallel::query(queryContext& context) const {
  SeqTreeObj::query(context); // defaults
  if(context.action==count_acqs) return; // nothing to do

  context.treelevel++;

  for(unsigned int cn=readDirection; cn <= sliceDirection; cn++) {
    context.parentnode=this; // reset to this because it might changed by last query
    SeqGradChanList* sgcl=get_gradchan(direction(cn));
    if(sgcl) sgcl->query(context);
  }

  context.treelevel--;
}


STD_string SeqGradChanParallel::get_properties() const {
  STD_string result="ChanListSize=";
  for(unsigned int cn=readDirection; cn <= sliceDirection; cn++) {
    direction chanNo=direction(cn);
    if(get_gradchan(chanNo)) result+=itos(get_gradchan(chanNo)->size());
    else result+="-";
    if(cn!=sliceDirection) result+="/";
  }
  return result;
}


void SeqGradChanParallel::padd_channel_with_delay(direction chanNo, double maxdur) {
  Log<Seq> odinlog(this,"padd_channel_with_delay");
  ODINLOG(odinlog,normalDebug) << "maxdur=" << maxdur << STD_endl;
  if(!maxdur) return;

  double chandur=0.0;
  if(get_gradchan(chanNo)) chandur=fabs(get_gradchan(chanNo)->get_gradduration());
  if( chandur < maxdur) {
    ODINLOG(odinlog,normalDebug) << "chandur[" << chanNo << "]=" << chandur << STD_endl;
    SeqGradDelay* graddelay=new SeqGradDelay(STD_string(get_label())+"_paddelay",chanNo,maxdur-chandur);
    graddelay->set_temporary();
    if(get_gradchan(chanNo)) {
      ODINLOG(odinlog,normalDebug) << "appending to existing SeqGradChanList" << STD_endl;
      (*get_gradchan(chanNo))+=(*graddelay);
    } else {
      ODINLOG(odinlog,normalDebug) << "creating new SeqGradChanList" << STD_endl;
      SeqGradChanList* sgcl=new  SeqGradChanList(STD_string("(")+graddelay->get_label()+")");
      sgcl->set_temporary();
      (*sgcl)+=(*graddelay);
      set_gradchan(chanNo,sgcl);
    }
  }
}

SeqGradChanList* SeqGradChanParallel::get_gradchan(direction channel) const {
  return (gradchan[channel]).get_handled();
}

SeqGradChanParallel& SeqGradChanParallel::set_gradchan(direction channel, SeqGradChanList* sgcl) {
  if(sgcl) (gradchan[channel]).set_handled(sgcl);
  return *this;
}


