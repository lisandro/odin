/***************************************************************************
                          seqdiffweight.h  -  description
                             -------------------
    begin                : Wed Feb 12 2003
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQDIFFWEIGHT_H
#define SEQDIFFWEIGHT_H

#include <odinseq/seqparallel.h>
#include <odinseq/seqlist.h>
#include <odinseq/seqgrad.h>
#include <odinseq/seqgradpulse.h>
#include <odinseq/seqgradconst.h>
#include <odinseq/seqsimvec.h>

/**
  * @addtogroup odinseq
  * @{
  */

/**
  *
  * \brief  Diffusion Weighting
  *
  * A bipolar gradient pulse for diffusion weighting
  *
  * The b-value and the properties of the gradient pulses are connected through the equation
  *
  * b = gamma^2 * delta^2 ( Delta - delta/3 ) G^2
  *
  * - b: b-value
  * - gamma: gyromagnetic ratio
  * - delta: gradient pulse duration
  * - Delta: gradient pulse separation, the duration of midpart (sequence part which is enclosed by the two gradient) is considered here
  * - G: gradient strength
  *
  * The order in which the series of b-values is played out can be cyclically
  * reordered by using the vector which is returned by the get_reorder_vector()
  * function: At each iteration of this vector, the b-values are exchanged cyclically.
  */
class SeqDiffWeight : public SeqObjList, public SeqSimultanVector, public virtual SeqGradInterface {

 public:

/**
  * Constructs a bipolar gradient pulse labeled 'object_label' with the following properties:
  * - bvals:           The b-values for diffusion weighting, a postive value means that the first
  *                    gradient pulse will have positive polarity and the second gradient pulse a negative polarity.
  *                    These polarities are swapped in case of a negative b-value.
  * - maxgradstrength: The maximum gradient strength to use for the maximum b-value
  * - midpart:         The sequence part which will be enclosed by the two gradient pulses.
  * - chan:            The gradient channel for the gradient pulses.
  * - stejskalTanner:  If set to 'true', both gradients will have the same polarity
  * - nucleus:         The nucleus for which the gradient pulses will be calculated
  */
  SeqDiffWeight(const STD_string& object_label, const fvector& bvals, float maxgradstrength,
                    const SeqObjBase& midpart, direction chan, bool stejskalTanner=false, const STD_string& nucleus = "" );


/**
  * Constructs a bipolar gradient pulse labeled 'object_label' with the following properties:
  * - ndir:            Number of uniformly ditributed directions
  * - bvals:           The b-values for diffusion weighting, a postive value means that the first
  *                    gradient pulse will have positive polarity and the second gradient pulse a negative polarity.
  *                    These polarities are swapped in case of a negative b-value.
  * - maxgradstrength: The maximum gradient strength to use for the maximum b-value
  * - midpart:         The sequence part which will be enclosed by the two gradient pulses.
  * - baseline_rep:    Repetition rate of the baseline (b=0) scan. Not repeated if zero.
  * - stejskalTanner:  If set to 'true', both gradients will have the same polarity
  * - nucleus:         The nucleus for which the gradient pulses will be calculated
  */
  SeqDiffWeight(const STD_string& object_label, unsigned int ndir, const fvector& bvals, float maxgradstrength,
                    const SeqObjBase& midpart, unsigned int baseline_rep=0, bool stejskalTanner=false, const STD_string& nucleus = "" );


/**
  * Default constructor
  */
  SeqDiffWeight(const STD_string& object_label = "unnamedSeqDiffWeight" );

/**
  * Copy constructor
  */
  SeqDiffWeight(const SeqDiffWeight& sgdw) {SeqDiffWeight::operator = (sgdw);}

/**
  * Assignment operator
  */
  SeqDiffWeight& operator = (const SeqDiffWeight& sgdw);

/**
  * Returns the first gradient lobe
  */
  const SeqGradInterface& get_grad1() const {return par1;}

/**
  * Returns the second gradient lobe
  */
  const SeqGradInterface& get_grad2() const {return par2;}

/**
  * Returns the duration of the first gradient lobe
  */
  double get_grad1_duration() const {return par1.get_duration();}

/**
  * Returns the duration of the second gradient lobe
  */
  double get_grad2_duration() const {return par2.get_duration();}

/**
  * Returns the duration of the part in the middle between the two gradient lobes
  */
  double get_midpart_duration() const {return middle_part.get_duration();}

/**
  * Returns the b-vectors used in subsequent order.
  */
  const darray& get_b_vectors() const {return b_vectors_cache;}



  // overloading virtual function from SeqGradInterface
  SeqGradInterface& set_strength(float gradstrength);
  SeqGradInterface& invert_strength();
  float get_strength() const;
  fvector get_gradintegral() const;
  double get_gradduration() const;
  SeqGradInterface& set_gradrotmatrix(const RotMatrix& matrix);


  // final overrider of virtual functions from SeqClass
  void clear_container() {SeqObjList::clear(); SeqSimultanVector::clear();}


  // implemented virtual functions from SeqVector
  bool is_qualvector() const {return false;}

 private:

  void build_seq();


  SeqGradVectorPulse pfg1[n_directions];
  SeqGradVectorPulse pfg2[n_directions];


  SeqParallel par1;
  SeqParallel par2;

  SeqObjList middle_part;

  darray b_vectors_cache;

};


//////////////////////////////////////////////////////////////////////////////////////////

/**
  *
  * \brief  Flow-Compensated Diffusion Weighting
  *
  * A flow-compensated gradient train for diffusion weighting
  *
  */
class SeqDiffWeightFlowComp : public SeqGradChanList, public SeqSimultanVector {

 public:

/**
  * Constructs a flow-compensated gradient train labeled 'object_label' with the following properties:
  * - bvals:           The b-values for diffusion weighting, a postive value means that the first
  *                    gradient pulse will have positive polarity and the second gradient pulse a negative polarity.
  *                    These polarities are swapped in case of a negative b-value
  * - maxgradstrength: The maximum gradient strength to use for the maximum b-value
  * - chan:            The gradient channel for the gradient pulses
  * - stimdelay:       Extra delay between gradient sub-pulses to avoid stimulation
  * - nucleus:         The nucleus for which the gradient pulses will be calculated
  */
  SeqDiffWeightFlowComp(const STD_string& object_label, const fvector& bvals, float maxgradstrength,
                    direction chan, double stimdelay=1.0, const STD_string& nucleus = "" );

/**
  * Default constructor
  */
  SeqDiffWeightFlowComp(const STD_string& object_label = "unnamedSeqDiffWeightFlowComp" );

/**
  * Copy constructor
  */
  SeqDiffWeightFlowComp(const SeqDiffWeightFlowComp& sgdwfc) {SeqDiffWeightFlowComp::operator = (sgdwfc);}

/**
  * Assignment operator
  */
  SeqDiffWeightFlowComp& operator = (const SeqDiffWeightFlowComp& sgdwfc);


  // final overrider of virtual functions from SeqClass
  void clear_container() {SeqGradChanList::clear(); SeqSimultanVector::clear();}


  // implemented virtual functions from SeqVector
  bool is_qualvector() const {return false;}

 private:

  void build_seq();

  SeqGradVectorPulse pfg1;
  SeqGradVectorPulse pfg2;
  SeqGradVectorPulse pfg3;
  SeqGradDelay delay;
};




/** @}
  */


#endif
