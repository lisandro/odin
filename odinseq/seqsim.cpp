#include <tjutils/tjfeedback.h>
#include <tjutils/tjthread_code.h>

#include "seqsim.h"


///////////////////////////////////////////////////////////////////////////

void SeqSimMagsi::common_init() {


  magsi=false;
  nthreads=1;

  Mamp.set_filemode(compressed);
  Mpha.set_filemode(compressed);
  Mz.set_filemode(compressed);

  online=true;

  time_intervals_cache=0;

  xpos_cache=0;
  ypos_cache=0;
  zpos_cache=0;
  freqoffset_cache=0;
  ppm_cache=0;
  spin_density_cache=0;
  B1map_transm_cache=0;
  B1map_receiv_cache=0;
  Dcoeff_cache=0;
  num_rec_channel_cache=0;
  r1_cache=0;
  r2_cache=0;
  has_relax_cache=0;
  spat_rotmatrix=0;

  initial_vector[0]=0.0;
  initial_vector[1]=0.0;
  initial_vector[2]=1.0;

  online.set_description("Perform simulation online, i.e. each time a pulse parameter has been changed");
  update_now.set_description("Recalculate magnetization");
  initial_vector.set_description("Magnetization at beginning of pulse");

  // intra-voxel magn gradients
  for(unsigned int k=0; k<4; k++) {
    dMx[k]=0;
    dMy[k]=0;
    dMz[k]=0;
    if(k<3) dppm[k]=0;
  }

  // init cache, must be called after setting pointers to zero
  outdate_simcache();

  // set defaults on axes
  Sample default_sample;
  set_axes_cache(default_sample);
}


void SeqSimMagsi::outdate_simcache() {
  if(time_intervals_cache) delete[] time_intervals_cache; time_intervals_cache=0;
  if(xpos_cache) delete[] xpos_cache; xpos_cache=0;
  if(ypos_cache) delete[] ypos_cache; ypos_cache=0;
  if(zpos_cache) delete[] zpos_cache; zpos_cache=0;
  if(freqoffset_cache) delete[] freqoffset_cache; freqoffset_cache=0;
  if(ppm_cache) delete[] ppm_cache; ppm_cache=0;
  if(spin_density_cache) delete[] spin_density_cache; spin_density_cache=0;
  if(B1map_transm_cache) delete[] B1map_transm_cache; B1map_transm_cache=0;
  if(B1map_receiv_cache) {
    for(unsigned int ichan=0; ichan<num_rec_channel_cache; ichan++) delete[] B1map_receiv_cache[ichan];
    delete[] B1map_receiv_cache; B1map_receiv_cache=0;
  }
  if(Dcoeff_cache) delete[] Dcoeff_cache; Dcoeff_cache=0;
  sim_diffusion=false;

  if(r1_cache) delete[] r1_cache; r1_cache=0;
  if(r2_cache) delete[] r2_cache; r2_cache=0;
  if(has_relax_cache) delete[] has_relax_cache; has_relax_cache=0;

  // intra-voxel magn gradients
  for(unsigned int k=0; k<4; k++) {
    if(dMx[k]) delete[] dMx[k]; dMx[k]=0;
    if(dMy[k]) delete[] dMy[k]; dMy[k]=0;
    if(dMz[k]) delete[] dMz[k]; dMz[k]=0;
    if(k<3) {
      if(dppm[k]) delete[] dppm[k]; dppm[k]=0;
    }
  }

  simcache_up2date=false;
}


SeqSimMagsi::SeqSimMagsi(const STD_string& label) : LDRblock(label) {
  SeqClass::set_label(label);
  common_init();

  resize(1,1,1,1);

  SeqSimMagsi::append_all_members();
  outdate_simcache();
}


SeqSimMagsi::SeqSimMagsi(const SeqSimMagsi& ssm) {
  common_init();
  SeqSimMagsi::operator = (ssm);
}


SeqSimMagsi::~SeqSimMagsi() {
  if(spat_rotmatrix) delete spat_rotmatrix;
  outdate_simcache();
}


int SeqSimMagsi::append_all_members() {

  append_member(online,"OnlineSimulation");
  append_member(update_now,"UpdateMagnetization");
  append_member(initial_vector,"InitialMagnVector");
  append_member(Mamp,"MagnetizationAmplitude");
  append_member(Mpha,"MagnetizationPhase");
  append_member(Mz,"z-Magnetization");

  update_axes();

  return 0;
}


SeqSimMagsi& SeqSimMagsi::operator = (const SeqSimMagsi& ssm) {
  SeqClass::set_label(ssm.SeqClass::get_label());
  LDRblock::operator = (ssm);

  Mx=ssm.Mx;
  My=ssm.My;
  Mz=ssm.Mz;
  Mamp=ssm.Mamp;
  Mpha=ssm.Mpha;

  online=ssm.online;
  update_now=ssm.update_now;
  initial_vector=ssm.initial_vector;

  magsi=ssm.magsi;
  nthreads=ssm.nthreads;

  SeqSimMagsi::append_all_members();

  outdate_simcache();
  return *this;
}


SeqSimMagsi& SeqSimMagsi::resize(unsigned int xsize, unsigned int ysize,  unsigned int zsize, unsigned int freqsize) {
  Mx.redim (freqsize,zsize,ysize,xsize);
  My.redim (freqsize,zsize,ysize,xsize);
  Mz.redim (freqsize,zsize,ysize,xsize);
  Mamp.redim (freqsize,zsize,ysize,xsize);
  Mpha.redim (freqsize,zsize,ysize,xsize);
  reset_magnetization();
  outdate_simcache();
  return *this;
}


SeqSimMagsi& SeqSimMagsi::reset_magnetization() {
  unsigned int i;
  for(i=0; i<Mx.length(); i++) {
    Mx[i]=initial_vector[0];
    My[i]=initial_vector[1];
    Mz[i]=initial_vector[2];
    Mamp[i]=0.0;
    Mpha[i]=0.0;
  }

  for(unsigned int k=0; k<4; k++) {
    if(dMx[k]) {
      for(i=0; i<oneframe_size_cache; i++) {
        dMx[k][i]=0.0;
        dMy[k][i]=0.0;
        dMz[k][i]=0.0;
      }
    }
  }

  return *this;
}


SeqSimMagsi& SeqSimMagsi::set_initial_vector(float Mx, float My, float Mz) {
  initial_vector[0]=Mx;
  initial_vector[1]=My;
  initial_vector[2]=Mz;
  return reset_magnetization();
}


SeqSimMagsi& SeqSimMagsi::update() {
  update_axes();
  outdate_simcache();
  return *this;
}


SeqSimMagsi& SeqSimMagsi::set_spat_rotmatrix(const RotMatrix& rotmatrix) {
  if(spat_rotmatrix) delete spat_rotmatrix;
  spat_rotmatrix=new RotMatrix(rotmatrix);
  return *this;
}


SeqSimMagsi& SeqSimMagsi::MampMpha2MxMy() {
/*
  unsigned int nx=Mx.size(xDim);
  unsigned int ny=Mx.size(yDim);
  unsigned int nz=Mx.size(zDim);
  unsigned int nfreq=Mx.size(freqDim);

  Mx.redim (nfreq,nz,ny,nx);
  My.redim (nfreq,nz,ny,nx);
*/

  My.redim(Mx.get_extent());

  for(unsigned int i=0; i<get_total_size(); i++) {
    Mx[i]=float(Mamp[i])*cos(PII/180.0*float(Mpha[i]));
    My[i]=float(Mamp[i])*sin(PII/180.0*float(Mpha[i]));
  }

  return *this;
}




SeqSimMagsi& SeqSimMagsi::MxMy2MampMpha() {
/*
  unsigned int nx=Mx.size(xDim);
  unsigned int ny=Mx.size(yDim);
  unsigned int nz=Mx.size(zDim);
  unsigned int nfreq=Mx.size(freqDim);

  Mamp.redim (nfreq,nz,ny,nx);
  Mpha.redim (nfreq,nz,ny,nx);
*/

  Mamp.redim(Mx.get_extent());
  Mpha.redim(Mx.get_extent());

  for(unsigned int i=0; i<get_total_size(); i++) {
    Mamp[i]=norm(Mx[i],My[i]);
    Mpha[i]=180.0/PII*atan2(My[i],Mx[i]);
  }

  return *this;
}

void SeqSimMagsi::update_axes() {
  Log<Seq> odinlog(this,"update_axes");
  unsigned int nz=Mx.size(zDim);
  unsigned int nfreq=Mx.size(freqDim);

  ODINLOG(odinlog,normalDebug) << "nz/nfreq=" << nz << "/" << nfreq << STD_endl;

  GuiProps gp;
  if(nfreq>1) {
    gp.scale[xPlotScale]=ArrayScale("Frequency Offset",ODIN_FREQ_UNIT,freq_low,freq_upp);
  }
  if(nz>1) {
    gp.scale[xPlotScale]=ArrayScale("Spatial Offset",ODIN_SPAT_UNIT,x_low,x_upp);
  }
  Mx.  set_gui_props(gp);
  My.  set_gui_props(gp);
  Mz.  set_gui_props(gp);
  Mamp.set_gui_props(gp);
  Mpha.set_gui_props(gp);
}


bool SeqSimMagsi::do_simulation() {
  bool action=false;
  if(update_now) action=true;
  if(online) action=true;
  return action;
}


void SeqSimMagsi::set_axes_cache(const Sample& sample) {
  Log<Seq> odinlog(this,"create_simcache");
  x_low=sample.get_spatial_offset(xAxis)-0.5*sample.get_FOV(xAxis);
  x_upp=sample.get_spatial_offset(xAxis)+0.5*sample.get_FOV(xAxis);
  ODINLOG(odinlog,normalDebug) << "x_low/x_upp=" << x_low << "/" << x_upp << STD_endl;
  y_low=sample.get_spatial_offset(yAxis)-0.5*sample.get_FOV(yAxis);
  y_upp=sample.get_spatial_offset(yAxis)+0.5*sample.get_FOV(yAxis);
  ODINLOG(odinlog,normalDebug) << "y_low/y_upp=" << y_low << "/" << y_upp << STD_endl;
  z_low=sample.get_spatial_offset(zAxis)-0.5*sample.get_FOV(zAxis);
  z_upp=sample.get_spatial_offset(zAxis)+0.5*sample.get_FOV(zAxis);
  ODINLOG(odinlog,normalDebug) << "z_low/z_upp=" << z_low << "/" << z_upp << STD_endl;
  freq_low=sample.get_freqoffset()-0.5*sample.get_freqrange(); // in kHz
  freq_upp=sample.get_freqoffset()+0.5*sample.get_freqrange(); // in kHz
  ODINLOG(odinlog,normalDebug) << "freq_low/freq_upp=" << freq_low << "/" << freq_upp << STD_endl;
}


void SeqSimMagsi::prepare_simulation(const Sample& sample, CoilSensitivity* transmit_coil, CoilSensitivity* receive_coil, ProgressMeter* progmeter) {
  if(simcache_up2date) return;

  Log<Seq> odinlog(this,"create_simcache");

  outdate_simcache(); // delete old pointers


  ndim extent=sample.get_extent();
  unsigned int nx=extent[xDim];
  unsigned int ny=extent[yDim];
  unsigned int nz=extent[zDim];
  unsigned int nfreq=extent[freqDim];

//  unsigned int n=extent.total();
  oneframe_size_cache=nx*ny*nz*nfreq; // size of one frame

  ODINLOG(odinlog,normalDebug) << "extent/oneframe_size=" << (STD_string)extent << "/" << oneframe_size_cache << STD_endl;
  resize(nx, ny, nz, nfreq); // will call reset_magnetization() and outdate_simcache()

  if(progmeter) progmeter->new_task(extent.total(),"Initializing Simulation");


  elapsed_time=0.0;
  time_index_cache=0;
  numof_time_intervals_cache=0;
  dvector ti(sample.get_frame_durations());
  unsigned int nti=ti.size();
  if(nti>1) {
    numof_time_intervals_cache=nti;
    ODINLOG(odinlog,normalDebug) << "numof_time_intervals_cache=" << numof_time_intervals_cache << STD_endl;
    time_intervals_cache=new double[numof_time_intervals_cache];
    for(unsigned int iti=0; iti<numof_time_intervals_cache; iti++) time_intervals_cache[iti]=ti[iti];
  }

  farray ppmMap=sample.get_ppmMap();
  farray spinDensity=sample.get_spinDensity();
  farray Dcoeff=sample.get_DcoeffMap();
  farray t1map=sample.get_T1map();
  farray t2map=sample.get_T2map();

  xpos_cache=new float[oneframe_size_cache];
  ypos_cache=new float[oneframe_size_cache];
  zpos_cache=new float[oneframe_size_cache];
  freqoffset_cache=new float[oneframe_size_cache];

  nframes_ppm_cache=ppmMap.size(frameDim);
  ODINLOG(odinlog,normalDebug) << "nframes_ppm=" << nframes_ppm_cache << STD_endl;
  ppm_cache=new float[nframes_ppm_cache*oneframe_size_cache];

  nframes_spin_density_cache=spinDensity.size(frameDim);
  ODINLOG(odinlog,normalDebug) << "nframes_spin_density=" << nframes_spin_density_cache << STD_endl;
  spin_density_cache=new float[nframes_spin_density_cache*oneframe_size_cache];

  B1map_transm_cache=new STD_complex[oneframe_size_cache];

  num_rec_channel_cache=1;
  if(receive_coil) num_rec_channel_cache=receive_coil->get_numof_channels();


  B1map_receiv_cache=new STD_complex*[num_rec_channel_cache];
  unsigned int ichan;
  for(ichan=0; ichan<num_rec_channel_cache; ichan++) B1map_receiv_cache[ichan]=new STD_complex[oneframe_size_cache];

  if(sample.have_DcoeffMap) {
    nframes_Dcoeff_cache=Dcoeff.size(frameDim);
    ODINLOG(odinlog,normalDebug) << "nframes_Dcoeff=" << nframes_Dcoeff_cache << STD_endl;
    Dcoeff_cache=new float[nframes_Dcoeff_cache*oneframe_size_cache];
    sim_diffusion=true;
  }


  nframes_r1_cache=t1map.size(frameDim);
  ODINLOG(odinlog,normalDebug) << "nframes_r1=" << nframes_r1_cache << STD_endl;
  nframes_r2_cache=t2map.size(frameDim);
  ODINLOG(odinlog,normalDebug) << "nframes_r2=" << nframes_r2_cache << STD_endl;

  r1_cache=new float[nframes_r1_cache*oneframe_size_cache];
  r2_cache=new float[nframes_r2_cache*oneframe_size_cache];
  has_relax_cache=new bool[oneframe_size_cache];

  if(magsi || sim_diffusion) {
    for(unsigned int k=0; k<4; k++) {
      dMx[k]=new float[oneframe_size_cache];
      dMy[k]=new float[oneframe_size_cache];
      dMz[k]=new float[oneframe_size_cache];
      if(k<3) dppm[k]=new float[oneframe_size_cache];
    }
  }


  set_axes_cache(sample);

  float delta_x=(x_upp-x_low);
  float delta_y=(y_upp-y_low);
  float delta_z=(z_upp-z_low);

  unsigned int k;
  for(k=0; k<4; k++) L[k]=0.0;

  ODINLOG(odinlog,normalDebug) << "delta_x/y/z=" << delta_x << "/" << delta_y << "/" << delta_z << STD_endl;
  ODINLOG(odinlog,normalDebug) << "nx/ny/nz=" << nx << "/" << ny << "/" << nz << STD_endl;

  L[0]=secureDivision(delta_x,nx);
  L[1]=secureDivision(delta_y,ny);
  L[2]=secureDivision(delta_z,nz);
  if(nfreq>1) L[3]=2.0*PII*sample.get_freqrange()/float(nfreq); // in rad*kHz
  ODINLOG(odinlog,normalDebug) << "L=" << L[0] << "/" << L[1] << "/" << L[2] << "/" << L[3]<< STD_endl;

  B0_ppm=1.0e-6*systemInfo->get_B0();

  if(spat_rotmatrix) {
    ODINLOG(odinlog,normalDebug) << "spat_rotmatrix=" << spat_rotmatrix->print() << STD_endl;
  }

  ODINLOG(odinlog,normalDebug) << "ppmMap.get_extent()=" << ppmMap.get_extent() << STD_endl;
  ODINLOG(odinlog,normalDebug) << "spinDensity.get_extent()=" << spinDensity.get_extent() << STD_endl;
  ODINLOG(odinlog,normalDebug) << "spinDensity.sum()=" << spinDensity.sum() << STD_endl;

  extent[frameDim]=1; // iterate over time dim for each map separately
  unsigned int iframe;
  for(unsigned int i=0; i<oneframe_size_cache; i++) {
    ndim indexvec=extent.index2extent(i);

    unsigned int ifreq=indexvec[freqDim];
    unsigned int iz   =indexvec[zDim];
    unsigned int iy   =indexvec[yDim];
    unsigned int ix   =indexvec[xDim];

    freqoffset_cache[i]=2.0*PII*sample.get_freqoffset();
    if(nfreq>1) freqoffset_cache[i] = 2.0*PII*freq_low + (float(ifreq)+0.5)*L[3];

    float x=sample.get_spatial_offset(xAxis);
    if(nx>1) x = x_low + (float(ix)+0.5)*L[0];

    float y=sample.get_spatial_offset(yAxis);
    if(ny>1) y = y_low + (float(iy)+0.5)*L[1];

    float z=sample.get_spatial_offset(zAxis);
    if(nz>1) z = z_low + (float(iz)+0.5)*L[2];


    if(spat_rotmatrix) {
      dvector srcvec(3);
      srcvec[0]=x;
      srcvec[1]=y;
      srcvec[2]=z;
      dvector targetvec=(*spat_rotmatrix)*srcvec;
      xpos_cache[i]=targetvec[0];
      ypos_cache[i]=targetvec[1];
      zpos_cache[i]=targetvec[2];
    } else {
      xpos_cache[i]=x;
      ypos_cache[i]=y;
      zpos_cache[i]=z;
    }

    for(iframe=0; iframe<nframes_ppm_cache; iframe++) ppm_cache[iframe*oneframe_size_cache+i]=ppmMap[iframe*oneframe_size_cache+i];

    for(iframe=0; iframe<nframes_spin_density_cache; iframe++) spin_density_cache[iframe*oneframe_size_cache+i]=spinDensity[iframe*oneframe_size_cache+i];

    STD_complex b1sample=STD_complex(1.0); // sample.get_B1map()[i];
    B1map_transm_cache[i]=b1sample;
    for(ichan=0; ichan<num_rec_channel_cache; ichan++) B1map_receiv_cache[ichan][i]=b1sample;

    if(transmit_coil) { // transmitter signal is sum of all coils
       STD_complex chansum(0.0);
       for(ichan=0; ichan<transmit_coil->get_numof_channels(); ichan++) {
         chansum+=transmit_coil->get_sensitivity_value(ichan,xpos_cache[i],ypos_cache[i],zpos_cache[i]);
       }
       B1map_transm_cache[i]*=chansum;
    }

    if(receive_coil) { // separate B1map for different channels of coil
      for(ichan=0; ichan<num_rec_channel_cache; ichan++) B1map_receiv_cache[ichan][i]*=receive_coil->get_sensitivity_value(ichan,xpos_cache[i],ypos_cache[i],zpos_cache[i]);
    }

    if(sim_diffusion) {
      for(iframe=0; iframe<nframes_Dcoeff_cache; iframe++) Dcoeff_cache[iframe*oneframe_size_cache+i]=Dcoeff[iframe*oneframe_size_cache+i];
    }

    iframe=0;
    while(iframe<nframes_r1_cache || iframe<nframes_r2_cache) {

      unsigned int it1 = (iframe%nframes_r1_cache)*oneframe_size_cache + i;
      unsigned int it2 = (iframe%nframes_r2_cache)*oneframe_size_cache + i;

      float t1=t1map[it1];
      float t2=t2map[it2];
      if(t1<0.0) t1=0.0;
      if(t2<0.0) t1=0.0;
      if(t2>t1) t2=t1;

      if(iframe==0) has_relax_cache[i]=false; // reset only in 1st frame
      if(t1>0.0) has_relax_cache[i]=true;

      r1_cache[it1]=secureDivision( 1.0 , t1 );
      r2_cache[it2]=secureDivision( 1.0 , t2 );

      iframe++;
    }

    if(magsi || sim_diffusion) {
      for(k=0; k<4; k++) {
        dMx[k][i]=0.0;
        dMy[k][i]=0.0;
        dMz[k][i]=0.0;
      }
      for(k=0; k<3; k++) { // iterate through 3 spat dims to calc spat grads from ppmMap
        dppm[k][i]=0.0;

        bool calc_grads=true;
//        if(!have_ppmMap) calc_grads=false;
//        if(have_spinDensity && spinDensity(indexvec)<=0.0) calc_grads=false;
        if(spinDensity(indexvec)<=0.0) calc_grads=false; // use only first frame

        if(calc_grads) {
          bool have_low=false;
          bool have_upp=false;
          float lowval=0.0;
          float uppval=0.0;
          float currval=ppmMap(indexvec); // use only first frame

          int index=indexvec[xDim-k];
          if(index>0) {
            ndim indexvec_low(indexvec); indexvec_low[xDim-k]--;
//            if(!have_spinDensity) have_low=true;
            if(/*have_spinDensity && */ spinDensity(indexvec_low)>0.0) have_low=true;
            if(have_low) lowval=ppmMap(indexvec_low);
          }
          if(index<int(extent[xDim-k]-1)) {
            ndim indexvec_upp(indexvec); indexvec_upp[xDim-k]++;
//            if(!have_spinDensity) have_upp=true;
            if(/* have_spinDensity && */ spinDensity(indexvec_upp)>0.0) have_upp=true;
            if(have_upp) uppval=ppmMap(indexvec_upp);
          }
          if(have_upp && have_low)  dppm[k][i]=secureDivision(uppval-lowval,  2.0*L[k]);
          if(!have_upp && have_low) dppm[k][i]=secureDivision(currval-lowval, L[k]);
          if(have_upp && !have_low) dppm[k][i]=secureDivision(uppval-currval, L[k]);
        }
      }
    }

    if(progmeter) progmeter->increase_counter();
  }

  update_axes();

  if(!ThreadedLoop<SeqSimInterval,cvector,int>::init(nthreads, oneframe_size_cache)) {
    ODINLOG(odinlog,errorLog) << "cannot init multithreading" << STD_endl;
  }

  simcache_up2date=true;
}




cvector SeqSimMagsi::simulate(const SeqSimInterval& simvals, double gamma) {
  Log<Seq> odinlog(this,"simulate");

  cvector result(num_rec_channel_cache); // initialized with zeroes

  gamma_cache=gamma;

  if(numof_time_intervals_cache) {

    elapsed_time+=simvals.dt;

    while(elapsed_time>=time_intervals_cache[time_index_cache]) {
      elapsed_time-=time_intervals_cache[time_index_cache];
      time_index_cache++;
      if(time_index_cache>=numof_time_intervals_cache) time_index_cache=0;
      ODINLOG(odinlog,normalDebug) << "Switching to time_index=" << time_index_cache << STD_endl;
    }

  }

  STD_vector<cvector> outvec;
  if(!ThreadedLoop<SeqSimInterval,cvector,int>::execute(simvals, outvec)) {
    ODINLOG(odinlog,errorLog) << "cannot start multithreading" << STD_endl;
    return result;
  }


  if(simvals.rec>0.0) {
    for(unsigned int i=0; i<outvec.size(); i++) {
      if(outvec[i].size()) { // zero-duration intervals have zero size
        result+=outvec[i];
      }
    }
  }
  return result;
}


bool SeqSimMagsi::kernel(const SeqSimInterval& simvals, cvector& signal, int&, unsigned int begin, unsigned int end) {
  Log<Seq> odinlog(this,"kernel");

  if(simvals.dt<=0.0) return true;

  unsigned int ichan;

  // doubles to sum up signal, and use local (thread-specific) storage
  double* reSig=0;
  double* imSig=0;
  if(simvals.rec>0.0) {
    reSig=new double[num_rec_channel_cache];
    imSig=new double[num_rec_channel_cache];
    for(ichan=0; ichan<num_rec_channel_cache; ichan++) {
      reSig[ichan]=0.0;
      imSig[ichan]=0.0;
    }
  }

  bool sim_intravoxel_grads=false;
  if(magsi || sim_diffusion) sim_intravoxel_grads=true;


  float Mxnew, Mynew, Mznew;
  float Mxold, Myold, Mzold;
  float ax,ay;
  float bx,by,bz,b;
  float E1,E2;
  float ph_rf, ph_gr;
  float si_rf, co_rf, si_gr, co_gr;
  STD_complex b1_pulse,b1;

  float u,v,w;
  float axx,ayy;

  float Dw;
  float w0_ppm=gamma_cache*B0_ppm;

  float d_rf[3][3];

  float phase_rad=simvals.phase * PII / 180.0;

//  float B1amp=cabs(simvals.B1);
//  float B1phase=phase(simvals.B1);

  // stuff for intra-voxel magn gradients / diffusion
  float dMxnew[4];
  float dMynew[4];
  float dMznew[4];
  float dMxold[4];
  float dMyold[4];
  float dMzold[4];
  float h[4];
  float gamma_G[3];
  if(sim_intravoxel_grads) {
    gamma_G[0]=gamma_cache*simvals.Gx;
    gamma_G[1]=gamma_cache*simvals.Gy;
    gamma_G[2]=gamma_cache*simvals.Gz;
  }
  unsigned int k;
  float dphi,weight;
  float dphi_prev[3];
  float d_prev=0.0;


//  b1_pulse= float(gamma_cache) * B1amp * STD_complex( cos(B1phase+phase_rad), sin(B1phase+phase_rad));
  b1_pulse= float(gamma_cache) * simvals.B1 * expc(STD_complex(0.0,phase_rad));
  ODINLOG(odinlog,normalDebug) << "b1_pulse=" << b1_pulse << STD_endl;

  for(unsigned int i=begin; i<end; i++) {

    float s0=spin_density_cache[(time_index_cache%nframes_spin_density_cache)*oneframe_size_cache+i];

    if(s0>0.0) {


      ////////////////// Initialization ///////////////////////////

      // the initial magnetization vector
      Mxold=Mx[i];
      Myold=My[i];
      Mzold=Mz[i];

      // inits for intra-voxel deph
      if(sim_intravoxel_grads) {
        for(k=0; k<4; k++) {
          dMxold[k]=dMx[k][i];
          dMyold[k]=dMy[k][i];
          dMzold[k]=dMz[k][i];
          if(k<3) h[k]=simvals.dt*(gamma_G[k]+w0_ppm*dppm[k][i]);
          else    h[k]=simvals.dt;
        }
      }

      // the effective field in the rotating frame of reference
      b1=B1map_transm_cache[i]*b1_pulse;
      bx=b1.real();
      by=b1.imag();
      Dw = ppm_cache[(time_index_cache%nframes_ppm_cache)*oneframe_size_cache+i] * w0_ppm + freqoffset_cache[i] - 2.0*PII*simvals.freq;
      bz = gamma_cache * (simvals.Gx*xpos_cache[i] + simvals.Gy*ypos_cache[i] + simvals.Gz*zpos_cache[i]) + Dw;

      si_gr=co_gr=0.0;
      if (bz != 0.0 || sim_intravoxel_grads) {
        ph_gr = bz*simvals.dt; // the precession angle around the effective field
        si_gr = sin (ph_gr);
        co_gr = cos (ph_gr);
      }


      if(sim_diffusion) {

        float dM_sum2=0.0;


        // calculate component of dM which is perpendicular to M
        float Mnorm=norm3(Mxold,Myold,Mzold);
        float Mxunit=0.0;
        float Myunit=0.0;
        float Mzunit=0.0;
        if(Mnorm) {
          Mxunit=Mxold/Mnorm;
          Myunit=Myold/Mnorm;
          Mzunit=Mzold/Mnorm;
        }

        float dMx_perp[3];
        float dMy_perp[3];
        float dMz_perp[3];

        for(k=0; k<3; k++) {
          float M_dM_proj = Mxunit*dMxold[k] + Myunit*dMyold[k] + Mzunit*dMzold[k];
          dMx_perp[k] = dMxold[k] - M_dM_proj * Mxunit;
          dMy_perp[k] = dMyold[k] - M_dM_proj * Myunit;
          dMz_perp[k] = dMzold[k] - M_dM_proj * Mzunit;
        }


        for(k=0; k<3; k++) {
          dM_sum2 += dMx_perp[k]*dMx_perp[k] + dMy_perp[k]*dMy_perp[k] + dMz_perp[k]*dMz_perp[k];
          dphi_prev[k] = secureDivision(dMyold[k]*Mxold - Myold*dMxold[k], Mxold*Mxold + Myold*Myold);
        }

        d_prev=secureDivision(dM_sum2, Mxold*Mxold + Myold*Myold + Mzold*Mzold);

      }


      ////////////////// Rotation around z-Axis due to frequency offset ///////////////////////////

      if (bz != 0.0) {
        Mxnew =  co_gr * Mxold + si_gr * Myold;
        Mynew = -si_gr * Mxold + co_gr * Myold;
      } else {
        Mxnew = Mxold;
        Mynew = Myold;
      }
      Mznew = Mzold;


      // intra-voxel magn gradients
      if(sim_intravoxel_grads) {
        for(k=0; k<4; k++) {
          dMxnew[k] = h[k] * (-si_gr*Mxold+co_gr*Myold) + co_gr * dMxold[k] + si_gr * dMyold[k];
          dMynew[k] = h[k] * (-co_gr*Mxold-si_gr*Myold) - si_gr * dMxold[k] + co_gr * dMyold[k];
          dMznew[k] = dMzold[k];
        }
      }

      // setup for RF matrix multiplication
      Mxold = Mxnew;
      Myold = Mynew;
      Mzold = Mznew;

      // set up intra-voxel magn grads for RF matrix multiplication
      if(sim_intravoxel_grads) {
        for(k=0; k<4; k++) {
          dMxold[k]=dMxnew[k];
          dMyold[k]=dMynew[k];
          dMzold[k]=dMznew[k];
        }
      }


      ////////////////// RF matrix multiplication ///////////////////////////////////////////////

      b = norm(bx,by);

      if (b != 0.0) {
        // rotate magnetization vector according to extra fields in rotating frame

        ax = bx / b;
        ay = by / b;

        ph_rf = b*simvals.dt; // the precession angle around the effective field
        si_rf = sin (ph_rf);
        co_rf = cos (ph_rf);

        axx= ax * ax;
        ayy= ay * ay;

        u=  ax * ay * (1 - co_rf);
        v=  ay * si_rf;
        w=  ax * si_rf;

        // rotation matrix
        d_rf[0][0]= axx + ayy * co_rf;
        d_rf[0][1]= u;
        d_rf[0][2]= -v;
        d_rf[1][0]= u;
        d_rf[1][1]= ayy + axx * co_rf;
        d_rf[1][2]= w;
        d_rf[2][0]= v;
        d_rf[2][1]= -w;
        d_rf[2][2]= co_rf;

        Mxnew = Mxold * d_rf[0][0] + Myold * d_rf[0][1] + Mzold * d_rf[0][2];
        Mynew = Mxold * d_rf[1][0] + Myold * d_rf[1][1] + Mzold * d_rf[1][2];
        Mznew = Mxold * d_rf[2][0] + Myold * d_rf[2][1] + Mzold * d_rf[2][2];

/*
        float det=   d_rf[0][0]*d_rf[1][1]*d_rf[2][2] + d_rf[0][1]*d_rf[1][2]*d_rf[2][0] + d_rf[0][2]*d_rf[1][0]*d_rf[2][1]
                   - d_rf[0][2]*d_rf[1][1]*d_rf[2][0] - d_rf[0][0]*d_rf[1][2]*d_rf[2][1] - d_rf[0][1]*d_rf[1][0]*d_rf[2][2];
        float dev=fabs(det-1.0);
        if(dev>1.e-6) ODINLOG(odinlog,errorLog) << "determinante deviates by " << dev << STD_endl;
*/

        // intra-voxel deph
        if(sim_intravoxel_grads) {
          for(k=0; k<4; k++) {
            dMxnew[k] = dMxold[k] * d_rf[0][0] + dMyold[k] * d_rf[0][1] + dMzold[k] * d_rf[0][2];
            dMynew[k] = dMxold[k] * d_rf[1][0] + dMyold[k] * d_rf[1][1] + dMzold[k] * d_rf[1][2];
            dMznew[k] = dMxold[k] * d_rf[2][0] + dMyold[k] * d_rf[2][1] + dMzold[k] * d_rf[2][2];
          }
        }

      } else {
        // just copy values if no RF field

        Mxnew = Mxold;
        Mynew = Myold;
        Mznew = Mzold;

        // intra-voxel deph
        if(sim_intravoxel_grads) {
          for(k=0; k<4; k++) {
            dMxnew[k] = dMxold[k];
            dMynew[k] = dMyold[k];
            dMznew[k] = dMzold[k];
          }
        }
      }

      ////////////////// Diffusion  ///////////////////////////////////////////////



      if(sim_diffusion) {

        float A1=1.0;
        float A2=1.0;

        float bval2=0.0;
        for(k=0; k<3; k++) bval2 += (h[k] * h[k]);
        bval2/=3.0;
        for(k=0; k<3; k++) bval2 -= dphi_prev[k] * h[k];
        bval2+=d_prev;
        bval2*=simvals.dt;

        float Dval=Dcoeff_cache[(time_index_cache%nframes_Dcoeff_cache)*oneframe_size_cache+i];

        A1= exp(-Dval * d_prev * simvals.dt);
        if(bval2>0.0) A2= exp(-Dval * bval2); // Check for round-off errors when bval2 was calculated

        Mxnew *= A2;
        Mynew *= A2;
        Mznew *= A1;

        for(k=0; k<4; k++) {
          dMxnew[k] *= A2;
          dMynew[k] *= A2;
          dMznew[k] *= A1;
        }
      }



      ////////////////// Relaxation ///////////////////////////////////////////////

      if ( has_relax_cache[i] ) { //with relaxation
        E1=exp(-simvals.dt*r1_cache[(time_index_cache%nframes_r1_cache)*oneframe_size_cache+i]);
        E2=exp(-simvals.dt*r2_cache[(time_index_cache%nframes_r2_cache)*oneframe_size_cache+i]);
        Mxnew *= E2;
        Mynew *= E2;
        Mznew = (Mznew-1.0) * E1 + 1.0;

        // intra-voxel deph
        if(sim_intravoxel_grads) {
          for(k=0; k<4; k++) {
            dMxnew[k] *= E2;
            dMynew[k] *= E2;
            dMznew[k] *= E1;
          }
        }
      }




      ////////////////// Store Results  ///////////////////////////////////////////////


      // store new values
      Mx[i]=Mxnew;
      My[i]=Mynew;
      Mz[i]=Mznew;

      // store intra-voxel magn gradients
      if(sim_intravoxel_grads) {
        for(k=0; k<4; k++) {
          dMx[k][i]=dMxnew[k];
          dMy[k][i]=dMynew[k];
          dMz[k][i]=dMznew[k];
        }
      }



      ////////////////// Calculate signal  ////////////////////////////////////////////

      if(simvals.rec>0.0) {
        // create signal by accumulating transverse magnetization

        STD_complex Mtrans(Mxnew,Mynew);

	if(magsi) {

          weight=1.0;
          for(k=0; k<4; k++) {
            float Mabs2=Mxnew*Mxnew + Mynew*Mynew;
            if(Mabs2) { // check before division
              dphi = (dMynew[k]*Mxnew - Mynew*dMxnew[k]) / Mabs2;
              if(dphi && (L[k]>0.0)) {
                weight *= sinc(0.5*dphi*L[k]);
              }
            }
          }
          Mtrans *= STD_complex(weight);
        }

        STD_complex sigMagn=s0*conj(Mtrans);
        for(ichan=0; ichan<num_rec_channel_cache; ichan++) {
          STD_complex cplxsig=sigMagn*B1map_receiv_cache[ichan][i];
          reSig[ichan]+=cplxsig.real();
          imSig[ichan]+=cplxsig.imag();
        }

      }

    } // if(spin_density>0.0)

  } // end loop over Mx,My,.. arrays


  if(simvals.rec>0.0) {
    signal.resize(num_rec_channel_cache);
    for(ichan=0; ichan<num_rec_channel_cache; ichan++) {
      signal[ichan]=simvals.rec*STD_complex(reSig[ichan],imSig[ichan])*expc(STD_complex(0.0,phase_rad));
    }
    delete[] reSig;
    delete[] imSig;
  }

  ODINLOG(odinlog,normalDebug) << "signal=" << signal.printbody() << STD_endl;
  return true;
}



void SeqSimMagsi::finalize_simulation() {
  MxMy2MampMpha();
  ThreadedLoop<SeqSimInterval,cvector,int>::destroy(); // Stop threads
  outdate_simcache(); // delete old pointers
}



/////////////////////////////////////////////////////////////////////

#ifdef STANDALONE_PLUGIN

void SeqSimMonteCarlo::common_init() {
  Dcoeff_cache=0;
  ppmMap_cache=0;
  R1map_cache=0;
  R2map_cache=0;
  spinDensity_cache=0;
}

void SeqSimMonteCarlo::clear_cache() {
  if(Dcoeff_cache) delete[] Dcoeff_cache; Dcoeff_cache=0;
  if(ppmMap_cache) delete[] ppmMap_cache; ppmMap_cache=0;
  if(R1map_cache) delete[] R1map_cache; R1map_cache=0;
  if(R2map_cache) delete[] R2map_cache; R2map_cache=0;
  if(spinDensity_cache) delete[] spinDensity_cache; spinDensity_cache=0;
}


SeqSimMonteCarlo::SeqSimMonteCarlo(const STD_string& label, unsigned int nparticles, unsigned int nthreads) {
  common_init();
  SeqClass::set_label(label);
  particle.resize(nparticles);
  numof_threads=nthreads;
}


SeqSimMonteCarlo& SeqSimMonteCarlo::operator = (const SeqSimMonteCarlo& ssmc) {
  SeqClass::set_label(ssmc.SeqClass::get_label());
  particle=ssmc.particle;
  numof_threads=ssmc.numof_threads;
  return *this;
}

farray SeqSimMonteCarlo::get_spatial_dist() const {
  farray result(size_cache[2],size_cache[1],size_cache[0]); // is already initialized to zero
  for(unsigned int i=0; i<particle.size(); i++) {
    const Particle& part=particle[i];
    unsigned int j= linear_index(part.pos);
    result[j]+=1.0;
  }
  return result;
}


void SeqSimMonteCarlo::prepare_simulation(const Sample& sample, CoilSensitivity* transmit_coil, CoilSensitivity* receive_coil, ProgressMeter* progmeter) {
  Log<Seq> odinlog(this,"prepare_simulation");
  clear_cache();

  size_cache[0]=sample.get_extent()[xDim];
  size_cache[1]=sample.get_extent()[yDim];
  size_cache[2]=sample.get_extent()[zDim];
  unsigned int ntotal=size_cache[0]*size_cache[1]*size_cache[2];

  Dcoeff_cache=new float[ntotal];
  ppmMap_cache=new float[ntotal];
  R1map_cache=new float[ntotal];
  R2map_cache=new float[ntotal];
  spinDensity_cache=new float[ntotal];
  for(unsigned int i=0; i<ntotal; i++) {
    Dcoeff_cache[i]=sample.get_DcoeffMap()[i];
    ppmMap_cache[i]=sample.get_ppmMap()[i];
    R1map_cache[i]=secureInv(sample.get_T1map()[i]);
    R2map_cache[i]=secureInv(sample.get_T2map()[i]);
    spinDensity_cache[i]=sample.get_spinDensity()[i];
  }

  pixelspacing_cache[0]=secureDivision(sample.get_FOV(xAxis),size_cache[0]);
  pixelspacing_cache[1]=secureDivision(sample.get_FOV(yAxis),size_cache[1]);
  pixelspacing_cache[2]=secureDivision(sample.get_FOV(zAxis),size_cache[2]);

  for(unsigned int i=0; i<particle.size(); i++) {
    Particle& part=particle[i];

/*
    float dstpos[3];
    float Ddst=0.0;
    while(Ddst<=0.0) { // Do not init to positions with zero diff coefficient to avoid particle traps
      for(unsigned int k=0; k<3; k++) dstpos[k]=size_cache[k]*rng.uniform();
      Ddst=Dcoeff_cache[linear_index(dstpos)];
    }
    for(unsigned int k=0; k<3; k++) part.pos[k]=dstpos[k];
*/

    for(unsigned int k=0; k<3; k++) part.pos[k]=size_cache[k]*rng.uniform();
    part.Mx=0.0;
    part.My=0.0;
    part.Mz=1.0;
  }

  B0_ppm_cache=1.0e-6*systemInfo->get_B0();

  if(!ThreadedLoop<SeqSimInterval,cvector,RandomDist>::init(numof_threads, particle.size())) {
    ODINLOG(odinlog,errorLog) << "cannot init multithreading" << STD_endl;
  }

}


unsigned int SeqSimMonteCarlo::linear_index(const float pos[3]) const {
  unsigned int index[3];
  for(unsigned int k=0; k<3; k++) index[k]=((unsigned int)pos[k])%size_cache[k]; // assuming periodic pattern of spatial maps
  unsigned int result= index[2]*size_cache[0]*size_cache[1] + index[1]*size_cache[0] + index[0];
  return result;
}


cvector SeqSimMonteCarlo::simulate(const SeqSimInterval& simvals, double gamma) {
  Log<Seq> odinlog(this,"simulate");

  cvector result(1);
  if(simvals.dt<=0.0) return result;

  gamma_cache=gamma;

  STD_vector<cvector> outvec;
  if(!ThreadedLoop<SeqSimInterval,cvector,RandomDist>::execute(simvals, outvec)) {
    ODINLOG(odinlog,errorLog) << "cannot start multithreading" << STD_endl;
    return result;
  }

  if(simvals.rec>0.0) {
    for(unsigned int i=0; i<outvec.size(); i++) {
      if(outvec[i].size()) { // zero-duration intervals have zero size
        result+=outvec[i];
      }
    }
  }
  return result;
}


  
  

bool SeqSimMonteCarlo::kernel(const SeqSimInterval& simvals, cvector& signal, RandomDist& local_rng, unsigned int begin, unsigned int end) {
  Log<Seq> odinlog(this,"kernel");

  if(simvals.dt<=0.0) return true;

  float phase_rad=simvals.phase * PII / 180.0;

  float d_rf[3][3];
  float b = 0.0;

  // Uniform B1 field for all particles
  float B1amp=cabs(simvals.B1);
  if(B1amp) {
//    float B1phase=phase(simvals.B1);
//    STD_complex b1_pulse= float(gamma_cache) * B1amp * STD_complex( cos(B1phase+phase_rad), sin(B1phase+phase_rad));
    STD_complex b1_pulse= float(gamma_cache) * simvals.B1 * expc(STD_complex(0.0,phase_rad));
    float bx=b1_pulse.real();
    float by=b1_pulse.imag();
    b = norm(bx,by);

    // Uniform B1 rotation matrix for all particles
    float ax = bx / b;
    float ay = by / b;

    float ph_rf = b*simvals.dt; // the precession angle around the effective field
    float si_rf = sin (ph_rf);
    float co_rf = cos (ph_rf);

    float axx= ax * ax;
    float ayy= ay * ay;

    float u=  ax * ay * (1 - co_rf);
    float v=  ay * si_rf;
    float w=  ax * si_rf;


    // rotation matrix
    d_rf[0][0]= axx + ayy * co_rf;
    d_rf[0][1]= u;
    d_rf[0][2]= -v;
    d_rf[1][0]= u;
    d_rf[1][1]= ayy + axx * co_rf;
    d_rf[1][2]= w;
    d_rf[2][0]= v;
    d_rf[2][1]= -w;
    d_rf[2][2]= co_rf;
  }


  float w0_ppm=gamma_cache*B0_ppm_cache;

   // Use double precision to avoid round-off error when summing up signal
  double reSig=0.0;
  double imSig=0.0;

  for(unsigned int i=begin; i<end; i++) {
    Particle& part=particle[i];

    unsigned int j= linear_index(part.pos);
    ODINLOG(odinlog,normalDebug) << "linear_index[" << i << "]=" << j << STD_endl;


    ////////////////// Initialization ///////////////////////////

    // the effective field in the rotating frame of reference
    float bz = ppmMap_cache[j] * w0_ppm - 2.0*PII*simvals.freq;

    // Calculate spatial position
    if(simvals.Gx || simvals.Gy || simvals.Gz) {
      float spat[3];
      for(unsigned int k=0; k<3; k++) spat[k]=(part.pos[k]-0.5*float(size_cache[k]))*pixelspacing_cache[k];
      bz += gamma_cache * (simvals.Gx*spat[0] + simvals.Gy*spat[1] + simvals.Gz*spat[2]);  // imaging gradients
    }

    float ph_gr = bz*simvals.dt; // the precession angle around the effective field
    float si_gr = sin (ph_gr);
    float co_gr = cos (ph_gr);


    ////////////////// Rotation around z-Axis due to frequency offset ///////////////////////////

    float Mxrot =  co_gr * part.Mx + si_gr * part.My;
    float Myrot = -si_gr * part.Mx + co_gr * part.My;
    float Mzrot = part.Mz;


    ////////////////// RF matrix multiplication ///////////////////////////////////////////////


    if (b != 0.0) {
      // rotate magnetization vector according to extra fields in rotating frame

      part.Mx = Mxrot * d_rf[0][0] + Myrot * d_rf[0][1] + Mzrot * d_rf[0][2];
      part.My = Mxrot * d_rf[1][0] + Myrot * d_rf[1][1] + Mzrot * d_rf[1][2];
      part.Mz = Mxrot * d_rf[2][0] + Myrot * d_rf[2][1] + Mzrot * d_rf[2][2];

    } else {
      part.Mx = Mxrot;
      part.My = Myrot;
      part.Mz = Mzrot;
    }



    ////////////////// Relaxation ///////////////////////////////////////////////

    float E1=exp(-simvals.dt*R1map_cache[j]);
    float E2=exp(-simvals.dt*R2map_cache[j]);
    part.Mx *= E2;
    part.My *= E2;
    part.Mz = (part.Mz-1.0) * E1 + 1.0;

    ////////////////// Calculate signal  ////////////////////////////////////////////

    if(simvals.rec>0.0) {
      // create signal by accumulating transverse magnetization

      reSig+=spinDensity_cache[j]*part.Mx;
      imSig-=spinDensity_cache[j]*part.My; // conjugate

    }


    /////////////////////////////////////////////////////////////////////////////////////
    // calculate new spatial position

    float Dfactor=sqrt(2.0*Dcoeff_cache[j]*simvals.dt);
    if(Dfactor) {
      float newpos[3];
      float Dnew=0.0;
      while(Dnew<=0.0) { // Do not jump to positions with zero diff coefficient to avoid particle traps
        for(unsigned int k=0; k<3; k++) newpos[k]=part.pos[k]+local_rng.gaussian(secureDivision(Dfactor,pixelspacing_cache[k]));
        Dnew=Dcoeff_cache[linear_index(newpos)];
      }
      for(unsigned int k=0; k<3; k++) part.pos[k]=newpos[k];
    }


  } // end loop over particles

  if(simvals.rec>0.0) {
    signal.resize(1);
    signal[0]=simvals.rec*STD_complex(reSig,imSig)*expc(STD_complex(0.0,phase_rad)); // consider receiver weighting/phase
  }

  return true;
}


void SeqSimMonteCarlo::finalize_simulation() {
  ThreadedLoop<SeqSimInterval,cvector,RandomDist>::destroy(); // Stop threads
  clear_cache();
}



#endif

