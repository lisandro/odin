/***************************************************************************
                          seqdelay.h  -  description
                             -------------------
    begin                : Wed Aug 8 2001
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQDELAY_H
#define SEQDELAY_H

#include <odinseq/seqobj.h>
#include <odinseq/seqdur.h>
#include <odinseq/seqdriver.h>


/**
  * @ingroup odinseq_internals
  * The base class for platform specific drivers of delays
  */
class SeqDelayDriver : public SeqDriverBase {

 public:
  SeqDelayDriver() {}
  virtual ~SeqDelayDriver() {}

  virtual STD_string get_program(programContext& context, double duration, const STD_string& cmd, const STD_string& durcmd) const = 0;

  virtual SeqDelayDriver* clone_driver() const = 0;
};

///////////////////////////////////////////////////////////////


/**
  * @addtogroup odinseq
  * @{
  */

/**
  * \brief  Timing delay
  *
  * This class represents a delay with an optional command.
  */
class SeqDelay : public SeqObjBase, public SeqDur {

 public:

/**
  * Constructs a delay labeled 'object_label' with the following properties:
  * - delayduration: The duration of the delay.
  * - command:       An optional command that will be issued during the delay in
  *                  the pulse program.
  * - durationVariable: If the the command that specifies the delay duration in
  *                  the pulse program should differ from the default, it can be
  *                  overwritten by this string.
  */
  SeqDelay(const STD_string& object_label = "unnamedSeqDelay",float delayduration=0.0,
                    const STD_string& command="",const STD_string& durationVariable="" );

/**
  * Constructs a delay which is a copy of 'sd'
  */
  SeqDelay(const SeqDelay& sd);

/**
  * This assignment operator will make this object become an exact copy of 'sd'.
  */
  SeqDelay& operator = (const SeqDelay& sd);

/**
  * This assignment operator can be used to specify the duration of the delay
  */
  SeqDelay& operator = (float dur);

/**
  * Specifies the extra command for the delay
  */
  SeqDelay& set_command(const STD_string& command);


  // overloading virtual function from SeqTreeObj
  STD_string get_program(programContext& context) const;


 private:
  mutable SeqDriverInterface<SeqDelayDriver> delaydriver;
 
  STD_string cmd;
  STD_string durcmd;
};




/** @}
  */


#endif
