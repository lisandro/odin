/***************************************************************************
                          seqloop.h  -  description
                             -------------------
    begin                : Mon Aug 13 2001
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQLOOP_H
#define SEQLOOP_H

#include <tjutils/tjembed.h>

#include <odinseq/seqcounter.h>
#include <odinseq/seqlist.h>


/**
  * @addtogroup odinseq
  * @{
  */

/**
  * \brief Sequence Loop
  *
  * This class represents a loop which is used to enclose other sequence objects
  * so that they are repeatedely played out in the sequence
  */
class SeqObjLoop : public SeqCounter, public SeqObjList, public Embed<SeqObjLoop,SeqObjBase> {

 public:

/**
  * Construct an empty loop object with the given label
  */
  SeqObjLoop(const STD_string& object_label = "unnamedSeqObjLoop");

/**
  * Constructs a copy of 'sl'
  */
  SeqObjLoop(const SeqObjLoop& sl);


  ~SeqObjLoop() {}

/**
  * Assignment operator that makes this loop object become a copy of 'sl'
  */
  SeqObjLoop& operator = (const SeqObjLoop& sl);


/**
  * Duration the loop itself takes before iteration is started
  */
  double get_preduration () const;

/**
  * Duration the loop itself takes after its kernel is iterated
  */
  double get_postduration() const;


/**
  * Sets the inner part of loop, only useful for explicit setup of loops, i.e. in SeqEpiDriverDefault
  */
  SeqObjLoop& set_body(const SeqObjBase& so);

/**
  * Specify the number of times the body of the loop will be repeated, only useful for explicit setup of loops, i.e. in SeqEpiDriverDefault
  */
  SeqObjLoop& set_times(unsigned int t);


/**
  * Generates and returns a new loop object that is a copy of this. The new loop will
  * loop over 'embeddedBody'.
  */
  SeqObjLoop& operator () (const SeqObjBase& embeddedBody);


/**
  * This operator will append 'seqvector' to the list of vectors the loop will iterate through
  */
  SeqObjLoop& operator [] (const SeqVector& seqvector);

/**
  * This operator specifies the number of times the body of the loop will be repeated
  */
  SeqObjLoop& operator [] (unsigned int t);




  // overwriting virtual functions from SeqCounter
  int get_times() const;


  // overloading virtual function from SeqTreeObj
  STD_string get_program(programContext& context) const;
  double get_duration() const;
  unsigned int event(eventContext& context) const;
  SeqValList get_freqvallist(freqlistAction action) const;
  SeqValList get_delayvallist() const;
  double get_rf_energy() const;
  void query(queryContext& context) const;
  RecoValList get_recovallist(unsigned int reptimes, LDRkSpaceCoords& coords) const;
  STD_string get_properties() const;


 private:

  // overwriting virtual functions from SeqClass
  bool prep();
  void clear_container();

  // overwriting virtual functions from SeqCounter
  void add_vector(const SeqVector& seqvector);
  bool unroll_program(programContext& context) const;

/**
  * Returns 'true' if the loop only repeats its contents and does not change its objects while looping.
  * If 'only_qualvectors' is set to true, all vectors that do not influence the timing or RF power
  * deposition will be considered as repetitions.
  */
  bool is_repetition_loop(bool only_qualvectors=false) const;

/**
  * Returns 'true' if the loop only repeats the acquisition ordering.
  */
  bool is_acq_repetition_loop() const;

/**
  * Returns 'true' if the loop repeats its kernel by altering values of the vectors therein
  * (gradient strength, frequency, phase) but without changing the order of the sequence objects.
  */
  bool is_obj_repetition_loop() const;
  
/**
  * Returns 'true' if the subtree of the loop contains an iterator which changes reco indices.
  */
  bool contains_acq_iter() const;

  
  float get_single_duration() const;

  unsigned int times;

  unsigned int get_numof_acq() const;
  mutable unsigned int numof_acq_cache;
  mutable bool is_toplevel_reploop;
};


/** @}
  */

#endif
