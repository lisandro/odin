/***************************************************************************
                          seqgradecho.h  -  description
                             -------------------
    begin                : Mon Feb 17 2003
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQGRADECHO_H
#define SEQGRADECHO_H

#include <odinseq/seqgradpulse.h>
#include <odinseq/seqgradphase.h>
#include <odinseq/seqacqread.h>
#include <odinseq/seqacqdeph.h>
#include <odinseq/seqpulsar.h>
#include <odinseq/seqsimvec.h>


/**
  * @addtogroup odinseq
  * @{
  */


/**
  * \brief Gradient echo module
  *
  * A gradient echo module (excitation pulse + phase encoding + frequency encoding).
  * This is a convenience class which groups a number of low-level sequence
  * objects together. All objects necessary to produce a gradient echo are part
  * of this class, except the excitation pulse. The latter is passed per reference
  * (no copy is created within the module, it uses a safe pointer to the pulse). 
  *
  */
class SeqGradEcho : public SeqObjList, public virtual SeqGradInterface, public virtual SeqAcqInterface {

  // virtual functions of SeqFreqChanInterface are marhshalled to acqread

 public:

/**
  * Constructs a 2D (i.e. multi-slice) gradient echo module labeled 'object_label' with the following properties:
  * - exc:          The excitation pulse
  * - sweepwidth:   The sampling frequency
  * - readnpts:     Matrix size in frequency-encoding direction
  * - FOVread:      The FOV in frequency-encoding direction
  * - phasenpts:    Matrix size in phase-encoding direction
  * - FOVphase:     The FOV in phase-encoding direction
  * - scheme:       The phase encoding scheme
  * - reorder:      The reordering scheme
  * - nsegments:    The number of segments for segmented reordering
  * - reduction:    Reduction factor for parallel imaging (sparsely sampled k-space)
  * - acl_bands:    Number of autocalibration bands (bunch of adjacent lines between actual k-space lines) for GRAPPA
  * - balanced:     Whether rephasers (+ exc dephaser) should be added after acquisition, suitable for SSFP sequences
  * - partial_fourier_phase: The amount of partial Fourier undersampling in phase encoding direction (0=no undersampling, 1=half fourier)
  * - partial_fourier_read: The amount of partial Fourier undersampling  in readout direction (0=no undersampling, 1=half fourier)
  * - partial_fourier_read_at_end: If set to 'true', partial Fourier will omit data at the end of the readout
  * - os_factor:    The oversampling factor, os_factor=1 means no oversampling
  * - nucleus:      The nucleus to observe
  */
  SeqGradEcho(const STD_string& object_label, SeqPulsar& exc,
              double sweepwidth, unsigned int readnpts, float FOVread,
              unsigned int phasenpts, float FOVphase, encodingScheme scheme=linearEncoding,
              reorderScheme reorder=noReorder, unsigned int nsegments=1, unsigned int reduction=1, unsigned int acl_bands=DEFAULT_ACL_BANDS,
              bool balanced=false, float partial_fourier_phase=0.0, float partial_fourier_read=0.0, bool partial_fourier_read_at_end=false,
              float os_factor=1.0, const STD_string& nucleus="");


/**
  * Constructs a gradient echo module labeled 'object_label' for 3D imaging with the following properties:
  * - readnpts:     Matrix size in frequency-encoding direction
  * - FOVread:      The FOV in frequency-encoding direction
  * - phasenpts:    Matrix size in phase-encoding direction
  * - FOVphase:     The FOV in phase-encoding direction
  * - slicenpts:    Matrix size in the 2nd phase-encoding (slice) direction
  * - FOVslice:     The FOV in the 2nd phase-encoding (slice) direction
  * - exc:          The excitation pulse
  * - sweepwidth:   The sampling frequency
  * - reduction:    Reduction factor for parallel imaging (sparsely sampled k-space), the factor applies to both phase-encoding directions
  * - acl_bands:    Number of autocalibration bands (bunch of adjacent lines between actual k-space lines) for GRAPPA
  * - balanced:     Whether rephasers (+ exc dephaser) should be added after acquisition, suitable for SSFP sequences
  * - partial_fourier_phase: The amount of partial Fourier undersampling in phase encoding direction (0=no undersampling, 1=half fourier)
  * - partial_fourier_phase3d: The amount of partial Fourier undersampling in 2nd phase encoding direction (0=no undersampling, 1=half fourier)
  * - partial_fourier_read: The amount of partial Fourier undersampling  in readout direction (0=no undersampling, 1=half fourier)
  * - partial_fourier_read_at_end: If set to 'true', partial Fourier will omit data at the end of the readout
  * - os_factor:    The oversampling factor, os_factor=1 means no oversampling
  * - nucleus:      The nucleus to observe
  */
  SeqGradEcho(const STD_string& object_label,
              unsigned int readnpts, float FOVread, unsigned int phasenpts, float FOVphase, unsigned int slicenpts, float FOVslice,
              SeqPulsar& exc, double sweepwidth,
              unsigned int reduction=1, unsigned int acl_bands=DEFAULT_ACL_BANDS,
              bool balanced=false, float partial_fourier_phase=0.0, float partial_fourier_phase3d=0.0,
              float partial_fourier_read=0.0, bool partial_fourier_read_at_end=false,
              float os_factor=1.0, const STD_string& nucleus="");


/**
  * Constructs an empty SeqGradEcho
  */
  SeqGradEcho(const STD_string& object_label="unnamedSeqGradEcho");

/**
  * Constructs a copy of 'sge'
  */
  SeqGradEcho(const SeqGradEcho& sge);

/**
  * Assignment operator that makes this object become a copy of 'sge'
  */
  SeqGradEcho& operator = (const SeqGradEcho& sge);

/**
  * Returns the phase encoding vector as a sequence object (for loop insertion)
  */
  SeqVector& get_pe_vector();

/**
  * Sets the reordering scheme and the number of segments for phase encoding
  */
  SeqGradEcho& set_pe_reorder_scheme(reorderScheme scheme,unsigned int nsegments);

/**
  * Returns the phase encoding reordering vector (for loop insertion)
  */
  const SeqVector& get_pe_reorder_vector() const;

/**
  * Returns the 2nd phase encoding vector as a sequence object (for loop insertion)
  */
  SeqVector& get_pe3d_vector();

/**
  * Returns the excitation pulse as a sequence object (for loop insertion)
  */
  const SeqVector& get_exc_vector() const {return *pulsptr.get_handled();}

/**
  * Sets the reordering scheme for the frequency list of the excitation pulse
  */
  SeqGradEcho& set_freq_reorder_scheme(reorderScheme scheme,unsigned int nsegments=1) {(*pulsptr.get_handled()).set_reorder_scheme(scheme,nsegments); return *this;}



/**
  * Returns the reorder vector for the frequency list of the excitation pulse (for loop insertion)
  */
  const SeqVector& get_freq_reorder_vector() const {return (*pulsptr.get_handled()).get_reorder_vector();}


/**
  * Returns the echo time (from the middle of excitation pulse to the middle of the acquisition window)
  */
  double get_echo_time() const;


/**
  * Places an additional sequence object between excitation and readout
  */
  SeqGradEcho& set_midpart(const SeqObjBase& soa);


  // implementing virtual functions of SeqGradInterface
  SeqGradInterface& set_strength(float gradstrength) {return *this;}  // nothing useful
  float get_strength() const {return 0.0;}  // nothing useful
  SeqGradInterface& invert_strength();
  double get_gradduration() const {return get_duration();}
  SeqGradInterface& set_gradrotmatrix(const RotMatrix& matrix);
  fvector get_gradintegral() const;


  // marshalling virtual functions of SeqAcqInterface to acqread, except:
  double get_acquisition_center() const {return get_preacq()+acqread.get_acquisition_center();}
  double get_acquisition_start() const {return get_preacq()+acqread.get_acquisition_start();}
  SeqAcqInterface& set_template_type(templateType type);




 private:
  double get_preacq() const;

  void common_init(const STD_string& objlabel);

  void build_seq();

  // hide these functions from the user
  SeqAcqInterface& set_pe_vector(const SeqVector& pevec) {return *this;}


  Handler<SeqPulsNdim*> pulsptr;
  SeqPulsarReph pls_reph;
  SeqGradVector phase;
  SeqGradVector phase3d;
  SeqGradVector phase_rew;
  SeqGradVector phase3d_rew;
  SeqSimultanVector phasesim;
  SeqSimultanVector phasesim3d;
  SeqSimultanVector phasereordsim;
  SeqAcqRead acqread;
  SeqGradConst readdeph;
  SeqParallel postexcpart;
  SeqParallel postacqpart;
  SeqObjList midpart;

  geometryMode mode;
  bool balanced_grads;
};

/** @}
  */



#endif
