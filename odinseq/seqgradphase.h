/***************************************************************************
                          seqgradphase.h  -  description
                             -------------------
    begin                : Tue Mar 21 2006
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQGRADPHASE_H
#define SEQGRADPHASE_H

#include <odinseq/seqgradpulse.h>
#include <odinseq/seqsimvec.h>

#define DEFAULT_ACL_BANDS 4

/**
  * @addtogroup odinseq
  * @{
  */

/**
  * \brief Phase Encoding
  *
  * A phase encoding gradient
  *
  */
class SeqGradPhaseEnc : public SeqGradVectorPulse {

 public:

/**
  * Constructs a phase encoding gradient labeled 'object_label' with the following properties:
  * - nsteps:       The number of phase encoding steps
  * - fov:          The Field of View
  * - gradchannel:  The channel this object should be played out
  * - gradstrength: The maximum gradient strength
  * - scheme:       The phase encoding scheme
  * - reorder:      The reordering scheme
  * - nsegments:    The number of segments for segmented reordering
  * - reduction:    Reduction factor for parallel imaging (sparsely sampled k-space)
  * - acl_bands:    Number of autocalibration bands (bunch of adjacent lines between actual k-space lines) for GRAPPA
  * - partial_fourier: The amount of partial Fourier undersampling (0=no undersampling, 1=half fourier)
  * - nucleus:      The nucleus this object should be calculated for
  */
  SeqGradPhaseEnc(const STD_string& object_label, unsigned int nsteps,
                  float fov, direction gradchannel, float gradstrength,
                  encodingScheme scheme=linearEncoding, reorderScheme reorder=noReorder,
                  unsigned int nsegments=1, unsigned int reduction=1, unsigned int acl_bands=DEFAULT_ACL_BANDS,
                  float partial_fourier=0.0, const STD_string& nucleus="");

/**
  * Constructs a phase encoding gradient labeled 'object_label' with the following properties:
  * - nsteps:       The number of phase encoding steps
  * - fov:          The Field of View
  * - gradduration: The duration of the gradient
  * - gradchannel:  The channel this object should be played out
  * - scheme:       The phase encoding scheme
  * - reorder:      The reordering scheme
  * - nsegments:    The number of segments for segmented reordering
  * - reduction:    Reduction factor for parallel imaging (sparsely sampled k-space)
  * - acl_bands:    Number of autocalibration bands (bunch of adjacent lines between actual k-space lines) for GRAPPA
  * - partial_fourier: The amount of partial Fourier undersampling (0=no undersampling, 1=half fourier)
  * - nucleus:      The nucleus this object should be calculated for
  */
  SeqGradPhaseEnc(const STD_string& object_label, unsigned int nsteps,
                  float fov, float gradduration, direction gradchannel,
                  encodingScheme scheme=linearEncoding, reorderScheme reorder=noReorder,
                  unsigned int nsegments=1, unsigned int reduction=1, unsigned int acl_bands=DEFAULT_ACL_BANDS,
                  float partial_fourier=0.0, const STD_string& nucleus="");


/**
  * Constructs an empty phase encoding gradient labeled 'object_label'
  */
  SeqGradPhaseEnc(const STD_string& object_label="unnamedSeqGradPhaseEnc")  : SeqGradVectorPulse(object_label) {}

/**
  * Constructs a copy of 'sgpe'
  */
  SeqGradPhaseEnc(const SeqGradPhaseEnc& sgpe) {SeqGradPhaseEnc::operator = (sgpe);}

/**
  * Assignment operator that makes this object become a copy of 'sgpe'
  */
  SeqGradPhaseEnc& operator = (const SeqGradPhaseEnc& sgpe) {SeqGradVectorPulse::operator = (sgpe); return *this;}


 private:
  void init_encoding(unsigned int nsteps,encodingScheme scheme, reorderScheme reorder, unsigned int nsegments, unsigned int reduction, unsigned int acl_bands, float partial_fourier);

};


///////////////////////////////////////////////////////////////////////////////////////////////////


/**
  * \brief Flow-Compensated Phase Encoding
  *
  * A phase encoding gradient with 1st order flow compensation
  *
  */
class SeqGradPhaseEncFlowComp : public SeqGradChanList {

 public:

/**
  * Constructs a flow-compensated phase encoding gradient labeled 'object_label' with the following properties:
  * - t0:           The starting time of the gradient after excitation
  * - nsteps:       The number of phase encoding steps
  * - fov:          The Field of View
  * - gradchannel:  The channel this object should be played out
  * - gradstrength: The maximum gradient strength
  * - scheme:       The phase encoding scheme
  * - reorder:      The reordering scheme
  * - nsegments:    The number of segments for segmented reordering
  * - reduction:    Reduction factor for parallel imaging (sparsely sampled k-space)
  * - acl_bands:    Number of autocalibration bands (bunch of adjacent lines between actual k-space lines) for GRAPPA
  * - partial_fourier: The amount of partial Fourier undersampling (0=no undersampling, 1=half fourier)
  * - nucleus:      The nucleus this object should be calculated for
  */
  SeqGradPhaseEncFlowComp(const STD_string& object_label, double t0, unsigned int nsteps,
                  float fov, direction gradchannel, float gradstrength,
                  encodingScheme scheme=linearEncoding, reorderScheme reorder=noReorder,
                  unsigned int nsegments=1, unsigned int reduction=1, unsigned int acl_bands=DEFAULT_ACL_BANDS,
                  float partial_fourier=0.0, const STD_string& nucleus="");


/**
  * Constructs an empty flow-compensated phase encoding gradient labeled 'object_label'
  */
  SeqGradPhaseEncFlowComp(const STD_string& object_label="unnamedSeqGradPhaseEncFlowComp")  : SeqGradChanList(object_label) {}

/**
  * Constructs a copy of 'sgpefc'
  */
  SeqGradPhaseEncFlowComp(const SeqGradPhaseEncFlowComp& sgpefc) {SeqGradPhaseEncFlowComp::operator = (sgpefc);}

/**
  * Assignment operator that makes this object become a copy of 'sgpefc'
  */
  SeqGradPhaseEncFlowComp& operator = (const SeqGradPhaseEncFlowComp& sgpefc);

/**
  * conversion operator for loop insertion
  */
  operator const SeqVector& () const {return simvec;}

 private:

  void build_seq();

  static void calc_flowcomp_pe(float& negfact, float& tc, float Gpos, float M0, float t0, float slewrate);

  SeqGradVectorPulse pos;
  SeqGradVectorPulse neg;
  SeqSimultanVector simvec;

};

/** @}
  */

#endif
