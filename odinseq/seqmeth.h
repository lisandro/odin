/***************************************************************************
                          seqmeth.h  -  description
                             -------------------
    begin                : Thu Aug 9 2001
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SEQMETH_H
#define SEQMETH_H

#include <tjutils/tjstate.h>
#include <tjutils/tjhandler.h>

#include <odinpara/protocol.h>

#include <odinseq/seqlist.h>
#include <odinseq/seqplatform.h>


//////////////////////////////////////////////////////////////////////

// forward declarations
class SeqMethod;
class SeqPulsar;


//////////////////////////////////////////////////////////////////////


/**
  * @ingroup odinseq_internals
  * Proxy class to deal with different ODIN methods at once
  */
class SeqMethodProxy : public StaticHandler<SeqMethodProxy> {


 public:
  SeqMethodProxy() {
    Log<Seq> odinlog("SeqMethodProxy","SeqMethodProxy()");
  }

  static void set_current_method(unsigned int index);
  static SeqMethod* get_current_method();
  static unsigned int get_numof_methods();
  static unsigned int delete_methods();
  static const char* get_method_label();

  static const char* get_status_string();

  // pointer-like syntax to access current method
  SeqMethod* operator -> () {return get_current_method();}
  SeqMethod& operator [] (unsigned int index);

  static bool load_method_so(const STD_string& so_filename);

  // functions to initialize/delete static members by the StaticHandler template class
  static void init_static();
  static void destroy_static();


 protected:

  // called by derivedmethod classes to register themselves
  static void register_method(SeqMethod* meth);


 private:

  // struct to store references to all registered methods
  struct MethodList : public STD_list<SeqMethod*>, public Labeled {};

  static SingletonHandler<MethodList,false> registered_methods;

  
  // struct to store references to the current method
  struct MethodPtr : public Labeled {
    SeqMethod* ptr;
  };

  static SingletonHandler<MethodPtr,false> current_method;

  static SeqMethod* empty_method;


};



//////////////////////////////////////////////////////////////////////

/*
  State diagram for SeqMethod, i.e. for all NMR sequences:

  Possible transitions between states are indicated by
  the corresponding member functions that perform the transition.


  empty <--------\
                 |
    |            | clear()
    | init()     |
    |            |
    V            |
                 |
  initialised ---|
                 |
    |            |
    | build()    |
    |            |
    V            |
                 |
  built ---------|
                 |
    |            |
    | prepare()  |
    |            |
    V            |
                 |
  prepared ------'

*/




/**
  * @ingroup odinseq
  *
  * \brief Base class for methods (sequences)
  *
  * The base class for all NMR sequences.
  * To create a new sequence, write a new class which is derived
  * from SeqMethod and overload the following virtual functions
  * with the specified functionality:
  * - void method_pars_init(): Initialize sequence parameters (set default values,
  *                            append to the list of registered parameters of the method)
  * - void method_seq_init(): Create the internal layout of the sequence, i.e.
  *                           the sequence tree
  * - void method_rels(): Calculate timings of the sequence
  * - void method_pars_set(): Finalize the sequence
  *
  * To get an idea what has to be done in each function, please take a
  * look at the example sequences.
  */
class SeqMethod : protected SeqMethodProxy, public SeqObjList, public virtual LDReditCaller, public StateMachine<SeqMethod> {


 public:

/**
  * Constructs a method with the given label.
  */
  SeqMethod(const STD_string& method_label);

/**
  * Destructor
  */
  virtual ~SeqMethod();

  // public interface to obtain the different states:

/**
  * Performs state transition to 'empty'
  */
  bool clear() {return empty.obtain_state();}

/**
  * Performs state transition to 'initialised'
  */
  bool init() {return initialised.obtain_state();}

/**
  * Performs state transition to 'built'
  */
  bool build() {return built.obtain_state();}

/**
  * Performs state transition to 'prepared'
  */
  bool prepare() {return prepared.obtain_state();}

/**
  * Updates the timing of the sequence (TR, TE) without changing its structure.
  * If the sequence is not yet in the 'built' state, this state will
  * be obtained first.
  */
  bool update_timings();

/**
  * Prepares the acquisition parameters, i.e. recoInfo structure
  * and performs platform specific preparation (generating pulse program, etc.).
  * This function will be called by the current frontend (ODIN GUI, odin2idea, ...)
  * prior to the measurement.
  */
  bool prep_acquisition() const;


/**
  * Returns the description of the method
  */
  const char* get_description() const {return description.c_str();}

/**
  * This function is used in the ODINMAIN function to hand over control to the SeqMethod object.
  */
  int process(int argc, char *argv[]);


/**
  * Loads the sequence protocol (systemInfo, geometryInfo, sequencePars) from file 'filename'
  */
  int load_protocol(const STD_string& filename);

/**
  * Writes the sequence protocol (systemInfo, geometryInfo, sequencePars) to file 'filename'
  */
  int write_protocol(const STD_string& filename) const {create_protcache(); return protcache->write(filename);}


/**
  * Returns the method-specific parameters
  */
  LDRblock& get_methodPars() {return *methodPars;}

/**
  * Loads the sequence parameters (commonPars & methodPars) from file
  */
  int load_sequencePars(const STD_string& filename);

/**
  * Writes the sequence parameters (commonPars & methodPars) to file
  */
  int write_sequencePars(const STD_string& filename) const;

/**
  * Sets the value of the sequence parameter 'parameter_label' to 'value'.
  * Returns true if successful.
  */
  bool set_sequenceParameter(const STD_string& parameter_label, const STD_string& value);

/**
  * Sets the current set of common sequence parameters
  */
  SeqMethod& set_commonPars(const SeqPars& pars);

/**
  * Returns the current set of common sequence parameters
  */
  SeqPars& get_commonPars() {return (*commonPars);}

/**
  * Sets the current geometry to 'geo'
  */
  SeqMethod& set_geometry(const Geometry& geo)  {geometryInfo->operator = (geo); return *this;}

/**
  * Returns the current geometry
  */
  Geometry& get_geometry() {return *(geometryInfo.unlocked_ptr());}

/**
  * Loads the current geometry from disk
  */
  int load_geometry(const STD_string& filename) {return geometryInfo->load(filename);}

/**
  * Writes the current geometry to disk
  */
  int write_geometry(const STD_string& filename) const {return geometryInfo->write(filename);}

/**
  * Returns reference to the current study info
  */
  Study& get_studyInfo() {return *(studyInfo.unlocked_ptr());}

/**
  * Writes the systemInfo to disk
  */
  int write_systemInfo(const STD_string& filename) const {return systemInfo->write(filename);}

/**
  * Loads the systemInfo from disk
  */
  int load_systemInfo(const STD_string& filename);

/**
  * Initialises the method with the given system-specific stuff
  */
  SeqMethod& init_systemInfo(double basicfreq,double maxgrad,double slewrate);

/**
  * Writes the recoInfo to disk
  */
  int write_recoInfo(const STD_string& filename) const;

/**
  * Writes all files which describe the measurement, uses the given filename-prefix
  */
  int write_meas_contex(const STD_string& prefix) const;

/**
  * Returns the scan duration for this method
  */
  double get_totalDuration() const;

/**
  * Returns the current main nucleus of the sequence, i.e. the nucleus for which both, excitation and acquisition, will be performed
  */
  STD_string get_main_nucleus() const {return systemInfo->get_main_nucleus();}

/**
  * Returns the total number of acquisition windows in the sequence.
  */
  unsigned int get_numof_acquisitions() const;

/**
  * Returns a list of Pulsar pulses which are currently
  * active.
  */
  STD_list<const SeqPulsar*>  get_active_pulsar_pulses() const;
  
  // overloaded virtual functions from SeqTreeObj
  unsigned int event(eventContext& context) const;

  // leave this public for idea_emulation
  void set_current_testcase(unsigned int index) {if(index<numof_testcases()) current_testcase=index;}


 protected:

/**
  * This function must be implemented by the method programmer. It should contain all
  * code that initialies parameters of the method.
  */
  virtual void method_pars_init() = 0;

/**
  * This function must be implemented by the method programmer. It should contain all
  * code that initialises the sequence objects and their arrangement.
  */
  virtual void method_seq_init() = 0;

/**
  * This function must be implemented by the method programmer. It should contain all
  * code that changes the aspects of the sequence objects without changing their
  * serialisation, .e.g. timing calculations should be coded in this function.
  */
  virtual void method_rels() = 0;

/**
  * This function must be implemented by the method programmer. It should contain all
  * code that is called only once before the sequence is started, e.g. the setup of the reconsruction.
  */
  virtual void method_pars_set() = 0;


/**
  * Append a user defined parameter to the list of parameters that describe the method
  */
  SeqMethod& append_parameter(LDRbase& ldr,const STD_string& label,parameterMode parmode=edit);

/**
  * This function is used to specify the sequence of this method that will be played out
  */
  SeqMethod& set_sequence(const SeqObjBase& s);


/**
  * Specifies the description of the method
  */
  SeqMethod& set_description(const char* descr) {description=descr;return *this;}


/**
  * This function can be implemented by the method programmer to indicate
  * how many different use cases are there to be tested during sequence test.
  */
  virtual unsigned int numof_testcases() const {return 1;}

/**
  * Returns the index of the current test case, used in method_pars_init to 
  * assign different parameter settings for each test case.
  */
  unsigned int get_current_testcase() const {return current_testcase;}


/**
  * Pointer to the current set of common sequence parameters
  */
  SeqPars* commonPars;

 private:
  friend class SeqMethodProxy;
  friend class SeqCmdLine;

  void set_parblock_labels();

  bool calc_timings();


  // overloaded virtual function from LDReditCaller
  void parameter_relations(LDReditWidget* editwidget);

  // interface for quick access to current platform
  mutable SeqPlatformProxy platform;

  // block to hold method-specific parameters
  LDRblock* methodPars;

  STD_string description;

  unsigned int current_testcase;


  // cache handle to method plug-in so that it can be removed later
  void *dl_handle;


  void create_protcache() const;
  mutable Protocol* protcache; // used for returning reference to protocol, local static object does not work on VxWorks



  // states and their transition functions:

  State<SeqMethod> empty;
  bool reset();

  State<SeqMethod> initialised;
  bool empty2initialised();

  State<SeqMethod> built;
  bool initialised2built();

  State<SeqMethod> prepared;
  bool built2prepared();
};

//////////////////////////////////////////////////////////////////////

// Macro to include in every sequence file to create an entry point

// Double-stage macros to stringize METHOD_LABEL, see http://c-faq.com/ansi/stringize.html
#define ODINMETHOD_STRINGIZE_MACRO(x) #x
#define ODINMETHOD_STRINGIZE(x) ODINMETHOD_STRINGIZE_MACRO(x)


#ifdef NO_CMDLINE

#define ODINMETHOD_ENTRY_POINT \
int ODINMAIN(int argc, char *argv[]) {\
  return (new METHOD_CLASS(ODINMETHOD_STRINGIZE(METHOD_LABEL)))->process(argc,argv); \
}

#else

#define ODINMETHOD_ENTRY_POINT \
int ODINMAIN(int argc, char *argv[]) {\
  if(LogBase::set_log_levels(argc,argv,false)) return 0; \
  return (new METHOD_CLASS(ODINMETHOD_STRINGIZE(METHOD_LABEL)))->process(argc,argv); \
}

#endif


#endif
