#include "seqoperator.h"

#include "seqobj.h"
#include "seqgradobj.h"
#include "seqlist.h"
#include "seqloop.h"
#include "seqdec.h"
#include "seqparallel.h"
#include "seqgradchan.h"
#include "seqgradchanlist.h"
#include "seqgradchanparallel.h"


#ifdef HAVE_TYPEINFO
#include <typeinfo>
#endif


void bad_parallel(const SeqGradInterface& s1, const SeqGradInterface& s2, direction chan) {
  Log<Seq> odinlog("","bad_parallel");

  STD_string typestr;
#ifdef HAVE_TYPEINFO
  typestr=STD_string("[")+typeid(s1).name()+"/"+typeid(s2).name()+"] ";
#endif

  ODINLOG(odinlog,errorLog) << typestr << "(" << s1.get_label() << ") / (" << s2.get_label() << ") - same channel: " << directionLabel[chan] << STD_endl;
}


/////////////////////////////////////////////////////////////////

void SeqOperator::append_list2list(SeqObjList& dst, const SeqObjList& src) {

  if(src.gradrotmatrixvec.get_handled()) {
    dst+=src; // keep whole object in list for correct rotation
  } else {
    STD_list<const SeqObjBase*>::const_iterator constiter;
    for(constiter=src.get_const_begin();constiter!=src.get_const_end();++constiter) {
      dst+=(**constiter);
    }
  }
}

SeqObjList& SeqOperator::create_SeqObjList_label(const STD_string& label1, const STD_string& label2, bool swap) {
  STD_string l1=label1;
  STD_string l2=label2;
  if(swap) {
    l1=label2;
    l2=label1;
  }
  SeqObjList* result=new  SeqObjList(l1+"+"+l2);
  result->set_temporary();
  return *result;
}

SeqObjList& SeqOperator::create_SeqObjList_obj(const SeqObjBase& s1, const SeqObjBase& s2, bool swap) {
  SeqObjList& result=create_SeqObjList_label(s1.get_label(),s2.get_label(),swap);
  if(!swap) result+=s1;
  result+=s2;
  if(swap) result+=s1;
  return result;
}


SeqObjList& SeqOperator::create_SeqObjList_list(const SeqObjList& s1, const SeqObjBase& s2, bool swap) {
  SeqObjList& result=create_SeqObjList_label(s1.get_label(),s2.get_label(),swap);
  if(swap) result+=s2;
  append_list2list(result,s1);
  if(!swap) result+=s2;
  return result;
}

SeqGradChanList& SeqOperator::create_SeqGradChanList(const STD_string& label1, const STD_string& label2, bool swap) {
  STD_string l1=label1;
  STD_string l2=label2;
  if(swap) {
    l1=label2;
    l2=label1;
  }
  SeqGradChanList* result=new  SeqGradChanList(l1+"+"+l2);
  result->set_temporary();
  return *result;
}

SeqGradChanList& SeqOperator::create_SeqGradChanList(SeqGradChan& sgc) {
  SeqGradChanList* result=new  SeqGradChanList("("+sgc.get_label()+")");
  result->set_temporary();
  (*result)+=sgc;  
  return *result;
}
  

SeqGradChanParallel& SeqOperator::create_SeqGradChanParallel_concat(const STD_string& label1, const STD_string& label2, bool swap) {
  STD_string l1=label1;
  STD_string l2=label2;
  if(swap) {
    l1=label2;
    l2=label1;
  }
  SeqGradChanParallel* result=new  SeqGradChanParallel(l1+"+"+l2);
  result->set_temporary();
  return *result;
}
  
SeqParallel& SeqOperator::create_SeqParallel(const STD_string& label1, const STD_string& label2) {
  SeqParallel* result=new  SeqParallel(label1+"/"+label2);
  result->set_temporary();
  return *result;
}

SeqGradChanParallel& SeqOperator::create_SeqGradChanParallel_simultan(const STD_string& label1, const STD_string& label2) {
  SeqGradChanParallel* result=new  SeqGradChanParallel(label1+"/"+label2);
  result->set_temporary();
  return *result;
}
  
////////////////////////////////////////////////////////////////////////////////////////////

SeqObjList& SeqOperator::concat(const SeqObjBase& s1, const SeqObjBase& s2) {
  SeqObjList& result=create_SeqObjList_label(s1.get_label(),s2.get_label(),false);
  result+=s1;
  result+=s2;
  return result;
}

SeqObjList& SeqOperator::concat(const SeqObjBase& s1, const SeqObjList& s2, bool swap) {
  return create_SeqObjList_list(s2,s1,!swap);
}

SeqObjList& SeqOperator::concat(const SeqObjBase& s1, const SeqObjLoop& s2, bool swap) {
  return create_SeqObjList_obj(s1,s2,swap);
}


SeqObjList& SeqOperator::concat(const SeqObjBase& s1, const SeqDecoupling& s2, bool swap) {
  return create_SeqObjList_obj(s1,s2,swap);
}

SeqObjList& SeqOperator::concat(const SeqObjBase& s1, const SeqParallel& s2, bool swap) {
  return create_SeqObjList_obj(s1,s2,swap);
}

SeqObjList& SeqOperator::concat(const SeqObjBase& s1, SeqGradChan& s2, bool swap) {
  SeqObjList& result=create_SeqObjList_label(s1.get_label(),s2.get_label(),swap);
  if(!swap) result+=s1;
  result+=s2;
  if(swap) result+=s1;
  return result;
}

SeqObjList& SeqOperator::concat(const SeqObjBase& s1, SeqGradChanList& s2, bool swap) {
  SeqObjList& result=create_SeqObjList_label(s1.get_label(),s2.get_label(),swap);
  if(!swap) result+=s1;
  result+=s2;
  if(swap) result+=s1;
  return result;
}

SeqObjList& SeqOperator::concat(const SeqObjBase& s1, SeqGradChanParallel& s2, bool swap) {
  SeqObjList& result=create_SeqObjList_label(s1.get_label(),s2.get_label(),swap);
  if(!swap) result+=s1;
  result+=s2;
  if(swap) result+=s1;
  return result;
}

SeqObjList& SeqOperator::concat(const SeqObjList& s1, const SeqObjList& s2) {
  SeqObjList& result=create_SeqObjList_label(s1.get_label(),s2.get_label(),false);
  append_list2list(result,s1);
  append_list2list(result,s2);
  return result;
}
  
SeqObjList& SeqOperator::concat(const SeqObjList& s1, const SeqObjLoop& s2, bool swap) {
  return create_SeqObjList_list(s1,s2,swap);
}

SeqObjList& SeqOperator::concat(const SeqObjList& s1, const SeqDecoupling& s2, bool swap) {
  return create_SeqObjList_list(s1,s2,swap);
}

SeqObjList& SeqOperator::concat(const SeqObjList& s1, const SeqParallel& s2, bool swap) {
  return create_SeqObjList_list(s1,s2,swap);
}
  
SeqObjList& SeqOperator::concat(const SeqObjList& s1, SeqGradChan& s2, bool swap) {
  SeqObjList& result=create_SeqObjList_label(s1.get_label(),s2.get_label(),swap);
  if(swap) result+=s2;
  append_list2list(result,s1);
  if(!swap) result+=s2;
  return result;
}

SeqObjList& SeqOperator::concat(const SeqObjList& s1, SeqGradChanList& s2, bool swap) {
  SeqObjList& result=create_SeqObjList_label(s1.get_label(),s2.get_label(),swap);
  if(swap) result+=s2;
  append_list2list(result,s1);
  if(!swap) result+=s2;
  return result;
}

SeqObjList& SeqOperator::concat(const SeqObjList& s1, SeqGradChanParallel& s2, bool swap) {
  SeqObjList& result=create_SeqObjList_label(s1.get_label(),s2.get_label(),swap);
  if(swap) result+=s2;
  append_list2list(result,s1);
  if(!swap) result+=s2;
  return result;
}
  
SeqObjList& SeqOperator::concat(const SeqObjLoop& s1, const SeqObjLoop& s2) {
  return create_SeqObjList_obj(s1,s2,false);
}
  
SeqObjList& SeqOperator::concat(const SeqObjLoop& s1, const SeqDecoupling& s2, bool swap) {
  return create_SeqObjList_obj(s1,s2,swap);
}

SeqObjList& SeqOperator::concat(const SeqObjLoop& s1, const SeqParallel& s2, bool swap) {
  return create_SeqObjList_obj(s1,s2,swap);
}

SeqObjList& SeqOperator::concat(const SeqObjLoop& s1, SeqGradChan& s2, bool swap) {
  SeqObjList& result=create_SeqObjList_label(s1.get_label(),s2.get_label(),swap);
  if(swap) result+=s2;
  result+=s1;
  if(!swap) result+=s2;
  return result;
}

SeqObjList& SeqOperator::concat(const SeqObjLoop& s1, SeqGradChanList& s2, bool swap) {
  SeqObjList& result=create_SeqObjList_label(s1.get_label(),s2.get_label(),swap);
  if(swap) result+=s2;
  result+=s1;
  if(!swap) result+=s2;
  return result;
}

SeqObjList& SeqOperator::concat(const SeqObjLoop& s1, SeqGradChanParallel& s2, bool swap) {
  SeqObjList& result=create_SeqObjList_label(s1.get_label(),s2.get_label(),swap);
  if(swap) result+=s2;
  result+=s1;
  if(!swap) result+=s2;
  return result;
}

SeqObjList& SeqOperator::concat(const SeqDecoupling& s1, const SeqDecoupling& s2) {
  return create_SeqObjList_obj(s1,s2,false);
}

SeqObjList& SeqOperator::concat(const SeqDecoupling& s1, const SeqParallel& s2, bool swap) {
  return create_SeqObjList_obj(s1,s2,swap);
}

SeqObjList& SeqOperator::concat(const SeqDecoupling& s1, SeqGradChan& s2, bool swap) {
  SeqObjList& result=create_SeqObjList_label(s1.get_label(),s2.get_label(),swap);
  if(swap) result+=s2;
  result+=s1;
  if(!swap) result+=s2;
  return result;
}

SeqObjList& SeqOperator::concat(const SeqDecoupling& s1, SeqGradChanList& s2, bool swap) {
  SeqObjList& result=create_SeqObjList_label(s1.get_label(),s2.get_label(),swap);
  if(swap) result+=s2;
  result+=s1;
  if(!swap) result+=s2;
  return result;
}

SeqObjList& SeqOperator::concat(const SeqDecoupling& s1, SeqGradChanParallel& s2, bool swap) {
  SeqObjList& result=create_SeqObjList_label(s1.get_label(),s2.get_label(),swap);
  if(swap) result+=s2;
  result+=s1;
  if(!swap) result+=s2;
  return result;
}

SeqObjList& SeqOperator::concat(const SeqParallel& s1, const SeqParallel& s2) {
  return create_SeqObjList_obj(s1,s2,false);
}

SeqObjList& SeqOperator::concat(const SeqParallel& s1, SeqGradChan& s2, bool swap) {
  SeqObjList& result=create_SeqObjList_label(s1.get_label(),s2.get_label(),swap);
  if(swap) result+=s2;
  result+=s1;
  if(!swap) result+=s2;
  return result;
}

SeqObjList& SeqOperator::concat(const SeqParallel& s1, SeqGradChanList& s2, bool swap) {
  SeqObjList& result=create_SeqObjList_label(s1.get_label(),s2.get_label(),swap);
  if(swap) result+=s2;
  result+=s1;
  if(!swap) result+=s2;
  return result;
}

SeqObjList& SeqOperator::concat(const SeqParallel& s1, SeqGradChanParallel& s2, bool swap) {
  SeqObjList& result=create_SeqObjList_label(s1.get_label(),s2.get_label(),swap);
  if(swap) result+=s2;
  result+=s1;
  if(!swap) result+=s2;
  return result;
}

SeqGradChanList& SeqOperator::concat(SeqGradChan& s1, SeqGradChan& s2) {
  SeqGradChanList& result=create_SeqGradChanList(s1.get_label(),s2.get_label(),false);
  result+=s1;
  result+=s2;
  return result;
}

SeqGradChanList& SeqOperator::concat(SeqGradChan& s1, SeqGradChanList& s2, bool swap) {
  SeqGradChanList& result=create_SeqGradChanList(s1.get_label(),s2.get_label(),swap);
  if(swap) result+=s2;
  result+=s1;
  if(!swap) result+=s2;
  return result;
}

SeqGradChanParallel& SeqOperator::concat(SeqGradChan& s1, SeqGradChanParallel& s2, bool swap) {
  SeqGradChanParallel& result=create_SeqGradChanParallel_concat(s1.get_label(),s2.get_label(),swap);
  if(swap) result+=s2;
  result+=s1;
  if(!swap) result+=s2;
  return result;
}
  
SeqGradChanList& SeqOperator::concat(SeqGradChanList& s1, SeqGradChanList& s2) {
  SeqGradChanList& result=create_SeqGradChanList(s1.get_label(),s2.get_label(),false);
  result+=s1;
  result+=s2;
  return result;
}
  
SeqGradChanParallel& SeqOperator::concat(SeqGradChanList& s1, SeqGradChanParallel& s2, bool swap) {
  SeqGradChanParallel& result=create_SeqGradChanParallel_concat(s1.get_label(),s2.get_label(),swap);
  if(swap) result+=s2;
  result+=s1;
  if(!swap) result+=s2;
  return result;
}


SeqGradChanParallel& SeqOperator::concat(SeqGradChanParallel& s1, SeqGradChanParallel& s2) {
  SeqGradChanParallel& result=create_SeqGradChanParallel_concat(s1.get_label(),s2.get_label(),false);
  result+=s1;
  result+=s2;
  return result;
}

////////////////////////////////////////////////////////////////////////////////////////////


SeqParallel& SeqOperator::simultan(const SeqObjBase& s1, SeqGradObjInterface& s2) {
  SeqParallel& result=create_SeqParallel(s1.get_label(),s2.get_label());
  result.set_pulsptr(&s1);
  result.set_gradptr(&s2);
  return result;
}

SeqParallel& SeqOperator::simultan(const SeqObjBase& s1, SeqGradChan& s2) {
  SeqParallel& result=create_SeqParallel(s1.get_label(),s2.get_label());
  result.set_pulsptr(&s1);
  SeqGradChanParallel* sgcp=new SeqGradChanParallel("{"+s2.get_label()+"}");
  sgcp->set_temporary();
  (*sgcp)+=s2;
  result.set_gradptr((SeqGradObjInterface*)sgcp);
  return result;
}
  
SeqParallel& SeqOperator::simultan(const SeqObjBase& s1, SeqGradChanList& s2) {
  SeqParallel& result=create_SeqParallel(s1.get_label(),s2.get_label());
  result.set_pulsptr(&s1);
  SeqGradChanParallel* sgcp=new SeqGradChanParallel("{"+s2.get_label()+"}");
  sgcp->set_temporary();
  (*sgcp)+=s2;
  result.set_gradptr((SeqGradObjInterface*)sgcp);
  return result;
}

SeqGradChanParallel& SeqOperator::simultan(SeqGradChan& s1, SeqGradChan& s2) {
  SeqGradChanParallel& result=create_SeqGradChanParallel_simultan(s1.get_label(),s2.get_label());
  if(s1.get_channel()==s2.get_channel()) {
    bad_parallel(s1,s2,s1.get_channel());
    return result;
  }
  result.set_gradchan(s1.get_channel(),&create_SeqGradChanList(s1));
  result.set_gradchan(s2.get_channel(),&create_SeqGradChanList(s2));
  return result;
}

SeqGradChanParallel& SeqOperator::simultan(SeqGradChan& s1, SeqGradChanList& s2) {
  SeqGradChanParallel& result=create_SeqGradChanParallel_simultan(s1.get_label(),s2.get_label());
  if(s2.size() && s1.get_channel()==s2.get_channel()) {
    bad_parallel(s1,s2,s1.get_channel());
    return result;
  }
  result.set_gradchan(s1.get_channel(),&create_SeqGradChanList(s1));

  SeqGradChanList* s2_copy=new  SeqGradChanList(s2);
  s2_copy->set_temporary();
  result.set_gradchan(s2.get_channel(),s2_copy);
  return result;
}

SeqGradChanParallel& SeqOperator::simultan(SeqGradChan& s1, SeqGradChanParallel& s2) {
  SeqGradChanParallel* result=new SeqGradChanParallel(s2);
  result->set_label(s1.get_label()+"/"+s2.get_label());
  result->set_temporary();
  if(result->get_gradchan(s1.get_channel()))  {
    bad_parallel(s1,s2,s1.get_channel());
    return *result;
  }
  result->set_gradchan(s1.get_channel(),&create_SeqGradChanList(s1));
  return (*result);
}

SeqGradChanParallel& SeqOperator::simultan(SeqGradChanList& s1, SeqGradChanList& s2) {
  Log<Seq> odinlog("SeqOperator","simultan");
  SeqGradChanParallel& result=create_SeqGradChanParallel_simultan(s1.get_label(),s2.get_label());
  if(s1.size() && s2.size()) {
    if(s1.get_channel()==s2.get_channel()) {
      bad_parallel(s1,s2,s1.get_channel());
      return result;
    }
  }
  ODINLOG(odinlog,normalDebug) << "chan1/2=" << s1.get_channel() << "/" << s2.get_channel() << STD_endl;

  SeqGradChanList* s1_copy=new  SeqGradChanList(s1);
  s1_copy->set_temporary();
  result.set_gradchan(s1.get_channel(),s1_copy);


  SeqGradChanList* s2_copy=new  SeqGradChanList(s2);
  s2_copy->set_temporary();
  result.set_gradchan(s2.get_channel(),s2_copy);

  return result;
}
  
SeqGradChanParallel& SeqOperator::simultan(SeqGradChanList& s1, SeqGradChanParallel& s2) {
  SeqGradChanParallel *result=new SeqGradChanParallel(s2);
  result->set_label(s1.get_label()+"/"+s2.get_label());
  result->set_temporary();
  if(result->get_gradchan(s1.get_channel()))  {
    bad_parallel(s1,s2,s1.get_channel());
    return *result;
  } else {
    SeqGradChanList* s1_copy=new  SeqGradChanList(s1);
    s1_copy->set_temporary();
    result->set_gradchan(s1.get_channel(),s1_copy);
  }
  return *result;
}

SeqGradChanParallel& SeqOperator::simultan(SeqGradChanParallel& s1, SeqGradChanParallel& s2) {
  SeqGradChanParallel *result=new SeqGradChanParallel(s2);
  result->set_label(s1.get_label()+"/"+s2.get_label());
  result->set_temporary();

  for(unsigned int cn=readDirection; cn <= sliceDirection; cn++) {
    direction chanNo=direction(cn);
    if(result->get_gradchan(chanNo) && s1.get_gradchan(chanNo)) {
      bad_parallel(s1,s2,chanNo);
      return *result;
    } else {
      if(s1.get_gradchan(chanNo)) {
        SeqGradChanList* s1_copy=new  SeqGradChanList(*s1.get_gradchan(chanNo));
        s1_copy->set_temporary();
        result->set_gradchan(chanNo,s1_copy);
      }
    }
  }
  return *result;
}










