#include "seqvec.h"
#include "seqloop.h"

#include <tjutils/tjhandler_code.h>
#include <tjutils/tjlist_code.h>

void SeqVector::common_int() {
  reordvec=0;
  nr_cache_up2date=false;
}

SeqVector::SeqVector(const STD_string& object_label) {
  common_int();
  set_label(object_label);
}

SeqVector::SeqVector(const STD_string& object_label, unsigned int nindices, int slope, int offset ) {
  common_int();
  set_label(object_label);
  ivector ivec(nindices);
  for(unsigned int i=0; i<nindices; i++) ivec[i]=offset+slope*i;
  SeqVector::set_indexvec(ivec);
}

SeqVector::SeqVector(const SeqVector& sv) {
  common_int();
  SeqVector::operator = (sv);
}

SeqVector::~SeqVector() {
  Log<Seq> odinlog(this,"~SeqVector()");
  if(reordvec) delete reordvec;
}

SeqVector& SeqVector::operator = (const SeqVector& sv) {
  SeqClass::operator = (sv);
  indexvec=sv.indexvec;
  if(reordvec) delete reordvec; reordvec=0;
  if(sv.reordvec) reordvec=new SeqReorderVector(this,sv.reordvec);
  return *this;
}


unsigned int SeqVector::get_numof_iterations() const {
  Log<Seq> odinlog(this,"get_numof_iterations");
  unsigned int result=get_vectorsize();
  if(reordvec) {
    result=reordvec->get_reordered_size(result);
  }
  ODINLOG(odinlog,normalDebug) << "result=" << result << STD_endl;
  return result;
}

int SeqVector::get_current_index() const {
  Log<Seq> odinlog(this,"get_current_index");
  int result=0;

  const SeqVector* sim=simhandler.get_handled();
  if(sim) {
    result=sim->get_current_index();
    ODINLOG(odinlog,normalDebug) << "result(sim)=" << result << STD_endl;
  } else {
    if(loopcounter_is_active()) {
      result=get_loopcounter();
      ODINLOG(odinlog,normalDebug) << "result(loopcounter)=" << result << STD_endl;
    }
  }

  if(reordvec) {
    result=reordvec->get_reordered_index(result);
    ODINLOG(odinlog,normalDebug) << "result(reordvec)=" << result << STD_endl;
  }

  return result;
}

int SeqVector::get_acq_index() const {
  Log<Seq> odinlog(this,"get_acq_index");
  int result=get_current_index();
  if(result>=0 && result<int(indexvec.size())) { // Check whether we should use indexvec
    ODINLOG(odinlog,normalDebug) << "using indexvec" << STD_endl;
    result=indexvec[result];
  }
  ODINLOG(odinlog,normalDebug) << "result=" << result << STD_endl;
  return result;
}



SeqVector& SeqVector::set_reorder_scheme(reorderScheme scheme,unsigned int nsegments) {
  if(!reordvec) reordvec=new SeqReorderVector(this);
  else reordvec->clear_cache();
  reordvec->reord_scheme=scheme;
  reordvec->n_reord_segments=nsegments;
  return *this;
}


SeqVector& SeqVector::set_encoding_scheme(encodingScheme scheme) {
  if(!reordvec) reordvec=new SeqReorderVector(this);
  else reordvec->clear_cache();
  reordvec->encoding_scheme=scheme;
  return *this;
}



const SeqVector& SeqVector::get_reorder_vector() const {
  if(!reordvec) reordvec=new SeqReorderVector(this);
  return *reordvec;
}

nestingRelation SeqVector::get_nesting_relation() const {
  Log<Seq> odinlog(this,"get_nesting_relation");

  if(reordvec && reordvec->nr_cache_up2date==false) nr_cache_up2date=false;
  if(nr_cache_up2date) return nr_cache;

  nr_cache=noRelation;

  if(vechandler.get_handled()) {
    ODINLOG(odinlog,normalDebug) << "has vechandler" << STD_endl;
    if(reordvec && reordvec->vechandler.get_handled()) {
      ODINLOG(odinlog,normalDebug) << "has reorder vector" << STD_endl;
      if(vechandler.get_handled()->contains(reordvec->vechandler.get_handled())) nr_cache=reorderInner;
      if(reordvec->vechandler.get_handled()->contains(vechandler.get_handled())) nr_cache=vecInner;
    } else ODINLOG(odinlog,normalDebug) << "has NO reorder vector" << STD_endl;
  } else ODINLOG(odinlog,normalDebug) << "has NO vechandler" << STD_endl;

  programContext gpgcontext;
  gpgcontext.mode=brukerGpg;

  if(reordvec && nr_cache!=noRelation) {
    if(reordvec->vechandler.get_handled()->unroll_program(gpgcontext) || vechandler.get_handled()->unroll_program(gpgcontext)) nr_cache=unrolledLoop;
  }


  if(nr_cache==noRelation)   {ODINLOG(odinlog,normalDebug) << "noRelation" << STD_endl;}
  if(nr_cache==reorderInner) {ODINLOG(odinlog,normalDebug) << "reorderInner" << STD_endl;}
  if(nr_cache==vecInner)     {ODINLOG(odinlog,normalDebug) << "vecInner" << STD_endl;}
  if(nr_cache==unrolledLoop) {ODINLOG(odinlog,normalDebug) << "unrolledLoop" << STD_endl;}

  nr_cache_up2date=true;
  if(reordvec) reordvec->nr_cache_up2date=true;

  return nr_cache;
}

iarray SeqVector::get_index_matrix() const {
  unsigned int ncols=get_numof_iterations();
  unsigned int nrows=1;
  if(reordvec) nrows=reordvec->get_numof_iterations();

  iarray result(nrows,ncols);

  unsigned int icol,irow;

  if(reordvec) {
    for(irow=0; irow<nrows; irow++) {
       for(icol=0; icol<ncols; icol++) {
         result(irow,icol)=reordvec->get_reordered_index(icol,irow);
       }
    }
  } else {
    for(icol=0; icol<ncols; icol++) result(0,icol)=icol;
  }

  return result;
}

int SeqVector::get_current_reord_index() const {
  int result=0;
  if(reordvec) result=reordvec->get_current_index();
  return result;
}

STD_string SeqVector::get_reord_iterator(const STD_string& iterator) const {
  STD_string result(iterator);
  if(reordvec) result=reordvec->get_reordered_iterator(iterator);
  return result;
}


const SeqVector& SeqVector::set_vechandler(const SeqCounter *sc) const {
  Log<Seq> odinlog(this,"set_vechandler");
  ODINLOG(odinlog,normalDebug) << "counter=" << sc->get_label() << STD_endl;
  vechandler.set_handled(sc);
  return *this;
}


bool SeqVector::loopcounter_is_active() const {
  Log<Seq> odinlog(this,"loopcounter_is_active");
  bool result=false;
  const SeqCounter* sc=vechandler.get_handled();
  ODINLOG(odinlog,normalDebug) << "sc=" << (void*)sc << STD_endl;
  if(sc) {
     result=sc->counter_is_active();
     ODINLOG(odinlog,normalDebug) << "loop " << vechandler.get_handled()->get_label() << " says " << result << STD_endl;
  }
  return result;
}


int SeqVector::get_loopcounter() const {
  Log<Seq> odinlog(this,"get_loopcounter");
  unsigned int result=0;
  const SeqCounter* sc=vechandler.get_handled();
  if(sc) {
    result=sc->get_counter();
    ODINLOG(odinlog,normalDebug) << "result(" << sc->get_label() << ")=" << result << STD_endl;
  }
  if(result>=get_numof_iterations()) result=0;
  return result;
}


bool SeqVector::is_qualvector() const {
  if(indexvec.size()) return false; // assume simple index vector
  return true;
}


bool SeqVector::is_acq_vector() const {
  if(simhandler.get_handled()) return simhandler.get_handled()->is_acq_vector();
  return Handled<const SeqVector*>::is_handled();
}


///////////////////////////////////////////////////////////////////////////////
// template instantiations, collect them here to include tjutils/tjhandler_code.h only once

template class Handler<const SeqCounter*>;
template class Handled<const SeqCounter*>;

///////////////////////////////////////////////////////////////////////////////

SeqReorderVector::SeqReorderVector(const SeqVector* user, const SeqReorderVector* copy_templ)
  : reord_scheme(noReorder), n_reord_segments(1), encoding_scheme(linearEncoding), reorder_user(user) {
  set_label(STD_string(user->get_label())+"_reordvec");
  if(copy_templ) {
    reord_scheme=copy_templ->reord_scheme;
    n_reord_segments=copy_templ->n_reord_segments;
    encoding_scheme=copy_templ->encoding_scheme;
  }
}


unsigned int SeqReorderVector::get_reordered_size(unsigned int vecsize) const {
  Log<Seq> odinlog(this,"get_reordered_size");
  unsigned int result=vecsize;
  if( reord_scheme==blockedSegmented || reord_scheme==interleavedSegmented) {
    result=vecsize/n_reord_segments;
    ODINLOG(odinlog,normalDebug) << "vecsize/n_reord_segments/result=" << vecsize << "/" << n_reord_segments << "/" << result << STD_endl;
  }
  return result;
}


svector SeqReorderVector::get_vector_commands(const STD_string& iterator) const {
  reord_iterator_cache=iterator; // cache for later use by get_reordered_iterator()
  return reorder_user->get_reord_vector_commands(iterator);
}


int SeqReorderVector::get_reordered_index(int counter, int reord_iteration) const {
  Log<Seq> odinlog(this,"get_reordered_index");

  int nvals_per_segment =reorder_user->get_numof_iterations();

  ODINLOG(odinlog,normalDebug) << "counter/reord_iteration/nvals_per_segment/n_reord_segments=" <<  counter << "/" << reord_iteration << "/" << nvals_per_segment << "/" << n_reord_segments << STD_endl;

  int peindex=counter;

  if(reord_scheme==rotateReorder) {
    ODINLOG(odinlog,normalDebug) << "using rotateReorder" << STD_endl;
    peindex=counter+reord_iteration;
    if(peindex>=nvals_per_segment) peindex-=nvals_per_segment;
  }

  if( reord_scheme==blockedSegmented ) {
    ODINLOG(odinlog,normalDebug) << "using blockedSegmented" << STD_endl;
    peindex=reord_iteration*nvals_per_segment+counter;
  }

  if( reord_scheme==interleavedSegmented ) {
    ODINLOG(odinlog,normalDebug) << "using interleavedSegmented" << STD_endl;
     peindex=counter*n_reord_segments+reord_iteration;
  }


  // phase-encoding scheme

  int result=peindex; //default

  if(encoding_scheme==reverseEncoding) {
    result=reorder_user->get_vectorsize()-1-peindex;
  }

  if(encoding_scheme==centerOutEncoding || encoding_scheme==centerInEncoding) {
    int n=reorder_user->get_vectorsize();
    int cent=n/2;
    int index=peindex;
    if(encoding_scheme==centerInEncoding) index=n-1-peindex;
    int sign=int(pow(-1,index));
    result=cent+sign*int((index+1)/2);
    ODINLOG(odinlog,normalDebug) << "sign/cent=" <<  sign << "/" << cent<< STD_endl;
  }

  if(encoding_scheme==maxDistEncoding) {
    result = peindex%2 * (reorder_user->get_vectorsize()+1)/2 + peindex/2;
  }


  ODINLOG(odinlog,normalDebug) << "peindex/result" << peindex << "/" << result << STD_endl;
  return result;
}

STD_string SeqReorderVector::get_reordered_iterator(const STD_string& iterator) const {
  STD_string result(iterator);

  STD_string nvals_per_segment =itos(reorder_user->get_numof_iterations());

  if(reord_scheme==rotateReorder) {
    result= "("+iterator+"+"+reord_iterator_cache+")%"+nvals_per_segment;
  }

  if( reord_scheme==blockedSegmented ) {
    result= reord_iterator_cache+"*"+nvals_per_segment+"+"+iterator;
  }

  if( reord_scheme==interleavedSegmented ) {
    result= iterator+"*"+itos(n_reord_segments)+"+"+reord_iterator_cache;
  }


  // phase-encoding scheme

  STD_string vectorsize =itos(reorder_user->get_vectorsize());

  if(encoding_scheme==reverseEncoding) {
    result = vectorsize +"-1-(" + result + ")";
  }

  if(encoding_scheme==centerOutEncoding || encoding_scheme==centerInEncoding) {
    STD_string sign="(int)(pow(-1,"+result+"))";
    STD_string cent=vectorsize+"/2";
    STD_string indexstr=result;
    if(encoding_scheme==centerInEncoding) indexstr="("+vectorsize+"-1-"+result+")";
    result=cent+"+"+sign+"*(int)(("+indexstr+"+1)/2)";
  }

  if(encoding_scheme==maxDistEncoding) {
    result = result + "%2 * (" +vectorsize + "+1)/2 + "+result+"/2";
  }

  return result;
}


unsigned int SeqReorderVector::get_vectorsize() const {
  unsigned int result=1;

  if(reord_scheme==rotateReorder) result=reorder_user->get_vectorsize();

  if( blockedSegmented <= reord_scheme && reord_scheme <= interleavedSegmented ) {
    result=n_reord_segments;
  }

  return result;
}


// Template instantiations
template class ListItem<SeqVector>;
template class List<SeqVector, const SeqVector*, const SeqVector&>;

template class Handler<const SeqVector*>;
template class Handled<const SeqVector*>;
