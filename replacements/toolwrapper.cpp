#include<string>
#include<fstream>
#include <cstdlib>

// Win-style process handling
#include <process.h>
#include <errno.h>
#include <windows.h>

using namespace std;

int main(int argc, char* argv[]) {

  string tool;
  string toolpath;

#include "toolwrapper_config.h"

  // add path for ppc-compiler binaries
  string newpath="PATH="+toolpath;
  const char* oldpath_ptr=getenv("PATH");
  if(oldpath_ptr) {
    string oldpath=oldpath_ptr;

    // remove blanks which make ccppc fail
    while(oldpath.find("; ")!=string::npos) oldpath.replace(oldpath.find("; "),2,";");
    while(oldpath.find(" ;")!=string::npos) oldpath.replace(oldpath.find(" ;"),2,";");

    newpath+=";"+string(oldpath);
  }
  _putenv(newpath.c_str());

  _putenv("WIND_BASE=;"); // for VB20, value seems to be irrelevant

  argv[0]=(char*)tool.c_str();
  return _spawnv( _P_WAIT, tool.c_str(), argv );
}
