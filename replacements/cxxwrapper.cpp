/**
  * Since the compiler for MPCU does not create executables directly,
  * using autoconf is not straighforward. Instead we have to wrap the
  * compiler for the MPCU into this program to create dummy files
  * instead of actual executables.
  */


#include<string>
#include<fstream>
#include <cstdlib>

// Win-style process handling
#include <process.h>
#include <errno.h>
#include <windows.h>

using namespace std;

int main(int argc, char* argv[]) {

  string cxx;
  string cxxpath;

#include "cxxwrapper_config.h"

  // check if we only compile, i.e. have the -c flag
  bool only_compile=false;
  for(int i=1; i<argc; i++) {
    if(string(argv[i])=="-c") {
      only_compile=true;
    }
  }

  if(!only_compile) { // Asked to create a binary, which would normally fail

    string autoconf_object="conftest"; // the default for autoconf

    // Let all checks for library functions (AC_CHECK_FUNCS) fail
    for(int i=1; i<argc; i++) {
      if(string(argv[i])=="-o") {
        if((i+1)<argc && argv[i+1]==autoconf_object) return -1; //  checks have '-o conftest' (and no '-c')
      }
    }

    // Catch other checks, e.g. for output file name of compiler
    for(int i=1; i<argc; i++) {
      if(argv[i]==(autoconf_object+".cpp")) {

        // Create dummy 'binary'
        ofstream os(autoconf_object.c_str(),ios_base::out);
        os << "dummy" << endl;
        os.close();
        return 0;
      }
    }

    return -1; // fail otherwise
  }

  // add path for ppc-compiler binaries
  string newpath="PATH="+cxxpath;
  const char* oldpath_ptr=getenv("PATH");
  if(oldpath_ptr) {
    string oldpath=oldpath_ptr;

    // remove blanks which make ccppc fail
    while(oldpath.find("; ")!=string::npos) oldpath.replace(oldpath.find("; "),2,";");
    while(oldpath.find(" ;")!=string::npos) oldpath.replace(oldpath.find(" ;"),2,";");

    newpath+=";"+string(oldpath);
  }
  _putenv(newpath.c_str());

  _putenv("WIND_BASE=;"); // for VB20, value seems to be irrelevant


  argv[0]=(char*)cxx.c_str(); // Binary as first arg
  return _spawnv( _P_WAIT, cxx.c_str(), argv );




//  int result=0;
//  if(autoconf_compile) {

/*
    // Ad extra option '-c'
    char* argv_c[argc+2];
    for(int i=0; i<argc; i++) argv_c[i]=argv[i];
    argv_c[argc]="-c";
    argv_c[argc+1]=0;

    // Log command line and src file
    ofstream log("compile.log",ios_base::app);
    string srcfile;
    for(int i=0; i<(argc+1); i++) {
      log << argv_c[i] << " ";
      if(string(argv_c[i]).find(".cpp")!=string::npos) srcfile=argv_c[i];
    }
    log << endl << endl;

    if(srcfile!="") {
      ifstream srcstream(srcfile.c_str());
      string srcline;
      while(srcstream.good()) {
        getline(srcstream,srcline);
        log << srcline << endl;
      }
    }
    log << endl << endl << endl;
    log.close();


    // Compile only, upon succesful compile, conftest will be created
    result = _spawnv( _P_WAIT, cxx.c_str(), argv_c );
*/

    // Let all checks (AC_CHECK_FUNCS) fail
//    result=-1;

//    ofstream os(object.c_str(),ios_base::out);
//    os << "dummy" << endl;
//    os.close();
//  } else {
//    result = _spawnv( _P_WAIT, cxx.c_str(), argv );
//  }

//  return result;
}
