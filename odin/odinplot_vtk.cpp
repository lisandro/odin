#include "odinplot_vtk.h"
#include "odincomp.h"

#ifdef VTKSUPPORT

#include <vtkActor.h>
#include <vtkCamera.h>
#include <vtkMath.h>
#include <vtkPointData.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>
#include <vtkAxes.h>
#include <vtkTubeFilter.h>
#include <vtkPolyDataMapper.h>
#include <vtkArrowSource.h>

#endif


VtkMagnPlotter::VtkMagnPlotter() {


  renderer=0;
  renWin=0;
  iren=0;
  magnArrow=0;
  magnMapper=0;
  magnActor=0;
  magnGradActor=0;

  axes=0;
  axesTubes=0;
  axesMapper=0;
  axesActor=0;

}


#if VTK_MAJOR_VERSION > 5
#define SET_INPUT_FUNC_NAME SetInputData
#else
#define SET_INPUT_FUNC_NAME SetInput
#endif


void VtkMagnPlotter::start() {
  Log<OdinComp> odinlog("VtkMagnPlotter","start");
#ifdef VTKSUPPORT 

  magnArrow=vtkArrowSource::New();

  magnMapper = vtkPolyDataMapper::New();
  magnMapper->SET_INPUT_FUNC_NAME(magnArrow->GetOutput());

  magnActor = vtkActor::New();
  magnActor->SetMapper(magnMapper);
  magnActor->GetProperty()->SetColor(1,1,1);

  magnGradActor = vtkActor::New();
  magnGradActor->SetMapper(magnMapper);
  magnGradActor->GetProperty()->SetColor(1,1,50);

  // Axes
  axes = vtkAxes::New();
//  axes->SetOrigin(bounds[0], bounds[2], bounds[4]);
//  axes->SetScaleFactor(0.01);

  axesTubes = vtkTubeFilter::New();

  axesTubes->SET_INPUT_FUNC_NAME(axes->GetOutput());
  axesTubes->SetRadius(axes->GetScaleFactor()/100.0);
  axesTubes->SetNumberOfSides(6);

  axesMapper = vtkPolyDataMapper::New();
  axesMapper->SET_INPUT_FUNC_NAME(axesTubes->GetOutput());

  vtkActor *axesActor = vtkActor::New();
  axesActor->SetMapper(axesMapper);


  // Create the usual rendering stuff
  renderer = vtkRenderer::New();
  renWin = vtkRenderWindow::New();
  renWin->AddRenderer(renderer);


  renderer->AddActor(magnActor);
  magnGradActor_added=false;
  renderer->AddActor(axesActor);
//  renderer->SetBackground(1,1,1);
  renderer->GetActiveCamera()->Roll(-120.0);
//  renderer->GetActiveCamera()->Azimuth(120.0);
  renderer->GetActiveCamera()->Elevation(-70.0);
  renderer->GetActiveCamera()->Zoom(0.25);
  renWin->SetSize(500,500);


#else
  ODINLOG(odinlog,errorLog) << "vtk support is disabled, recompile with --enable-vtksupport" << STD_endl;
#endif
}

void VtkMagnPlotter::plot_vector(float M[3], float* dM) {
#ifdef VTKSUPPORT 

  double Mnorm=norm3(M[0],M[1],M[2]);
  double heightAngle=180.0/PII*acos(secureDivision(M[2],Mnorm));
  double azimuthAngle=180.0/PII*atan2(M[1],M[0]);
  magnActor->SetScale(Mnorm);
  magnActor->SetOrientation(0.0,heightAngle-90.0,azimuthAngle);

  if(dM) {

    // add magnGradActor if we have MagnGrads
    if(!magnGradActor_added) {
      renderer->AddActor(magnGradActor);
      magnGradActor_added=true;
    }

//    magnGradActor->SetOrigin(M[0],M[1],M[2]);
    Mnorm=norm3(dM[0],dM[1],dM[2]);
    heightAngle=180.0/PII*acos(secureDivision(dM[2],Mnorm));
    azimuthAngle=180.0/PII*atan2(dM[1],dM[0]);
    magnGradActor->SetScale(0.02*Mnorm);
    magnGradActor->SetOrientation(0.0,heightAngle-90.0,azimuthAngle);
  }


  if(renWin) renWin->Render();


#endif 
}

void VtkMagnPlotter::interact() {
#ifdef VTKSUPPORT 
  if(!iren) iren = vtkRenderWindowInteractor::New();
  iren->SetRenderWindow(renWin);


  // interact with data
  if(iren) iren->Start();
#endif 
}

VtkMagnPlotter::~VtkMagnPlotter() {
#ifdef VTKSUPPORT 

  if(axes) axes->Delete();
  if(axesTubes) axesTubes->Delete();
  if(axesMapper) axesMapper->Delete();
  if(axesActor) axesActor->Delete();

  if(magnArrow) magnArrow->Delete();
  if(magnMapper) magnMapper->Delete();
  if(magnActor) magnActor->Delete();

  if(renderer) renderer->Delete();
  if(iren) iren->Delete();
  if(renWin) renWin->Delete();

#endif 
}


