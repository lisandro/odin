/***************************************************************************
                          odindialog_idea.h  -  description
                             -------------------
    begin                : Fri Mar 9 21:30:11 CEST 2007
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef ODINDIALOG_IDEA_H
#define ODINDIALOG_IDEA_H

// Qt related headers first to avoid ambiguity with 'debug'
#include <qobject.h>
#include <odinqt/ldrwidget.h>

#include <odinpara/ldrblock.h>
#include <odinpara/ldrtypes.h>
#include <odinpara/ldrnumbers.h>


#include "odinconf.h"

struct IdeaOpts : LDRblock {

  IdeaOpts();

  bool set_defaults(const OdinConf& conf, STD_string* errmsg, QWidget *parent);

  STD_string get_vxworks_postfix() const;
  STD_string get_vxworks_ext() const;
  STD_string get_vxworks_Makefile() const;
  STD_string get_vxworks_cxx() const {return odindir+"/bin/cxxwrapper.exe";}

  STD_string get_ideadir_slash() const {return replaceStr(ideadir,"\\","/");}
  STD_string get_odindir_slash() const {return replaceStr(odindir,"\\","/");}


  LDRfileName arch;

  LDRfileName ideadir;
  LDRfileName odindir;

  LDRfileName msvcdir;

  LDRfileName seqdir;
  LDRstring odin2idea_label;

  LDRfileName icedir;

  LDRenum vxworks_cpu;
  LDRint vxworks_heap_size;
  LDRbool debug_vxworks_host;

  LDRint make_jobs;
  LDRbool make_clean;



  // read-only settings
  LDRfileName vxworks_dir;
  LDRstring vxworks_flags;
  LDRstring vxworks_opts;
  LDRfileName vxworks_path;
  LDRfileName win_cxx;
  LDRstring hostd_flags;
  LDRstring host_flags;

};


///////////////////////////////////////////////////////


class IdeaDialog :  public QObject, public GuiDialog {
 Q_OBJECT

 public:
  IdeaDialog(QWidget *parent, IdeaOpts& opts, const OdinConf& conf);
  ~IdeaDialog();

 signals:
  void changed();

 private slots:
  void cancel();
  void defaults();
  void compile();
  void update();

 private:
  void do_compile();
  void error_msg(const STD_string& msg);
  int find_in_alternatives(const STD_string& findstr, const STD_list<STD_string>& alternatives);
  bool execute_make(const STD_string& make, const STD_string& target);
  STD_string hosttarget(bool debug, const STD_string& build_includes, const STD_string& global_conf, const STD_string& mprefix, const STD_string& make_install) const;
  static STD_string errcodes_src(const STD_string& msgfile);



  IdeaOpts& opts_cache;
  const OdinConf& conf_cache;
  GuiGridLayout* grid;
  GuiButton* pb_cancel;
  GuiButton* pb_defaults;
  GuiButton* pb_compile;
  LDRwidget* optswidget;

};

///////////////////////////////////////////////////////

class IdeaMethodDialog : public QObject, public GuiDialog {
 Q_OBJECT

 public:
  IdeaMethodDialog(const STD_string& methdir, sarray& selectedMethods, const STD_string& install_prefix, const IdeaOpts& ideaopts, QWidget *parent);
  ~IdeaMethodDialog();


 private slots:
  void make();
  void emitDone();

 private:

  GuiButton* pb_make;
  GuiButton* pb_cancel;
  GuiGridLayout* grid;

  GuiListView* method_list;
  STD_list<GuiListItem*> method_checks;

  sarray& selMeth;
  STD_string methrootdir;
  STD_string install_dir;

  const IdeaOpts& ideaopts_cache;
};



#endif
