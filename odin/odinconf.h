/***************************************************************************
                          odinconf.h  -  description
                             -------------------
    begin                : Fri Nov 11 21:30:11 CEST 2003
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef OdinConf_H
#define OdinConf_H

#include <tjutils/tjprocess.h>

#include <odinpara/ldrblock.h>
#include <odinpara/ldrtypes.h>
#include <odinpara/ldrarrays.h>

///////////////////////////////////////////////////////////////////

#define DEFAULT_METH_ROOT "odin-methods"

///////////////////////////////////////////////////////////////////

class QWidget; // forward declaration

///////////////////////////////////////////////////////////////////

struct OdinConf : public LDRblock {

  OdinConf(const STD_string& label="unnamedOdinConf", bool ignore_environment = false);

  LDRfileName  sourcecode;
  LDRfileName  editor;
  LDRfileName  browser;
  LDRfileName  methroot;
  LDRfileName  smpfile;
  LDRfileName  protfile;
  LDRfileName  compiler;  // Set once on startup, do not include in file
  LDRfileName  linker;    // Set once on startup, do not include in file
  LDRstring    compiler_flags;
  LDRstring    extra_includes;
  LDRstring    extra_libs;
  LDRstringArr selectedMethods;
  LDRbool      attachDebugger;

  bool init(QWidget* parent);

  svector get_method_compile_chain() const;

  // helper functions
  static STD_string get_binprefix();
  static STD_string get_homedir();
  static STD_string get_seqexamplesdir();
  static STD_string get_samplesdir();
  static STD_string get_coilsdir();
  static STD_string get_confdir();
  static STD_string get_tmpdir();
  static STD_string get_manual_location();

  bool display_html(const STD_string& location, STD_list<Process>& subprocs);
  bool open_ascfile(const STD_string& filename, STD_list<Process>& subprocs);

  static STD_string get_installdir();

#ifndef USING_WIN32
  static bool start_proc(const STD_string& program, const STD_string& filename, STD_list<Process>& subprocs);
#endif

 private:
  static STD_string get_registryvalue(const STD_string& keyname, const STD_string& valuename);

  static STD_string get_datadir();

  static bool ignore_env; // for clean start in IDEA
};

#endif
