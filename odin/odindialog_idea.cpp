#include "odindialog_idea.h"

#include "odincomp.h"

#include "odindialog_process.h"

#include <odinqt/ldrblockwidget.h>

#include <odinseq/seqmakefile.h>

////////////////////////////////////////////////////////////////////////////////////////
// Helper functions

bool select_alternative(LDRfileName& result, const STD_list<STD_string>& alternatives, QWidget *parent=0) {
  Log<OdinComp> odinlog("","select_alternative");

  if(parent && alternatives.size()>1) { // Ask with dialog if parent is provided

    LDRenum alts;
    alts.set_label("Alternatives");
    for(STD_list<STD_string>::const_iterator it=alternatives.begin(); it!=alternatives.end(); ++it) alts.add_item(*it);

    LDRblock block("Select Alternative");
    block.append(alts);
    new LDRwidgetDialog(block,1,parent,true);

    result=alts.operator STD_string();

  } else {

    for(STD_list<STD_string>::const_iterator it=alternatives.begin(); it!=alternatives.end(); ++it) {
      result=*it;
      bool exists=result.exists();
      ODINLOG(odinlog,normalDebug) << "exists(" << result << ")=" << exists << STD_endl;
      if(exists) return true;
      result="";
    }
  }

  return false;
}

void select_idea_include_dirs(STD_string& result, const STD_list<STD_string>& possible_dirs, const IdeaOpts& opts) {
  LDRfileName testdir;
  testdir.set_dir(true);
  STD_string ideadir_slash=opts.get_ideadir_slash();
  for(STD_list<STD_string>::const_iterator it=possible_dirs.begin(); it!=possible_dirs.end(); ++it) {
    testdir=ideadir_slash+SEPARATOR_STR+(*it);
    if(testdir.exists()) {
      result+=" -I"+ideadir_slash+"/"+(*it); // use forward slashes for includes
    }
  }
}

svector match_in_dir(const STD_string& dir, const STD_string& match) {
  svector result;
  svector dirs=browse_dir(dir, true,true);
  for(unsigned int i=0; i<dirs.size(); i++) {
    if(tolowerstr(dirs[i]).find(match)==0) {
      result.push_back(LDRfileName(dir+"/"+dirs[i])); // better formatting
    }
  }
  return result;
}



STD_string mgwpath(const STD_string& path) {
  return "/"+replaceStr(replaceStr(path,"\\","/"),":","");
}


////////////////////////////////////////////////////////////////////////////////////////

IdeaOpts::IdeaOpts() : LDRblock("ODIN for IDEA settings") {

  arch.set_description("ODIN source code archive to be compiled");
  append_member(arch,"ODIN source code");

  ideadir.set_dir(true);
  ideadir.set_description("The location of the IDEA installation");
  append_member(ideadir,"IDEA directory");

  odindir.set_dir(true);
  odindir.set_description("The location where ODIN will be installed.");
  append_member(odindir,"ODIN target directory");

  msvcdir.set_dir(true);
  seqdir.set_description("The installation directory of the Visual C++ compiler, linker and libraries");
  append_member(msvcdir,"Visual C++ installation directory");

  seqdir.set_dir(true);
  seqdir.set_description("The location of your IDEA sequences.");
  append_member(seqdir,"IDEA sequence directory");

  odin2idea_label.set_description("Name for the IDEA sequence that holds ODIN methods");
  append_member(odin2idea_label,"IDEA sequence label");

  icedir.set_dir(true);
  icedir.set_description("The location of the ICE reconstructions.");
  append_member(icedir,"ICE directory");

  vxworks_cpu.add_item("PPC750");
  vxworks_cpu.add_item("PENTIUM");
  vxworks_cpu.set_description("CPU type of MPCU/VxWorks");
  append_member(vxworks_cpu,"VxWorks CPU type");

  vxworks_heap_size.set_description("Heap (dynamic memory) size for ODIN in MB on VxWorks").set_unit("MB");
  vxworks_heap_size.set_minmaxval(2,10);
  append_member(vxworks_heap_size,"VxWorks Heap Size");

  debug_vxworks_host.set_description("Include debugging code in VxWorks and Host target");
  append_member(debug_vxworks_host,"VxWorks/Host Debug");

  make_jobs.set_description("Number of parallel make jobs");
  make_jobs.set_minmaxval(1,8);
  append_member(make_jobs,"Make jobs");

  make_clean.set_description("Clean up build directory before and after build process");
  append_member(make_clean,"Make clean");

#ifdef ODIN_DEBUG
  parameterMode parmode=noedit;
#else
  parameterMode parmode=hidden;
#endif

  vxworks_dir.set_dir(true);
  vxworks_dir.set_parmode(parmode).set_description("VxWorks compiler/linker directory");
  append_member(vxworks_dir,"VxWorks directory");

  vxworks_flags.set_parmode(parmode).set_description("Compiler flags for VxWorks");
  append_member(vxworks_flags,"VxWorks flags");

  vxworks_opts.set_parmode(parmode).set_description("Extra compiler options for VxWorks, not used by preprocessor");
  append_member(vxworks_opts,"VxWorks options");

  vxworks_path.set_dir(true);
  vxworks_path.set_parmode(parmode).set_description("Extra executable path for VxWorks");
  append_member(vxworks_path,"Extra VxWorks path");

  win_cxx.set_parmode(parmode).set_description("Visual C++ compiler");
  append_member(win_cxx,"Windows compiler");

  hostd_flags.set_parmode(parmode).set_description("Compiler options for host(debug)");
  append_member(hostd_flags,"Host(debug) options");

  host_flags.set_parmode(parmode).set_description("Compiler options for host(release)");
  append_member(host_flags,"Host(release) options");

}



bool IdeaOpts::set_defaults(const OdinConf& conf, STD_string* errmsg, QWidget *parent) {
  Log<OdinComp> odinlog("IdeaOpts","set_defaults");

  STD_list<STD_string> possible_arch;
  possible_arch.push_back(conf.get_installdir()+SEPARATOR_STR+"src"+SEPARATOR_STR+PACKAGE+"-"+VERSION+".tar.gz");
  if(!select_alternative(arch,possible_arch)) {
    if(errmsg) (*errmsg)="Cannot find Odin source archive";
    return false;
  }

  STD_list<STD_string> possible_ideadirs;

  LDRfileName midearoot("C:/MIDEA");
  midearoot.set_dir(true);
  if(midearoot.exists()) { // VB*
    svector mideadirs=match_in_dir("C:/MIDEA", "n4_");
    for(unsigned int i=0; i<mideadirs.size(); i++) possible_ideadirs.push_back(mideadirs[i]);
  } else { // VA*
     possible_ideadirs.push_back("V:");
  }

  if(!select_alternative(ideadir,possible_ideadirs,parent)){ // Ask with dialog in case of multiple IDEAs
    if(errmsg) (*errmsg)="Cannot find IDEA directory";
    return false;
  }

  odindir=conf.get_installdir()+SEPARATOR_STR+"Odin4Idea";

  LDRfileName msdevdir(getenv_nonnull("MsDevDir"));
  if(msdevdir=="") {
    if(errmsg) (*errmsg)="Cannot find Visual Studio environment variable MsDevDir";
    return false;
  }
  msvcdir=LDRfileName(msdevdir.get_dirname()).get_dirname(); // two levels up
//  msvcdir="C:/MSVStudio6";
  ODINLOG(odinlog,normalDebug) << "msdevdir - msvcdir=" << msdevdir << " - " << msvcdir << STD_endl;

  STD_list<STD_string> possible_seqdirs;
  possible_seqdirs.push_back(ideadir+SEPARATOR_STR+"n4/pkg/MrServers/MrImaging/seq");
  possible_seqdirs.push_back(ideadir+SEPARATOR_STR+"n4/comp/Application/seq");
  if(!select_alternative(seqdir,possible_seqdirs)) {
    if(errmsg) (*errmsg)="Cannot find sequence directory";
    return false;
  }


  odin2idea_label="odin";

  STD_list<STD_string> possible_icedirs;
  possible_icedirs.push_back(ideadir+SEPARATOR_STR+"n4/pkg/MrServers/MrIcePrograms"); // pre VB15
  possible_icedirs.push_back(ideadir+SEPARATOR_STR+"n4/pkg/MrServers/MrVista/Ice/IceIdeaFunctors"); // VB15
  possible_icedirs.push_back(seqdir); // fallback
  if(!select_alternative(icedir,possible_icedirs)) {
    if(errmsg) (*errmsg)="Cannot find ICE directory";
    return false;
  }

  vxworks_heap_size=3;

  debug_vxworks_host=false;


  STD_list<STD_string> vxworks_dir_alternatives;
  vxworks_dir_alternatives.push_back(ideadir+"/n4_fsp/tornado/i86/host/x86-win32");
  vxworks_dir_alternatives.push_back(ideadir+"/n4_fsp/tornado/ppc/host/x86-win32");
  vxworks_dir_alternatives.push_back(ideadir+"/n4_fsp/WindRiver/gnu/4.3.3-vxworks-6.9/x86-win32");
  if(!select_alternative(vxworks_dir, vxworks_dir_alternatives)) {
    if(errmsg) (*errmsg)="Cannot find vxworks directory";
    return false;
  }

  STD_list<STD_string> vxworks_path_alternatives;
  vxworks_path_alternatives.push_back(vxworks_dir+"/lib/gcc-lib/powerpc-wrs-vxworks/cygnus-2.7.2-960126");
  vxworks_path_alternatives.push_back(vxworks_dir+"/bin"); // for VB12
  if(!select_alternative(vxworks_path, vxworks_path_alternatives)) {
    if(errmsg) (*errmsg)="Cannot find vxworks executable path";
    return false;
  }


  LDRfileName ccpentium(vxworks_path+SEPARATOR_STR+"ccpentium.exe");
  if(ccpentium.exists()) vxworks_cpu.set_actual("PENTIUM");
  else vxworks_cpu.set_actual("PPC750");

  make_jobs=numof_cores();
  make_clean=true;

  return true;
}

/////////////////////////////////////////////////////////////////////////////

STD_string IdeaOpts::get_vxworks_postfix() const {
  STD_string result;

  if(tolowerstr(vxworks_cpu).find("pentium")!=STD_string::npos) result="pentium";
  else result="ppc";
  return result;
}

STD_string IdeaOpts::get_vxworks_ext() const {
  STD_string result;

  if(tolowerstr(vxworks_cpu).find("pentium")!=STD_string::npos) result="i86";
  else result="ppc";
  return result;
}

STD_string IdeaOpts::get_vxworks_Makefile() const {
  STD_string result;

  if(tolowerstr(vxworks_cpu).find("pentium")!=STD_string::npos) result="MakI86.txt";
  else result="MakPpc3.txt";
  return result;
}



/////////////////////////////////////////////////////////////////////////////


IdeaDialog::IdeaDialog(QWidget *parent, IdeaOpts& opts, const OdinConf& conf)
 : GuiDialog(parent,"Compile ODIN for IDEA",true), opts_cache(opts), conf_cache(conf) {
  Log<OdinComp> odinlog("IdeaDialog","IdeaDialog");

  grid=new GuiGridLayout(GuiDialog::get_widget(), 2, 3);

  optswidget=new LDRwidget(opts,2,GuiDialog::get_widget());
  connect(optswidget,SIGNAL(valueChanged()),this,SLOT(update()));
  grid->add_widget( optswidget, 0, 0, GuiGridLayout::Default, 1, 3 );

  pb_cancel = new GuiButton( GuiDialog::get_widget(), this, SLOT(cancel()), "Cancel" );
  grid->add_widget( pb_cancel->get_widget(), 1, 0, GuiGridLayout::Center );

  pb_defaults = new GuiButton( GuiDialog::get_widget(), this, SLOT(defaults()), "Defaults" );
  grid->add_widget( pb_defaults->get_widget(), 1, 1, GuiGridLayout::Center );

  pb_compile = new GuiButton( GuiDialog::get_widget(), this, SLOT(compile()), "Compile" );
  grid->add_widget( pb_compile->get_widget(), 1, 2, GuiGridLayout::Center );

  update(); // initialize on startup
  GuiDialog::show();
}

void IdeaDialog::cancel() {
  GuiDialog::cancel();
}

void IdeaDialog::defaults() {
  STD_string errmsg;
  if(!opts_cache.set_defaults(conf_cache, &errmsg, GuiDialog::get_widget())) {
    error_msg(errmsg);
  }
  update(); // update other parameters
  optswidget->updateWidget();
  emit changed();
}

void IdeaDialog::compile() {
  emit changed();
  do_compile();
  GuiDialog::done();
}

void IdeaDialog::error_msg(const STD_string& msg) {
  message_question(msg.c_str(), "Error Compiling ODIN for IDEA", GuiDialog::get_widget(), false, true);
}

int IdeaDialog::find_in_alternatives(const STD_string& findstr, const STD_list<STD_string>& alternatives) {
  LDRfileName fname;
  if(!select_alternative(fname, alternatives)) {
    error_msg("Cannot find file containing "+findstr);
    return -1;
  } else {
    STD_string str;
    if(load (str, fname)<0) {
      error_msg("Cannot load file "+fname);
      return -1;
    } else {
      if(str.find(findstr)!=STD_string::npos) {
        return 1;
      }
    }
  }
  return 0;
}



STD_string IdeaDialog::hosttarget(bool debug, const STD_string& build_includes, const STD_string& global_conf, const STD_string& mprefix, const STD_string& make_install) const {

  STD_string flags;
  STD_string target;
  if(debug) {flags=opts_cache.hostd_flags; target="hostd";}
  else      {flags=opts_cache.host_flags;  target="host";}

  STD_string win_cxx_mgwpath=mgwpath(opts_cache.win_cxx);

  STD_string confstr;
  confstr+=" LD="+mgwpath(opts_cache.msvcdir)+"/VC98/Bin/link.exe";
  confstr+=" CXX="+win_cxx_mgwpath;
  confstr+=" CXXFLAGS=\""+flags+" "+build_includes+"\"";
  confstr+=" CC="+win_cxx_mgwpath;
  confstr+=" CFLAGS=\""+flags+"\"";
  confstr+=" ../../${ARCHNAME}/configure "+global_conf;
  confstr+=" --prefix="+opts_cache.get_odindir_slash()+"/"+target;
  if(debug) confstr+=" --enable-debug";
  else      confstr+=" ${DEBUGOPT}";
  confstr+=" --with-extra-odinseq-include-path=-I"+opts_cache.get_ideadir_slash()+"/SWF_extern/SDK/include"; // Since VB15 code compiles only with the SDK provided by IDEA, and ODIN compiles only with VC++ SDK, we need a separate include path for odinseq where IDEA code is included

  STD_string result;
  result+="configure-"+target+":\n";
  result+=mprefix+"cd "+target+"/build && "+confstr+"\n";
  result+=mprefix+"cat "+target+"/build/libtool | sed s/\\\\.libs/tmplibfiles/g > "+target+"/build/libtool.tmp\n"; // dot in .libs (dir for temporary lib files) confuses lib.exe in MinGW
  result+=mprefix+"mv "+target+"/build/libtool.tmp "+target+"/build/libtool\n\n";

  result+="build-"+target+":\n";
  result+=mprefix+"cd "+target+"/build && "+make_install+"\n";
  // add lib prefix, if missing
  // and add lib version with postfix 'd' (quick hack for VB20)
  result+=mprefix+"for odinlibname in tjutils odinpara odinseq ; do if test -f "+target+"/lib/$${odinlibname}.lib ; then mv "+target+"/lib/$${odinlibname}.lib "+target+"/lib/lib$${odinlibname}.lib ; fi; cp "+target+"/lib/lib$${odinlibname}.lib "+target+"/lib/lib$${odinlibname}d.lib; done\n\n";

  result+="build-"+target+"-clean: build-"+target+"\n";
  result+=mprefix+"rm -rf "+target+"/build\n\n";

  return result;
}




STD_string IdeaDialog::errcodes_src(const STD_string& msgfile) {
  STD_string str=dos2unix(msgfile);

  STD_string blockbegin("MessageId:");
  STD_string blockend("#define");

  STD_string result;

  STD_string txt;
  do {

    txt=extract(str, blockbegin,  blockend);

    if(txt!="") {
      STD_string errcode=shrink(extract(txt, "",  "*/"));

      STD_string msg=extract(txt, "MessageText:",  "");

      msg=replaceStr(msg, "/*", "");
      msg=replaceStr(msg, "*/", "");
      msg=justificate(msg,0,false,1000);
      msg=replaceStr(msg, "\n", "");

      result+="if(code=="+errcode+") return \""+msg+"\";\n";

      str=rmblock(str, blockbegin, blockend, true, true, false);
    }

  } while(txt!="");

  return result;
}



void IdeaDialog::do_compile() {
  Log<OdinComp> odinlog("IdeaDialog","do_compile");

  // for testing existence of files/dirs
  LDRfileName testfile;
  LDRfileName testdir; testdir.set_dir(true);


  LDRfileName msys_bin(conf_cache.get_installdir()+SEPARATOR_STR+"msys"+SEPARATOR_STR+"bin");
  LDRfileName make(msys_bin+"/make.exe");
  if(!make.exists()) {
    error_msg("Cannot find make program "+make);
    return;
  }

  if(createdir(opts_cache.odindir.c_str())) {
    error_msg("Cannot create target directory "+opts_cache.odindir);
    return;
  }

  if(copyfile(opts_cache.arch.c_str(), (opts_cache.odindir+SEPARATOR_STR+opts_cache.arch.get_basename()).c_str())) {
    error_msg("Cannot copy "+opts_cache.arch+" to target directory "+opts_cache.odindir);
    return;
  }





  // Parse IDEA error codes
  LDRfileName msgfile;
  STD_list<STD_string> msgfile_alternatives;
  msgfile_alternatives.push_back(opts_cache.ideadir+"/n4/x86/delivery/include/Measurement/Sequence/libRT/libRTmsg.h"); // pre VA25
  msgfile_alternatives.push_back(opts_cache.ideadir+"/n4/x86/delivery/include/MrServers/MrMeasSrv/SeqIF/libRT/libRTmsg.h"); // VA25 and later
  if(!select_alternative(msgfile, msgfile_alternatives)) {
    error_msg("Cannot find file for IDEA error codes");
    return;
  }
  STD_string msgfilestr;
  if(load(msgfilestr,msgfile)<0) {
    error_msg("Cannot load file for IDEA error codes");
    return;
  }
  STD_string msgsrc=errcodes_src(msgfilestr);
  STD_string msgsrc_fname="idea_errcodes.cpp";
  if(write(msgsrc,opts_cache.odindir+SEPARATOR_STR+msgsrc_fname)<0) {
    error_msg("Cannot write file for IDEA error codes");
    return;
  }



  STD_string makestr="\""+mgwpath(make)+"\"";
  STD_string make_install=makestr+" install";
  if(opts_cache.make_jobs>1) {
    make_install="MAKEFLAGS=\"-j "+itos(opts_cache.make_jobs)+"\" "+makestr+" -e install";
  }

  STD_string makefile;

  LDRfileName shell=msys_bin+SEPARATOR_STR+"sh.exe";
  if(!shell.exists()) {
    error_msg("Cannot find shell "+shell);
    return;
  }
  makefile+="SHELL = \""+mgwpath(shell)+"\"\n\n";

  makefile+="SETPATH = PATH=\""+mgwpath(msys_bin)+"\":$$PATH;\n\n";
  STD_string mprefix="\t${SETPATH} ";

  makefile+="SUBDIRS = vxworks host hostd\n\n";
  makefile+="ARCHNAME = "+replaceStr(opts_cache.arch.get_basename(),".tar.gz","")+"\n\n";

  makefile+="ODIN2IDEA_LABEL = "+opts_cache.odin2idea_label+"\n";
  makefile+="ODIN2IDEADIR_MGWPATH = "+mgwpath(opts_cache.seqdir)+"/${ODIN2IDEA_LABEL}\n";
  makefile+="ODIN2IDEADIR_SLASH = "+replaceStr(opts_cache.seqdir,"\\","/")+"/${ODIN2IDEA_LABEL}\n\n";

  makefile+="ICEODINDIR = \""+mgwpath(opts_cache.icedir)+"/IceOdin\"\n\n";

  makefile+="DEBUGOPT = ";
  if(!opts_cache.debug_vxworks_host) makefile+="# "; // just comment out if release is enabled
  makefile+="--enable-debug\n\n";

  STD_string global_conf="--enable-only-idea-plugin --enable-ideasupport --disable-cmdline --disable-threads --disable-unit-test --enable-static --disable-shared";

  STD_string odindir_slash=opts_cache.get_odindir_slash();  // use forward slashes for includes
  STD_string build_includes="-I"+odindir_slash+"/${ARCHNAME}";

  makefile+="all: build\n\n"; // default: recompile odin

  makefile+="unpack:\n";
  makefile+=mprefix+"tar -xvzf ${ARCHNAME}.tar.gz\n";
  makefile+=mprefix+"cp "+msgsrc_fname+" ${ARCHNAME}/platforms/IDEA_n4/odinseq_idea/\n";
  makefile+=mprefix+"mkdir -p /tmp\n";

  // hacking configure:
  // 1) default file extension for C++ source code since .cc somehow fails with autoconf 2.59 ...
  // 2) replace limits.h (which fails on VB15) by stdlib.h as header for GCC sanity check
  makefile+=mprefix+"cat ${ARCHNAME}/configure | sed s/ac_ext=cc/ac_ext=cpp/ | sed s/limits.h/stdlib.h/g > ${ARCHNAME}/configure.tmp\n";
  makefile+=mprefix+"mv ${ARCHNAME}/configure.tmp ${ARCHNAME}/configure\n";


  STD_string mpcu_postfix=opts_cache.get_vxworks_postfix();
  STD_string vxworks_cxx=opts_cache.vxworks_dir+"/bin/cc"+opts_cache.get_vxworks_postfix()+".exe";

  STD_string gcc=mgwpath(conf_cache.get_installdir()+"/bin/g++");

  makefile+=mprefix+"mkdir -p bin\n";

  STD_string vxworks_path_slash=replaceStr(opts_cache.vxworks_path,"\\","/");

  // create *wrapper.exe
  STD_map<STD_string,STD_string> tools;
  tools["ld"]=tools["ar"]=tools["ranlib"]=tools["cpp"]=tools["nm"]=""; // add entries, nm is just for debugging purpose
  for(STD_map<STD_string,STD_string>::iterator it=tools.begin(); it!=tools.end(); ++it) {
    STD_string tool=it->first;
    makefile+=mprefix+"echo \"tool=\\\""+replaceStr(opts_cache.vxworks_dir+"/bin/"+tool+mpcu_postfix+".exe","\\","/")+"\\\";\" > ${ARCHNAME}/replacements/toolwrapper_config.h\n";
    if(opts_cache.vxworks_path!="") makefile+=mprefix+"echo \"toolpath=\\\""+vxworks_path_slash+"\\\";\" >> ${ARCHNAME}/replacements/toolwrapper_config.h\n";
    makefile+=mprefix+gcc+" ${ARCHNAME}/replacements/toolwrapper.cpp -o bin/"+tool+"wrapper.exe\n";
    it->second=mgwpath(opts_cache.odindir)+"/bin/"+tool+"wrapper.exe";
  }


  // for VB20
  LDRfileName lmapi_dll_dir(opts_cache.ideadir+"/n4_fsp/WindRiver/lmapi-5.0/x86-win32/bin");
  STD_string lmapi_dll_path;
  if(lmapi_dll_dir.exists()) lmapi_dll_path=replaceStr(lmapi_dll_dir,"\\","/")+";"; // replace backslashes by slashes


  // create cxxwrapper.exe
  makefile+=mprefix+"echo \"cxx=\\\""+replaceStr(vxworks_cxx,"\\","/")+"\\\";\" > ${ARCHNAME}/replacements/cxxwrapper_config.h\n";
  if(opts_cache.vxworks_path!="") makefile+=mprefix+"echo \"cxxpath="+"\\\""+lmapi_dll_path+vxworks_path_slash+"\\\";\" >> ${ARCHNAME}/replacements/cxxwrapper_config.h\n";
  makefile+=mprefix+gcc+" ${ARCHNAME}/replacements/cxxwrapper.cpp -o bin/cxxwrapper.exe\n";
  tools["cxx"]=mgwpath(opts_cache.odindir)+"/bin/cxxwrapper.exe";

  // create build dirs
  makefile+=mprefix+"for dir in ${SUBDIRS}; do mkdir -p $$dir/build; done\n\n";

  STD_string vxworks_cxxflags=opts_cache.vxworks_flags+" "+opts_cache.vxworks_opts+" "+build_includes;
  STD_string vxworks_cppflags=opts_cache.vxworks_flags; // only defines


  // configure/build vxworks
  STD_string confstr;
  confstr+=" LD="+tools["ld"];
  confstr+=" AR="+tools["ar"];
  confstr+=" RANLIB="+tools["ranlib"];
  confstr+=" CXX="+tools["cxx"];
  confstr+=" CXXFLAGS=\""+vxworks_cxxflags+"\"";
  confstr+=" CXXCPP=\""+tools["cpp"]+" "+vxworks_cppflags+"\"";
  confstr+=" CC="+tools["cxx"];
  confstr+=" CFLAGS=\""+vxworks_cxxflags+"\"";
  confstr+=" CPP=\""+tools["cpp"]+" "+vxworks_cppflags+"\"";
  confstr+=" ../../${ARCHNAME}/configure --host="+mpcu_postfix+"-wrs-vxworks "+global_conf;
  confstr+=" --enable-custom-heap="+itos(opts_cache.vxworks_heap_size)+" --disable-filehandling --disable-processhandling";
  confstr+=" ${DEBUGOPT} --prefix="+odindir_slash+"/vxworks";

  makefile+="configure-vxworks:\n";
  makefile+=mprefix+"cd vxworks/build && "+confstr+"\n\n";
  makefile+="build-vxworks:\n";
  makefile+=mprefix+"cd vxworks/build && "+make_install+"\n\n";
  makefile+="build-vxworks-clean: build-vxworks\n";
  makefile+=mprefix+"rm -rf vxworks/build\n\n";


  // configure/build host(d)
  makefile+=hosttarget(true,  build_includes, global_conf, mprefix, make_install);
  makefile+=hosttarget(false, build_includes, global_conf, mprefix, make_install);


  // targets for all
  makefile+="debug:\n";
  makefile+=mprefix+"echo $$PATH\n";
  makefile+=mprefix+"echo $$SHELL\n\n";

  makefile+="clean:\n";
  makefile+=mprefix+"rm -rf ${SUBDIRS} ${ARCHNAME} bin build_complete_tag\n\n";

  makefile+="configure: configure-vxworks configure-hostd configure-host\n\n";
  makefile+="build: build-vxworks build-hostd build-host\n";
  makefile+=mprefix+"touch build_complete_tag\n\n";
  makefile+="build-clean: build-vxworks-clean build-hostd-clean build-host-clean\n";
  makefile+=mprefix+"touch build_complete_tag\n\n";
  makefile+="rebuild: unpack configure build\n\n";
  makefile+="rebuild-clean: clean unpack configure build-clean\n\n";



  // find location of IDEA libs
  STD_string idea_libdir;
  svector n4_delis=match_in_dir(opts_cache.ideadir, "n4_deli_");
  if(n4_delis.size()) {
    idea_libdir=n4_delis[0]+"/x86/delivery/lib"; // VA 25
  } else {
    idea_libdir=opts_cache.ideadir+"/n4_prod/x86/prod/lib"; // default (e.g. VA15)
  }

  bool has_idea_libdir=false;
  testdir=idea_libdir;
  if(testdir.exists()) has_idea_libdir=true;

  // shell command as separate variable to avoid problems with linebreak under MSYS make
  if(has_idea_libdir) {
    makefile+="LIB_HOST_CMD = \\\n";
    makefile+="for file in "+mgwpath(idea_libdir)+"/*d.lib; do \\\n";
    makefile+="  bname=$$(basename $$file d.lib); \\\n";
    makefile+="  if test -f "+mgwpath(idea_libdir)+"/$${bname}.lib; then \\\n";
    makefile+="    echo \"LDLIBS += \"$${bname}.lib  >> ${ODIN2IDEADIR_MGWPATH}/libs_host.txt; \\\n";
    makefile+="    echo \"LDDLLS += \"$${bname}.dll  >> ${ODIN2IDEADIR_MGWPATH}/libs_host.txt; \\\n";
    makefile+="    echo \"LDLIBS += \"$${bname}d.lib >> ${ODIN2IDEADIR_MGWPATH}/libs_hostd.txt; \\\n";
    makefile+="    echo \"LDDLLS += \"$${bname}d.dll >> ${ODIN2IDEADIR_MGWPATH}/libs_hostd.txt; \\\n";
    makefile+="  fi; \\\n";
    makefile+="done\n\n";
  }

  // find location of IDEA includes
  STD_list<STD_string> possible_includepaths;

  // pre VA25
  possible_includepaths.push_back("n4/comp");
  possible_includepaths.push_back("n4/comp/STLport");
  possible_includepaths.push_back("n4_extsw/x86/extsw/MedCom/versant/5_2_2/NT/h");
  possible_includepaths.push_back("n4/comp/Measurement/Sequence");
  possible_includepaths.push_back("n4/comp/Measurement/Sequence/libSBB");
  possible_includepaths.push_back("n4/comp/Measurement/PerAns/PerProxies");
  possible_includepaths.push_back("n4/comp/Measurement/MeasPatient");
  possible_includepaths.push_back("n4/comp/Application/seq");
  possible_includepaths.push_back("n4/comp/ExamUI/ExamDb/Protocol/UILink/StdProtRes");
  possible_includepaths.push_back("n4/comp/Common/STL");

  // VA25 and later
  possible_includepaths.push_back("n4/pkg");

/*
  possible_includepaths.push_back("n4/interfaces/deli/n4/pkg"); // VB20
  possible_includepaths.push_back("n4/tool"); // VB20
  possible_includepaths.push_back("n4/interfaces/deli/n4/tool"); // VB20
  possible_includepaths.push_back("n4/opensource/STLport/stlport"); // VB20
  possible_includepaths.push_back("n4/x86/prod/include"); // VB20
  possible_includepaths.push_back("n4/x86/delivery/include"); // VB20
  possible_includepaths.push_back("n4/x86/extsw/MedCom/include"); // VB20
  possible_includepaths.push_back("SWF_extern/SDK/include"); // VB20
*/

  possible_includepaths.push_back("n4/pkg/STLport");
  possible_includepaths.push_back("n4_extsw/x86/extsw/MedCom/versant/vds605/h");
  possible_includepaths.push_back("n4/pkg/MrServers/MrMeasSrv/SeqIF");
  possible_includepaths.push_back("n4/pkg/MrServers/MrMeasSrv/MeasPatient");
  possible_includepaths.push_back("n4/pkg/MrServers/MrImaging/libSBB");
  possible_includepaths.push_back("n4/pkg/MrServers/MrPerAns/PerProxies");
  possible_includepaths.push_back("n4/pkg/MrServers/MrImaging/seq");
  possible_includepaths.push_back("n4/pkg/MrServers/MrProtSrv/MrProtocol/UILink/StdProtRes");
  possible_includepaths.push_back("n4/pkg/MrCommon/MrCFramework/STL");


  STD_string idea_includes;
  for(STD_list<STD_string>::const_iterator it=possible_includepaths.begin(); it!=possible_includepaths.end(); ++it) {
    testdir=opts_cache.ideadir+SEPARATOR_STR+(*it);
    if(testdir.exists()) {
      idea_includes+=" "+opts_cache.get_ideadir_slash()+"/"+(*it);
    }
  }

  // shell command as separate variable to avoid problems with linebreak under MSYS make
  makefile+="IDEA_INCLUDES_CMD = \\\n";
  makefile+="for include in "+idea_includes+"; do \\\n";
  makefile+="  echo \"CPPFLAGS_LOCAL += -I$${include}\" >> ${ODIN2IDEADIR_MGWPATH}/idea_includes.txt; \\\n";
  makefile+="done\n\n";




  STD_string idea_defines;

  // checking iPAT (parallel imaging) capability
  testfile=opts_cache.ideadir+"/n4/pkg/MrServers/MrImaging/seq/common/iPAT/iPAT.h";
  if(testfile.exists()) idea_defines="-DHAVE_iPAT";

  // checking whether standard STL header 'vector' is available (which is the case on VB13)

  STD_list<STD_string> vector_header_alternatives;
  vector_header_alternatives.push_back(opts_cache.ideadir+"/n4_opensource/STLport/stlport/vector");
  vector_header_alternatives.push_back(opts_cache.ideadir+"/n4/opensource/STLport/stlport/vector"); // VB18 and later
  LDRfileName vector_header_file;
  select_alternative(vector_header_file, vector_header_alternatives);
  if(vector_header_file.exists()) idea_defines+=" -DHAVE_STL_VECTOR";


  // Checking for MultiByteToWideChar in MrProt.h
  STD_list<STD_string> possible_mrprot;
  possible_mrprot.push_back(opts_cache.ideadir+"/n4/comp/Measurement/Sequence/Prot/MrProt.h");
  possible_mrprot.push_back(opts_cache.ideadir+"/n4/pkg/MrServers/MrProtSrv/MrProt/MrProt.h");
  int retval=find_in_alternatives("MultiByteToWideChar",possible_mrprot);
  if(retval<0) return;
  if(retval>0) idea_defines+=" -DHAVE_MULTIBYTETOWIDECHAR";


  // Checking whether we have study index
  STD_list<STD_string> possible_measpatient;
  possible_measpatient.push_back(opts_cache.ideadir+"/n4/comp/Measurement/MeasPatient/MeasPatient.h");
  possible_measpatient.push_back(opts_cache.ideadir+"/n4/pkg/MrServers/MrMeasSrv/MeasPatient/MeasPatient.h");
  retval=find_in_alternatives("uiStudyIndex",possible_measpatient);
  if(retval<0) return;
  if(retval>0) idea_defines+=" -DHAVE_STUDYINDEX";


  // Checking whether we have INC_16
  STD_list<STD_string> possible_seqdefines;
  possible_seqdefines.push_back(opts_cache.ideadir+"/n4/comp/Measurement/Sequence/SeqDefines.h");
  possible_seqdefines.push_back(opts_cache.ideadir+"/n4/pkg/MrServers/MrProtSrv/MrProt/SeqDefines.h");
  retval=find_in_alternatives("INC_16",possible_seqdefines);
  if(retval<0) return;
  if(retval>0) idea_defines+=" -DHAVE_INC_16";



  // Assume new Linux-based Ice reco is there if IceConfigurators are present
  STD_string icecmd="MakeIcePrg";
  testdir=opts_cache.ideadir+"/n4/pkg/MrServers/MrVista/Config/IceConfigurators";
  if(testdir.exists()) {
    idea_defines+=" -DHAVE_ICE_CONFIGURATOR";
    icecmd="mi";
  }

  // checking whether libGTR is there
  testfile=opts_cache.ideadir+"/n4/x86/delivery/lib/libGTR.lib";
  bool has_libgtr=testfile.exists();


  // shell command as separate variable to avoid problems with linebreak under MSYS make
  makefile+="VXWORKS_MAKEFILE_CMD = \\\n";
  makefile+="cat ${ARCHNAME}/platforms/IDEA_n4/"+opts_cache.get_vxworks_Makefile()+" | \\\n";
  makefile+="awk -v repl=${ODIN2IDEA_LABEL} '{gsub(\"_ODIN2IDEA_TAG_\",repl); print; }' | \\\n";
  makefile+="awk -v repl=\"$$(cat ${ODIN2IDEADIR_MGWPATH}/idea_includes.txt)\"  '{gsub(\"_IDEA_INCLUDES_TAG_\",repl); print; }' | \\\n";
  makefile+="awk -v repl=\""+odindir_slash+"/vxworks\"  '{gsub(\"_ODINDIR_TAG_\",repl); print; }' | \\\n";
  makefile+="awk -v repl=\""+idea_defines+"\" '{gsub(\"_CPPFLAGS_TAG_\",repl); print; }' \\\n";
  makefile+="> ${ODIN2IDEADIR_MGWPATH}/"+opts_cache.get_vxworks_Makefile()+"\n\n";

  // shell command as separate variable to avoid problems with linebreak under MSYS make
  STD_string idea_libdir_backslash=replaceStr(idea_libdir,"\\","\\\\\\\\"); // double protect backslash from shell
  makefile+="HOST_MAKEFILE_CMD = \\\n";
  makefile+="for HOSTLETTER in \"d\" \"\" ; do \\\n";
  makefile+="  cat ${ARCHNAME}/platforms/IDEA_n4/MakHost.txt | \\\n";
  makefile+="  awk -v repl=\"$$HOSTLETTER\" '{gsub(\"_DEBUGLETTER_TAG_\",repl); print; }' | \\\n";
  makefile+="  awk -v repl=${ODIN2IDEA_LABEL} '{gsub(\"_ODIN2IDEA_TAG_\",repl); print; }' | \\\n";
  makefile+="  awk -v repl=\""+idea_defines+"\" '{gsub(\"_CPPFLAGS_TAG_\",repl); print; }' | \\\n";
  makefile+="  awk -v repl=\""+idea_libdir_backslash+"\" '{gsub(\"_IDEA_LIBDIR_TAG_\",repl); print; }' | \\\n";
  if(has_libgtr)      makefile+="  awk '{gsub(\"\\\\\\#REM_IF_HAS_LIBGTR \",\"\"); print; }' | \\\n";
  if(has_idea_libdir) makefile+="  awk -v repl=\"$$(cat ${ODIN2IDEADIR_MGWPATH}/libs_host$${HOSTLETTER}.txt)\"  '{gsub(\"\\\\\\#REPL_IF_HAS_IDEA_HOST_LIBS\",repl); print; }' | \\\n";
  makefile+="  awk -v repl=\"$$(cat ${ODIN2IDEADIR_MGWPATH}/idea_includes.txt)\"  '{gsub(\"_IDEA_INCLUDES_TAG_\",repl); print; }' | \\\n";
  makefile+="  awk -v repl=\""+odindir_slash+"/host$${HOSTLETTER}\"  '{gsub(\"_ODINDIR_TAG_\",repl); print; }' \\\n";
  makefile+="  > ${ODIN2IDEADIR_MGWPATH}/MakHost$${HOSTLETTER}.txt; \\\n";
  makefile+="done\n\n";

  // shell command as separate variable to avoid problems with linebreak under MSYS make
  makefile+="VB_MAKEFILE_CMD = \\\n";
  makefile+="cat ${ARCHNAME}/platforms/IDEA_n4/odin.mk | \\\n";
  makefile+="awk -v repl=${ODIN2IDEA_LABEL} '{gsub(\"_ODIN2IDEA_TAG_\",repl); print; }' | \\\n";
  makefile+="awk -v repl=\"$$(cat ${ODIN2IDEADIR_MGWPATH}/idea_includes.txt)\"  '{gsub(\"_IDEA_INCLUDES_TAG_\",repl); print; }' | \\\n";
  makefile+="awk -v repl=\""+idea_defines+"\" '{gsub(\"_CPPFLAGS_TAG_\",repl); print; }' | \\\n";
  makefile+="awk -v repl=\""+odindir_slash+"\"  '{gsub(\"_ODINDIR_TAG_\",repl); print; }' \\\n";
  makefile+="> ${ODIN2IDEADIR_MGWPATH}/${ODIN2IDEA_LABEL}.mk\n\n";







  // Set up IDEA sequence
  makefile+="ideaseq:\n";
  makefile+=mprefix+"mkdir -p ${ODIN2IDEADIR_MGWPATH}\n";
  makefile+=mprefix+"echo \"\\\""+replaceStr(conf_cache.get_installdir(),"\\","/")+"/bin/odin.exe\\\" -m \\\"${ODIN2IDEADIR_SLASH}\\\"\" > ${ODIN2IDEADIR_MGWPATH}/odin.bat\n";

  // copying files
  makefile+=mprefix+"cp ${ARCHNAME}/platforms/IDEA_n4/odin2idea.cpp ${ODIN2IDEADIR_MGWPATH}\n";
  makefile+=mprefix+"cp ${ARCHNAME}/platforms/IDEA_n4/ldr4idea.h ${ODIN2IDEADIR_MGWPATH}\n";
  makefile+=mprefix+"cat ${ARCHNAME}/platforms/IDEA_n4/makefile.trs | sed s/ODIN2IDEA_TAG/${ODIN2IDEA_LABEL}/ > ${ODIN2IDEADIR_MGWPATH}/makefile.trs\n";




  // Create libs_host(d).txt which contains all libs found in the directory where IDEA libs are stored
  if(has_idea_libdir) {
    makefile+=mprefix+"rm -f ${ODIN2IDEADIR_MGWPATH}/libs_host.txt ${ODIN2IDEADIR_MGWPATH}/libs_hostd.txt\n";
    makefile+=mprefix+"${LIB_HOST_CMD}\n";
  }

  makefile+=mprefix+"rm -f ${ODIN2IDEADIR_MGWPATH}/idea_includes.txt\n";
  makefile+=mprefix+"${IDEA_INCLUDES_CMD}\n";


  // Creating batch script for Makefiles
  makefile+=mprefix+"cat ${ARCHNAME}/platforms/IDEA_n4/MakSeq.bat | sed s/_VXWORKS_EXT_TAG_/"+opts_cache.get_vxworks_ext()+"/g > ${ODIN2IDEADIR_MGWPATH}/MakSeq.bat\n";

  // creating VA*/VB12 Makefile for vxworks
  makefile+=mprefix+"${VXWORKS_MAKEFILE_CMD}\n";

  // creating VA*/VB12 Makefiles for host(d)
  makefile+=mprefix+"${HOST_MAKEFILE_CMD}\n";

  // creating Makefile for VB13 and later
  makefile+=mprefix+"${VB_MAKEFILE_CMD}\n";


  // Set up Ice
  STD_string iceodindir="\""+mgwpath(opts_cache.icedir)+"/IceOdin\"";
  makefile+="ice:\n";
  makefile+=mprefix+"mkdir -p ${ICEODINDIR}\n";
//  makefile+=mprefix+"cat ${ARCHNAME}/platforms/IDEA_n4/Ice/IceOdin.evp | sed s/ODINRECO_FLAG/false/ > ${ICEODINDIR}/IceOdin.evp\n";
//  makefile+=mprefix+"cat ${ARCHNAME}/platforms/IDEA_n4/Ice/IceOdin.evp | sed s/ODINRECO_FLAG/true/ > ${ICEODINDIR}/IceOdinOnline.evp\n";
  makefile+=mprefix+"cp ${ARCHNAME}/platforms/IDEA_n4/Ice/IceOdin.evp ${ICEODINDIR}\n";
  makefile+=mprefix+"cp ${ARCHNAME}/platforms/IDEA_n4/Ice/*.h ${ICEODINDIR}\n";
  makefile+=mprefix+"cp ${ARCHNAME}/platforms/IDEA_n4/Ice/*.cpp ${ICEODINDIR}\n";
  makefile+=mprefix+"cp ${ARCHNAME}/platforms/IDEA_n4/Ice/MakeRelease.X86 ${ICEODINDIR}\n\n";
  makefile+=mprefix+"cp ${ARCHNAME}/platforms/IDEA_n4/Ice/Makefile.imk ${ICEODINDIR}\n\n";

  STD_string makefilename=opts_cache.odindir+SEPARATOR_STR+"Makefile";
  write(makefile, makefilename);

  STD_string msg=justificate("A Makefile to build and install ODIN for IDEA has been stored under "+makefilename+". Execute this Makefile now?");
  if(!message_question(msg.c_str(), "Execute Makefile?", GuiDialog::get_widget(),true)) return;


   // set environment variables which would be set by cygwin.bat otherwise
#ifdef USING_WIN32
/*
  STD_string oldpath=getenv_nonnull("PATH");
  if(oldpath.find(cygnus_bin) != 0) { // add only if cygnus_bin is not already 1st entry
    STD_string path="PATH="+cygnus_bin+";"+oldpath;
    ODINLOG(odinlog,normalDebug) << "oldpath/path=" << oldpath << "/" << path << STD_endl;
    _putenv(path.c_str());
  }
*/
  _putenv("MAKE_MODE=UNIX");
#endif

  STD_string target="rebuild";
  if(opts_cache.make_clean) target+="-clean";
  if(!execute_make(make,target)) return;
  msg=justificate("Congratulations! You have sucessfully compiled ODIN for IDEA. Includes and libraries have been installed under "+opts_cache.odindir);
  message_question(msg.c_str(), "Build complete", GuiDialog::get_widget());

  if(!execute_make(make,"ideaseq")) return;
  msg=justificate("An IDEA sequence '"+opts_cache.odin2idea_label+"' which holds the ODIN methods has been "
                  "installed under "+opts_cache.seqdir+". "
                  "You can now open the SDE shell and change to the newly "
                  "created sequence by using the 'cs' command. "
                  "Then type 'odin' to start the ODIN graphical user interface. "
                  "After creating/editing your ODIN methods, you can create the "
                  "DLLs via the 'Siemens' menu.");
  message_question(msg.c_str(), "Sequence setup complete", GuiDialog::get_widget());

  if(!execute_make(make,"ice")) return;
  msg=justificate("An ICE reconstruction 'IceOdin' which writes the raw data to disk has been "
                  "installed under "+opts_cache.icedir+". "
                  "You can now use the ICE shell to compile the ICE "
                  "reconstruction using the command '"+icecmd+"'.");
  message_question(msg.c_str(), "ICE setup complete", GuiDialog::get_widget());

}


bool IdeaDialog::execute_make(const STD_string& make, const STD_string& target) {
  Log<OdinComp> odinlog("IdeaDialog","execute_make");

  svector cmd_chain; cmd_chain.resize(1);
  cmd_chain[0]="\""+make+"\" -C "+opts_cache.odindir+" "+target;

  ProcessDialog makedlg(GuiDialog::get_widget());
  int waitstate=makedlg.execute_cmds(cmd_chain,false);  // log to console
  bool status=(!waitstate);
  ODINLOG(odinlog,normalDebug) << "status / waitstate = " << status << " / " << waitstate << STD_endl;
  if(!status && waitstate!=-2) {
    error_msg("Make "+target+" failed");
    return false;
  }

  if(waitstate==-2) { // code for cancel
    return false;
  }

  return true;
}




void IdeaDialog::update() {
  Log<OdinComp> odinlog("IdeaDialog","update");

  if(!checkdir(opts_cache.ideadir.c_str())) {
    error_msg("Cannot find IDEA installation under "+opts_cache.ideadir);
    return;
  }

  // check for space characters
  if(opts_cache.odindir.find(" ") != STD_string::npos) {
    error_msg("Please do not use space characters in installation path, otherwise compilation fails");
  }

  // set compilers and path of tools
  opts_cache.win_cxx=opts_cache.msvcdir+"/VC98/Bin/cl.exe";

/*
  STD_list<STD_string> vxworks_dir_alternatives;
  vxworks_dir_alternatives.push_back(opts_cache.ideadir+"/n4_fsp/tornado/i86/host/x86-win32");
  vxworks_dir_alternatives.push_back(opts_cache.ideadir+"/n4_fsp/tornado/ppc/host/x86-win32");
  vxworks_dir_alternatives.push_back(opts_cache.ideadir+"/n4_fsp/WindRiver/gnu/4.3.3-vxworks-6.9/x86-win32");
  if(!select_alternative(opts_cache.vxworks_dir, vxworks_dir_alternatives)) {
    error_msg("Cannot find vxworks directory");
  }

  STD_list<STD_string> vxworks_path_alternatives;
  vxworks_path_alternatives.push_back(opts_cache.vxworks_dir+"/lib/gcc-lib/powerpc-wrs-vxworks/cygnus-2.7.2-960126");
  vxworks_path_alternatives.push_back(opts_cache.vxworks_dir+"/bin"); // for VB12
  if(!select_alternative(opts_cache.vxworks_path, vxworks_path_alternatives)) {
    error_msg("Cannot find vxworks executable path");
  }
*/

  STD_string global_includes;
  STD_string global_defines;


  // set includes
  STD_list<STD_string> possible_includepaths;
  possible_includepaths.push_back("n4/x86/delivery/include");
  // pre VA25
  possible_includepaths.push_back("n4/comp");
  possible_includepaths.push_back("n4/comp/Measurement/Sequence/libRT");
  possible_includepaths.push_back("n4/comp/Measurement/Sequence/libSBB"); // for fSBBECGFillTimeRun
  possible_includepaths.push_back("n4/comp/Measurement/Sequence/SeqBuffer");
  possible_includepaths.push_back("n4/comp/Measurement/Sequence/Prot"); // for MrProt.h
  // VA25/VB*
  possible_includepaths.push_back("n4/pkg");
  possible_includepaths.push_back("n4/pkg/MrServers/MrMeasSrv/SeqIF/libRT");
  possible_includepaths.push_back("n4/pkg/MrServers/MrImaging/libSBB"); // for fSBBECGFillTimeRun
//  possible_includepaths.push_back("n4/pkg/MrServers/MrImaging/libSeqUtil"); // for SysProperties
  possible_includepaths.push_back("n4/pkg/MrServers/MrMeasSrv/SeqIF/SeqBuffer");
  possible_includepaths.push_back("n4/pkg/MrServers/MrProtSrv/MrProt"); // for MrProt.h
  possible_includepaths.push_back("n4/x86/delivery/include/MrServers/MrMeasSrv/SeqIF/libRT"); // for libRTmsg.h

  select_idea_include_dirs(global_includes, possible_includepaths, opts_cache);



  // extra includes for VxWorks
  LDRfileName vxworks_target_includes;
  vxworks_target_includes.set_dir(true);
  STD_list<STD_string> vxworks_target_includes_alternatives;
  vxworks_target_includes_alternatives.push_back(opts_cache.ideadir+"/n4/pkg/MrServers/MrMPCUSystem/Tornado_i86/target/h");
  vxworks_target_includes_alternatives.push_back(opts_cache.ideadir+"/n4_fsp/tornado/ppc/target/h");
  vxworks_target_includes_alternatives.push_back(opts_cache.ideadir+"/n4_fsp/WindRiver/vxworks-6.9/target/h"); // VB20
  select_alternative(vxworks_target_includes, vxworks_target_includes_alternatives);


  // checking for presence of sGRAD_PULSE_ARB class in sGRAD_PULSE.h
  STD_list<STD_string> sGRAD_PULSE_h_alternatives;
  sGRAD_PULSE_h_alternatives.push_back(opts_cache.ideadir+"/n4/pkg/MrServers/MrMeasSrv/SeqIF/libRT/sGRAD_PULSE.h");
  sGRAD_PULSE_h_alternatives.push_back(opts_cache.ideadir+"/n4/comp/Measurement/Sequence/libRT/sGRAD_PULSE.h");
  int retval=find_in_alternatives("sGRAD_PULSE_ARB",sGRAD_PULSE_h_alternatives);
  if(retval<0) return;
  if(retval>0) global_defines+="-DHAVE_sGRAD_PULSE_ARB";


  // checking for presence of FFTScale in MrCoilSelect.h
  STD_list<STD_string> MrCoilSelect_h_alternatives;
  MrCoilSelect_h_alternatives.push_back(opts_cache.ideadir+"/n4/comp/Measurement/Sequence/CoilSelect/MrCoilSelect.h");
  MrCoilSelect_h_alternatives.push_back(opts_cache.ideadir+"/n4/pkg/MrServers/MrProtSrv/MrProt/CoilSelect/MrCoilSelect.h");
  retval=find_in_alternatives("FFTScale",MrCoilSelect_h_alternatives);
  if(retval<0) return;
  if(retval>0) global_defines+=" -DHAVE_MRCOILSELECT_FFTSCALE";


  opts_cache.vxworks_flags=global_includes+" -I"+replaceStr(vxworks_target_includes,"\\","/")+" "+global_defines+" -DVXWORKS -DCPU="+opts_cache.vxworks_cpu.LDRenum::operator STD_string();

  opts_cache.vxworks_opts="-ansi";
  if(tolowerstr(opts_cache.vxworks_cpu).find("pentium")!=STD_string::npos) {
     opts_cache.vxworks_opts+=" -O1"; // -O2 causes the compiler to enter inifinite loop ...
     opts_cache.vxworks_opts+=" -mcpu=pentium -march=pentium -fvolatile -nostdlib -fno-builtin -fno-defer-pop -malign-double";
  } else {
     opts_cache.vxworks_opts+=" -O2";
  }

  STD_list<STD_string> possible_hostall_includepaths;
  possible_hostall_includepaths.push_back("n4/comp/STLport"); // VA* path
  possible_hostall_includepaths.push_back("n4_opensource/STLport/stlport"); // VB* path, if this path is absent, the MSVC iostream would be used which fails
  possible_hostall_includepaths.push_back("n4/opensource/STLport/stlport"); // VB20

  possible_hostall_includepaths.push_back("n4_extsw/x86/extsw/MedCom/include"); // VA* path
  possible_hostall_includepaths.push_back("n4/x86/extsw/MedCom/include"); // VB* path
  STD_string hostall_idea_includes;
  select_idea_include_dirs(hostall_idea_includes, possible_hostall_includepaths, opts_cache);

  STD_string hostall_defines=global_defines+" -DWIN32 -D_AFXDLL -DCSA_HAS_DLL -D_MBCS -DAFX_NOVTABLE= -D_RWTOOLSDLL -D_UNICODE -DUNICODE -DO_DLL_PCLASS_ONLY -D_CONSOLE -D_WINDLL -DACE_HAS_DLL";
  STD_string hostall_includes=global_includes+" "+hostall_idea_includes;
//  STD_string hostall_opts="-nologo -Z7 -O2 -GR -GX -G5 -GF";
  STD_string hostall_opts="-nologo -Zi -O2 -GR -GX -G5 -GF";

  opts_cache.hostd_flags=hostall_defines+" "+hostall_includes+" "+hostall_opts+" -MDd";
  opts_cache.host_flags= hostall_defines+" "+hostall_includes+" "+hostall_opts+" -MD";

  optswidget->updateWidget();
  emit changed();
}


IdeaDialog::~IdeaDialog() {
  delete pb_compile;
  delete optswidget;
  delete grid;
}

////////////////////////////////////////////////////////////////////////////////////////


IdeaMethodDialog::IdeaMethodDialog(const STD_string& methdir, sarray& selectedMethods, const STD_string& install_prefix, const IdeaOpts& ideaopts, QWidget *parent)
  : GuiDialog(parent,"Select Methods",false),
  selMeth(selectedMethods), methrootdir(methdir), install_dir(install_prefix), ideaopts_cache(ideaopts) {
  Log<OdinComp> odinlog("IdeaMethodDialog","IdeaMethodDialog");

  svector subdirs(browse_dir(methdir,true,true));
  ODINLOG(odinlog,normalDebug) << "subdirs=" << subdirs.printbody() << STD_endl;

  grid = new GuiGridLayout( GuiDialog::get_widget(), 3, 1 );

  svector collabel; collabel.resize(1);
  collabel[0]="Method";
  method_list=new GuiListView (GuiDialog::get_widget(), collabel, 100, 300);

  for(unsigned int i=0; i<subdirs.size(); i++) {

    bool initstate=false;
    for(unsigned int j=0;j<selectedMethods.length();j++) {
      if(selectedMethods[j]==subdirs[i]) initstate=true;
    }

    collabel[0]=subdirs[i];
    GuiListItem* subdir_entry=new GuiListItem(method_list, collabel, true, initstate);

    method_checks.push_back(subdir_entry);
  }

  grid->add_widget( method_list->get_widget(), 0, 0 );

  pb_make = new GuiButton( GuiDialog::get_widget(), this, SLOT(make()), "Make" );
  grid->add_widget( pb_make->get_widget(), 1, 0, GuiGridLayout::Center );

  pb_cancel = new GuiButton( GuiDialog::get_widget(), this, SLOT(emitDone()), "Cancel" );
  grid->add_widget( pb_cancel->get_widget(), 2, 0, GuiGridLayout::Center );

  GuiDialog::show();
}


IdeaMethodDialog::~IdeaMethodDialog() {
  for(STD_list<GuiListItem*>::iterator dirit=method_checks.begin();dirit!=method_checks.end();++dirit) {
    delete (*dirit);
  }
  delete method_list;
  delete pb_make;
  delete pb_cancel;
  delete grid;
}


void IdeaMethodDialog::make() {
  Log<OdinComp> odinlog("IdeaMethodDialog","make");

  STD_list<STD_string> methlist;

  for(STD_list<GuiListItem*>::const_iterator dirit=method_checks.begin();dirit!=method_checks.end();++dirit) {
    if((*dirit)->is_checked()) methlist.push_back((*dirit)->get_text());
  }

  if(!methlist.size()) {
    message_question("Please select at least one method", "No Method(s) Selected", GuiDialog::get_widget(),false,true);
    return;
  }

  selMeth.resize(methlist.size());


  unsigned int index=0;
  for(STD_list<STD_string>::const_iterator it=methlist.begin(); it!=methlist.end(); ++it) {
    selMeth[index]=(*it);
    ODINLOG(odinlog,normalDebug) << "selMeth[" << index << "]=" << (*it) << STD_endl;
    SeqMakefile mf(selMeth[index],install_dir);

    STD_string methdir=methrootdir+SEPARATOR_STR+selMeth[index]+SEPARATOR_STR;

    svector cmd_chain;
    cmd_chain=mf.get_odin4idea_method_compile_chain(methdir, ideaopts_cache.odindir,
                                                      ideaopts_cache.vxworks_path, ideaopts_cache.get_vxworks_cxx(), ideaopts_cache.vxworks_flags+" "+ideaopts_cache.vxworks_opts,
                                                      ideaopts_cache.win_cxx, ideaopts_cache.hostd_flags, ideaopts_cache.host_flags);

    ProcessDialog makedlg(GuiDialog::get_widget());
    int waitstate=makedlg.execute_cmds(cmd_chain);
    bool status=(!waitstate);
    ODINLOG(odinlog,normalDebug) << "status / waitstate = " << status << " / " << waitstate << STD_endl;
    if(!status && waitstate!=-2) {
      message_question("An error occured during method compilation", "Make Failed", GuiDialog::get_widget(),false,true);
      this->emitDone();
      return;
    }

    index++;
  }


  unsigned int imeth;
  STD_string methods_decls,methods_inits,methods_objs;
  for(imeth=0; imeth<selMeth.length(); imeth++) {
    methods_decls+="int "+selMeth[imeth]+"_main(int argc,char* argv[]);\n";
    methods_inits+=selMeth[imeth]+"_main(0,0);\n";

    // use forward slashes in the following for unix-style make of VB*
    methods_objs +="HostObjects += "+selMeth[imeth]+"/"+selMeth[imeth]+".obj\n";
    methods_objs +="HostObjectsd += "+selMeth[imeth]+"/"+selMeth[imeth]+"d.obj\n";
    methods_objs +="VxWorksObjects += "+selMeth[imeth]+"/"+selMeth[imeth]+".o\n";
  }

  ::write(methods_decls,   methrootdir+SEPARATOR_STR+"methods_decls.h");
  ::write(methods_inits,   methrootdir+SEPARATOR_STR+"methods_inits.h");
  ::write(methods_objs,    methrootdir+SEPARATOR_STR+"methods_objs.txt");

  message_question("You can now execute 'ms' (make sequence) in \n the SDE shell to complete the DLL build", "Finished", GuiDialog::get_widget());


  this->emitDone();
  return;
}

void IdeaMethodDialog::emitDone() {
  GuiDialog::done();
}
