/***************************************************************************
                          odindialog_process.h  -  description
                             -------------------
    begin                : Sun Oct 2 21:30:11 CEST 2005
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef ODINDIALOG_PROCESS_H
#define ODINDIALOG_PROCESS_H

#include <tjutils/tjprocess.h> // for forkResult

#include "odindialog_progress.h"

#define TIMER_INTERVAL 100

class ProcessDialog : public ProgressDisplayDialog {

 public:
  ProcessDialog(QWidget *parent);

  int execute_cmds(const svector& command_chain, bool log_std_streams=true);

 private:
  void update_progbar();

  int progcount;
};

#endif
