/***************************************************************************
                          odindialog_system.h  -  description
                             -------------------
    begin                : Sun Oct 2 21:30:11 CEST 2005
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef ODINDIALOG_SYSTEM_H
#define ODINDIALOG_SYSTEM_H

#include <qobject.h>

#include <odinqt/ldrwidget.h>

#include <odinpara/system.h>



class SystemDialog : public QObject, public GuiDialog {
 Q_OBJECT

 public:
  SystemDialog(QWidget *parent);
  ~SystemDialog();

 signals:
  void new_platform(odinPlatform pF);
  void new_setting();
  void finished();

 private slots:
  void emitDone();
  void emitChanged();
  void change_platform(int pF);

 private:
  void create_systemInfo_widget();

  GuiGridLayout* grid;
  enumBox* pf;
  GuiButton* pb_done;
  LDRwidget* sysInfo_widget;
};

#endif
