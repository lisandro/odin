/***************************************************************************
                          odinmethod.h  -  description
                             -------------------
    begin                : Sun Dec 25 21:30:11 CEST 2005
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef OdinMethod_H
#define OdinMethod_H

#include <tjutils/tjstate.h>

#include <odinseq/seqmeth.h>

#include "odincomp.h"
#include "odinconf.h"

class QWidget; // forward declaration


//////////////////////////////////////////////////////////////////////

/*
  State diagram for OdinMethod, i.e. for compiling/linking of sequences:

  Possible transitions between states are indicated by
  the corresponding member functions that perform the transition.


  clean <-------\
                 |
    |            | clear()
    | compile()  |
    |            |
    V            |
                 |
  compiled ------| /---------\
                 | |         |
    |            | |         |
    | link()     | |         | relink()
    |            | |         |
    V            | |         |
                 | V         |
  linked --------'  ---------'

*/

//////////////////////////////////////////////////////////////////////

/**
  *
  * Callback to report success/failure of state transitions
  *
  */
struct OdinMethodCallback  {

  virtual void report(bool status, const STD_string& trans_label, const STD_string& message) = 0;

  virtual void create_widgets() = 0;

  virtual void delete_widgets() = 0;

};


//////////////////////////////////////////////////////////////////////

/**
  *
  * State machine to handle compiling and linking of sequences
  *
  */
class OdinMethod : public StateMachine<OdinMethod> {

 public:
  OdinMethod(const OdinConf& conf, OdinMethodCallback* callback, QWidget* parent4dialog);

/**
  * Performs state transition to 'clean'
  */
  bool clear() {return clean.obtain_state();}

/**
  * Performs state transition to 'compiled'
  */
  bool compile() {return compiled.obtain_state();}

/**
  * Performs state transition to 'linked'
  */
  bool link() {return linked.obtain_state();}

/**
  * Relinks the sequence into process space
  */
  bool relink();

/**
  * Returns pointer to the encapsulated method
  */
  SeqMethod* operator -> () {return method.get_current_method();}

/**
  * Returns true if valid source code is present
  */
  bool check_srcdir();

/**
  * Location of sequencePars file for this method
  */
  STD_string seqParsFile();

 private:

  // states and their transition functions:

  State<OdinMethod> clean;
  bool make_clean();

  State<OdinMethod> compiled;
  bool clean2compiled();

  State<OdinMethod> linked;
  bool compiled2linked();
  bool linked2compiled();


  // helpers:
  bool unlink();
  bool make();
  STD_string find_so();




  SeqMethodProxy method;

  // stuff passed in during construction
  const OdinConf& settings;
  OdinMethodCallback* cback;
  QWidget* parent;


};

#endif
