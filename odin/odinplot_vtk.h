/***************************************************************************
                          odinplot_vtk.h  -  description
                             -------------------
    begin                : Tue Jul 26 2005
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef ODINPLOT_VTK_H
#define ODINPLOT_VTK_H



#include <tjutils/tjtools.h>
#include <tjutils/tjlog.h>

// forward declarations of vtk classes
class vtkRenderer;
class vtkRenderWindow;
class vtkRenderWindowInteractor;
class vtkPolyDataMapper;
class vtkActor;
class vtkAxes;
class vtkTubeFilter;
class vtkActor;
class vtkArrowSource;


/////////////////////////////////////////////////////


class VtkMagnPlotter {

 public:
  VtkMagnPlotter();
  ~VtkMagnPlotter();

  void start();

  void plot_vector(float M[3], float* dM);

  void interact();

 private:
  vtkRenderer* renderer;
  vtkRenderWindow* renWin;
  vtkRenderWindowInteractor* iren;
  vtkArrowSource* magnArrow;
  vtkPolyDataMapper* magnMapper;
  vtkActor* magnActor;
  vtkActor* magnGradActor;
  vtkAxes* axes;
  vtkTubeFilter* axesTubes;
  vtkPolyDataMapper* axesMapper;
  vtkActor* axesActor;

  bool magnGradActor_added;
};

#endif
