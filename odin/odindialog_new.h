/***************************************************************************
                          odindialog_new.h  -  description
                             -------------------
    begin                : Mon Oct 10 21:30:11 CEST 2005
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef ODINDIALOG_NEW_H
#define ODINDIALOG_NEW_H

#include <qobject.h>

#include <odinqt/ldrwidget.h>



class NewMethDialog : public QObject, public GuiDialog {
 Q_OBJECT

 public:
  NewMethDialog(QWidget *parent, LDRblock& files);
  ~NewMethDialog();

 public:
  void updateWidget();

 signals:
  void valueChanged();


 private slots:
  void done();
  void cancel();
  void emit_valueChanged();

 private:

  GuiGridLayout* grid;
  GuiButton*   pb_done;
  GuiButton*   pb_cancel;
  LDRwidget* fileswidget;
};



#endif
