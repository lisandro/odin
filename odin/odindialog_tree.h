/***************************************************************************
                          odindialog_tree.h  -  description
                             -------------------
    begin                : Mon Oct 10 21:30:11 CEST 2005
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef ODINDIALOG_TREE_H
#define ODINDIALOG_TREE_H


#include <odinqt/odinqt.h>

#include <odinseq/seqtree.h>



class TreeDialog : public GuiDialog, public SeqTreeCallbackAbstract {

 public:
  TreeDialog(QWidget *parent, const char* caption, const svector& column_labels);
  ~TreeDialog();

 private:
  // virtual functions from SeqTreeCallbackAbstract
  void display_node(const SeqClass* thisnode, const SeqClass* parentnode, int treelevel, const svector& columntext);

  GuiGridLayout* grid;

  GuiListView* root;

  STD_map<const SeqClass*, GuiListItem*> nodemap;
  STD_map<GuiListItem*, GuiListItem*>  lastitemmap;

};

#endif
