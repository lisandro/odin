/***************************************************************************
                          odindialog_pulsar.h  -  description
                             -------------------
    begin                : Mon Oct 10 21:30:11 CEST 2005
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef ODINDIALOG_PULSAR_H
#define ODINDIALOG_PULSAR_H

#include <qobject.h>

#include <tjutils/tjprocess.h>

#include <odinqt/odinqt.h>


class SeqPulsar; // forward declaration

class PulsarDialog :  public QObject, public GuiDialog, public GuiListViewCallback {
 Q_OBJECT

 public:
  PulsarDialog(const STD_list<const SeqPulsar*>& pulses, const STD_string& pulsarcmd, STD_list<Process>& subprocs, const STD_string& tmpdir, const STD_string& systemInfoFile, QWidget *parent);
  ~PulsarDialog();

 private slots:
  void emitDone();

 private:

  // overloading virtual function of GuiListViewCallback
  void clicked(GuiListItem* item);

  GuiGridLayout* grid;
  GuiListView* pulsar_list;
  GuiButton* pb_done;
  STD_map<GuiListItem*, const SeqPulsar*> pulse_map;
  STD_string pulsar_cmd;
  STD_list<Process>& procs;
  STD_string tmp_dir;
  STD_string systemInfo_file;
};


#endif
