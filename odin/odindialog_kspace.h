/***************************************************************************
                          odindialog_kspace.h  -  description
                             -------------------
    begin                : Mon Oct 10 21:30:11 CEST 2005
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef ODINDIALOG_KSPACE_H
#define ODINDIALOG_KSPACE_H

#include <qobject.h>

#include <odinqt/odinqt.h>

#include "odindialog_progress.h" // for OdinCancel


#define KSPACE_POINT_SIZE 3
#define KSPACE_INPLANE_SIZE 512
#define KSPACE_THROUGHPLANE_SIZE 128
#define KSPACE_BORDER_SIZE 50


class KspaceDialog : public QObject, public GuiDialog {
 Q_OBJECT

 public:
  KspaceDialog(QWidget *parent, float kmax, unsigned int nadc);
  ~KspaceDialog();

  void draw_point(float kx, float ky, float kz);

 protected:
  void repaint(); // overloaded from GuiDialog
  void close() {canceled=true;} // overloaded from GuiDialog

 private slots:
  void cancel();

 private:

  int kpos2index(float kpos) const;

  void progress();

  GuiGridLayout* grid;
  GuiButton*   pb_cancel;
  QLabel*      kspace_label;
  QPixmap*     kspace_pixmap_buff;
  GuiProgressBar* progbar;

  GuiPainter* painter;

  float k0;
  int progcount;
  bool canceled;
};



#endif
