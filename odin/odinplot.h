/***************************************************************************
                          odinplot.h  -  description
                             -------------------
    begin                : Mon Jul 26 2004
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef OdinPlot_H
#define OdinPlot_H

#include <qwidget.h>

#include <odinpara/ldrtypes.h>
#include <odinpara/ldrnumbers.h>

#include <odinqt/plot.h>
#include <odinqt/ldrwidget.h>

#include <odinseq/seqplot.h>

#include "odinplot_vtk.h"


#define MIN_HEIGHT_PER_CHAN 120
#define MIN_WIDTH_PER_CHAN 1000

#define CLOSEST_CURVE_MAX 30
#define PLOT_SYMBOLS_MAX_RANGE 10
#define MAX_HIGHRES_INTERVAL 1000.0
#define MAX_MARKER_INTERVAL  400.0
#define MAX_START_RANGE 10000

#define MAGN_PLOT_PAGE_DURATION 20.0


class RangeWidget; // forward declaration

/////////////////////////////////////////////////////////////

class PlotView : public QWidget, public SeqSimFeedbackAbstract {

 Q_OBJECT

 public:
  PlotView(const char* method, const STD_string& nucleus, GuiMainWindow* parent);

  ~PlotView();

  const SeqTimecourseData* get_timecourses();
  const SeqTimecourseData* get_kspace_trajs();

  bool simulate(const STD_string& fidfile, const STD_string& samplefile, ProgressMeter* progmeter);



 public slots:

  void replot();

  void mouseMovedInPlot0(const QMouseEvent& e) {mouseMovedInPlot(0,e);}
  void mouseMovedInPlot1(const QMouseEvent& e) {mouseMovedInPlot(1,e);}
  void mouseMovedInPlot2(const QMouseEvent& e) {mouseMovedInPlot(2,e);}
  void mouseMovedInPlot3(const QMouseEvent& e) {mouseMovedInPlot(3,e);}
  void mouseMovedInPlot4(const QMouseEvent& e) {mouseMovedInPlot(4,e);}
  void mouseMovedInPlot5(const QMouseEvent& e) {mouseMovedInPlot(5,e);}
  void mouseMovedInPlot6(const QMouseEvent& e) {mouseMovedInPlot(6,e);}
  void mouseMovedInPlot7(const QMouseEvent& e) {mouseMovedInPlot(7,e);}
  void mouseMovedInPlot8(const QMouseEvent& e) {mouseMovedInPlot(8,e);}

  void mousePressedInPlot0(const QMouseEvent& e) {mousePressedInPlot(0,e);}
  void mousePressedInPlot1(const QMouseEvent& e) {mousePressedInPlot(1,e);}
  void mousePressedInPlot2(const QMouseEvent& e) {mousePressedInPlot(2,e);}
  void mousePressedInPlot3(const QMouseEvent& e) {mousePressedInPlot(3,e);}
  void mousePressedInPlot4(const QMouseEvent& e) {mousePressedInPlot(4,e);}
  void mousePressedInPlot5(const QMouseEvent& e) {mousePressedInPlot(5,e);}
  void mousePressedInPlot6(const QMouseEvent& e) {mousePressedInPlot(6,e);}
  void mousePressedInPlot7(const QMouseEvent& e) {mousePressedInPlot(7,e);}
  void mousePressedInPlot8(const QMouseEvent& e) {mousePressedInPlot(8,e);}

  void mouseReleasedInPlot0(const QMouseEvent& e) {mouseReleasedInPlot(0,e);}
  void mouseReleasedInPlot1(const QMouseEvent& e) {mouseReleasedInPlot(1,e);}
  void mouseReleasedInPlot2(const QMouseEvent& e) {mouseReleasedInPlot(2,e);}
  void mouseReleasedInPlot3(const QMouseEvent& e) {mouseReleasedInPlot(3,e);}
  void mouseReleasedInPlot4(const QMouseEvent& e) {mouseReleasedInPlot(4,e);}
  void mouseReleasedInPlot5(const QMouseEvent& e) {mouseReleasedInPlot(5,e);}
  void mouseReleasedInPlot6(const QMouseEvent& e) {mouseReleasedInPlot(6,e);}
  void mouseReleasedInPlot7(const QMouseEvent& e) {mouseReleasedInPlot(7,e);}
  void mouseReleasedInPlot8(const QMouseEvent& e) {mouseReleasedInPlot(8,e);}

  void autoscalePlot0() {autoscale_y(0);}
  void autoscalePlot1() {autoscale_y(1);}
  void autoscalePlot2() {autoscale_y(2);}
  void autoscalePlot3() {autoscale_y(3);}
  void autoscalePlot4() {autoscale_y(4);}
  void autoscalePlot5() {autoscale_y(5);}
  void autoscalePlot6() {autoscale_y(6);}
  void autoscalePlot7() {autoscale_y(7);}
  void autoscalePlot8() {autoscale_y(8);}

  void rescalePlot0(double val) {rescale_y(0,val);}
  void rescalePlot1(double val) {rescale_y(1,val);}
  void rescalePlot2(double val) {rescale_y(2,val);}
  void rescalePlot3(double val) {rescale_y(3,val);}
  void rescalePlot4(double val) {rescale_y(4,val);}
  void rescalePlot5(double val) {rescale_y(5,val);}
  void rescalePlot6(double val) {rescale_y(6,val);}
  void rescalePlot7(double val) {rescale_y(7,val);}
  void rescalePlot8(double val) {rescale_y(8,val);}


  void update_x_axes();

  void autoscale_x();

  void hide_and_show();

  void set_rect_zoom_tool() {set_zoom_tool(rect);}
  void set_hort_zoom_tool() {set_zoom_tool(horizontal);}
  void set_vert_zoom_tool() {set_zoom_tool(vertical);}

  
  void change_toolbar();
  
  void plot_all();

  void osci();

  void print();

  void save();


  void close();
  
  void changeMode(int newmode);

  void settingsChanged();

  void save_closest_curve_data();
  void save_current_channel_data();

 signals:
  void setMessage(const char* text);

  void closeMe();
  
 private:
  friend class PlotWindow;

  
  double get_x(int iplot, int x_pixel);
  double get_y(int iplot, int y_pixel);


  void mouseMovedInPlot   (int iplot, const QMouseEvent& e);
  void mousePressedInPlot (int iplot, const QMouseEvent& e);
  void mouseReleasedInPlot(int iplot, const QMouseEvent& e);

  void set_range_and_update_x_axes(double min, double max, bool discard_scrollbar=false);

  void autoscale_all_y();
  void autoscale_y(int iplot);
  void rescale_y(int iplot, double val);


  void get_plot_label(int iplot, STD_string& label, STD_string& unit) const;
  void set_plot_labels(bool extra_space=false);

  void set_curve_pens(bool thick_lines=false);

  void create_baseline();

  void add_plotcurve(const Curve4Qwt& curve);
  void create_plotcurves();

  void add_marker(const Marker4Qwt& marker);
  void add_tc_marker(const TimecourseMarker4Qwt& marker);
  void create_markers();

  void create_plotcurves_and_markers();


  bool create_timecourses(timecourseMode type);

  void create_timecourse_markers();

  void plot_timecourses(timecourseMode type);



  SeqPlotDataAbstract* plotdata;

  double totaldur;
  STD_string nuc;

  int plot_pressed,x_pressed,y_pressed;

  static int lastplot_old;

  double baseline_x[2];
  double baseline_y[2];
  long curveid_baseline[numof_plotchan];
  

  int closest;
  int lastplot_closest;
  int lastcurve_closest;

  int iplot_cache;


  GuiToolBar* chan_toolbar;
  GuiToolBar* zoom_toolbar;
  GuiToolBar* settings_toolbar;

  GuiComboBox* plotmode;
  timecourseMode oldmode;

  
  GuiGridLayout* grid;
  GuiPlot* plotter[numof_plotchan];
  GuiWheel* wheel[numof_plotchan];
  GuiToolButton* plotflag[numof_plotchan];
//  GuiButton* autoscalebutton[numof_plotchan];

  GuiToolButton* oscibutton;
  GuiToolButton* printbutton;
  GuiToolButton* savebutton;
  GuiToolButton* closebutton;

  RangeWidget* x_range;
  GuiPrinter* printer;

  GuiToolButton* plotAxes;
  GuiToolButton* plotMarkers;
  GuiToolButton* plotGrid;
  

  enum zoomMode {rect=0, horizontal, vertical, numof_zoomModes};

  void set_zoom_tool(zoomMode mode);

  GuiToolButton* zoomflag[numof_zoomModes];

  long markid_vertzoom;
  long markid_hortzoom;
  

  STD_map<long,Curve4Qwt> curves_map[numof_plotchan];

  long timecourse_curve_id[numof_tcmodes][numof_plotchan];


  // Stuff for osci
  LDRenum TriggerType;
  LDRdouble TriggerWidth;
  LDRdouble RefreshRate;


  VtkMagnPlotter* active_magplot;
  long markid_magplot;
  int ipage;
  void plot_vector(double timepoint, float M[3], float* dM);  // implementing virtual function of SeqSimFeedbackAbstract

};

////////////////////////////////////////

class PlotWindow : public QObject, public GuiMainWindow {
 Q_OBJECT

 public:
  PlotWindow(const char* method, const STD_string& nucleus, QWidget *parent);
  ~PlotWindow();

  const SeqTimecourseData* get_timecourses()  {return view->get_timecourses();}
  const SeqTimecourseData* get_kspace_trajs() {return view->get_kspace_trajs();}

  bool simulate(const STD_string& fidfile, const STD_string& samplefile, ProgressMeter* progmeter) {return view->simulate(fidfile,samplefile,progmeter);}
  
 public slots:
  void statusBarMessage(const char* text);
  void close();

 private:
  PlotView* view;
};


#endif
