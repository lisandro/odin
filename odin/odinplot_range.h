/***************************************************************************
                          odinplot_range.h  -  description
                             -------------------
    begin                : Wed Oct 12 2005
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef ODINPLOT_RANGE_H
#define ODINPLOT_RANGE_H

#include <qwidget.h>

#include <odinqt/floatedit.h>

class QScrollBar; // forward declaration


class RangeWidget : public QWidget {

 Q_OBJECT

 public:
  RangeWidget(double total_min, double total_max, double start_min, double start_max, QWidget *parent);
  ~RangeWidget();

  double get_min() {return min_x->get_value();}
  double get_max() {return max_x->get_value();}

// public slots:
  void set_range(double min, double max, bool discard_scrollbar=false);

 signals:
  void new_range();



 private slots:
  void new_scroll_val(int v);

  void new_minmax_val(float);

 private:
  friend class QScrollBar;
  friend class floatLineEdit;

  void check_and_set_range(double min, double max);
  void update_scrollbar();

  GuiGridLayout* grid;
  GuiScrollBar* scrollbar;
  floatLineEdit *min_x, *max_x;

  double total_min_cache, total_max_cache;

  bool react_on_scrollbar_change;
};



#endif
