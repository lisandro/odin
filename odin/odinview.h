/***************************************************************************
                          odinview.h  -  description
                             -------------------
    begin                : Sun Sep 28 21:30:11 CEST 2003
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef OdinView_H
#define OdinView_H

#include <odinqt/ldrwidget.h>
#include <odinqt/ldrblockwidget.h>

#include <odinpara/ldrarrays.h>


#include "odinconf.h"
#include "odindebugger.h"
#include "odinmethod.h"
#include "odinplot.h"


#define ODIN_MINWIDTH 600
#define MESSAGES_HEIGHT 200

#define IMAGEVIEWER "miview"



class ProgressDisplayDialog; // forward declaration

class NewMethDialog;  // forward declaration

class IdeaOpts; // forward declaration


class OdinView : public QWidget, public OdinMethodCallback {

 Q_OBJECT
 public:
  OdinView(QWidget *parent);
  ~OdinView();

  void initOdinView(GuiToolBar* action_toolbar, bool has_debug_cmdline);

  static void usage();


 public slots:
  void new_method();
  void open_method();
  void close_method();
  void exit_odin();

  void edit();
  void recompile();
  bool plot() {return create_seqplot();}
  void kspace();
  void sim();
  void geo();

  void seqprops();
  void seqtree();
  void recoinfo();
  void pulsars();

  void pulsprog();
  void gradprog();
  void parx();
  void pv_pilot();
  void pv_scan();
  void pv_rgscan();
  void pv_howto();

  void ideaevents();
  void compile_idea();
  void export_dlls();
  void ideahowto();

  void epiccode();

  void edit_prefs();
  void edit_system();
  void load_system();
  void edit_study();
  void debug_opts();


  void show_manual();
  void show_apidoc();
  void show_seqdoc();

  void switch_to_platform_noask(odinPlatform pF);

  void recalc_method();

  void new_geo_pars();

  int load_prefs(bool ignore_debugLevels);
  int react_on_changed_prefs();
  int save_prefs();

  int save_protocol();
  int load_protocol();

  void newmeth_rels();

//  void changeMethod(int index);


 signals:
  void newCaption(const char* text);
  void newStatus(bool, const char* text);

 private:


  void bruker_scan(bool autorg);

  bool chsrcdir();

  bool prepare_method();
  bool prepare_acquisition();

  bool do_odinreco(const STD_string& outprefix);

  void update_methodsel();

  bool switch_to_platform(odinPlatform pF, bool ask=true);

  STD_string get_samplefile();

  bool create_seqplot(bool for_simulation=false);


  // implementing virtual functions of OdinMethodCallback
  void report(bool status, const STD_string& trans_label, const STD_string& message);
  void create_widgets();
  void delete_widgets();

  static void odintracefunction(const LogMessage& msg);
  static GuiTextView* messages_ptr;

  OdinConf settings;
  IdeaOpts* ideaopts;

  OdinMethod method;

  SeqPlatformProxy platform;

  GuiGridLayout *grid;

  GuiComboBox* methodsel;

  GuiTextView* messages;

  LDRwidget* seqpars;
  LDRwidget* commpars;


  ProgressDisplayDialog* progress;

  PlotWindow* seqplot;

  double* signal_x;
  double* signal_y;


  LDRfileName templatemeth;
  LDRstring newmethlabel;
  NewMethDialog* mewmethwizzard;
  bool newmethlabel_modified;

  STD_list<Process> subprocs;

  OdinDebugger debugger;
  void change_debugger();
};

#endif
