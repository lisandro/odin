/***************************************************************************
                          odin.h  -  description
                             -------------------
    begin                : Sun Sep 28 21:30:11 CEST 2003
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef Odin_H
#define Odin_H


#include "odinview.h"

/**
  * \page odin_doc ODIN user interface for sequence design (odin binary)
  *
  * \image html http://od1n.sourceforge.net/screenshots/odin.jpg
  *
  * \section odin_intro Introduction
  * The ODIN user interface is an easy-to-use program to edit, compile, test, and
  * simulate NMR sequences interactively in a single window.
  * Thereby the NMR sequence is compiled as a plug-in (shared object on UNIX or DLL
  * on Windows) and linked into the user interface. After that, the sequence
  * can be analyzed in various ways (vizualisation, simulation).
  * This documentation will describe all elements of the user interface
  * and their functionality to accomplish this task.
  *
  * \section odin_qg Quick Guide
  * A typical session with the ODIN user interface involves the following steps
  * - Create a new sequence by File->New or open an existing sequence by File->Open
  * - Compile & Link the sequence by the 'recycling'-symbol in the tool bar (alternatively Action->Compile & Link)
  * - Edit the sequence paramers
  * - Visualize the sequence by the 'plot'-symbol in the tool bar (alternatively Action->Plot Sequence)
  * - Simulate the sequence by the 'oscilloscope'-symbol in the tool bar (alternatively Action->Simulate)
  *
  *
  * \section parfield Parameter Field
  * After successfully compiling and linking a sequence, the central part
  * will contain a grid of widgets which contain the sequence parameters.
  * Each time a parameter is changed, the sequence will be recalculated.
  *
  * \section msgbox Message Box
  * The lower part contains a text field which displays warning/error messages from the
  * current sequence, compiler errors, etc.
  *
  * \section statindicator Status Bar
  * At the very bottom, the current status of the user interface is indicated by
  * a green/red light and a short message. For example, if compilation of
  * the sequence fails, this will be indicated in the Status Bar.
  *
  *
  * \section fileMenu File Menu
  * \subsection fileMenu_new New
  * Creates a new sequence by taking the source code of another sequences as a template
  * \subsection fileMenu_open Open
  * Opens the source code of a previously used sequence
  * \subsection fileMenu_close Close
  * Closes the current sequence
  * \subsection fileMenu_save Save Settings
  * Saves the current state of the ODIN user interface (compiler settings, debug levels, sequence parameters, system configuration, ...)
  * \subsection fileMenu_storeprot Save Protocol
  * Saves the current measurement protocol in a single protocol file
  * \subsection fileMenu_loadprot Load Protocol
  * Loads the current measurement protocol from a single protocol file
  * \subsection fileMenu_quit Quit
  * Exit the ODIN user interface
  *
  * \section actionMenu Action Menu
  * All entries in this menu can also be found in the tool bar with the corresponding icon.
  * \subsection actionMenu_edit Edit
  * Opens an editor (customizable via Preferences->Settings) to edit the source code of the sequence
  * \subsection actionMenu_comp Compile & &Link
  * Compiles the current sequence, and if successful, links it into the ODIN user interface
  * \subsection actionMenu_geo Geometry
  * Opens a dialog to edit the current geometry, i.e. slice positioning
  * \subsection actionMenu_plot Plot Sequence
  * Plots the sequence using an embedded plotting window
  * \subsection actionMenu_kspace Acquisition k-Space
  * Shows the k-Space during acquisition
  * \subsection actionMenu_sim Simulate
  * Simulates the current sequence. First, a dialog will pop-up to ask you for
  * various simulation settings. Then, after loading a virtual sample, a virtual NMR
  * signal will be created which can then be viewed in the plotting window. The
  * simulated raw data together with the parameter files is stored under
  * $HOME/odin-methods/<current-method>/sim. After simulation, the reconstruction is started using the simulated raw data.
  *
  * \section infoMenu Info Menu
  * \subsection infoMenu_prop Sequence Properties
  * Shows the description of the sequence together with some additional information
  * \subsection infoMenu_tree Sequence Tree
  * Displays the sequence tree, i.e. the internal layout of the sequence
  * \subsection infoMenu_reco Reconstruction Parameters
  * Displays all parameters used for automatic reconstruction
  * \subsection infoMenu_pulsars Pulsar Pulses
  * Displays a list of used SeqPulsar pulses in the current sequence.
  * By clicking an item in the list, the Pulsar user interface will pop up to show the
  * parameters of the pulse.
  *
  * \if Paravision
  * \section brukerMenu Bruker Menu
  * \subsection brukerMenu_ppg Pulse Program
  * Displays the pulse program
  * \subsection brukerMenu_gp Gradient Program
  * Displays the gradient program
  * \subsection brukerMenu_parx PARX parameters
  * Displays parameters in the PARX parameter space which are used by ODIN
  * \subsection brukerMenu_pilot Pilot
  * Start a pilot scan on the host where Paravision is running
  * \subsection brukerMenu_scan Scan
  * Start a scan (GOP) on host where Paravision is running using the current sequence settings
  * \endif
  *
  * \if IDEA_n4
  * \section siemensMenu Siemens Menu
  * \subsection siemensMenu_events Event Table
  * Displays the event blocks and the events therein of the current sequence
  * \subsection siemensMenu_idea Compile ODIN for IDEA
  * Compile the ODIN libraries for sequence design for IDEA so that IDEA DLLs can be exported
  * \subsection siemensMenu_dll Export Siemens DLLs
  * Create all files/objects necessary to create a Siemens sequence DLL which contains all previously selected ODIN sequences
  * \endif
  *
  * \if EPIC
  * \section geMenu GE Menu
  * \subsection geMenu_events EPIC Code
  * Generates code for the EPIC framework
  * \endif
  *
  * \section prefMenu Preferences Menu
  * \subsection prefMenu_sett Settings
  * Edit the Settings of the ODIN user interface
  * \subsection prefMenu_sys System
  * Edit the system configuration
  * \subsection prefMenu_loadsys Load systemInfo
  * Load the system configuration from a file
  * \subsection prefMenu_debug Debugging/Tracing
  * Change the debugging/tracing settings.
  * This option is not available if ODIN was compiled as a release
  *
  */

#define IDS_ODIN_ABOUT               "ODIN user interface\nVersion " VERSION \
                                    "\n(w) 2004-2015 by Thies Jochimsen\n\n" \
                                    "Compiled with the following plugins:\n" \
                                    PLUGIN_LIBS


class Odin : public QObject, public GuiMainWindow {
  Q_OBJECT

 public:
  Odin();

  void initOdin(GuiApplication* a, bool has_debug_cmdline);



 public slots:
  void changeCaption(const char* text);
  void changeStatus(bool status, const char* text);


 private slots:
  void slotHelpAbout();

 private:
  OdinView *view;

  GuiPopupMenu *fileMenu;
  GuiPopupMenu *actionMenu;
  GuiPopupMenu *infoMenu;
  GuiPopupMenu *brukerMenu;
  GuiPopupMenu *siemensMenu;
  GuiPopupMenu *geMenu;
  GuiPopupMenu *prefMenu;

  GuiPopupMenu *helpMenu;

  GuiToolBar *toolbar;

  bool old_status;
  bool first_status;
};



#endif

