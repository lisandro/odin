/***************************************************************************
                          odindialog_progress.h  -  description
                             -------------------
    begin                : Sun Oct 2 21:30:11 CEST 2005
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef ODINDIALOG_PROGRESS_H
#define ODINDIALOG_PROGRESS_H

#include <odinqt/odinqt.h>

#include <tjutils/tjfeedback.h>


struct OdinCancel {}; // exception

/////////////////////////////////////////////

class ProgressDisplayDialog : public GuiProgressDialog, public virtual ProgressDisplayDriver {

 public:
  ProgressDisplayDialog(QWidget *parent, bool modal = true);

  void finish();

 private:
  // Implementing virtual functions of ProgressDisplayDriver
  void init(unsigned int nsteps, const char* txt);
  void increase(const char*);
  bool refresh();

  int counter;  // Getting current progress from GuiProgressDialog does not work in non-percent mode, so we need an extra counter
};


#endif
