/***************************************************************************
                          odindialog_debug.h  -  description
                             -------------------
    begin                : Sun Oct 3 21:30:11 CEST 2005
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef ODINDIALOG_DEBUG_H
#define ODINDIALOG_DEBUG_H

#include <qobject.h>

#include <odinqt/odinqt.h>


class DebugDialog : public QObject, public GuiDialog {

 Q_OBJECT

 public:
  DebugDialog(QWidget *parent);
  ~DebugDialog();

 signals:
  void finished();

 private slots:
  void emitDone();
  void levelChanged(int);

 private:
  friend class QComboBox;
  friend class QPushButton;

  GuiGridLayout* grid;
  GuiButton* pb_done;

  STD_list<GuiComboBox*> debugenums;
};


#endif
