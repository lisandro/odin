#include "odinview.h"

#include "odindialog_process.h"
#include "odindialog_progress.h"
#include "odindialog_system.h"
#include "odindialog_debug.h"
#include "odindialog_pulsar.h"
#include "odindialog_tree.h"
#include "odindialog_kspace.h"
#include "odindialog_new.h"
#include "odindialog_idea.h"

#include "odincomp.h"

#include <tjutils/tjprofiler.h>

#include <odinseq/seqsim.h>
#include <odinseq/seqmakefile.h>


////////////////////////////////////////////////////////////

void OdinView::usage() {
   STD_cout << "odin: Graphical user interface of ODIN" << STD_endl;
   STD_cout << "Usage: odin [options]" << STD_endl;
   STD_cout << "Options:" << STD_endl;
   STD_cout << "\t-c : Do not load configuration" << STD_endl;
   STD_cout << "\t-cl : Log to shell" << STD_endl;
   STD_cout << "\t-i : Ignore environment/registry settings" << STD_endl;
   STD_cout << "\t-m : Root directory of methods" << STD_endl;
   STD_cout << "\t" << LogBase::get_usage() << STD_endl;
   STD_cout << "\t" << helpUsage() << STD_endl;
}



OdinView::OdinView(QWidget *parent) : QWidget(parent),
  settings("settings",isCommandlineOption(GuiApplication::argc(),GuiApplication::argv(),"-i")), ideaopts(new IdeaOpts), method(settings, this, parent),
  grid(0), messages(0), seqpars(0), commpars(0), progress(0), seqplot(0),signal_x(0),signal_y(0) {

  Log<OdinComp> odinlog("OdinView","OdinView");

  newCaption("empty");

  grid = new GuiGridLayout(this, 2, 2, false );

  grid->set_row_minsize(0, 2*MESSAGES_HEIGHT); // reserve space for parameter widgets

  if(!isCommandlineOption(GuiApplication::argc(),GuiApplication::argv(),"-cl")) {
    messages=new GuiTextView(this, ODIN_MINWIDTH, MESSAGES_HEIGHT);
    grid->add_widget( messages->get_widget(), 1, 0, GuiGridLayout::Default, 1, 2 );
    messages_ptr=messages;
    LogBase::set_log_output_function(OdinView::odintracefunction);
  }

}

OdinView::~OdinView() {
  messages_ptr=0;
  delete ideaopts;
}


void OdinView::initOdinView(GuiToolBar* action_toolbar, bool has_debug_cmdline) {
  Log<OdinComp> odinlog("OdinView","initOdinView");

//  methodsel= new GuiComboBox(action_toolbar, svector() );
//  connect( methodsel->get_widget(), SIGNAL(activated(int)),this, SLOT(changeMethod(int)) );


  if(!settings.init(this)) {
    ODINLOG(odinlog,errorLog) << "Unable to initialize settings" << STD_endl;
    exit(-1);
  }
  ODINLOG(odinlog,normalDebug) << "GuiApplication::argc/argv()=" << GuiApplication::argc() << "/" << GuiApplication::argv() << STD_endl;

  char buff[ODIN_MAXCHAR];
  if(!isCommandlineOption(GuiApplication::argc(),GuiApplication::argv(),"-c")) {

    ODINLOG(odinlog,normalDebug) << "loading settings" << STD_endl;
    int loadresult=load_prefs(has_debug_cmdline);
    settings.set_label("General Settings");
    ODINLOG(odinlog,normalDebug) << "loadresult=" << loadresult << STD_endl;

    if(getCommandlineOption(GuiApplication::argc(),GuiApplication::argv(),"-m",buff,ODIN_MAXCHAR)) {
      settings.methroot=STD_string(buff)+SEPARATOR_STR;
      ODINLOG(odinlog,normalDebug) << "method root dir = >" << buff << "<" << STD_endl;
    }

    if( loadresult < 0 ) new_method();
    else {
      //if(chsrcdir()) link();
//      emit newStatus(chsrcdir(),SeqMethodProxy::get_status_string());
      chsrcdir();
    }

  } else {
    ODINLOG(odinlog,normalDebug) << "starting in clean mode" << STD_endl;
    new_method();
  }
}


void OdinView::report(bool status, const STD_string& trans_label, const STD_string& message) {
  emit newStatus(status,message.c_str());
}


void OdinView::create_widgets() {
  Log<OdinComp> odinlog("OdinView","create_widgets");

  commpars=new LDRwidget(method->get_commonPars(),1,this,false,(STD_string(method->get_label())+"_").c_str());
  commpars->show();
  grid->add_widget( commpars, 0, 0, GuiGridLayout::Center );
  connect(commpars,SIGNAL(valueChanged()),this,SLOT(recalc_method()));

  seqpars=new LDRwidget(method->get_methodPars(),1,this,false,(STD_string(method->get_label())+"_").c_str());
  seqpars->show();
  grid->add_widget( seqpars, 0, 1, GuiGridLayout::Center );
  connect(seqpars,SIGNAL(valueChanged()),this,SLOT(recalc_method()));
}


void OdinView::delete_widgets() {
  Log<OdinComp> odinlog("OdinView","delete_widgets");
  if(seqpars) {
    ODINLOG(odinlog,normalDebug) << "deleting seqpars" << STD_endl;
    seqpars->hide();
    delete seqpars;
    seqpars=0;
  }
  if(commpars) {
    ODINLOG(odinlog,normalDebug) << "deleting commpars" << STD_endl;
    commpars->hide();
    delete commpars;
    commpars=0;
  }
}


void OdinView::new_method() {
  Log<OdinComp> odinlog("OdinView","new_method");

  LDRblock newmethfiles("NewSequence");

  templatemeth.set_label("SequenceTemplate");
  templatemeth.set_defaultdir(settings.get_seqexamplesdir());
  templatemeth.set_suffix("cpp");

  newmethlabel.set_label("NewSequenceLabel");
  newmethlabel_modified=false;

  // start with empty fields
  templatemeth="";
  newmethlabel="";

  newmethfiles.append(templatemeth);
  newmethfiles.append(newmethlabel);


  mewmethwizzard=new NewMethDialog(this, newmethfiles);
  connect(mewmethwizzard,SIGNAL(valueChanged()),this,SLOT(newmeth_rels()));

  if(mewmethwizzard->exec()) {

    ODINLOG(odinlog,normalDebug) << "newmethlabel/templatemeth=" << newmethlabel << "/" << templatemeth << STD_endl;

    if( STD_string(newmethlabel)!="" && STD_string(templatemeth)!="" ) {

      createdir(settings.methroot.c_str());
      createdir((settings.methroot+SEPARATOR_STR+newmethlabel).c_str());

      settings.sourcecode=settings.methroot+SEPARATOR_STR+newmethlabel+SEPARATOR_STR+newmethlabel+".cpp";
      ODINLOG(odinlog,normalDebug) << "settings.sourcecode=" << settings.sourcecode << STD_endl;

      copyfile(templatemeth.c_str(),settings.sourcecode.c_str());

      // Create default Makefile
      SeqMakefile mf(newmethlabel,settings.get_installdir());
      ::write(mf.get_Makefile("."),settings.methroot+SEPARATOR_STR+newmethlabel+SEPARATOR_STR+"Makefile");


      if(chsrcdir()) {

	// Delete old sequencePars file
	STD_string seqparsfile(method.seqParsFile());
	ODINLOG(odinlog,normalDebug) << "seqparsfile=" << seqparsfile << STD_endl;
	if(filesize(seqparsfile.c_str())) rmfile(seqparsfile.c_str());

        method.link();
      }

    }

  }
}


void OdinView::newmeth_rels() {
  Log<OdinComp> odinlog("OdinView","newmeth_rels");
  STD_string template_id=templatemeth.get_basename_nosuffix();
  ODINLOG(odinlog,normalDebug) << "templatemeth/newmethlabel/template_id=" << templatemeth << "/" << STD_string(newmethlabel) << "/" << template_id << STD_endl;
  if(!newmethlabel_modified && STD_string(newmethlabel)=="" && template_id!="" ) {
    newmethlabel=template_id;
    ODINLOG(odinlog,normalDebug) << "newmethlabel(pre)=" << STD_string(newmethlabel) << STD_endl;
    mewmethwizzard->updateWidget();
    ODINLOG(odinlog,normalDebug) << "newmethlabel(post)=" << STD_string(newmethlabel) << STD_endl;
    newmethlabel_modified=true;
  }
}


/*
void OdinView::changeMethod(int index) {
}
*/


void OdinView::update_methodsel() {
/*
  int nmeth=method.get_numof_methods();
  svector methlabel; methlabel.resize(nmeth);
  for(int i=0; i<nmeth; i++) methlabel[i]=method[i].get_label();
//  methodsel->set_names(methlabel);
*/
}


void OdinView::open_method() {
  Log<OdinComp> odinlog("OdinView","open_method");
  LDRfileName fname=get_open_filename("Open Method", settings.methroot.c_str(), (STD_string(settings.sourcecode.get_label())+" (*."+settings.sourcecode.get_suffix()+")").c_str(), this);
  if(fname!="") {
    method.clear();
    settings.sourcecode=STD_string(fname);

    // Set methroot accordingly
    LDRfileName methdir=fname.get_dirname();
    settings.methroot=STD_string(methdir.get_dirname());

    if(chsrcdir()) method.link();
  }
}


void OdinView::close_method() {
  Log<OdinComp> odinlog("OdinView","close_method");
  method.clear();
  settings.sourcecode="";
  save_prefs();
  newCaption("empty");
}


void OdinView::exit_odin() {
  Log<OdinComp> odinlog("OdinView","exit_odin");
  save_prefs(); // This must be executed before method.clear() so that linked in libs (odindata) still have their debug handler activated
  method.clear();
//  Profiler::dump_final_result();
  debugger.detach();
  for(STD_list<Process>::iterator it=subprocs.begin(); it!=subprocs.end(); ++it) it->kill();
  GuiApplication::quit();
}


void OdinView::edit() {
  Log<OdinComp> odinlog("OdinView","edit");
  settings.open_ascfile(settings.sourcecode,subprocs);
}


void OdinView::recompile() {
  Log<OdinComp> odinlog("OdinView","recompile");
  method.clear();
  if(method.compile()) method.link();
}


bool OdinView::create_seqplot(bool for_simulation) {
#ifdef STANDALONE_PLUGIN
  Log<OdinComp> odinlog("OdinView","create_seqplot");

  if(seqplot) delete seqplot; seqplot=0;

  if(!switch_to_platform(standalone)) return false;

  // ask for options prior to prepare_method because of coil selection
  SeqPlotDataAbstract* pd=SeqPlatformProxy()->get_plot_data();
  if(!pd) {
    ODINLOG(odinlog,errorLog) << "Unable to get plot_data" << STD_endl;
    return false;
  }

  // load old config
  pd->set_coilsdir(settings.get_coilsdir());
  pd->get_opts(true,true).load(settings.get_confdir()+SEPARATOR_STR+"plotsimopts");

  LDRwidgetDialog* sidlg=new LDRwidgetDialog(pd->get_opts(true,for_simulation),1,this,true);
  ODINLOG(odinlog,normalDebug) << "sidlg=" << sidlg << STD_endl;

  // store new config
  pd->get_opts(true,true).write(settings.get_confdir()+SEPARATOR_STR+"plotsimopts");

  if(!prepare_acquisition()) return false;

  ProgressDisplayDialog display(this);
  ProgressMeter progmeter(display);


  bool display_plot=true;
  try {
    SeqPlatformProxy()->create_plot_events(&progmeter);
  } catch(OdinCancel) {
    ODINLOG(odinlog,infoLog) << " aborted" << STD_endl;
    display_plot=false;
  }

  display.finish();

  if(display_plot) {
    seqplot=new PlotWindow(method->get_label().c_str(), method->get_main_nucleus(), this);
  }

  return display_plot;
#else
  return false;
#endif
}


STD_string OdinView::get_samplefile() {
  Log<OdinComp> odinlog("OdinView","get_samplefile");
  STD_string fname=get_open_filename("Please select a virtual sample", settings.smpfile.c_str(), "Sample Files (*.smp)", this);
  if(fname=="") return "";

  settings.smpfile=fname;
  return settings.smpfile;
}


bool OdinView::do_odinreco(const STD_string& outprefix) {
  Log<OdinComp> odinlog("OdinView","do_odinreco");

  STD_string scandir=method->get_systemInfo().get_scandir();
  ODINLOG(odinlog,normalDebug) << "scandir=" << scandir << STD_endl;

  svector cmd_chain; cmd_chain.resize(1);
  cmd_chain[0]="\""+settings.get_binprefix()+"odinreco\" -ff Hamming -fp 0.8 -f jdx -v 1 -r \""+scandir+"/recoInfo\" -o \""+outprefix+"\"";
  ODINLOG(odinlog,normalDebug) << "cmd_chain=" << cmd_chain.printbody() << STD_endl;

  ProcessDialog recoproc(this);
  int waitstate=recoproc.execute_cmds(cmd_chain);
  bool status=(!waitstate);

  return status;
}


void OdinView::sim() {
#ifdef STANDALONE_PLUGIN
  Log<OdinComp> odinlog("OdinView","sim");

  if(!create_seqplot(true)) return;

  STD_string methdir(settings.sourcecode.get_dirname());
  STD_string simdir(methdir+SEPARATOR_STR+"sim"+SEPARATOR_STR);
  createdir(simdir.c_str());

  STD_string fidfile(simdir+platform->get_rawfile());
  STD_string outfile(simdir+"image");

  STD_string sample=get_samplefile();
  if(sample=="") return;

  ProgressDisplayDialog display(this);
  ProgressMeter progmeter(display);

  bool do_reco=true;

  try {
    if(!seqplot->simulate(fidfile,sample,&progmeter)) do_reco=false;

  } catch(OdinCancel) {
    ODINLOG(odinlog,infoLog) << " aborted" << STD_endl;
    do_reco=false;
  }
  display.finish();

  Profiler::dump_final_result();


  if(do_reco) {
    method->get_systemInfo().set_scandir(simdir);
    method->write_meas_contex(simdir);

    if(!do_odinreco(outfile)) return;

    Process proc;
    proc.start("\""+settings.get_binprefix()+IMAGEVIEWER+"\" \""+outfile+".jdx\"");
    subprocs.push_back(proc);
  }
#endif
}


void OdinView::geo() {
  Log<OdinComp> odinlog("OdinView","geo");

  STD_string geoeditcmd="\""+settings.get_binprefix()+"geoedit\" ";

  STD_string pilot=platform->pv_pilot_scan();
  if(pilot!="") {
    geoeditcmd+="-blowup 2 -pilot  \""+pilot+"\" ";
  } else {
    STD_string sample=get_samplefile();
    if(sample!="") geoeditcmd+="-sample \""+sample+"\" ";
  }

  STD_string geofile=settings.get_tmpdir()+SEPARATOR_STR+"geometryInfo";
  geoeditcmd+="\""+geofile+"\"";

  ODINLOG(odinlog,normalDebug) << "geoeditcmd=" << geoeditcmd << STD_endl;

  method->write_geometry(geofile);

  Process proc;
  proc.start(geoeditcmd,true); // Block odin while geoedit is open
  subprocs.push_back(proc);

  method->load_geometry(geofile);

  new_geo_pars();
}


void OdinView::new_geo_pars() {
  method->get_geometry().update();
  recalc_method();
}


void OdinView::seqprops() {
  Log<OdinComp> odinlog("OdinView","seqprops");

  if(!prepare_method()) return;

  STD_string msg;

  msg+=STD_string(method->get_label())+"\n\n";
  msg+=STD_string(justificate(method->get_description())+"\n");
  msg+="Total Duration : " + ftos(method->get_totalDuration()) + " min\n";
  msg+="Number of Acquisitions : " + itos(method->get_numof_acquisitions()) + "\n";
  msg+="RF Energy Deposition : "+ftos(method->get_rf_energy())+" a.u.\n";

  message_question(msg.c_str(), "Sequence Properies", this);
}


void OdinView::seqtree() {
  Log<OdinComp> odinlog("OdinView","seqtree");
  if(!method.link()) return;

  svector column_labels; column_labels.resize(4);
  column_labels[0]="Label";
  column_labels[1]="Type";
  column_labels[2]="Duration[ms]";
  column_labels[3]="Properties";

  TreeDialog* treedlg=new TreeDialog(this,"Sequence Tree",column_labels);

  method->tree(treedlg);

  treedlg->show();
}


void OdinView::recoinfo() {
  Log<OdinComp> odinlog("OdinView","recoinfo");
  if(!prepare_acquisition()) return;

  STD_string fname(settings.get_tmpdir()+SEPARATOR_STR+"recoInfo");
  method->write_recoInfo(fname);
  settings.open_ascfile(fname,subprocs);
}


void OdinView::pulsars() {
  Log<OdinComp> odinlog("OdinView","pulsars");
  if(!prepare_method()) return;
  STD_string sifname(settings.get_tmpdir()+SEPARATOR_STR+"systemInfo4pulsar");
  method->write_systemInfo(sifname);
  new PulsarDialog(method->get_active_pulsar_pulses(),settings.get_binprefix()+"pulsar",subprocs,settings.get_tmpdir(),sifname,this);
}


void OdinView::kspace() {
#ifdef STANDALONE_PLUGIN
  Log<OdinComp> odinlog("OdinView","kspace");

  if(!create_seqplot()) return;

  const SeqTimecourseData* ktrajs=seqplot->get_kspace_trajs();
  if(!ktrajs) return;

  float kmax=0.0;
  geometryMode geomode=method->get_geometry().get_Mode();
  for(int i=0; i<n_directions; i++) {
    if(geomode==voxel_3d || i<sliceDirection) { // Excluding 3rd direction in slicepack mode
      unsigned int n=method->get_commonPars().get_MatrixSize(direction(i));
      if(n>1) {
        float kmaxdir=secureDivision( PII * n , method->get_geometry().get_FOV(direction(i)) );
        kmax=STD_max(kmax,kmaxdir);
      }
    }
  }

  ODINLOG(odinlog,normalDebug) << "kmax/n_rec_points=" << kmax << "/" << ktrajs->n_rec_points << STD_endl;

  KspaceDialog* kdlg=new KspaceDialog(this,kmax,ktrajs->n_rec_points);

  try {
    for(unsigned int i=0; i<(ktrajs->size); i++) {
      if((ktrajs->y[rec_plotchan][i])>0.0) {
        kdlg->draw_point(ktrajs->y[Gread_plotchan][i],ktrajs->y[Gphase_plotchan][i],ktrajs->y[Gslice_plotchan][i]);
      }
    }
  } catch(OdinCancel) {
    ODINLOG(odinlog,infoLog) << " aborted" << STD_endl;
    delete kdlg;
  }
#endif
}


void OdinView::pulsprog() {
  Log<OdinComp> odinlog("OdinView","pulsprog");
  if(!switch_to_platform(paravision)) return;
  if(!prepare_method()) return;
  STD_string fname(settings.get_tmpdir()+SEPARATOR_STR+"pulsprog");
  programContext context;
  context.mode=brukerPpg;
  STD_string file(platform->get_program(context));
  ::write(file,fname);
  settings.open_ascfile(fname,subprocs);
}


void OdinView::gradprog() {
  Log<OdinComp> odinlog("OdinView","gradprog");
  if(!switch_to_platform(paravision)) return;
  if(!prepare_method()) return;
  STD_string fname(settings.get_tmpdir()+SEPARATOR_STR+"gradprog");
  programContext context;
  context.mode=brukerGpg;
  STD_string file(platform->get_program(context));
  ::write(file,fname);
  settings.open_ascfile(fname,subprocs);
}


void OdinView::parx() {
  Log<OdinComp> odinlog("OdinView","parx");
  if(!switch_to_platform(paravision)) return;
  if(!prepare_acquisition()) return;
  STD_string fname(settings.get_tmpdir()+SEPARATOR_STR+"parx");
  programContext context;
  context.mode=brukerParx;
  STD_string file(platform->get_program(context));
  ::write(file,fname);
  settings.open_ascfile(fname,subprocs);
}


void OdinView::pv_pilot() {
  Log<OdinComp> odinlog("OdinView","pv_pilot");
  method.clear(); // unlink current method
  if(!switch_to_platform(paravision)) return;

  ProgressDisplayDialog display(this);
  ProgressMeter progmeter(display);
  progmeter.new_task(0, "Pilot Scan");

  bool success=false;
  try {
    success=platform->pv_pilot(&progmeter);
  } catch(OdinCancel) {
    ODINLOG(odinlog,infoLog) << " aborted" << STD_endl;
    platform->pv_stop();
  }

  display.finish();

  if(success) geo();

}


void OdinView::bruker_scan(bool autorg) {

  Log<OdinComp> odinlog("OdinView","pv_scan");
  if(!switch_to_platform(paravision)) return;
  if(!prepare_acquisition()) return;

  ProgressDisplayDialog display(this);
  ProgressMeter progmeter(display);
  progmeter.new_task(0, "GOP Scan");

  bool success=false;
  try {
    success=platform->pv_gop(autorg,&progmeter);
  } catch(OdinCancel) {
    ODINLOG(odinlog,infoLog) << " aborted" << STD_endl;
    platform->pv_stop();
  }

  display.finish();

  if(success) {
    STD_string outprefix=method->get_systemInfo().get_scandir()+"/image"; // Get prefix after clone in pv_gop

    if(!do_odinreco(outprefix)) return;

    Process proc;
    proc.start("\""+settings.get_binprefix()+IMAGEVIEWER+"\" -blowup 2 \""+outprefix+".jdx\"");
    subprocs.push_back(proc);
  }


}



void OdinView::pv_scan() {bruker_scan(false);}
void OdinView::pv_rgscan() {bruker_scan(true);}

void OdinView::pv_howto() {
  settings.open_ascfile(settings.get_installdir()+SEPARATOR_STR+"share"+SEPARATOR_STR+"doc"+SEPARATOR_STR+"odin"+SEPARATOR_STR+"Paravision"+SEPARATOR_STR+"HOWTO.txt",subprocs);
}


void OdinView::ideaevents() {
  Log<OdinComp> odinlog("OdinView","ideaevents");
  if(!switch_to_platform(numaris_4)) return;
  if(!prepare_acquisition()) return;

  svector column_labels; column_labels.resize(8);
  column_labels[0]="Event Block / Sequence Object";
  column_labels[1]="NCO";
  column_labels[2]="RF";
  column_labels[3]="Gx";
  column_labels[4]="Gy";
  column_labels[5]="Gz";
  column_labels[6]="ADC";
  column_labels[7]="Sync";

  TreeDialog* evdlg=new TreeDialog(this,"IDEA events",column_labels);

  eventContext context;
  context.action=seqRun;
  context.event_display=evdlg;
  method->event(context);

  evdlg->show();
}

void OdinView::ideahowto() {
  settings.open_ascfile(settings.get_installdir()+SEPARATOR_STR+"share"+SEPARATOR_STR+"doc"+SEPARATOR_STR+"odin"+SEPARATOR_STR+"IDEA_n4"+SEPARATOR_STR+"HOWTO.txt",subprocs);
}


void OdinView::epiccode() {
  Log<OdinComp> odinlog("OdinView","epiccode");
  if(!switch_to_platform(epic)) return;
  if(!prepare_method()) return;
  STD_string fname(settings.get_tmpdir()+SEPARATOR_STR+"odin.e");
  programContext context;
  context.mode=epicCode;
  STD_string file(platform->get_program(context));
  ::write(file,fname);
  settings.open_ascfile(fname,subprocs);
}


void OdinView::edit_prefs() {
  Log<OdinComp> odinlog("OdinView","edit_prefs");
  LDRwidgetDialog* dlg=new LDRwidgetDialog(settings,2,this);
  connect(dlg,SIGNAL(finished()),this,SLOT(react_on_changed_prefs()));
}


void OdinView::edit_system() {
  Log<OdinComp> odinlog("OdinView","edit_system");
  SystemDialog* dlg=new SystemDialog(this);
  connect(dlg,SIGNAL(new_setting()),this,SLOT(recalc_method()));
  connect(dlg,SIGNAL(new_platform(odinPlatform)),this,SLOT(switch_to_platform_noask(odinPlatform)));
  connect(dlg,SIGNAL(finished()),this,SLOT(save_prefs()));
}

void OdinView::edit_study() {
  Log<OdinComp> odinlog("OdinView","edit_study");
  LDRwidgetDialog* dlg=new LDRwidgetDialog(method->get_studyInfo(),2,this);
  connect(dlg,SIGNAL(finished()),this,SLOT(react_on_changed_prefs()));
}

void OdinView::load_system() {
  Log<OdinComp> odinlog("OdinView","load_system");
  STD_string fname=get_open_filename("Load systemInfo", settings.get_homedir().c_str(), "", this);
  if(fname!="") {
    method->load_systemInfo(fname);
    recalc_method();
  }
}


void OdinView::debug_opts() {
  Log<OdinComp> odinlog("OdinView","debug_opts");
#ifdef ODIN_DEBUG
  DebugDialog* dlg=new DebugDialog(this);
  connect(dlg,SIGNAL(finished()),this,SLOT(save_prefs()));
#endif
}



int OdinView::load_prefs(bool ignore_debugLevels) {
  Log<OdinComp> odinlog("OdinView","load_prefs");

  method->load_systemInfo(settings.get_confdir()+SEPARATOR_STR+"systemInfo");

  method->load_geometry(settings.get_confdir()+SEPARATOR_STR+"geometryInfo");

  method->get_studyInfo().load(settings.get_confdir()+SEPARATOR_STR+"studyInfo");

#ifdef ODIN_DEBUG
  if(!ignore_debugLevels) {
    STD_string debugLevels;
    ::load(debugLevels,settings.get_confdir()+SEPARATOR_STR+"debugLevels");
    LogBase::set_levels(debugLevels.c_str());
  }
#endif

  int retval=settings.load(settings.get_confdir()+SEPARATOR_STR+"settings");
  ODINLOG(odinlog,normalDebug) << "retval=" << retval << STD_endl;
  change_debugger();

  ideaopts->set_defaults(settings,0,0); // do not ask for IDEA directory and ignore errors
  ideaopts->load(settings.get_confdir()+SEPARATOR_STR+"ideaopts");

  return retval;
}


int OdinView::react_on_changed_prefs() {
  Log<OdinComp> odinlog("OdinView","react_on_changed_prefs");
  change_debugger();
  return save_prefs();
}



int OdinView::save_prefs() {
  Log<OdinComp> odinlog("OdinView","save_prefs");

  ODINLOG(odinlog,normalDebug) << "saving systemInfo" << STD_endl;
  method->write_systemInfo(settings.get_confdir()+SEPARATOR_STR+"systemInfo");

  ODINLOG(odinlog,normalDebug) << "saving geometryInfo" << STD_endl;
  method->write_geometry(settings.get_confdir()+SEPARATOR_STR+"geometryInfo");

  ODINLOG(odinlog,normalDebug) << "saving studyInfo" << STD_endl;
  method->get_studyInfo().write(settings.get_confdir()+SEPARATOR_STR+"studyInfo");

  ODINLOG(odinlog,normalDebug) << "saving debugLevels" << STD_endl;
  STD_string debugLevels(LogBase::get_levels());
  ::write(debugLevels,settings.get_confdir()+SEPARATOR_STR+"debugLevels");

  ideaopts->write(settings.get_confdir()+SEPARATOR_STR+"ideaopts");

  ODINLOG(odinlog,normalDebug) << "saving settings" << STD_endl;
  LDRblock prefs;
  prefs.merge(settings);
  return prefs.write(settings.get_confdir()+SEPARATOR_STR+"settings");
}


int OdinView::save_protocol() {
  Log<OdinComp> odinlog("OdinView","save_protocol");

  if(!method.link()) return -1;

  STD_string fname=get_save_filename("Store Protocol", settings.protfile.c_str(), "ODIN Protocols (*.pro)", this);
  if(fname=="") return 0;

  ODINLOG(odinlog,normalDebug) << "fname=" << fname << STD_endl;

  settings.protfile=fname;

  return method->write_protocol(fname);
}


int OdinView::load_protocol() {
  Log<OdinComp> odinlog("OdinView","load_protocol");
  if(!method.link()) return -1;

  STD_string fname=get_open_filename("Load Protocol", settings.protfile.c_str(), "ODIN Protocols (protocol *.pro)", this);
  if(fname=="") return 0;

  settings.protfile=fname;

  int result=method->load_protocol(fname);
  if(result>=0) method.relink();
  return result;
}



void OdinView::compile_idea() {
  IdeaDialog* dlg=new IdeaDialog(this, *ideaopts, settings);
  connect(dlg,SIGNAL(changed()),this,SLOT(save_prefs()));
}



void OdinView::export_dlls() {
  Log<OdinComp> odinlog("OdinView","export_dlls");
  LDRfileName testfile(ideaopts->odindir+SEPARATOR_STR+"build_complete_tag");
  ODINLOG(odinlog,normalDebug) << "testfile=>" << testfile << "<" << STD_endl;
  if(!testfile.exists()) {
    message_question(justificate("Cannot find compiled ODIN for IDEA installation under "+ideaopts->odindir+". Please create it first using the following dialog.").c_str(), "ODIN for IDEA not found", this, false, true);
    compile_idea();
  }
  new IdeaMethodDialog(settings.methroot,settings.selectedMethods,settings.get_installdir(), *ideaopts, this);
}



void OdinView::show_manual() {settings.display_html(settings.get_manual_location()+"odin_doc.html",subprocs);}
void OdinView::show_apidoc() {settings.display_html(settings.get_manual_location()+"index.html",subprocs);}
void OdinView::show_seqdoc() {settings.display_html(settings.get_manual_location()+"group__odinseq.html",subprocs);}


bool OdinView::chsrcdir() {
  Log<OdinComp> odinlog("OdinView","chsrcdir");

  method.clear();
  bool result=method.check_srcdir();

  if(result) {
    save_prefs();
    newCaption(settings.sourcecode.get_basename_nosuffix().c_str());
  }

  ODINLOG(odinlog,normalDebug) << "result=" << result << STD_endl;
  
  return result;
}




bool OdinView::prepare_method() {
  Log<OdinComp> odinlog("OdinView","prepare_method");
  bool result=true;
  if(!method.link()) return result=false;
  else {
    method->clear();
    if(!method->prepare()) result=false;
  }
  return result;
}


bool OdinView::prepare_acquisition() {
  Log<OdinComp> odinlog("OdinView","prepare_acquisition");
  bool result=prepare_method();
  if(result) result=method->prep_acquisition();
  return result;
}


void OdinView::recalc_method() {
  Log<OdinComp> odinlog("OdinView","recalc_method");

  if(!method.link()) return;

  if(seqplot) delete seqplot; seqplot=0;

  if(!method->clear()) {
    ODINLOG(odinlog,errorLog) << "clear() failed" << STD_endl;
//    emit newStatus(false,SeqMethodProxy::get_status_string());
    return;
  }

  if(!method->build()) {
    ODINLOG(odinlog,errorLog) << "build() failed" << STD_endl;
//    emit newStatus(false,SeqMethodProxy::get_status_string());
    return;
  }

  ODINLOG(odinlog,normalDebug) << "updating seqpars/commpars=" << seqpars << "/" << commpars << STD_endl;

  if(seqpars) seqpars->updateWidget();
  if(commpars) commpars->updateWidget();
}


void OdinView::odintracefunction(const LogMessage& msg) {
  if(messages_ptr) {

    // Do not use code in here which uses Log

    STD_string msgstr=msg.str(0, false);
    int msglength=msgstr.length();
    if(msgstr[msglength-1]=='\n') msgstr=msgstr.substr(0,msglength-1); // remove endl because lines are wrapped by GuiTextView

    // catch compiler warnings
    logPriority level=msg.level;
    if(msgstr.find("warning:")!=STD_string::npos) level=warningLog;
    if(msgstr.find("error:")  !=STD_string::npos) level=errorLog;

    // Replace any tag braces
    msgstr=replaceStr(msgstr, "<","&lt;");
    msgstr=replaceStr(msgstr, ">","&gt;");

/*
    for(unsigned int i=0; i<msgstr.length(); i++) {
      if(msgstr[i]=='<') msgstr[i]='[';
      if(msgstr[i]=='>') msgstr[i]=']';
    }
*/

    // color-code log level
    if(level==errorLog) msgstr="<font color=red>"+msgstr+"</font>";
    else if(level==warningLog) msgstr="<font color=blue>"+msgstr+"</font>";
    else msgstr="<font color=black>"+msgstr+"</font>";

    messages_ptr->append_text(msgstr.c_str());

  } else {
    default_tracefunction(msg); // fall back to console
  }
}

GuiTextView* OdinView::messages_ptr=0;


void OdinView::switch_to_platform_noask(odinPlatform pF) {
  switch_to_platform(pF,false);
}


bool OdinView::switch_to_platform(odinPlatform pF, bool ask) {
  Log<OdinComp> odinlog("OdinView","switch_to_platform");
  bool result=true;
  if(ask) {
    if(method->get_systemInfo().get_platform()==pF) return true;
    STD_string pFstring(SeqPlatformProxy::get_possible_platforms()[pF]);
    STD_string msg=justificate("In order to perform the operation, the platform must be switched to "+pFstring+". Should it be switched now? (You can switch it back by the Preferences->System menu entry.)");
    result=message_question(msg.c_str(), "Switching Platform", this, true);
  }
  if(result) {
     platform.set_current_platform(pF);
     method.relink();
  }
  return result;
}

void OdinView::change_debugger() {
  Log<OdinComp> odinlog("OdinView","change_debugger");
  if(settings.attachDebugger) debugger.attach();
  else                        debugger.detach();
}



