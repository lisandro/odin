/***************************************************************************
                          odincomp.h  -  description
                             -------------------
    begin                : Sun Oct 3 21:30:11 CEST 2005
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef ODINCOMP_H
#define ODINCOMP_H

// for debugging ODIN component
class OdinComp {
 public:
  static const char* get_compName();
};

#endif
