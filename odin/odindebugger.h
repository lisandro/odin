/***************************************************************************
                          logger.h  -  description
                             -------------------
    begin                : Fri Nov 11 21:30:11 CEST 2003
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef OdinDebugger_H
#define OdinDebugger_H

#include <tjutils/tjprocess.h>

/////////////////////////////////////////////

class OdinDebugger {

 public:
  OdinDebugger() {}

  bool attach();
  bool detach();

 private:
  Process debugger_proc;
};


#endif

