/**
  * \page odinseq_doc ODIN sequence modelling framework (odinseq library)
  *
  * This page describes the design guidelines of the module \ref odinseq
  *
  * \section odinseq_intro Introduction
  * This is a framework for NMR sequence design. It follows
  * a hardware independent, object oriented design approach to
  * reach a high degree of flexibilty and portability while
  * keeping the amount of source code to a minimum.
  *
  * \section motivation Motivation
  * An NMR sequence typically consists of different components
  * like pulses, gradient shapes and so on. From the physicists
  * point of view these objects and their relations can be exclusively
  * described by physical parameters (pulse duration, gradient stength,
  * echo time, ...). This description does not depend on the current
  * hardware the sequence is used on.
  * Furthermore, at this level of abstraction these objects can
  * be grouped together in different ways to form a variety of NMR
  * sequences.
  *
  * Unfortunately most programming enviroments of contemporary
  * spectrometers/scanners do not reflect this modularity. A sequence
  * written in the native enviroment of the machine often contains
  * a vast amount of redundant source code repeated in every method
  * and a difficult low-level interface.
  * Furthermore each manufacturer follows his own approach to write
  * unportable sequences, which may be a disadvantage for scientists
  * working on different spectrometers.
  *
  * This is the point where this framework could come into play.
  * It offers a hardware independent programming interface to a C++
  * class library. This library then performs all the low-level
  * operations that are required for playing out the sequence.
  *
  * \section design Design Principles
  * The class hierachy (\ref odinseq) is designed according to the following guidelines:
  *
  * \subsection design_hierachy Class Hierachy
  *   Each type of component in an NMR sequence (RF-pulse, gradient pulse, ...)
  *   is identified with a class in the C++ programming language.
  *   The components are ordered in a 'family tree' where each child
  *   is a specialisation of its parent. For instance a constant
  *   gradient (SeqGradConst) on one of the gradient channels has a parent
  *   (SeqGradChan) that represents arbitrarily shaped gradient waveforms.
  *   This inheritance graph is modelled by the inheritence mechanisms for
  *   classes in C++. This approach minimises the amount of code that perform
  *   certain operations on the objects, e.g. a function for
  *   rotating the SeqGradChan class in the spatial domain (set_gradrotmatrix())
  *   is automatically available for all derived classes.
  *
  * \subsection design_seqobj Sequence Objects
  *   In this manual the term 'sequence objects' refers to all components
  *   of an NMR experiment that control the timing of the sequence.
  *   For example RF-pulses, delays and acquisition windows are all specialised
  *   sequence objects.
  *   An (abstract) base class SeqObjBase exists for all sequence objects.
  *   These objects then have a common
  *   interface, e.g. they know how to write themselve to the pulse/gradient
  *   program.
  *
  * \subsection design_seqgradobj Gradient Objects
  *   The term 'gradient objects' refers to all components of an NMR sequence
  *   that control the timecourse of the gradient fields, e.g. constant gradients,
  *   phase encoding gradients, gradient waveforms.
  *   An (abstract) base class SeqGradInterface exists for all gradient objects
  *   in the sequence. These objects
  *   then have a common interface, i.e. they know how to write themselve to the gradient
  *   program or how to be combined among themselves.
  *
  * \subsection design_seqgrouping Grouping of Sequence Objects 
  *   Sequence objects can be grouped together by ordering them along
  *   the time axis. The result is then a new, more complex sequence object.
  *   In analogy to notations commonly used in scientific papers, this
  *   is done by using the \p + operator. For instance two RF-pulses
  *   represented by two objects called \p alpha and \p beta can build
  *   a new sequence object
  *\code{.cpp}
alpha + beta
\endcode
  *   which results in a sequence
  *   object which will play out the first pulse and then the second pulse.
  *
  * \subsection design_gradgrouping Grouping of Gradient Objects 
  *   Gradient objects can also be serialised by using the \p + operator. Furthermore
  *   the \p / operator tries to build a new object that plays out the two operands
  *   in parallel if the timing is apropriate. For example two gradient pulses
  *   \p gp1 and \p gp2, one for the read and another for the phase channel, can be
  *   played out in parallel by using the combination
  *\code{.cpp}
gp1 / gp2
\endcode
  *
  * \subsection design_seqparallel Combining Sequence and Gradient objects
  *   Sequence and gradient objects can also be serialised by the \p + operator. To
  *   play out a gradient and a sequence object in parallel, the \p / operator can be
  *   used. For example let \p alpha be an excitation pulse and \p constgrad a constant
  *   gradient, then the combination
  *\code{.cpp}
alpha / constgrad
\endcode
  *   would give a slice selective pulse.
  *
  * \subsection design_seqcontainer Container for Sequence Objects 
  *   The sequence object SeqObjList can be used as a container for other
  *   sequence objects. That is, each SeqObjList can have its own sub-sequence that
  *   consists of other sequence components.
  *   If a certain operation is applied to a SeqObjList object, the operation
  *   will also be applied to all elements of the sub-sequence. For example:
  *\code{.cpp}
      SeqPuls           alpha;                   // excitation pulse
      SeqGradConstPulse constgrad;               // constant gradient
      SeqObjList   objlist = alpha + constgrad;  // alpha is played out after constgrad
\endcode
  *   Calling the member function
  *\code{.cpp}
objlist.get_duration()
\endcode
would then return the sum of the durations of \p alpha and \p constgrad.
  *
  * \subsection design_seqgradcontainer Container for Gradient Objects
  *   The gradient object SeqGradChanList can be used as a container for other
  *   gradient objects which share the same gradient channel. That is, each SeqGradChanList
  *   can have its own sub-sequence that consists of other gradient components.
  *   These sub-objects are then played out subsequently.
  *   This analogous to the above described sequence container.
  *
  * \subsection design_append Appending to Container Objects 
  *   New objects can be added to the container class SeqObjList
  *   by using the \p += operator.
  *
  * \subsection design_loops Sequence Loops ans Vectors
  *   Loops are special sequence containers. They are used to repeat
  *   a certain part of the sequence. Within ODIN, the class SeqObjLoop
  *   accomplishes this task. Two kind of loops are possible when using
  *   this class: Pure repitition loops and vector loops. Repitition
  *   loops simply repeat the specified part of the sequence without
  *   altering it. Let \p loop be of type SeqObjLoop, then heir syntax is:
  *\code{.cpp}
loop ( kernel ) [ n ];
\endcode
  *   This statement will return a sequence object that repeats the sequence
  *   object \p kernel \p n times, where \p n is an integer number.
  *
  *   Vector loops also repeat a specified part of the sequence. Furthermore
  *   they increment the value of a sequence vector each time it is played out.
  *   Sequence vectors are for example phase encoding gradients or pulses
  *   with a frequency list. All sequence vectors share the same base class
  *   SeqVector. Let \p loop be of type SeqObjLoop, then the syntax for vector loops is: 
  *\code{.cpp}
loop ( kernel ) [ vector1 ] [ vector2 ] ...;
\endcode
  *   This statement will return a sequence object that repeats the sequence
  *   object \p kernel while incrementing the values of the attached sequence
  *   vectors  \p vector1, \p vector2, ...
  *   The vectors must contain the same number of values. The loop is then
  *   repeated this number of times.
  *
  * \subsection design_vectors Specialialized Sequence Vectors
  *   - Gradient pulses with different gradient strengths for phase encoding or diffusion weighting (SeqGradVectorPulse, SeqGradPhaseEnc).
  *   - Sequence objects that drive the transmitter (RF pulses) or receiver (acquisition windows)
  *   contain two vector objects for frequency and phase switching to be used for multislice
  *   experiments or phase cycling (SeqFreqChan and SeqPhaseListVector).
  *   - Delay objects with a variable duration, which is changed for each iteration (SeqDelayVector).
  *   - A list of user-defined rotation matrices that can be attached to gradient-related objects in order
  *   to alter their direction subsequently (SeqRotMatrixVector).
  *   - A container object that holds a list of other sequence objects which are played out
  *   sequentially for each repetition (SeqObjVector).
  *
  *
  * \section units Physical Units
  * ODIN is consistent corncerning the physical units. That is, everywhere in the library the
  * following units and their combinations are used:
  * - [mT] for magnetic field strength
  * - [mm] as the spatial unit
  * - [ms] for durations
  *
  * This system has shown to be of practical value for NMR because the numbers are then
  * in a reasonable range.
  * For example the gradient strength is then given in mT/mm and the frequency is given in
  * 1/ms=kHz.
  * The only exception is the angular unit which is treated internally as rad but can be
  * specified at the interface functions in degree for phaselists and flipangles.
  *
  *
  *
  * \section coding Coding Standards
  * \subsection coding_class Building new classes
  * If you want to write your own class that can be plugged into the ODIN framework,
  * please use the following form for your class 'SeqMyClass' that is derived from
  * 'SeqBaseClass':
  *\code{.cpp}

/////////////// *.h stuff: //////////////////////////////

class SeqMyClass : public SeqBaseClass {

 public:
  SeqMyClass(const STD_string& object_label, float parameter1, ... );

  SeqMyClass(const STD_string& object_label = "unnamedSeqMyClass" );

  SeqMyClass(const SeqMyClass& sct);

  SeqMyClass& operator = (const SeqMyClass& sct);

  ~SeqMyClass();

 private:
  float parameter1;

};

/////////////// *.cpp stuff: ////////////////////////////


SeqMyClass::SeqMyClass(const STD_string& object_label, float parameter1_value, ...  ) : SeqBaseClass(object_label) {

  parameter1=parameter1_value;
  ...

}

SeqMyClass::SeqMyClass(const STD_string& object_label ) : SeqBaseClass(object_label) {

  parameter1=0.0;
  ...

}


SeqMyClass::SeqMyClass(const SeqMyClass& sct) {
  SeqMyClass::operator = (sct);
}

SeqMyClass& SeqMyClass::operator = (const SeqMyClass& sct) {
  SeqBaseClass::operator = (sct);

  parameter1=sct.parameter1;
  ...

  return *this;
}

SeqMyClass::~SeqMyClass() {
}
\endcode
  * Following this standard prevents the base classes from getting confused by not
  * correctly initialising or copying them.
  *
  * \subsection coding_stl Using the C++ standard library
  * Unfortunately, on some systems the stanard C++ library seems to be either broken
  * or absent. For these platforms, ODIN contains its own implementation that is a subset of
  * the original library.
  * To use the same source code on different platforms, classes of the standard
  * library are used together with the prefix STD_, e.g. STD_list
  * instead of std::list. These macros will be replaced by the appropriate classes on
  * each platform.
  *
  * \subsection coding_debug Debugging/Tracing
  * To generate debugging and tracing output, please use the Log template class instead
  * of using streams.
  * Instances of that class offer a stream to log trace messages via the ODINLOG macro:
  *\code{.cpp}
int MyClass::myfunction() {
  Log<Seq> odinlog("MyClass","myfunction");

  ODINLOG(odinlog,significantDebug) << "Hello World" << STD_endl;
}
\endcode
  * Which will result in the following output:
  *\code{.cpp}
Seq    |          MyClass.myfunction : START
Seq    |          MyClass.myfunction : Hello World
Seq    |          MyClass.myfunction : END
\endcode
  * It can generate debugging output at different levels of verbosity
  * and the output is redirected to the native logging channel (console, log file) of
  * the current platform. Furthermore, it can be completely removed from the executable
  * for release compilations in a safe way without changing the source code.
  */



/**
  * @defgroup odinseq Classes for sequence design (odinseq library)
  *
  * \ref odinseq_doc
  *
  */

/**
  *
  * @defgroup odinseq_internals Internal Classes for sequence design (odinseq library)
  *
  * PLEASE DO NOT USE THESE CLASSES DIRECTLY WHEN WRITING SEQUENCES!
  */

///////////////////////////////////////////////////////////////////////////////////////

/**
  * \page tjutils_doc Basic data types, vectors and arrays used by ODIN (tjutils library)
  *
  *
  * \section tjutils_intro Introduction
  * Well known data types, such as integers, floating point numbers, complex numbers, strings,
  * vectors (1-dimensional) and arrays (multi-dimensional) are used in ODIN.
  * For all these basic types discussed in this section,
  * there exists a corresponding labeled data record (LDR) type (\ref ldr).
  * This LDR type is prefixed by 'LDR' followed the data type, e.g. 'float' has a LDR
  * counterpart 'LDRfloat' with the same basic functionality.
  * In addition, LDR parameters can be assigned a label, grouped together with other LDR parameters,
  * written to and read from file etc.
  *
  * \section vectors Vectors
  * Vectors are implemented by the tjvector template class which is
  * derived from the STL vector class. Its interface is therefore
  * equivalent to that of the std::vector class.
  * In addition, some useful functions are added.
  * Some predefined vector classes are created by using the tjvector
  * template class:
  * - fvector: vector of float numbers
  * - dvector: vector of double precision float numbers
  * - ivector: vector of int numbers
  * - svector: vector of strings (STD_string objects)
  * - cvector: vector of complex numbers (STD_complex objects)
  *
  * \section arrays Arrays
  * Multidimensional arrays are covered by the tjarray template class.
  * It is derived from the tjvector class to store the data. In addition,
  * the information about the dimensionality and extent in each dimension
  * is stored separately in the class by an ndim object.
  * Some predefined array classes and their corresponding LDR type are
  * created by using the tjarray template class:
  * - farray / LDRfloatArr:   array of float numbers
  * - darray / LDRdoubleArr:  array of double precision float numbers
  * - iarray / LDRintArr:     array of int numbers
  * - sarray / LDRstringArr:  array of strings (STD_string objects)
  * - carray / LDRcomplexArr: array of complex numbers (STD_complex objects)
  *
  * Some examples on how to use these arrays:
  *\code{.cpp}
farray   fa1(3,3);    // creates a 2-dimensional 3x3 array

fa1(0,1)=5.7;         // assigns 5.7 to the second element in the firs row

farray   fa2(3,3);    // creates another 2-dimensional 3x3 array

fa1+fa2;              // returns the element-wise sum of the two arrays

fa1.redim(1,2,3);     // resizes fa1 to a 3-dimensional 1x2x3 array

\endcode
  * Please see the class documention for a complete reference.
  *
  */

/**
  * @defgroup tjutils Classes for basic data types and vectors/arrays (tjutils library)
  *
  * \ref tjutils_doc
  *
  */

///////////////////////////////////////////////////////////////////////////////////////

/**
  * @defgroup ldr Labeled Data Record (LDR) implementation (odinpara library)
  *
  * This page describes the design guidelines of the module \ref ldr
  *
  * The following framework can be used to deal with single parameters of different
  * type (int,float,string,...) and multidimensional arrays that are build from these
  * base types. It is possible to build blocks of parameter, i.e. parameter lists,
  * that can be written/loaded to/from disk in an easily editable ASCII format.
  * The file format is designed roughly according to the JCAMP-DX [1,2] standard, but
  * its main aim was to be compatible with the PARX[3] file format.
  *
  *
  * A very simple examples on how to use this module:
  *\code{.cpp}

LDRint mynumber(23,"mynumber");  // Create an integer parameter with initial value 23

mynumber+=42;                    // LDRint can be used just as a native int for arithmetics

LDRblock block;              // A block (list) of parameters

block.append(mynumber);          // Append the parameter to the block

block.write("block.jdx");        // Write block and its parameters to file 'block.jdx'

block.load("block.jdx");         // Load block and its parameters from file 'block.jdx'

\endcode
  * Please see the class documention for a complete reference.
  *
  *
  *  References:
  * -# JCAMP-DX: A Standard Form of Exchange of Infrared Spectra in Computer
  *     Readable Form, McDONALD, R.S., WILKS, P.A., Apllied Spectroscopy,
  *     Vol.42, No.1, 1988
  * -# JCAMP-DX for NMR, DAVIES, A.N., LAMPEN, P., Apllied Spectroscopy,
  *     Vol.47, No.8, 1993
  * -# PARX is a preprocessor/compiler framework to interactively modify parameters
  *     that can also be embedded into C-code.
  *     It is part of the Bruker software 'Paravision' that serves as an user interface
  *     for their medical MR-scanners.
  *
  *
  *@author Thies H. Jochimsen
  */

///////////////////////////////////////////////////////////////////////////////////////

/**
  * @defgroup odinpara  MR Parameters  (odinpara library)
  *
  * This page describes the design guidelines of the module \ref odinpara_doc
  *
  *
  *@author Thies H. Jochimsen
  */

/**
  * \page odinpara_doc Protcol classes in ODIN (odinpara library)
  *
  * This module/library contains a set of classes which describe
  * different aspects of an MR measurement, i.e. the measurement protocol:
  * - System: The MR system used
  * - Geometry: The geometry of the scan
  * - SeqPars: Common sequence parameters
  * - Study:   Information about the current study (patient data, etc.)
  * - RecoPars: Parameters for automatic reconstruction
  *
  * The class Protocol combines the first four of the above in a single
  * class for convenient access.
  *
  * Within sequence classes, instances of these classes are
  * accessible via the pointers systemInfo, geometryInfo,
  * commonPars, studyInfo and recoInfo
  */


///////////////////////////////////////////////////////////////////////////////////////


/**
  * \page odindata_doc ODIN data processing framework (odindata library)
  *
  * This page describes the design guidelines of the module  \ref odindata
  *
  * \section blitz4nmr Blitz++ for NMR
  * The Blitz++ library is great for numerical calculations, but it lacks
  * some functionality that is crucial when using it together with NMR data:
  * - Convenient input/output of medical image data
  * - Numerical routines essential for NMR (FFT, gridding, fitting, ...)
  * - Associating large data files on disk with arrays in memory
  *
  * For this reason, ODIN contains two classes that help dealing with these tasks:
  * - Data: A template class derived from the Blitz++ Array class that
  * has the ability to be associated with a raw data file for read and
  * write access. This is done by using the mmap function so that no extra
  * memory is allocated on contstruction. Instead, the task of reading/writing
  * is handled transparently by the operating system. Any changes made
  * to the array in memory are also visible in the file on disk. With this
  * mechanism, arrays that are larger than the available RAM can be handled
  * without extra effort.
  * In addition, member functions Data::autoread() and Data::autowrite() are available to load/store
  * the data in different file formats using the FileIO module (\ref fileio_doc ).
  *
  * - ComplexData: A template class with a
  * fixed storage type of complex numbers. It contains useful functions
  * to deal with NMR data (FFT,...). On construction, it can be
  * associated with raw data on disk (16bit, 32bit, float) and blocks
  * of the file can be loaded subsequently into memory for further
  * processing.
  *
  *
  * \section numerics Common Numerical Operations
  *
  * In order to offer a framework for other numerical calculations,
  * the following functions/classes are available within ODIN:
  *
  * \subsection numerics_gridding Gridding
  * Regridding an array from one Cartesian grid to another can be
  * done using the Data::congrid() member function.
  * In addition, the functor Gridding can be used to data from a
  * non-Cartesian grid to a Cartesian grid.
  *
  * \subsection numerics_transform Coordinate Transformation
  * Transforming an array to a new coordinate system by
  * rotation, scaling and shifting can be done by using
  * the CoordTransformation functor.
  *
  * \subsection numerics_fft Fourier Transform
  * FFT of a whole complex data set can be calculated by the ComplexData::fft() member function.
  * If the FFT has to be applied only for certain dimensions, use ComplexData::partial_fft()
  *
  * \subsection numerics_linalg Linear Algebra
  * The functions solve_linear(), pseudo_inverse() and eigenvalues() will solve a sets of linear equations,
  * calculate the pseudo inverse and eigenvalues, respectively.
  *
  * \subsection numerics_fitting Non-Linear Function Fitting
  * Fitting of functions is accomplished by the two classes
  * ModelFunction and FunctionFit. The former is
  * used as a base class to specify the modelling function
  * by implementing its virtual functions.
  * The latter is used to do the actual fitting and to
  * store temporary data of the fit.
  *
  * \subsection numerics_linfit Linear Regression
  * The class LinearFunction can be used for linear fitting.
  *
  * \subsection numerics_polfit Polynomial Fit
  * The function polyniomial_fit() provides pixel-wise fitting
  * of polynomials using the values of neighbouring pixels.
  *
  * \subsection numerics_integration Integration of Functions
  * Integration of one-dimensional functions can be achieved
  * by the two classes Integrand and FunctionIntegral.
  * The former is used as a base class to specify the function
  * by implementing a virtual functions.
  * The latter is used to do the actual integration and to
  * store temporary data.
  *
  * \subsection numerics_statistics Basic Statistics
  * Simple statistics (mean, standard deviation) can be
  * calculated by the statistics() function.
  *
  * \subsection numerics_correlation Correlation Analysis
  * Correlation of two vectors can be calculated by
  * the correlation() function.
  *
  * \subsection numerics_fileio Convenient File Input/Output
  * The functions inside FileIO (\ref fileio_doc ) are a convenient way ro
  * read/write 4-dim Arrays, a variety of formats common
  * for medical image are supported.
  *
  *
  *
  */

///////////////////////////////////////////////////////////////////////////////////////


/**
  * \page fileio_doc Input/Output of medical image data (odindata library)
  *
  * \section fileio A unified approach for handling medical image data
  * This page describes the functionality of the FileIO module.
  *
  * \subsection fileio_intro Introduction
  * When working with different systems for generating and processing of medical images very quickly one problem arises: These systems use different data formats and the available export features of any toolkit mostly support only a subset of the needed formats. The following report describes an approach to design a unified and extensible interface to medical image data. Implemented in an function library this interface can be used to efficently enable custom-built applications to read and write medical image data.
  *
  * \subsection fileio_pdmap The Protocol-Data-Map
  * Unified access to different data formats implies a common interface for accessing this data.
  * Thus a data structure, which provides a superset of all features of all data formats which shall be supported.
  * Every medical image consists of image data of any type and metadata related to this image data. This data pair will be dicussed in the following.
  *
  * The metadata
  * which belong to an image usually describe the technical and administrative characteristics of an image.
  * They consist of some required information like image dimension and used data type, some common information like the age of the subject or the date the image was taken and some optional information like used field strength for MR data. These parameters are either in a header inside the imagefile or in a separate header file. The required and the common parameter are very usefull to distinguish images. They, and with them the whole metadataset, can be used as a unique key to reference this image. In the following this paremeter set will be called the Protocol.
  *
  * Some imagefile formats are only capable of storing at least two-dimensional images. In this case N-dimensional datasets are often stored by using several two-dimensional images. The protocols of these images differ only in the remaining coordinates (which needs to be specified in this case). 
  * The system must recognize this and combine them into one N-dimensional dataset when reading. And the inverse approach appiles to writing N-dimensional data in a fileformat which does not support that many dimensions.
  * 
  * The pair of protocol and dataset can be used as a universal interface to access image data.
  * While the protocol offers all information to interpret the data, the dataset serves as a coordinate-based interface to the data for reading and writing.
  * 
  * Multiple protocol-dataset-pairs can efficiently be stored in a associative container where the protocol is the key which maps to the dataset. This enables the system to handle multiple datasets. A particular dataset can always be referenced by its protocol and a list of the available datasets can be obtained by iterating through this protocol-dataset-map (FileIO::ProtocolDataMap).
  * Also, when reading multiple low-dimensional image files which are part of an high-dimensional image, they all will have the same protocol and thus will be automaticly sorted into one high-dimensional dataset referenced by this protocol.
  * 
  * \subsection fileio_formats read and write different file formats
  * To guarantee the flexibility of the system, the input from and the output to files is done via plugins (classes derived from FileFormat) which use the described protocol-dataset-map as front end.
  * These plugins are gathered at the initialisation and held in a map with the file suffixes they support as key. If available, they are also referenced by a regular expression to detect the supported format directly from that data.
  * 
  * The selection of the right plugin is done in two ways. If a fileformat is directly requested, the corresponding plugin is used. If the format is not selected explicitly, the system at first tries to get the suffix of the file requested for read or write and guesses the format.
  * 
  * Many medical image formats exist in several different dialects or have optional extensions which makes it necessary to fine tune them. Thus every plugin can bring a set of additional parameters which than can be used by the client to modify their behavior.
  *
  *@author Enrico Reimer
  */


/**
  * @defgroup odindata Classes of the ODIN data processing framework (odindata library)
  *
  * \ref odindata_doc
  * \ref fileio_doc
  *
  */

///////////////////////////////////////////////////////////////////////////////////////

/**
  * @defgroup odinreco The reconstruction framework (odinreco)
  *
  * \ref odinreco_doc
  *
  */

///////////////////////////////////////////////////////////////////////////////////////

/**
  * @defgroup odinreco_steps Steps(functors) of the reconstruction framework (odinreco)
  *
  * These steps(functors) can be used within the \ref odinreco_doc
  *
  */

///////////////////////////////////////////////////////////////////////////////////////

/**
  * \page cmdline_utils Command-line utilities
  *
  * miconv: \ref miconv
  *
  * micalc: \ref micalc
  *
  * miview: \ref miview
  *
  * odinreco: \ref odinreco_usage
  *
  * gencoil: \ref gencoil
  *
  * genmakefile: \ref genmakefile
  *
  * gensample: \ref gensample
  *
  * swab: \ref swab
  *
  * For command-line-based simulation of Odin sequences (e.g. for sequence 'mysequence'):
  * - Create a Makefile using 'genmakefile' on the command line:
  *   \verbatim
       genmakefile mysequence > Makefile
      \endverbatim
  * - Copy mysequence.cpp to the same directory, then use 'make' to build the executable 'mysequence'
  * - Calling the executable without arguments shows the options. For instance, the simulation can be started with:
  *   \verbatim
       mysequence simulate -magsi -s <The virtual sample file> -p <The file with the measurement protocol> -scandir <output-dir>
      \endverbatim
  */

///////////////////////////////////////////////////////////////////////////////////////



/*! \mainpage ODIN Reference Manual
 *
 *
 * \section quick_start Quick Start
 *
 * I want to see documentation about ...
 *
 * \ref odin_doc
 *
 * \ref cmdline_utils
 *
 * \ref odinseq_doc
 *
 * \ref odinseq
 *
 * \ref odindata_doc
 *
 * \ref fileio_doc
 *
 * \ref odinreco_doc
 *
 * \ref odinreco_steps
 *
 * \ref tjutils_doc
 *
 * \ref odinpara_doc
 *
 *
 *@author Thies H. Jochimsen
 */




