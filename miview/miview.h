/***************************************************************************
                          miview.h  -  description
                             -------------------
    begin                : Thu Aug 31 16:27:06 CEST 2000
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef MIVIEW_H
#define MIVIEW_H

#include "miviewview.h"

/**
  * \page miview View medical images
  * \verbinclude miview/miview.usage
  *
  * \section miview_examples Some examples how to use miview:
  *
  * Display DICOM file 'image.dcm':
  *   \verbatim
      miview image.dcm
      \endverbatim
  *
  *
  * Display magnitude of float-type complex signal data stored in 'data.raw':
  *   \verbatim
      miview -rf float -abs data.raw
      \endverbatim
  *
  *
  * Display short (16 bit) raw data file '2dseq' of size 256x256
  *   \verbatim
      miview -nx 256 -ny 256 -rf short 2dseq 
      \endverbatim
  */

#define IDS_MIVIEW_ABOUT            "MiView user interface\nVersion " VERSION \
                                    "\n(w) 2000-2015 by Thies Jochimsen\n\n"


class MiView : public QObject, public GuiMainWindow {
 Q_OBJECT
  
 public:
  MiView();

 
 public slots:
  void slotHelpAbout();
  void slotStatusMsg(const char* text) {GuiMainWindow::set_status_message(text);}
  void quit();

 private:

  MiViewView *view;

  GuiPopupMenu* fileMenu;
  GuiPopupMenu* exportMenu;
  GuiPopupMenu* showMenu;
  GuiPopupMenu* helpMenu;

};

#endif
