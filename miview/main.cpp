/***************************************************************************
                          main.cpp  -  description
                             -------------------
    begin                : Thu Aug 31 16:27:06 CEST 2000
    copyright            : (C) 2000-2015 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "miview.h"

int main(int argc, char *argv[]) {

  // do this here so we do not have to query X server in order to get usage for manual
  if(argc<=1)   {MiViewView::usage();exit(0);}
  if(hasHelpOption(argc, argv)) {MiViewView::usage();exit(0);}

  char lastarg[ODIN_MAXCHAR];
  char parname[ODIN_MAXCHAR];
  STD_string parstring;
  parname[0]='\0';
  getLastArgument(argc,argv,lastarg,ODIN_MAXCHAR,false);
  if(getCommandlineOption(argc,argv,"-jdx",parname,ODIN_MAXCHAR,false)) {
    parstring=STD_string("::")+parname;
  }

  GuiApplication a(argc, argv);  // debug handler will be initialized here
  MiView* miview=new MiView();
  miview->set_caption((STD_string("MiView - ") + lastarg + parstring).c_str());
  return a.start(miview->get_widget());
}
