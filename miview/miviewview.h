/***************************************************************************
                          miviewview.h  -  description
                             -------------------
    begin                : Thu Jun 20 19:02:26 CEST 2002
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef MIVIEWVIEW_H
#define MIVIEWVIEW_H

#include <odinqt/ldrwidget.h>


#include <odinpara/odinpara.h>
#include <odinpara/ldrnumbers.h>
#include <odinpara/ldrarrays.h>
#include <odinpara/ldrblock.h>

// miview modules
#include "miview_fmri.h"


class Protocol; // forward declaration
struct FileData; // forward declaration
struct SelectionData; // forward declaration

////////////////////////////////////////

// for debugging MiView component
class MiViewComp {
 public:
  static const char* get_compName();
};

////////////////////////////////////////

struct MiViewOpts : LDRblock {

  LDRbool color;
  LDRfloat contrast;
  LDRfloat brightness;
  LDRstring valfile;
  LDRstring recfile;
  LDRint blowup;
  LDRstring low;
  LDRstring upp;
  LDRstring dump;
  LDRstring mapfile;
  LDRfloat maplow;
  LDRfloat mapupp;
  LDRfloat maprect;
  LDRstring legendexport;
  LDRstring maplegendexport;
  LDRbool noscale;

  MiViewOpts();
};

////////////////////////////////////////

class MiViewView : public QWidget {
 Q_OBJECT

 public:
  MiViewView(QWidget *parent);
  ~MiViewView();


  static void set_defaults(Protocol& prot);
  static void usage();

  bool is_image_display() const {return (get_nx()*get_ny())>1;}
  bool has_timecourse() const {return get_nrep()>1;}
  bool has_fmri() const {return fmri.is_valid();}
  bool has_overlay() const {return has_olm;}

 signals:

  void setMessage(const char* text);


 public slots:

  void writeData();
  void writeImage();
  void writeROIs();
  void writeFmriClusters();
  void writeProfile();
  void writeTimecourse();
  void writeLegend();
  void writeMapLegend();

  void selectVoxel();
  void showProfile();
  void showTimecourse();
  void showProtocol();


 private slots:

  void update();

  void slotClicked(int x, int y, int z);
  void slotNewProfile(const float *data, int npts, bool horizontal, int position);
  void slotNewMask(const float *data, int slice);


 private:

  int get_nx() const;
  int get_ny() const;
  int get_nz() const;
  int get_nrep() const;

  void filedata2displaydata();

  STD_string check_and_get_format(const STD_string& fname, const svector& possible_formats, bool show_error_msg=true);

  void export_legend(const STD_string& filename);
  void export_map_legend(const STD_string& filename);

  void val2file(int rep, int z, int y, int x, float val) const;


  MiViewOpts mopts;

  bool has_bounds; // true if set manually
  float lowbound;
  float uppbound;


  FileData* file; // holding the data of the file

  LDRfloatArr displaydata;
  GuiProps guiprops_cache;
  LDRblock settings;
  LDRint repetition;
  LDRfloatArr timecourse;

  bool has_olm;


  SelectionData* selection;  // holding data of user selections

  GuiGridLayout* grid;
  LDRwidget* displaywidget;
  LDRwidget* settingswidget;
  LDRwidget* tcoursewidget;
  LDRwidget* fmriwidget;

  STD_string data_fname_cache;
  STD_string image_fname_cache;
  STD_string rois_fname_cache;
  STD_string clusters_fname_cache;
  STD_string profile_fname_cache;
  STD_string tcourse_fname_cache;

  LDRint pos_cache[n_directions];


  // miview modules
  MiViewFmri fmri;


};


#endif
