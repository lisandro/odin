#include "miview.h"

MiView::MiView() {

  view=new MiViewView(GuiMainWindow::get_widget());
  connect(view, SIGNAL(setMessage(const char*)), this, SLOT  (slotStatusMsg(const char*)));


  fileMenu=new GuiPopupMenu(GuiMainWindow::get_widget());
  fileMenu->insert_item("&Quit", this, SLOT(quit()), Qt::CTRL+Qt::Key_Q);


  exportMenu=new GuiPopupMenu(GuiMainWindow::get_widget());
  showMenu=new GuiPopupMenu(GuiMainWindow::get_widget());


  showMenu->insert_item("Select Voxel", view, SLOT(selectVoxel()));

  if(view->is_image_display()) {
    exportMenu->insert_item("Save Data as ...", view, SLOT(writeData()));
    exportMenu->insert_item("Dump current display as image (screenshot)", view, SLOT(writeImage()));
    exportMenu->insert_item("Export current ROIs", view, SLOT(writeROIs()));
    exportMenu->insert_item("Export current profile as ASCII", view, SLOT(writeProfile()));
    if(view->has_timecourse()) exportMenu->insert_item("Export current timecourse as ASCII", view, SLOT(writeTimecourse()));

    showMenu->insert_item("Show current profile in Xmgr(ace)", view, SLOT(showProfile()));
    showMenu->insert_item("Show current timecourse in Xmgr(ace)", view, SLOT(showTimecourse()));
  }

  if(view->has_fmri()) {
    exportMenu->insert_item("Export current fMRI clusters", view, SLOT(writeFmriClusters()));
  }

  exportMenu->insert_item("Export legend", view, SLOT(writeLegend()));
  if(view->has_overlay()) {
    exportMenu->insert_item("Export overlay legend", view, SLOT(writeMapLegend()));
  }


  showMenu->insert_item("Show Protcol", view, SLOT(showProtocol()));


  helpMenu=new GuiPopupMenu(GuiMainWindow::get_widget());
  helpMenu->insert_item("About...", this, SLOT(slotHelpAbout()));


  GuiMainWindow::insert_menu("&File", fileMenu);
  GuiMainWindow::insert_menu("Ex&port", exportMenu);
  GuiMainWindow::insert_menu("S&how", showMenu);
  GuiMainWindow::insert_menu_separator();
  GuiMainWindow::insert_menu("&Help", helpMenu);

  GuiMainWindow::show(view);
}


void MiView::slotHelpAbout() {
  message_question(IDS_MIVIEW_ABOUT, "About...", GuiMainWindow::get_widget());
}

void MiView::quit() {
  GuiApplication::quit();
}
