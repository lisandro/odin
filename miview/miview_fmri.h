/***************************************************************************
                          miview_fmri.h  -  description
                             -------------------
    begin                : Mon Jan 2 19:02:26 CEST 2006
    copyright            : (C) 2000-2015 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef MIVIEW_FMRI_H
#define MIVIEW_FMRI_H

#include <odinpara/ldrnumbers.h>
#include <odinpara/ldrtypes.h>
#include <odinpara/ldrblock.h>
#include <odinpara/ldrarrays.h>

// forward declarations
class FileReadOpts;
class Protocol;
class FilterChain;
struct FmriData;

////////////////////////////////////////////////////////////

class MiViewFmri : public LDRblock {

 public:
  MiViewFmri();
  ~MiViewFmri();

  bool init(const FileReadOpts& ropts, const Protocol& prot, const FilterChain& filterchain);

  bool is_valid() const {return valid;}

  const farray& get_overlay_map() const;
  bool write_overlay_map(const STD_string& filename);

  void update();

 private:
  LDRstring designfile;
  LDRstring fmrifile;
  LDRstring maskfile;
  LDRbool bonferr;
  LDRbool hrf;
  LDRfloat corr;
  LDRfloat zscore;
  LDRint designavg;
  LDRint neighb;
  LDRstring sigchange;
  LDRfloat zsum;
  LDRint zcount;
  LDRstring zaverage;
  LDRfloatArr design;
  LDRfloatArr tcourse;
  LDRfloatArr sigcourse;
  LDRstring dumptcourse;
  LDRstring dumpzmap;
  LDRstring dumpsmap;

  FmriData* data;
  mutable farray overlay_map;
  mutable farray toberemoved;
  bool valid;

};





#endif


