#include "miviewview.h"

#include <odinpara/ldrtypes.h>
#include <odinpara/protocol.h>

#include <odindata/fileio.h>
#include <odindata/statistics.h>
#include <odindata/utils.h>
#include <odindata/filter.h>

#include <odinqt/ldrblockwidget.h> // for showProtocol

#include <tjutils/tjlog_code.h>


const char* MiViewComp::get_compName() {return "MiView";}
LOGGROUNDWORK(MiViewComp)


/////////////////////////////////////////////////////////////////

struct FileData {
  Data<float,4> data;
  Protocol prot;
  statisticResult stat;
};

/////////////////////////////////////////////////////////////////

struct SelectionData {
  Data<float,4> roi; // 4D to be suitable for autowrite
  Data<float,1> profile;
};


/////////////////////////////////////////////////////////////////

MiViewOpts::MiViewOpts() {

  color=false;
  color.set_cmdline_option("color").set_description("Use color map to display values");
  append_member(color,"color");

  contrast=0.0;
  contrast.set_minmaxval(-100, 100);
  contrast.set_cmdline_option("contrast").set_description("Relative contrast of display");
  append_member(contrast,"Contrast");

  brightness=0.0;
  brightness.set_minmaxval(-100, 100);
  brightness.set_cmdline_option("bright").set_description("Relative brightness of display");
  append_member(brightness,"Brightness");

  valfile.set_cmdline_option("val").set_description("Save value of ROI/point selection to this file");
  append_member(valfile,"valfile");

  recfile.set_cmdline_option("rec").set_description("Record clicked coordinates and values into this file");
  append_member(recfile,"recfile");

  blowup=0;
  blowup.set_cmdline_option("blowup").set_description("Enlarge display size by this factor (0=automatic)");
  append_member(blowup,"blowup");

  low.set_cmdline_option("low").set_description("Lower windowing boundary: This value will appear black in display");
  append_member(low,"low");

  upp.set_cmdline_option("upp").set_description("Upper windowing boundary: This value will appear white in display");
  append_member(upp,"upp");

  dump.set_cmdline_option("dump").set_description("Dump all images as graphic files and exit, use the given filename postfix to specify the format");
  append_member(dump,"dump");

  mapfile.set_cmdline_option("map").set_description("Load overlay map (colored voxels superimposed on image) from this file");
  append_member(mapfile,"mapfile");

  maplow=0.0;
  maplow.set_cmdline_option("maplow").set_description("Lower windowing boundary for overlay map");
  append_member(maplow,"maplow");

  mapupp=0.0;
  mapupp.set_cmdline_option("mapupp").set_description("Upper windowing boundary for overlay map");
  append_member(mapupp,"mapupp");

  maprect=0.6;
  maprect.set_cmdline_option("maprect").set_description("Relative size of rectangles which represent voxels of the overlay map");
  append_member(maprect,"maprect");

  maplegendexport.set_cmdline_option("maplegend").set_description("Export map legend as bitmap to this file");
  append_member(maplegendexport,"maplegendexport");

  legendexport.set_cmdline_option("legend").set_description("Export legend as bitmap to this file");
  append_member(legendexport,"legendexport");

  noscale=false;
  noscale.set_cmdline_option("noscale").set_description("Disable scale in 2D/3D display");
  append_member(noscale,"noscale");

}


/////////////////////////////////////////////////////////////////

void show_in_xmgr(const Data<float,1>& data) {
  STD_string tmpfile="/tmp/miview"+itos(int(current_time_s()))+".asc";
  data.write_asc_file(tmpfile);
  STD_list<STD_string> execs2try;
  execs2try.push_back("xmgrace");
  execs2try.push_back("xmgr");
  for(STD_list<STD_string>::const_iterator it=execs2try.begin(); it!=execs2try.end(); ++it) {
    if(!system(STD_string((*it)+" "+tmpfile+" && rm "+tmpfile+" &").c_str())) break;
  }
}


/////////////////////////////////////////////////////////////////

void MiViewView::usage() {
  Protocol prot;
  MiViewOpts mopts;
  FileReadOpts ropts;
  FilterChain filter;
  MiViewFmri fmri;
  set_defaults(prot);

  STD_cout << "miview: Viewer for medical image files" << STD_endl;
  STD_cout << "        File formats are automatically identified by their file extension." << STD_endl;
  STD_cout << "Usage: miview [ options ] <image-file>" << STD_endl;

  STD_cout << "Global options:" << STD_endl;
  STD_cout << mopts.get_cmdline_usage("\t");
  STD_cout << "\t" << LogBase::get_usage() << STD_endl;

  STD_cout << "fMRI options (Give at least -design and -fmri to activate):" << STD_endl;
  STD_cout << fmri.get_cmdline_usage("\t");

  STD_cout << "File read options:" << STD_endl;
  STD_cout << prot.get_cmdline_usage("\t");
  STD_cout << ropts.get_cmdline_usage("\t");

  STD_cout << "Filters:" << STD_endl;
  STD_cout << filter.get_cmdline_usage("\t");

  STD_cout << "Supported file extensions(formats):" << STD_endl;
  STD_cout << FileIO::autoformats_str("\t") << STD_endl;
}

void MiViewView::set_defaults(Protocol& prot) {
  prot.seqpars.set_MatrixSize(readDirection,1);
  prot.seqpars.set_MatrixSize(phaseDirection,1);
  prot.seqpars.set_MatrixSize(sliceDirection,1);
  prot.set_parmode(noedit);
}


MiViewView::MiViewView(QWidget *parent) : QWidget(parent) {
  Log<MiViewComp> odinlog("MiViewView","MiViewView");

  char optval[ODIN_MAXCHAR];

  file=new FileData();

  set_defaults(file->prot);
  file->prot.parse_cmdline_options(GuiApplication::argc(),GuiApplication::argv());

  FileReadOpts ropts;
  ropts.parse_cmdline_options(GuiApplication::argc(),GuiApplication::argv());

  mopts.parse_cmdline_options(GuiApplication::argc(),GuiApplication::argv());

  fmri.parse_cmdline_options(GuiApplication::argc(),GuiApplication::argv());

  FilterChain filterchain(GuiApplication::argc(),GuiApplication::argv());

  LDRfileName fname;
  if(getLastArgument(GuiApplication::argc(),GuiApplication::argv(),optval,ODIN_MAXCHAR)) {
    fname=optval;
  } else {
    usage(); exit(0);
  }
  if(fname=="") {
    usage(); exit(0);
  }

  ProgressDisplayConsole display;
  ProgressMeter progmeter(display);

  if(file->data.autoread(fname,ropts,&(file->prot),&progmeter)<=0) exit(-1);
  ODINLOG(odinlog,normalDebug) << "mem(read): " << Profiler::get_memory_usage() << STD_endl;

  if(!filterchain.apply(file->prot, file->data)) exit(-1);

  file->stat=statistics(file->data);
  ODINLOG(odinlog,infoLog) << "data = " << file->stat << STD_endl;



  has_bounds=false;
  lowbound=file->stat.min;
  uppbound=file->stat.max;
  if(mopts.low!="") {
    lowbound=atof(mopts.low.c_str());
    has_bounds=true;
  }
  if(mopts.upp!="") {
    uppbound=atof(mopts.upp.c_str());
    has_bounds=true;
  }
  if(uppbound<=lowbound) has_bounds=false; // check input
  ODINLOG(odinlog,normalDebug) << "lowbound/uppbound/has_bounds=" << lowbound << "/" << uppbound << "/" << has_bounds << STD_endl;

  if(mopts.blowup>0) guiprops_cache.pixmap.minsize=mopts.blowup*STD_min(get_nx(), get_ny());
  else               guiprops_cache.pixmap.minsize=256;
  guiprops_cache.fixedsize=false;
  guiprops_cache.pixmap.autoscale=false;
  guiprops_cache.pixmap.color=mopts.color;
  guiprops_cache.pixmap.overlay_rectsize=mopts.maprect;
  guiprops_cache.pixmap.overlay_minval=mopts.maplow;
  guiprops_cache.pixmap.overlay_maxval=mopts.mapupp;

  ODINLOG(odinlog,normalDebug) << "guiprops.pixmap.minsize=" << guiprops_cache.pixmap.minsize << STD_endl;

  has_olm=false;
  if(mopts.mapfile!="") {
    Data<float,4> mapdata;
    if(mapdata.autoread(mopts.mapfile,ropts)>0) {
      Protocol protdummy;
      if(!filterchain.apply(protdummy, mapdata)) exit(-1);
      guiprops_cache.pixmap.overlay_map=mapdata;
      has_olm=true;
//      gp.pixmap.overlay_map.normalize();
    }
  }


  fmriwidget=0;
  if(fmri.init(ropts,file->prot,filterchain)) {
    guiprops_cache.pixmap.overlay_map=fmri.get_overlay_map(); // must be present during construction of displaywidget
    guiprops_cache.pixmap.overlay_firescale=true;
    fmri.update();
    fmri.set_label("fMRI");
    fmriwidget=new LDRwidget(fmri, 1, this);
    connect(fmriwidget,SIGNAL(valueChanged()),this,SLOT(update()));
  }


  displaydata.redim(get_nz(),get_ny(),get_nx());

  filedata2displaydata(); // will set gui_props of displaydata


  displaydata.set_label("Data Plot");
  displaywidget=new LDRwidget(displaydata, 1, this);

  export_legend(mopts.legendexport); // Do this after displaywidget is initialized, bur before dump. String will be checked in export_map_legend()
  export_map_legend(mopts.maplegendexport); // Do this after displaywidget is initialized, bur before dump. String will be checked in export_map_legend()

  if(mopts.dump!="") {

    STD_string dumpfname(mopts.dump);

    STD_string format=check_and_get_format(mopts.dump, get_possible_image_fileformats(), false);
    if(format=="") {
      format="bmp"; // default
      dumpfname+="."+format;
      ODINLOG(odinlog,warningLog) << "Unknown file format, using format" << format << STD_endl;
    }

    ODINLOG(odinlog,infoLog) << "Dumping images with filename " << dumpfname << " and format " << format << STD_endl;
    displaywidget->write_pixmap(dumpfname.c_str(), format.c_str(), true);
    exit(0);
  }


  connect(displaywidget,SIGNAL(clicked(int,int,int)),                     this,SLOT(slotClicked(int,int,int)));
  connect(displaywidget,SIGNAL(newProfile(const float*, int, bool, int)), this, SLOT(slotNewProfile(const float*, int, bool, int)));
  connect(displaywidget,SIGNAL(newMask(const float*, int)),               this, SLOT(slotNewMask(const float*, int)));


  if(has_timecourse()) {
    repetition.set_label("Repetition");
    repetition.set_minmaxval(0, get_nrep()-1);
    settings.append(repetition);
  }

  if(is_image_display() && !has_bounds) {
    settings.append(mopts.contrast);
    settings.append(mopts.brightness);
  }

  settingswidget=0;
  int n_settings=settings.numof_pars();
  ODINLOG(odinlog,normalDebug) << "n_settings=" << n_settings << STD_endl;
  if(n_settings) {
    settings.set_label("Display Settings");
    settingswidget=new LDRwidget(settings, 1, this);
    connect(settingswidget,SIGNAL(valueChanged()),this,SLOT(update()));
  }

  tcoursewidget=0;
  if(has_timecourse()) {
    GuiProps tcgp;
    tcgp.fixedsize=false;
    timecourse.set_gui_props(tcgp);
    timecourse.set_label("Timecourse");
    timecourse.redim(get_nrep());
    tcoursewidget=new LDRwidget(timecourse, 1, this);
  }

  int ncols=1;
  int nrows=2;
  if(settingswidget) ncols++;
  if(tcoursewidget) nrows++;
  grid=new GuiGridLayout( this, nrows, ncols);

  grid->add_widget(displaywidget, 0, 0, GuiGridLayout::Default, 2, 1);
  if(settingswidget) grid->add_widget(settingswidget, 0, 1);
  if(fmriwidget)     grid->add_widget(fmriwidget,     1, 1);
  if(tcoursewidget) grid->add_widget(tcoursewidget, 2, 0, GuiGridLayout::Default, 1, 2);

  selection=new SelectionData();
  selection->roi.resize(1,get_nz(),get_ny(),get_nx());
  selection->roi=0.0;

  if(mopts.recfile!="") rmfile(mopts.recfile.c_str());

}

void MiViewView::writeData() {
  data_fname_cache=get_save_filename("Data File", data_fname_cache.c_str(), "", this);
  if(data_fname_cache=="") return;
  STD_string format=check_and_get_format(data_fname_cache,FileIO::autoformats());
  if(format!="") {
    file->data.autowrite(data_fname_cache, FileWriteOpts(), &(file->prot));
  }
}

void MiViewView::writeImage() {
  image_fname_cache=get_save_filename("Image File for Screen Dump", image_fname_cache.c_str(), "", this);
  if(image_fname_cache=="") return;
  STD_string format=check_and_get_format(image_fname_cache,get_possible_image_fileformats());
  if(format!="") {
    displaywidget->write_pixmap(image_fname_cache.c_str(), format.c_str());
  }
}

void MiViewView::writeROIs() {
  Log<MiViewComp> odinlog("MiViewView","writeROIs");
  rois_fname_cache=get_save_filename("Data File for ROIs", rois_fname_cache.c_str(), "", this);
  if(rois_fname_cache=="") return;
  STD_string format=check_and_get_format(rois_fname_cache,FileIO::autoformats());
  if(format!="") {
    selection->roi.autowrite(rois_fname_cache);
  }
}

void MiViewView::writeFmriClusters() {
  Log<MiViewComp> odinlog("MiViewView","writeFmriClusters");
  clusters_fname_cache=get_save_filename("Data File for fMRI clusters", clusters_fname_cache.c_str(), "", this);
  if(clusters_fname_cache=="") return;
  STD_string format=check_and_get_format(clusters_fname_cache,FileIO::autoformats());
  if(format!="") {
    fmri.write_overlay_map(clusters_fname_cache);
/*
    farray olmap(fmri.get_overlay_map());
    Data<float,4> maskdata(1,olmap.size(0),olmap.size(1),olmap.size(2)); maskdata=0.0;
    for(unsigned int i=0; i<olmap.total(); i++) {
      if(olmap[i]>0.0) maskdata(maskdata.create_index(i))=1.0;
    }
    maskdata.autowrite(clusters_fname_cache);
*/
  }
}

void MiViewView::writeProfile() {
  profile_fname_cache=get_save_filename("ASCII File for Profile", profile_fname_cache.c_str(), "", this);
  if(profile_fname_cache=="") return;
  selection->profile.write_asc_file(profile_fname_cache);
}

void MiViewView::writeTimecourse() {
  tcourse_fname_cache=get_save_filename("ASCII File for Timecourse", tcourse_fname_cache.c_str(), "", this);
  if(tcourse_fname_cache=="") return;
  Data<float,1> tcourse(timecourse);
  tcourse.write_asc_file(tcourse_fname_cache);
}


void MiViewView::export_legend(const STD_string& filename) {
  Log<MiViewComp> odinlog("MiViewView","export_legend");
  if(filename=="") return;
  STD_string format=check_and_get_format(filename,get_possible_image_fileformats());
  if(format!="") {
    ODINLOG(odinlog,infoLog) << "Writing legend of format " << format <<  " to file " << filename << STD_endl;
    displaywidget->write_legend(filename.c_str(), format.c_str());
  }
}


void MiViewView::export_map_legend(const STD_string& filename) {
  Log<MiViewComp> odinlog("MiViewView","export_map_legend");
  if(filename=="") return;
  STD_string format=check_and_get_format(filename,get_possible_image_fileformats());
  if(format!="") {
    ODINLOG(odinlog,infoLog) << "Writing map legend of format " << format <<  " to file " << filename << STD_endl;
    displaywidget->write_map_legend(filename.c_str(), format.c_str());
  }
}


void MiViewView::writeLegend() {
  export_legend(get_save_filename("File for Legend", "", "", this));
}

void MiViewView::writeMapLegend() {
  export_map_legend(get_save_filename("File for Map Legend", "", "", this));
}

void MiViewView::selectVoxel() {

  LDRblock block;
  for(int idir=(n_directions-1); idir>=0; idir--) { // display reversed in dialog
    pos_cache[idir].set_label(directionLabel[idir]);
    pos_cache[idir].set_description("Voxel index in "+STD_string(directionLabel[idir])+" direction").set_unit("pixel");
    block.append(pos_cache[idir]);
  }

  new LDRwidgetDialog(block,n_directions,this,true);

  slotClicked(pos_cache[readDirection], pos_cache[phaseDirection], pos_cache[sliceDirection]);

  displaywidget->updateWidget();
}


void MiViewView::showProfile() {
  show_in_xmgr(selection->profile);
}

void MiViewView::showTimecourse() {
  show_in_xmgr(timecourse);
}

void MiViewView::showProtocol() {
  file->prot.set_parmode(noedit);
  new LDRwidgetDialog(file->prot,2,this,false,true);
}


STD_string MiViewView::check_and_get_format(const STD_string& fname, const svector& possible_formats, bool show_error_msg) {
  Log<MiViewComp> odinlog("MiViewView","check_and_get_format");
  STD_string result;
  STD_string format=LDRfileName(fname).get_suffix();
  for(unsigned int ifmt=0; ifmt<possible_formats.size(); ifmt++) if(possible_formats[ifmt]==format) result=format;
  if(show_error_msg && result=="") {
    STD_string msg=justificate("File format/extension \'"+format+"\' is not supported. Possible formats are:\n"+possible_formats.printbody(), 0, false, _DEFAULT_LINEWIDTH_/2);
    message_question(msg.c_str(), "Invalid File Format", this, false, true);
  }
  return result;
}





void MiViewView::update() {
  Log<MiViewComp> odinlog("MiViewView","update");
  filedata2displaydata();
  displaywidget->updateWidget();
  if(fmriwidget) fmriwidget->updateWidget();
}


void MiViewView::slotClicked(int x, int y, int z) {
  Log<MiViewComp> odinlog("MiViewView","slotClicked");
  ODINLOG(odinlog,normalDebug) << "x/y/z=" << x << "/" << y << "/" << z << STD_endl;

  ODINLOG(odinlog,normalDebug) << "get_nx/ny/nz()=" << get_nx() << "/" << get_ny() << "/" << get_nz() << STD_endl;

  if(x<0 || x>=get_nx()) return;
  if(y<0 || y>=get_ny()) return;
  if(z<0 || z>=get_nz()) return;

  timecourse=Data<float,1>(file->data(Range::all(),z,y,x));
  if(tcoursewidget) tcoursewidget->updateWidget();

  float val=file->data(int(repetition),z,y,x);

  STD_string msg="f(";
  if(get_nz()>1) msg+=itos(z)+",";
  msg+=itos(y)+","+itos(x)+")="+ftos(val);

  ODINLOG(odinlog,normalDebug) << "msg=" << msg << STD_endl;
  setMessage(msg.c_str());

  val2file(repetition,z,y,x,val);
}


void MiViewView::slotNewProfile(const float *data, int npts, bool horizontal, int position) {
  Log<MiViewComp> odinlog("MiViewView","slotNewProfile");
  if(!data) return;

  selection->profile.resize(npts);
  selection->profile=Array<float,1>((float*)data,npts,neverDeleteData);

  ODINLOG(odinlog,normalDebug) << "profile=" << selection->profile << STD_endl;


  STD_string msg;
  if(horizontal) msg="y=";
  else           msg="x=";
  msg+=itos(position);
  setMessage(msg.c_str());
}


void MiViewView::slotNewMask(const float *data, int slice) {
  Log<MiViewComp> odinlog("MiViewView","slotNewMask");
  if(!data) return;

  Range all=Range::all();

  Array<float,2> oneslicemask((float*)data, TinyVector<int,2>(get_ny(),get_nx()), neverDeleteData);

  if(sum(oneslicemask)<=0.0) return;

  selection->roi(0,slice,all,all)=oneslicemask;

  if(tcoursewidget) {
    for(int irep=0; irep<get_nrep(); irep++) {
      statisticResult repstats=statistics(file->data(irep,slice,all,all),&oneslicemask);
      timecourse[irep]=repstats.mean;
    }
    tcoursewidget->updateWidget();
  }

  statisticResult roistats=statistics(file->data(int(repetition),slice,all,all),&oneslicemask);
  STD_string msg="f(ROI)="+ftos(roistats.mean)+"+/-"+ftos(roistats.meandev);
  setMessage(msg.c_str());

  val2file(repetition,slice,-1,-1,roistats.mean);
}



MiViewView::~MiViewView() {
  if(tcoursewidget) delete tcoursewidget;
  if(settingswidget) delete settingswidget;
  if(fmriwidget) delete fmriwidget;
  delete displaywidget;
  delete grid;
  delete file;
  delete selection;
}


int MiViewView::get_nx() const {return file->data.extent()(3);}
int MiViewView::get_ny() const {return file->data.extent()(2);}
int MiViewView::get_nz() const {return file->data.extent()(1);}
int MiViewView::get_nrep() const {return file->data.extent()(0);}



void MiViewView::filedata2displaydata() {
  Log<MiViewComp> odinlog("MiViewView","filedata2displaydata");
  Range all=Range::all();

  if(is_image_display()) {
    if(has_bounds) {

      displaydata=Data<float,3>( (file->data(repetition,all,all,all)-lowbound)/(uppbound-lowbound) );

    } else {
      float min=file->stat.min;
      float delta=file->stat.max-file->stat.min;
      ODINLOG(odinlog,normalDebug) << "min/max/delta=" << file->stat.min << "/" << file->stat.max << "/" << delta << STD_endl;

      if(delta<=0.0) {displaydata=LDRfloat(min); return;}

      float brightoffset=mopts.brightness/100.0-0.5;
      float contrfactor=pow(10.0,mopts.contrast/100.0);

      displaydata=Data<float,3>( ((file->data(repetition,all,all,all)-min)/delta+brightoffset)*contrfactor+0.5 );

      lowbound=(-1.0/(2.0*contrfactor)-brightoffset)*delta+min;
      uppbound=(+1.0/(2.0*contrfactor)-brightoffset)*delta+min;

    }

    if(fmri.is_valid()) {
      guiprops_cache.pixmap.overlay_map=fmri.get_overlay_map();
      fmri.update();
    }

  } else {
    displaydata=Data<float,3>(file->data(repetition,all,all,all));
  }

  ODINLOG(odinlog,normalDebug) << "lowbound/uppbound=" << lowbound << "/" << uppbound << STD_endl;

  guiprops_cache.scale[displayScale].minval=lowbound;
  guiprops_cache.scale[displayScale].maxval=uppbound;
  guiprops_cache.scale[displayScale].enable=!mopts.noscale;

  displaydata.set_gui_props(guiprops_cache);

}

void MiViewView::val2file(int rep, int z, int y, int x, float val) const {
  if(mopts.valfile!="") {
    ::write(ftos(val),mopts.valfile);
  }
  if(mopts.recfile!="") {
    STD_string linestr=itos(rep)+","+itos(z)+","+itos(y)+","+itos(x)+"\t "+ftos(val)+"\n";
    ::write(linestr,mopts.recfile, appendMode);
  }

}
