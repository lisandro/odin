#include "miview_fmri.h"
#include "miviewview.h" // for Log

#include <odindata/correlation.h>
#include <odindata/filter.h>


float glover_hrf(float t) {
  if(t<=0.0) return 0.0;

  const double a1=6;
  const double a2=12;
  const double b=0.9;
  const double c=0.35;
  const double d1=5.4;
  const double d2=10.8;

  return pow(t/d1,a1)*exp(-(t-d1)/b) - c * pow(t/d2,a2)*exp(-(t-d2)/b);
}


/////////////////////////////////////////////////////////////////

struct FmriData {
  unsigned int brainvoxels;
  Data<float,4> fmri;
  Data<float,4> mask;
  Data<float,4> pmap;
  Data<float,4> zmap;
  Data<float,4> smap;
  Data<float,1> designdata;
  bool has_mask;
  Protocol protcopy; // writable copy
};

/////////////////////////////////////////////////////////////////

MiViewFmri::MiViewFmri() {

  valid=false;

  data=new FmriData();

  designfile.set_parmode(hidden).set_cmdline_option("design").set_description("Load fMRI design from this file (comma or space separated)");
  append_member(designfile,"designfile");

  fmrifile.set_parmode(hidden).set_cmdline_option("fmri").set_description("Load fMRI data from this file");
  append_member(fmrifile,"fmrifile");

  maskfile.set_parmode(hidden).set_cmdline_option("fmask").set_description("fMRI mask file");
  append_member(maskfile,"maskfile");

  bonferr=false;
  bonferr.set_cmdline_option("bonferr").set_description("Use Bonferroni correction");
  append_member(bonferr,"Bonferroni Correction");

  designavg=0;
  designavg.set_parmode(hidden).set_cmdline_option("davg").set_description("Smooth the design function using a moving average filter of width N (TR)");
  append_member(designavg,"Moving Average");

  hrf=false;
  hrf.set_parmode(hidden).set_cmdline_option("hrf").set_description("Convolve design function by hemodynamic response function prior to correlation (see Glover NeuroImage 9, 416-429)");
  append_member(hrf,"hrf");

  corr=0.05;
  corr.set_cmdline_option("corr").set_description("Error probability threshold for correlation");
  append_member(corr,"Error Probability");

  zscore=0.0;
  zscore.set_cmdline_option("zscore").set_description("z-Score threshold for correlation");
  append_member(zscore,"z-Score");

  neighb=1;
  neighb.set_cmdline_option("neighb").set_description("Minimum next neighbours with significant activation");
  append_member(neighb,"Min Neighbours");

  sigchange.set_parmode(noedit).set_description("fMRI signal change, averaged over all significant voxels");
  append_member(sigchange,"Signal Change");
  
  zsum.set_parmode(noedit).set_description("Sum of the z-scores of all significant voxels");
  append_member(zsum,"z-Sum");

  zcount.set_parmode(noedit).set_description("Count of all significant voxels");
  append_member(zcount,"z-Count");
  
  zaverage.set_parmode(noedit).set_description("Average z-score of all significant voxels");
  append_member(zaverage,"z-Average");

  design.set_description("fMRI design vector");
  append_member(design,"fMRI Design");

  tcourse.set_description("average signal in activated voxels");
  append_member(tcourse,"fMRI Timecourse");

  GuiProps gp;
  gp.scale[yPlotScaleLeft]=ArrayScale("Change","%");
  sigcourse.set_gui_props(gp);
  sigcourse.set_description("fMRI signal change time course");
  append_member(sigcourse,"Relative fMRI Timecourse");

  dumptcourse.set_parmode(hidden).set_cmdline_option("scourse").set_description("Dump relative fMRI signal change time course to this file");
  append_member(dumptcourse,"dumptcourse");

  dumpzmap.set_parmode(hidden).set_cmdline_option("zmap").set_description("Dump z-score map to this file");
  append_member(dumpzmap,"dumpzmap");

  dumpsmap.set_parmode(hidden).set_cmdline_option("smap").set_description("Dump map of relative fMRI signal change to this file");
  append_member(dumpsmap,"dumpsmap");
}


MiViewFmri::~MiViewFmri() {
  delete data;
}


bool MiViewFmri::init(const FileReadOpts& ropts, const Protocol& prot, const FilterChain& filterchain) {
  Log<MiViewComp> odinlog("MiViewFmri","init");
  Range all=Range::all();


  if(fmrifile=="") return false;
  ODINLOG(odinlog,infoLog) << "Loading fMRI data ..." << STD_endl;
  if(data->fmri.autoread(fmrifile,ropts,&(data->protcopy))<=0) {
    ODINLOG(odinlog,errorLog) << "Unable to load fmrifile " << fmrifile << STD_endl;
    return false;
  }
  if(!filterchain.apply(data->protcopy, data->fmri)) return false;

  // read and parse design file
  if(designfile=="") return false;
  STD_string designstr;
  if(::load(designstr,designfile)<0) {
    ODINLOG(odinlog,errorLog) << "Unable to load designfile " << designfile << STD_endl;
    return false;
  }
  designstr=replaceStr(designstr,","," ");
  svector designvecstr=tokens(designstr);
  int nrep=designvecstr.size();
  design.resize(nrep);
  tcourse.resize(nrep);
  sigcourse.resize(nrep);
  data->designdata.resize(nrep);
  for(int i=0; i<nrep; i++) {
    design[i]=atof(designvecstr[i].c_str());
  }

  if(hrf) {
    float tr=data->protcopy.seqpars.get_RepetitionTime();
    ODINLOG(odinlog,infoLog) << "Convolving with HRF and TR=" << tr << STD_endl;
    fvector designcopy(design);
    design=LDRfloat(0.0);
    for(int i=0; i<nrep; i++) {
      for(int j=i; j<nrep; j++) {
        design(j)+=designcopy[i]*glover_hrf((j-i)*0.001*tr);
      }
    }
  }

  if(designavg>0) {
    ODINLOG(odinlog,infoLog) << "Smoothing design function with moving average filter of width=" << designavg << "(TR)" << STD_endl;
    int WidthHalf = designavg / 2;
    fvector designcopy(design);
    for(int i=0; i<nrep; i++) {
      float count = 0;
      design(i) = 0.0;
      for(int j=i-WidthHalf; j<i+WidthHalf; j++) {
        if((j>=0) && (j<nrep)) {
          design(i)+=designcopy[j];
          count++;
        }
      }
      design(i) /= count;
    }
  }

  data->designdata=design;


  Protocol maskprot(data->protcopy); // local copy for mask
  
  data->has_mask=false;
  if(maskfile!="") {
    ODINLOG(odinlog,infoLog) << "Loading fMRI mask ..." << STD_endl;
    if(data->mask.autoread(maskfile,ropts,&maskprot)<=0) {
      ODINLOG(odinlog,errorLog) << "Unable to load maskfile " << maskfile << STD_endl;
      return false;
    }

    if(!filterchain.apply(maskprot, data->mask)) return false;

    TinyVector<int,4> fmrishape=data->fmri.shape(); fmrishape(0)=1;
    TinyVector<int,4> maskshape=data->mask.shape(); maskshape(0)=1;

    if(fmrishape!=maskshape) {
      ODINLOG(odinlog,errorLog) << "Shape mismatch:  fmrishape/maskshape" << fmrishape << "/" << maskshape << STD_endl;
      return false;
    }

    data->has_mask=true;
  }
  

  int nrep_data=data->fmri.extent(0);
  if(nrep_data!=nrep) {
    ODINLOG(odinlog,errorLog) << "Repetition size mismatch: " << nrep_data << "!=" << nrep << STD_endl;
    return false;
  }

  if(!nrep) {
    return false;
  }


  int nz=data->fmri.extent(1);
  int ny=data->fmri.extent(2);
  int nx=data->fmri.extent(3);

  data->pmap.resize(1,nz,ny,nx);
  data->zmap.resize(1,nz,ny,nx);
  data->smap.resize(1,nz,ny,nx);
  data->brainvoxels=0;
  overlay_map.redim(nz,ny,nx);
  toberemoved.redim(nz,ny,nx);



  Data<float,1> onepixel(nrep);
  double sigthreshold=0.5*mean(data->fmri);
  for(int iz=0; iz<nz; iz++) {
    for(int iy=0; iy<ny; iy++) {
      for(int ix=0; ix<nx; ix++) {
        onepixel=data->fmri(all,iz,iy,ix);
        float p=0.0;
        float z=0.0;
//        float sigchange=0.0;
        if(mean(onepixel)>sigthreshold) {
          correlationResult corr=correlation(data->designdata, onepixel);
          if(corr.r>0.0) { // take only positivie correlation
            p=corr.p;
            z=corr.z;
//            sigchange=fmri_eval(onepixel, data->designdata).rel_diff;
          }
          data->brainvoxels++;
        }
        data->pmap(0,iz,iy,ix)=p;
        data->zmap(0,iz,iy,ix)=z;
//        data->smap(0,iz,iy,ix)=sigchange;
        data->smap(0,iz,iy,ix)=fmri_eval(onepixel, data->designdata).rel_diff;
      }
    }
  }
  ODINLOG(odinlog,infoLog) << "Correlation analysis with " << data->brainvoxels << " voxels" << STD_endl;

  valid=true;

  return true;
}



const farray& MiViewFmri::get_overlay_map() const {
  int nz=data->pmap.extent(1);
  int ny=data->pmap.extent(2);
  int nx=data->pmap.extent(3);

  overlay_map=0.0; // reset

  // Bonferroni correction
  float corrthresh=corr;
  unsigned int total_pixels=data->brainvoxels;
  if(bonferr && total_pixels>0) {
    corrthresh=1.0-pow(1.0-corr,1.0/float(total_pixels));
  }

  for(int iz=0; iz<nz; iz++) {
    for(int iy=0; iy<ny; iy++) {
      for(int ix=0; ix<nx; ix++) {
        float p=data->pmap(0,iz,iy,ix);
        float z=data->zmap(0,iz,iy,ix);
        bool include=(p<corrthresh && z>zscore);
        if(data->has_mask && data->mask(0,iz,iy,ix)<=0.0) include=false;
        if(include) overlay_map(iz,iy,ix)=data->zmap(0,iz,iy,ix);
      }
    }
  }


  // next-neighbour analysis
  if(neighb>0) {
    for(int iz=0; iz<nz; iz++) {
      for(int iym=0; iym<ny; iym++) {
        for(int ixm=0; ixm<nx; ixm++) {

          int neighbours=0;
          for(int iyc=iym-1; iyc<=iym+1; iyc++) {
            for(int ixc=ixm-1; ixc<=ixm+1; ixc++) {

              if( iyc!=iym || ixc!=ixm ) {
                if( iyc>=0 && iyc<ny && ixc>=0 && ixc<nx ) {
                  if(overlay_map(iz,iyc,ixc)>0.0) neighbours++;
                }
              }
            }
          }

          if(neighbours<neighb) toberemoved(iz,iym,ixm)=0.0;
          else toberemoved(iz,iym,ixm)=1.0;
        }
      }
    }

    overlay_map*=toberemoved;
  }

  return overlay_map;
}


bool MiViewFmri::write_overlay_map(const STD_string& filename) {
  Data<float,3> tmpmap(get_overlay_map());
  return (tmpmap.autowrite(dumpzmap,FileWriteOpts(),&(data->protcopy))>=0);
}


void MiViewFmri::update() {
  Log<MiViewComp> odinlog("MiViewFmri","update");

  Range all=Range::all();
  int nz=data->pmap.extent(1);
  int ny=data->pmap.extent(2);
  int nx=data->pmap.extent(3);


  // update tcourse and z-statistic
  int nrep=tcourse.length();
  Data<float,1> tcourse_all(nrep); tcourse_all=0.0;
  int nvoxels=0;
  zsum = 0;
  tcourse_all=0.0;
  for(int iz=0; iz<nz; iz++) {
    for(int iy=0; iy<ny; iy++) {
      for(int ix=0; ix<nx; ix++) {
        if(overlay_map(iz,iy,ix)>0.0) {
          tcourse_all(all)+=data->fmri(all,iz,iy,ix);
          zsum += overlay_map(iz,iy,ix);
          nvoxels++;
        }
      }
    }
  }
 
  zcount = nvoxels;
  float zavg = secureDivision(zsum,zcount);

  // calculate stddev of zaverage
  float zavgerr = 0.0;
  for(int iz=0; iz<nz; iz++) {
    for(int iy=0; iy<ny; iy++) {
      for(int ix=0; ix<nx; ix++) {
        if(overlay_map(iz,iy,ix)>0.0) {
          zavgerr += pow(overlay_map(iz,iy,ix) - zavg,2);
        }
      }
    }
  }  
  zavgerr = sqrt(secureDivision(zavgerr,zcount-1));
  zaverage = ftos(zavg,2) + " +/- " + ftos(zavgerr,2); 
 
    
  if(nvoxels) tcourse_all/=float(nvoxels);
  for(int i=0; i<nrep; i++) {
//    tcourse[i]=STD_complex(tcourse_all(i))*expc(float2imag(data->designdata(i))); // Encode as amplitude and phase
    tcourse[i]=tcourse_all(i);
  }

  // update sigchange
  fmriResult fr=fmri_eval(tcourse_all, data->designdata);
  sigchange=ftos(100.0*fr.rel_diff,2)+" +/- "+ftos(100.0*fr.rel_err,2)+"%";
 
  // update sigcourse
  if(fr.Sbaseline>0.0) { // Use baseline, if available
    sigcourse.assignValues(tcourse);
    sigcourse-=fr.Sbaseline;
    sigcourse*=(100.0/fr.Sbaseline);
  } else {
    if(fr.Srest>0.0) { // fallback
      sigcourse.assignValues(tcourse);
      sigcourse-=fr.Srest;
      sigcourse*=(100.0/fr.Srest);
    }
  }



  if(dumptcourse!="") {
    STD_ofstream file(dumptcourse.c_str());
    for(int i=0; i<nrep; i++) file << sigcourse[i] << "\n";
  }

  ODINLOG(odinlog,normalDebug) << "dumpzmap=" << dumpzmap << STD_endl;
  if(dumpzmap!="") {
    write_overlay_map(dumpzmap);
  }

  ODINLOG(odinlog,normalDebug) << "dumpsmap=" << dumpsmap << STD_endl;
  if(dumpsmap!="") {
/*
    Data<float,4> mask(get_overlay_map());
    Data<float,4> smap_mask(data->smap.shape());

    smap_mask=where(Array<float,4>(mask)>0.0, Array<float,4>(data->smap), float(0.0));

    smap_mask.autowrite(dumpsmap,FileWriteOpts(),&(data->protcopy));
*/
    data->smap.autowrite(dumpsmap,FileWriteOpts(),&(data->protcopy));
  }

}
