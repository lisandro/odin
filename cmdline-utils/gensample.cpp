#include <odinseq/seqsim.h>
#include <odindata/data.h>


/**
  * \page gensample Generate a sample file
  * \verbinclude cmdline-utils/gensample.usage
  */


void inplane_reduction(Data<float,4>& map, int factor) {
  if(factor>1) {
    TinyVector<int,4> newshape=map.shape();
    newshape(3)/=factor;
    newshape(2)/=factor;
    map.congrid(newshape);
  }
}



farray create_map(const Data<float,4>& inmap) {
  Range all=Range::all();

  TinyVector<int,4> inshape=inmap.shape();

  Data<float,5> outmap(inshape(0),1,inshape(1),inshape(2),inshape(3));
  outmap(all,0,all,all,all)=inmap;

  return outmap;
}





/////////////////////////////////////////////////////////////////////////////////////////////////


void usage(const STD_string& missing_arg="") {
  STD_cout << STD_endl;
  STD_cout << "gensample: Generates a virtual sample suitable for sequence simulation in ODIN" << STD_endl;
  STD_cout << STD_endl;
  STD_cout << "Usage and options:" << STD_endl;
  STD_cout << "  Create a sample from external maps:" << STD_endl;
  STD_cout << "    gensample -fr <FOVread> -fp <FOVphase> -fs <FOVslice> -s0 <spin-density> [-t1 <T1map>] [-t2 <T2map>] [-pm <ppmMap>] [-ir <inplane-reduction>] [-ss <single-slice number>] [-ft <comma-separated-list of frame-rates for dymaic phantoms>]  -o <Sample-file>" << STD_endl;
  STD_cout << "  Create a point-spread function:" << STD_endl;
  STD_cout << "    gensample -psf -t1 <point T1> -t2 <point T2> [-dc <DiffusionCoeff>] -o <Sample-file>" << STD_endl;
  STD_cout << "  Create rectangle of uniform spin distribution about origin in all 3 spatial dimensions:" << STD_endl;
  STD_cout << "    gensample -uni -n <size> -f <FOV> [-t1 <uniform T1>] [-t2 <uniform T2>] -o <Sample-file>" << STD_endl;
  STD_cout << "  Create an isochromat distribution:" << STD_endl;
  STD_cout << "    gensample -iso -n <size> -df <freq-distribution-width[kHz]> -t1 <point T1> -t2 <point T2> [-dc <point Dcoeff>] -o <Sample-file>" << STD_endl;
  STD_cout << "  Create a homogenous disk with given radius:" << STD_endl;
  STD_cout << "    gensample -dsk -n <size> -f <FOV> -R <radius> [-t1 <uniform T1>] [-t2 <uniform T2>] [-dc <uniform DiffusionCoeff>] -o <Sample-file>" << STD_endl;
  STD_cout << "  Create coaxial cylinder:" << STD_endl;
  STD_cout << "    gensample -cyl -n <size> -f <FOV> -Xi <inner-suscept[ppm]> -Xo <outer-suscept[ppm]> -Ri <inner-radius> -Ro <outer-radius> [-t1 <uniform T1> -t2 <uniform T2> -sl <single-line>] -o <Sample-file>" << STD_endl;
  STD_cout << "Other options:" << STD_endl;
  STD_cout << "    " << LogBase::get_usage() << STD_endl;
  STD_cout << "    " << helpUsage() << STD_endl;
  if(missing_arg!="") STD_cout << "Missing argument: -" << missing_arg << STD_endl;
}

int main(int argc, char* argv[]) {
  LogBase::set_log_levels(argc,argv);
  Log<Para> odinlog("gensample","main");


  if(hasHelpOption(argc,argv)) {usage(); return 0;}

  Range all=Range::all();
  char optval[ODIN_MAXCHAR];


  STD_string phantom_fname;
  if(getCommandlineOption(argc,argv,"-o",optval,ODIN_MAXCHAR)) phantom_fname=optval; else {usage("o");exit(0);}


  Sample sample("Virtual Sample",false);

  bool valid_mode=false;

  //////////////////////////////////////////////////////////////////////////////////////////////////////////

  if(isCommandlineOption(argc,argv,"-psf")) {
    ODINLOG(odinlog,infoLog) << "Creating point spread function" << STD_endl;

    float t1=0.0;
    float t2=0.0;
    float D=0.0;

    if(getCommandlineOption(argc,argv,"-t1",optval,ODIN_MAXCHAR)) t1=atof(optval); else {usage("t1");exit(0);}
    if(getCommandlineOption(argc,argv,"-t2",optval,ODIN_MAXCHAR)) t2=atof(optval); else {usage("t2");exit(0);}
    if(getCommandlineOption(argc,argv,"-dc",optval,ODIN_MAXCHAR)) D=atof(optval);

    sample.resize(1,1,1,1,1);

    farray point(sample.get_extent());

    point=1.0; sample.set_spinDensity(point);
    point=t1;  sample.set_T1map(point);
    point=t2;  sample.set_T2map(point);
    point=D;   sample.set_DcoeffMap(point);

    sample.set_FOV(10.0);


    valid_mode=true;
  }

  //////////////////////////////////////////////////////////////////////////////////////////////////////////

  if(isCommandlineOption(argc,argv,"-iso")) {
    ODINLOG(odinlog,infoLog) << "Creating isochromat distribution" << STD_endl;

    float t1,t2,df;
    int n;
    float D=0;

    if(getCommandlineOption(argc,argv,"-df",optval,ODIN_MAXCHAR)) df=atof(optval); else {usage("df");exit(0);}
    if(getCommandlineOption(argc,argv,"-t1",optval,ODIN_MAXCHAR)) t1=atof(optval); else {usage("t1");exit(0);}
    if(getCommandlineOption(argc,argv,"-t2",optval,ODIN_MAXCHAR)) t2=atof(optval); else {usage("t2");exit(0);}
    if(getCommandlineOption(argc,argv,"-n" ,optval,ODIN_MAXCHAR)) n=atoi(optval);  else {usage("n");exit(0);}
    if(getCommandlineOption(argc,argv,"-dc",optval,ODIN_MAXCHAR)) D=atof(optval);

    float total_width=4.0*df;

    sample.resize(1,n,1,1,1);
    sample.set_freqrange(total_width);

    farray map(sample.get_extent());

    map=t1;  sample.set_T1map(map);
    map=t2;  sample.set_T2map(map);
    map=D;   sample.set_DcoeffMap(map);

    float sum=0.0;

    for(int i=0; i<n; i++) {
      float f=(secureDivision(i,n-1)-0.5)*total_width;
      if(n==1) f=0.0;
      map[i]=exp(-2.0*f*f/df/df);
      sum+=map[i];
    }

    sample.set_FOV(1.0e-6);

    map/=sum;
    sample.set_spinDensity(map);

    valid_mode=true;
  }

  //////////////////////////////////////////////////////////////////////////////////////////////////////////

  if(isCommandlineOption(argc,argv,"-cyl")) {
    ODINLOG(odinlog,infoLog) << "Creating cylinder" << STD_endl;

    float t1=0.0;
    float t2=0.0;
    float fov,Xi,Xo,Ri,Ro;
    int n;
    int sl=-1;

    if(getCommandlineOption(argc,argv,"-t1",optval,ODIN_MAXCHAR)) t1=atof(optval);
    if(getCommandlineOption(argc,argv,"-t2",optval,ODIN_MAXCHAR)) t2=atof(optval);
    if(getCommandlineOption(argc,argv,"-sl",optval,ODIN_MAXCHAR)) sl=atoi(optval);
    if(getCommandlineOption(argc,argv,"-n" ,optval,ODIN_MAXCHAR)) n=atoi(optval);  else {usage("n");exit(0);}
    if(getCommandlineOption(argc,argv,"-f" ,optval,ODIN_MAXCHAR)) fov=atof(optval);else {usage("f");exit(0);}
    if(getCommandlineOption(argc,argv,"-Xi" ,optval,ODIN_MAXCHAR)) Xi=atof(optval); else {usage("Xi");exit(0);}
    if(getCommandlineOption(argc,argv,"-Xo" ,optval,ODIN_MAXCHAR)) Xo=atof(optval); else {usage("Xo");exit(0);}
    if(getCommandlineOption(argc,argv,"-Ri" ,optval,ODIN_MAXCHAR)) Ri=atof(optval); else {usage("Ri");exit(0);}
    if(getCommandlineOption(argc,argv,"-Ro" ,optval,ODIN_MAXCHAR)) Ro=atof(optval); else {usage("Ro");exit(0);}

    int nx=n;
    int nz=n;
    if(sl>=0) nz=1;
    ODINLOG(odinlog,infoLog) << "nx/nz=" << nx << "/" << nz << STD_endl;

    ODINLOG(odinlog,infoLog) << "Initializing sample ..." << STD_endl;
    sample.resize(1,1,nz,1,nx); // x-z-plane
    sample.set_FOV(xAxis, fov);
    sample.set_FOV(yAxis, 1.0);
    sample.set_FOV(zAxis, fov);
    if(sl>=0) sample.set_FOV(zAxis, fov/n);

    ODINLOG(odinlog,infoLog) << "Initializing maps ..." << STD_endl;

    farray map(sample.get_extent());
    farray sdmap (sample.get_extent());
    sdmap=1.0;

    ODINLOG(odinlog,infoLog) << "Setting T1/T2 maps " << STD_endl;
    if(t1>0.0) {map=t1; sample.set_T1map(map);}
    if(t2>0.0) {map=t2; sample.set_T2map(map);}


    ODINLOG(odinlog,infoLog) << "Calculating sample ..." << STD_endl;
    map=0.0;
    ndim ii(n_sampleDim);
    ii[frameDim]=0;
    ii[freqDim]=0;
    ii[yDim]=0;

    for(int iz=0; iz<nz; iz++) {
      for(int ix=0; ix<nx; ix++) {
        float x=(double(ix)/double(n)-0.5)*fov;
        float z=(double(iz)/double(n)-0.5)*fov;
        if(sl>=0) z=(double(sl)/double(n)-0.5)*fov;
        float r=norm(x,z);
        ii[zDim]=iz;
        ii[xDim]=ix;
        if(r>=Ri && r<=Ro) {
          if(Xo==0.0 && Xi==0.0) map(ii)=0.0;
          else {
            float factor=0.5*(Xo-Xi)*Ri*Ri;
            map(ii)=1.0-Xo/6.0 + secureDivision(factor*(x*x-z*z),r*r*r*r);
          }
        } else {
          sdmap(ii)=0.0;
        }
      }
    }

    ODINLOG(odinlog,infoLog) << "Setting spinDensity/ppmMap maps ..." << STD_endl;
    sample.set_spinDensity(sdmap);
    sample.set_ppmMap(map);

    valid_mode=true;
  }

  //////////////////////////////////////////////////////////////////////////////////////////////////////////

  if(isCommandlineOption(argc,argv,"-dsk")) {
    ODINLOG(odinlog,infoLog) << "Creating disk" << STD_endl;

    float t1=0.0;
    float t2=0.0;
    float fov,R;
    int n;
    int nz=1; //20;
    float D=0;

    if(getCommandlineOption(argc,argv,"-t1",optval,ODIN_MAXCHAR)) t1=atof(optval);
    if(getCommandlineOption(argc,argv,"-t2",optval,ODIN_MAXCHAR)) t2=atof(optval);
    if(getCommandlineOption(argc,argv,"-dc",optval,ODIN_MAXCHAR)) D=atof(optval);
    if(getCommandlineOption(argc,argv,"-n" ,optval,ODIN_MAXCHAR)) n=atoi(optval);  else {usage("n");exit(0);}
    if(getCommandlineOption(argc,argv,"-f" ,optval,ODIN_MAXCHAR)) fov=atof(optval);else {usage("f");exit(0);}
    if(getCommandlineOption(argc,argv,"-R" ,optval,ODIN_MAXCHAR)) R=atof(optval); else {usage("R");exit(0);}


    ODINLOG(odinlog,infoLog) << "Initializing sample ..." << STD_endl;
    sample.resize(1,1,nz,n,n);
    sample.set_FOV(xAxis, fov);
    sample.set_FOV(yAxis, fov);
    sample.set_FOV(zAxis, nz);

    ODINLOG(odinlog,infoLog) << "Initializing maps ..." << STD_endl;
    farray sdmap (sample.get_extent()); sdmap=0.0;
    farray Dmap (sample.get_extent()); Dmap=0.0;


    ODINLOG(odinlog,infoLog) << "Setting T1/T2 " << STD_endl;
    if(t1>0.0) {sample.set_T1(t1);}
    if(t2>0.0) {sample.set_T2(t2);}


    ODINLOG(odinlog,infoLog) << "Calculating sample ..." << STD_endl;

    for(int iy=0; iy<n; iy++) {
      for(int ix=0; ix<n; ix++) {
        float x=(double(ix)/double(n)-0.5)*fov;
        float y=(double(iy)/double(n)-0.5)*fov;
        float r=norm(x,y);
        if(r<R) {
          for(int iz=0; iz<nz; iz++) {
            sdmap(0,0,iz,iy,ix)=1.0;
            Dmap(0,0,iz,iy,ix)=D;
          }
        }
      }
    }

    ODINLOG(odinlog,infoLog) << "Setting spinDensity/DcoeffMap maps ..." << STD_endl;
    sample.set_spinDensity(sdmap);
    if(D>0.0) sample.set_DcoeffMap(Dmap);

    valid_mode=true;
  }

  //////////////////////////////////////////////////////////////////////////////////////////////////////////

  if(isCommandlineOption(argc,argv,"-uni")) {
    ODINLOG(odinlog,infoLog) << "Creating uniform sample" << STD_endl;

    float t1=0.0;
    float t2=0.0;
    float fov;
    int n;

    if(getCommandlineOption(argc,argv,"-t1",optval,ODIN_MAXCHAR)) t1=atof(optval);
    if(getCommandlineOption(argc,argv,"-t2",optval,ODIN_MAXCHAR)) t2=atof(optval);
    if(getCommandlineOption(argc,argv,"-n" ,optval,ODIN_MAXCHAR)) n=atoi(optval);  else {usage("n");exit(0);}
    if(getCommandlineOption(argc,argv,"-f" ,optval,ODIN_MAXCHAR)) fov=atof(optval);else {usage("f");exit(0);}

    ODINLOG(odinlog,infoLog) << "Initializing sample ..." << STD_endl;
    sample.resize(1,1,n,n,n);
    sample.set_FOV(xAxis, fov);
    sample.set_FOV(yAxis, fov);
    sample.set_FOV(zAxis, fov);

    ODINLOG(odinlog,infoLog) << "Initializing maps ..." << STD_endl;
    farray map(sample.get_extent());

    ODINLOG(odinlog,infoLog) << "Setting T1/T2/PD maps " << STD_endl;
    if(t1>0.0) {map=t1; sample.set_T1map(map);}
    if(t2>0.0) {map=t2; sample.set_T2map(map);}
    if(t2>0.0) {map=1.0; sample.set_spinDensity(map);}

    valid_mode=true;
  }

  //////////////////////////////////////////////////////////////////////////////////////////////////////////

  if(!valid_mode) {
    ODINLOG(odinlog,infoLog) << "Combining arrays" << STD_endl;

    int inplane_reduction_factor=1;
    int single_slice=-1;
    dvector frame_durations;

    float FOVread;
    float FOVphase;
    float FOVslice;
    STD_string S0map_fname;
    STD_string T1map_fname;
    STD_string T2map_fname;
    STD_string ppmMap_fname;

    if(getCommandlineOption(argc,argv,"-fr",optval,ODIN_MAXCHAR)) FOVread=atof(optval);  else {usage("fr");exit(0);}
    if(getCommandlineOption(argc,argv,"-fp",optval,ODIN_MAXCHAR)) FOVphase=atof(optval); else {usage("fp");exit(0);}
    if(getCommandlineOption(argc,argv,"-fs",optval,ODIN_MAXCHAR)) FOVslice=atof(optval); else {usage("fs");exit(0);}

    if(getCommandlineOption(argc,argv,"-s0",optval,ODIN_MAXCHAR)) S0map_fname=optval; else {usage("s0");exit(0);}


    if(getCommandlineOption(argc,argv,"-t1",optval,ODIN_MAXCHAR)) T1map_fname=optval;
    if(getCommandlineOption(argc,argv,"-t2",optval,ODIN_MAXCHAR)) T2map_fname=optval;
    if(getCommandlineOption(argc,argv,"-pm",optval,ODIN_MAXCHAR)) ppmMap_fname=optval;

    if(getCommandlineOption(argc,argv,"-ir",optval,ODIN_MAXCHAR)) inplane_reduction_factor=atoi(optval);
    if(getCommandlineOption(argc,argv,"-ss",optval,ODIN_MAXCHAR)) single_slice=atoi(optval);


    int nframes_arg=0;
    if(getCommandlineOption(argc,argv,"-ft",optval,ODIN_MAXCHAR)) {
      svector toks=tokens(optval,',');
      nframes_arg=toks.size();
      ODINLOG(odinlog,infoLog) << "nframes_arg=" << nframes_arg << STD_endl;
      frame_durations.resize(nframes_arg);
      for(int i=0; i<nframes_arg; i++) {
        frame_durations[i]=atof(toks[i].c_str());
      }
    }

    int nframes_max=0;

    Data<float,4> filedata_s0;
    filedata_s0.autoread(S0map_fname);
    nframes_max=STD_max(nframes_max, filedata_s0.extent(0));

    Data<float,4> filedata_t1;
    if(T1map_fname!="") filedata_t1.autoread(T1map_fname);
    nframes_max=STD_max(nframes_max, filedata_t1.extent(0));

    Data<float,4> filedata_t2;
    if(T2map_fname!="") filedata_t2.autoread(T2map_fname);
    nframes_max=STD_max(nframes_max, filedata_t2.extent(0));

    Data<float,4> filedata_ppmmap;
    if(ppmMap_fname!="") filedata_ppmmap.autoread(ppmMap_fname);
    nframes_max=STD_max(nframes_max, filedata_ppmmap.extent(0));

    ODINLOG(odinlog,infoLog) << "nframes_max=" << nframes_max << STD_endl;

    if(nframes_max==nframes_arg) {
      sample.set_frame_durations(frame_durations);
    } else {
      ODINLOG(odinlog,infoLog) << "nframes_max(" << nframes_max << ") != nframes_arg(" << nframes_arg << ")" << STD_endl;
      return -1;
    }


    if(single_slice>=0) {
      FOVslice/=filedata_s0.extent(1);

      filedata_s0.reference(filedata_s0(all,Range(single_slice,single_slice),all,all));

      if(filedata_t1.size()) {
        filedata_t1.reference(filedata_t1(all,Range(single_slice,single_slice),all,all));
      }

      if(filedata_t2.size()) {
        filedata_t2.reference(filedata_t2(all,Range(single_slice,single_slice),all,all));
      }

      if(filedata_ppmmap.size()) {
        filedata_ppmmap.reference(filedata_t2(all,Range(single_slice,single_slice),all,all));
      }
    }


/*
    for(int i=0; i<product(fileshape); i++) {
      TinyVector<int,4> index=filedata_s0.create_index(i);

      bool valid_voxel=true;

      if(filedata_s0(index)<=0.0) valid_voxel=false;
      if(filedata_t1(index)<=0.0) valid_voxel=false;
      if(filedata_t2(index)<=0.0) valid_voxel=false;

      if(!valid_voxel) {
        filedata_s0(index)=0.0;
        filedata_t1(index)=0.0;
        filedata_t2(index)=0.0;
        filedata_ppmmap(index)=0.0;
      }

    }
*/

    if(inplane_reduction_factor>1) {

      inplane_reduction(filedata_s0,inplane_reduction_factor);

      if(filedata_t1.size()) inplane_reduction(filedata_t1,inplane_reduction_factor);
      if(filedata_t2.size()) inplane_reduction(filedata_t2,inplane_reduction_factor);
      if(filedata_ppmmap.size()) inplane_reduction(filedata_ppmmap,inplane_reduction_factor);
    }


    TinyVector<int,4> s0shape=filedata_s0.shape();

    sample.resize(s0shape(0),1,s0shape(1),s0shape(2),s0shape(3));

    sample.set_FOV(xAxis, FOVread);
    sample.set_FOV(yAxis, FOVphase);
    sample.set_FOV(zAxis, FOVslice);

    sample.set_spinDensity(create_map(filedata_s0));
    if(filedata_t1.size()) sample.set_T1map(create_map(filedata_t1));
    else sample.set_T1(1000.0);
    if(filedata_t2.size()) sample.set_T2map(create_map(filedata_t2));
    else sample.set_T2(100.0);
    if(filedata_ppmmap.size()) sample.set_ppmMap(create_map(filedata_ppmmap));
  }

  ODINLOG(odinlog,infoLog) << "sample.get_extent()=" << STD_string(sample.get_extent()) << STD_endl;

  sample.write(phantom_fname);

  return 0;
}
