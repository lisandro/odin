#include <odindata/fileio.h>
#include <odindata/utils.h>
#include <odindata/filter.h>

/**
  * \page miconv Conversion utility for medical images
  * \verbinclude cmdline-utils/miconv.usage
  *
  * \section miconv_examples Some examples how to use miconv:
  *
  * Read a bunch of DICOM files (format 'dcm'), produced by Siemens, from directory 'mosaic' and convert it to NIFTI file 'mosaic.nii'
  * with a dialect suitable for FSL:
  *   \verbatim
       miconv -rf dcm -rdialect siemens -wdialect fsl mosaic mosaic.nii
      \endverbatim
  *
  * Converting a short (16 bit) raw data file '2dseq' of size 256x256 to a DICOM file 'image.dcm':
  *   \verbatim
       miconv -nx 256 -ny 256 -rf short 2dseq image.dcm
      \endverbatim
  */

struct MiConvOpts : LDRblock {

  LDRbool   quiet;
  LDRstring protfile;
  LDRstring parlist;

  MiConvOpts() {

    quiet.set_cmdline_option("q").set_description("Quiet mode: do not print progress meter");
    append_member(quiet,"quiet");

    protfile.set_cmdline_option("p").set_description("Load this protcol for defaults");
    append_member(protfile,"protfile");

    parlist.set_cmdline_option("l").set_description("Lists space-separated set of protocol parameters and exits. Use '-l help' to get a list of possible parameters and '-l all' to list all parameters. The parameter 'output-file' must be omitted when using this option.");
    append_member(parlist,"parlist");
  }

};

////////////////////////////////////////

void usage(const Protocol& prot) {
  FileReadOpts ropts;
  FileWriteOpts wopts;
  MiConvOpts mopts;
  FilterChain filter;
  STD_cout << "miconv:  Converts medical image data between formats." << STD_endl;
  STD_cout << "         File formats are automatically identified by their file extension." << STD_endl;
  STD_cout << "         It expects at least two filenames/directories (for the input and output file) at the end of the command line." << STD_endl;
  STD_cout << "         An exception is the option '-l' which requires only one filename/directory to list its content." << STD_endl;
  STD_cout << "Usage: miconv [options] <input-file> [<output-file>]" << STD_endl;
  STD_cout << "General options:" << STD_endl;
  STD_cout << mopts.get_cmdline_usage("\t");
  STD_cout << "Protocol options:" << STD_endl;
  STD_cout << prot.get_cmdline_usage("\t");
  STD_cout << "File read options:" << STD_endl;
  STD_cout << ropts.get_cmdline_usage("\t");
  STD_cout << "File write options:" << STD_endl;
  STD_cout << wopts.get_cmdline_usage("\t");
  STD_cout << "Filters:" << STD_endl;
  STD_cout << filter.get_cmdline_usage("\t");
  STD_cout << "Other options:" << STD_endl;
  STD_cout << "\t" << LogBase::get_usage() << STD_endl;
  STD_cout << "\t" << helpUsage() << STD_endl;
  STD_cout << "Supported file extensions(formats):" << STD_endl;
  STD_cout << FileIO::autoformats_str("\t") << STD_endl;
}


////////////////////////////////////////

int main(int argc, char* argv[]) {
  if(LogBase::set_log_levels(argc,argv)) return 0;

  Log<OdinData> odinlog("miconv","main");

  char optval[ODIN_MAXCHAR];

  Protocol prot_template;
  FileReadOpts  ropts;
  FileWriteOpts wopts;
  MiConvOpts mopts;

  if(hasHelpOption(argc,argv)) {usage(prot_template); return 0;}
  if(argc<3) {usage(prot_template); return 0;}

  // set defaults
  prot_template.seqpars.set_MatrixSize(readDirection,1);
  prot_template.seqpars.set_MatrixSize(phaseDirection,1);
  prot_template.seqpars.set_MatrixSize(sliceDirection,1);

  mopts.parse_cmdline_options(argc,argv);

  STD_string infile;
  STD_string outfile;


  if(mopts.parlist!="") {
    infile=argv[argc-1];
  } else {
    infile=argv[argc-2];
    outfile=argv[argc-1];
  }

  if(mopts.parlist=="help") {
    STD_cout << "Possible protocol parameters:" << STD_endl;
    for(unsigned int i=0; i<prot_template.numof_pars(); i++) {
      LDRbase& ldr=prot_template[i];
      if(ldr.get_filemode()!=exclude) {
	STD_cout <<  ldr.get_label() << STD_endl;
      }
    }
    return 0;
  }

  svector pars;
  if(mopts.parlist=="all") {
    for(unsigned int i=0; i<prot_template.numof_pars(); i++) {
      LDRbase& ldr=prot_template[i];
      if(ldr.get_filemode()!=exclude) pars.push_back(ldr.get_label());
    }
  } else {
    pars=tokens(mopts.parlist);
  }

  int npars=pars.size();
  if(npars) FileIO::set_trace_status(false);

  if(mopts.protfile!="") {
    ODINLOG(odinlog,infoLog) << "Loading default protocol " << optval << STD_endl;
    prot_template.load(mopts.protfile);
  }

  // override parameters of protocol file
  prot_template.parse_cmdline_options(argc,argv,false); // do not modify argc/argv on 1st pass

  FileIO::ProtocolDataMap pdmap_in;

  // Read data
  ropts.parse_cmdline_options(argc,argv);

  ProgressDisplayConsole display;
  ProgressMeter progmeter(display);
  ProgressMeter* progmeter_ptr=0;
  if(!mopts.quiet) progmeter_ptr=&progmeter;

  int readresult=FileIO::autoread(pdmap_in, infile, ropts, prot_template,progmeter_ptr);

  if(readresult<0) {
    ODINLOG(odinlog,errorLog) << "autoread failed" << STD_endl;
    return readresult;
  }

  // If requested, print formatted protocol properties and exit
  if(npars) {
    
    int ncols=npars;
    int irow=0;
    int ndsets=pdmap_in.size();
    if(ndsets>1) {
      ncols++;
    }
    int nrows=ndsets;
    if(npars>1) nrows++;
    sarray table(nrows,ncols);
    if(npars>1) {
      for(int ipar=0; ipar<npars; ipar++) {
        table(0,ipar)=pars[ipar];
      }
      table(0,npars)="Dataset";
      irow++;
    }
    for(FileIO::ProtocolDataMap::const_iterator pdit=pdmap_in.begin(); pdit!=pdmap_in.end(); ++pdit) {
      for(int ipar=0; ipar<npars; ipar++) {
        table(irow,ipar)=rmblock(pdit->first.printval(pars[ipar]), "\n", ""); // truncate after first newline
      }
      if(ndsets>1) {
        STD_ostringstream oss;
        oss << pdit->second.shape();
        table(irow,npars)=oss.str();
      }
      irow++;
    }

    STD_cout << print_table(table);

    return 0;
  }

  ODINLOG(odinlog,normalDebug) << "mem(in ): " << Profiler::get_memory_usage() << STD_endl;

  // Create copy of protocol-data map, thereby modify protocol
  FileIO::ProtocolDataMap pdmap_out;
  for(FileIO::ProtocolDataMap::const_iterator pdit=pdmap_in.begin(); pdit!=pdmap_in.end(); ++pdit) {
    Protocol prot(pdit->first);
    prot.parse_cmdline_options(argc,argv); // override parameters of infile
    pdmap_out[prot].reference(pdit->second);
  }

  ODINLOG(odinlog,normalDebug) << "mem(out): " << Profiler::get_memory_usage() << STD_endl;

  // Parse before filters to remove options
  wopts.parse_cmdline_options(argc,argv);

  FilterChain filterchain(argc,argv);

  //apply filters on pdmap_out
  if(!filterchain.apply(pdmap_out)) return -1;


  // Write data
  int result=FileIO::autowrite(pdmap_out, outfile, wopts);

  if(result>0) result=0;
  return result;
}
