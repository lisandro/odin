#include <stdlib.h>

#include <iostream>
#include <fstream>

/**
 * \page swab Swap adjacent bytes
 * \verbinclude cmdline-utils/swab.usage
 */

int main(int argc, char* argv[]) {

  if(argc < 4 || argc > 5) {
    std::cout << "swab: Mirrors <word-size> bytes, in the case of 2, it swaps adjacent bytes" << std::endl;
    std::cout << "Usage: swab  <word-size>  <infile> <outfile>  [<block size in MB>]" << std::endl;
    return 0;
  }

  char *temp1,*temp2;
  unsigned long n_bytes;
  unsigned long block_size,i,k,n_data,j;
  std::ofstream out_data;

  n_bytes = atoi(argv[1]);
  if(n_bytes<=0) {
    std::cerr << "swab: ERROR: invalid n_bytes=" << n_bytes << std::endl;
    return -1;
  }

  std::ifstream in_data(argv[2],std::ios::in|std::ios::binary);
  if(in_data.bad()) {
    std::cerr << "swab: ERROR: can't open file " << argv[2] << std::endl;
    return -1;
  }
  in_data.seekg(0,std::ios::end);
  if(argc == 5) block_size = (unsigned)atol(argv[4])*1000000;
  else block_size = in_data.tellg();
  block_size-= block_size%n_bytes;
  n_data=in_data.tellg();
  if(block_size > n_data) {
    //		std::cerr << "swab: block size exceeding file size !" << std::endl
    //			 << "setting block size to file size (=" << n_data/1000000. << "MB)" << std::endl;
    block_size = n_data;
  }
  in_data.seekg(0,std::ios::beg);

  if((float)n_data/(float)n_bytes-n_data/n_bytes) {
    std::cerr << "swab: ERROR: swab: size of \n" << argv[2] <<
      "not a multiple of no. of swap bytes (" << n_bytes << ")" << std::endl;
    return -1;
  }

  temp1=new char[block_size];
  temp2=new char[block_size];

  for(k=0;k<n_data/block_size;k++) {
    if(k == 0) {
      out_data.open(argv[3],std::ios::out|std::ios::binary);
      if(out_data.bad()) {
        std::cerr << "swab: ERROR: can't open file " << argv[3] << std::endl;
        return -1;
      }
    }
    else {
      out_data.open(argv[3],std::ios::out|std::ios::binary|std::ios::app);
      if(out_data.bad()) {
        std::cerr << "swab: ERROR: can't open file " << argv[3] << std::endl;
        return -1;
      }
    }
    std::cout << "swab: reading block "<< k+1 << " of " <<n_data/block_size
      << " blocks" << std::endl;
    in_data.read(temp1,block_size);
    std::cout << "swab: swabbing block "<< k+1 <<" ..." << std::endl;
    for(i=0;i<block_size/n_bytes;i++)
      for(j=0;j<n_bytes;j++)temp2[i*n_bytes+j]=temp1[(i+1)*n_bytes-1-j];
    out_data.write(temp2,block_size);
    out_data.close();
    std::cout << "swab: block "<< k+1 << " written to file " << argv[3] << std::endl;
  }

  temp1=new char[n_data%block_size];
  temp2=new char[n_data%block_size];
  std::cout << "swab: reading rest block of size " << n_data%block_size/1000 << " KByte" << std::endl;
  in_data.read(temp1,n_data%block_size);
  std::cout << "swab: swabbing rest block ... "<< std::endl;
  for(i=0;i<n_data%block_size/n_bytes;i++)
    for(j=0;j<n_bytes;j++)temp2[i*n_bytes+j]=temp1[(i+1)*n_bytes-1-j];
  out_data.open(argv[3],std::ios::out|std::ios::binary|std::ios::app);
  out_data.write(temp2,n_data%block_size);
  out_data.close();
  std::cout << "swab: rest block written to file " << argv[3] << std::endl;

  in_data.close();
  return 0;
}
