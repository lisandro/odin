//program to preform a unit test on crucial ODIN components

#include <tjutils/tjtest.h>
#include <tjutils/tjlog.h>

// Avoid including lots of header
void alloc_TjToolsTest();
void alloc_ComplexTest();
void alloc_VectorTest();
void alloc_ValListTest();
void alloc_ListTest();
void alloc_StringTest();
void alloc_NumericsTest();
void alloc_StlTest();
void alloc_NdimTest();
void alloc_ArrayTest();
void alloc_IndexTest();
void alloc_ProcessTest();
void alloc_ThreadTest();
void alloc_DownhillSimplexTest();
void alloc_LDRstringArrTest();
void alloc_LDRintArrTest();
void alloc_LDRcomplexArrTest();
void alloc_GeometryTest();
void alloc_LDRintTest();
void alloc_LDRstringTest();
void alloc_LDRcomplexTest();
void alloc_LDRboolTest();
void alloc_LDRenumTest();
void alloc_LDRfileNameTest();
void alloc_LDRserTest();
void alloc_CoilSensitivityTest();
void alloc_ProtocolTest();
void alloc_DataUtilsTest();
void alloc_DataTest();
void alloc_ComplexDataTest();
void alloc_FileIOTest();
#ifndef FILEIO_ONLY
void alloc_FunctionFitTest();
void alloc_LinAlgTest();
void alloc_FunctionIntegralTest();
void alloc_StatisticsTest();
void alloc_GriddingTest();
#endif //FILEIO_ONLY


int main(int argc, char* argv[]) {
  if(LogBase::set_log_levels(argc,argv)) return 0;

  if(hasHelpOption(argc,argv)) {STD_cout << "UnitTest of ODIN" << STD_endl; return 0;}

#ifdef NO_UNIT_TEST
  STD_cout << "UnitTest disabled" << STD_endl;
  return 0;
#else

  alloc_TjToolsTest();
  alloc_ComplexTest();
  alloc_VectorTest();
  alloc_ValListTest();
  alloc_ListTest();
  alloc_StringTest();
  alloc_NumericsTest();
  alloc_StlTest();
  alloc_NdimTest();
  alloc_ArrayTest();
  alloc_IndexTest();
  alloc_ProcessTest();
  alloc_ThreadTest();
  alloc_LDRserTest();
  alloc_LDRintTest();
  alloc_LDRstringTest();
  alloc_LDRcomplexTest();
  alloc_LDRboolTest();
  alloc_LDRenumTest();
  alloc_LDRfileNameTest();
  alloc_LDRstringArrTest();
  alloc_LDRintArrTest();
  alloc_LDRcomplexArrTest();
  alloc_GeometryTest();
  alloc_CoilSensitivityTest();
  alloc_ProtocolTest();
  alloc_DataUtilsTest();
  alloc_DataTest();
  alloc_ComplexDataTest();
  alloc_FileIOTest();
#ifndef FILEIO_ONLY
  alloc_FunctionFitTest();
  alloc_LinAlgTest();
  alloc_FunctionIntegralTest();
  alloc_StatisticsTest();
  alloc_GriddingTest();
#endif //FILEIO_ONLY

  int result=UnitTest::check_all();
  Static::destroy_all(); // testing StaticHandler
  return result;
#endif
}
