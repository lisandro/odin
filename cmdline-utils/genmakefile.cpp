#include <odinseq/seqmakefile.h>

/**
  * \page genmakefile Generate a Makefile for ODIN sequences
  * \verbinclude cmdline-utils/genmakefile.usage
  */

void usage() {
  STD_cout << "genmakefile: Generates a Makefile for ODIN methods" << STD_endl;
  STD_cout << "Usage: genmakefile [options] <method-label>" << STD_endl;
  STD_cout << "Options:" << STD_endl;
  STD_cout << "\t" << LogBase::get_usage() << STD_endl;
  STD_cout << "\t" << helpUsage() << STD_endl;
}

int main(int argc, char* argv[]) {
  LogBase::set_log_levels(argc,argv);
  if(hasHelpOption(argc,argv)) {usage(); return 0;}

  char buff[ODIN_MAXCHAR];

  if(argc<=1) {usage(); exit(0);}

  STD_string methlabel;
  if(getLastArgument(argc,argv,buff,ODIN_MAXCHAR)) {
    methlabel=buff;
  }

  SeqMakefile mf(methlabel,INSTALL_PREFIX);
  STD_cout << mf.get_Makefile(".") << STD_endl;

  return 0;
}
